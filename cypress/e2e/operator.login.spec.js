describe('Login page', function () {
  it('Loads log in', async function () {
    cy.visit('/')
      .getByTestId('login-page-title')
      .should('exist')
      .should('contain', 'Log in')
  })

  it('Logs in as operator', async function () {
    cy.visit('/')
      .get('input[name="email"]')
      .type('operator@flavorwiki.com')
      .get('input[name="password"]')
      .type('operator')
      .getByTestId('submit-button')
      .click()
      .assertRoute('/operator/dashboard')
  })
})
