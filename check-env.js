const spawn = require('child_process').spawnSync

async function checkEnv () {
  try {
    console.log('os:')
    console.log(process.platform, process.arch)
    console.log()

    console.log('exec:')
    console.log(process.execPath, process.execArgv)
    console.log()

    console.log('node info:')
    const printOutput = (...args) => {
      const { stdout, stderr } = spawn(...args)
      return (stdout && stdout.toString()) || (stderr && stderr.toString())
    }
    console.log({
      npm: printOutput('npm', ['-v']),
      jest: printOutput('jest', ['-v']),
      ...process.versions
    })
    console.log()

    console.log('dump env:')
    console.log(process.env)
    console.log()

    console.log('versions:')
    const printVersion = path => {
      try {
        const mod = require(path)
        return mod && mod.version
      } catch {
        return 'N/A'
      }
    }
    ;[
      'react',
      'react-dom',
      'react-responsive',
      'jest',
      'enzyme',
      'enzyme-adapter-react-16',
      'enzyme-to-json',
      'antd'
    ].map(mod =>
      console.log(
        '  -',
        mod,
        printVersion(`${mod}/package.json`),
        '/',
        printVersion(`./node_modules/${mod}/package.json`)
      )
    )
  } catch (ex) {
    console.log(ex)
  }
}

checkEnv()
