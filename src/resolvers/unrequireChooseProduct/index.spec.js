import unrequireChooseProduct from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const mockSurveyCreation = {
  surveyId: 'survey-1',
  basics: {
    name: 'Survey 1',
    minimumProducts: 1,
    maximumProducts: 1
  },

  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-2',
      type: 'choose-product',
      prompt: 'Product namem'
    },
    {
      id: 'question-3',
      type: 'name',
      prompt: 'What is your name?'
    }
  ],
  mandatoryQuestions: [
    {
      id: 'question-2',
      type: 'choose-product',
      prompt: 'Product namem'
    }
  ]
}

const mockSurveyCreationNotChooseProduct = {
  surveyId: 'survey-1',
  basics: {
    name: 'Survey 1',
    minimumProducts: 1,
    maximumProducts: 1
  },

  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-2',
      type: 'choose-product',
      prompt: 'Product namem'
    },
    {
      id: 'question-3',
      type: 'name',
      prompt: 'What is your name?'
    }
  ],
  mandatoryQuestions: [
    {
      id: 'question-2',
      type: 'email',
      prompt: 'What is your email?'
    }
  ]
}

describe('unrequireChooseProduct', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query for unrequireChooseProduct', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })

    unrequireChooseProduct({}, {}, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })

  test('should call SURVEY_CREATION query for mandatoryQuestions choose-product is null', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreationNotChooseProduct
    })

    unrequireChooseProduct({}, {}, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
