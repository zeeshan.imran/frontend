import unrequirePaypalEmail from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const mockSurveyCreation = {
  surveyId: 'survey-1',
  basics: {
    name: 'Survey 1',
    minimumProducts: 1,
    maximumProducts: 1
  },

  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-2',
      type: 'choose-product',
      prompt: 'Product namem'
    },
    {
      id: 'question-3',
      type: 'name',
      prompt: 'What is your name?'
    }
  ],
  mandatoryQuestions: [
    {
      id: 'question-2',
      type: 'choose-product',
      prompt: 'Product namem'
    }
  ]
}

const mockSurveyCreationPaypalEmail = {
  surveyId: 'survey-1',
  basics: {
    name: 'Survey 1',
    minimumProducts: 1,
    maximumProducts: 1
  },

  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-2',
      type: 'paypal-email',
      prompt: 'Paypal Email'
    },
    {
      id: 'question-3',
      type: 'name',
      prompt: 'What is your name?'
    }
  ],
  mandatoryQuestions: [
    {
      id: 'question-2',
      type: 'email',
      prompt: 'What is your email?'
    }
  ]
}

describe('unrequirePaypalEmail', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query for unrequirePaypalEmail', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })

    unrequirePaypalEmail({}, {}, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })

  test('should call SURVEY_CREATION query for mandatoryQuestions choose-product is null', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreationPaypalEmail
    })

    unrequirePaypalEmail({}, {}, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
