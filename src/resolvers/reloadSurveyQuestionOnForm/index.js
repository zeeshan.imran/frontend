/** @typedef {import("apollo-cache").ApolloCache} ApolloCache */
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const buildReloadQuestion = ({
  clientGeneratedId,
  updatedQuestion: { id, skipFlow }
}) => question => {
  if (clientGeneratedId && question.clientGeneratedId === clientGeneratedId) {
    return {
      ...question,
      id,
      clientGeneratedId: undefined,
      skipFlow
    }
  }

  if (question.id === id) {
    return {
      ...question,
      skipFlow
    }
  }

  return question
}

/**
 * When receiving question from GraphQL Server:
 *   - on question: delete question's clientGeneratedId, update question's id
 *   - on question: update new skipFlow (recalculated on server-side)
 * @param {*} _
 * @param {*} param1
 * @param {{ cache: ApolloCache }} param2
 */
const reloadSurveyQuestionOnForm = (
  _,
  { clientGeneratedId, question: updatedQuestion },
  { cache }
) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })
  const { questions, mandatoryQuestions, ...restOfSurvey } = surveyCreation

  const reloadQuestion = buildReloadQuestion({
    clientGeneratedId,
    updatedQuestion
  })

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...restOfSurvey,
        questions: questions.map(reloadQuestion),
        mandatoryQuestions: mandatoryQuestions.map(reloadQuestion)
      }
    }
  })
}

export default reloadSurveyQuestionOnForm
