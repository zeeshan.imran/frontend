import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { pickBy, type } from 'ramda'

export default (_, { ...properties }, { cache }) => {
  let currentSurveyParticipation = {}
  const propertiesToChange = pickBy(
    val => type(val) !== 'Undefined' && val !== 'null',
    properties
  )
  try {
    const data = cache.readQuery({
      query: SURVEY_PARTICIPATION_QUERY
    })
    currentSurveyParticipation = data.currentSurveyParticipation
  } catch (error) {
    cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          __typename: 'CurrentSurveyParticipation',
          ...propertiesToChange
        }
      }
    })
  }

  cache.writeQuery({
    query: SURVEY_PARTICIPATION_QUERY,
    data: {
      currentSurveyParticipation: {
        __typename: 'CurrentSurveyParticipation',
        ...currentSurveyParticipation,
        ...propertiesToChange
      }
    }
  })

  return null
}
