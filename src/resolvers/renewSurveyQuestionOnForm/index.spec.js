import renewSurveyQuestionOnForm from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const mockSurveyCreation = {
  surveyId: 'survey-1',
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?',
      skipFlow: {
        rules: [{ skipTo: ['question-2'] }]
      }
    },
    {
      id: 'question-2',
      type: 'choose-product',
      prompt: 'Product namem'
    }
  ],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}

describe('renewSurveyQuestionOnForm', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query for renewSurveyQuestionOnForm', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })

    renewSurveyQuestionOnForm({}, { questionIndex: 1 }, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
