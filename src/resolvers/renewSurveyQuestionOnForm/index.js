/** @typedef {import("apollo-cache").ApolloCache} ApolloCache */
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { generate } from 'shortid'
import skipFlowUtils from '../../utils/skipFlowUtils'

/**
 * When renew the question before republishing the survey:
 *   - on question: delete question's id, generate new question's clientGeneratedId
 *   - on question: update skipFlow (recalculated on client-side)
 * @param {*} _
 * @param {*} param1
 * @param {{ cache: ApolloCache }} param2
 */
const renewSurveyQuestionOnForm = (_, _args, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })
  const { questions, mandatoryQuestions, ...restOfSurvey } = surveyCreation

  const allQuestions = [...questions, ...mandatoryQuestions]

  const generatedIdByOldId = {}
  for (let question of allQuestions) {
    if (question.id) {
      const originalId = question.id
      const newClientId = generate()
      generatedIdByOldId[originalId] = newClientId
      question.clientGeneratedId = newClientId

      for (let skipQuestion of allQuestions) {
        if (skipQuestion.skipFlow) {
          skipQuestion.skipFlow = skipFlowUtils
            .fromSkipFlow(skipQuestion.skipFlow)
            .updateRules(rule => rule.skipTo === originalId, {
              skipTo: newClientId
            })
            .getNextSkipFlow()
        }
      }
    }
  }

  for (let question of allQuestions) {
    if (question.likingQuestion) {
      question.likingQuestion =
        generatedIdByOldId[question.likingQuestion] || question.likingQuestion
    }
    delete question.id
  }

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...restOfSurvey,
        questions: questions,
        mandatoryQuestions: mandatoryQuestions
      }
    }
  })
}

export default renewSurveyQuestionOnForm
