import SIGN_UP_FORM_QUERY from '../../queries/signUpForm'

const SIGN_UP_FORM_DEFAULT = {
  signUpForm: {
    __typename: 'SignUpForm',
    userId: '',
    firstStepValid: false,
    secondStepValid: false,
    lastStepValid: false,
    email: '',
    password: '',
    firstname: '',
    lastname: '',
    birthday: '',
    gender: '',
    language: '',
    ethnicity: '',
    currency: '',
    incomeRange: '',
    hasChildren: '',
    numberOfChildren: 0,
    childrenAges: [],
    smokerKind: '',
    productTypes: [],
    dietaryRestrictions: [],
    recaptchaResponseToken: ''
  }
}

export default {
  defaults: {
    ...SIGN_UP_FORM_DEFAULT
  },
  resolvers: {
    Mutation: {
      resetSignUpForm: (_, __, { cache }) => {
        const data = {
          ...SIGN_UP_FORM_DEFAULT
        }
        cache.writeData({ query: SIGN_UP_FORM_QUERY, data })
        return data
      },
      updateSignUpForm: (_, { field, value }, { cache }) => {
        const data = {
          signUpForm: { [field]: value, __typename: 'SignUpForm' }
        }
        cache.writeData({ query: SIGN_UP_FORM_QUERY, data })
        return data
      },
      updateChildrenAges: (_, { index, value }, { cache }) => {
        const {
          signUpForm: { childrenAges: previousChildrenAges }
        } = cache.readQuery({ query: SIGN_UP_FORM_QUERY })

        previousChildrenAges[index] = value

        const data = {
          signUpForm: {
            childrenAges: previousChildrenAges,
            __typename: 'SignUpForm'
          }
        }
        cache.writeData({ query: SIGN_UP_FORM_QUERY, data })
        return null
      }
    }
  }
}
