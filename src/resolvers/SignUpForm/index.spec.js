import signupForm from '.'
import SIGN_UP_FORM_QUERY from '../../queries/signUpForm'

describe('updateSurveyCreationBasics', () => {
  let mockedCache = (function () {
    let store = { ...signupForm.defaults }
    return {
      readData: function () {
        return store
      },
      writeData: function (value) {
        store = {
          ...store,
          signUpForm: {
            ...store.signUpForm,
            ...value.data.signUpForm
          }
        }
      }
    }
  })()
  test('updateSignUpForm', () => {
    signupForm.resolvers.Mutation.updateSignUpForm(
      {},
      { field: 'some field from signUpForm.defaults', value: 'some value' },
      { cache: mockedCache }
    )
    expect(JSON.stringify(mockedCache.readData().signUpForm)).toBe(
      JSON.stringify({
        ...signupForm.defaults.signUpForm,
        'some field from signUpForm.defaults': 'some value'
      })
    )
  })

  test('resetSignUpForm', () => {
    signupForm.resolvers.Mutation.resetSignUpForm(
      {},
      { field: 'some field from signUpForm.defaults', value: 'some value' },
      { cache: mockedCache }
    )
    expect(JSON.stringify(mockedCache.readData().signUpForm)).toBe(
      JSON.stringify({
        ...signupForm.defaults.signUpForm,
        'some field from signUpForm.defaults': 'some value'
      })
    )
  })

  test('updateChildrenAges', () => {
    let mockedCache1 = (function () {
      let store = { ...signupForm.defaults }
      return {
        readData: function () {
          return store
        },
        readQuery: function () {
          return {
            signUpForm: {childrenAges : [1,2]}
          }
        },

        writeData: function (value) {
          store = {
            ...store,
            signUpForm: {
              ...store.signUpForm,
              ...value.data.signUpForm,
              childrenAges: []
            }
          }
        }
      }
    })()

    signupForm.resolvers.Mutation.updateChildrenAges(
      {},
      { index: 1, value: 'some value' },
      { cache: mockedCache1 }
    )
    expect(JSON.stringify(mockedCache1.readData().signUpForm)).toBe(
      JSON.stringify({
        ...signupForm.defaults.signUpForm,
      })
    )
  })
})
