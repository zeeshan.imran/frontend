/** @typedef {import("apollo-cache").ApolloCache} ApolloCache */
/** @typedef {import("apollo-client").ApolloClient} ApolloClient */
import gql from 'graphql-tag'
import { values, last, findLast } from 'ramda'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { surveyInfo, questionInfo } from '../../fragments/survey'
import isSurveyHasOnlyOneProduct from '../../utils/isSurveyHasOnlyOneProduct'
import getAnswerLocalId from '../../utils/getAnswerLocalId'
import skipFlowUtils from '../../utils/skipFlowUtils'
import { getAuthenticatedUser } from '../../utils/userAuthentication'

// const checkAuthUser = getAuthenticatedUser()

const getLastAnswerValue = surveyParticipation => {
  const localAnswer = last(surveyParticipation.answers)
  const answer = localAnswer && localAnswer.values && localAnswer.values[0]
  // if answer is object contains { value }, returns answer.value. Otherwise returns answer
  return (answer && answer.value) || answer
}

const getResumeSurveyLastAnsweredValue = surveyParticipation => {
  // TO DO luca - test it and if necessary, make it work for other type of questions not only choose one
  const localAnswer = last(surveyParticipation.answers)
  const answer = localAnswer && localAnswer.value && localAnswer.value[0]

  // if answer is object contains { value }, returns answer.value. Otherwise returns answer
  return (answer && answer.value) || answer
}

const getCurrentAnswers = surveyParticipation => {
  const localAnswer = last(surveyParticipation.answers)
  const answer = localAnswer && localAnswer.values
  // if answer is object contains { value }, returns answer.value. Otherwise returns answer
  return (answer && answer.value) || answer
}

const CLIENT_GENERATED_STEPS = {
  SCREENING_COMPLETED: 'screening-completed',
  INSTRUCTIONS: 'instructions',
  FORCED_LOGIN: 'survey-signup',
  FINISHED: 'finished',
  REJECTED: 'rejected'
}

/**
 * @param {*} _
 * @param {*} param1
 * @param {{ cache: ApolloCache, client: ApolloClient }} param2
 */
export default async (
  _,
  { fromStep, skipLoop, resumeSurvey },
  { cache, client }
) => {
  const { currentSurveyParticipation: existingParticipation } = cache.readQuery(
    {
      query: SURVEY_PARTICIPATION_QUERY
    }
  )

  let {
    surveyId,
    selectedProducts,
    answers,
    surveyEnrollmentId,
    selectedProduct
  } = existingParticipation

  const { survey = {} } = cache.readQuery({
    query: SURVEY_QUERY,
    variables: { id: surveyId }
  })

  const isOnlyOneProduct = isSurveyHasOnlyOneProduct(survey)

  let allSteps = getSortedSteps(survey)
// Always need to show product selection screen
  // if (isOnlyOneProduct) {
    // allSteps = removeChooseProductSteps(allSteps)
  // }

  const { products } = survey
  const availableProducts =
    (products && products.filter(product => product.isAvailable)) || []
  let nextStep = {}

  const moveNext = fromStep => {
    const stepIndex = getStepIndex(fromStep, allSteps)
    nextStep = getNextStep({
      steps: allSteps,
      currentStepIndex: stepIndex,
      numberOfLoopsStarted: selectedProducts.length,
      maxNumberOfLoops: survey.maximumProducts || products.length,
      answers
    })
  }

  if (skipLoop) {
    nextStep = getStepAfterLoop(allSteps)
  } else {
    let matchedSkipRule = null
    if (fromStep && !values(CLIENT_GENERATED_STEPS).includes(fromStep)) {
      const {
        data: { question }
      } = await client.query({
        query: QUESTION_QUERY,
        variables: { id: fromStep }
      })

      if (
        [
          'choose-product',
          'choose-one',
          'dropdown',
          'select-and-justify',
          'vertical-rating',
          'choose-multiple',
          'matrix'
        ].includes(question.type)
      ) {
        let answerValue
        if (!resumeSurvey) {
          answerValue =
            question.type === 'choose-multiple'
              ? getCurrentAnswers(existingParticipation)
              : getLastAnswerValue(existingParticipation)
        } else {
          answerValue = getResumeSurveyLastAnsweredValue(existingParticipation)
        }
        matchedSkipRule = findSkipRule(question, answerValue)
      }
    }

    if (matchedSkipRule) {
      nextStep = allSteps.find(step => step.step === matchedSkipRule.skipTo)
    } else {
      moveNext(fromStep)
      if (nextStep && nextStep.type === 'profile') {
        const fromStepIndex = getStepIndex(fromStep, allSteps)
        const step = allSteps[fromStepIndex]
        if (step.type === 'slider') {
          const { answers } = existingParticipation
          const sliderAnswer = findLast(
            answer => answer.id.split('-')[0] === fromStep
          )(answers)
          if (!sliderAnswer.context) {
            moveNext(nextStep.step)
          }
        }
      }
    }
  }

  if (
    nextStep &&
    nextStep.type === 'choose-product' &&
    existingParticipation.selectedProducts.length === availableProducts.length
  ) {
    nextStep =
      allSteps.find(step => step.section === 'end') ||
      allSteps.find(step => step.step === CLIENT_GENERATED_STEPS.FINISHED)
  }
  const addNewAnwser = (stepIndex, answer) => {
    client.mutate({
      variables: {
        input: {
          question: nextStep.step,
          value: answer.values,
          surveyEnrollment: surveyEnrollmentId,
          startedAt: new Date(),
          selectedProduct: answer.selectedProduct || ''
        }
      },
      mutation: gql`
        mutation submitAnswer($input: SubmitAnswerInput!) {
          submitAnswer(input: $input)
        }
      `
    })
    answers = [
      ...answers.slice(0, stepIndex),
      answer,
      ...answers.slice(stepIndex + 1)
    ]
    nextStep = getNextStep({
      steps: allSteps,
      currentStepIndex: stepIndex,
      numberOfLoopsStarted: selectedProducts.length,
      maxNumberOfLoops: survey.maximumProducts || products.length
    })
  }

  while (nextStep.type === 'time-stamp') {
    const stepIndex = getStepIndex(nextStep.step, allSteps)
    // const selectedProduct =
    const answer = {
      __typename: 'LocalAnswer',
      id: getAnswerLocalId(nextStep.step, selectedProducts),
      values: [new Date().getHours()],
      selectedProduct: selectedProduct
    }
    addNewAnwser(stepIndex, answer)
  }

  const checkAuthUser = getAuthenticatedUser()
  if (
    nextStep.type === 'paypal-email' &&
    checkAuthUser &&
    checkAuthUser.paypalEmailAddress !== '' &&
    existingParticipation &&
    existingParticipation.email === checkAuthUser.emailAddress
  ) {
    const stepIndex = getStepIndex(nextStep.step, allSteps)
    const answer = {
      __typename: 'LocalAnswer',
      id: getAnswerLocalId(nextStep.step, selectedProducts),
      values: [checkAuthUser.paypalEmailAddress]
    }
    addNewAnwser(stepIndex, answer)
  }

  const getSelectedProduct = isTastingSection => {
    if (!isTastingSection) {
      return null
    }

    if (isOnlyOneProduct) {
      return survey.products.find(p => p.isAvailable).id
    }

    return existingParticipation.selectedProduct
  }

  const isTastingSection =
    nextStep && nextStep.section && nextStep.section === 'tasting'

  cache.writeQuery({
    query: SURVEY_PARTICIPATION_QUERY,
    data: {
      currentSurveyParticipation: {
        ...existingParticipation,
        answers,
        __typename: 'CurrentSurveyParticipation',
        currentSurveyStep: nextStep.step,
        currentSurveySection: nextStep.section || '',
        isCurrentAnswerValid: false,
        canSkipCurrentQuestion: false,
        currentAnswerSkipTarget: null,
        selectedProduct: getSelectedProduct(isTastingSection)
      }
    }
  })

  return {
    currentSurveyStep: nextStep && nextStep.step,
    currentSurveySection: nextStep && nextStep.section
  }
}

const findSkipRule = (question, answerValue) => {
  if (question.skipFlow) {
    switch (question.skipFlow.type) {
      case 'MATCH_RANGE': {
        const numericValue = parseFloat(answerValue)
        return skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findRule(
            rule =>
              rule.minValue <= numericValue && numericValue <= rule.maxValue
          )
      }
      case 'MATCH_OPTION': {
        const optionIndex = question.options.findIndex(
          op => op.value === answerValue
        )
        return skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findRule(rule => rule.index === optionIndex)
      }
      case 'MATCH_PRODUCT': {
        const productId = answerValue
        return skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findRule(rule => rule.productId === productId)
      }
      case 'MATCH_MULTIPLE_OPTION': {
        return skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findMultipleRule(answerValue)
      }
      case 'MATCH_MATRIX_OPTION': {
        const matrixRule = skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findMatrixRule(answerValue, question)
          if(matrixRule && matrixRule.skipTo && matrixRule.skipTo === 'continue'){
            return false
          }
          return matrixRule
      }
      default:
        break
    }
  }
}
// We might need this again
// export const removeChooseProductSteps = allSteps => {
//   const newAllSteps = allSteps.filter(
//     step => step && step.type !== 'choose-product'
//   )
//   const allChooseProducts = allSteps.filter(
//     step => step && step.type === 'choose-product'
//   )

//   function findNextStepNotChooseProduct (nextStep) {
//     const nextChooseProductStep = allChooseProducts.find(
//       step => step && step.step === nextStep
//     )

//     if (!nextChooseProductStep) {
//       return nextStep
//     }

//     return findNextStepNotChooseProduct(nextChooseProductStep.nextStep)
//   }

//   return newAllSteps.map(step => ({
//     ...step,
//     nextStep: findNextStepNotChooseProduct(step.nextStep)
//   }))
// }

const generateStep = ({ question, section, nextStepOverride }) => {
  return {
    step: question.id,
    section,
    type: question.type,
    nextStep: nextStepOverride || question.nextQuestion
  }
}

const insertStepAtBeginning = ({ step, steps }) => {
  let toInsert = { ...step, nextStep: steps[0] && steps[0].step }

  return [toInsert, ...steps]
}

const insertStep = ({ step, steps, at }) => {
  if (at <= 0) {
    return insertStepAtBeginning({ step, steps })
  }

  let stepBefore = steps[at - 1]
  let toInsert = { ...step, nextStep: stepBefore.nextStep }
  stepBefore = { ...stepBefore, nextStep: toInsert.step }

  return [...steps.slice(0, at - 1), stepBefore, toInsert, ...steps.slice(at)]
}

const getSortedSteps = ({
  screeningQuestions = [],
  setupQuestions = [],
  productsQuestions = [],
  finishingQuestions = [],
  paymentQuestions = [],
  forcedAccount = false,
  forcedAccountLocation = 'start'
}) => {
  let sortedScreening = screeningQuestions
  let sortedPayments = paymentQuestions

  if (paymentQuestions.length > 0) {
    sortedPayments = paymentQuestions.map(question =>
      generateStep({ question: question, section: 'payments' })
    )
  }

  if (screeningQuestions.length > 0) {
    sortedScreening = screeningQuestions.map(question =>
      generateStep({ question: question, section: 'screening' })
    )

    if (forcedAccount && forcedAccountLocation === 'end') {
      sortedScreening = insertStep({
        steps: sortedScreening,
        step: generateStep({
          question: { id: CLIENT_GENERATED_STEPS.FORCED_LOGIN }
        }),
        at: sortedScreening.length
      })
    }

    sortedScreening = insertStep({
      steps: sortedScreening,
      step: generateStep({
        question: { id: CLIENT_GENERATED_STEPS.SCREENING_COMPLETED }
      }),
      at: sortedScreening.length
    })
  }

  let allSteps = [
    ...sortedPayments,
    ...sortedScreening,
    ...setupQuestions.map(question =>
      generateStep({ question: question, section: 'question' })
    ),
    ...productsQuestions.map(question =>
      generateStep({ question: question, section: 'tasting' })
    ),
    ...finishingQuestions.map(question =>
      generateStep({ question: question, section: 'question' })
    )
  ]

  allSteps = insertStep({
    steps: allSteps,
    step: generateStep({
      question: { id: CLIENT_GENERATED_STEPS.INSTRUCTIONS }
    }),
    at: 0
  })

  allSteps = insertStep({
    steps: allSteps,
    step: generateStep({ question: { id: CLIENT_GENERATED_STEPS.FINISHED } }),
    at: allSteps.length
  })
  allSteps = insertStep({
    steps: allSteps,
    step: generateStep({ question: { id: CLIENT_GENERATED_STEPS.REJECTED } }),
    at: allSteps.length
  })

  return allSteps
}

const getStepIndex = (step, steps) =>
  steps.findIndex(stepData => stepData.step === step)

const getStepAfterLoop = steps =>
  steps.find(
    ({ section }, index) =>
      index > 0 &&
      section !== 'tasting' &&
      steps[index - 1] &&
      steps[index - 1].section === 'tasting'
  )

const getNextStep = ({
  steps,
  currentStepIndex,
  numberOfLoopsStarted,
  maxNumberOfLoops,
  availableProductsNumber,
  answers = []
}) => {
  if (currentStepIndex < 0) {
    return steps[0]
  }
  const currentStep = steps[currentStepIndex] || {}
  const nextLinearStepIndex = getStepIndex(currentStep.nextStep, steps)
  const nextLinearStep = steps[nextLinearStepIndex]

  const isFinishingLoopIteration =
    currentStep &&
    currentStep.section === 'tasting' &&
    nextLinearStep &&
    nextLinearStep.section !== 'tasting'

  const hasDoneEveryLoop = numberOfLoopsStarted >= maxNumberOfLoops

  if (
    isFinishingLoopIteration &&
    !hasDoneEveryLoop
    // availableProductsNumber > numberOfLoopsStarted  ---- NO IDEA WHAT THIS IS
  ) {
    const firstStepOfLoop = steps.filter(data => data.section === 'tasting')[0]
    return firstStepOfLoop
  }

  if (
    currentStep.type === 'choose-payment' &&
    nextLinearStep.type === 'paypal-email'
  ) {
    const nextQuestion = getStepIndex(nextLinearStep.nextStep, steps)
    const checkAnswer = answers.find(answer => {
      if (numberOfLoopsStarted > 0) {
        return (
          answer.id === `${currentStep.step}-${numberOfLoopsStarted}` &&
          answer.values.includes('11')
        )
      } else {
        return (
          answer.id === `${currentStep.step}` && answer.values.includes('11')
        )
      }
    })
    if (checkAnswer) {
      return steps[nextQuestion]
    }
  }
  return nextLinearStep
}

export const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyInfo
    }
  }

  ${surveyInfo}
`

export const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      ...questionInfo
    }
  }
  ${questionInfo}
`
