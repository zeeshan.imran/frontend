// import resolver, { removeChooseProductSteps } from './index'

describe('advanceInSurveyResolver', () => {
  test('TODO: update this test', () => {})
  //   jest.mock('graphql-tag', () => () => 'gql query')
  //   let mockedWrite = () => {}
  //   const mockedQuestions = [
  //     {
  //       id: 'abc',
  //       nextQuestion: 'def'
  //     },
  //     {
  //       id: 'def'
  //     }
  //   ]
  //   const surveyQuestionsWithPaddedLoop = {
  //     setupQuestions: [
  //       {
  //         id: 'before',
  //         nextQuestion: mockedQuestions[0].id
  //       }
  //     ],
  //     productsQuestions: [
  //       mockedQuestions[0],
  //       { ...mockedQuestions[1], nextQuestion: 'after' }
  //     ],
  //     finishingQuestions: [
  //       {
  //         id: 'after'
  //       }
  //     ]
  //   }

  //   const surveyQuestionsWithPreLoop = {
  //     setupQuestions: [
  //       {
  //         id: 'before',
  //         nextQuestion: mockedQuestions[0].id
  //       }
  //     ],
  //     productsQuestions: mockedQuestions,
  //     finishingQuestions: []
  //   }

  //   beforeEach(() => {
  //     mockedWrite = jest.fn()
  //   })

  //   test('should skip the choose-product question (first step) and change the next question', () => {
  //     const allSteps = removeChooseProductSteps([
  //       {
  //         step: 'step-1',
  //         type: 'choose-product',
  //         nextStep: 'step-2'
  //       },
  //       {
  //         step: 'step-2',
  //         type: 'choose-one',
  //         nextStep: 'step-3'
  //       },
  //       {
  //         step: 'step-3',
  //         type: 'dropdown',
  //         nextStep: 'finish'
  //       },
  //       {
  //         step: 'finish'
  //       }
  //     ])

  //     expect(allSteps).toHaveLength(3)
  //     expect(allSteps[0]).toMatchObject({
  //       step: 'step-2',
  //       type: 'choose-one',
  //       nextStep: 'step-3'
  //     })
  //   })

  //   test('should skip the choose-product question (one but not first step) and change the next question', () => {
  //     const allSteps = removeChooseProductSteps([
  //       {
  //         step: 'step-1',
  //         type: 'choose-one',
  //         nextStep: 'step-2'
  //       },
  //       {
  //         step: 'step-2',
  //         type: 'choose-product',
  //         nextStep: 'step-3'
  //       },
  //       {
  //         step: 'step-3',
  //         type: 'dropdown',
  //         nextStep: 'finish'
  //       },
  //       {
  //         step: 'finish'
  //       }
  //     ])

  //     expect(allSteps).toHaveLength(3)
  //     expect(allSteps[0]).toMatchObject({
  //       step: 'step-1',
  //       type: 'choose-one',
  //       nextStep: 'step-3'
  //     })
  //   })

  //   test('should skip the choose-product question (two steps, no one is the first step) and change the next question', () => {
  //     const allSteps = removeChooseProductSteps([
  //       {
  //         step: 'step-1',
  //         type: 'choose-one',
  //         nextStep: 'step-2'
  //       },
  //       {
  //         step: 'step-2',
  //         type: 'choose-product',
  //         nextStep: 'step-3'
  //       },
  //       {
  //         step: 'step-3',
  //         type: 'choose-product',
  //         nextStep: 'step-4'
  //       },
  //       {
  //         step: 'step-4',
  //         type: 'dropdown',
  //         nextStep: 'finish'
  //       },
  //       {
  //         step: 'finish'
  //       }
  //     ])

  //     expect(allSteps).toHaveLength(3)
  //     expect(allSteps[0]).toMatchObject({
  //       step: 'step-1',
  //       type: 'choose-one',
  //       nextStep: 'step-4'
  //     })
  //   })

  //   // NOTE: should update this tests
  //   test('should get next immediate step in same section', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: [],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         setupQuestions: mockedQuestions,
  //         productsQuestions: [],
  //         finishingQuestions: [],
  //         products: []
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[0].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual(mockedQuestions[1].id)
  //     expect(response.currentSurveySection).toEqual('question')
  //   })

  //   test('should load instructions as first step', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: [],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         setupQuestions: mockedQuestions,
  //         productsQuestions: [],
  //         finishingQuestions: [],
  //         products: []
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: null },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual('instructions')
  //     expect(response.currentSurveySection).not.toBeDefined()
  //   })

  //   test('should load "finished" step after last question', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: [],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         setupQuestions: mockedQuestions,
  //         productsQuestions: [],
  //         finishingQuestions: [],
  //         products: []
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[1].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual('finished')
  //     expect(response.currentSurveySection).not.toBeDefined()
  //   })

  //   test('should load "screening-completed" step after answering screening questions', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: [],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: mockedQuestions,
  //         productsQuestions: [],
  //         setupQuestions: [],
  //         finishingQuestions: [],
  //         products: []
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[1].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual('screening-completed')
  //     expect(response.currentSurveySection).not.toBeDefined()
  //   })

  //   test('should skip "screening-completed" if there are no screening questions', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: [],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         setupQuestions: mockedQuestions,
  //         productsQuestions: [],
  //         finishingQuestions: [],
  //         products: []
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: 'instructions' },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual(mockedQuestions[0].id)
  //     expect(response.currentSurveySection).toEqual('question')
  //   })

  //   test("should not repeat the loop if the survey doesn't support multiple tastings", async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: ['productA'],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         maximumProducts: 1,
  //         screeningQuestions: [],
  //         ...surveyQuestionsWithPaddedLoop,
  //         products: ['productA', 'productB']
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[1].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual('after')
  //     expect(response.currentSurveySection).toEqual('question')
  //   })

  //   test('should load the first tasting question after completing one tasting loop', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: ['productA'],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         ...surveyQuestionsWithPaddedLoop,
  //         products: ['productA', 'productB']
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[1].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual(mockedQuestions[0].id)
  //     expect(response.currentSurveySection).toEqual('tasting')
  //   })

  //   test('should get out of the tasting loop if all products are tasted', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: ['productA', 'productB'],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         ...surveyQuestionsWithPaddedLoop,
  //         products: ['productA', 'productB']
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[1].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual('after')
  //     expect(response.currentSurveySection).toEqual('question')
  //   })

  //   test('should get out of the tasting loop to the finishing page if all products are tasted and there are no more products', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: ['productA', 'productB'],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         ...surveyQuestionsWithPreLoop,
  //         products: ['productA', 'productB']
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[1].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual('finished')
  //     expect(response.currentSurveySection).not.toBeDefined()
  //   })

  //   test('if passed skipLoop it should go to step after loop', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: ['productA', 'productB'],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         ...surveyQuestionsWithPaddedLoop,
  //         products: ['productA', 'productB']
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[1].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual('after')
  //     expect(response.currentSurveySection).toEqual('question')
  //   })

  //   test('if passed skipLoop it should go to finished step if there is no other step after loop', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: ['productA', 'productB'],
  //         surveyId: 'abc'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         ...surveyQuestionsWithPreLoop,
  //         finishingQuestions: [],
  //         products: ['productA', 'productB']
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: mockedQuestions[1].id },
  //       { cache: mockedCache }
  //     )

  //     expect(response.currentSurveyStep).toEqual('finished')
  //     expect(response.currentSurveySection).not.toBeDefined()
  //   })
  // })

  // describe('advanceInSurveyResolver: skip choose product if the survey has only one question', () => {
  //   const mockedQuestions = [
  //     {
  //       id: 'question-1',
  //       type: 'choose-product',
  //       nextQuestion: 'question-2'
  //     },
  //     {
  //       id: 'question-2',
  //       type: 'dropdown'
  //     }
  //   ]
  //   let mockedWrite
  //   beforeEach(() => {
  //     mockedWrite = jest.fn()
  //   })
  //   test('if passed skipLoop it should go to finished step if there is no other step after loop', async () => {
  //     const getMockedData = () => ({
  //       currentSurveyParticipation: {
  //         selectedProducts: [],
  //         selectedProduct: null,
  //         surveyId: 'survey-1'
  //       },
  //       survey: {
  //         screeningQuestions: [],
  //         setupQuestions: [],
  //         finishingQuestions: [],
  //         productsQuestions: mockedQuestions,
  //         products: [{ id: 'product-1' }]
  //       }
  //     })

  //     const mockedCache = {
  //       readQuery: getMockedData,
  //       writeQuery: mockedWrite
  //     }

  //     const response = await resolver(
  //       'a',
  //       { fromStep: 'instructions' },
  //       { cache: mockedCache }
  //     )

  //     // should jump from instructions to question-2 because question-1 is choose-product
  //     expect(response.currentSurveyStep).toEqual('question-2')
  //   })
})
