import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import surveyCreationDefaults from '../../defaults/surveyCreation'

export default (_, __, { cache }) => {
  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      ...surveyCreationDefaults
    }
  })
}
