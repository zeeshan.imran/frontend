import resetCreationForm from './index'
import surveyCreationDefaults from '../../defaults/surveyCreation'

describe('resetCreationForm', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query', () => {
    mockCache.readQuery.mockReturnValueOnce({
      ...surveyCreationDefaults
    })

    resetCreationForm({}, {}, { cache: mockCache })
    expect(mockCache.writeQuery).toHaveBeenCalledTimes(1)
  })
})
