import requirePaypalEmail from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const mockSurveyCreation = {
  surveyId: 'survey-1',
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-2',
      type: 'paypal-email',
      prompt: 'Paypal Email'
    },
    {
      id: 'question-3',
      type: 'name',
      prompt: 'What is your name?'
    }
  ],
  mandatoryQuestions: ['question-2', 'question-3'],
  uniqueQuestionsToCreate: []
}

const mockSurveyCreationNotPaypalEmail= {
  surveyId: 'survey-1',
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-3',
      type: 'name',
      prompt: 'What is your name?'
    }
  ],
  mandatoryQuestions: ['question-1'],
  uniqueQuestionsToCreate: []
}

describe('requirePaypalEmail', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query for requirePaypalEmail', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })

    requirePaypalEmail({}, {}, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })

  test('should call SURVEY_CREATION query for requirePaypalEmail choose-product question null', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreationNotPaypalEmail
    })

    requirePaypalEmail({}, {}, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
