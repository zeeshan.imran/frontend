import { remove, uniq, clone } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import defaultQuestions from '../../defaults/questions'

export default (_, __, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })

  const {
    questions,
    mandatoryQuestions,
    uniqueQuestionsToCreate
  } = surveyCreation
  

  const existingMandatoryQuestionIndex = mandatoryQuestions.findIndex(
    q => q.type === 'paypal-email'
  )

  if(existingMandatoryQuestionIndex > -1){
    return 
  }


  const existingQuestionIndex = questions.findIndex(
    q => q.type === 'paypal-email'
  )
  
  let updatedMandatoryQuestions = [...mandatoryQuestions]
  let updatedQuestions = [...questions]
  if (existingQuestionIndex > -1) {
    updatedMandatoryQuestions = [
      ...updatedMandatoryQuestions,
      questions[existingQuestionIndex]
    ]
    updatedQuestions = remove(existingQuestionIndex, 1, updatedQuestions)
  } else {
    updatedMandatoryQuestions = [
      ...updatedMandatoryQuestions,
      clone(defaultQuestions['paypal-email'])
    ]
  }

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        __typename: 'SurveyCreation',
        mandatoryQuestions: updatedMandatoryQuestions,
        questions: updatedQuestions,
        uniqueQuestionsToCreate: uniq([
          ...uniqueQuestionsToCreate,
          'paypal-email'
        ])
      }
    }
  })

  return null
}
