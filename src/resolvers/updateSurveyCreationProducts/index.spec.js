import updateSurveyCreationProducts from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

describe('updateSurveyCreationProducts', () => {
  const mockSurveyCreation = {
    surveyId: 'survey-1',
    basics: {
      name: 'Survey 1',
      minimumProducts: 1,
      maximumProducts: 1
    },

    questions: [
      {
        id: 'question-3',
        type: 'choose-product',
        prompt: 'Please choose product here',
        chooseProductOptions: {
          maximumProducts: 2
        }
      }
    ],
    products: ['product-1', 'product-2'],
    mandatoryQuestions: ['question-3']
  }

  const mockSurveyCreationProduct = {
    surveyId: 'survey-1',
    basics: {
      name: 'Survey 1',
      minimumProducts: 1,
      maximumProducts: 1
    },

    questions: [
      {
        id: 'question-3',
        type: 'choose-product',
        prompt: 'Please choose product here',
        chooseProductOptions: {
          maximumProducts: 3
        },
        skipFlow: {
          rules: [{ value: ['product-1'] }]
        }
      }
    ],
    products: ['product-1', 'product-2'],
    mandatoryQuestions: ['question-3']
  }

  const mockSurveyCreationWithSkip = {
    surveyId: 'survey-1',
    basics: {
      name: 'Survey 1',
      minimumProducts: 1,
      maximumProducts: 1
    },

    questions: [
      {
        id: 'question-3',
        type: 'choose-product',
        prompt: 'Please choose product here',
        chooseProductOptions: {
          maximumProducts: 1
        },
        skipFlow: {
          rules: [{ value: ['product-1'] }]
        }
      }
    ],
    products: ['product-1', 'product-2'],
    mandatoryQuestions: ['question-3']
  }
  const mockSurveyCreationWithNotSkip = {
    surveyId: 'survey-1',
    basics: {
      name: 'Survey 1',
      minimumProducts: 1,
      maximumProducts: 1
    },

    questions: [
      {
        id: 'question-3',
        type: 'choose-product',
        prompt: 'Please choose product here',
        chooseProductOptions: {
          maximumProducts: 1
        }
      }
    ],
    products: ['product-1', 'product-2'],
    mandatoryQuestions: ['question-3']
  }
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query for maximumProducts and product length are same', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })

    const products = [
      {
        id: 'product-1',
        name: 'product',
        brand: 'brand'
      }
    ]
    updateSurveyCreationProducts({}, { products }, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })

  test('should call SURVEY_CREATION query for maximumProducts greater than product length', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreationProduct
    })

    const products = [
      {
        id: 'product-1',
        name: 'product',
        brand: 'brand'
      }
    ]
    updateSurveyCreationProducts({}, { products }, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })

  test('should call SURVEY_CREATION query for check skipFlowUtils question', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreationWithSkip
    })

    const products = [
      {
        id: 'product-1',
        name: 'product',
        brand: 'brand'
      }
    ]
    updateSurveyCreationProducts({}, { products }, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })

  test('should call SURVEY_CREATION return Question', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreationWithNotSkip
    })

    const products = [
      {
        id: 'product-1',
        name: 'product',
        brand: 'brand'
      }
    ]
    updateSurveyCreationProducts({}, { products }, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
