import advanceInSurvey from './advanceInSurvey'
import loadSurveyBasicsToForm from './loadSurveyBasicsToForm'
import loadSurveyProductsToForm from './loadSurveyProductsToForm'
import loadSurveyQuestionsToForm from './loadSurveyQuestionsToForm'
import saveCurrentSurveyParticipation from './saveCurrentSurveyParticipation'
import resetCache from './resetCache'
import resetCreationForm from './resetCreationForm'
import requireChooseProduct from './requireChooseProduct'
import unrequireChooseProduct from './unrequireChooseProduct'
import requirePaypalEmail from './requirePaypalEmail'
import unrequirePaypalEmail from './unrequirePaypalEmail'
import updateSurveyCreationProducts from './updateSurveyCreationProducts'
import updateSurveyCreationBasics from './updateSurveyCreationBasics'
import updateSurveyCreationQuestions from './updateSurveyCreationQuestions'
import updateSurveyCreationQuestion from './updateSurveyCreationQuestion'
import updateSurveyCreationQuestionType from './updateSurveyCreationQuestionType'
import removeSurveyCreationQuestion from './removeSurveyCreationQuestion'
import reloadSurveyProductOnForm from './reloadSurveyProductOnForm'
import reloadSurveyQuestionOnForm from './reloadSurveyQuestionOnForm'
import renewSurveyQuestionOnForm from './renewSurveyQuestionOnForm'
import setSurveyEditMode from './setSurveyEditMode'
import requirePaymentQuestion from './requirePaymentQuestion'
import unrequirePaymentQuestion from './unrequirePaymentQuestion'

export default {
  Mutation: {
    resetCache,
    advanceInSurvey,
    requireChooseProduct,
    unrequireChooseProduct,
    requirePaypalEmail,
    unrequirePaypalEmail,
    saveCurrentSurveyParticipation,
    updateSurveyCreationProducts,
    updateSurveyCreationBasics,
    updateSurveyCreationQuestions,
    updateSurveyCreationQuestion,
    updateSurveyCreationQuestionType,
    removeSurveyCreationQuestion,
    resetCreationForm,
    loadSurveyBasicsToForm,
    loadSurveyProductsToForm,
    loadSurveyQuestionsToForm,
    reloadSurveyProductOnForm,
    reloadSurveyQuestionOnForm,
    renewSurveyQuestionOnForm,
    setSurveyEditMode,
    requirePaymentQuestion,
    unrequirePaymentQuestion
  }
}
