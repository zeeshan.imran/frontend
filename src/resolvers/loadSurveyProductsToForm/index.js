import { omit } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

export default (_, { products = [], brands = [] }, { cache }) => {
  const brandsById = brands.reduce(
    (mapped, brand) => ({ ...mapped, [brand.id]: brand }),
    {}
  )

  const { surveyCreation: cachedData = { products: [] } } = cache.readQuery({
    query: SURVEY_CREATION
  })

  const restOfCachedData = omit(['products'], cachedData)

  const productsWithBrandNames = products.map(product => ({
    ...product,
    brand: brandsById[product.brand] && brandsById[product.brand].name
  }))

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        __typename: 'SurveyCreation',
        products: productsWithBrandNames,
        ...restOfCachedData
      }
    }
  })
}
