import { remove, uniq, clone } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import defaultQuestions from '../../defaults/questions'

export default (_, __, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })

  const {
    questions,
    mandatoryQuestions,
    uniqueQuestionsToCreate
  } = surveyCreation
  

  const existingMandatoryQuestionIndex = mandatoryQuestions.findIndex(
    q => (q.type === 'choose-payment')
  )
  
  if(existingMandatoryQuestionIndex > -1){
    return 
  }


  const existingQuestionIndex = questions.findIndex(
    q => (q.type === 'choose-payment')
  )
  
  let updatedMandatoryQuestions = [...mandatoryQuestions]
  let updatedQuestions = [...questions]
  if (existingQuestionIndex > -1) {
    updatedMandatoryQuestions = [
      ...updatedMandatoryQuestions,
      questions[existingQuestionIndex]
    ]
    updatedQuestions = remove(existingQuestionIndex, 1, updatedQuestions)
  } else {
    updatedMandatoryQuestions = [
      clone(defaultQuestions['choose-payment']),
      ...updatedMandatoryQuestions
    ]
  }


  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        __typename: 'SurveyCreation',
        mandatoryQuestions: updatedMandatoryQuestions,
        questions: updatedQuestions,
        uniqueQuestionsToCreate: uniq([
          ...uniqueQuestionsToCreate,
          'choose-payment'
        ])
      }
    }
  })

  return null
}
