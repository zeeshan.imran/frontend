import { getNextQuestion } from './index'

describe('should modify required field while changing question type', () => {
  test('should set required to false while changing question type to info', () => {
    expect(
      getNextQuestion({ required: true, type: 'choose-one' }, 'info')
    ).toMatchObject({ required: false })

    expect(
      getNextQuestion({ required: false, type: 'choose-one' }, 'info')
    ).toMatchObject({ required: false })
  })

  test('should reset required to true while changing question type non-info to non-info', () => {
    expect(
      getNextQuestion({ required: true, type: 'choose-one' }, 'dropdown')
    ).toMatchObject({ required: true })

    expect(
      getNextQuestion({ required: false, type: 'choose-one' }, 'dropdown')
    ).toMatchObject({ required: true })
  })

  test('should reset required to true while changing question type info to non-info', () => {
    expect(
      getNextQuestion({ required: true, type: 'info' }, 'dropdown')
    ).toMatchObject({ required: true })

    expect(
      getNextQuestion({ required: false, type: 'info' }, 'dropdown')
    ).toMatchObject({ required: true })
  })
})
