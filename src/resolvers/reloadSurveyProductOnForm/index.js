/** @typedef {import("apollo-cache").ApolloCache} ApolloCache */
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const buildReloadProduct = ({
  clientGeneratedId,
  updatedProduct: { id }
}) => product => {
  if (clientGeneratedId && product.clientGeneratedId === clientGeneratedId) {
    return {
      ...product,
      id,
      clientGeneratedId: undefined
    }
  }
  return product
}

/**
 * When receiving product from GraphQL Server
 * - on choose-product question: replace skipFlow.rules[*].value by product's id
 * - on product: delete clientGeneratedId, update product's id
 *
 * @param {*} _
 * @param {*} param1
 * @param {{ cache: ApolloCache }} param2
 */
const reloadSurveyProductOnForm = (
  _,
  { clientGeneratedId, product: updatedProduct },
  { cache }
) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })
  const { products, mandatoryQuestions, ...restOfSurvey } = surveyCreation

  const reloadProduct = buildReloadProduct({
    clientGeneratedId,
    updatedProduct
  })

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...restOfSurvey,
        products: products.map(reloadProduct),
        mandatoryQuestions: mandatoryQuestions
      }
    }
  })
}

export default reloadSurveyProductOnForm
