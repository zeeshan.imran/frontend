import loadSurveyBasicsToForm from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { defaultSurvey } from '../../mocks'

const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    basicsDefaults: 'profile',
    name: 'Survey 1'
  }
}

describe('loadSurveyBasicsToForm', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query for loadSurveyBasicsToForm', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })

    loadSurveyBasicsToForm(
      {},
      {
        survey: {
          linkedSurveys: [],
          settings: {
            recaptcha: {
              name: 'recaptcha'
            }
          },
          products: [
            {
              id: 'product-1',
              name: 'product one'
            }
          ],
          exclusiveTasters: [
            {
              emailAddress: 'test@flowor.com'
            }
          ]
        }
      },
      { cache: mockCache }
    )
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
