import loadSurveyQuestionsToForm from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const mockSurveyCreation = {
  surveyId: 'survey-1',
  products: ['product-1', 'product-2'],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-2',
      type: 'choose-product',
      prompt: 'Product namem'
    },
    {
      id: 'question-3',
      type: 'profile',
      prompt: 'What is your name?'
    },
    {
      id: 'question-1',
      type: 'paired-questions',
      prompt: 'Select two flawor type.',
      pairs: ['paired-1', 'paired-2']
    }
  ],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}

describe('loadSurveyQuestionsToForm', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call loadSurveyQuestionsToForm for paired-questions', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })
    const mockQuestionPaired = [
      {
        id: 'question-1',
        type: 'paired-questions',
        prompt: 'Select two flawor type.',
        pairs: ['paired-1', 'paired-2'],
        displayOn: 'screening'
      },
      {
        id: 'question-2',
        type: 'profile',
        prompt: 'What is your name?',
        relatedQuestions: ['question-1'],
        displayOn: 'screening'
      }
    ]
    loadSurveyQuestionsToForm(
      {},
      { questions: mockQuestionPaired },
      { cache: mockCache }
    )
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })

  test('should call loadSurveyQuestionsToForm for sliders', () => {
    const mockQuestionSlider = [
      {
        id: 'question-1',
        type: 'slider',
        prompt: 'Question text',
        secondaryPrompt: 'Additional question text',
        min: 'Min value',
        step: 'Step value',
        max: 'Max value',
        addMarks: 'Please add slider marks below',
        addSliders: 'Please add Sliders below',
        addSlider: 'Add slider',
        showProfile: 'Show profile charts after this step',
        profileStepTitle: 'Profile step title',
        alert: {
          deleteSlider: 'Are you sure you want to delete this slider?',
          description: 'This action can not be reversed.',
          okText: 'Delete',
          deleteMark: 'Are you sure you want to delete this mark?'
        },
        marks: {
          isMajor: 'Major unit',
          add: 'Add mark'
        }
      },
      {
        id: 'question-2',
        type: 'profile',
        prompt: 'What is your name?',
        relatedQuestions: ['question-1']
      }
    ]

    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })

    loadSurveyQuestionsToForm(
      {},
      { questions: mockQuestionSlider },
      { cache: mockCache }
    )
    //expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
