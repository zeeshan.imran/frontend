import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import currentSurveyParticipationDefault from '../../defaults/currentSurveyParticipation'

export default async (_, __, { cache }) => {
  const {
    currentSurveyParticipation: { surveyId }
  } = await cache.readQuery({ query: SURVEY_PARTICIPATION_QUERY })

  const resetCurrentSurveyParticipation = {
    ...currentSurveyParticipationDefault.currentSurveyParticipation,
    surveyId
  }

  await cache.writeQuery({
    query: SURVEY_PARTICIPATION_QUERY,
    data: {
      currentSurveyParticipation: resetCurrentSurveyParticipation
    }
  })

  return null
}
