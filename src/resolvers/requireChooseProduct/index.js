import { remove, uniq, clone } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import defaultQuestions from '../../defaults/questions'

export default (_, __, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })

  const {
    questions,
    mandatoryQuestions,
    uniqueQuestionsToCreate
  } = surveyCreation

  const existingQuestionIndex = questions.findIndex(
    q => q.type === 'choose-product'
  )
  
  const existingQuestionMandatoryIndex = mandatoryQuestions.findIndex(
    q => q.type === 'choose-product'
  )

  if(existingQuestionIndex > -1 || existingQuestionMandatoryIndex > -1){
    return 
  }

  let updatedMandatoryQuestions = [...mandatoryQuestions]
  let updatedQuestions = [...questions]
  if (existingQuestionIndex > -1) {
    updatedMandatoryQuestions = [
      ...updatedMandatoryQuestions,
      questions[existingQuestionIndex]
    ]
    updatedQuestions = remove(existingQuestionIndex, 1, updatedQuestions)
  } else {
    updatedMandatoryQuestions = [
      ...updatedMandatoryQuestions,
      clone(defaultQuestions['choose-product'])
    ]
  }

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        __typename: 'SurveyCreation',
        mandatoryQuestions: updatedMandatoryQuestions,
        questions: updatedQuestions,
        uniqueQuestionsToCreate: uniq([
          ...uniqueQuestionsToCreate,
          'choose-product'
        ])
      }
    }
  })

  return null
}
