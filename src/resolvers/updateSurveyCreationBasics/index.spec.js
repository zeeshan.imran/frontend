import updateSurveyCreationBasics from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

describe('updateSurveyCreationBasics', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: {
        basics: {
          customButtons: {
            continue: 'Continue',
            start: 'Start',
            next: 'Next',
            skip: 'Skip'
          }
        }
      }
    })
    const basicsconst = {
      basics: {
        customButtons: {
          continue: 'Continue',
          start: 'Start',
          next: 'Next',
          skip: 'Skip'
        }
      }
    }
    updateSurveyCreationBasics({}, basicsconst, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
