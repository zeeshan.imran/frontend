import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const reduceOnAuthorizationTypeChange = authorizationType => {
  if (authorizationType === 'public') {
    return {
      enabledEmailTypes: [],
      emails: {
        surveyWaiting: null,
        surveyCompleted: null,
        surveyRejected: null,
        __typename: 'SurveyCreationEmails'
      }
    }
  }

  return {}
}
 
export default (_, { basics }, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        __typename: 'SurveyCreation',
        basics: {
          __typename: 'BasicInfo',
          ...surveyCreation.basics,
          ...basics,
          customButtons: {
            __typename: 'CustomButtons',
            ...surveyCreation.basics.customButtons,
            ...basics.customButtons
          },
          ...reduceOnAuthorizationTypeChange(basics.authorizationType)
        }
      }
    }
  })

  return null
}
