// import preval from 'preval.macro'
import gql from 'graphql-tag'
import { ApolloClient } from 'apollo-client'
import {
  InMemoryCache,
  IntrospectionFragmentMatcher
} from 'apollo-cache-inmemory'
import { createUploadLink } from 'apollo-upload-client'
import { setContext } from 'apollo-link-context'
// import { CachePersistor } from 'apollo-cache-persist'
import defaults from '../defaults'
import resolvers from '../resolvers'
import {
  getAuthenticationToken,
  getImpersonateUserId
} from '../utils/userAuthentication'
import introspectionQueryResultData from './fragmentTypes.json'
import logger from '../utils/logger'
import { onError } from 'apollo-link-error'

// const isInDev =
//   process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test'

// const SCHEMA_VERSION = isInDev ? 'DEV' : preval`module.exports = Date.now()`
// const SCHEMA_VERSION_KEY = 'apollo-schema-version'

export default async usePersistentStorage => {
  const typeDefs = gql`
    union FormValue = String | Array
  `

  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData
  })

  const cache = new InMemoryCache({
    fragmentMatcher
  })

  // const persistor = new CachePersistor({
  //   key: process.env.REACT_APP_FLAVORWIKI_APOLLO_CACHE,
  //   cache,
  //   storage: usePersistentStorage ? window.localStorage : window.sessionStorage,
  //   debug: isInDev
  // })
  // persistor.pause()

  const uploadLink = createUploadLink({
    uri: `${process.env.REACT_APP_FLAVORWIKI_API_URL}/graphql`
  })

  const authLink = setContext((_, { headers: originalHeaders }) => {
    const token = getAuthenticationToken()
    const impersonateUserId = getImpersonateUserId()

    const headers = {
      ...originalHeaders,
      Authorization: token ? `Bearer ${token}` : ''
    }

    if (impersonateUserId) {
      headers.Impersonate = impersonateUserId
    }

    return {
      headers
    }
  })

  const errorLink = onError(({ _graphQLErrors, networkError }) => {
    if (networkError) {
      logger.log(networkError)
    }
  })

  const client = new ApolloClient({
    typeDefs,
    link: errorLink.concat(authLink.concat(uploadLink)),
    cache,
    resolvers
  })

  cache.writeData({
    data: {
      ...defaults
    }
  })

  // const currentSchemaVersion = window.localStorage.getItem(SCHEMA_VERSION_KEY)

  // if (currentSchemaVersion === SCHEMA_VERSION) {
  //   try {
  //     await persistor.restore()
  //   } catch (error) {
  //     await persistor.purge()
  //     window.localStorage.setItem(SCHEMA_VERSION_KEY, SCHEMA_VERSION)
  //   }
  // } else {
  //   await persistor.purge()
  //   window.localStorage.setItem(SCHEMA_VERSION_KEY, SCHEMA_VERSION)
  // }

  // await persistor.purge()
  // persistor.resume()
  // client.persistor = persistor

  return client
}
