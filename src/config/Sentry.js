import * as Sentry from '@sentry/browser'

const dsn = process.env.REACT_APP_SENTRY_URI

dsn && Sentry.init({ dsn })
