import gql from 'graphql-tag'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

// this function allow quick redirect qr-code/[organization-uniqueName]/[qrcode-uniqueName] without load application
const redirectQrCode = async () => {
  var match = /\/qr-code\/([^/]+)\/([^/]+)/g.exec(window.location.pathname)
  if (match) {
    const client = new ApolloClient({
      link: new HttpLink({
        uri: `${process.env.REACT_APP_FLAVORWIKI_API_URL}/graphql`
      }),
      cache: new InMemoryCache()
    })

    const organization = match[1]
    const qrCode = match[2]

    try {
      const {
        data: { targetLink }
      } = await client.query({
        query: QUERY_QR_CODE,
        fetchPolicy: 'network-only',
        variables: {
          organization,
          qrCode
        }
      })

      window.location = targetLink.url
      return true
    } catch (e) {
      //
    }

    client.stop()
    return false
  }
}

const QUERY_QR_CODE = gql`
  query getTargetLink($organization: String!, $qrCode: String!) {
    targetLink(organization: $organization, qrCode: $qrCode) {
      id
      type
      url
    }
  }
`

export default redirectQrCode
