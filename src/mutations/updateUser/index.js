import gql from 'graphql-tag'
import { currentUserInfo } from '../../fragments/currentUser'

export const UPDATE_USER_MUTATION = gql`
  mutation updateUser(
    $id: ID
    $languageId: String
    $email: String
    $firstname: String
    $lastname: String
    $password: String
    $oldPassword: String
  ) {
    updateUser(
      id: $id
      languageId: $languageId
      email: $email
      firstname: $firstname
      lastname: $lastname
      password: $password
      oldPassword: $oldPassword
    ) {
      ok
      error
      user {
        ...currentUserInfo
      }
    }
  }
  ${currentUserInfo}
`
