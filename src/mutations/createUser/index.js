import gql from 'graphql-tag'
import { userInfo } from '../../fragments/user'

export const CREATE_USER_MUTATION = gql`
  mutation createUser(
    $languageId: String
    $email: String!
    $firstname: String!
    $lastname: String!
    $password: String
    $recaptchaResponseToken: String!
  ) {
    createUser(
      languageId: $languageId
      email: $email
      firstname: $firstname
      lastname: $lastname
      password: $password
      recaptchaResponseToken: $recaptchaResponseToken
    ) {
      ok
      user {
        ...userInfo
      }
      error
    }
  }
  ${userInfo}
`
