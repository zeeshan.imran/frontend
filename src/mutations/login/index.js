import gql from 'graphql-tag'
import { loginInfo } from '../../fragments/login'

export const LOGIN_MUTATION = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      ...loginInfo
    }
  }

  ${loginInfo}
`
