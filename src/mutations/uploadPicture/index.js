import gql from 'graphql-tag'

export const UPLOAD_PICTURE_MUTATION = gql`
  mutation uploadPicture($id: ID, $photo: Upload) {
    updateTesterInfo(id: $id, photo: $photo) {
      ok
      error
    }
  }
`
