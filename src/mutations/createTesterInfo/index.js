import gql from 'graphql-tag'
import { testerInfo } from '../../fragments/testerInfo'

export const CREATE_TESTER_INFO_MUTATION = gql`
  mutation createTesterInfo(
    $dietaryPreferenceId: String
    $birthday: String!
    $childrenAges: String
    $ethnicityId: String
    $genderId: String
    $incomeRangeId: String
    $paypalEmail: String
    $photo: Upload
    $smokerKindId: String
    $userId: String!
    $preferredProductCategoriesIds: [String]
  ) {
    createTesterInfo(
      dietaryPreferenceId: $dietaryPreferenceId
      birthday: $birthday
      childrenAges: $childrenAges
      ethnicityId: $ethnicityId
      genderId: $genderId
      incomeRangeId: $incomeRangeId
      paypalEmail: $paypalEmail
      photo: $photo
      smokerKindId: $smokerKindId
      userId: $userId
      preferredProductCategoriesIds: $preferredProductCategoriesIds
    ) {
      ok
      error
      testerInfo {
        ...testerInfo
      }
    }
  }
  ${testerInfo}
`
