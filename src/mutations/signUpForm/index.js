import gql from 'graphql-tag'

export const UPDATE_SIGN_UP_FORM_MUTATION = gql`
  mutation updateSignUpForm($field: String!, $value: FormValue!) {
    updateSignUpForm(field: $field, value: $value) @client
  }
`
