import gql from 'graphql-tag'

export const SET_PASSWORD_WITH_TOKEN_MUTATION = gql`
  mutation setPasswordWithToken(
    $email: String!
    $password: String!
    $token: String!
  ) {
    setPasswordWithToken(email: $email, password: $password, token: $token) {
      ok
    }
  }
`
