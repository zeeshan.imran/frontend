import gql from 'graphql-tag'

export const LOGIN_TO_SURVEY = gql`
  mutation loginToSurvey(
    $email: String
    $survey: String!
    $referral: ID
    $isUserLoggedIn: Boolean,
    $password: String
    $country: String
    $browserInfo: JSON
  ) {
    loginToSurvey(
      email: $email
      survey: $survey
      referral: $referral
      isUserLoggedIn: $isUserLoggedIn
      password: $password
      country: $country
      browserInfo: $browserInfo
    ) {
      id
      lastAnsweredQuestion {
        id
      }
      state
      selectedProducts {
        id
        name
      }
      paypalEmail
      savedRewards
      answers
      lastSelectedProduct
      productDisplayOrder
      productDisplayType
    }
  }
`
