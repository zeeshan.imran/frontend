import gql from 'graphql-tag'

export const UPDATE_CHILDREN_AGES_MUTATION = gql`
  mutation updateChildrenAges($index: String, $value: FormValue!) {
    updateChildrenAges(index: $index, value: $value) @client
  }
`
