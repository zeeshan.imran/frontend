import gql from 'graphql-tag'
import { testerInfo } from '../../fragments/testerInfo'

export const UPDATE_TESTER_INFO_MUTATION = gql`
  mutation updateTesterInfo(
    $id: ID
    $birthday: String
    $childrenAges: String
    $dietaryPreferenceId: String
    $ethnicityId: String
    $genderId: String
    $incomeRangeId: String
    $paypalEmail: String
    $photo: Upload
    $preferredProductCategoriesIds: [String]
    $smokerKindId: String
    $privacyMarketingEmails: Boolean
    $privacyMarketingPushNotifications: Boolean
    $privacyOtherPushNotifications: Boolean
  ) {
    updateTesterInfo(
      id: $id
      birthday: $birthday
      childrenAges: $childrenAges
      dietaryPreferenceId: $dietaryPreferenceId
      ethnicityId: $ethnicityId
      genderId: $genderId
      incomeRangeId: $incomeRangeId
      paypalEmail: $paypalEmail
      photo: $photo
      preferredProductCategoriesIds: $preferredProductCategoriesIds
      smokerKindId: $smokerKindId
      privacyMarketingEmails: $privacyMarketingEmails
      privacyMarketingPushNotifications: $privacyMarketingPushNotifications
      privacyOtherPushNotifications: $privacyOtherPushNotifications
    ) {
      ok
      error
      testerInfo {
        ...testerInfo
      }
    }
  }
  ${testerInfo}
`
