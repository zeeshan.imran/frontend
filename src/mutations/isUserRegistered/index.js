import gql from 'graphql-tag'

export const IS_USER_REGISTERED = gql`
  mutation isUserRegistered($email: String!) {
    isUserRegistered(email: $email)
  }
`
