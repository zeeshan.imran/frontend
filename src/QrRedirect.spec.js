import { ApolloClient } from 'apollo-client'
import redirectQrCode from './QrRedirect'

jest.mock('apollo-client')
jest.mock('apollo-link-http')

describe('Qr Redirect', () => {
  let setLocation
  let pushState

  beforeEach(() => {
    setLocation = jest.fn()
    pushState = jest.fn()

    const _location = window.location

    Object.assign(window.history, {
      pushState
    })

    Object.defineProperty(window, 'location', {
      get: () => _location,
      set: setLocation
    })

    Object.defineProperty(window.location, 'pathname', {
      writable: true
    })
  })

  test("should call window.location = newUrl if the Qr Code's type is a custom link", async () => {
    window.location.pathname = '/qr-code/org-1/qr-code-1'

    ApolloClient.mockImplementation(() => ({
      query: () => ({
        data: {
          targetLink: {
            id: 'qr-code-1',
            type: 'link',
            url: 'https://flavor-wiki.slack.com'
          }
        }
      }),
      stop: () => {}
    }))

    await redirectQrCode()

    expect(setLocation).toHaveBeenCalledWith('https://flavor-wiki.slack.com')
  })

  test("should call window.location = surveyUrl if the Qr Code's type is a survey link", async () => {
    window.location.pathname = '/qr-code/org-1/qr-code-2'

    ApolloClient.mockImplementation(() => ({
      query: () => ({
        data: {
          targetLink: {
            id: 'qr-code-2',
            type: 'survey',
            url: '/survey/survey-1'
          }
        }
      }),
      stop: () => {}
    }))

    await redirectQrCode()

    expect(setLocation).toHaveBeenCalledWith('/survey/survey-1')
  })
})
