const formatChooseProductOptions = options => {
  if (options === undefined) {
    return undefined
  }

  if (!options) {
    return null
  }

  return {
    minimumProducts: parseInt(options.minimumProducts, 10),
    maximumProducts: parseInt(options.maximumProducts, 10)
  }
}

export default formatChooseProductOptions
