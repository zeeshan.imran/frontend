let colors = {}
if (process.env.REACT_APP_THEME) {
  colors = require(`./${process.env.REACT_APP_THEME}Colors`)
} else {
  colors = require('./defaultColors')
}

module.exports = colors
