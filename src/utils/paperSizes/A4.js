import { getPPI } from './getPPI'

export const A4 = {
  name: 'A4',
  printZoom: 1.5,
  width: 210 / 25.4 * getPPI(),
  height: 297 / 25.4 * getPPI(),
  cols: 20,
  rows: 28,
  colsBetweenPages: 3
}
