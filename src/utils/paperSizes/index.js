import { A4 } from './A4'
import { Letter } from './Letter'
export const getColWidth = (paperSize) => paperSize.width / paperSize.cols

export const getRowHeight = (paperSize) =>
  paperSize.height / paperSize.rows

export const getBoundRect = (paperSize) => {
  const { cols, rows } = paperSize
  return {
    left: 1,
    right: cols - 1,
    top: 1,
    bottom: rows - 1
  }
}

export const getBottomOfItems = items => {
  let bottom = 0

  for (let item of items) {
    if (bottom < item.y + item.h) {
      bottom = item.y + item.h
    }
  }

  return bottom
}

export const getGridSize = (paperSize, zoom) => {
  const colWidth = Math.round(getColWidth(paperSize) * zoom)
  const rowHeight = getRowHeight(paperSize) * zoom
  return {
    colWidth,
    rowHeight,
    zoom
  }
}

const PAPER_SIZES = {
  A4,
  Letter
}

export { PAPER_SIZES }
