import { getPPI } from './getPPI'

export const Letter = {
  name: 'Letter',
  printZoom: 1.5,
  width: 8.5 * getPPI(),
  height: 11 * getPPI(),
  cols: 20,
  rows: 28,
  colsBetweenPages: 3
}
