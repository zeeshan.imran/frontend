export const getNavEntries = (t, firstAction, logoutAction) => {
  return [
    {
      title: t('profileLink'),
      action: () => {
        window.location = firstAction
      }
    },
    {
      title: t('signOut'),
      action: logoutAction
    }
  ]
}
