import Resizer from 'react-image-file-resizer'

const ImageResize = async (file, height, width, imageType, quality) => {
  return new Promise((resolve, reject) => {
    Resizer.imageFileResizer(
      file,
      height, // max width of image set
      width, // max height of iamge set
      imageType, // image type
      quality, // quality of image set 
      0, // rotation of image set
      uri => {
        uri ? resolve(uri) : reject(null)
      },
      'base64'
    )
  })
}

export default ImageResize