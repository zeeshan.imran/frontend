export const formatQuestionSettings = settings =>
  (settings && {
    ...settings,
    minAnswerValues: parseInt(settings.minAnswerValues, 10),
    maxAnswerValues: parseInt(settings.maxAnswerValues, 10)
  }) ||
  undefined
