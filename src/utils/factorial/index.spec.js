import { factorial } from './index.js'

describe('factorial', () => {
  test('factorial to be 0', () => {
    expect(factorial(0)).toBe(1)
  })

  test('factorial tobe false', () => {
    expect(factorial(10)).toBe(3628800)
  })
})
