const mockLocalStorage = () => {
  let storage = {}
  window.localStorage = {
    getItem: jest.fn(key => storage[key] || null),
    setItem: jest.fn((...args) => {
      if (args.length < 2) throw Error('2 argument required')
      const [key, value] = args
      storage[key] = value
    }),
    removeItem: jest.fn((...args) => {
      if (args.length < 1) throw Error('1 argument required')
      const [key] = args
      delete storage[key]
    }),
    clear: jest.fn(() => {
      storage = {}
    })
  }
}

export default mockLocalStorage
