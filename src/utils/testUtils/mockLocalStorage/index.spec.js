import React from 'react'
import { mount } from 'enzyme'
import mockLocalStorage from '.'

window.localStorage = localStorage

describe('mockLocalStorage', () => {
  let testRender
  let client

  beforeEach(() => localStorage.clear())

  afterEach(() => {
    localStorage.clear()
  })

  test("returns undefined if requested item doesn't exist", () => {
    const foo = localStorage.getItem('foo')
    expect(foo).toBe(null)
    mockLocalStorage()
  })
  test('gets the value of an item', () => {
    localStorage.setItem('foo', 'bar')
    const foo = localStorage.getItem('foo')
    expect(foo).toBe('bar')
    mockLocalStorage()
  })

  test('removes an item', () => {
    localStorage.setItem('foo', 'bar')
    localStorage.removeItem('foo')
    const foo = localStorage.getItem('foo')
    expect(foo).toBe(null)
    mockLocalStorage()
  })

  test("should throw Error with message '2 argument required' when one param were passed", () => {
    try {
      localStorage.setItem('foo')
    } catch (e) {
      expect(e.message).toBe('2 argument required')
    }
    mockLocalStorage()
  })
  test("should throw Error with message '1 argument required' when no params were passed", () => {
    try {
      localStorage.removeItem()
    } catch (e) {
      expect(e.message).toBe('1 argument required')
    }

    mockLocalStorage()
  })
})
