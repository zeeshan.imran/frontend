import { message as AntMessage } from 'antd'
import { MESSAGE_TIMEOUT } from '../Constants'

export const displayMessage = message => {
  AntMessage.destroy()
  AntMessage.info(message, MESSAGE_TIMEOUT)
}
