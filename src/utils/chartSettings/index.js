import { settings } from './settings'
import { clone } from 'ramda'

export const getAvailableChartSettings = ({
  chartType,
  questionType,
  hasProducts
}) => {
  const chartSettings = []

  // Settings for all charts
  chartSettings.push(settings.isTotalRespondentsShown, settings.howManyDecimals)

  // Append middle section of settings. Very variative depending on chart / question / survey section
  switch (chartType) {
    case 'polar':
      chartSettings.push(settings.divider, settings.howManyDecimals_axis)
      break
    case 'pie':
      chartSettings.push(settings.divider, settings.valueType)
      break
    case 'line':
      chartSettings.push(settings.divider, settings.valueType, {
        ...settings.isCountProLabelShown,
        default: true
      })
      break
    case 'bar':
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.sortOrder
      )
      break
    case 'columns-mean':
    case 'horizontal-bars-mean':
      chartSettings.push(settings.divider, settings.isCountProLabelShown)
      if (hasProducts) {
        chartSettings.push(settings.sortOrder)
      }
      break
    case 'column':
      if (!['paired-questions', 'slider'].includes(questionType)) {
        chartSettings.push(settings.divider, settings.valueType)
        if (hasProducts) chartSettings.push(settings.percentageGrouping)
      }
      chartSettings.push(settings.divider, settings.isCountProLabelShown)
      if (
        hasProducts &&
        !['paired-questions', 'slider'].includes(questionType)
      ) {
        chartSettings.push(settings.isCountProGroupShown)
      }
      if (hasProducts) {
        chartSettings.push(settings.isProductLabelsSwapped)
      }
      break
    case 'stacked-bar':
    case 'stacked-column':
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.isStackedLabelsReversed
      )
      break
    case 'stacked-column-horizontal-bars':
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.isStackedLabelsReversed,
        settings.isMeanProLabelsShown,
        settings.sortOrder
      )
      break
    case 'all':
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.isCountProGroupShown,
        settings.isMeanProLabelsShown,
        settings.isProductLabelsSwapped,
        settings.isStackedLabelsReversed,
        settings.sortOrder
      )
      break
    default:
      break
  }

  // Append data table settings
  // In proposal: extract data table in separate type of chart
  if (!['table', 'spearmann-table', 'pearson-table', 'tags-list'].includes(chartType)) {
    chartSettings.push(settings.divider, settings.isDataTableShown)
  }
  switch (chartType) {
    case 'column':
    case 'stacked-column':
    case 'stacked-bar':
    case 'stacked-column-horizontal-bars':
      if (
        !['paired-questions', 'slider'].includes(questionType) ||
        (questionType === 'slider' && chartType === 'stacked-bar')
      ) {
        const dataFormatOptions = clone(
          settings.isDataTableShown_format.options
        )
        if (hasProducts) {
          dataFormatOptions.push(
            {
              name: 'settings.charts.optionLabels.dataformat.perc_abs_teh',
              value: 'perc_abs_teh'
            },
            {
              name: 'settings.charts.optionLabels.dataformat.perc_abs_prod',
              value: 'perc_abs_prod'
            }
          )
        }
        chartSettings.push({
          ...settings.isDataTableShown_format,
          options: dataFormatOptions
        })
      }
      break
    case 'map':
    case 'line':
    case 'pie':
    case 'all':
      chartSettings.push(settings.isDataTableShown_format)
      break
    default:
      break
  }

  // Append statistics table settings
  // In proposal: extract statistics table in separate type of chart
  if (
    [
      'stacked-column',
      'stacked-column-horizontal-bars',
      'stacked-bar',
      'column',
      'bar',
      'pie',
      'all'
    ].includes(chartType) &&
    questionType !== 'choose-multiple'
  ) {
    chartSettings.push(
      settings.divider,
      settings.isStatisticsTableShown,
      settings.statisticsTable_howManyDecimals,
      settings.isStatisticsTableMeanShown,
      settings.isStatisticsTableStdevShown
    )
  }

  // Outline settings
  if (
    [
      'stacked-column',
      'stacked-column-horizontal-bars',
      'stacked-bar',
      'horizontal-bars-mean',
      'column',
      'columns-mean',
      'bar',
      'all'
    ].includes(chartType)
  ) {
    chartSettings.push(
      settings.divider,
      settings.hasOutline,
      settings.hasOutline_width,
      settings.hasOutline_color
    )
  }

  // Grouping settings
  if (
    ([
      'stacked-column-horizontal-bars',
      'column',
      'stacked-column',
      'stacked-bar',
      'pie'
    ].includes(chartType) &&
      !['paired-questions', 'slider'].includes(questionType)) ||
    (chartType === 'stacked-bar' && questionType === 'slider')
  ) {
    chartSettings.push(settings.divider, settings.groupLabels)
  }

  // Color pickers
  switch (chartType) {
    case 'pie':
    case 'polar':
    case 'stacked-column':
    case 'stacked-bar':
    case 'horizontal-bars-mean':
    case 'column':
    case 'columns-mean':
    case 'bar':
      chartSettings.push(settings.divider, settings.colorsArr)
      break
    case 'map':
      chartSettings.push(settings.divider, settings.colorsArr_map)
      break
    case 'line':
      chartSettings.push(settings.divider, settings.singleColor)
      break
    case 'stacked-column-horizontal-bars':
      chartSettings.push(
        settings.divider,
        settings.colorsArr,
        settings.singleColor
      )
      break
    case 'all':
      chartSettings.push(
        settings.divider,
        settings.colorsArr,
        settings.colorsArr_map,
        settings.singleColor
      )
      break
    default:
      break
  }

  return chartSettings
}

export const getDefaultGlobalChartSettings = () => {
  const globalSettings = {}

  Object.keys(settings).forEach(settingName => {
    const settingObj = settings[settingName]
    if (settingObj.type === 'divider') {
      return
    }
    if (['groupLabels'].includes(settingObj.name)) {
      return
    }
    globalSettings[settingName] = settingObj.default || false
  })

  return globalSettings
}

export const getDefaultChartSettings = stats => {
  const defaultSettings = {}

  stats.tabs.forEach(tab => {
    tab.charts.forEach(chart => {
      const availableSettings = getAvailableChartSettings({
        chartType: chart.chart_type,
        questionType: chart.question_type,
        hasProducts: chart.question_with_products
      })

      const chartDefaultSettings = {}

      availableSettings.forEach(setting => {
        chartDefaultSettings[setting.name] =
          typeof setting.default === 'function'
            ? setting.default(chart)
            : setting.default || false
      })

      defaultSettings[
        `${chart.question}-${chart.chart_type}`
      ] = chartDefaultSettings
    })
  })

  defaultSettings['global'] = getDefaultGlobalChartSettings()

  return defaultSettings
}
