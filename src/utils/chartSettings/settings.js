import { defaultChartSettings } from '../../defaults/chartSettings'

export const settings = {
  divider: {
    type: 'divider'
  },
  isTotalRespondentsShown: {
    type: 'switch',
    name: 'isTotalRespondentsShown',
    label: 'settings.charts.optionLabels.isTotalRespondentsShown'
  },
  howManyDecimals: {
    type: 'dropdown',
    options: [
      { name: '0', value: 0 },
      { name: '1', value: 1 },
      { name: '2', value: 2 },
      { name: '3', value: 3 },
      { name: '4', value: 4 },
      { name: '5', value: 5 }
    ],
    name: 'howManyDecimals',
    label: 'settings.charts.optionLabels.howManyDecimals',
    placeholder: 'placeholders.chartsSettings.howManyDecimals',
    default: 2
  },
  howManyDecimals_axis: {
    type: 'dropdown',
    options: [
      { name: '0', value: 0 },
      { name: '1', value: 1 },
      { name: '2', value: 2 },
      { name: '3', value: 3 },
      { name: '4', value: 4 },
      { name: '5', value: 5 }
    ],
    name: 'howManyDecimals_axis',
    label: 'settings.charts.optionLabels.howManyDecimals_axis',
    placeholder: 'placeholders.chartsSettings.howManyDecimals',
    default: 1
  },
  valueType: {
    type: 'dropdown',
    options: [
      { name: 'Percentage', value: 'percentage' },
      { name: 'Absolute values', value: 'value' }
    ],
    name: 'valueType',
    label: 'settings.charts.optionLabels.valueType',
    placeholder: 'placeholders.chartsSettings.valueType',
    default: 'value'
  },
  isCountProLabelShown: {
    type: 'switch',
    name: 'isCountProLabelShown',
    label: 'settings.charts.optionLabels.isCountProLabelShown'
  },
  percentageGrouping: {
    type: 'dropdown',
    options: [
      {
        name: 'settings.charts.optionLabels.percentageGrouping.inner',
        value: 'inner_group'
      },
      {
        name: 'settings.charts.optionLabels.percentageGrouping.outer',
        value: 'outer_group'
      }
    ],
    name: 'percentageGrouping',
    label: 'settings.charts.optionLabels.percentageGroupingLabel',
    default: 'inner_group',
    showIf: settings => settings.valueType === 'percentage'
  },
  isCountProGroupShown: {
    type: 'switch',
    name: 'isCountProGroupShown',
    label: 'settings.charts.optionLabels.isCountProGroupShown'
  },
  isProductLabelsSwapped: {
    type: 'switch',
    name: 'isProductLabelsSwapped',
    label: 'settings.charts.optionLabels.isProductLabelsSwapped'
  },
  isStackedLabelsReversed: {
    type: 'switch',
    name: 'isStackedLabelsReversed',
    label: 'settings.charts.optionLabels.isStackedLabelsReversed'
  },
  isMeanProLabelsShown: {
    type: 'switch',
    name: 'isMeanProLabelsShown',
    label: 'settings.charts.optionLabels.isMeanProLabelsShown'
  },
  sortOrder: {
    type: 'dropdown',
    options: [
      {
        name: 'settings.charts.optionLabels.sortOrder.valueAsc',
        value: 'value.asc'
      },
      {
        name: 'settings.charts.optionLabels.sortOrder.valueDesc',
        value: 'value.desc'
      },
      {
        name: 'settings.charts.optionLabels.sortOrder.nameAsc',
        value: 'label.asc'
      },
      {
        name: 'settings.charts.optionLabels.sortOrder.nameDesc',
        value: 'label.desc'
      }
    ],
    name: 'sortOrder',
    default: 'label.asc',
    label: 'settings.charts.optionLabels.barsSortOrder',
    placeholder: 'placeholders.chartsSettings.barsSortOrder'
  },
  isDataTableShown: {
    type: 'switch',
    name: 'isDataTableShown',
    label: 'settings.charts.optionLabels.isDataTableShown'
  },
  isDataTableShown_format: {
    type: 'dropdown',
    options: [
      { name: 'settings.charts.optionLabels.dataformat.abs', value: 'abs' },
      { name: 'settings.charts.optionLabels.dataformat.perc', value: 'perc' },
      {
        name: 'settings.charts.optionLabels.dataformat.perc_abs',
        value: 'perc_abs'
      }
    ],
    name: 'isDataTableShown_format',
    label: 'settings.charts.optionLabels.isDataTableShown_format',
    placeholder: 'placeholders.chartsSettings.isDataTableShown_format',
    default: 'abs',
    showIf: settings => settings.isDataTableShown
  },
  isStatisticsTableShown: {
    type: 'switch',
    name: 'isStatisticsTableShown',
    label: 'settings.charts.optionLabels.isStatisticsTableShown'
  },
  statisticsTable_howManyDecimals: {
    type: 'dropdown',
    options: [
      { name: '0', value: 0 },
      { name: '1', value: 1 },
      { name: '2', value: 2 },
      { name: '3', value: 3 },
      { name: '4', value: 4 },
      { name: '5', value: 5 }
    ],
    name: 'statisticsTable_howManyDecimals',
    label: 'settings.charts.optionLabels.howManyDecimals',
    placeholder: 'placeholders.chartsSettings.howManyDecimals',
    default: 2,
    showIf: settings => settings.isStatisticsTableShown
  },
  isStatisticsTableMeanShown: {
    type: 'switch',
    name: 'isStatisticsTableMeanShown',
    label: 'charts.table.mean',
    showIf: settings => settings.isStatisticsTableShown,
    default: true
  },
  isStatisticsTableStdevShown: {
    type: 'switch',
    name: 'isStatisticsTableStdevShown',
    label: 'charts.table.stdev',
    showIf: settings => settings.isStatisticsTableShown
  },
  hasOutline: {
    type: 'switch',
    name: 'hasOutline',
    label: 'settings.charts.optionLabels.hasOutline'
  },
  hasOutline_width: {
    type: 'dropdown',
    options: [
      { name: '1', value: 1 },
      { name: '2', value: 2 },
      { name: '3', value: 3 },
      { name: '4', value: 4 }
    ],
    name: 'hasOutline_width',
    label: 'settings.charts.optionLabels.outlineWidth',
    placeholder: 'placeholders.chartsSettings.outlineWidth',
    showIf: settings => settings.hasOutline
  },
  hasOutline_color: {
    type: 'color-picker',
    name: 'hasOutline_color',
    label: 'settings.charts.optionLabels.outlineColor',
    default: [defaultChartSettings.outlineColor],
    showIf: settings => settings.hasOutline,
    colorLimit: 1
  },
  groupLabels: {
    type: 'group-labels',
    name: 'groupLabels',
    label: 'settings.charts.optionLabels.groupLabels',
    default: chart => chart.labels.map(label => [label])
  },
  colorsArr: {
    type: 'color-picker',
    name: 'colorsArr',
    label: 'settings.charts.optionLabels.colorPicker',
    default: [...defaultChartSettings.colorsArr]
  },
  colorsArr_map: {
    type: 'color-picker',
    name: 'colorsArr_map',
    label: 'settings.charts.optionLabels.colorPicker',
    default: [...defaultChartSettings.mapChartColorsArr]
  },
  singleColor: {
    type: 'color-picker',
    name: 'singleColor',
    label: 'settings.charts.optionLabels.colorPicker',
    default: [defaultChartSettings.colorsArr[0]],
    colorLimit: 1
  }
}
