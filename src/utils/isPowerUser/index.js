const isPowerUser = user => user && user.type === 'power-user'

export default isPowerUser
