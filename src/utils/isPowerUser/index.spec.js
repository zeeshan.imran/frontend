import isPowerUser from '.'

describe('isPowerUser', () => {
  test('should render isPowerUser', () => {
    const user = {
      organization: { id: 1 },
      emailAddress: 'test@flowor.com',
      type: 'power-user'
    }
    expect(isPowerUser(user)).toBe(true)
  })
})
