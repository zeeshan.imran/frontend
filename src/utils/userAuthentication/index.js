import merge from 'lodash.merge'
import { AUTHENTICATION_REDIRECT } from '../Constants'
import history from '../../history'

export const FLAVORWIKI_AUTH_TOKEN = 'flavorwiki-operator-token'

export const FLAVORWIKI_AUTH_STATUS = 'flavorwiki-isAuthenticated'

export const FLAVORWIKI_AUTH_USER_ORG = 'flavorwiki-authenticated-org'

export const FLAVORWIKI_USER_DETAILS = 'flavorwiki-user-details'

export const FLAVORWIKI_IMPERSONATE = 'flavorwiki-impersonate'

export const FLAVORWIKI_UI_STATE = 'flavorwiki-ui-state'

export const FLAVORWIKI_APOLLO_CACHE = 'flavorwiki-apollo-cache'

export const setAuthenticationToken = token =>
  window.localStorage.setItem(FLAVORWIKI_AUTH_TOKEN, token)

export const setAuthenticatedOrganization = organization =>
  window.localStorage.setItem(FLAVORWIKI_AUTH_USER_ORG, organization)

export const setAuthenticationStatus = status =>
  window.localStorage.setItem(FLAVORWIKI_AUTH_STATUS, status)

export const setAuthenticatedUser = user =>
  window.localStorage.setItem(FLAVORWIKI_USER_DETAILS, JSON.stringify(user))

export const setImpersonateUserId = userId =>
  window.localStorage.setItem(FLAVORWIKI_IMPERSONATE, userId)

export const getAuthenticationToken = () =>
  window.localStorage.getItem(FLAVORWIKI_AUTH_TOKEN)

export const getImpersonateUserId = () =>
  window.localStorage.getItem(FLAVORWIKI_IMPERSONATE)

export const getAuthenticatedOrganization = () =>
  window.localStorage.getItem(FLAVORWIKI_AUTH_USER_ORG)

export const getAuthenticatedUser = () =>
  JSON.parse(window.localStorage.getItem(FLAVORWIKI_USER_DETAILS))

export const getUIState = () => {
  const uiState = window.localStorage.getItem(FLAVORWIKI_UI_STATE)
  return uiState ? JSON.parse(uiState) : {}
}

export const updateUIState = updateState => {
  const newState = merge(getUIState(), updateState)
  window.localStorage.setItem(FLAVORWIKI_UI_STATE, JSON.stringify(newState))
}

export const isUserAuthenticated = () =>
  !!window.localStorage.getItem(FLAVORWIKI_AUTH_TOKEN)

export const isUserAuthenticatedAsTaster = () => {
  const user = getAuthenticatedUser()
  return (
    isUserAuthenticated() && user && (user.type === 'taster' || user.isTaster)
  )
}

export const isUserAuthenticatedAsOperator = () => {
  const user = getAuthenticatedUser()
  return isUserAuthenticated() && user && user.type === 'operator'
}

export const isUserAuthenticatedAsAnalytics = () => {
  const user = getAuthenticatedUser()
  return isUserAuthenticated() && user && user.type === 'analytics'
}

export const isUserAuthenticatedAsPowerUser = () => {
  const user = getAuthenticatedUser()
  return isUserAuthenticated() && user && user.type === 'power-user'
}

export const isUserAuthenticatedAsSuperAdmin = () => {
  const user = getAuthenticatedUser()
  return isUserAuthenticated() && user && !!user.isSuperAdmin
}

export const isUserInitialized = () => {
  const user = getAuthenticatedUser()
  return user && user.initialized
}

const deleteAuthenticationToken = () =>
  window.localStorage.removeItem(FLAVORWIKI_AUTH_TOKEN)

const deleteAuthenticationStatus = () =>
  window.localStorage.removeItem(FLAVORWIKI_AUTH_STATUS)

const deleteAuthenticatedOrganization = () =>
  window.localStorage.removeItem(FLAVORWIKI_AUTH_USER_ORG)

export const deleteImpersonateUserId = () =>
  window.localStorage.removeItem(FLAVORWIKI_IMPERSONATE)

const deleteUser = () => window.localStorage.removeItem(FLAVORWIKI_USER_DETAILS)
const deleteUIState = () => window.localStorage.removeItem(FLAVORWIKI_UI_STATE)
const deleteApolloCache = () => window.localStorage.removeItem(FLAVORWIKI_APOLLO_CACHE)

export const logout = () => {
  deleteAuthenticationStatus()
  deleteAuthenticationToken()
  deleteAuthenticatedOrganization()
  deleteUser()
  deleteImpersonateUserId()
  deleteUIState()
  deleteApolloCache()
  return history.push(`${AUTHENTICATION_REDIRECT}/login`)
}
