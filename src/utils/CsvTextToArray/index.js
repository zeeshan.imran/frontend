export default data => {
	const lines = data.split("\n")
	const result = []
	const headers = lines[0].split(",")
	for (let i = 1; i < lines.length; i++) {        
		if (!lines[i])
				continue
		const obj = {}
		const currentline = lines[i].split(",")

		for (let j = 0; j < headers.length; j++) {
				obj[headers[j].replace(/"/g,"")] = currentline[j].replace(/"/g,""); //replace to remove "" from start and end because everyvalue read from csv comes like ""value""
		}
		result.push(obj)
	}
	return result
}