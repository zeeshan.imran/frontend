import { pipe } from 'ramda'
import { getBoundRect, getBottomOfItems } from '../paperSizes'

const getDefaultChartSize = item => {
  switch (item.type) {
    case 'banner':
      return [18, 2]
    case 'chart-pie':
      return [9, 9]
    case 'chart-map':
      return [18, 11]
    case 'chart-polar':
      return [18, 12]
    default:
      return [18, 9]
  }
}

const addTableSize = item => ([w, h]) => {
  if (item.settings && item.settings.isDataTableShown) {
    return [w, h + 4]
  }
  return [w, h]
}

const getDefaultSize = item =>
  pipe(
    getDefaultChartSize,
    addTableSize(item)
  )(item)

const calculateElementsLayout = (paperSize, gridItems) => {
  let x = 1
  let y = 1
  let page = 0
  const pages = [[]]
  let rect = getBoundRect(paperSize)

  gridItems.forEach(gridItem => {
    const [w, h] = getDefaultSize(gridItem)
    let { left, right, bottom } = rect
    // over horizontal => move to bottom of items
    if (x + w > right) {
      x = left
      y = getBottomOfItems(pages[page])

      // over vertical => next page
      if (y + h > bottom) {
        page = page + 1
        pages[page] = pages[page] || []
        x = rect.left
        y = rect.top
      }
    }
    pages[page].push({
      i: gridItem.i,
      x,
      y,
      w,
      h,
      static: !!gridItem.static
    })
    x = x + w
  })

  return pages
}
export default calculateElementsLayout
