import React, {
  useMemo,
  useState,
  useImperativeHandle,
  forwardRef,
  useEffect
} from 'react'
import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'
import { path } from 'ramda'
import { getSingleChartSettings } from '../../utils/ChartSettingsUtils'
import { getChartComponent } from '../../utils/chartConfigs'
import { getImages } from '../../utils/getImages'
import DataGuard from '../../components/Charts/DataGuard'
import { imagesCollection } from '../../assets/png'
import { Logo, ChartLayoutItem } from '../../components/EditPdfLayout/styles'

const { desktopImage } = getImages(imagesCollection)

const mixed = Yup.mixed()

const CARD_WIDTHS = {
  DEFAULT: 900,
  '': 450,
  pie: 450
}

const getChartId = path(['result', 'question'])

export const GridItem = ({
  settings,
  chart,
  scale,
  cardWidth,
  cardHeight,
  chartWidth
}) => {
  const { el, chartId } = useMemo(() => {
    const el = chart.result
    return {
      el,
      chartId: getChartId(chart)
    }
  }, [chart])

  const { t } = useTranslation()
  const ChartComponent = getChartComponent(el)
  const chartElement = useMemo(
    () => (
      <DataGuard
        cardWidth={`${chartWidth}px`}
        cardMargin='0 0'
        validationSchema={mixed}
        chartSettings={settings}
        chartType={el.chart_type}
        chartId={chartId}
        inputData={{ ...el, chart_id: chartId }}
        showAll
        t={t}
        component={ChartComponent}
      />
    ),
    [t, chartWidth, ChartComponent, el, chartId, settings]
  )

  return (
    <ChartLayoutItem
      scale={scale}
      cardWidth={cardWidth}
      cardHeight={cardHeight}
      chartWidth={chartWidth}
    >
      {chartElement}
    </ChartLayoutItem>
  )
}

const GridItemContent = forwardRef(({ chart, settings }, ref) => {
  const { chartWidth, chartId } = useMemo(() => {
    const el = chart.result
    return {
      chartId: getChartId(chart),
      chartWidth: CARD_WIDTHS[el.chart_type] || CARD_WIDTHS.DEFAULT
    }
  }, [chart])
  const [gridItem, setGridItem] = useState(null)
  const [chartSize, setChartSize] = useState({
    width: chartWidth,
    height: null
  })
  const [scale, setScale] = useState(0)

  const cardWidth = useMemo(() => {
    if (!gridItem) {
      return 0
    }

    return Math.max(chartWidth, Math.round(gridItem.width / scale) || 0)
  }, [chartWidth, scale, gridItem])

  useImperativeHandle(
    ref,
    () => ({
      updateGridItemSize: ({ width, height }) => {
        if (
          !gridItem ||
          gridItem.width !== width ||
          gridItem.height !== height
        ) {
          //console.log('trigger', { width, height })
          setGridItem({
            height,
            width
          })
        }
      },
      getChartSize: () => ({
        scale,
        cardWidth
      })
    }),
    [gridItem, cardWidth, scale]
  )

  useEffect(() => {
    setTimeout(() => {
      // measure the height of card after rendering
      const dom = document.querySelector(`#pdf-${chartId} > div > div`)
      if (dom) {
        const height = dom.offsetHeight
        if (height !== chartSize.height) {
          //console.log(chartId, 'new height', height)
          setChartSize({ ...chartSize, height })
        }
      }
    }, 0)
  })

  useEffect(() => {
    if (gridItem && chartSize.height) {
      const newScale =
        Math.round(
          Math.min(
            gridItem.width / chartSize.width,
            gridItem.height / chartSize.height
          ) * 1e5
        ) / 1e5

      // prettier-ignore
      //console.log(chartId, 'inner:', chartSize, 'outer:', gridItem, '->', newScale)

      if (!isNaN(newScale)) {
        setScale(newScale)
      }
    }
  }, [chartId, chartSize, gridItem])

  if (!gridItem) {
    return null
  }

  return (
    <GridItem
      chart={chart}
      settings={settings}
      scale={scale}
      cardWidth={cardWidth}
      cardHeight={chartSize.height / scale}
      chartWidth={chartWidth}
    />
  )
})

const OVERRIDER_SETTINGS = {
  operator: {},
  taster: {
    isDataTableShown_format: 'abs',
    isDataTableShown: true
  }
}

const createElementItems = (stats, settings, templateName) => {
  return [
    {
      i: 'banner',
      type: 'banner',
      align: 'scale',
      content: (
        <div
          style={{
            transform: `scale(${50 / 445})`,
            transformOrigin: 'top left'
          }}
        >
          <div>
            <Logo src={desktopImage} />
          </div>
        </div>
      ),
      static: true
    },
    ...stats.map(chart => {
      const chartSettings = {
        ...getSingleChartSettings(settings, chart.result),
        ...OVERRIDER_SETTINGS[templateName]
      }
      let ref = null
      return {
        i: getChartId(chart),
        type: `chart-${chart.result.chart_type}`,
        chartType: chart.result.chart_type,
        align: 'fit',
        settings: chartSettings,
        getRef: () => ref,
        content: (
          <GridItemContent
            ref={_ref => (ref = _ref)}
            chart={chart}
            settings={chartSettings}
          />
        )
      }
    })
  ]
}

export default createElementItems
