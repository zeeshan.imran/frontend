import { getPathFromLocation } from './index'

describe('getPathFromLocation', () => {
  test('should render getPathFromLocation', () => {
    expect(getPathFromLocation({})).toBe(undefined)

    expect(
      getPathFromLocation({
        pathname: '/survey/complete'
      })
    ).toBe('complete')
  })
})
