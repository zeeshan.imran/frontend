import { without } from 'ramda'

export const CAROUSEL_AUTOPLAY_INTERVAL = 5000

export const AUTHENTICATION_REDIRECT = '/onboarding'

export const ONBOARD_ROOT_PATH = '/onboarding'

export const MESSAGE_DEFAULT_TIMEOUT = 10

export const MESSAGE_TIMEOUT = 4

export const UNIQUE_QUESTION_TYPES = {
  'choose-product': true,
  'paypal-email': true,
  'choose-payments': true
}

export const HIDE_SHARING_LINK_QUESTION_TYPES = [
  'paypal-email',
  'choose-payment'
]

export const CHART_TYPES = {
  'time-stamp': {
    screening: ['line'],
    begin: ['line'],
    middle: ['line'],
    end: ['line']
  },
  'taster-name': {
    screening: ['tags-list'],
    begin: ['tags-list'],
    middle: [],
    end: ['tags-list']
  },
  'open-answer': {
    screening: ['', 'tags-list'],
    begin: ['', 'tags-list'],
    middle: [],
    end: ['', 'tags-list']
  },
  email: {
    screening: ['', 'tags-list'],
    begin: ['', 'tags-list'],
    middle: [],
    end: ['', 'tags-list']
  },
  'choose-multiple': {
    screening: ['column', 'stacked-column', 'stacked-bar'],
    begin: ['column', 'stacked-column', 'stacked-bar'],
    middle: ['column', 'stacked-column', 'stacked-bar'],
    end: ['column', 'stacked-column', 'stacked-bar']
  },
  'choose-one': {
    screening: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    begin: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    middle: [
      'column',
      'stacked-bar',
      'stacked-column',
      'stacked-column-horizontal-bars',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    end: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ]
  },
  dropdown: {
    screening: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    begin: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    middle: [
      'column',
      'stacked-bar',
      'stacked-column',
      'stacked-column-horizontal-bars',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    end: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ]
  },
  'select-and-justify': {
    screening: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    begin: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    middle: [
      'column',
      'stacked-bar',
      'stacked-column',
      'stacked-column-horizontal-bars',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    end: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ]
  },
  'vertical-rating': {
    screening: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    begin: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    middle: [
      'column',
      'stacked-bar',
      'stacked-column',
      'stacked-column-horizontal-bars',
      'horizontal-bars-mean',
      'columns-mean'
    ],
    end: [
      'column',
      'pie',
      'stacked-column',
      'stacked-bar',
      'horizontal-bars-mean',
      'columns-mean'
    ]
  },
  'paired-questions': {
    screening: ['spider', 'column', 'bar'],
    begin: ['spider', 'column', 'bar'],
    middle: ['spider', 'spearmann-table', 'pearson-table', 'column', 'bar'],
    end: ['spider', 'column', 'bar']
  },
  slider: {
    screening: ['spider', 'column', 'bar'],
    begin: ['spider', 'column', 'bar'],
    middle: ['spider', 'stacked-bar', 'column', 'bar'],
    end: ['spider', 'column', 'bar']
  },
  location: {
    screening: ['map'],
    begin: [],
    middle: [],
    end: []
  },
  numeric: {
    screening: ['table', 'tags-list'],
    begin: ['table', 'tags-list'],
    middle: ['table'],
    end: ['table', 'tags-list']
  }
}

export const CHARTS_WITH_LIKING_QUESTION = ['spearmann-table', 'pearson-table']

export const QUESTION_TYPES = [
  'open-answer',
  'choose-product',
  'email',
  'upload-picture',
  'info',
  'choose-one',
  'choose-multiple',
  'numeric',
  'select-and-justify',
  'dropdown',
  'choose-date',
  'paypal-email',
  'taster-name',
  'time-stamp',
  'choose-payment'
].sort()

export const SCREENING_QUESTION_TYPES = without(
  ['choose-product', 'paypal-email', 'choose-payment'],
  QUESTION_TYPES
)
export const PAYMENT_QUESTION_TYPES = without(
  ['choose-product', 'paypal-email', 'taster-name'],
  QUESTION_TYPES
)
export const BEFORE_QUESTION_TYPES = without(
  ['location', 'choose-product', 'paypal-email', 'choose-payment'],
  QUESTION_TYPES
)
export const LOOP_QUESTION_TYPES = without(
  [
    'location',
    'choose-product',
    'paypal-email',
    'choose-payment',
    'taster-name'
  ],
  QUESTION_TYPES
)
export const AFTER_QUESTION_TYPES = without(
  ['location', 'choose-product', 'paypal-email', 'choose-payment'],
  QUESTION_TYPES
)

export const DEFAULT_N_ELEMENTS_PER_PAGE = 10

export const PUBSUB = {
  VALIDATE_SURVEY_PRODUCTS: 'surveyCreation.validateProducts',
  VALIDATE_SURVEY_QUESTIONS: 'surveyCreation.validateQuestions',
  UPDATE_OPERATOR_DROPDOWN: 'operator.updateOperatorDropdown'
}

export const COUNTRY_PHONE_CODES = [
  { name: 'Afghanistan', dial: '+93', code: 'AF' },
  { name: 'Åland Islands', dial: '+358', code: 'AX' },
  { name: 'Albania', dial: '+355', code: 'AL' },
  { name: 'Algeria', dial: '+213', code: 'DZ' },
  { name: 'American Samoa', dial: '+1684', code: 'AS' },
  { name: 'Andorra', dial: '+376', code: 'AD' },
  { name: 'Angola', dial: '+244', code: 'AO' },
  { name: 'Anguilla', dial: '+1264', code: 'AI' },
  { name: 'Antarctica', dial: '+672', code: 'AQ' },
  { name: 'Antigua and Barbuda', dial: '+1268', code: 'AG' },
  { name: 'Argentina', dial: '+54', code: 'AR' },
  { name: 'Armenia', dial: '+374', code: 'AM' },
  { name: 'Aruba', dial: '+297', code: 'AW' },
  {
    name: 'Australia',
    dial: '+61',
    code: 'AU',
    currency: 'AUD',
    referralAmount: 0
  },
  { name: 'Austria', dial: '+43', code: 'AT' },
  { name: 'Azerbaijan', dial: '+994', code: 'AZ' },
  { name: 'Bahamas', dial: '+1242', code: 'BS' },
  { name: 'Bahrain', dial: '+973', code: 'BH' },
  {
    name: 'Bangladesh',
    dial: '+880',
    code: 'BD',
    currency: 'BDT',
    referralAmount: 0
  },
  { name: 'Barbados', dial: '+1246', code: 'BB' },
  { name: 'Belarus', dial: '+375', code: 'BY' },
  { name: 'Belgium', dial: '+32', code: 'BE' },
  { name: 'Belize', dial: '+501', code: 'BZ' },
  { name: 'Benin', dial: '+229', code: 'BJ' },
  { name: 'Bermuda', dial: '+1441', code: 'BM' },
  { name: 'Bhutan', dial: '+975', code: 'BT' },
  {
    name: 'Bolivia, Plurinational State of bolivia',
    dial: '+591',
    code: 'BO'
  },
  { name: 'Bosnia and Herzegovina', dial: '+387', code: 'BA' },
  { name: 'Botswana', dial: '+267', code: 'BW' },
  { name: 'Bouvet Island', dial: '+47', code: 'BV' },
  {
    name: 'Brazil',
    dial: '+55',
    code: 'BR',
    currency: 'BRL',
    referralAmount: 0
  },
  { name: 'British Indian Ocean Territory', dial: '+246', code: 'IO' },
  { name: 'Brunei Darussalam', dial: '+673', code: 'BN' },
  {
    name: 'Bulgaria',
    dial: '+359',
    code: 'BG',
    currency: 'BGN',
    referralAmount: 0
  },
  { name: 'Burkina Faso', dial: '+226', code: 'BF' },
  { name: 'Burundi', dial: '+257', code: 'BI' },
  { name: 'Cambodia', dial: '+855', code: 'KH' },
  { name: 'Cameroon', dial: '+237', code: 'CM' },
  {
    name: 'Canada',
    dial: '+1',
    code: 'CA',
    currency: 'CAD',
    referralAmount: 0
  },
  { name: 'Cape Verde', dial: '+238', code: 'CV' },
  { name: 'Cayman Islands', dial: '+ 345', code: 'KY' },
  { name: 'Central African Republic', dial: '+236', code: 'CF' },
  { name: 'Chad', dial: '+235', code: 'TD' },
  {
    name: 'Chile',
    dial: '+56',
    code: 'CL',
    currency: 'CLP',
    referralAmount: 0
  },
  {
    name: 'China',
    dial: '+86',
    code: 'CN',
    currency: 'CNY',
    referralAmount: 0
  },
  { name: 'Christmas Island', dial: '+61', code: 'CX' },
  { name: 'Cocos (Keeling) Islands', dial: '+61', code: 'CC' },
  {
    name: 'Colombia',
    dial: '+57',
    code: 'CO',
    currency: 'COP',
    referralAmount: 0
  },
  { name: 'Comoros', dial: '+269', code: 'KM' },
  { name: 'Congo', dial: '+242', code: 'CG' },
  {
    name: 'Congo, The Democratic Republic of the Congo',
    dial: '+243',
    code: 'CD'
  },
  { name: 'Cook Islands', dial: '+682', code: 'CK' },
  { name: 'Costa Rica', dial: '+506', code: 'CR' },
  { name: "Cote d'Ivoire", dial: '+225', code: 'CI' },
  {
    name: 'Croatia',
    dial: '+385',
    code: 'HR',
    currency: 'HRK',
    referralAmount: 0
  },
  { name: 'Cuba', dial: '+53', code: 'CU' },
  { name: 'Cyprus', dial: '+357', code: 'CY' },
  { name: 'Czech Republic', dial: '+420', code: 'CZ' },
  {
    name: 'Denmark',
    dial: '+45',
    code: 'DK',
    currency: 'DKK',
    referralAmount: 0
  },
  { name: 'Djibouti', dial: '+253', code: 'DJ' },
  { name: 'Dominica', dial: '+1767', code: 'DM' },
  { name: 'Dominican Republic', dial: '+1849', code: 'DO' },
  { name: 'Ecuador', dial: '+593', code: 'EC' },
  { name: 'Egypt', dial: '+20', code: 'EG' },
  { name: 'El Salvador', dial: '+503', code: 'SV' },
  { name: 'Equatorial Guinea', dial: '+240', code: 'GQ' },
  { name: 'Eritrea', dial: '+291', code: 'ER' },
  { name: 'Estonia', dial: '+372', code: 'EE' },
  { name: 'Ethiopia', dial: '+251', code: 'ET' },
  { name: 'Falkland Islands (Malvinas)', dial: '+500', code: 'FK' },
  { name: 'Faroe Islands', dial: '+298', code: 'FO' },
  { name: 'Fiji', dial: '+679', code: 'FJ' },
  { name: 'Finland', dial: '+358', code: 'FI' },
  { name: 'France', dial: '+33', code: 'FR' },
  { name: 'French Guiana', dial: '+594', code: 'GF' },
  { name: 'French Polynesia', dial: '+689', code: 'PF' },
  { name: 'French Southern Territories', dial: '+262', code: 'TF' },
  { name: 'Gabon', dial: '+241', code: 'GA' },
  { name: 'Gambia', dial: '+220', code: 'GM' },
  {
    name: 'Georgia',
    dial: '+995',
    code: 'GE',
    currency: 'GEL',
    referralAmount: 0
  },
  {
    name: 'Germany',
    dial: '+49',
    code: 'DE',
    currency: 'EUR',
    referralAmount: 0
  },
  { name: 'Ghana', dial: '+233', code: 'GH' },
  { name: 'Gibraltar', dial: '+350', code: 'GI' },
  { name: 'Greece', dial: '+30', code: 'GR' },
  { name: 'Greenland', dial: '+299', code: 'GL' },
  { name: 'Grenada', dial: '+1473', code: 'GD' },
  { name: 'Guadeloupe', dial: '+590', code: 'GP' },
  { name: 'Guam', dial: '+1671', code: 'GU' },
  { name: 'Guatemala', dial: '+502', code: 'GT' },
  { name: 'Guernsey', dial: '+44', code: 'GG' },
  { name: 'Guinea', dial: '+224', code: 'GN' },
  { name: 'Guinea-Bissau', dial: '+245', code: 'GW' },
  { name: 'Guyana', dial: '+592', code: 'GY' },
  { name: 'Haiti', dial: '+509', code: 'HT' },
  { name: 'Heard Island and Mcdonald Islands', dial: '+0', code: 'HM' },
  { name: 'Holy See (Vatican City State)', dial: '+379', code: 'VA' },
  { name: 'Honduras', dial: '+504', code: 'HN' },
  { name: 'Hong Kong', dial: '+852', code: 'HK' },
  {
    name: 'Hungary',
    dial: '+36',
    code: 'HU',
    currency: 'HUF',
    referralAmount: 0
  },
  { name: 'Iceland', dial: '+354', code: 'IS' },
  {
    name: 'India',
    dial: '+91',
    code: 'IN',
    currency: 'INR',
    referralAmount: 0
  },
  {
    name: 'Indonesia',
    dial: '+62',
    code: 'ID',
    currency: 'IDR',
    referralAmount: 0
  },
  {
    name: 'Iran, Islamic Republic of Persian Gulf',
    dial: '+98',
    code: 'IR'
  },
  { name: 'Iraq', dial: '+964', code: 'IQ' },
  { name: 'Ireland', dial: '+353', code: 'IE' },
  { name: 'Isle of Man', dial: '+44', code: 'IM' },
  {
    name: 'Israel',
    dial: '+972',
    code: 'IL',
    currency: 'ILS',
    referralAmount: 0
  },
  { name: 'Italy', dial: '+39', code: 'IT' },
  { name: 'Jamaica', dial: '+1876', code: 'JM' },
  {
    name: 'Japan',
    dial: '+81',
    code: 'JP',
    currency: 'JPY',
    referralAmount: 0
  },
  { name: 'Jersey', dial: '+44', code: 'JE' },
  { name: 'Jordan', dial: '+962', code: 'JO' },
  { name: 'Kazakhstan', dial: '+7', code: 'KZ' },
  {
    name: 'Kenya',
    dial: '+254',
    code: 'KE',
    currency: 'KES',
    referralAmount: 0
  },
  { name: 'Kiribati', dial: '+686', code: 'KI' },
  {
    name: "Korea, Democratic People's Republic of Korea",
    dial: '+850',
    code: 'KP'
  },
  {
    name: 'Korea, Republic of South Korea',
    dial: '+82',
    code: 'KR',
    currency: 'KRW',
    referralAmount: 0
  },
  { name: 'Kosovo', dial: '+383', code: 'XK' },
  { name: 'Kuwait', dial: '+965', code: 'KW' },
  { name: 'Kyrgyzstan', dial: '+996', code: 'KG' },
  { name: 'Laos', dial: '+856', code: 'LA' },
  { name: 'Latvia', dial: '+371', code: 'LV' },
  { name: 'Lebanon', dial: '+961', code: 'LB' },
  { name: 'Lesotho', dial: '+266', code: 'LS' },
  { name: 'Liberia', dial: '+231', code: 'LR' },
  { name: 'Libyan Arab Jamahiriya', dial: '+218', code: 'LY' },
  { name: 'Liechtenstein', dial: '+423', code: 'LI' },
  { name: 'Lithuania', dial: '+370', code: 'LT' },
  { name: 'Luxembourg', dial: '+352', code: 'LU' },
  { name: 'Macao', dial: '+853', code: 'MO' },
  { name: 'Macedonia', dial: '+389', code: 'MK' },
  { name: 'Madagascar', dial: '+261', code: 'MG' },
  { name: 'Malawi', dial: '+265', code: 'MW' },
  {
    name: 'Malaysia',
    dial: '+60',
    code: 'MY',
    currency: 'MYR',
    referralAmount: 0
  },
  { name: 'Maldives', dial: '+960', code: 'MV' },
  { name: 'Mali', dial: '+223', code: 'ML' },
  { name: 'Malta', dial: '+356', code: 'MT' },
  { name: 'Marshall Islands', dial: '+692', code: 'MH' },
  { name: 'Martinique', dial: '+596', code: 'MQ' },
  { name: 'Mauritania', dial: '+222', code: 'MR' },
  { name: 'Mauritius', dial: '+230', code: 'MU' },
  { name: 'Mayotte', dial: '+262', code: 'YT' },
  {
    name: 'Mexico',
    dial: '+52',
    code: 'MX',
    currency: 'MXN',
    referralAmount: 0
  },
  {
    name: 'Micronesia, Federated States of Micronesia',
    dial: '+691',
    code: 'FM'
  },
  { name: 'Moldova', dial: '+373', code: 'MD' },
  { name: 'Monaco', dial: '+377', code: 'MC' },
  { name: 'Mongolia', dial: '+976', code: 'MN' },
  { name: 'Montenegro', dial: '+382', code: 'ME' },
  { name: 'Montserrat', dial: '+1664', code: 'MS' },
  {
    name: 'Morocco',
    dial: '+212',
    code: 'MA',
    currency: 'MAD',
    referralAmount: 0
  },
  { name: 'Mozambique', dial: '+258', code: 'MZ' },
  { name: 'Myanmar', dial: '+95', code: 'MM' },
  { name: 'Namibia', dial: '+264', code: 'NA' },
  { name: 'Nauru', dial: '+674', code: 'NR' },
  { name: 'Nepal', dial: '+977', code: 'NP' },
  { name: 'Netherlands', dial: '+31', code: 'NL' },
  { name: 'Netherlands Antilles', dial: '+599', code: 'AN' },
  { name: 'New Caledonia', dial: '+687', code: 'NC' },
  {
    name: 'New Zealand',
    dial: '+64',
    code: 'NZ',
    currency: 'NZD',
    referralAmount: 0
  },
  { name: 'Nicaragua', dial: '+505', code: 'NI' },
  { name: 'Niger', dial: '+227', code: 'NE' },
  {
    name: 'Nigeria',
    dial: '+234',
    code: 'NG',
    currency: 'NGN',
    referralAmount: 0
  },
  { name: 'Niue', dial: '+683', code: 'NU' },
  { name: 'Norfolk Island', dial: '+672', code: 'NF' },
  { name: 'Northern Mariana Islands', dial: '+1670', code: 'MP' },
  {
    name: 'Norway',
    dial: '+47',
    code: 'NO',
    currency: 'NOK',
    referralAmount: 0
  },
  { name: 'Oman', dial: '+968', code: 'OM' },
  {
    name: 'Pakistan',
    dial: '+92',
    code: 'PK',
    currency: 'PKR',
    referralAmount: 0
  },
  { name: 'Palau', dial: '+680', code: 'PW' },
  { name: 'Palestinian Territory, Occupied', dial: '+970', code: 'PS' },
  { name: 'Panama', dial: '+507', code: 'PA' },
  { name: 'Papua New Guinea', dial: '+675', code: 'PG' },
  { name: 'Paraguay', dial: '+595', code: 'PY' },
  { name: 'Peru', dial: '+51', code: 'PE', currency: 'PEN', referralAmount: 0 },
  {
    name: 'Philippines',
    dial: '+63',
    code: 'PH',
    currency: 'PHP',
    referralAmount: 0
  },
  { name: 'Pitcairn', dial: '+64', code: 'PN' },
  {
    name: 'Poland',
    dial: '+48',
    code: 'PL',
    currency: 'PLN',
    referralAmount: 0
  },
  { name: 'Portugal', dial: '+351', code: 'PT' },
  { name: 'Puerto Rico', dial: '+1939', code: 'PR' },
  { name: 'Qatar', dial: '+974', code: 'QA' },
  {
    name: 'Romania',
    dial: '+40',
    code: 'RO',
    currency: 'RON',
    referralAmount: 0
  },
  {
    name: 'Russia',
    dial: '+7',
    code: 'RU',
    currency: 'RUB',
    referralAmount: 0
  },
  { name: 'Rwanda', dial: '+250', code: 'RW' },
  { name: 'Reunion', dial: '+262', code: 'RE' },
  { name: 'Saint Barthelemy', dial: '+590', code: 'BL' },
  {
    name: 'Saint Helena, Ascension and Tristan Da Cunha',
    dial: '+290',
    code: 'SH'
  },
  { name: 'Saint Kitts and Nevis', dial: '+1869', code: 'KN' },
  { name: 'Saint Lucia', dial: '+1758', code: 'LC' },
  { name: 'Saint Martin', dial: '+590', code: 'MF' },
  { name: 'Saint Pierre and Miquelon', dial: '+508', code: 'PM' },
  { name: 'Saint Vincent and the Grenadines', dial: '+1784', code: 'VC' },
  { name: 'Samoa', dial: '+685', code: 'WS' },
  { name: 'San Marino', dial: '+378', code: 'SM' },
  { name: 'Sao Tome and Principe', dial: '+239', code: 'ST' },
  { name: 'Saudi Arabia', dial: '+966', code: 'SA' },
  { name: 'Senegal', dial: '+221', code: 'SN' },
  { name: 'Serbia', dial: '+381', code: 'RS' },
  { name: 'Seychelles', dial: '+248', code: 'SC' },
  { name: 'Sierra Leone', dial: '+232', code: 'SL' },
  {
    name: 'Singapore',
    dial: '+65',
    code: 'SG',
    currency: 'SGD',
    referralAmount: 0
  },
  { name: 'Slovakia', dial: '+421', code: 'SK' },
  { name: 'Slovenia', dial: '+386', code: 'SI' },
  { name: 'Solomon Islands', dial: '+677', code: 'SB' },
  { name: 'Somalia', dial: '+252', code: 'SO' },
  {
    name: 'South Africa',
    dial: '+27',
    code: 'ZA',
    currency: 'ZAR',
    referralAmount: 0
  },
  { name: 'South Sudan', dial: '+211', code: 'SS' },
  {
    name: 'South Georgia and the South Sandwich Islands',
    dial: '+500',
    code: 'GS'
  },
  { name: 'Spain', dial: '+34', code: 'ES' },
  {
    name: 'Sri Lanka',
    dial: '+94',
    code: 'LK',
    currency: 'LKR',
    referralAmount: 0
  },
  { name: 'Sudan', dial: '+249', code: 'SD' },
  { name: 'Suriname', dial: '+597', code: 'SR' },
  { name: 'Svalbard and Jan Mayen', dial: '+47', code: 'SJ' },
  { name: 'Swaziland', dial: '+268', code: 'SZ' },
  {
    name: 'Sweden',
    dial: '+46',
    code: 'SE',
    currency: 'SEK',
    referralAmount: 0
  },
  {
    name: 'Switzerland',
    dial: '+41',
    code: 'CH',
    currency: 'CHF',
    referralAmount: 0
  },
  { name: 'Syrian Arab Republic', dial: '+963', code: 'SY' },
  { name: 'Taiwan', dial: '+886', code: 'TW' },
  { name: 'Tajikistan', dial: '+992', code: 'TJ' },
  {
    name: 'Tanzania, United Republic of Tanzania',
    dial: '+255',
    code: 'TZ'
  },
  {
    name: 'Thailand',
    dial: '+66',
    code: 'TH',
    currency: 'THB',
    referralAmount: 0
  },
  { name: 'Timor-Leste', dial: '+670', code: 'TL' },
  { name: 'Togo', dial: '+228', code: 'TG' },
  { name: 'Tokelau', dial: '+690', code: 'TK' },
  { name: 'Tonga', dial: '+676', code: 'TO' },
  { name: 'Trinidad and Tobago', dial: '+1868', code: 'TT' },
  { name: 'Tunisia', dial: '+216', code: 'TN' },
  {
    name: 'Turkey',
    dial: '+90',
    code: 'TR',
    currency: 'TRY',
    referralAmount: 0
  },
  { name: 'Turkmenistan', dial: '+993', code: 'TM' },
  { name: 'Turks and Caicos Islands', dial: '+1649', code: 'TC' },
  { name: 'Tuvalu', dial: '+688', code: 'TV' },
  { name: 'Uganda', dial: '+256', code: 'UG' },
  {
    name: 'Ukraine',
    dial: '+380',
    code: 'UA',
    currency: 'UAH',
    referralAmount: 0
  },
  {
    name: 'United Arab Emirates',
    dial: '+971',
    code: 'AE',
    currency: 'AED',
    referralAmount: 0
  },
  {
    name: 'United Kingdom',
    dial: '+44',
    code: 'GB',
    currency: 'GBP',
    referralAmount: 0
  },
  {
    name: 'United States of America',
    dial: '+1',
    code: 'US',
    currency: 'USD',
    referralAmount: 0
  },
  { name: 'Uruguay', dial: '+598', code: 'UY' },
  { name: 'Uzbekistan', dial: '+998', code: 'UZ' },
  { name: 'Vanuatu', dial: '+678', code: 'VU' },
  {
    name: 'Venezuela, Bolivarian Republic of Venezuela',
    dial: '+58',
    code: 'VE'
  },
  {
    name: 'Vietnam',
    dial: '+84',
    code: 'VN',
    currency: 'VND',
    referralAmount: 0
  },
  { name: 'Virgin Islands, British', dial: '+1284', code: 'VG' },
  { name: 'Virgin Islands, U.S.', dial: '+1340', code: 'VI' },
  { name: 'Wallis and Futuna', dial: '+681', code: 'WF' },
  { name: 'Yemen', dial: '+967', code: 'YE' },
  { name: 'Zambia', dial: '+260', code: 'ZM' },
  { name: 'Zimbabwe', dial: '+263', code: 'ZW' }
]

const STATES_US = [
  { name: 'Alabama', code: 'US-AL' },
  { name: 'Alaska', code: 'US-AK' },
  { name: 'American Samoa', code: 'US-AS' },
  { name: 'Arizona', code: 'US-AZ' },
  { name: 'Arkansas', code: 'US-AR' },
  { name: 'California', code: 'US-CA' },
  { name: 'Colorado', code: 'US-CO' },
  { name: 'Connecticut', code: 'US-CT' },
  { name: 'Delaware', code: 'US-DE' },
  { name: 'District Of Columbia', code: 'US-DC' },
  { name: 'Federated States Of Micronesia', code: 'US-FM' },
  { name: 'Florida', code: 'US-FL' },
  { name: 'Georgia', code: 'US-GA' },
  { name: 'Guam', code: 'US-GU' },
  { name: 'Hawaii', code: 'US-HI' },
  { name: 'Idaho', code: 'US-ID' },
  { name: 'Illinois', code: 'US-IL' },
  { name: 'Indiana', code: 'US-IN' },
  { name: 'Iowa', code: 'US-IA' },
  { name: 'Kansas', code: 'US-KS' },
  { name: 'Kentucky', code: 'US-KY' },
  { name: 'Louisiana', code: 'US-LA' },
  { name: 'Maine', code: 'US-ME' },
  { name: 'Marshall Islands', code: 'US-MH' },
  { name: 'Maryland', code: 'US-MD' },
  { name: 'Massachusetts', code: 'US-MA' },
  { name: 'Michigan', code: 'US-MI' },
  { name: 'Minnesota', code: 'US-MN' },
  { name: 'Mississippi', code: 'US-MS' },
  { name: 'Missouri', code: 'US-MO' },
  { name: 'Montana', code: 'US-MT' },
  { name: 'Nebraska', code: 'US-NE' },
  { name: 'Nevada', code: 'US-NV' },
  { name: 'New Hampshire', code: 'US-NH' },
  { name: 'New Jersey', code: 'US-NJ' },
  { name: 'New Mexico', code: 'US-NM' },
  { name: 'New York', code: 'US-NY' },
  { name: 'North Carolina', code: 'US-NC' },
  { name: 'North Dakota', code: 'US-ND' },
  { name: 'Northern Mariana Islands', code: 'US-MP' },
  { name: 'Ohio', code: 'US-OH' },
  { name: 'Oklahoma', code: 'US-OK' },
  { name: 'Oregon', code: 'US-OR' },
  { name: 'Palau', code: 'US-PW' },
  { name: 'Pennsylvania', code: 'US-PA' },
  { name: 'Puerto Rico', code: 'US-PR' },
  { name: 'Rhode Island', code: 'US-RI' },
  { name: 'South Carolina', code: 'US-SC' },
  { name: 'South Dakota', code: 'US-SD' },
  { name: 'Tennessee', code: 'US-TN' },
  { name: 'Texas', code: 'US-TX' },
  { name: 'Utah', code: 'US-UT' },
  { name: 'Vermont', code: 'US-VT' },
  { name: 'Virgin Islands', code: 'US-VI' },
  { name: 'Virginia', code: 'US-VA' },
  { name: 'Washington', code: 'US-WA' },
  { name: 'West Virginia', code: 'US-WV' },
  { name: 'Wisconsin', code: 'US-WI' },
  { name: 'Wyoming', code: 'US-WY' }
]

const STATES_DE = [
  { name: 'Baden-Württemberg', code: 'DE-BW' },
  { name: 'Bayern', code: 'DE-BY' },
  { name: 'Berlin', code: 'DE-BE' },
  { name: 'Brandenburg', code: 'DE-BB' },
  { name: 'Bremen', code: 'DE-HB' },
  { name: 'Hamburg', code: 'DE-HH' },
  { name: 'Hessen', code: 'DE-HE' },
  { name: 'Mecklenburg-Vorpommern', code: 'DE-MV' },
  { name: 'Niedersachsen', code: 'DE-NI' },
  { name: 'Nordrhein-Westfalen', code: 'DE-NW' },
  { name: 'Rheinland-Pfalz', code: 'DE-RP' },
  { name: 'Saarland', code: 'DE-SL' },
  { name: 'Sachsen', code: 'DE-SN' },
  { name: 'Sachsen-Anhalt', code: 'DE-ST' },
  { name: 'Schleswig-Holstein', code: 'DE-SH' },
  { name: 'Thüringen', code: 'DE-TH' }
]

const STATES_UK = [
  { name: 'Avon', code: 'GB-AVN' },
  { name: 'Bedfordshire', code: 'GB-BDF' },
  { name: 'Berkshire', code: 'GB-BRK' },
  { name: 'Buckinghamshire', code: 'GB-BKM' },
  { name: 'Cambridgeshire', code: 'GB-CAM' },
  { name: 'Cheshire', code: 'GB-CHS' },
  { name: 'Cleveland', code: 'GB-CLV' },
  { name: 'Cornwall', code: 'GB-CON' },
  { name: 'Cumbria', code: 'GB-CMA' },
  { name: 'Derbyshire', code: 'GB-DBY' },
  { name: 'Devon', code: 'GB-DEV' },
  { name: 'Dorset', code: 'GB-DOR' },
  { name: 'Durham', code: 'GB-DUR' },
  { name: 'East Sussex', code: 'GB-SXE' },
  { name: 'Essex', code: 'GB-ESS' },
  { name: 'Gloucestershire', code: 'GB-GLS' },
  { name: 'Hampshire', code: 'GB-HAM' },
  { name: 'Herefordshire', code: 'GB-HEF' },
  { name: 'Hertfordshire', code: 'GB-HRT' },
  { name: 'Isle of Wight', code: 'GB-IOW' },
  { name: 'Kent', code: 'GB-KEN' },
  { name: 'Lancashire', code: 'GB-LAN' },
  { name: 'Leicestershire', code: 'GB-LEI' },
  { name: 'Lincolnshire', code: 'GB-LIN' },
  { name: 'London', code: 'GB-LDN' },
  { name: 'Merseyside', code: 'GB-MSY' },
  { name: 'Norfolk', code: 'GB-NFK' },
  { name: 'Northamptonshire', code: 'GB-NTH' },
  { name: 'Northumberland', code: 'GB-NBL' },
  { name: 'North Yorkshire', code: 'GB-NYK' },
  { name: 'Nottinghamshire', code: 'GB-NTT' },
  { name: 'Oxfordshire', code: 'GB-OXF' },
  { name: 'Rutland', code: 'GB-RUT' },
  { name: 'Shropshire', code: 'GB-SAL' },
  { name: 'Somerset', code: 'GB-SOM' },
  { name: 'South Yorkshire', code: 'GB-SYK' },
  { name: 'Staffordshire', code: 'GB-STS' },
  { name: 'Suffolk', code: 'GB-SFK' },
  { name: 'Surrey', code: 'GB-SRY' },
  { name: 'Tyne and Wear', code: 'GB-TWR' },
  { name: 'Warwickshire', code: 'GB-WAR' },
  { name: 'West Midlands', code: 'GB-WMD' },
  { name: 'West Sussex', code: 'GB-SXW' },
  { name: 'West Yorkshire', code: 'GB-WYK' },
  { name: 'Wiltshire', code: 'GB-WIL' },
  { name: 'Worcestershire', code: 'GB-WOR' }
]

export const STATES_OTHERS = [{ name: 'Not Applicable', code: 'NA' }]

export const CURRENT_STATES = {
  DE: STATES_DE,
  GB: STATES_UK,
  US: STATES_US,
  others: STATES_OTHERS
}

export const NEVER_REQUIRED_QUESTIONS_TYPES = new Set([
  'info',
  'profile',
  'time-stamp'
])

export const locationQuestionCountries = [
  'us',
  'de',
  'uk',
  'fr',
  'fr-dep',
  'br',
  'ca'
]

export const GENDERS = ['male', 'female', 'other']

export const LANGUAGES = ['english', 'german']

export const ETHNICITIES = [
  'african or african origin',
  'east asian or east asian origin',
  'asian or asian origin',
  'hispanic/latino or hispanic/latino origin',
  'caucasian',
  'other'
]

export const INCOME_RANGES_DE = [
  'Less than €25K',
  '€25K - €50K',
  '€50K - €75K',
  '€75K - €100K',
  '€100K or more'
]

export const INCOME_RANGES_UK = [
  'Less than £25K',
  '£25K - £50K',
  '£50K - £75K',
  '£75K - £100K',
  '£100K or more'
]

export const INCOME_RANGES_US = [
  'Less than $25K',
  '$25K - $50K',
  '$50K - $75K',
  '$75K - $100K',
  '$100K or more'
]

export const INCOME_RANGES = {
  DE: INCOME_RANGES_DE,
  GB: INCOME_RANGES_UK,
  US: INCOME_RANGES_US,
  others: INCOME_RANGES_US
}

export const MARKET_RESEARCH_PARTICIPATION = [
  'Once a week',
  'Once a month',
  'Once every six months',
  'Once per year',
  'Less often than once per year',
  'This is the first time'
]

export const CHILDREN_AGES = [
  { name: '0 years old', code: 0 },
  { name: '1 year old', code: 1 },
  { name: '2 years old', code: 2 },
  { name: '3 years old', code: 3 },
  { name: '4 years old', code: 4 },
  { name: '5 years old', code: 5 },
  { name: '6 years old', code: 6 },
  { name: '7 years old', code: 7 },
  { name: '8 years old', code: 8 },
  { name: '9 years old', code: 9 },
  { name: '10 years old', code: 10 },
  { name: '11 years old', code: 11 },
  { name: '12 years old', code: 12 },
  { name: '13 years old', code: 13 },
  { name: '14 years old', code: 14 },
  { name: '15 years old', code: 15 },
  { name: '16 years old', code: 16 },
  { name: '17 years old', code: 17 }
]

export const PHOTO_ANSWER_FILTERS = [
  {
    name: 'Not Processed',
    code: 'not_processed'
  },
  {
    name: 'Valid',
    code: 'valid'
  },
  {
    name: 'Invalid',
    code: 'invalid'
  },
  {
    name: 'All',
    code: 'all'
  }
]

export const ERRORS = {
  NOT_AUTH_SURVEY: 'Could not find survey with the name'
}

export const SKIP_MULTIPLE_TYPES = {
  ALL_OF_THE_FOLLOWING: 'All of the following',
  ANY_OF_THE_FOLLOWING: 'Any of the following',
  ANY_OF_THE_FOLLOWING_NOT_SELECTED: 'Any of the following are not selected'
}

export const MAX_LIMIT_REFERRAL = 100000

export const PRODUCT_STATS_COUNT_MIN = 6

export const PRODUCT_STATS_COUNT_MAX = 12

export const PRODUCT_LIMIT_MIN_STATS = 2

export const STATS_FILTER_ACCEPTED_TYPES = [
  'choose-one',
  'choose-multiple',
  'dropdown',
  'location'
]

export const DEFAULT_TASTING_NOTES = {
  tastingId: '',
  tastingLeader: '',
  customer: '',
  country: '',
  dateOfTasting: '',
  otherInfo: ''
}
