import i18n from '../internationalization/i18n'
import ReactDOMServer from 'react-dom/server'

export const getMessage = (tkey) => {
  return i18n.t(tkey, {
    amount: ReactDOMServer.renderToString('{{amount}}')
  })
}