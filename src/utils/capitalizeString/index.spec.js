import { capitalizeString } from '.'

describe('capitalizeString', () => {
  test('should render capitalizeString', () => {
    expect(capitalizeString('test')).toBe('Test')
  })
})
