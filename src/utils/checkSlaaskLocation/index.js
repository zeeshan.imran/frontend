const checkSlaaskLocation = location => {
  const { pathname } = location
  let substrings = ['taster-pdf', 'stats-pdf', 'preview-operator-pdf', 'preview-taster-pdf']
  let flaskLocationFlag = false
  if (substrings.some(v => pathname.includes(v))) {
    flaskLocationFlag = true
  }
  return flaskLocationFlag
}

export default checkSlaaskLocation
