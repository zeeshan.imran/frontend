import { displaySuccessMessage } from './index'

describe('displaySuccessMessage', () => {
  let message

  beforeEach(() => {
    message = 'This is a message of success'
  })

  test('should render displaySuccessMessage', () => {
    displaySuccessMessage(message)

    expect(
      global.document.querySelectorAll('.ant-message-success')
    ).not.toBeNull()
    expect(
      global.document.querySelector('.ant-message-success').textContent
    ).toBe('This is a message of success')
  })
})
 