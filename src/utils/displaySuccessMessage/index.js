import { message as AntMessage } from 'antd'
import { MESSAGE_DEFAULT_TIMEOUT } from '../Constants'

export const displaySuccessMessage = message =>
  AntMessage.success(message, MESSAGE_DEFAULT_TIMEOUT)
