import { CHART_TYPES } from '../Constants'

const getChartTypes = ({ type, displayOn }) => {
  // please sync with
  // https://docs.google.com/spreadsheets/d/1gUqFqK7L-mI9sHGt6arBzaJ3lqy9eC2x8VQBvBahx6U/edit#gid=2129518787
  let allChartTypes = CHART_TYPES

  return (allChartTypes[type] || {})[displayOn] || []
}

export default getChartTypes
