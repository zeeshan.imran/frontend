import formatBasicsForPublishing from './index.js'
import defaultSurvey from '../../mocks/defaultSurvey.js'

const basics = {
  ...defaultSurvey.basics,
  authorizationType: 'selected',
  basicsDefaults: 'profile',
  name: 'Survey 1',
  uniqueName: 'survey-1',
  recaptcha: true,
  minimumProducts: 0,
  allowedDaysToFillTheTasting: 5
}
const products = {
  id: 'product-1',
  name: 'product',
  brand: 'brand'
}

describe('formatBasicsForPublishing', () => {
  test('Should test of formatBasicsForPublishing utilis', () => {
    expect(formatBasicsForPublishing({ basics, products })).toMatchObject({
      authorizationType: 'selected',
      basicsDefaults: 'profile',
      customButtons: {
        continue: 'Continue',
        next: 'Next',
        skip: 'Skip',
        start: 'Start'
      },
      exclusiveTasters: [],
      minimumProducts: NaN,
      name: 'Survey 1',
      recaptcha: true,
      uniqueName: 'survey-1'
    })
  })
})
