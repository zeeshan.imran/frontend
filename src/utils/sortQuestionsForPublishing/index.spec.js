import sortQuestionsForPublishing from './index'

describe('sortQuestionsForPublishing', () => {
  test('should set updating requiredQuestion for slider', () => {
    const question = [
      {
        id: 'question-1',
        type: 'paired-questions',
        prompt: 'What is your email?',

        pairsOptions: ['pair-1', 'pair-2'],
        clientGeneratedId: 'question-1',
        displayOn: 'screening'
      },
      {
        id: 'question-4',
        type: 'paired-questions',
        prompt: 'Please select and click buttons',
        pairsOptions: ['pair-3', 'pair-4'],
        clientGeneratedId: 'question-4',
        displayOn: 'begin'
      },
      {
        id: 'question-2',
        type: 'email',
        prompt: 'What is your email?',
        displayOn: 'middle'
      },
      {
        id: 'question-5',
        type: 'text',
        prompt: 'Please share your feedback hee',
        displayOn: 'end'
      }
    ]
    const mandatoryQuestions = [
      {
        id: 'question-2',
        type: 'email',
        prompt: 'What is your email?',
        displayOn: 'middle'
      }
    ]
    expect(
      sortQuestionsForPublishing(question, mandatoryQuestions)
    ).toMatchObject([
      {
        clientGeneratedId: 'question-1',
        displayOn: 'screening',
        id: 'question-1',
        pairsOptions: ['pair-1', 'pair-2'],
        prompt: 'What is your email?',
        type: 'paired-questions'
      },
      {
        clientGeneratedId: 'question-4',
        displayOn: 'begin',
        id: 'question-4',
        pairsOptions: ['pair-3', 'pair-4'],
        prompt: 'Please select and click buttons',
        type: 'paired-questions'
      },
      {
        displayOn: 'middle',
        id: 'question-2',
        prompt: 'What is your email?',
        type: 'email'
      },
      {
        displayOn: 'middle',
        id: 'question-2',
        prompt: 'What is your email?',
        type: 'email'
      },
      {
        displayOn: 'end',
        id: 'question-5',
        prompt: 'Please share your feedback hee',
        type: 'text'
      }
    ])
  })
})
