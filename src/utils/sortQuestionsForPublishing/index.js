const getQuestionsBySection = questions =>
  questions.reduce(
    (bySection, question) => {
      return {
        ...bySection,
        [question.displayOn]: [...bySection[question.displayOn], question]
      }
    },
    { payments: [], screening: [], begin: [], middle: [], end: [] }
  )

const sortQuestionsForPublishing = (questions, mandatoryQuestions) => {
  const questionsBySection = getQuestionsBySection(questions)
  const mandatoryQuestionsBySection = getQuestionsBySection(mandatoryQuestions)

  const allQuestionsBySection = {
    payments: [
      ...mandatoryQuestionsBySection.payments,
      ...questionsBySection.payments
    ],
    screening: [
      ...mandatoryQuestionsBySection.screening,
      ...questionsBySection.screening
    ],
    begin: [...mandatoryQuestionsBySection.begin, ...questionsBySection.begin],
    middle: [
      ...mandatoryQuestionsBySection.middle,
      ...questionsBySection.middle
    ],
    end: [...mandatoryQuestionsBySection.end, ...questionsBySection.end]   
  }

  return [
    ...allQuestionsBySection.payments,
    ...allQuestionsBySection.screening,
    ...allQuestionsBySection.begin,
    ...allQuestionsBySection.middle,
    ...allQuestionsBySection.end
  ]
}

export default sortQuestionsForPublishing
