const Fonts = require('../Fonts')
module.exports = () => {
  return {
    '@link-color': "#0085F2", // link color
    '@primary-color': "#0E83EA", // primary color
    '@success-color': '#7AC2A1', // success state color
    '@warning-color': '#fcb204', // warning state color
    '@error-color': '#F0485E', // error state color
    '@font-family': Fonts.family.primaryRegular,
    '@font-size-base': '14px', // major text font size
    '@heading-color': 'rgba(0, 0, 0, .85)', // heading text color
    '@text-color': 'rgb(0, 18, 51)',
    '@btn-height-lg': '50px',
    '@table-row-hover-bg' : '#D3EBE0'
  }
}



