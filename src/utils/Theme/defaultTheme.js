const colors = require('../Colors')
const Fonts = require('../Fonts')

module.exports = () => {
  return {
    '@link-color': colors.LIGHT_OLIVE_GREEN, // link color
    '@primary-color': colors.LIGHT_OLIVE_GREEN, // primary color
    '@success-color': '#52c41a', // success state color
    '@warning-color': '#faad14', // warning state color
    '@error-color': '#f5222d', // error state color
    '@font-family': Fonts.family.primaryRegular,
    '@font-size-base': '14px', // major text font size
    '@heading-color': 'rgba(0, 0, 0, .85)', // heading text color
  }
}
