let Theme = null
if (process.env.REACT_APP_THEME) {
  Theme = require(`./${process.env.REACT_APP_THEME}Theme`)
} else {
  Theme = require('./defaultTheme')
}

module.exports = Theme
