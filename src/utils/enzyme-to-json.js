import { createSerializer } from 'enzyme-to-json'

class MockedClient {}

class I18n {}
class YupSchema {}

module.exports = createSerializer({
  noKey: true,
  map: json => {
    if(json.props.validationSchema && json.props.validationSchema.constructor.name === 'ObjectSchema') {
      return {
        ...json,
        props: {
          ...json.props,
          validationSchema: new YupSchema()
        }
      }
    }
    if (json.props.i18n && json.props.i18n.constructor.name === 'I18n') {
      return {
        ...json,
        props: {
          ...json.props,
          i18n: new I18n()
        }
      }
    }
    if (json.type === 'ApolloProvider') {
      return {
        ...json,
        props: {
          ...json.props,
          client: new MockedClient()
        }
      }
    }
    return json
  }
})
