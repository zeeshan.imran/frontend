export const CURRENCIES = ['EUR', 'USD', 'CHF']

export const BINARY_RESPONSES = ['Yes', 'No']

export const CHILDREN_AGES = [...new Array(parseInt(17, 10))].map((_, index) =>
  parseInt(index + 1, 10).toString()
)

export const SMOKER = ['Yes', 'No', 'Casual']

export const PRODUCT_TYPES = ['Milk', 'Fast Food']

export const DIETS = [
  'Vegetarian',
  'Ovo-Vegetarian',
  'Lacto-Vegetarian',
  'Lacto-Ovo Vegetarian',
  'Pescetarian',
  'Vegan'
]
