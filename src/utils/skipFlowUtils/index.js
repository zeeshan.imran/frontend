import { all, intersection, ap, concat as rconcat } from 'ramda'
import { SKIP_MULTIPLE_TYPES } from '../Constants'

const getSkipToByValue = (skipFlow, value) => {
  if (!skipFlow) {
    return null
  }
  const skipRule = skipFlow.rules.find(m => m.value === value)
  return (skipRule && skipRule.skipTo) || null
}

const getSkipToByIndex = (skipFlow, index) => {
  if (!skipFlow) {
    return null
  }
  const skipRule = skipFlow.rules[index]
  return (skipRule && skipRule.skipTo) || null
}

class SkipFlowChain {
  constructor (skipFlow) {
    this.skipFlow = skipFlow
  }

  findRule (filter) {
    return this.skipFlow.rules.find(rule => filter(rule))
  }

  findMultipleRule (answers) {
    const found = this.skipFlow.rules.find(rule => {
      if (rule.type === SKIP_MULTIPLE_TYPES.ALL_OF_THE_FOLLOWING) {
        return (
          intersection(rule.answers, answers).length === rule.answers.length
        )
      } else if (rule.type === SKIP_MULTIPLE_TYPES.ANY_OF_THE_FOLLOWING) {
        return intersection(rule.answers, answers).length > 0
      } else if (
        rule.type === SKIP_MULTIPLE_TYPES.ANY_OF_THE_FOLLOWING_NOT_SELECTED
      ) {
        return intersection(rule.answers, answers).length === 0
      } else {
        return false
      }
    })
    return found
  }

  findMatrixRule (answers, question) {
    const found = this.skipFlow.rules.find(rule => {
      if (!rule.skipTo) {
        return false
      }
      let allMatched = true
      const rows = Object.keys(answers)
      let rowAnswers = []

      if (rule.type === SKIP_MULTIPLE_TYPES.ALL_OF_THE_FOLLOWING) {
        rows.forEach(singleRow => {
          let singleRowAnswers = answers[singleRow]
          if (typeof singleRowAnswers !== 'object') {
            singleRowAnswers = [singleRowAnswers]
          }
          if (
            intersection(rule.answers, singleRowAnswers).length !==
            rule.answers.length
          ) {
            allMatched = false
          }
          rowAnswers = rowAnswers.concat(
            ap([rconcat(singleRow + '-')], singleRowAnswers)
          )
        })
        if (!allMatched) {
          return (
            intersection(rule.answers, rowAnswers).length ===
            rule.answers.length
          )
        }
        return allMatched
      } else if (rule.type === SKIP_MULTIPLE_TYPES.ANY_OF_THE_FOLLOWING) {
        allMatched = false
        rows.forEach(singleRow => {
          let singleRowAnswers = answers[singleRow]
          if (typeof singleRowAnswers !== 'object') {
            singleRowAnswers = [singleRowAnswers]
          }
          if (intersection(rule.answers, singleRowAnswers).length > 0) {
            allMatched = true
            return
          }
          rowAnswers = rowAnswers.concat(
            ap([rconcat(singleRow + '-')], singleRowAnswers)
          )
        })
        if (!allMatched) {
          return intersection(rule.answers, rowAnswers).length > 0
        }
        return allMatched
      } else if (
        rule.type === SKIP_MULTIPLE_TYPES.ANY_OF_THE_FOLLOWING_NOT_SELECTED
      ) {
        allMatched = false
        rows.forEach(singleRow => {
          let singleRowAnswers = answers[singleRow]
          if (typeof singleRowAnswers !== 'object') {
            singleRowAnswers = [singleRowAnswers]
          }
          if (intersection(rule.answers, singleRowAnswers).length === 0) {
            allMatched = true
            return
          }
          rowAnswers = rowAnswers.concat(
            ap([rconcat(singleRow + '-')], singleRowAnswers)
          )
        })
        if (!allMatched) {
          return intersection(rule.answers, rowAnswers).length === 0
        }
        return allMatched
      } else {
        return false
      }
    })
    return found
  }

  removeRules (filter) {
    this.skipFlow.rules = this.skipFlow.rules.filter(rule => !filter(rule))
    return this
  }

  updateRules (filter, updateSet) {
    this.skipFlow.rules = this.skipFlow.rules.map(rule => {
      if (filter(rule)) {
        return {
          ...rule,
          ...updateSet
        }
      }

      return rule
    })

    return this
  }

  addRule (rule) {
    this.skipFlow.rules = [...this.skipFlow.rules, rule]
    return this
  }

  getNextSkipFlow () {
    return this.skipFlow
  }
}

const isAllSkipToEmpty = skipFlow =>
  !skipFlow || all(m => !m.skipTo)(skipFlow.rules)

const isValidSkipFlow = skipFlow => skipFlow && skipFlow.rules

const hasSkipFlow = question => isValidSkipFlow(question.skipFlow)

const skipFlowUtils = {
  getSkipToByValue,
  getSkipToByIndex,
  isAllSkipToEmpty,
  hasSkipFlow,
  fromSkipFlow: skipFlow => new SkipFlowChain(skipFlow)
}

export default skipFlowUtils
