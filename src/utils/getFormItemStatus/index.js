export const getFormItemStatus = condition => (condition ? 'error' : 'success')
