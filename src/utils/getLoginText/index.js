import i18n from '../internationalization/i18n'

export const getLoginText = () => {
  return i18n.t('defaultValues.loginText')
}
