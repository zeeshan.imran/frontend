import formatProductsForPublishing from './index.js'

describe('formatProductsForPublishing', () => {
  test('formatProductsForPublishing to be 0', () => {
    const products = [
      {
        id: 'product-1',
        name: 'product'
      }
    ]
    expect(formatProductsForPublishing(products)).toMatchObject([
      { id: 'product-1', name: 'product' }
    ])
  })
})
