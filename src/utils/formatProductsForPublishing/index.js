import { omit } from 'ramda'

const UNSUPPORTED_PROPS_IN_REQUEST = ['__typename', 'editMode']

const formatProductsForPublishing = products => {
  const formattedProducts = products.map(product =>
    omit(UNSUPPORTED_PROPS_IN_REQUEST, product)
  )

  return formattedProducts
}

export default formatProductsForPublishing
