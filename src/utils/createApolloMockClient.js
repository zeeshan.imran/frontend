import ApolloClient from 'apollo-client'
import { InMemoryCache as Cache } from 'apollo-cache-inmemory'
import { MockLink } from 'react-apollo/test-links'
import { onError } from 'apollo-link-error'
import resolvers from '../resolvers'
import defaults from '../defaults'
import fs from 'fs'

export const createApolloMockClient = ({ mocks, cache: passedCache } = {}) => {
  const addTypename = false
  const defaultOptions = undefined
  const cache = passedCache || new Cache({ addTypename })

  cache.writeData({
    data: {
      ...{
        ...defaults.currentSurveyParticipation,
        __type: undefined
      },
      ...{
        ...defaults.surveyCreation,
        __type: undefined
      }
    }
  })

  return new ApolloClient({
    cache: cache || new Cache({ addTypename }),
    defaultOptions,
    link: onError(({ networkError }) => {
      if (networkError) {
        console.log(`[Network error]: ${networkError}`)
        process.env.GRAPHQL_REPORT && fs.appendFileSync(
          './test-graphql-report.log',
          `[Network error]: ${new Error(networkError.toString()).stack}\n\n`
        )
      }
    }).concat(new MockLink(mocks || [], addTypename)),
    resolvers
  })
}
