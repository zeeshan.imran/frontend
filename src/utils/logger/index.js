/* eslint-disable no-console */
import * as Sentry from '@sentry/browser'
import '../../config/Sentry'

const KB = 1024
const logger = {
  setContext: (name, data) => {
    try {
      if (JSON.stringify(data).length < 150 * KB) {
        Sentry.setContext(name, data)
      }
    } catch (ex) {
      //
      Sentry.setContext(name, 'Data are too large data')
    }
  },
  clearContext: () => {
    Sentry.configureScope(scope => scope.clear())
  },
  error: e => {
    // use Sentry for production
    if (process.env.NODE_ENV === 'production') {
      Sentry.captureException(e)
    }
  },
  warn: console.warn,
  log: console.log
}

export default logger
