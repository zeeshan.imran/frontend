import { message as AntMessage } from 'antd'
import { MESSAGE_DEFAULT_TIMEOUT } from '../Constants'

export const displayInfoPopup = message => {
  AntMessage.destroy()
  AntMessage.info(message, MESSAGE_DEFAULT_TIMEOUT)
}
 