import { displayInfoPopup } from './index'

describe('displayInfoPopup', () => {
  let message

  beforeEach(() => {
    message = 'This is a message of info'
  })

  test('should render displayInfoPopup', () => {
    displayInfoPopup(message)

    expect(
      global.document.querySelectorAll('.ant-message-info')
    ).not.toBeNull()
    expect(
      global.document.querySelector('.ant-message-info').textContent
    ).toBe('This is a message of info')
  })
})
