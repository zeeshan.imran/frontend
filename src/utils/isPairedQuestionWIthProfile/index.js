const isPairedQuestionWithProfile = question =>
  question.type === 'paired-questions' &&
  question.pairsOptions &&
  question.pairsOptions.hasFollowUpProfile

export default isPairedQuestionWithProfile
