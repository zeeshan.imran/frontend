export const getSurveyCompletionPercentage = (participants, filter) => {
  const totalParticipants = participants.length

  const count = participants.reduce(
    (nParticipantsInState, participant) =>
      participant.state === filter
        ? nParticipantsInState + 1
        : nParticipantsInState,
    0
  )
  const percentage = (count / totalParticipants) * 100

  return Math.round(percentage * 10) / 10
}
