import { getSurveyCompletionPercentage } from './index'

describe('getSurveyCompletionPercentage', () => {
  test('should render getSurveyCompletionPercentage', () => {
    expect(getSurveyCompletionPercentage([4, 5, 6, 7, 8], 'COMPLETE')).toBe(0)
  })
})
