import { isNil, pickBy } from 'ramda'
import isPairedQuestionWithProfile from '../isPairedQuestionWIthProfile'
import formatChooseProductOptions from '../formatChooseProductOptions'
import { formatQuestionSettings } from '../formatQuestionSettings'

const UNSUPPORTED_PROPS_IN_REQUEST = new Set([
  '__typename',
  'required',
  'nextQuestion',
  'editMode'
])

const isSliderQuestionWithProfile = question =>
  question.type === 'slider' &&
  question.sliderOptions &&
  question.sliderOptions.hasFollowUpProfile

export const getQuestionForUpdating = ({ question, order, surveyId }) => {
  return {
    ...pickBy(
      (val, key) => !UNSUPPORTED_PROPS_IN_REQUEST.has(key) && !isNil(val),
      question
    ),
    requiredQuestion: question.required,
    order,
    survey: surveyId,
    settings: formatQuestionSettings(question.settings),
    chooseProductOptions: formatChooseProductOptions(
      question.chooseProductOptions
    )
  }
}

const formatQuestionsForUpdating = ({ questions, surveyId }) => {
  let questionOrder = 0
  const formattedQuestions = questions.reduce((accFormatted, question) => {
    const formattedToUpdate = getQuestionForUpdating({
      question,
      order: questionOrder,
      surveyId
    })

    if (isPairedQuestionWithProfile(formattedToUpdate)) {
      const pairedAndProfile = {
        ...formattedToUpdate,
        pairsOptions: {
          ...formattedToUpdate.pairsOptions,
          profileQuestion: {
            ...getQuestionForUpdating({
              question: formattedToUpdate.pairsOptions.profileQuestion,
              order: questionOrder + 1,
              surveyId
            }),
            order: questionOrder + 1,
            type: 'profile',
            displayOn: question.displayOn
          }
        }
      }

      questionOrder += 2
      return [...accFormatted, pairedAndProfile]
    } else if (isSliderQuestionWithProfile(formattedToUpdate)) {
      const sliderAndProfile = {
        ...formattedToUpdate,
        sliderOptions: {
          ...formattedToUpdate.sliderOptions,
          profileQuestion: {
            ...getQuestionForUpdating({
              question: {
                prompt: formattedToUpdate.sliderOptions.profilePrompt
              },
              order: questionOrder + 1,
              surveyId
            }),
            order: questionOrder + 1,
            type: 'profile',
            displayOn: question.displayOn
          }
        }
      }

      questionOrder += 2
      return [...accFormatted, sliderAndProfile]
    } else {
      questionOrder++
      return [...accFormatted, formattedToUpdate]
    }
  }, [])

  return formattedQuestions
}

export default formatQuestionsForUpdating
