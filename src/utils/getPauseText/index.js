import i18n from '../internationalization/i18n'

export const getPauseText = () => {
  return i18n.t('defaultValues.pauseText')
}
