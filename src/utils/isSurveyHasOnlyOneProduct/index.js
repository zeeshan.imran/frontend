const isSurveyHasOnlyOneProduct = survey => {
  const availableProducts =
    (survey.products &&
      survey.products.filter(product => product.isAvailable)) ||
    []
  return availableProducts.length === 1
}

export default isSurveyHasOnlyOneProduct
