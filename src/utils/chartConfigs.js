import * as Yup from 'yup'
import PolarChart from '../components/Charts/PolarChart'
import PieChart from '../components/Charts/PieChart'
import LineChart from '../components/Charts/LineChart'
import StackedColumnsChart from '../components/Charts/StackedColumnsChart'
import StackedBarChart from '../components/Charts/StackedBarChart'
import MultipleProductsStackedBarChart from '../components/Charts/StackedBarChart/multipleProducts'
import ColumnsChart from '../components/Charts/ColumnsChart'
import ColumnsChartMean from '../components/Charts/ColumnsChartMean'
import MapChart from '../components/Charts/MapChart'
import TableChart from '../components/Charts/TableChart'
import TagsList from '../components/Charts/TagsList'
import {
  lineChartInputSchema,
  defaultChartSchema,
  defaultChartSchemaGoupedByProducts,
  defaultWithAvgChartSchema,
  defaultWithCountryGeoData,
  polarChartInputSchema,
  tableSchema
} from '../validates/chartsResults'

export const CHART_SIZES = {
  DEFAULT: 24,
  '': 12,
  pie: 12
}

export const CARD_WIDTH = {
  DEFAULT: 'full',
  '': 'half',
  pie: 'half'
}

const CHART_COMPONENTS = {
  DEFAULT: PieChart,
  pie: PieChart,
  map: MapChart,
  'stacked-column': StackedColumnsChart,
  'stacked-column-horizontal-bars': StackedColumnsChart,
  column: ColumnsChart,
  'columns-mean': ColumnsChartMean,
  polar: PolarChart,
  line: LineChart,
  table: TableChart,
  'spearmann-table': TableChart,
  'pearson-table': TableChart,
  'tags-list': TagsList
}

const CHART_SCHEMAS = {
  DEFAULT: defaultChartSchema,
  map: defaultWithCountryGeoData,
  'stacked-column-horizontal-bars': defaultWithAvgChartSchema,
  'horizontal-bars-mean': defaultWithAvgChartSchema,
  polar: polarChartInputSchema,
  line: lineChartInputSchema,
  table: tableSchema,
  'spearmann-table': tableSchema,
  'pearson-table': tableSchema,
  'tags-list': Yup.mixed()
}

export const getChartSchema = el => {
  if (['stacked-bar', 'horizontal-bars-mean', 'bar'].includes(el.chart_type)) {
    return el.attributes
      ? defaultChartSchemaGoupedByProducts
      : el.chart_type === 'horizontal-bars-mean'
      ? defaultWithAvgChartSchema
      : defaultChartSchema
  }
  return CHART_SCHEMAS[el.chart_type] || CHART_SCHEMAS.DEFAULT
}

export const getChartComponent = el => {
  if (['stacked-bar', 'horizontal-bars-mean', 'bar'].includes(el.chart_type)) {
    return el.attributes ||
      (!['stacked-bar', 'horizontal-bars-mean'].includes(el.chart_type) &&
        Array.isArray(el.results))
      ? MultipleProductsStackedBarChart
      : StackedBarChart
  }
  return CHART_COMPONENTS[el.chart_type] || CHART_COMPONENTS.DEFAULT
}
