import startCase from 'lodash.startcase'
import capitalize from 'lodash.capitalize'

export const getRoutesArray = route => {
  const splitUp = route.pathname.split('/')

  return splitUp.reduce((formatted, part) => [
    ...formatted,
    { path: part, breadcrumbName: capitalize(startCase(part)) }
  ])
}
