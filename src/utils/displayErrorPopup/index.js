import { message as AntMessage } from 'antd'
import { MESSAGE_DEFAULT_TIMEOUT } from '../Constants'

export const displayErrorPopup = message => {
  AntMessage.destroy()
  AntMessage.error(message, MESSAGE_DEFAULT_TIMEOUT)
}
export const distroyErrorPopup = message => {
  AntMessage.destroy()
}
 