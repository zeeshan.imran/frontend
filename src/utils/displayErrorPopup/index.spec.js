import { displayErrorPopup } from './index'

describe('displayErrorPopup', () => {
  let message

  beforeEach(() => {
    message = 'This is a message of error'
  })

  test('should render displayErrorPopup', () => {
    displayErrorPopup(message)

    expect(
      global.document.querySelectorAll('.ant-message-error')
    ).not.toBeNull()
    expect(
      global.document.querySelector('.ant-message-error').textContent
    ).toBe('This is a message of error')
  })
})
