export const getCategoryName = (category = {}) => category.value

export const getCategoryPicture = (category = {}) => category.picture

export const getCategoryKey = (category = {}) => category.key
