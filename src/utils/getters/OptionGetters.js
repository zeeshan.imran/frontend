export const getOptionValue = (option = {}) => option.value

export const getOptionName = (option = {}) => option.label
