const flavorwikiRegex = /(@flavorwiki\.com)$/g

const emailWhiteLists = [
  'babar.shahzad@gigalabs.co',
  'kmtank.qa@gmail.com',
  'jineshdarji3011@gmail.com',
  'jinesh301192@gmail.com'
]

const isFlavorwikiUser = user =>
  user &&
  user.emailAddress &&
  (user.emailAddress.search(flavorwikiRegex) > 0 ||
    emailWhiteLists.includes(user.emailAddress))

export default isFlavorwikiUser
