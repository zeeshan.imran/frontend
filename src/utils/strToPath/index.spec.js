import strToPath from '.'

describe('strToPath', () => {
  test('should render strToPath', () => {
    const str = 'this is test'
    expect(strToPath(str)).toBe('this-is-test')
  })
})
