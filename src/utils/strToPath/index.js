const strToPath = str =>
  str
    .trim()
    .replace('&', 'and')
    .replace(/[?/+%]+/g, '')
    .split(' ')
    .map(part => part.toLowerCase())
    .join('-')

export default strToPath
