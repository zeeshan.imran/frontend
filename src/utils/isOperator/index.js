const isOperator = user => user && user.type === 'operator'

export default isOperator
