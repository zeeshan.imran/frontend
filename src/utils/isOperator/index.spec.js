import  isOperator  from './index'

describe('isOperator', () => {
  test('should render isOperator', () => {
    expect(
      isOperator({
        organization: { id: 1 },
        emailAddress: 'test@flowor.com',
        type: 'operator'
      })
    ).toBe(true)
  })
})
