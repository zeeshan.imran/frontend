const StaticUrlOrBase64 = (image) => {
  if (image && image.includes('base64')) {
    return image
  } else if (image) {
    return `${process.env.REACT_APP_BACKEND_API_URL}${image}`
  } else {
    return image
  }
}

export default StaticUrlOrBase64