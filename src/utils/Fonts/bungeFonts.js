const defaultFonts = {
    primaryRegular: 'Avenir-Roman',
    primaryLight: 'Avenir-Light',
    primaryItalic: 'Avenir-Roman',
    primaryBoldItalic: 'Verdana-Bold',
    primaryBold: 'Verdana-Bold',
    primaryBlack: 'Avenir-Medium',

    GeezaProRegular: 'Geeza-Pro-Regular',
    Yahei: 'Yahei',
  
    NotoSansKRRegular: 'NotoSans-kr-Regular',
    NotoSansKRLight: 'NotoSans-kr-Light',
    NotoSansKRBold: 'NotoSans-kr-Bold',
    NotoSansKRBlack: 'NotoSans-kr-Black'
  
  }
  
  module.exports = defaultFonts
  