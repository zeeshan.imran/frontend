let family = {}
if (process.env.REACT_APP_THEME) {
  family = require(`./${process.env.REACT_APP_THEME}Fonts`)
} else {
  family = require('./defaultFonts')
}
module.exports = {
  family
}
