const hanseFonts = {
  primaryRegular: 'NunitoSans-Regular',
  primaryLight: 'NunitoSans-Light',
  primaryItalic: 'Lato-Italic',
  primaryBoldItalic: 'Lato-Bold-Italic',
  primaryBold: 'NunitoSans-Bold',
  primaryBlack: 'NunitoSans-Black',

  NotoSansKRRegular: 'NunitoSans-Regular',
  NotoSansKRLight: 'NunitoSans-Light',
  NotoSansKRBold: 'NunitoSans-Bold',
  NotoSansKRBlack: 'NunitoSans-Black',
}

module.exports = hanseFonts
