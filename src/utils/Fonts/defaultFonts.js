const defaultFonts = {
  primaryRegular: 'Lato-Regular',
  primaryLight: 'Lato-Light',
  primaryItalic: 'Lato-Italic',
  primaryBoldItalic: 'Lato-Bold-Italic',
  primaryBold: 'Lato-Bold',
  primaryBlack: 'Lato-Black',

  NotoSansKRRegular: 'NotoSans-kr-Regular',
  NotoSansKRLight: 'NotoSans-kr-Light',
  NotoSansKRBold: 'NotoSans-kr-Bold',
  NotoSansKRBlack: 'NotoSans-kr-Black'

}

module.exports = defaultFonts
