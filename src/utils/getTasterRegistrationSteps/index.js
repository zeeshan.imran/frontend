import gql from 'graphql-tag'

export const getTasterRegistrationSteps = (loggedInUser = {}, t) => {
  return [
    {
      title: t('components.createTasterAccount.steps.first.title'),
      description: t('components.createTasterAccount.steps.first.description'),
      path: 'welcome',
      isValid: !!loggedInUser,
      formData: {
        emailAddress: loggedInUser ? loggedInUser.emailAddress : ''
      },
      okText: t('components.createTasterAccount.steps.first.okText'),
      redirectToLogin: true,
      mutation: gql`
        mutation createTasterAccount($input: CreateTasterAccountInput) {
          createTasterAccount(input: $input) {
            token
            user {
              id
              emailAddress
              type
            }
          }
        }
      `
    },
    {
      title: t('components.createTasterAccount.steps.second.title'),
      description: t('components.createTasterAccount.steps.second.description'),
      path: 'personal-information',
      isValid:
        loggedInUser &&
        loggedInUser.fullName &&
        loggedInUser.gender &&
        loggedInUser.dateofbirth &&
        loggedInUser.country &&
        loggedInUser.state &&
        loggedInUser.language,
      formData: {
        firstName:
          loggedInUser && loggedInUser.emailAddress !== loggedInUser.fullName
            ? loggedInUser.fullName.split(' ')[0]
            : '',
        lastName: loggedInUser
          ? loggedInUser.fullName.split(' ')[1]
            ? loggedInUser.fullName.split(' ')[1]
            : ''
          : '',
        gender: loggedInUser ? loggedInUser.gender : '',
        dateofbirth: loggedInUser ? loggedInUser.dateofbirth : '',
        country: loggedInUser ? loggedInUser.country : '',
        state: loggedInUser ? loggedInUser.state : '',
        language: loggedInUser ? loggedInUser.language : ''
      },
      okText: t('components.createTasterAccount.steps.second.okText'),
      mutation: gql`
        mutation updateTasterAccount($input: UpdateTasterAccountInput) {
          updateTasterAccount(input: $input) {
            fullName
            gender
            dateofbirth
            country
            state
            language
          }
        }
      `
    },
    {
      title: t('components.createTasterAccount.steps.third.title'),
      description: t('components.createTasterAccount.steps.third.description'),
      path: 'additional-information',
      isValid:
        loggedInUser &&
        loggedInUser.ethnicity &&
        loggedInUser.incomeRange &&
        // loggedInUser.hasChildren &&
        loggedInUser.smokerKind &&
        loggedInUser.foodAllergies &&
        loggedInUser.marketResearchParticipation &&
        loggedInUser.paypalEmailAddress,
      formData: {
        ethnicity: loggedInUser ? loggedInUser.ethnicity : '',
        incomeRange: loggedInUser ? loggedInUser.incomeRange : '',
        hasChildren: loggedInUser ? loggedInUser.hasChildren : '',
        numberOfChildren: loggedInUser ? loggedInUser.numberOfChildren : 0,
        childrenAges: loggedInUser ? loggedInUser.childrenAges : {},
        smokerKind: loggedInUser ? loggedInUser.smokerKind : '',
        foodAllergies: loggedInUser ? loggedInUser.foodAllergies : '',
        marketResearchParticipation: loggedInUser
          ? loggedInUser.marketResearchParticipation
          : '',
        paypalEmailAddress: loggedInUser ? loggedInUser.paypalEmailAddress : ''
      },
      okText: t('components.createTasterAccount.steps.third.okText'),
      mutation: gql`
        mutation updateTasterAccount($input: UpdateTasterAccountInput) {
          updateTasterAccount(input: $input) {
            ethnicity
            incomeRange
            hasChildren
            numberOfChildren
            childrenAges
            smokerKind
            foodAllergies
            marketResearchParticipation
            paypalEmailAddress
          }
        }
      `
    }
  ]
}

export const getStartingStep = steps => {
  for (let stepCount = 0; stepCount < steps.length; stepCount++) {
    let currentStep = steps[stepCount]
    if (!currentStep.isValid) {
      return currentStep.path
    }
  }
  return false
}
