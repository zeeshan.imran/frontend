const listToString = list => list.join(', ')

export default listToString
