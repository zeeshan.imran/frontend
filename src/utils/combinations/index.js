import { factorial } from '../factorial/index'

export const combinations = objects => {
  if (objects === 0) return 0
  if (objects === 1) return 1
  return factorial(objects) / (2 * factorial(objects - 2))
}
