const isPowerUser = user => user && user.type === 'analytics'

export default isPowerUser
