import i18n from './internationalization/i18n'

/*
 * getErrorMessage(error, defaultMessage, i18nScope)
 * return:
 *   t({{i18nScope}}.{{RETURN_CODE}}) if error is GraphQLError
 *   t({{i18nScope}}.{{defaultMessage}}) if error is not GraphQLError
 */
const getErrorMessage = (error, defaultMessage, { prefix = '' } = {}) => {
  if (error.graphQLErrors && error.graphQLErrors.length > 0) {
    return (
      i18n.t(`errors.${prefix}${error.graphQLErrors[0].message}`, {
        defaultValue: null
      }) || i18n.t(`errors.${prefix}${defaultMessage}`)
    )
  }

  return i18n.t(`errors.${prefix}${defaultMessage}`)
}

export default getErrorMessage
