import React from 'react'
import { withRouter } from 'react-router-dom'
import EditPdfLayout from '../../containers/EditPdfLayout'

export const EditOperatorPdfLayoutPage = ({ match }) => {
  const { surveyId, jobGroupId } = match.params
  return <EditPdfLayout templateName="operator" surveyId={surveyId} jobGroupId={jobGroupId} />
}

export default withRouter(EditOperatorPdfLayoutPage)
