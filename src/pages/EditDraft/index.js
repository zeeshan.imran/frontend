import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import OperatorCreateSurveyProducts from '../OperatorCreateSurveyProducts'
import OperatorCreateSurveyBasics from '../OperatorCreateSurveyBasics'
import OperatorCreateTastingNotes from '../OperatorCreateTastingNotes'
import OperatorCreateSurveySettings from '../OperatorCreateSurveySettings'
import OperatorCreateSurveyQuestions from '../OperatorCreateSurveyQuestions'
import OperatorSurveyDraftTabBar from '../../containers/OperatorSurveyDraftTabBar'
import OperatorCreateSurveyPayments from '../OperatorCreateSurveyPayments'
import OperatorCreateSurveyLinkedSurveys from '../OperatorCreateSurveyLinkedSurveys'
import { Page, PageContent } from './styles'
import OperatorCreateSurveyEmailSettings from '../OperatorCreateSurveyEmailSettings'

const EditDraft = ({ match: { path, url } }) => (
  <React.Fragment>
    <Route path={`${path}/:step`} component={OperatorSurveyDraftTabBar} />
    <Page>
      <PageContent>
        <Switch>
          <Redirect exact path={`${path}`} to={`${url}/basics`} />
          <Redirect exact path={`${path}/`} to={`${url}/basics`} />
          <Route
            path={`${path}/basics`}
            component={props => (
              <OperatorCreateSurveyBasics {...props} isDraft />
            )}
          />
          <Route
            path={`${path}/emails`}
            component={OperatorCreateSurveyEmailSettings}
          />
          <Route
            path={`${path}/settings`}
            component={props => (
              <OperatorCreateSurveySettings {...props} editing />
            )}
          />
          <Route
            path={`${path}/tasting-notes`}
            component={OperatorCreateTastingNotes}
          />
          <Route
            path={`${path}/products`}
            component={OperatorCreateSurveyProducts}
          />
          <Route
            path={`${path}/questions`}
            component={OperatorCreateSurveyQuestions}
          />
          <Route
            path={`${path}/financial`}
            component={OperatorCreateSurveyPayments}
          />
          <Route
            path={`${path}/linked-surveys`}
            component={OperatorCreateSurveyLinkedSurveys}
          />
        </Switch>
      </PageContent>
    </Page>
  </React.Fragment>
)

export default EditDraft
