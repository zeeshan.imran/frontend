import styled from 'styled-components'
import colors from '../../utils/Colors'
import { DEFAULT_MOBILE_PADDING } from '../../utils/Metrics'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 100%;
  background-color: ${colors.WHITE};
`

export const PageContent = styled.div`
  display: flex;
  flex-direction: ${({ desktop }) => (desktop ? 'row' : 'column')};
  padding-left: ${({ desktop }) =>
    desktop ? 0 : `${DEFAULT_MOBILE_PADDING}rem`};
  padding-right: ${({ desktop }) =>
    desktop ? 0 : `${DEFAULT_MOBILE_PADDING}rem`};
  align-items: center;
  justify-content: center;
  height: 100%;
`

export const Image = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
`

export const Logo = styled(Image)`
  width: 18.5rem;
  height: 3.6rem;
  margin-left: ${({ desktop }) =>
    desktop ? '5rem' : `${DEFAULT_MOBILE_PADDING}rem`};
  margin-top: 2.3rem;
  cursor: pointer;
`
