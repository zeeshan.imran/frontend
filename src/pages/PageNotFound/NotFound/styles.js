import styled from 'styled-components'
import { family } from '../../../utils/Fonts'
import Text from '../../../components/Text'
import colors from '../../../utils/Colors'
import { Image } from '../styles'

export const Illustration = styled(Image)`
  max-width: 100%;
  width: 44.3rem;
  height: 33.8rem;
  margin-right: ${({ desktop }) => (desktop ? '10rem' : 0)};
`

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: ${({ desktop }) => (desktop ? 'left' : 'center')};
`

export const StyledText = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: ${colors.BLACK};
  opacity: 0.65;
`

export const ErrorNumber = styled(StyledText)`
  font-family: ${family.primaryBold};
  font-size: 11rem;
`
