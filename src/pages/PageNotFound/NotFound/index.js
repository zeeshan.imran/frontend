import React from 'react'
import { Illustration, TextContainer, ErrorNumber, StyledText } from './styles'
import ErrorIllustration from '../../../assets/svg/illus_error.svg'
import { useTranslation } from 'react-i18next';

const NotFound = ({ desktop, history }) => {
  const{t} =useTranslation();
  return (
    <React.Fragment>
      <Illustration desktop={desktop} src={ErrorIllustration} />
      <TextContainer desktop={desktop}>
        <ErrorNumber>404</ErrorNumber>
        <StyledText>{t('containers.page.notFound.styledText')}</StyledText>
      </TextContainer>
    </React.Fragment>
  )
}
export default NotFound
