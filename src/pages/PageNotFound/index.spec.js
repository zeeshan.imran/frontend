import React from 'react'
import { mount } from 'enzyme'
import PageNotFound from '.'
import history from '../../history'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: text => text }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PageNotFound', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PageNotFound', () => {
    testRender = mount(<PageNotFound desktop={''} history={history} />)

    expect(testRender).toMatchSnapshot()
  })
})
