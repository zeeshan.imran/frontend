import React from 'react'
import { storiesOf } from '@storybook/react'
import PageNotFound from '.'

storiesOf('PageNotFound', module).add('Page Not Found', () => (
  <PageNotFound>STORYBOOK</PageNotFound>
))
