import React from 'react'
import { Desktop } from '../../components/Responsive'
import { Container, Logo, PageContent } from './styles'
import NotFound from './NotFound'
import { getImages } from '../../utils/getImages'
import { imagesCollection } from '../../assets/png'
const { desktopImage } = getImages(imagesCollection)

const PageNotFound = ({ history }) => (
  <Desktop>
    {desktop => {
      return (
        <Container desktop={desktop}>
          <Logo
            onClick={() => history.push('/')}
            desktop={desktop}
            src={desktopImage}
          />
          <PageContent desktop={desktop}>
            <NotFound desktop={desktop} history={history} />
          </PageContent>
        </Container>
      )
    }}
  </Desktop>
)

export default PageNotFound
