import React from 'react'
import Select from '../../../components/Select'
import { Input } from 'antd'

import {
  Container,
  RightContainerMain,
  LeftContainerMain,
  RightContainer,
  LeftContainer,
  Title,
  SubTitle,
  ClearFloats,
  SearchBarContainer
} from './styles'
import { withTranslation } from 'react-i18next'

const PhotoValidationHeaderSection = ({
  title,
  filters,
  selectedFilter,
  setSelectedFilter,
  setSearchedEmail,
  filterCounts,
  t
}) => (
  <Container>
    <LeftContainerMain>
      <Title>{title}</Title>
    </LeftContainerMain>

    <RightContainerMain>
      <LeftContainer>
        <SubTitle>Filter By</SubTitle>
      </LeftContainer>
      <RightContainer>
        <Select
          name='photo-filter'
          value={selectedFilter}
          options={filters}
          optionFilterProp='children'
          getOptionName={filter => filter.name}
          getOptionValue={filter => filter.code}
          onChange={filter => setSelectedFilter(filter)}
          size={`default`}
        />
      </RightContainer>
      <ClearFloats />
    </RightContainerMain>
    <ClearFloats />
    <SearchBarContainer>
      <Input.Search
        placeholder={t(`components.surveyValidate.filters.email`)}
        onSearch={value => setSearchedEmail(value)}
        enterButton
      />
    </SearchBarContainer>
    <SearchBarContainer>
      <SubTitle>
        {t(`components.surveyValidate.filters.total`, {
          records: filterCounts
        })}
      </SubTitle>
    </SearchBarContainer>
    
  </Container>
)

export default withTranslation()(PhotoValidationHeaderSection)
