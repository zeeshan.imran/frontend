import React, { useState } from 'react'
import { useQuery } from 'react-apollo-hooks'
import { useTranslation } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { SURVEY_QUERY } from '../../queries/Survey'
import { Page, PageContent, Marginer } from './styles'
import LoadingModal from '../../components/LoadingModal'
import PhotoValidationHeaderSection from './PhotoValidationHeaderSection'
import PhotoHub from '../../components/PhotoHub'

import { PHOTO_ANSWER_FILTERS } from '../../utils/Constants'

const PhotoValidateSurvey = ({ match, history }) => {
  const { t } = useTranslation()
  const surveyId = match.params.surveyId

  // if no survey ID present
  if (!surveyId) {
    history.push('/')
    return null
  }

  const [selectedFilter, setSelectedFilter] = useState('not_processed')
  const [searchedEmail, setSearchedEmail] = useState('')
  const [filterCounts, setFilterCounts] = useState(0)

  const {
    data: { survey },
    loading
  } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId }
  })
  if (loading) {
    return <LoadingModal visible />
  }
  return (
    <React.Fragment>
      <Marginer />
      <Page>
        <PageContent>
          {survey ? (
            <React.Fragment>
              <PhotoValidationHeaderSection
                title={t(`components.surveyPhotos.mainHeader`, {
                  surveyName: survey.name
                })}
                filters={PHOTO_ANSWER_FILTERS}
                selectedFilter={selectedFilter}
                setSelectedFilter={setSelectedFilter}
                setSearchedEmail={setSearchedEmail}
                filterCounts={filterCounts}
              />
              <PhotoHub
                survey={survey}
                searchedEmail={searchedEmail}
                selectedFilter={selectedFilter}
                setFilterCounts={setFilterCounts}
                filterCounts={filterCounts}
              />
            </React.Fragment>
          ) : null}
        </PageContent>
      </Page>
    </React.Fragment>
  )
}

export default withRouter(PhotoValidateSurvey)
