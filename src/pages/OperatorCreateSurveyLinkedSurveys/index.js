import React from 'react'
import OperatorSection from '../../components/OperatorSection'
import OperatorSurveyLinkedSurveys from '../../containers/OperatorSurveyLinkedSurveys'
import { useTranslation } from 'react-i18next'

const OperatorCreateSurveyLinkedSurveys = ({ match }) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <OperatorSection
        title={t('containers.page.operatorCreateSurveyLinkedSurveys.title')}
      />
      <OperatorSurveyLinkedSurveys
        surveyId={match.params && match.params.surveyId}
      />
    </React.Fragment>
  )
}

export default OperatorCreateSurveyLinkedSurveys
