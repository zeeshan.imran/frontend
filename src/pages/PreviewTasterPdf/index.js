import React from 'react'
import { withRouter } from 'react-router-dom'
import PreviewTasterPdf from '../../containers/PreviewTasterPdf'

export const PreviewTasterPdfPage = ({ match }) => {
  const { surveyId, jobGroupId } = match.params
  return <PreviewTasterPdf surveyId={surveyId} jobGroupId={jobGroupId} />
}

export default withRouter(PreviewTasterPdfPage)
