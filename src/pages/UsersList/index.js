import React from 'react'
import UsersListContainer from '../../containers/UsersList'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'

const UsersList = ({ ...props }) => {
  return (
    <OperatorPage>
      <OperatorPageContent>
        <UsersListContainer {...props} />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default UsersList
