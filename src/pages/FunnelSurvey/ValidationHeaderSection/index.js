import React from 'react'
import { Container, Title } from './styles'

const PhotoValidationHeaderSection = ({ title }) => (
  <Container>
    <Title>{title}</Title>
  </Container>
)

export default PhotoValidationHeaderSection
