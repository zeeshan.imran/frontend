import React from 'react'
import { useQuery } from 'react-apollo-hooks'
import { useTranslation } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { SURVEY_QUERY } from '../../queries/Survey'
import { Page, PageContent, Marginer } from './styles'
import ValidationHeaderSection from './ValidationHeaderSection'
import FunnelHub from '../../components/FunnelHub'
import LoadingModal from '../../components/LoadingModal'

const FunnelSurvey = ({ match, history }) => {
  const { t } = useTranslation()
  const surveyId = match.params.surveyId

  // if no survey ID present
  if (!surveyId) {
    history.push('/')
    return null
  }

  const {
    data: { survey },
    loading
  } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId }
  })
  if (loading) {
    return <LoadingModal visible />
  }
  return (
    <React.Fragment>
      <Marginer />
      <Page>
        <PageContent>
          {survey ? (
            <React.Fragment>
              <ValidationHeaderSection
                title={t(`components.surveyFunnel.mainHeader`, {
                  surveyName: survey.name
                })}
              />
              <FunnelHub survey={survey} />
            </React.Fragment>
          ) : null}
        </PageContent>
      </Page>
    </React.Fragment>
  )
}

export default withRouter(FunnelSurvey)
