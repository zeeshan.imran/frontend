import React from 'react'
import { Col, Row } from 'antd'
import Input from '../../../components/Input'
import { FormItem } from './styles'
import { getFormItemStatus } from '../../../utils/getFormItemStatus'
import { useTranslation } from 'react-i18next';

const WelcomeForm = ({ email, password, getChangeHandler, errors }) => {
  const {t} = useTranslation();
  const {
    email: emailErrors = '',
    password: passwordErrors = '',
    confirm: confirmErrors = ''
  } = errors

  return (
    <React.Fragment>
      <Row>
        <Col xs={{ span: 24 }}>
          <FormItem
            validateStatus={getFormItemStatus(emailErrors)}
            help={emailErrors}
          >
            <Input
              name={'email'}
              placeholder={t('containers.page.welcomeForm.emailPlaceholder')}
              label={t('containers.page.welcomeForm.emailLabel')}
              required
              value={email}
              onChange={getChangeHandler('email')}
            />
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col xs={{ span: 24 }}>
          <FormItem
            validateStatus={getFormItemStatus(passwordErrors)}
            help={passwordErrors}
          >
            <Input
              required
              name='password'
              placeholder={t('containers.page.welcomeForm.passwordPlceholder')}
              label={t('containers.page.welcomeForm.passwordLabel')}
              type={'password'}
              value={password}
              onChange={getChangeHandler('password')}
            />
          </FormItem>
        </Col>
      </Row>
      <Row>
        <Col xs={{ span: 24 }}>
          <FormItem
            validateStatus={getFormItemStatus(confirmErrors)}
            help={confirmErrors}
          >
            <Input
              required
              name='confirm'
              placeholder={t('containers.page.welcomeForm.confirmPlaceholder')}
              label={t('containers.page.welcomeForm.confirmLabel')}
              type={'password'}
              onChange={getChangeHandler('confirm')}
            />
          </FormItem>
        </Col>
      </Row>
    </React.Fragment>
  )
}

export default WelcomeForm
