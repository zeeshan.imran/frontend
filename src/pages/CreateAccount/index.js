import React from 'react'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import RegisterLayout from '../../components/RegisterLayout'
import WelcomeForm from '../../containers/CreateAccount/WelcomeForm'
import PersonalInformation from '../../containers/CreateAccount/PersonalInformation'
import AdditionalInformation from '../../containers/CreateAccount/AdditionalInformation'
import { getPathFromLocation } from '../../utils/getPathFromLocation/'
import SIGN_UP_FORM_QUERY from '../../queries/signUpForm'
import { Query } from 'react-apollo'
import i18n from '../../utils/internationalization/i18n'

export const STEPS = [
  {
    title:  i18n.t('containers.page.createAccount.welcomeStep.title'),
    description: i18n.t('containers.page.createAccount.welcomeStep.description'),
    path: 'welcome'
  },
  {
    title: i18n.t('containers.page.createAccount.personalInformationStep.title'),
    description: i18n.t('containers.page.createAccount.personalInformationStep.description'),
    path: 'personal-information'
  },
  {
    title: i18n.t('containers.page.createAccount.additionalInformartionStep.title'),
    description: i18n.t('containers.page.createAccount.additionalInformartionStep.description'),
    path: 'additional-information'
  }
]

const CreateAccount = ({ match, location }) => (
  <Query query={SIGN_UP_FORM_QUERY}>
    {({
      data: {
        signUpForm: { firstStepValid }
      }
    }) => {

      return (
        <RegisterLayout
          currentPath={getPathFromLocation(location)}
          steps={STEPS}
          content={
            <Switch>
              <Redirect exact from={match.path} to={`${match.path}/welcome`} />

              <Route
                path={`${match.path}/welcome`}
                render={props => <WelcomeForm {...props} />}
              />
              <Route
                path={`${match.path}/personal-information`}
                render={props => {
                  if (firstStepValid) {
                    return <PersonalInformation {...props} />
                  }
                  return <Redirect to={`${match.path}/welcome`} />
                }}
              />
              <Route
                path={`${match.path}/additional-information`}
                component={AdditionalInformation}
              />
            </Switch>
          }
        />
      )
    }}
  </Query>
)

export default withRouter(CreateAccount)
