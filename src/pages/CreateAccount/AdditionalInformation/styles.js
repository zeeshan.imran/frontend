import styled from 'styled-components'
import { Form } from 'antd'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const CustomForm = styled(Form)``

export const FormItem = Form.Item

export const SectionTitle = styled.span`
  font-family: ${family.primaryLight};
  font-size: 2rem;
  line-height: 1.6;
  letter-spacing: normal;
  color: ${colors.BLACK};
  margin-bottom: 0.6rem;
`

export const SideBySideContainer = styled.div`
  display: flex;
  width: 100%;
  flex-direction: row;
  height: 100%;
`
