import React, { Component } from 'react'
import { Col, Row } from 'antd'
import { Query } from 'react-apollo'
import { isEmpty } from 'ramda'
import Select from '../../../components/Select'
import {
  CURRENCIES,
  BINARY_RESPONSES,
  CHILDREN_AGES
} from '../../../utils/Data'
import { SectionTitle, FormItem } from './styles'
import INCOME_RANGES_QUERY from '../../../queries/incomeRanges'
import Input from '../../../components/Input'
import {
  getOptionValue,
  getOptionName
} from '../../../utils/getters/OptionGetters'
import { getFormItemStatus } from '../../../utils/getFormItemStatus'
import getBase64 from '../../../utils/getBase64'
import UploadPictureWithButton from '../../../components/UploadPictureWithButton'
import { withTranslation } from 'react-i18next';

class AdditionalInformation extends Component {
  state = {
    imageUrl: ''
  }

  handleUpload = file => {
    const { uploadPicture, userId } = this.props
    uploadPicture({ variables: { photo: file, id: userId } })
  }

  handleBeforeUpload = async file => {
    const imageUrl = await getBase64(file)
    this.setState({ imageUrl }, () => this.handleUpload(file))
  }

  render () {
    const {
      getChangeHandler,
      getOptionChangeHandler,
      getChildrenAgesChangeHandler,
      ethnicity,
      ethnicities,
      currency,
      incomeRange,
      hasChildren,
      numberOfChildren,
      childrenAges,
      smokerKinds,
      smokerKind,
      productCategories,
      productTypes,
      dietaryPreferences,
      dietaryRestrictions,
      errors,
      t
    } = this.props

    const {
      numberOfChildren: numberOfChildrenErrors,
      incomeRange: incomeRangeErrors,
      childrenAges: childrenAgesErrors
    } = errors

    const shouldRenderChildrenAgesDropdown =
      hasChildren === 'Yes' && numberOfChildren > 0 && !numberOfChildrenErrors

    const { imageUrl } = this.state

    return (
      <Query query={INCOME_RANGES_QUERY} variables={{ currency }}>
        {({ loading, error, data: { incomeRanges } }) => {
          return (
            <React.Fragment>
              <Row>
                <Col xs={{ span: 24 }}>
                  <UploadPictureWithButton
                    desktop
                    imageUrl={imageUrl}
                    handleBeforeUpload={this.handleBeforeUpload}
                  />
                </Col>
              </Row>
              <Row type='flex'>
                <SectionTitle>{t('containers.page.additionalInfo.ethnicity')}</SectionTitle>
              </Row>
              <Row>
                <Col xs={{ span: 24 }}>
                  <FormItem>
                    <Select
                      name={'ethnicity'}
                      value={ethnicity}
                      getOptionValue={getOptionValue}
                      getOptionName={getOptionName}
                      onChange={getOptionChangeHandler('ethnicity')}
                      options={ethnicities}
                      placeholder={t('containers.page.additionalInfo.ethnicityPlaceholder')}
                    />
                  </FormItem>
                </Col>
              </Row>
              <Row type='flex'>
                <SectionTitle>{t('containers.page.additionalInfo.income')}</SectionTitle>
              </Row>
              <Row type='flex' gutter={24}>
                <Col xs={{ span: 12 }}>
                  <FormItem>
                    <Select
                      name={'currency'}
                      value={currency}
                      onChange={getOptionChangeHandler('currency')}
                      options={CURRENCIES}
                      placeholder={t('containers.page.additionalInfo.incomePlaceholder')}
                    />
                  </FormItem>
                </Col>
                <Col xs={{ span: 12 }}>
                  <FormItem
                    validateStatus={getFormItemStatus(incomeRangeErrors)}
                    help={incomeRangeErrors}
                  >
                    <Select
                      name={'incomeRange'}
                      value={incomeRange}
                      getOptionValue={getOptionValue}
                      getOptionName={getOptionName}
                      onChange={getOptionChangeHandler('incomeRange')}
                      disabled={isEmpty(incomeRanges)}
                      options={incomeRanges}
                      placeholder={t('containers.page.additionalInfo.incomeRangePlaceholder')}
                    />
                  </FormItem>
                </Col>
              </Row>
              <Row type='flex'>
                <SectionTitle>{t('containers.page.additionalInfo.childAtHome')}</SectionTitle>
              </Row>
              <Row type='flex' gutter={24}>
                <Col xs={{ span: 12 }}>
                  <FormItem>
                    <Select
                      name={'hasChildren'}
                      value={hasChildren}
                      onChange={getOptionChangeHandler('hasChildren')}
                      options={BINARY_RESPONSES}
                      placeholder={t('containers.page.additionalInfo.childAtHomePlaceholder')}
                    />
                  </FormItem>
                </Col>
                <Col xs={{ span: 12 }}>
                  <FormItem
                    validateStatus={getFormItemStatus(
                      hasChildren === 'Yes' && numberOfChildrenErrors
                    )}
                    help={hasChildren === 'Yes' && numberOfChildrenErrors}
                  >
                    <Input
                      disabled={hasChildren === 'No'}
                      name={'numberOfChildren'}
                      value={
                        numberOfChildren === 0 ? undefined : numberOfChildren
                      }
                      placeholder={t('containers.page.additionalInfo.numberChildPlaceholder')}
                      onChange={getChangeHandler('numberOfChildren')}
                    />
                  </FormItem>
                </Col>
              </Row>
              {shouldRenderChildrenAgesDropdown && (
                <Row gutter={24}>
                  {[...new Array(parseInt(numberOfChildren, 10))].map(
                    (_, index) => (
                      <Col key={index} xs={{ span: 6 }}>
                        <FormItem
                          validateStatus={getFormItemStatus(
                            !childrenAges[index]
                          )}
                          help={!childrenAges[index] && childrenAgesErrors}
                        >
                          <Select
                            value={childrenAges[index]}
                            label={t('containers.page.additionalInfo.childAgeLabel')}
                            name={`children-age-${index}`}
                            onChange={getChildrenAgesChangeHandler(index)}
                            options={CHILDREN_AGES}
                            placeholder={t('containers.page.additionalInfo.childAgePlaceholder')}
                          />
                        </FormItem>
                      </Col>
                    )
                  )}
                </Row>
              )}
              <Row type='flex'>
                <SectionTitle>{t('containers.page.additionalInfo.smoke')}</SectionTitle>
              </Row>
              <Row>
                <Col xs={{ span: 24 }}>
                  <FormItem>
                    <Select
                      name={'smokerKind'}
                      value={smokerKind}
                      getOptionValue={getOptionValue}
                      getOptionName={getOptionName}
                      onChange={getOptionChangeHandler('smokerKind')}
                      options={smokerKinds}
                      placeholder={t('containers.page.additionalInfo.smokePlaceholder')}
                    />
                  </FormItem>
                </Col>
              </Row>
              <Row type='flex'>
                <SectionTitle>{t('containers.page.additionalInfo.product')}</SectionTitle>
              </Row>
              <Row>
                <Col xs={{ span: 24 }}>
                  <FormItem>
                    <Select
                      name={'productTypes'}
                      value={productTypes}
                      mode={'multiple'}
                      getOptionValue={getOptionValue}
                      getOptionName={getOptionName}
                      onChange={getOptionChangeHandler('productTypes')}
                      options={productCategories}
                      placeholder={t('containers.page.additionalInfo.productPlaceholder')}
                    />
                  </FormItem>
                </Col>
              </Row>
              <Row type='flex'>
                <SectionTitle>{t('containers.page.additionalInfo.dietary')}</SectionTitle>
              </Row>
              <Row>
                <Col xs={{ span: 24 }}>
                  <FormItem>
                    <Select
                      name={'dietaryRestrictions'}
                      value={dietaryRestrictions}
                      getOptionValue={getOptionValue}
                      getOptionName={getOptionName}
                      onChange={getOptionChangeHandler('dietaryRestrictions')}
                      options={dietaryPreferences}
                      placeholder={t('containers.page.additionalInfo.dietaryPlaceholder')}
                    />
                  </FormItem>
                </Col>
              </Row>
            </React.Fragment>
          )
        }}
      </Query>
    )
  }
}

export default withTranslation()(AdditionalInformation)
