import React from 'react'
import { mount } from 'enzyme'
import CreateAccount from './index'
import WelcomeForm from '../../containers/CreateAccount/WelcomeForm'
import PersonalInformation from '../../containers/CreateAccount/PersonalInformation'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo'
import SIGN_UP_FORM_QUERY from '../../queries/signUpForm'
import wait from '../../utils/testUtils/waait'
import { Router, Route, Redirect } from 'react-router-dom'

jest.mock('../../containers/CreateAccount/WelcomeForm', () => ({
  __esModule: true,
  default: jest.fn(() => null) // null component
}))
jest.mock('../../containers/CreateAccount/PersonalInformation', () => ({
  __esModule: true,
  default: jest.fn(() => null) // null component
}))

jest.mock('react-router-dom', () => ({
  ...require.requireActual('react-router-dom'),
  Redirect: jest.fn(() => null)
}))

describe('CreateAccount', () => {
  let testRender
  let client
  let history

  const mockGraphQLCache = overwrite => {
    client.cache.writeQuery({
      query: SIGN_UP_FORM_QUERY,
      data: {
        signUpForm: {
          firstStepValid: true,
          secondStepValid: true,
          lastStepValid: true,
          userId: 'user-1',
          email: 'test@flaworwiki.com',
          password: '123456',
          firstname: 'firstname',
          lastname: 'lastname',
          birthday: null,
          gender: 'male',
          language: 'en',
          ethnicity: 'ethnicity',
          currency: '$',
          incomeRange: null,
          hasChildren: null,
          numberOfChildren: null,
          childrenAges: null,
          smokerKind: null,
          productTypes: null,
          dietaryRestrictions: null,
          recaptchaResponseToken: null,
          ...overwrite
        }
      }
    })
  }

  beforeEach(() => {
    client = createApolloMockClient()
  })

  beforeEach(() => {
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '/request-account/welcome' },
      replace: jest.fn()
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render of create-account', async () => {
    mockGraphQLCache()
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <Route path='/request-account' component={CreateAccount} />
        </Router>
      </ApolloProvider>
    )
    await wait(0)

    expect(testRender.find(CreateAccount)).toHaveLength(1)
    expect(WelcomeForm).toHaveBeenCalled()
  })

  test('should render of PersonalInformation', async () => {
    history.location.pathname = '/request-account/personal-information'
    mockGraphQLCache()
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <Route path='/request-account' component={CreateAccount} />
        </Router>
      </ApolloProvider>
    )
    await wait(0)

    expect(testRender.find(CreateAccount)).toHaveLength(1)
    expect(PersonalInformation).toHaveBeenCalled()
  })

  test('should Redirect to welcome', async () => {
    history.location.pathname = '/request-account/personal-information'
    mockGraphQLCache({ firstStepValid: false })
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <Route path='/request-account' component={CreateAccount} />
        </Router>
      </ApolloProvider>
    )
    await wait(0)

    expect(testRender.find(CreateAccount)).toHaveLength(1)
    expect(Redirect).toHaveBeenCalledWith(
      { to: '/request-account/welcome' },
      {}
    )
  })
})
