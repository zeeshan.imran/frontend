import React, { Component } from 'react'
import { Col, Row } from 'antd'
import ReCAPTCHA from 'react-google-recaptcha'
import Input from '../../../components/Input'
import { Desktop } from '../../../components/Responsive'
import DatePicker from '../../../components/DatePicker'
import Select from '../../../components/Select'
import { FormItem } from './styles'
import {
  getOptionName,
  getOptionValue
} from '../../../utils/getters/OptionGetters'
import { getFormItemStatus } from '../../../utils/getFormItemStatus'
import moment from 'moment'
import { withTranslation } from 'react-i18next'

class PersonalInformation extends Component {
  render () {
    const {
      getBirthdayChangeHandler,
      getOptionChangeHandler,
      getChangeHandler,
      firstname,
      lastname,
      birthday,
      gender,
      language,
      genders,
      languages,
      errors,
      t
    } = this.props
    const {
      firstname: firstnameErrors,
      lastname: lastnameErrors,
      birthday: birthdayErrors,
      recaptchaVerification: recaptchaVerificationErrors
    } = errors

    return (
      <Desktop>
        {desktop => {
          return (
            <React.Fragment>
              <Row gutter={24}>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <FormItem
                    validateStatus={getFormItemStatus(firstnameErrors)}
                    help={firstnameErrors}
                  >
                    <Input
                      required
                      name={'firstname'}
                      value={firstname}
                      placeholder={t('containers.page.personalInfo.firstnamePlaceholder')}
                      label={t('containers.page.personalInfo.firstnameLabel')}
                      onChange={getChangeHandler('firstname')}
                    />
                  </FormItem>
                </Col>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <FormItem
                    validateStatus={getFormItemStatus(lastnameErrors)}
                    help={lastnameErrors}
                  >
                    <Input
                      required
                      name={'lastname'}
                      value={lastname}
                      placeholder={t('containers.page.personalInfo.lastnamePlaceholder')}
                      label={t('containers.page.personalInfo.lastnameLabel')}
                      onChange={getChangeHandler('lastname')}
                    />
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <FormItem
                    validateStatus={getFormItemStatus(birthdayErrors)}
                    help={birthdayErrors}
                  >
                    <DatePicker
                      type={'date'}
                      value={birthday}
                      label={t('containers.page.personalInfo.dobLabel')}
                      required
                      name={'birthday'}
                      onChange={getBirthdayChangeHandler('birthday')}
                      disabledDate={currentDate => currentDate > moment()}
                    />
                  </FormItem>
                </Col>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <FormItem>
                    <Select
                      name={'gender'}
                      options={genders}
                      label={t('containers.page.personalInfo.genderLabel')}
                      getOptionName={getOptionName}
                      getOptionValue={getOptionValue}
                      value={gender}
                      placeholder={t('containers.page.personalInfo.genderPlaceholder')}
                      onChange={getOptionChangeHandler('gender')}
                    />
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <FormItem>
                    <Select
                      options={languages}
                      label={t('containers.page.personalInfo.languagesLabel')}
                      placeholder={t('containers.page.personalInfo.languagesPlaceholder')}
                      getOptionName={getOptionName}
                      getOptionValue={getOptionValue}
                      showSearch
                      name={'language'}
                      value={language}
                      onChange={getOptionChangeHandler('language')}
                    />
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                  <FormItem
                    validateStatus={getFormItemStatus(
                      recaptchaVerificationErrors
                    )}
                    help={recaptchaVerificationErrors}
                  >
                    <ReCAPTCHA
                      name={'recaptchaResponseToken'}
                      ref='recaptcha'
                      sitekey={process.env.REACT_APP_GOOGLE_RECAPTCHA_KEY}
                      onChange={getOptionChangeHandler(
                        'recaptchaResponseToken'
                      )}
                    />
                  </FormItem>
                </Col>
              </Row>
            </React.Fragment>
          )
        }}
      </Desktop>
    )
  }
}

export default withTranslation()(PersonalInformation)
