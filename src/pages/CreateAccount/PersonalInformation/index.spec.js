import React from 'react'
import { mount, shallow } from 'enzyme'
import PersonalInformation from '.'
import moment from 'moment'

jest.mock('../../../components/DatePicker', () => ({ disabledDate }) => {
  disabledDate('')
  return <div />
})
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))
describe('PersonalInformation', () => {
  let testRender
  let getBirthdayChangeHandler
  let getOptionChangeHandler
  let getChangeHandler
  let firstname
  let lastname
  let birthday
  let gender
  let language
  let genders
  let languages
  let errors
  
  beforeEach(() => {
    getBirthdayChangeHandler = jest.fn()
    getOptionChangeHandler = jest.fn()
    getChangeHandler = jest.fn()
    firstname = 'firstname'
    lastname = 'lastname'
    birthday = moment('1993-10-01')
    gender = 'male'
    language = 'eng'
    genders = ['male', 'female']
    languages = ['eng', 'fr']
    errors = []
  })
  afterEach(() => {
    testRender.unmount()
  })
  test('should render PersonalInformation', () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 1024,
      height: 800
    })
    testRender = shallow(
      <PersonalInformation
        getBirthdayChangeHandler={getBirthdayChangeHandler}
        getOptionChangeHandler={getOptionChangeHandler}
        getChangeHandler={getChangeHandler}
        firstname={firstname}
        lastname={lastname}
        birthday={birthday}
        gender={gender}
        language={language}
        genders={genders}
        languages={languages}
        errors={errors}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
  test('should render PersonalInformation birthday field', () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 1024,
      height: 900
    })
    testRender = mount(
      <PersonalInformation
        getBirthdayChangeHandler={getBirthdayChangeHandler}
        getChangeHandler={getChangeHandler}
        getOptionChangeHandler={getOptionChangeHandler}
        birthday={moment('2020-02-01')}
        errors={errors}
      />
    )
  })
})
