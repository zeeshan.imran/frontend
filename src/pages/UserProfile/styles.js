import styled from 'styled-components'
import Text from '../../components/Text'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const BaseContainer = styled.div`
  display: flex;
  width: 100%;
  margin: 0 auto;
  padding: 13.2rem 21.5rem;
`
export const Container = styled.div`
  background-color: #fff;
  width: 100%;
`

export const TitleContainer = styled.div`
  margin-bottom: 3.5rem;
`

export const Title = styled(Text)`
  font-family: ${family.primaryLight};
  font-size: 5.2rem;
  font-weight: 300;
  line-height: 1.24;

  color: ${colors.GENERAL_TITLE_COLOR};

  margin: 0;
  margin-bottom: 1.5rem;
`
export const SubTitle = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 3.2rem;
  color: #8a8a8a;
  display: flex;
`
export const TitleIconContainer = styled.div`
  font-size: 2rem;
`
export const TitleText = styled.span`
  font-size: 1.4rem;
  line-height: 2.2rem;
`
export const TabTitleContainer = styled.div`
  width: fit-content;
`
export const TabContainer = styled.div`
  width: 100%;
`
export const TableContainer = styled.div`
  width: 100%;
`
export const SingleEntry = styled.div`
  width: 100%;
  height: 10rem;
  border: 1px solid #e8e8e8;
  border-right: 0;
  border-left: 0;
  padding: 1rem;
`
export const IconArea = styled.div`
  width: 3.5rem;
  height: 3.5rem;
`

export const FieldContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  height: 10rem
`
export const IconContainer = styled.div`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  font-size: 35px;
  color: ${colors.LIGHT_OLIVE_GREEN}
`
export const FieldTitleContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`
export const ActionButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const PointsContainer = styled.div`
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  padding: 0.9rem 1.5rem;
  background-color: ${colors.LIGHT_OLIVE_GREEN};
  border-radius: 1rem
`

export const PointsFieldText = styled(Text)`
  font-size: 1.8rem;
  font-family: ${family.primaryRegular};
  line-height: 3.2rem;
  letter-spacing: 0.033rem;
  color: ${colors.WHITE};
  white-space: pre-wrap;
`

export const SurveyNameContainer = styled.div`
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  flex-wrap: wrap;
  padding-left: 50px
`

export const FieldText = styled(Text)`
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  line-height: 1.6rem;
  letter-spacing: 0.05rem;
  color: rgb(171, 171, 182);
  user-select: none;
  white-space: pre-wrap;
  flex-direction: row;
  flex-basis: 100%
`

export const DateFieldText = styled(Text)`
  font-size: 1.3rem;
  font-family: ${family.primaryRegular};
  line-height: 1.6rem;
  letter-spacing: 0.046rem;
  color: rgb(205, 205, 210);
  user-select: none;
  white-space: pre-wrap;
  flex-direction: row;
  flex-basis: 100%;
  margin-top: 1rem;
`

export const SurveyName = styled(FieldText)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 2.4rem;
  letter-spacing: 0.044rem;
  color: rgb(78, 78, 86);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  flex-direction: row;
  flex-basis: 100%;
  @media screen and (max-device-width: 1400px) {
    max-width: 40vw;
  }
  @media screen and (min-device-width: 1401px) {
    max-width: 500px;
  }
`
