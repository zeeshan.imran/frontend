import React from 'react'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import SurveyQuestionLayout from '../../containers/SurveyQuestionLayout'
import SurveyQuestionContainer from '../../containers/SurveyQuestion'

const SurveyQuestion = ({
  match: {
    params: { stepId, surveyId }
  }
}) => {
  return (
    <React.Fragment>
      <AuthenticatedLayout mergeNavbarToContent>
        <SurveyQuestionLayout questionId={stepId} surveyId={surveyId}>
          <SurveyQuestionContainer questionId={stepId} surveyId={surveyId} />
        </SurveyQuestionLayout>
      </AuthenticatedLayout>
    </React.Fragment>
  )
}

export default SurveyQuestion
