import React from 'react'
import { mount } from 'enzyme'
import TermsOfUsePage from '.'
import history from '../../history'

describe('TermsOfUsePage', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render TermsOfUsePage', () => {
    testRender = mount(<TermsOfUsePage history={history} />)

    expect(testRender).toMatchSnapshot()
  })
})
