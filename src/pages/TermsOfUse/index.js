import React from 'react'
import TermsOfUseContainer from '../../containers/TermsOfUse'

const TermsOfUsePage = ({ history }) => {
  return <TermsOfUseContainer history={history} />
}

export default TermsOfUsePage