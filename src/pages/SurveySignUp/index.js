import React from 'react'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import { Content } from './styles'
import SurveySignUpContainer from '../../containers/SurveySignUp'

const SurveySignUp = ({
  match: {
    params: { surveyId }
  }
}) => {
  return (
    <AuthenticatedLayout mergeNavbarToContent>
      <Content>
        <SurveySignUpContainer surveyId={surveyId} />
      </Content>
    </AuthenticatedLayout>
  )
}

export default SurveySignUp
