import styled from 'styled-components'
import { DEFAULT_MOBILE_PADDING } from '../../utils/Metrics'

export const Content = styled.div`
  padding: ${({ desktop }) =>
    desktop ? '0' : `5rem ${DEFAULT_MOBILE_PADDING}rem`};
`
