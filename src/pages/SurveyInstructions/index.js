import React from 'react'
import SurveyInstructionsContainer from '../../containers/SurveyInstructions'

const SurveyInstructions = ({
  match: {
    params: { surveyId }
  }
}) => <SurveyInstructionsContainer surveyId={surveyId} />

export default SurveyInstructions
