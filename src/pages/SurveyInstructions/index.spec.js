import React from 'react'
import { mount } from 'enzyme'
import SurveyInstructions from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

describe('SurveyInstructions', () => {
  let testRender
  let historyMock
  let client

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
    client = createApolloMockClient()
  })
  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyInstructions', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <SurveyInstructions match={{ params: { surveyId: 'survey-1' } }} />
        </Router>
      </ApolloProvider>
    )

    expect(testRender).toMatchSnapshot()
  })
})
