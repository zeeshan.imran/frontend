import React from 'react'
import SurveyNotAuthorizedContainer from '../../containers/SurveyNotAuthorized'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'

const SurveyNotAuthorized = () => {
  return (
    <OperatorPage>
      <OperatorPageContent>
        <SurveyNotAuthorizedContainer />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default SurveyNotAuthorized
