import React from 'react'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import ProfileLayout from '../../containers/ProfileLayout'

const MyProfile = () => (
  <AuthenticatedLayout>
    <ProfileLayout />
  </AuthenticatedLayout>
)

export default MyProfile
