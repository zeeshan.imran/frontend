import React from 'react'
import { mount } from 'enzyme'
import ChangePassword from '.'

jest.mock('../../../containers/ChangePassword')
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('ChangePassword', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ChangePassword', () => {
    testRender = mount(<ChangePassword />)
    expect(testRender).toMatchSnapshot() 
  })
})
