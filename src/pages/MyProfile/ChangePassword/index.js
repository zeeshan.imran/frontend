import React from 'react'
import ChangePasswordContainer from '../../../containers/ChangePassword'

const ChangePassword = props => <ChangePasswordContainer {...props} />

export default ChangePassword
