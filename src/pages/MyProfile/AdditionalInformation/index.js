import React from 'react'
import AdditionalInformationContainer from '../../../containers/AdditionalInformation'

const AdditionalInformation = props => (
  <AdditionalInformationContainer {...props} />
)

export default AdditionalInformation
