import React from 'react'
import { mount } from 'enzyme'
import PaymentDetails from '.'

jest.mock('../../../containers/PaymentDetails')
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PaymentDetails', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PaymentDetails', () => {
    testRender = mount(<PaymentDetails />)
    expect(testRender).toMatchSnapshot()
  })
})
