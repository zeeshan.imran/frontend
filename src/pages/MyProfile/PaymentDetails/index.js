import React from 'react'
import PaymentDetailsContainer from '../../../containers/PaymentDetails'

const PaymentDetails = props => <PaymentDetailsContainer {...props} />

export default PaymentDetails
