import React from 'react'
import { mount } from 'enzyme'
import Privacy from '.'
import { BrowserRouter as Router } from 'react-router-dom'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

jest.mock('../../../containers/Privacy')
describe('Privacy', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Privacy', () => {
    testRender = mount(
      <Router>
        <Privacy />
      </Router>
    )
    expect(testRender).toMatchSnapshot()
  })
})
