import React from 'react'
import PrivacyContainer from '../../../containers/Privacy'

const Privacy = props => <PrivacyContainer {...props} />

export default Privacy
