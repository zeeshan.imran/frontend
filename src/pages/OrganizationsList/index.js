import React from 'react'
import OrganizationsListContainer from '../../containers//OrganizationsList'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'

const OrganizationsList = ({ ...props }) => {
  return (
    <OperatorPage>
      <OperatorPageContent>
        <OrganizationsListContainer {...props} />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default OrganizationsList
