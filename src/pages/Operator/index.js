import React, { useState, useEffect, useMemo } from 'react'
import { Switch, withRouter } from 'react-router-dom'
import OperatorRoute from '../../containers/OperatorRoute'
import OperatorLayout from '../../components/OperatorLayout'
import ActiveSurveys from '../../containers/OperatorActiveSurveys'
import Surveys from '../../containers/OperatorSurveys'
import QrCodes from '../../containers/OperatorQrCodes'
import CreateQrCode from '../../containers/OperatorCreateQrCode'
import EditQrCode from '../../containers/OperatorEditQrCode'
import LoadSurvey from '../../containers/LoadSurvey'
import CreateSurvey from '../CreateSurvey'
import EditSurvey from '../EditSurvey'
import SurveyNotAuthorized from '../SurveyNotAuthorized'
import EditDraft from '../EditDraft'
import PhotoValidateSurvey from '../PhotoValidateSurvey'
import FunnelSurvey from '../FunnelSurvey'
import TermsOfUse from '../../containers/TermsOfUse'
import PrivacyPolicy from '../../containers/PrivacyPolicy'
import UsersList from '../UsersList'
import OrganizationsList from '../OrganizationsList'

import { Container } from './styles'
import {
  getAuthenticatedUser,
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsSuperAdmin,
  isUserAuthenticatedAsAnalytics
} from '../../utils/userAuthentication'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'

import SurveyStatus from '../../containers/SurveyStats'
import { useTranslation } from 'react-i18next'
import useUpdateLocale from '../../hooks/useUpdateSurveyLanguage'
import Download from '../Download'
import OperatorSurveyAnalysis from '../../containers/OperatorSurveyAnalysis'

const getActiveTabKey = (history, menu) => {
  const path = history.location.pathname.replace('/operator/', '').split('/')
  const menuItem = menu.find(el => el.path === path[0] || el.alias === path[0])
  if (menuItem && menuItem.submenu) {
    const subMenuItem = menuItem.submenu.find(
      el => el.path === path[0] + '/' + path[1]
    )
    return subMenuItem ? subMenuItem.path : ''
  } else {
    return menuItem ? menuItem.path : ''
  }
}

const Operator = ({ match, history }) => {
  const { t } = useTranslation()
  const [username, setUsername] = useState('')
  const [user, setUser] = useState(getAuthenticatedUser())
  const updateLocale = useUpdateLocale()

  useEffect(() => {
    updateLocale('en')
  })

  if (
    (isUserAuthenticatedAsOperator() ||
      isUserAuthenticatedAsAnalytics() ||
      isUserAuthenticatedAsPowerUser() ||
      isUserAuthenticatedAsSuperAdmin()) &&
    user &&
    username !== user.fullName
  ) {
    setUsername(user.fullName)
  }

  const updateUserData = e => {
    const newAuthenticatedUsed = getAuthenticatedUser()
    if (newAuthenticatedUsed !== user) {
      setUser(newAuthenticatedUsed)
      if (
        (isUserAuthenticatedAsOperator() ||
          isUserAuthenticatedAsAnalytics() ||
          isUserAuthenticatedAsPowerUser() ||
          isUserAuthenticatedAsSuperAdmin()) &&
        user
      ) {
        setUsername(user.fullName)
      }
    }
  }

  PubSub.subscribe(PUBSUB.UPDATE_OPERATOR_DROPDOWN, updateUserData)

  const isOperator = isUserAuthenticatedAsOperator()
  const isPowerUser = isUserAuthenticatedAsPowerUser()
  const isAdmin = isUserAuthenticatedAsSuperAdmin()

  const menu = useMemo(
    () =>
      [
        {
          icon: 'dashboard',
          title: t('containers.page.operator.menu.dashboard'),
          path: 'dashboard',
          allowed: true
        },
        {
          icon: 'survey_icon',
          title: t('containers.page.operator.menu.surveys'),
          path: 'surveys',
          alias: 'survey',
          allowed: true
        },
        {
          icon: 'qrcode',
          title: t('containers.page.operator.menu.qr-codes'),
          path: 'qr-codes',
          allowed: isAdmin || isOperator || isPowerUser
        },
        {
          icon: 'user',
          title: t('containers.page.operator.menu.users'),
          path: 'users',
          allowed: isAdmin || isPowerUser
        },
        {
          icon: 'bank',
          title: t('containers.page.operator.menu.organizations'),
          path: 'organizations',
          allowed: isAdmin
        },
        {
          icon: 'terms_of_use',
          title: t('containers.page.operator.menu.termsOfUse'),
          path: 'terms-of-use',
          allowed: true
        },
        {
          icon: 'privacy_policy',
          title: t('containers.page.operator.menu.privacyPolicy'),
          path: 'privacy-policy',
          allowed: true
        }
      ].filter(({ allowed }) => allowed),
    [t, isAdmin, isOperator, isPowerUser]
  )

  const [isReloaded, setisReloaded] = useState(true)
  const path = history.location.pathname
  const regexTab = /\/(basics|tasting-notes|emails|products|questions|financial|settings|linked-surveys)$/g

  if (isReloaded && regexTab.test(path)) {
    setisReloaded(false)
    const newPath = path.replace(regexTab, '')
    history.push(newPath)
  }

  return (
    <Container>
      <OperatorLayout
        menu={menu}
        activeMenuEntry={getActiveTabKey(history, menu)}
        handleMenuEntryClick={path => history.push(`${match.path}/${path}`)}
        username={username}
        hasTasterAccount={user.isTaster}
      >
        <Switch>
          <OperatorRoute
            exact
            path={`${match.path}/dashboard`}
            component={ActiveSurveys}
          />
          <OperatorRoute
            path={`${match.path}/analysis/:surveyId`}
            component={OperatorSurveyAnalysis}
          />
          <OperatorRoute
            exact
            path={`${match.path}/surveys`}
            component={Surveys}
          />
          <OperatorRoute
            path={`${match.path}/not-authorized`}
            component={SurveyNotAuthorized}
          />
          <OperatorRoute
            path={`${match.path}/stats/:surveyId/:tabId`}
            component={SurveyStatus}
          />
          <OperatorRoute
            path={`${match.path}/stats/:surveyId`}
            component={SurveyStatus}
          />
          <OperatorRoute
            exact
            path={`${match.path}/survey/edit/:surveyId`}
            component={LoadSurvey}
          />
          <OperatorRoute
            exact
            path={`${match.path}/survey/stricted-edit/:surveyId`}
            component={LoadSurvey}
            stricted
          />
          <OperatorRoute
            exact
            path={`${match.path}/draft/edit/:surveyId`}
            component={LoadSurvey}
          />
          <OperatorRoute
            path={`${match.path}/survey/edit/:surveyId`}
            component={EditSurvey}
          />
          <OperatorRoute
            path={`${match.path}/survey/stricted-edit/:surveyId`}
            component={EditSurvey}
            stricted
          />
          <OperatorRoute
            path={`${match.path}/draft/edit/:surveyId`}
            component={EditDraft}
          />
          <OperatorRoute
            exact
            path={`${match.path}/survey/photos/:surveyId`}
            component={PhotoValidateSurvey}
          />
          <OperatorRoute
            exact
            path={`${match.path}/survey/funnel/:surveyId`}
            component={FunnelSurvey}
          />
          <OperatorRoute
            path={`${match.path}/survey/create`}
            component={CreateSurvey}
          />
          <OperatorRoute path={`${match.path}/users`} component={UsersList} />
          <OperatorRoute
            path={`${match.path}/organizations`}
            component={OrganizationsList}
          />
          <OperatorRoute
            exact
            path={`${match.path}/qr-codes`}
            component={QrCodes}
          />
          <OperatorRoute
            exact
            path={`${match.path}/qr-codes/create`}
            component={CreateQrCode}
          />
          <OperatorRoute
            exact
            path={`${match.path}/qr-codes/:qrCodeId`}
            component={EditQrCode}
          />
          <OperatorRoute
            path={`${match.path}/terms-of-use`}
            component={TermsOfUse}
          />
          <OperatorRoute
            path={`${match.path}/privacy-policy`}
            component={PrivacyPolicy}
          />
          <OperatorRoute
            exact
            path={`${match.path}/download/:id`}
            component={Download}
          />
        </Switch>
      </OperatorLayout>
    </Container>
  )
}

export default withRouter(Operator)
