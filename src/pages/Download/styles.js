import styled from 'styled-components'
import Text from '../../components/Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: block;
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
  min-height: 200px;
`

export const Title = styled(Text)`
  display: block;
  font-size: 2rem;
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.85);
`

export const SubTitle = styled(Text)`
  display: block;
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  color: rgba(0, 0, 0, 0.85);
`

export const ButtonGroup = styled.div`
  text-align: center;
  button {
    margin: 0.5rem 0.25rem;
  }
`

export const LinkGroup = styled.div`
  margin: 3rem 0 1rem 0;
  text-align: center;
  svg {
    width: 2rem;
    height: 2rem;
  }
`

export const Link = styled.a`
  color: #444;
  font-size: 1.8rem;
  cursor: pointer;
`

export const ErrorMessage = styled(Text)`
  display: block;
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  color: #900;
  margin-bottom: 2rem;
`
