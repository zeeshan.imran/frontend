import React from 'react'
import PrivacyPolicyContainer from '../../containers/PrivacyPolicy'

const PrivacyPolicyPage = ({ history }) => {
  return <PrivacyPolicyContainer history={history} />
}

export default PrivacyPolicyPage