import React from 'react'
import { shallow } from 'enzyme'
import PrivacyPolicyPage from '.'
import history from '../../history'

describe('PrivacyPolicyPage', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PrivacyPolicyPage', () => {
    testRender = shallow(<PrivacyPolicyPage history={history} />)

    expect(testRender).toMatchSnapshot() 
  })
})
