import React from 'react'
import OperatorSection from '../../components/OperatorSection'
import OperatorSurveyCreateEmailSettings from '../../containers/OperatorSurveyCreateEmailSettings'
import { useTranslation } from 'react-i18next';

const OperatorCreateSurveyEmailSettings = ({ match }) => {
  const{t} = useTranslation();
  return (
    <React.Fragment>
      <OperatorSection title={t('containers.page.operatorCreateEmailSettings.title')} />
      <OperatorSurveyCreateEmailSettings
        surveyId={match.params && match.params.surveyId}
      />
    </React.Fragment>
  )
}

export default OperatorCreateSurveyEmailSettings
