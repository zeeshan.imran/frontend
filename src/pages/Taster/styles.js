import styled from 'styled-components'
import Text from '../../components/Text'
import { family } from '../../utils/Fonts'
import { components } from '../../utils/Metrics'
import colors from '../../utils/Colors'

export const BaseContainer = styled.div`
  display: flex;
  padding: 2rem 0;
  width: 100%;
  min-height: calc(100vh - ${components.NAVBAR_HEIGHT}rem);
  margin: 0 auto;
  background-color: #f0f2f5;
`
export const Container = styled.div`
  padding: 2.4rem 3.2rem 3.2rem;
  width: 70%;
  ${'' /* max-width: 120rem; */}
  margin: 0 auto;
  background-color: #fff;
`

export const TitleContainer = styled.div`
  margin-bottom: 3rem;
`

export const Title = styled(Text)`
  font-family: ${family.primaryBold};
  font-size: 2rem;
  color: rgba(0, 0, 0, 0.85);
  display: flex;
`
export const SubTitle = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 3.2rem;
  color: #8a8a8a;
  display: flex;
`
export const TasterWrapper = styled.div`
  padding: 8px 16px;
  margin-bottom: 10px;
  background: ${colors.TASTER_WRAPPER_COLOR};
  color: #fff;

  .ant-btn-link {
    padding: 0 4px;
    background: transparent;
    border: none;
    color: ${colors.TASTER_LINK_COLOR};
    :hover > span {
      text-decoration: underline;
    }
  }
`
