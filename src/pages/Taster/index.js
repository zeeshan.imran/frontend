import React from 'react'
import { Switch } from 'react-router-dom'
import path from 'path'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { Button } from 'antd'
import TasterExclusiveSurveysList from '../../containers/TasterExclusiveSurveysList'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import TasterRoute from '../../components/TasterRoute'
import {
  Container,
  BaseContainer,
  Title,
  SubTitle,
  TitleContainer,
  TasterWrapper
} from './styles'
import { withTranslation } from 'react-i18next'
import { getImpersonateUserId } from '../../utils/userAuthentication'

const Taster = ({ match, t, history }) => {
  const isImpersonating = !!getImpersonateUserId()
  const { data } = useQuery(QUERY_IMPERSONATE_USER, {
    fetchPolicy: 'cache-and-network',
    skip: !isImpersonating
  })
  return (
    <AuthenticatedLayout mergeNavbarToContent>
      <BaseContainer>
        <Container>
          <Switch>
            <TasterRoute
              path={path.normalize(`${match.path}/`)}
              component={() => (
                <React.Fragment>
                  {data && data.adminUser && data.impersonateUser ? (
                    <TasterWrapper>
                      {t(`components.impersonate.impersonatedAs`, {
                        user:
                          data.adminUser.fullName ||
                          data.adminUser.emailAddress,
                        targetUser:
                          data.impersonateUser.fullName ||
                          data.impersonateUser.emailAddress
                      })}
                      <Button
                        type='link'
                        onClick={() => history.push('/impersonate/exit')}
                      >
                        {t('components.impersonate.toGoToAdmin')}
                      </Button>
                    </TasterWrapper>
                  ) : null}
                  <TitleContainer>
                    <Title>{t('containers.page.taster.title')}</Title>
                    <SubTitle>
                      <span
                        dangerouslySetInnerHTML={{
                          __html: t('containers.page.taster.promt')
                        }}
                      />
                    </SubTitle>
                  </TitleContainer>
                  <TasterExclusiveSurveysList />
                </React.Fragment>
              )}
            />
          </Switch>
        </Container>
      </BaseContainer>
    </AuthenticatedLayout>
  )
}

export const QUERY_IMPERSONATE_USER = gql`
  query impersonateUser {
    impersonateUser: me {
      id
      fullName
      emailAddress
    }
    adminUser: me(impersonate: false) {
      id
      fullName
      emailAddress
    }
  }
`

export default withTranslation()(Taster)
