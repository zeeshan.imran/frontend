import React from 'react'
import { mount } from 'enzyme'
import SurveyScreeningCompleted from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { defaultSurvey } from '../../mocks'

jest.mock('../../containers/SurveyScreeningCompleted', () => ({ children }) => (
  <div>children</div>
))

const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    name: 'Survey 1',
    uniqueName: 'survey-1',
  },
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ]
}

describe('SurveyScreeningCompleted', () => {
  let testRender
  let history
  let client

  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyScreeningCompleted', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SurveyScreeningCompleted
          match={{ params: { surveyId: 'survey-1' } }}
          history={history}
        />
      </ApolloProvider>
    )

    expect(testRender).toMatchSnapshot()
  })

  //   test('should render SurveyScreeningCompleted', async () => {
  //     testRender = mount(
  //       <SurveyScreeningCompleted
  //         match={{ params: { stepId: 'stepid-1', surveyId: 'survey-1' } }}
  //       />
  //     )
  //     await wait(500)
  //     expect(SurveyScreeningCompleted).toHaveLength(1)

  //     // const submitValue = {
  //     //   stepId: 'stepid-1'
  //     // }

  //     // const onSubmit = testRender.find(SurveyQuestion).props('onSubmit')
  //     // // onSubmit.onSubmit(submitValue)
  //   })
})
