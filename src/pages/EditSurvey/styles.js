import styled from 'styled-components'

export const Page = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  flex: 1;
`

export const PageContent = styled.div`
  flex: 1;
  padding-left: 2.5rem;
  padding-right: 2.5rem;
  padding-bottom: 10rem;
  margin: 0 auto;
  width: 100%;
`
