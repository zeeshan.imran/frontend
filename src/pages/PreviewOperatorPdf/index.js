import React from 'react'
import { withRouter } from 'react-router-dom'
import PreviewOperatorPdf from '../../containers/PreviewOperatorPdf'

export const PreviewOperatorPdfPage = ({ match }) => {
  const { surveyId, jobGroupId } = match.params
  return <PreviewOperatorPdf surveyId={surveyId} jobGroupId={jobGroupId} />
}

export default withRouter(PreviewOperatorPdfPage)
