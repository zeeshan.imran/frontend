import React from 'react'
import OperatorSection from '../../components/OperatorSection'
import OperatorSurveyCreatePayments from '../../containers/OperatorSurveyCreatePayments'
import { useTranslation } from 'react-i18next';

const OperatorCreateSurveyPayments = ({ match, stricted=false }) => {
  const{t} = useTranslation();
  return (
    <React.Fragment>
      <OperatorSection title={t('containers.page.operatorCreatePayments.title')} />
      <OperatorSurveyCreatePayments
        surveyId={match.params && match.params.surveyId}
        stricted={stricted}
      />
    </React.Fragment>
  )
}

export default OperatorCreateSurveyPayments
