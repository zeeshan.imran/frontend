import React from 'react'
import { mount } from 'enzyme'
import SurveyScreening from '.'
import wait from '../../utils/testUtils/waait'

jest.mock('../../containers/SurveyQuestion', () => ({ onSubmit }) => onSubmit())
jest.mock('../../containers/SurveyQuestionLayout', () => ({ children }) =>
  children
)
describe('SurveyScreening', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })
  
  test('should render SurveyScreening', async () => {
    testRender = mount(
      <SurveyScreening
        match={{ params: { stepId: 'stepid-1', surveyId: 'survey-1' } }}
      />
    )
    await wait(500)
    expect(testRender).toMatchSnapshot()
  })
})
