import React from 'react'
import SurveyRejectionContainer from '../../containers/SurveyRejection'

const SurveyRejection = ({
  match: {
    params: { surveyId },
    url
  }
}) => <SurveyRejectionContainer surveyId={surveyId} />

export default SurveyRejection
