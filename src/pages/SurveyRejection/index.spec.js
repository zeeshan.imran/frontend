import React from 'react'
import { mount } from 'enzyme'
import SurveyRejection from '.'
import { Router } from 'react-router-dom'
import history from '../../history'

import gql from 'graphql-tag'
import defaults from '../../defaults'
import SurveyRejectionContainer from '../../containers/SurveyRejection'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'

jest.mock('../../containers/SurveyRejection', () => () => {
  return <div />
})

const mockFinishSurvey = {
  request: {
    query: gql`
      mutation finishSurvey($input: EnrollmentStateInput) {
        finishSurvey(input: $input)
      }
    `,
    variables: { input: { surveyEnrollment: 'survey-enrollment-1' } }
  },
  result: () => {
    return true
  }
}

describe('SurveyRejection', () => {
  let testRender
  let client

  beforeEach(() => {
    client = createApolloMockClient({
      mocks: [mockFinishSurvey]
    })
    client.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyEnrollmentId: 'survey-enrollment-1'
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render without crash', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <SurveyRejection match={{ params: { surveyId: 1 }, url: '' }} />
        </Router>
      </ApolloProvider>
    )

    expect(testRender.find(SurveyRejectionContainer)).toHaveLength(1)
  })
})
