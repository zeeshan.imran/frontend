import styled from 'styled-components'
import { Form } from 'antd'
import BaseText from '../../components/Text'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'
import {
  LOGIN_MOBILE_MARGIN,
  DOUBLE_MOBILE_MARGIN,
  DEFAULT_COMPONENTS_MARGIN
} from '../../utils/Metrics'

export const AlignerBottomNav = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  ${({ desktop, loggedInUser }) =>
    desktop
      ? 'padding: 0 0 2rem'
      : loggedInUser
        ? 'padding: 0rem 0 10rem'
        : 'padding: 2rem 0 10rem'};
`

export const StyledTitle = styled.p`
  font-family: ${family.primaryLight};
  font-size: ${({ desktop }) => (desktop ? '4.2rem' : `3.2rem`)};
  line-height: 1.24;
  letter-spacing: normal;
  color: ${colors.BLACK};
  margin-bottom: 1.5rem;
  text-align: left;
`
export const SubTitleText = styled.span`
  font-family: ${family.primaryLight};
  color: ${colors.BLACK};
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
  user-select: none;
  opacity: 0.7;
`

export const StyledSubTitle = styled(SubTitleText)`
  font-size: 1.8rem;
  white-space: pre-line;
`

export const Aligner = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  overflow-y: auto;
`

export const Container = styled.div`
  width: 37rem;
  max-width: calc(100vw - ${DOUBLE_MOBILE_MARGIN}rem);
  ${({ desktop }) => (desktop ? 'padding-top: 15rem' : '')};
`
export const ContainerWithIncreaseWidth = styled.div`
  width: 65rem;
  max-width: calc(100vw - ${DOUBLE_MOBILE_MARGIN}rem);
  ${({ desktop, loggedInUser }) =>
    desktop
      ? 'padding: 15rem 0 0 0'
      : loggedInUser
        ? 'padding: 10rem 0 0 0'
        : 'padding: 1rem 0 0 0'};
`
export const ContainerWithIncreaseWidthNoPadding = styled.div`
  width: 65rem;
  max-width: calc(100vw - ${DOUBLE_MOBILE_MARGIN}rem);
`

export const Logo = styled.div`
  position: ${({ desktop }) => (desktop ? 'absolute' : 'relative')};
  background: url(${props => props.src}) no-repeat center;
  background-size: cover;
  width: 18.5rem;
  height: 3.6rem;
  left: ${({ desktop }) => (desktop ? '5rem' : `${LOGIN_MOBILE_MARGIN}rem`)};
  top: ${({ desktop }) => (desktop ? '2.3rem' : `0`)};
  bottom: ${({ desktop }) => (desktop ? '20%' : '0')};
  ${({ desktop }) => (desktop ? '' : 'align-self: flex-start')};
  ${({ desktop }) =>
    desktop ? '' : `margin-bottom: ${LOGIN_MOBILE_MARGIN}rem`};

  &:hover {
    cursor: pointer;
  }
`

export const CustomForm = styled(Form)``

export const FormItem = Form.Item

export const SideBySideLayout = styled.div`
  display: flex;
  flex-direction: row;
  width: 100vw;
  height: 100vh;
  ${({ desktop, loggedInUser }) =>
    desktop ? '' : loggedInUser ? `padding-top: 0rem` : `padding-top: 2rem`};
`

export const Side = styled.div`
  flex: 1;
  min-width: 50%;
  height: 100%;
  overflow-y: auto;
`

export const Text = styled(BaseText)`
  display: flex;
  font-family: ${family.primaryLight};
  color: ${colors.BLACK};
  user-select: none;
`

export const GreyText = styled(Text)`
  font-size: ${({ desktop }) => (desktop ? 1.8 : 1.4)}rem;
  line-height: 1.56;
  opacity: 0.7;
`

export const StepsText = styled(GreyText)`
  margin-bottom: 3rem;
`
export const ErrorText = styled.div`
  font-size: 2rem;
  padding-bottom: 2rem;
  text-align: center;
`
