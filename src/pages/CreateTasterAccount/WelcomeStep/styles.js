import styled from 'styled-components'
import { Form } from 'antd'
import BaseText from '../../../components/Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const CustomForm = styled(Form)``

export const FormItem = Form.Item

export const Text = styled(BaseText)`
  display: flex;
  font-family: ${family.primaryLight};
  color: ${colors.BLACK};
  user-select: none;
`

export const GreyText = styled(Text)`
  font-size: ${({ desktop }) => (desktop ? 1.8 : 1.4)}rem;
  line-height: 1.56;
  opacity: 0.7;
`

export const StepsText = styled(GreyText)`
  margin-bottom: 3rem;
`