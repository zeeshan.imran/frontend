import React from 'react'
import {StyledTitle, StyledSubTitle} from '../styles'
import WelcomeForm from '../../../containers/CreateTasterAccount/WelcomeForm'

const WelcomeStep = ({ step, setForm, currentStep, desktop }) => {
  return (
    <React.Fragment>
      <StyledTitle desktop={desktop}>{step.title}</StyledTitle>
      <StyledSubTitle desktop={desktop}>{step.description}</StyledSubTitle>
      <WelcomeForm
        setForm={setForm}
        stepNumber={currentStep}
        initialValid={step.isValid}
        initialValues={step.formData}
      />
    </React.Fragment>
  )
}

export default WelcomeStep
