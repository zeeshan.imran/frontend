import React from 'react'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import { Content } from './styles'
import SurveyLoginContainer from '../../containers/SurveyLogin'

const SurveyLogin = ({
  match: {
    params: { surveyId }
  }
}) => {
  return (
    <AuthenticatedLayout mergeNavbarToContent>
      <Content>
        <SurveyLoginContainer surveyId={surveyId} />
      </Content>
    </AuthenticatedLayout>
  )
}

export default SurveyLogin
