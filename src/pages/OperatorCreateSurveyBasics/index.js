import React from 'react'
import OperatorSection from '../../components/OperatorSection'
import OperatorSurveyCreateBasicInfo from '../../containers/OperatorSurveyCreateBasicInfo'
import { useTranslation } from 'react-i18next'

const OperatorCreateSurveyBasics = ({ match, isDraft = false }) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <OperatorSection
        title={t('containers.page.operatorCreateBasics.title')}
      />
      <OperatorSurveyCreateBasicInfo
        surveyId={match.params && match.params.surveyId}
        isDraft={isDraft}
      />
    </React.Fragment>
  )
}

export default OperatorCreateSurveyBasics
