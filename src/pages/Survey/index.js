import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import PageNotFound from '../PageNotFound'
import SurveyInstructions from '../SurveyInstructions'
import SurveyQuestion from '../SurveyQuestion'
import SurveyScreeningStep from '../SurveyScreeningStep'
import SurveyPaymentStep from '../SurveyPaymentStep'
import SurveyScreeningCompleted from '../SurveyScreeningCompleted'
import SurveyCompleted from '../SurveyCompleted'
import SurveyRejection from '../SurveyRejection'
import SurveyLogin from '../SurveyLogin'
import SurveySignUp from '../SurveySignUp'
import InSurveyRoute from '../../containers/InSurveyRoute'
import SurveyDemo from '../../containers/SurveyDemo'

const Survey = ({
  location,
  match: {
    params: { surveyId }
  }
}) => (
  <React.Fragment>
    <Switch>
      <Route path='/survey/demo/:demoName' component={SurveyDemo} />
      <Redirect
        exact
        path='/survey/:surveyId'
        to={{
          pathname: '/survey/:surveyId/login',
          search: location.search || ''
        }}
      />
      <Route exact path='/survey/:surveyId/login' component={SurveyLogin} />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/instructions'
        component={SurveyInstructions}
      />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/screening/:stepId'
        component={SurveyScreeningStep}
      />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/screening-completed'
        component={SurveyScreeningCompleted}
      />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/payments/:stepId'
        component={SurveyPaymentStep}
      />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/tasting/:stepId'
        component={SurveyQuestion}
      />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/question/:stepId'
        component={SurveyQuestion}
      />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/survey-signup'
        component={SurveySignUp}
      />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/finished'
        component={SurveyCompleted}
      />
      <InSurveyRoute
        exact
        path='/survey/:surveyId/rejected'
        component={SurveyRejection}
      />
      <Route component={PageNotFound} />
    </Switch>
  </React.Fragment>
)

export default Survey
