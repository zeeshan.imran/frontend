import React from 'react'
import { shallow } from 'enzyme'
import SurveyCompleted from '.'

describe('SurveyCompleted', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyCompleted', () => {
    testRender = shallow(
      <SurveyCompleted match={{ params: { surveyId: 'survey-1' } }} />
    )

    expect(testRender).toMatchSnapshot()
  })
})
