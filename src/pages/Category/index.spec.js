import React from 'react'
import { mount } from 'enzyme'
import Category from '.'

jest.mock('../../containers/CategoryRedirect', () => () => {
  return <div />
})

describe('Category', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Category', () => {
    testRender = mount(
      <Category match={{ params: { categoryName: 'cate-1' } }} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
