import React from 'react'
import { shallow } from 'enzyme'
import Home from '.'

describe('Home', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Home', () => {
    testRender = shallow(<Home />)
    expect(testRender).toMatchSnapshot()
  })
})
