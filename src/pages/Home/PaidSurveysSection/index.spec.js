import React from 'react'
import { shallow } from 'enzyme'
import PaidSurveysSection from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PaidSurveysSection', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PaidSurveysSection', () => {
    testRender = shallow(<PaidSurveysSection />)

    expect(testRender).toMatchSnapshot()
  })
})
