import React from 'react'
import Section from '../Section'
import PaidSurveysGrid from '../../../containers/PaidSurveysGrid'
import { useTranslation } from 'react-i18next'

const PaidSurveysSection = () => {
  const { t } = useTranslation()
  return (
    <Section title={t('containers.page.paidSurveysSection.title')}>
      <PaidSurveysGrid />
    </Section>
  )
}

export default PaidSurveysSection
