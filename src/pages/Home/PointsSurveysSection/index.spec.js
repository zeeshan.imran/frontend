import React from 'react'
import { mount } from 'enzyme'
import PointsSurveysSection from '.'

jest.mock('../../../containers/PointsSurveysGrid', () => () => {
  return <div />
})

describe('PointsSurveysSection', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PointsSurveysSection', () => {
    testRender = mount(<PointsSurveysSection />)
    expect(testRender).toMatchSnapshot()
  })
})
