import React from 'react'
import Section from '../Section'
import PointsSurveysGrid from '../../../containers/PointsSurveysGrid'
import { useTranslation } from 'react-i18next'

const PointsSurveysSection = () => {
  const { t } = useTranslation()
  return (
    <Section dark title={t('containers.page.pointsSurveySection.title')}>
      <PointsSurveysGrid />
    </Section>
  )
}

export default PointsSurveysSection
