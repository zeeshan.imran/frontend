import React from 'react'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import PointsSurveysSection from './PointsSurveysSection'
import PaidSurveysSection from './PaidSurveysSection'
import CategoriesSection from './CategoriesSection'
import OngoingSurveysSection from './OngoingSurveysSection'

const Home = () => (
  <AuthenticatedLayout withFooter>
    <CategoriesSection />
    <OngoingSurveysSection />
    <PaidSurveysSection />
    <PointsSurveysSection />
  </AuthenticatedLayout>
)

export default Home
