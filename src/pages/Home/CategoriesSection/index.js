import React from 'react'
import Section from '../Section'
import CategoriesGrid from '../../../containers/CategoriesGrid'
import { useTranslation } from 'react-i18next'

const CategoriesSection = () => {
  const { t } = useTranslation()
  return (
    <Section title={t('containers.page.categoriesGridSection.title')}>
      <CategoriesGrid />
    </Section>
  )
}

export default CategoriesSection
