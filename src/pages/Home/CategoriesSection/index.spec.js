import React from 'react'
import { shallow } from 'enzyme'
import CategoriesSection from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))
jest.mock('../../../containers/CategoriesGrid')

describe('CategoriesSection', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CategoriesSection', () => {
    testRender = shallow(<CategoriesSection />)

    expect(testRender).toMatchSnapshot()
  })
})
