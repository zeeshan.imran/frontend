import React from 'react'
import { Desktop } from '../../../components/Responsive'
import SectionTitle from '../../../components/SectionTitle'
import PageContainer from '../../../components/PageContainer'

import { Background } from './styles'

const Section = ({ title, dark, children }) => (
  <Desktop>
    {desktop => (
      <Background desktop={desktop} dark={dark}>
        <PageContainer>
          <SectionTitle title={title} />
          {children}
        </PageContainer>
      </Background>
    )}
  </Desktop>
)

export default Section
