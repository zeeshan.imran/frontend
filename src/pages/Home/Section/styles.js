import styled from 'styled-components'
import colors from '../../../utils/Colors'

export const Background = styled.div`
  background-color: ${({ dark }) =>
    dark ? colors.DARK_BACKGROUND : colors.WHITE};
  padding: ${({ desktop }) => (desktop ? '10rem 0' : '1.25rem 0')};
  display: flex;
  flex-direction: column;
`
