import React from 'react'
import Section from '../Section'
import OngoingSurveysGrid from '../../../containers/OngoingSurveysGrid'
import { useTranslation } from 'react-i18next'

const OngoingSurveysSection = () => {
  const { t } = useTranslation()
  return (
    <Section dark title={t('containers.page.ongoingSurveysSection.title')}>
      <OngoingSurveysGrid />
    </Section>
  )
}

export default OngoingSurveysSection
