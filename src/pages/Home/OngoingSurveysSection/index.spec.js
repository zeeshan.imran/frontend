import React from 'react'
import { shallow } from 'enzyme'
import OngoingSurveysSection from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))
jest.mock('../../../containers/OngoingSurveysGrid')
describe('OngoingSurveysSection', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OngoingSurveysSection', () => {
    testRender = shallow(<OngoingSurveysSection />)

    expect(testRender).toMatchSnapshot()
  })
})
