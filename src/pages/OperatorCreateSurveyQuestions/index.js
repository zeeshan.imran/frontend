import React, { useState, useRef, useEffect } from 'react'
import OperatorSection from '../../components/OperatorSection'
import { useTranslation } from 'react-i18next'
import MandatoryQuestionsSection from '../../containers/MandatoryQuestionsToCreate'
import AddQuestionToCreate from '../../containers/AddQuestionToCreate'
import QuestionsToCreateList from '../../containers/QuestionsToCreateList'
import separateQuestionsBySection from '../../utils/separateQuestionsBySection'
import getQueryParams from '../../utils/getQueryParams'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { NEVER_REQUIRED_QUESTIONS_TYPES } from '../../utils/Constants'

const OperatorCreateSurveyQuestions = ({ location }) => {
  const { t } = useTranslation()
  const updateQuestionRequire = useMutation(UPDATE_QUESTION_REQUIRE)

  const { section } = location ? getQueryParams(location) : `payments`
  const [allToMandatory, setAllToMandatory] = useState(false)
  const [allToPicture, setAllToPicture] = useState(false)

  let {
    data: {
      surveyCreation: { questions }
    },
    refetch: refetchCurrent
  } = useQuery(SURVEY_CREATION)
  const refetch = useRef(refetchCurrent)
  useEffect(() => {
    if(refetch && refetch.current && typeof refetch.current === 'function'){
      refetch.current()
    }
  }, [questions])

  // to only check the mandatory question of the required seciton
  const settingAllToUnRequiredOnClick = value => {
    setAllToMandatory(value)
    for (let counter = 0; counter < questions.length; counter++) {
      if (section === questions[counter]['displayOn']) {
        updateQuestionRequire({
          variables: {
            questionIndex: counter,
            questionField: { required: value }
          }
        })
        if (NEVER_REQUIRED_QUESTIONS_TYPES.has(questions[counter]['type'])) {
          updateQuestionRequire({
            variables: {
              questionIndex: counter,
              questionField: { required: false }
            }
          })
        }
      }
    }
  }

  const settingAllToPictureOnClick = value => {
    setAllToPicture(value)
    for (let counter = 0; counter < questions.length; counter++) {
      if (section === questions[counter]['displayOn']) {
        updateQuestionRequire({
          variables: {
            questionIndex: counter,
            questionField: { showProductImage: value }
          }
        })
      }
    }
  }

  const unsetMandatory = () => {
    setAllToMandatory(false)
  }

  const unsetPicture = () => {
    setAllToPicture(false)
  }

  const questionData = separateQuestionsBySection(questions)

  return (
    <React.Fragment>
      <MandatoryQuestionsSection />
      <OperatorSection 
        title={t('containers.page.OperatorCreateSurveyQuestions.title')}
        showMandatoryBox={questionData[section] ? true : false}
        allToMandatory={
          questionData[section]
            ? questionData[section]['allRequired']
            : allToMandatory
        }
        settingAllToUnRequiredOnClick={settingAllToUnRequiredOnClick}
        showPictureBox={section === 'middle'}
        allToPicture = {
          questionData[section]
            ? questionData[section]['showProductImage']
            : allToPicture
        }
        settingAllToPictureOnClick={settingAllToPictureOnClick}
      />
      <QuestionsToCreateList
        allToMandatory={allToMandatory}
        questions={questions}
        unsetMandatory={unsetMandatory}
        unsetPicture={unsetPicture}
        allToPicture={allToPicture}
      />
      <AddQuestionToCreate />
    </React.Fragment>
  )
}

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      questions
    }
  }
`

const UPDATE_QUESTION_REQUIRE = gql`
  mutation($questionIndex: Number, $questionField: values) {
    updateSurveyCreationQuestion(
      questionIndex: $questionIndex
      questionField: $questionField
    ) @client
  }
`

export default OperatorCreateSurveyQuestions
