import React from 'react'
import SubCategoriesGrid from '../../containers/SubCategoriesGrid'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import { Container } from './styles'
import SectionTitle from '../../components/SectionTitle'
import startCase from 'lodash.startcase'
import capitalize from 'lodash.capitalize'
import BackNavigation from '../../containers/BackNavigation'

const SubCategory = ({
  match: {
    params: { categoryName }
  }
}) => (
  <AuthenticatedLayout withFooter>
    <Container>
      <BackNavigation />
      <SectionTitle title={capitalize(startCase(categoryName))} />
      <SubCategoriesGrid />
    </Container>
  </AuthenticatedLayout>
)

export default SubCategory
