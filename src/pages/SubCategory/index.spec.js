import React from 'react'
import { shallow } from 'enzyme'
import SubCategory from '.'

describe('SubCategory', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SubCategory', () => {
    testRender = shallow(
      <SubCategory match={{ params: { categoryName: 'cate-1' } }} />
    )

    expect(testRender).toMatchSnapshot()
  })
})
