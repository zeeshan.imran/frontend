import React from 'react'
import { mount } from 'enzyme'
import Onboarding from '.'
import { BrowserRouter as Router } from 'react-router-dom'
import { Logo } from './styles'

describe('Onboarding', () => {
  let testRender
  let history

  beforeEach(() => {
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Onboarding', () => {
    history.location = {
      pathname: '/create-account'
    }

    testRender = mount(
      <Router>
        <Onboarding />
      </Router>
    )
    expect(testRender).toMatchSnapshot()
  })

//   test('Onclick logo redirect', () => {
//     history.location = {
//       pathname: '/'
//     }

//     testRender = mount(
//       <Router>
//         <Onboarding
//           history={history}
//           match={{ params: { step: 'step-1' }, path: '/' }}
//         />
//       </Router>
//     )
//     testRender.find(Logo).simulate('click')

//     expect(history.push).toHaveBeenCalledWith('/')
//   })
})
