import React from 'react'
import { StyledTitle } from './styles'

const Title = ({ children, ...otherProps }) => (
  <StyledTitle {...otherProps}>{children}</StyledTitle>
)

export default Title
