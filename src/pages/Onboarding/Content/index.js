import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Login from '../Login'
// import RequestAccount from '../RequestAccount'
// import RequestSubmitted from '../RequestSubmitted'
import ForgotPassword from '../ForgotPassword'
import NewPassword from '../NewPassword'
import VerifyTaster from '../VerifyTaster'
import useResponsive from '../../../utils/useResponsive/index'

import { Aligner, Container, Logo } from './styles'
import { getImages } from '../../../utils/getImages'
import { imagesCollection } from '../../../assets/png'
const { desktopImage } = getImages(imagesCollection)

const Content = ({
  match: {
    params: { step },
    path
  },
  history
}) => {
  const { desktop } = useResponsive()
  return (
    <Aligner desktop={desktop}>
      <Logo
        onClick={() => history.push('/')}
        src={desktopImage}
        desktop={desktop}
      />
      <Container desktop={desktop}>
        <Switch>
          <Route exact path={`${path}/login`} component={Login} />
          {/* <Route
            exact
            path={`${path}/request-account/operator`}
            component={() => <RequestAccount userType='operator' />}
          /> */}
          {/* <Route
            exact
            path={`${path}/request-account/operator/submitted`}
            component={RequestSubmitted}
          /> */}
          {/* <Redirect
            from={`${path}/request-account`}
            to={`${path}/request-account/operator`}
          /> */}
          <Route
            exact
            path={`${path}/forgot-password`}
            component={ForgotPassword}
          />
          <Route
            exact
            path={`${path}/reset-password`}
            component={NewPassword}
          />
          <Route exact path={`${path}/verify-email`} component={VerifyTaster} />
        </Switch>
      </Container>
    </Aligner>
  )
}

export default Content
