import styled from 'styled-components'
import {
  LOGIN_MOBILE_MARGIN,
  DOUBLE_MOBILE_MARGIN
} from '../../../utils/Metrics'

export const Aligner = styled.div`
  position: relative;
  min-height: 100vh;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  overflow-y: auto;
  ${({ desktop }) => (desktop ? 'padding: 0 0 2rem' : 'padding: 2rem 0 10rem')};
`

export const Container = styled.div`
  width: 37rem;
  max-width: calc(100vw - ${DOUBLE_MOBILE_MARGIN}rem);
  ${({ desktop }) => (desktop ? 'padding-top: 15rem' : '')};
`

export const Logo = styled.div`
  position: ${({ desktop }) => (desktop ? 'absolute' : 'relative')};
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 18.5rem;
  height: 3.6rem;
  left: ${({ desktop }) => (desktop ? '5rem' : `${LOGIN_MOBILE_MARGIN}rem`)};
  top: ${({ desktop }) => (desktop ? '2.3rem' : `0`)};
  bottom: ${({ desktop }) => (desktop ? '20%' : '0')};
  ${({ desktop }) => (desktop ? '' : 'align-self: flex-start')};
  ${({ desktop }) =>
    desktop ? '' : `margin-bottom: ${LOGIN_MOBILE_MARGIN}rem`};

  &:hover {
    cursor: pointer;
  }
`
