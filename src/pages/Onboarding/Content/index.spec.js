import React from 'react'
import { mount } from 'enzyme'
import Content from '.'
import { Router } from 'react-router-dom'
import { Logo } from './styles'
import {
  setAuthenticationToken,
  setAuthenticatedUser
} from '../../../utils/userAuthentication'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

jest.mock('react-router-dom/Route', () => ({ component }) => (
  <div>{component()}</div>
))

jest.mock('../Login', () => () => <div>mocked component</div>)
jest.mock('../NewPassword', () => () => <div>mocked component</div>)
jest.mock('../RequestAccount', () => ({ children }) => (
  <div>mocked component</div>
))
jest.mock('../RequestSubmitted', () => () => <div>mocked component</div>)
jest.mock('../ForgotPassword', () => () => <div>mocked component</div>)

describe('Content', () => {
  let testRender
  let history
  let client

  beforeEach(() => {
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: 'request-account/operator' },
      replace: jest.fn()
    }
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Content', () => {
    testRender = mount(
      <Router history={history}>
        <Content
          match={{
            params: { step: 'request-account' },
            path: ''
          }}
        />
      </Router>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('Should render taster', () => {
    setAuthenticationToken('testTocken')
    setAuthenticatedUser({ type: 'operator' })

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <Content
            history={history}
            match={{
              params: { step: 'request-account' },
              path: ''
            }}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toHaveLength(1)

    testRender.find(Logo).simulate('click')
    expect(history.push).toHaveBeenCalledWith('/')
  })
})
