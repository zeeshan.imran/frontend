import React from 'react'
import Title from '../Title'
import SubTitle from '../SubTitle'
import RequestAccountForm from '../../../containers/RequestAccountForm'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'
import { useTranslation } from 'react-i18next';

const RequestAccount = ({ userType }) => {
const {t}=useTranslation();
return(
  <React.Fragment>
    <Title>{t('containers.page.onboarding.requestAccount.title')}</Title>
    <SubTitle>
      {t('containers.page.onboarding.requestAccount.subTitle')}
    </SubTitle>
    <RequestAccountForm userType={userType} />
    <TermsAndPrivacyFooter />
  </React.Fragment>
)
}
export default RequestAccount
