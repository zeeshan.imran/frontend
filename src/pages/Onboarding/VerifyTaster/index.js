import React, { useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import getQueryParams from '../../../utils/getQueryParams'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import {
  setAuthenticationToken,
  setAuthenticatedUser
} from '../../../utils/userAuthentication'

const VerifyTaster = ({ location, history }) => {
  const { token, email } = getQueryParams(location)
  const queryToken = token
  const verifyTasterMutation = useMutation(VERIFY_TASTER)

  useEffect(() => {
    const setVerifyTaster = async () => {
      const { data } = await verifyTasterMutation({
        variables: { input: { emailAddress: email, token: queryToken } }
      })
      let { verifyTaster } = data
      if (!verifyTaster.user.fullName) {
        verifyTaster.user.fullName = verifyTaster.user.emailAddress
      }
      setAuthenticatedUser(verifyTaster.user)
      setAuthenticationToken(verifyTaster.token)
      history.push('/create-account/taster')
    }
    setVerifyTaster()
  }, [])

  return <React.Fragment />
}

const VERIFY_TASTER = gql`
  mutation verifyTaster($input: VerifyTasterInput) {
    verifyTaster(input: $input) {
      user {
        id
        emailAddress
        type
      }
      token
    }
  }
`
export default withRouter(VerifyTaster)
