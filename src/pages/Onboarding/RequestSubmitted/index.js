import React from 'react'
import Title from '../Title'
import SubTitle from '../SubTitle'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'
import { useTranslation } from 'react-i18next';


const RequestSubmitted = () => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      <Title>{t('containers.page.onboarding.requestSubmitted.title')}</Title>
      <SubTitle>{t('containers.page.onboarding.requestSubmitted.subTitle')}
      </SubTitle>
      <TermsAndPrivacyFooter />
    </React.Fragment>
  )
}
export default RequestSubmitted
