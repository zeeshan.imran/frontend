import React from 'react'
import { shallow } from 'enzyme'
import RequestSubmitted from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('RequestSubmitted', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render RequestSubmitted', () => {
    testRender = shallow(<RequestSubmitted />)

    expect(testRender).toMatchSnapshot()
  })
})
