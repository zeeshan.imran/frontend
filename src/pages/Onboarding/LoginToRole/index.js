import React, { Component } from 'react'
import firstSliderImage from '../../../assets/pictures/onboarding/1.png'
import secondSliderImage from '../../../assets/pictures/onboarding/2.png'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'
import { withTranslation } from 'react-i18next'

import { PageContainer, Text, SmallText, StyledButton } from './styles'

export const RightContent = {
  slides: [
    {
      url: firstSliderImage
    },
    {
      url: secondSliderImage
    }
  ]
}

class LoginToRole extends Component {
  loginAs = role => this.props.history.push('/signup/login')

  goToHomePage = () => this.props.history.push('/signup/login')

  render () {
    const { t } = this.props
    return (
      <PageContainer>
        <Text>{t('containers.page.onboarding.loginToRole.text')}</Text>
        <StyledButton type='primary' onClick={() => this.loginAs('taster')}>
          {t('containers.page.onboarding.loginToRole.styledButtonTaster')}
        </StyledButton>
        <StyledButton type='primary' onClick={() => this.loginAs('operator')}>
          {t('containers.page.onboarding.loginToRole.styledButtonOperator')}
        </StyledButton>
        <SmallText onClick={this.goToHomePage}>
          {t('containers.page.onboarding.loginToRole.backBtn')}
        </SmallText>
        <TermsAndPrivacyFooter />
      </PageContainer>
    )
  }
}

export const Content = withTranslation()(LoginToRole)
