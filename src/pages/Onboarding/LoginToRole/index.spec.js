import React from 'react'
import { mount } from 'enzyme'
import { Content } from '.'
import { StyledButton, SmallText } from './styles'
import { BrowserRouter as Router } from 'react-router-dom'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('Content', () => {
  let testRender
  let history

  beforeEach(() => {
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render LoginToRole', () => {
    history.location = {
      pathname: '/signup/login'
    }

    testRender = mount(
      <Router>
        <Content history={history}>
          <StyledButton>Schmecker</StyledButton>
        </Content>
      </Router>
    )
    testRender
      .find(StyledButton)
      .first()
      .simulate('click')

    expect(history.push).toHaveBeenCalledWith('/signup/login')

    testRender
      .find(StyledButton)
      .last()
      .simulate('click')

    expect(history.push).toHaveBeenCalledWith('/signup/login')
  })

  test('after login go to homePage', () => {
    history.location = {
      pathname: '/signup/login'
    }

    testRender = mount(
      <Router>
        <Content history={history}>
          <StyledButton>Go back to homepage</StyledButton>
        </Content>
      </Router>
    )
    testRender
      .find(SmallText)
      .first()
      .simulate('click')

    expect(history.push).toHaveBeenCalledWith('/signup/login')
  })
})
