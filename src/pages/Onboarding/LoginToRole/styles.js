import styled from 'styled-components'
import Button from '../../../components/Button'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'
import {
  DOUBLE_MOBILE_MARGIN,
  LOGIN_LEFT_PANEL_WIDTH,
  DEFAULT_COMPONENTS_MARGIN
} from '../../../utils/Metrics'

export const PageContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: ${LOGIN_LEFT_PANEL_WIDTH}rem;
  max-width: calc(100vw - ${DOUBLE_MOBILE_MARGIN}rem);
  margin-top: 1.5rem;
`

export const Text = styled.span`
  font-family: ${family.primaryLight};
  font-size: 1.8rem;
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
  user-select: none;
  color: ${colors.BLACK};
  opacity: 0.7;
`

export const SmallText = styled(Text)`
  font-size: 1.4rem;
  opacity: 0.85;
  text-align: center;
  cursor: pointer;
  user-select: none;

  :hover {
    color: ${colors.LIGHT_OLIVE_GREEN};
  }
`

export const StyledButton = styled(Button)`
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
`
