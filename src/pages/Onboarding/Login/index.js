import React from 'react'
import { Redirect, withRouter } from 'react-router-dom'
import LoginForm from '../../../containers/LoginForm'
import Title from '../Title'
import {
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsTaster,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsAnalytics
} from '../../../utils/userAuthentication'
import { ONBOARD_ROOT_PATH } from '../../../utils/Constants'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'
import { useTranslation } from 'react-i18next'

const Login = ({ history }) => {
  const { t } = useTranslation()

  if (isUserAuthenticatedAsOperator()) {
    return <Redirect to='/operator' />
  }

  if (isUserAuthenticatedAsAnalytics()) {
    return <Redirect to='/operator' />
  }

  if (isUserAuthenticatedAsPowerUser()) {
    return <Redirect to='/operator' />
  }

  if (isUserAuthenticatedAsTaster()) {
    return <Redirect to='/taster' />
  }

  return (
    <React.Fragment>
      <Title data-testid='login-page-title'>
        {t('containers.page.onboarding.login.title')}
      </Title>
      <LoginForm
        onLoginOperator={() => history.push('/operator')}
        onLoginTaster={() => history.push('/create-account/taster')}
        loginProfile={() => history.push('/taster')}
        handleNavigateToForgotPassword={() =>
          history.push(`${ONBOARD_ROOT_PATH}/forgot-password`)
        }
        handleNavigateToRequestAccount={() =>
          history.push(`${ONBOARD_ROOT_PATH}/request-account/operator`)
        }
        // Taster Account Link
        handleNavigateToCreateTasterAccount={() =>
          history.push(`/create-account/taster`)
        }
      />
      <TermsAndPrivacyFooter />
    </React.Fragment>
  )
}

export default withRouter(Login)
