import styled from 'styled-components'

export const CarouselImage = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: cover;
  height: 100vh;
  width: 100%;
`
