import React from 'react'
import Carousel from '../../../components/Carousel'
import OnboardingIllustration from '../../../components/OnboardingIllustration'

import { CarouselImage } from './styles.js'

import carouselImages from '../../../assets/pictures/onboarding'

import illus1 from '../../../assets/svg/illus1.svg'
import illus2 from '../../../assets/svg/illus2.svg'
import illus3 from '../../../assets/svg/illus3.svg'
import illus4 from '../../../assets/svg/illus4.svg'
import { useTranslation } from 'react-i18next';



const LoginCarousel = ({
  match: {
    params: { step }
  }
}) => { 
  const {t} =useTranslation();
  const images = [carouselImages[1], carouselImages[2], carouselImages[3], carouselImages[4], carouselImages[5], carouselImages[6]]

  const illustrations = [illus1, illus2, illus3, illus4]

  const stepsContentMap = {
    'create-account': {
      images: images.slice(0, 4),
      illustrations,
      titles: [
        t('containers.page.onboarding.loginCarousel.titles.welcome'),
        t('containers.page.onboarding.loginCarousel.titles.discoverProduct'),
        t('containers.page.onboarding.loginCarousel.titles.buyProduct'),
        t('containers.page.onboarding.loginCarousel.titles.cashIn')
      ],
      text: [
        t('containers.page.onboarding.loginCarousel.text.welcomeText'),
        t('containers.page.onboarding.loginCarousel.text.discoverProductText'),
        t('containers.page.onboarding.loginCarousel.text.buyProductText'),
        t('containers.page.onboarding.loginCarousel.text.cashInText')
      ]
    },
    default: {
      images
    }
  }

  const getImagesForStep = step =>
    (stepsContentMap[step] || stepsContentMap.default)['images']

  const getTitlesForStep = step =>
    stepsContentMap[step] && stepsContentMap[step].titles

  const getTextForStep = step =>
    stepsContentMap[step] && stepsContentMap[step].text

  const getIllustrationsForStep = step =>
    stepsContentMap[step] && stepsContentMap[step].illustrations

  return(
    <Carousel
      slides={getImagesForStep(step)}
      renderSlideTopContent={(image, imageIndex) =>
        getIllustrationsForStep(step) && (
          <OnboardingIllustration
            illustration={getIllustrationsForStep(step)[imageIndex]}
            title={getTitlesForStep(step)[imageIndex]}
            text={getTextForStep(step)[imageIndex]}
          />
        )
      }
      renderSlideBottomContent={image => <CarouselImage src={image} />}
    />
  )
}

export default LoginCarousel
