import React from 'react'
import { mount } from 'enzyme'
import LoginCarousel from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('LoginCarousel', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render LoginCarousel', () => {
    testRender = mount(<LoginCarousel match={{ params: { step: 1 } }} />)
    expect(testRender).toMatchSnapshot()
  })

  test('should render LoginCarousel', () => {
    testRender = mount(
      <LoginCarousel match={{ params: { step: 'create-account' } }} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
