import styled from 'styled-components'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'
import { DEFAULT_COMPONENTS_MARGIN } from '../../../utils/Metrics'

export const Text = styled.span`
  font-family: ${family.primaryLight};
  color: ${colors.BLACK};
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
  user-select: none;
  opacity: 0.7;
`

export const StyledSubTitle = styled(Text)`
  font-size: 1.8rem;
  white-space: pre-line;
`
