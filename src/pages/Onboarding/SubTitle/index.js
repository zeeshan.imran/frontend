import React from 'react'
import { StyledSubTitle } from './styles'

const SubTitle = ({ children }) => <StyledSubTitle>{children}</StyledSubTitle>

export default SubTitle
