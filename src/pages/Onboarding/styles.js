import styled from 'styled-components'

export const SideBySideLayout = styled.div`
  display: flex;
  flex-direction: row;

  width: 100vw;
  height: 100vh;
`

export const Side = styled.div`
  flex: 1;
  min-width: 50%;
  height: 100%;
  overflow-y: auto;
`
