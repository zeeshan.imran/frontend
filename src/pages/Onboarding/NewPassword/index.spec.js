import React from 'react'
import { mount } from 'enzyme'
import NewPassword from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))
jest.mock(
  '../../../containers/NewPasswordForm',
  () => ({ onSetNewPassword }) => {
    onSetNewPassword()
    return <div />
  }
)

describe('NewPassword', () => {
  let testRender
  let location
  let history
  let client

  beforeEach(() => {
    location = {
      token: 'token',
      email: 'test@flower-wiki.com'
    }
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      createHref: jest.fn(),
      location: { pathname: '' }
    }

    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render NewPassword', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <NewPassword location={location} />
        </Router>
      </ApolloProvider>
    )

    expect(testRender).toMatchSnapshot()
  })
})
