import React from 'react'
import { withRouter } from 'react-router-dom'
import NewPasswordForm from '../../../containers/NewPasswordForm'
import Title from '../Title'
import SubTitle from '../SubTitle'
import getQueryParams from '../../../utils/getQueryParams'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'
import { useTranslation } from 'react-i18next';

const NewPassword = ({ location, history }) => {
  const {t} =useTranslation();
  const { token, email } = getQueryParams(location)
  const onSetNewPassword = () => history.push('/onboarding/login')
  return (
    <React.Fragment>
      <Title>{t('containers.page.onboarding.newPassword.title')}</Title>
      <SubTitle>{t('containers.page.onboarding.newPassword.subTitle')}</SubTitle>
      <NewPasswordForm token={token} email={email} onSetNewPassword={onSetNewPassword} />
      <TermsAndPrivacyFooter />
    </React.Fragment>
  )
}

export default withRouter(NewPassword)
