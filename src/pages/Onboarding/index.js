import React from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'
import { Desktop } from '../../components/Responsive'
import OnboardingContent from './Content'
import Carousel from './Carousel'

import { ONBOARD_ROOT_PATH } from '../../utils/Constants'

import { SideBySideLayout, Side } from './styles'

const Onboarding = () => {
  return (
    <Desktop>
      {desktop => (
        <SideBySideLayout>
          <Side>
            <Switch>
              <Redirect
                exact
                path={`${ONBOARD_ROOT_PATH}/`}
                to={`${ONBOARD_ROOT_PATH}/login`}
              />
              <Route
                path={`${ONBOARD_ROOT_PATH}`}
                component={OnboardingContent}
              />
            </Switch>
          </Side>

          {desktop ? (
            <Side>
              <Route path={`${ONBOARD_ROOT_PATH}/:step`} component={Carousel} />
            </Side>
          ) : null}
        </SideBySideLayout>
      )}
    </Desktop>
  )
}

export default Onboarding
