import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'

import {
  PageContainer,
  SignUpTextContainer,
  Text,
  MessageText,
  SignUpText,
  StyledButton
} from './styles'
import Title from '../Title'
import { ONBOARD_ROOT_PATH } from '../../../utils/Constants'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'
import { withTranslation } from 'react-i18next';

class CreateAccount extends Component {
  goToCreateAccount = () => this.props.history.push('/create-account')

  render () {
    const { history,t } = this.props

    return (
      <PageContainer>
        <Title>{t('containers.page.onboarding.createAccount.title')}</Title>
        <MessageText>
          {t('containers.page.onboarding.createAccount.msgText')}
        </MessageText>
        <StyledButton type='primary' onClick={this.goToCreateAccount}>
          {t('containers.page.onboarding.createAccount.buttonText')}
        </StyledButton>
        <SignUpTextContainer>
          <Text>{t('containers.page.onboarding.createAccount.signupText')}</Text>
          <SignUpText
            onClick={() => history.push(`${ONBOARD_ROOT_PATH}/login`)}
          >
            {t('containers.page.onboarding.createAccount.signupBtnText')}
          </SignUpText>
        </SignUpTextContainer>
        <TermsAndPrivacyFooter />
      </PageContainer>
    )
  }
}

export default withTranslation()(withRouter(CreateAccount))
