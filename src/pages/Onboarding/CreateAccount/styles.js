import styled from 'styled-components'
import Button from '../../../components/Button'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'
import {
  DOUBLE_MOBILE_MARGIN,
  LOGIN_LEFT_PANEL_WIDTH,
  DEFAULT_COMPONENTS_MARGIN
} from '../../../utils/Metrics'

export const PageContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: ${LOGIN_LEFT_PANEL_WIDTH}rem;
  max-width: calc(100vw - ${DOUBLE_MOBILE_MARGIN}rem);
  margin-top: 1.5rem;
`

export const SignUpTextContainer = styled.div`
  text-align: center;
`

export const Text = styled.span`
  font-family: ${family.primaryLight};
  color: ${colors.BLACK};
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
  user-select: none;
  opacity: 0.7;
`

export const MessageText = styled(Text)`
  font-size: 1.8rem;
`

export const SignUpText = styled(Text)`
  color: ${colors.LIGHT_OLIVE_GREEN};
  cursor: pointer;
`

export const StyledButton = styled(Button)`
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
`
