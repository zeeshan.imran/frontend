import React from 'react'
import { mount } from 'enzyme'
import CreateAccount from '.'
import { BrowserRouter as Router } from 'react-router-dom'
import { SignUpText, StyledButton } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('CreateAccount', () => {
  let testRender
  let history

  beforeEach(() => {
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render LoginToRole', () => {
    history.location = {
      pathname: '/create-account'
    }

    testRender = mount(
      <Router>
        <CreateAccount history={history} />
      </Router>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('Redirect onclick to SignUpText', () => {
    history.location = {
      pathname: '/create-account'
    }

    testRender = mount(
      <Router>
        <CreateAccount history={history}>
          <SignUpText>Go back to homepage</SignUpText>
        </CreateAccount>
      </Router>
    )
    testRender.find(SignUpText).simulate('click')
  })

  test('Redirect goToCreateAccount onclick StyledButton', () => {
    history.location = {
      pathname: '/create-account'
    }

    testRender = mount(
      <Router>
        <CreateAccount history={history}>
          <StyledButton>Create Account</StyledButton>
        </CreateAccount>
      </Router>
    )
    testRender.find(StyledButton).simulate('click')
  })
})
