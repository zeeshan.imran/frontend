import React from 'react'
import OperatorSection from '../../components/OperatorSection'
import ProductsToCreateList from '../../containers/ProductsToCreateList'
import AddProductToCreate from '../../containers/AddProductToCreate'
import { useTranslation } from 'react-i18next'
import OperatorSurveyCreateCoverSelect from '../../containers/OperatorSurveyCreateCoverSelect'

const OperatorCreateSurveyProducts = ({ match, stricted = false }) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <OperatorSection
        title={t('containers.page.OperatorCreateSurveyProducts.title')}
      />
      <OperatorSurveyCreateCoverSelect stricted={stricted} />
      <ProductsToCreateList stricted={stricted} />
      <AddProductToCreate stricted={stricted} />
    </React.Fragment>
  )
}

export default OperatorCreateSurveyProducts
