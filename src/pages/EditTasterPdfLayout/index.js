import React from 'react'
import { withRouter } from 'react-router-dom'
import EditPdfLayout from '../../containers/EditPdfLayout'

export const EditTasterPdfLayoutPage = ({ match }) => {
  const { surveyId, jobGroupId } = match.params
  return <EditPdfLayout templateName="taster" surveyId={surveyId} jobGroupId={jobGroupId} />
}

export default withRouter(EditTasterPdfLayoutPage)
