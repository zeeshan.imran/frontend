import styled from 'styled-components'
import {family} from "../../utils/Fonts"
import colors from "../../utils/Colors"

export const PageContainer = styled.div`
  min-height: 100vh;
  width: 100%;
`

export const Container = styled.div`
  width: 90%;
  max-width: 57rem;
  padding-top: ${({ mobile }) => (mobile ? 12 : 16)}rem;
  margin: 0 auto;
  padding-bottom: 10rem;
`

export const Title = styled.p`
  font-family: ${family.primaryLight};
  font-size: 4.2rem;
  font-weight: 300;
  line-height: 1.24;

  color:${colors.TASTER_PROF_TITLE_COLOR};
  margin: 0;
  margin-bottom: 1.5rem;
`

export const Subtitle = styled.p`
  font-family: ${family.primaryLight};
  font-size: 1.8rem;
  font-weight: 300;
  line-height: 1.56;

  color: ${colors.TASTER_PROF_TITLE_COLOR};
  opacity: 0.7;

  margin: 0;
  margin-bottom: 3.5rem;
`
