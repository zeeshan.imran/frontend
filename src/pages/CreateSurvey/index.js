import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import OperatorSurveyCreationTabBar from '../../containers/OperatorSurveyCreationTabBar'
import OperatorCreateSurveyProducts from '../OperatorCreateSurveyProducts'
import OperatorCreateSurveyBasics from '../OperatorCreateSurveyBasics'
import OperatorCreateTastingNotes from '../OperatorCreateTastingNotes'
import OperatorCreateSurveySettings from '../OperatorCreateSurveySettings'
import OperatorCreateSurveyPayments from '../OperatorCreateSurveyPayments'
import OperatorCreateSurveyQuestions from '../OperatorCreateSurveyQuestions'
import OperatorCreateSurveyLinkedSurveys from '../OperatorCreateSurveyLinkedSurveys'

import { Page, PageContent } from './styles'
import OperatorCreateSurveyEmailSettings from '../OperatorCreateSurveyEmailSettings'
import {
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsSuperAdmin
} from '../../utils/userAuthentication'

const CreateSurvey = ({ match: { path, url }, history }) => {
  if (
    !(
      isUserAuthenticatedAsOperator() ||
      isUserAuthenticatedAsPowerUser() ||
      isUserAuthenticatedAsSuperAdmin()
    )
  ) {
    history.push('/operator/dashboard')
  }

  return (
    <React.Fragment>
      <Route path={`${path}/:step`} component={OperatorSurveyCreationTabBar} />
      <Page>
        <PageContent>
          <Switch>
            <Redirect exact path={`${path}`} to={`${url}/basics`} />
            <Redirect exact path={`${path}/`} to={`${url}/basics`} />
            <Route
              path={`${path}/basics`}
              component={OperatorCreateSurveyBasics}
            />
            <Route
              path={`${path}/emails`}
              component={OperatorCreateSurveyEmailSettings}
            />
            <Route
              path={`${path}/settings`}
              component={OperatorCreateSurveySettings}
            />
            <Route
              path={`${path}/tasting-notes`}
              component={OperatorCreateTastingNotes}
            />
            <Route
              path={`${path}/products`}
              component={OperatorCreateSurveyProducts}
            />
            <Route
              path={`${path}/questions`}
              component={OperatorCreateSurveyQuestions}
            />
            <Route
              path={`${path}/financial`}
              component={OperatorCreateSurveyPayments}
            />
            <Route
              path={`${path}/linked-surveys`}
              component={OperatorCreateSurveyLinkedSurveys}
            />
          </Switch>
        </PageContent>
      </Page>
    </React.Fragment>
  )
}

export default CreateSurvey
