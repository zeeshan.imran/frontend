export default {
  currentSurveyUser: {
    __typename: 'CurrentSurveyUser',
    lastAnsweredQuestion: null,
    state: null,
    selectedProducts: [],
    surveyEnrollmentId: null,
    paypalEmail: null
  }
}
