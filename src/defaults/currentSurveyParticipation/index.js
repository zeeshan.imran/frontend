export default {
  currentSurveyParticipation: {
    __typename: 'CurrentSurveyParticipation',
    surveyId: null,
    surveyEnrollmentId: null,
    state: null,
    selectedProducts: [],
    selectedProduct: null,
    paypalEmail: null,
    currentSurveyStep: null,
    currentSurveySection: null,
    isCurrentAnswerValid: false,
    canSkipCurrentQuestion: false,
    answers: [],
    lastAnsweredQuestion: null,
    country: '',
    savedRewards: [],
    email: null,
    productDisplayOrder: [],
    productDisplayType: ''
  }
}
