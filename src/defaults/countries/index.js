import { COUNTRY_PHONE_CODES } from '../../utils/Constants'

export default {
  countries: [...COUNTRY_PHONE_CODES]
}
