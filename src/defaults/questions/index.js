const defaultQuestions = {
  'paired-questions': {
    showProductImage: true
  },
  'choose-one': {
    showProductImage: true,
    skipFlow: {
      type: 'MATCH_OPTION',
      rules: []
    }
  },
  'choose-product': {
    skipFlow: {
      type: 'MATCH_PRODUCT',
      rules: []
    },
    type: 'choose-product',
    displayOn: 'middle',
    chooseProductOptions: {
      minimumProducts: 1,
      maximumProducts: 1
    },
    required: true
  },
  'paypal-email': {
    type: 'paypal-email',
    displayOn: 'payments',
    required: true
  },
  dropdown: {
    showProductImage: true,
    skipFlow: {
      type: 'MATCH_OPTION',
      rules: []
    }
  },
  'select-and-justify': {
    showProductImage: true,
    skipFlow: {
      type: 'MATCH_OPTION',
      rules: []
    }
  },
  'vertical-rating': {
    showProductImage: true,
    range: {
      labels: [],
      min: 1,
      max: 9
    },
    skipFlow: {
      type: 'MATCH_RANGE',
      rules: []
    }
  },
  numeric: {
    showProductImage: true,
    numericOptions: {
      decimalNumbers: 0
    }
  },
  'choose-multiple': {
    showProductImage: true,
    skipFlow: {
      type: 'MATCH_MULTIPLE_OPTION',
      rules: []
    }
  },
  'choose-payment': {
    type: 'choose-payment',
    displayOn: 'payments',
    hasPaymentOptions: true,
    required: true,
    options: [
      {
        label: 'Paypal',
        value: '10',
        editMode: 'strict'
      },
      {
        label: 'Gift Card',
        value: '11',
        editMode: 'strict'
      }
    ]
  },
  'matrix':{
    skipFlow: {
      type: 'MATCH_MATRIX_OPTION',
      rules: []
    }
  }
}

export default defaultQuestions
