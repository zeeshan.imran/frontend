import i18n from '../../utils/internationalization/i18n'
import { getMessage } from '../../utils/getCustomSharingMessage'
import { getLoginText } from '../../utils/getLoginText'
import { getPauseText } from '../../utils/getPauseText'
import { DEFAULT_TASTING_NOTES } from '../../utils/Constants'

const defaultSurveyCreation = {
  surveyCreation: {
    __typename: 'SurveyCreation',
    editMode: 'default',
    products: [],
    basics: {
      __typename: 'BasicInfo',
      name: null,
      instructionsText: i18n.t('defaultValues.instructionsText'),
      instructionSteps: [
        i18n.t('defaultValues.instructionSteps.0'),
        i18n.t('defaultValues.instructionSteps.1'),
        i18n.t('defaultValues.instructionSteps.2'),
        i18n.t('defaultValues.instructionSteps.3')
      ],
      thankYouText: getMessage('defaultValues.thankYouText'),
      rejectionText: getMessage('defaultValues.rejectionText'),
      screeningText: getMessage('defaultValues.screeningText'),
      uniqueName: null,
      authorizationType: 'public',
      coverPhoto: null,
      exclusiveTasters: [],
      allowRetakes: false,
      forcedAccount: false,
      forcedAccountLocation: 'start',
      tastingNotes: {
        __typename: 'SurveyCreationTastingNotes',
        ...DEFAULT_TASTING_NOTES
      },
      referralAmount: 0,
      recaptcha: false,
      savedRewards: [],
      minimumProducts: 1,
      maximumProducts: null,
      surveyLanguage: 'en',
      country: 'US',
      maxProductStatCount: 6,
      allowedDaysToFillTheTasting: 5,
      customizeSharingMessage: getMessage('defaultValues.customizeSharingMessage'),
      loginText: getLoginText(),
      pauseText: getPauseText(),
      isPaypalSelected: false,
      isGiftCardSelected: false,
      customButtons: {
        __typename: 'CustomButtons',
        continue: 'Continue',
        start: 'Start',
        next: 'Next',
        skip: 'Skip'
      },
      isScreenerOnly: false,
      productDisplayType: 'none',
      showGeneratePdf: false,
      linkedSurveys: [],
      sharingButtons: true,
      validatedData: false,
      showOnTasterDashboard: false,
      autoAdvanceSettings: {
        __typename: 'AutoAdvanceSettings',
        active: false,
        debounce: 0,
        hideNextButton: false
      },
      pdfFooterSettings : {
        __typename: 'PdfFooterSettings',
        active : false,
        footerNote : ""
      },
      enabledEmailTypes: [],
      emails: {
        __typename: 'SurveyCreationEmails',
        surveyWaiting: {
          __typename: 'SurveyCreationEmail',
          subject: i18n.t('email.surveyWaiting.subject'),
          html: i18n.t('email.surveyWaiting.html'),
          text: i18n.t('email.surveyWaiting.text')
        },
        surveyCompleted: {
          __typename: 'SurveyCreationEmail',
          subject: i18n.t('email.surveyCompleted.subject'),
          html: i18n.t('email.surveyCompleted.html'),
          text: i18n.t('email.surveyCompleted.text')
        },
        surveyRejected: {
          __typename: 'SurveyCreationEmail',
          subject: i18n.t('email.surveyRejected.subject'),
          html: i18n.t('email.surveyRejected.html'),
          text: i18n.t('email.surveyRejected.text')
        }
      }
    },
    questions: [],
    mandatoryQuestions: [],
    uniqueQuestionsToCreate: []
  }
}

export default defaultSurveyCreation
