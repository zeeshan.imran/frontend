const { REACT_APP_THEME } = process.env
const themeName = REACT_APP_THEME || 'default'

const defaultTheme = {
  colorsArr: [
    '#3497db',
    '#e74c3c',
    '#2fcc71',
    '#f1c40f',
    '#8e44ad',
    '#95a5a7',
    '#ff7f0e',
    '#1abc9c',
    '#8c564b',
    '#e377c2'
  ],
  mapChartColorsArr: ['#3497db', '#b4d69a'],
  outlineColor: '#424242'
}

const chrHansenTheme = {
  colorsArr: [
    // colors at 100%:
    '#004b88',
    '#009864',
    '#ec1a3b',
    '#0c6bbf',
    '#f37321',
    '#7a0f2d',
    '#b61773',
    '#fcb813',
    '#769e00',
    '#001233',
    '#000000'

    // colors at 75%:
    // '#007ee5', '#00f19e', '#f0536c', '#2692f1', '#f59658', '#cd194b', '#e5339a', '#fcc94d', '#b7f600'

    // colors at 50%:
    // '#43abff', '#4bffc1', '#f58c9d', '#6eb6f5', '#f9b88f', '#ea5881', '#ed77bc', '#fddb88', '#d2fe4f'

    // colors at 25%
    // '#a1d5ff', '#a5ffe0', '#fac5ce', '#b6dafa', '#fcdbc7', '#f4abc0', '#f6bbdd', '#feedc3', '#e8ffa6'
  ],
  mapChartColorsArr: ['#004b88', '#009864'],
  outlineColor: '#424242'
}

const themes = {
  default: defaultTheme,
  chrHansen: chrHansenTheme
}

export const defaultChartSettings = themes[themeName]
  ? themes[themeName]
  : defaultTheme
