import styled from 'styled-components'
import colors from '../../utils/Colors'
import { Form, InputNumber as antdInput } from 'antd'
import Text from '../Text'
import { family } from '../../utils/Fonts'

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
`

export const LabelContainer = styled(Form.Item)`
  margin-bottom: 0;
`

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
`

export const CheckboxSpacer = styled.div`
  line-height: 1.57;
  font-size: 1.4;
  min-height: 21px;
  margin-bottom: 0.4rem;
`

export const CurrencySign = styled.div`
  position: absolute;
  display: block;
  line-height: 35px;
  font-size: 14px;
  text-align: left;
  color: rgba(0, 0, 0, 0.25);
  top: 8px;
  left: 12px;
`

export const InputNumber = styled(antdInput)`
  width: 100%;
  .ant-input-number-input-wrap {
    .ant-input-number-input {
      font-family: ${family.NotoSansKRRegular};
      color: #000000;
      font-size: 1.4rem;
      caret-color: ${colors.PRODUCT_CARET_COLOR};
      letter-spacing: 0.04rem;
      padding-left: 30px;
    }
  }
`

export const PrefixedInput = styled.div`
  .ant-input-number-input {
    ${props => (props.largePrefix ? 'padding-left: 46px' : '')}
  }
`
