import React, { useEffect, useRef } from 'react'
import { Form, Row, Col } from 'antd'
import PubSub from 'pubsub-js'
import Input from '../Input'
import Button from '../Button'
import {
  Container,
  CheckboxSpacer,
  PrefixedInput
} from './styles'
import UploadProductPicture from '../../containers/UploadProductPicture/index'
import { Formik } from 'formik'
import { PUBSUB } from '../../utils/Constants'
import { validateProductSchema } from '../../validates'
import { useTranslation } from 'react-i18next'
import HelperIcon from '../HelperIcon'
import { StyledCheckbox } from '../StyledCheckBox'

const MAX_LIMIT_REFERRAL = 100000

const CreateProductCard = ({
  onChange,
  handleRemove,
  name,
  brand,
  photo,
  reward,
  isAvailableError,
  isAvailable,
  prefix,
  suffix,
  editMode,
  disabledInputBrandName,
  products=[]
}) => {
  const { REACT_APP_THEME } = process.env
  const isStrictMode = editMode === 'strict'
  const formRef = useRef()
  const { t } = useTranslation()

  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.VALIDATE_SURVEY_PRODUCTS, () => {
      if (formRef.current) {
        formRef.current.validateForm()
      }
    })
    return () => {
      PubSub.unsubscribe(token)
    }
  }, [])

  return (
    <Formik
      ref={formRef}
      validationSchema={validateProductSchema}
      initialValues={{
        name,
        brand,
        reward,
        isAvailable
      }}
      render={({ values, errors, setFieldValue }) => {
        const {
          name: nameError,
          brand: brandError,
          reward: rewardError
        } = errors
        return (
          <Container>
            <Row type='flex' justify='end'>
              <Col>
                {isStrictMode && (
                  <HelperIcon
                    placement='bottomRight'
                    helperText={t('tooltips.product.canNotDelete')}
                  />
                )}
                {!isStrictMode && (
                  <Button type='red' onClick={handleRemove}>
                    X
                  </Button>
                )}
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 8 }}>
                <Row xs={{ span: 8 }}>
                  <Form.Item
                    help={nameError}
                    validateStatus={nameError ? 'error' : 'success'}
                  >
                    <Input
                      name='name'
                      value={values.name}
                      onChange={event => {
                        setFieldValue('name', event.target.value)
                        onChange({ name: event.target.value })
                      }}
                      label={t('components.product.name')}
                      tooltip={t('tooltips.product.name')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                {REACT_APP_THEME !== 'chrHansen' ? (
                  <Row xs={{ span: 8 }}>
                    <Form.Item
                      help={rewardError}
                      validateStatus={rewardError ? 'error' : 'success'}
                    >
                      <PrefixedInput
                        largePrefix={
                          (prefix && prefix.length > 2) ||
                          (suffix && suffix.length > 2)}
                      >
                        <Input
                          min={0}
                          max={MAX_LIMIT_REFERRAL}
                          precision={2}
                          label={t('components.product.reward')}
                          tooltip={t('tooltips.product.reward')}
                          name='reward'
                          value={values.reward}
                          placeholder={t('components.product.reward')}
                          onChange={event => {
                            setFieldValue(
                              'reward',
                              parseFloat(event.target.value, 10)
                            )
                            onChange({
                              reward: parseFloat(event.target.value, 10)
                            })
                          }}
                          size='default'
                          prefix={prefix || suffix}
                          type={`number`}
                        />
                      </PrefixedInput>
                    </Form.Item>
                  </Row>
                ): null}
                <Row xs={{ span: 8 }}>
                  <Form.Item />
                </Row>
              </Col>
              <Col xs={{ span: 8 }}>
                <Row xs={{ span: 8 }}>
                  <Form.Item
                    help={brandError}
                    validateStatus={brandError ? 'error' : 'success'}
                  >
                    <Input
                      required
                      name='brand'
                      value={values.brand}
                      onChange={event => {
                        setFieldValue('brand', event.target.value)
                        onChange({ brand: event.target.value })
                      }}
                      label={t('components.product.brand')}
                      tooltip={t('tooltips.product.brand')}
                      size='default'
                      disabledInput={disabledInputBrandName}
                    />
                  </Form.Item>
                </Row>
                <Row xs={{ span: 8 }}>
                  <Form.Item
                    help={
                      isAvailableError && t('validation.products.isAvailable')
                    }
                    validateStatus={isAvailableError ? 'error' : 'success'}
                  >
                    <CheckboxSpacer />
                    <StyledCheckbox
                      name='isAvailable'
                      checked={values.isAvailable}
                      onChange={e => {
                        const { checked: value } = e.target
                        setFieldValue('isAvailable', value)
                        onChange({ isAvailable: value })
                      }}
                      >
                      {t('components.product.isAvailable')}
                      <HelperIcon
                        placement='bottomRight'
                        helperText={t('tooltips.product.isAvailable')}
                      />
                    </StyledCheckbox>
                  </Form.Item>
                </Row>
              </Col>
              <Col xs={{ span: 8 }}>
                <UploadProductPicture
                  tooltip={t('tooltips.product.picture')}
                  name='photo'
                  value={photo}
                  onChange={picture => onChange({ photo: picture[0] })}
                />
              </Col>
            </Row>
          </Container>
        )
      }}
    />
  )
}

export default CreateProductCard
