import React from 'react'
import { Button } from 'antd'
import { ImpersonateWrapper } from './styles'
import { useTranslation } from 'react-i18next'

const ImpersonateHeader = ({ adminUser, impersonateUser, onExit }) => {
  const { t } = useTranslation()
  return (
    <ImpersonateWrapper>
      {t(`components.impersonate.impersonatedAs`, {
        user: adminUser.fullName || adminUser.emailAddress,
        targetUser: impersonateUser.fullName || impersonateUser.emailAddress
      })}
      <Button type='link' onClick={onExit}>
        {t('components.impersonate.toGoToAdmin')}
      </Button>
    </ImpersonateWrapper>
  )
}

export default ImpersonateHeader
