import styled from 'styled-components'
import colors from '../../utils/Colors'

export const ImpersonateWrapper = styled.div`
  padding: 8px 16px;
  background: ${colors.IMPERSONATE_WRAPPER_COLOR};
  color: #fff;

  .ant-btn-link {
    padding: 0 4px;
    background: transparent;
    border: none;
    color: ${colors.IMPERSONATE_LINK_COLOR};
    :hover > span {
      text-decoration: underline;
    }
  }
`
