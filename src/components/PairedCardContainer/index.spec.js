import React from 'react'
import { shallow } from 'enzyme'
import PairedCardContainer from './index'

describe('PairedCardContainer', () => {
  let testRender
  let children

  beforeEach(() => {
    children = ['1', '2', '3'].map(id => <div key={id}>Paired Card</div>)
  })

  afterEach(() => {
    testRender.unmount() 
  })

  test('should render PairedCardContainer', async () => {
    testRender = shallow(<PairedCardContainer children={children} />)
    expect(testRender).toMatchSnapshot()
  })
})
