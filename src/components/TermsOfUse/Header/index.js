import React from 'react'
import { Row, Col } from 'antd'
import { TopContainer, Logo, LogoAligner } from './styles'
import { getImages } from '../../../utils/getImages'
import { imagesCollection } from '../../../assets/png'
const { desktopImage } = getImages(imagesCollection)

const Header = ({ desktop }) => (
  <TopContainer desktop={desktop}>
    <Row type='flex' align='left'>
      <Col xs={{ span: 12 }} lg={{ span: 4 }}>
        <LogoAligner>
          <Logo src={desktopImage} />
        </LogoAligner>
      </Col>
    </Row>
  </TopContainer>
)

export default Header
