import styled from 'styled-components'
import { DEFAULT_MOBILE_MARGIN } from '../../../utils/Metrics'

export const TopContainer = styled.div`
  margin-bottom: ${({ desktop }) =>
    desktop ? `6rem` : `${DEFAULT_MOBILE_MARGIN}rem`};
    width: 100%;
    padding: 2.5rem 3.5rem 0.1rem;
`
export const Logo = styled.img`
  height: 3.6rem;
`

export const LogoAligner = styled.div`
  display: flex;
  justify-content: left;
`
