import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 3.5rem;
  margin-bottom: 3.5rem;
`