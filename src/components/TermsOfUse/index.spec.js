import React from 'react'
import { mount } from 'enzyme'
import TermsOfUse from './index'
import wait from '../../utils/testUtils/waait'
import Header from './Header'
import Footer from './Footer'

describe('TermsOfUse component', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test(`should not display Header and Footer for Operator`, async () => {
    testRender = mount(<TermsOfUse />)
    await wait(0)
    expect(testRender.find(Header).length).toBe(1)
    expect(testRender.find(Footer).length).toBe(1)
    testRender.setProps({ isOperator: true })
    testRender.update()
    expect(testRender.find(Header).length).toBe(0)
    expect(testRender.find(Footer).length).toBe(0)
  })
})
