import styled from 'styled-components'
import colors from '../../utils/Colors'
import { Row } from 'antd'
import Text from '../Text'

export const ProductsRow = styled(Row)`
  width: 965px;
  margin: 0 auto;
  margin-bottom: 2.5rem;
`
export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 3rem 3.5rem;
  margin-bottom: 2.5rem;
`
export const Gallery = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: stretch;
`

export const PhotoSection = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  margin: 10px 2%;
  border-bottom: 2px solid #8cbb5c;
  justify-content: center;
  cursor: pointer;
  width: 21%;
  padding: 5rem 0 2rem 0;
  :hover {
    border-bottom: 2px solid #3a581b;
  }
`
export const IconContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  background: #fff;
  svg {
    fill: ${({ state }) =>
      state
        ? state === 'valid'
          ? colors.SOFT_GREEN
          : colors.RED
        : colors.BLACK};
  }
`
export const Image = styled.img`
  width: 15.0rem;
  height: 15.0rem;
  object-fit: contain;
`

export const FieldText = styled(Text)`
  display: block;
  color: #aaa;
  font-size: 1.3rem;
`