import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import SurveyChooseProduct from './index'
import Select from '../Select'
import ProductCard from '../ProductCard'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('SurveyChooseProduct', () => {
  let testRender
  let chooseProduct

  beforeEach(() => {
    chooseProduct = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render brands dropdown if the survey has more than one brand', async () => {
    testRender = mount(
      <SurveyChooseProduct
        brands={[
          { id: 'brand-1', name: 'Brand 1' },
          { id: 'brand-2', name: 'Brand 2' }
        ]}
        products={[
          { id: 'product-1', name: 'Product 1', brand: 'brand-1' },
          { id: 'product-2', name: 'Product 2', brand: 'brand-1' },
          { id: 'product-3', name: 'Product 3', brand: 'brand-2' }
        ]}
        takenProducts={[]}
        selectedProduct={null}
        onChange={chooseProduct}
        t={name => name}
      />
    )

    expect(testRender.find(Select)).toHaveLength(1)
    // should NOT show product cards
    expect(testRender.find(ProductCard)).toHaveLength(0)

    // trigger change brand
    act(() => {
      testRender
        .find(Select)
        .first()
        .prop('onChange')('brand-1')
    })
    testRender.update()

    // the brand should changed
    expect(
      testRender
        .find(Select)
        .first()
        .props()
    ).toMatchObject({ value: 'brand-1' })
    // should show two product cards
    expect(testRender.find(ProductCard)).toHaveLength(2)

    // trigger click on product
    act(() => {
      testRender
        .find(ProductCard)
        .first()
        .prop('onClick')()
    })
    expect(chooseProduct).toHaveBeenCalledWith('product-1')
  })

  test('should NOT render brands dropdown if the survey has only one brand', async () => {
    testRender = mount(
      <SurveyChooseProduct
        brands={[{ id: 'brand-1', name: 'Brand 1' }]}
        products={[{ id: 'product-1', name: 'Product 1', brand: 'brand-1' },
        { id: 'product-2', name: 'Product 2', brand: 'brand-1' }]}
        takenProducts={[]}
        selectedProduct={null}
        onChange={chooseProduct}
        t={name => name}
      />
    )

    expect(testRender.find(Select)).toHaveLength(0)
    // should show two product cards
    expect(testRender.find(ProductCard)).toHaveLength(2)

    // trigger click on product
    act(() => {
      testRender
        .find(ProductCard)
        .first()
        .prop('onClick')()
    })
    expect(chooseProduct).toHaveBeenCalledWith('product-1')
  })
})
