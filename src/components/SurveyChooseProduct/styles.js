import styled from 'styled-components'
import { Col as AntCol } from 'antd'

export const Col = styled(AntCol)`
  display: flex;
  margin-top: 2.5rem;
  float: none;
  justify-content: ${({ mobile }) => (mobile ? 'center' : 'flex-start')};
`

export const Container = styled.div`
  margin-top: 2.5rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`
