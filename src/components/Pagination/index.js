import React from 'react'
import { Pagination as AntPagination } from 'antd'

const Pagination = ({ total, elementsPerPage, handlePaginationChange }) => (
  <AntPagination
    defaultCurrent={1}
    total={total}
    showSizeChanger={elementsPerPage}
    onChange={handlePaginationChange}
  />
)

export default Pagination
