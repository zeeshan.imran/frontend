import React from 'react'
import { shallow } from 'enzyme'
import Pagination from '.'


describe('LoadingModal', () => {
    let testRender
    let handlePaginationChange

    beforeEach(() => {
        handlePaginationChange = jest.fn();
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render LoadingModal', () => {
        testRender = shallow(
            <Pagination
                total={50}
                elementsPerPage={true}
                handlePaginationChange={handlePaginationChange}
            />
        )
        expect(testRender).toMatchSnapshot()
        
    })
})
