import React from 'react'
import { storiesOf } from '@storybook/react'
import Pagination from './'

storiesOf('Pagination', module).add('Simple pagination', () => (
  <Pagination total={50} />
))
storiesOf('Pagination', module).add('Pagination with Elements Per Page', () => (
  <Pagination total={50} elementsPerPage />
))
storiesOf('Pagination', module).add(
  'Pagination with handlePaginationChange',
  () => (
    <Pagination
      total={39}
      handlePaginationChange={(page, pageSize) =>
        console.log('page', page, pageSize)
      }
    />
  )
)
