import styled from 'styled-components'
import ReCAPTCHA from 'react-google-recaptcha'
import { DEFAULT_COMPONENTS_MARGIN } from '../../utils/Metrics'

export const RecaptchaVerification = styled(ReCAPTCHA)``

export const Container = styled.div`
  display: flex;
  justify-content: ${({ fullWidth }) => (fullWidth ? 'center' : 'flex-start')};
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
`
