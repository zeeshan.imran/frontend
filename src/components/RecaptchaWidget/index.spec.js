import React from 'react'
import { mount } from 'enzyme'
import RecaptchaWidget from '.'
import { RecaptchaVerification } from './styles'

describe('RecaptchaWidget', () => {
  let testRender
  let fullWidth
  let handleChange

  beforeEach(() => {
    fullWidth = true
    handleChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render RecaptchaWidget', () => {
    testRender = mount(
      <RecaptchaWidget fullWidth={fullWidth} handleChange={handleChange} />
    )

    expect(testRender.find(RecaptchaWidget)).toHaveLength(1)
  })

  test('should render RecaptchaWidget handle change', () => {
    testRender = mount(
      <RecaptchaWidget fullWidth={fullWidth} handleChange={handleChange} />
    )

    testRender
      .find(RecaptchaVerification)
      .props()
      .onChange()

    expect(handleChange).toHaveBeenCalled()
  })
})
