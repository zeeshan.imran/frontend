import React from 'react'

import { RecaptchaVerification, Container } from './styles'

const RecaptchaWidget = ({ fullWidth, handleChange }) => (
  <Container fullWidth={fullWidth}>
    <RecaptchaVerification
      sitekey={process.env.REACT_APP_GOOGLE_RECAPTCHA_KEY}
      onChange={handleChange}
    />
  </Container>
)

export default RecaptchaWidget
