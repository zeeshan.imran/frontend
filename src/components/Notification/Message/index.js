import React from 'react'
import { MessageText } from './styles'

const Message = ({ children }) => <MessageText>{children}</MessageText>

export default Message
