import React from 'react'
import { shallow } from 'enzyme'
import Notification from '.'
import moment from 'moment'

describe('Notification', () => {
  let testRender
  let onClick
  let type
  let title
  let message
  let date
  let points

  beforeEach(() => {
    onClick = jest.fn()
    type = 'team'
    title = 'Survey Completed'
    message = 'Congratulations'
    date = moment('2019-10-01')
    points = 20
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Notification', async () => {
    testRender = shallow(
      <Notification
        onClick={onClick}
        type={type}
        title={title}
        message={message}
        date={date}
        points={points}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
