import React from 'react'
import { NotificationIcon, CompletedNotificationIcon } from './styles'
import SurveyIcon from '../../../assets/svg/survey.svg'
import TeamIcon from '../../../assets/svg/FlavorWiki_Clear_Logo.svg'

const Icon = ({ type }) => {
  // FIXME This should be refactored as soon as we get the updated icon images
  switch (type) {
    case 'survey':
      return <NotificationIcon src={SurveyIcon} />
    case 'team':
      return <NotificationIcon src={TeamIcon} />
    case 'completed':
      return <CompletedNotificationIcon type='check' />
  }
}

export default Icon
