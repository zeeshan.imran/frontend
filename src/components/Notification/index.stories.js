import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Notification from './'
import moment from 'moment'

const surveyNotification = {
  title: 'Thank you for selecting one survey',
  message: 'Secret To Cooking Vegetables',
  date: moment(),
  type: 'survey'
}

const teamNotification = {
  title: 'Discover our new surveys',
  message:
    'Now you can use our app, check for products or for categories to start tasting products',
  date: moment(),
  type: 'team'
}

const completedNotification = {
  title: 'Survey Completed',
  message: 'Congratulations',
  date: moment(),
  type: 'completed',
  points: 20
}

storiesOf('Notification', module)
  .add('Survey Notification', () => (
    <Notification
      title={surveyNotification.title}
      message={surveyNotification.message}
      date={surveyNotification.date}
      type={surveyNotification.type}
      onClick={action('pressed')}
    />
  ))
  .add('Team Notification', () => (
    <Notification
      title={teamNotification.title}
      message={teamNotification.message}
      date={teamNotification.date}
      type={teamNotification.type}
      onClick={action('pressed')}
    />
  ))
  .add('Completed Notification', () => (
    <Notification
      title={completedNotification.title}
      message={completedNotification.message}
      date={completedNotification.date}
      type={completedNotification.type}
      onClick={action('pressed')}
    />
  ))
  .add('History - Completed With Points Awarded', () => (
    <Notification
      title={completedNotification.title}
      message={completedNotification.message}
      date={completedNotification.date}
      type={completedNotification.type}
      points={completedNotification.points}
      onClick={action('pressed')}
    />
  ))
