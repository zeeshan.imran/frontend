import React from 'react'
import PropTypes from 'prop-types'
import Moment from 'moment'
import colors from '../../utils/Colors'
import {
  Container,
  IconAndTextContainer,
  NotificationTextContainer
} from './styles'
import CircleWithIcon from '../CircleWithIcon'
import Title from './Title'
import Message from './Message'
import Date from './Date'
import Icon from './Icon'
import Points from './Points'

const typeToColorMap = {
  survey: colors.SALMON,
  completed: colors.LIGHT_OLIVE_GREEN,
  team: colors.MID_BLUE
}

const Notification = ({ onClick, type, title, message, date, points }) => {
  const withAwards = points || points === 0

  return (
    <Container onClick={onClick}>
      <IconAndTextContainer>
        <CircleWithIcon backgroundColor={typeToColorMap[type]}>
          <Icon type={type} />
        </CircleWithIcon>
        <NotificationTextContainer>
          <Title>{title}</Title>
          <Message>{message}</Message>
          <Date>{date.format('D MMM YYYY')}</Date>
        </NotificationTextContainer>
      </IconAndTextContainer>
      {withAwards && <Points points={points} />}
    </Container>
  )
}

Notification.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  date: PropTypes.instanceOf(Moment),
  type: PropTypes.string,
  onClick: PropTypes.func,
  points: PropTypes.number
}

export default Notification
