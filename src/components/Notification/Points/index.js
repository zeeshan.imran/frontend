import React from 'react'
import { RoundedSquare, PointsText } from './styles'

const Points = ({ points }) => (
  <RoundedSquare>
    <PointsText>{`${points}PT`}</PointsText>
  </RoundedSquare>
)

export default Points
