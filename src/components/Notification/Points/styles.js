import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const RoundedSquare = styled.div`
  background-color: ${colors.LIGHT_OLIVE_GREEN};
  border-radius: 1rem;
  width: 7.5rem;
  height: 5rem;
  display: flex;
  justify-content: center;
  align-items: center;
`
export const PointsText = styled.span`
  font-family: ${family.primaryBold};
  font-size: 1.8rem;
  line-height: 1.78;
  letter-spacing: 0.03rem;
  color: ${colors.WHITE};
`
