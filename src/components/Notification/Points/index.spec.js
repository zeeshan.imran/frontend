import React from 'react'
import { shallow } from 'enzyme'
import Points from '.'

describe('Points', () => {
  let testRender
  let point

  beforeEach(() => {
    point = 22
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Points', async () => {
    testRender = shallow(<Points point={point} />)
    expect(testRender).toMatchSnapshot()
  })
})
