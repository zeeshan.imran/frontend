import styled from 'styled-components'
import { Icon } from 'antd'
import colors from '../../utils/Colors'

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 1.6rem 1.9rem 1.6rem 2.6rem;
  cursor: pointer;
  user-select: none;
  justify-content: space-between;
`

export const NotificationTextContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const IconAndTextContainer = styled.div`
  display: flex;
  align-items: center;
`
