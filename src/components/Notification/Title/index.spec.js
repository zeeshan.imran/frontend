import React from 'react'
import { shallow } from 'enzyme'
import Title from '.'

describe('Title', () => {
  let testRender
  let children

  beforeEach(() => {
    children = 'are you want delete survey?'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Title', async () => {
    testRender = shallow(<Title children={children} />)
    expect(testRender).toMatchSnapshot()
  })
})
