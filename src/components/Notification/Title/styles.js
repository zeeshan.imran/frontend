import styled from 'styled-components'
import Text from '../../Text'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const TitleText = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1.71;
  letter-spacing: 0.04rem;
  color: ${colors.SLATE_GREY};
  margin-bottom: 0.2rem;
`
