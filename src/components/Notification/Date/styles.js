import styled from 'styled-components'
import Text from '../../Text'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const DateText = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  font-size: 1.3rem;
  line-height: 1.23;
  letter-spacing: 0.05rem;
  color: ${colors.LIGHT_GREY};
`
