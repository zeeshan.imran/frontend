import React from 'react'
import { DateText } from './styles'

const Date = ({ children }) => <DateText>{children}</DateText>

export default Date
