import styled from 'styled-components'
import Button from '../../Button'

export const CustomButton = styled(Button)`
  width: 100%;
`
