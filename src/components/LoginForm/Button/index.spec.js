import React from 'react'
import { shallow } from 'enzyme'
import Button from '.'

describe('Button', () => {
  let testRender
  let loading
  let otherProps

  beforeEach(() => {
    loading = true
    otherProps = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Button input', async () => {
    testRender = shallow(<Button loading={loading} otherProps={otherProps} />)

    expect(testRender).toMatchSnapshot()
  })
})
