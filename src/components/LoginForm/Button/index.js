import React from 'react'
import Loader from '../../Loader'
import { CustomButton } from './styles'

const Button = ({ loading = false, ...otherProps }) => (
  <Loader loading={loading}>
    <CustomButton {...otherProps} />
  </Loader>
)

export default Button
