import React from 'react'
import { Form, Icon } from 'antd'
import { isEmpty } from 'ramda'
import Input from '../Input'
import Button from './Button'
import Error from '../Error'
import OnboardingForm from '../OnboardingForm'

import {
  ButtonsContainer,
  FormErrorsContainer,
  SignUpTextContainer,
  Text,
  SignUpText,
  ForgotPasswordLink
} from './styles'
import { useTranslation } from 'react-i18next';

const LoginForm = ({
  email,
  password,
  loginError,
  handleChange,
  handleSubmit,
  handleNavigateToRequestAccount,
  handleNavigateToCreateTasterAccount,
  handleNavigateToForgotPassword,
  loading,
  errors,
  resendEmailButton,
  resendVerification
}) => {

 
  const { email: emailError, password: passwordError } = errors

  const canLogin = !!email && !!password && isEmpty(errors)

  const { t } = useTranslation();

  return (
    <OnboardingForm onSubmit={handleSubmit}>
      <Form.Item
        help={emailError}
        validateStatus={emailError ? 'error' : 'success'}
      >
        <Input
          name='email'
          value={email}
          onChange={handleChange}
          prefix={<Icon type='user' />}
          placeholder={t('placeholders.username')}
        />
      </Form.Item>
      <Form.Item
        help={passwordError}
        validateStatus={passwordError ? 'error' : 'success'}
      >
        <Input
          name='password'
          value={password}
          onChange={handleChange}
          type={'password'}
          prefix={<Icon type='lock' />}
          placeholder={t('placeholders.password')}
          onPressEnter={handleSubmit}
        />
      </Form.Item>
      {!!loginError && (
        <FormErrorsContainer>
          <Error>{loginError || ''}</Error>
        </FormErrorsContainer>
      )}
      <ForgotPasswordLink onClick={handleNavigateToForgotPassword}>
        {t('forgotPassword')}?
      </ForgotPasswordLink>
      {resendEmailButton && (
        <ForgotPasswordLink onClick={resendVerification}>
          {t('components.login.resendVerificatioEmail')}
        </ForgotPasswordLink>
      )}
      <ButtonsContainer>
        <Button
          loading={loading}
          type={canLogin ? 'primary' : 'disabled'}
          onClick={handleSubmit}
          data-testid='submit-button'
        >
          {t('signIn')}
        </Button>
        <SignUpTextContainer>
          {/* <Text>{t('dontHaveAccount')} </Text>
          <Text>{t('requestAccount')} </Text>
          <SignUpText onClick={handleNavigateToRequestAccount}>
            {t('requestOperatorAccount')}
          </SignUpText> */}
          <Text> {t('requestAccountOr')} </Text>
          <SignUpText onClick={handleNavigateToCreateTasterAccount}>
            {t('taster')}
          </SignUpText>
        </SignUpTextContainer>
      </ButtonsContainer>
    </OnboardingForm>
  )
}

export default LoginForm
