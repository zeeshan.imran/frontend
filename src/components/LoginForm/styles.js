import styled from 'styled-components'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'
import { DEFAULT_COMPONENTS_MARGIN } from '../../utils/Metrics'

export const ButtonsContainer = styled.div`
  margin-top: ${DEFAULT_COMPONENTS_MARGIN}rem;
  text-align: center;
  display: flex;
  flex-direction: column;
`

export const SignUpTextContainer = styled.div`
  margin-top: ${DEFAULT_COMPONENTS_MARGIN}rem;
`

export const Text = styled.span`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  line-height: 1.57;
  letter-spacing: 0.06rem;
  margin-top: ${DEFAULT_COMPONENTS_MARGIN}rem;
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
  user-select: none;
`

export const FormErrorsContainer = styled.div`
  margin: ${DEFAULT_COMPONENTS_MARGIN}rem 0;
`

export const SignUpText = styled(Text)`
  color: ${colors.LIGHT_OLIVE_GREEN};
  cursor: pointer;
`

export const ForgotPasswordLink = styled.p`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  line-height: 1.57;
  color: ${colors.LIGHT_BLUE};
  text-align: right;
  cursor: pointer;
`
