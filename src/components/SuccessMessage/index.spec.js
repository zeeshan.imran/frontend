import React from 'react'
import { mount } from 'enzyme'
import SuccessMessage from '.'

describe('SuccessMessage', () => {
  let testRender
  let invisible
  let children

  beforeEach(() => {
    invisible = 'no'
    children = '<container>Successfully Saved</container>'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SuccessMessage', async () => {
    testRender = mount(
      <SuccessMessage
        invisible={invisible}
        children={children}
      />
    )
    expect(testRender.find(SuccessMessage)).toHaveLength(1)
  })
})
