import React from 'react'
import { storiesOf } from '@storybook/react'
import SuccessMessage from './'

storiesOf('SuccessMessage', module).add('Success Label', () => (
  <SuccessMessage>{'Example Success message'}</SuccessMessage>
))
