import React from 'react'
import { Container, Icon, Text } from './styles'

/**
 *
 * @param {boolean} invisible set as true if you want the message occupy a given
 * space but keep it hidden from the user (good to avoid flickers)
 */

const SuccessMessage = ({ invisible, children }) => (
  <Container>
    <Icon invisible={invisible} type='exclamation-circle' />
    <Text invisible={invisible}>{children}</Text>
  </Container>
)

export default SuccessMessage
