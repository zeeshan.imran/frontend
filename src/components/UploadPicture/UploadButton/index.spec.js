import React from 'react'
import { shallow } from 'enzyme'
import UploadButton from '.'

describe('UploadButton', () => {
    window.localStorage = {
        getItem: () => '{}'
    }

    let testRender
    let imageUrl
    let loading
    let handleRemove
    let handleBeforeUpload


    beforeEach(() => {
        imageUrl = ''
        loading = true
        handleRemove = jest.fn()
        handleBeforeUpload = jest.fn()

    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render UploadButton', async () => {
        testRender = shallow(
            <UploadButton
                imageUrl={imageUrl}
                loading={loading}
                handleRemove={handleRemove}
                handleBeforeUpload={handleBeforeUpload}

            />
        )
        expect(testRender).toMatchSnapshot()
    })
})