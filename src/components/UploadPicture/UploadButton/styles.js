import styled from 'styled-components'
import { Upload as AntUpload, Icon as AntIcon } from 'antd'

export const Upload = styled(AntUpload)`
  .ant-upload {
    width: ${({ desktop }) =>
    desktop ? '32.5rem !important' : '20.8rem !important'};
    height: ${({ desktop }) =>
    desktop ? '16.6rem !important' : '20.8rem !important'};
  }
`

export const Avatar = styled.div`
  display: flex;
  background: url(${({ src }) => src}) no-repeat center;
  background-size: contain;
  border-radius: ${({ rounded }) => (rounded ? '50%' : '0%')};
  width: 100%;
  height: 100%;
`

export const Icon = styled(AntIcon)`
  font-size: 2.4rem;
`

export const IconAligner = styled.div`
  display: flex;
  flex-direction: column;
`
