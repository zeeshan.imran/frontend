import React from 'react'
import { Avatar, Icon, Upload, IconAligner } from './styles'
import { Desktop } from '../../Responsive'

const UploadButton = ({
  imageUrl,
  loading,
  handleRemove,
  handleBeforeUpload
}) => (
  <Desktop>
    {desktop => (
      <Upload
        desktop={desktop}
        name='avatar'
        accept='.jpg,.jpeg,.png'
        showUploadList={false}
        listType='picture-card'
        className='avatar-uploader'
        onRemove={handleRemove}
        beforeUpload={handleBeforeUpload}
        customRequest={() => {}} // This fixes the default 404 POST on localhost, and since we will not be using the request of the component, we can leave this as an empty function
      >
        {imageUrl && !loading ? (
          <Avatar src={imageUrl} alt={'avatar'} />
        ) : (
          <IconAligner>
            <Icon type={loading ? 'loading' : 'plus'} />
          </IconAligner>
        )}
      </Upload>
    )}
  </Desktop>
)

export default UploadButton
