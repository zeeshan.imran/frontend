import styled from 'styled-components'
import { Icon as AntIcon } from 'antd'
import colors from '../../../utils/Colors'

export const CustomUploadPicture = styled.div`
  align-self: center;
  user-select: none;

  .ant-upload.ant-upload-select-picture-card > .ant-upload {
    padding: 0 !important;
  }

  .ant-upload-list-picture-card .ant-upload-list-item {
    width: 9.6rem;
    height: 9.6rem;
    margin: 0;
    padding: 0;
    border: none;
  }

  .ant-upload-list-item-info {
    border-radius: 50% !important;
  }

  .ant-upload.ant-upload-select-picture-card {
    border: solid 0.2rem ${colors.LIGHT_OLIVE_GREEN};
    width: 9.6rem;
    height: 9.6rem;
    border-radius: 50%;
    background-color: ${colors.WHITE};
    margin: 0;
    box-shadow: 0 0.2rem 0.4rem 0 rgba(78, 78, 86, 0.4);
  }

  .ant-upload-list-item-uploading {
    border: none;
    border-radius: 50%;
  }

  /* This fixes the flicker when in mobile (Tested in Safari and Chrome) */
  .ant-upload-list-picture-card .ant-upload-list-item-info:before {
    border-radius: 50% !important;
  }
`

export const CenterContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const StyledIcon = styled(AntIcon)`
  position: absolute;
  font-size: 2.4rem;
  color: ${colors.LIGHT_OLIVE_GREEN};
`

export const LightGreenCircle = styled.div`
  width: 5.6rem;
  height: 5.6rem;
  border-radius: 50%;
  background-color: ${colors.LIGHT_OLIVE_GREEN};
  opacity: 0.2;
`

export const Img = styled.img`
  width: 100%;
  background-size: cover;
`
