import React from 'react'
import { Upload, Modal } from 'antd'
import {
  CustomUploadPicture,
  CenterContainer,
  StyledIcon,
  LightGreenCircle,
  Img
} from './styles'

const RoundedUploadButton = ({
  imageUrl,
  loading,
  previewVisible,
  handlePreview,
  handleBeforeUpload,
  handleRemove,
  handleCancel
}) => (
  <CustomUploadPicture>
    <Upload
      accept='.jpg,.jpeg,.png'
      listType='picture-card'
      onPreview={handlePreview}
      beforeUpload={handleBeforeUpload}
      onRemove={handleRemove}
      fileList={
        imageUrl && [
          {
            uid: '-1',
            url: imageUrl
          }
        ]
      }
    >
      {!imageUrl && (
        <CenterContainer>
          {loading ? (
            <StyledIcon type='sync' spin />
          ) : (
            <StyledIcon type='camera' theme='filled' />
          )}
          <LightGreenCircle />
        </CenterContainer>
      )}
    </Upload>
    {imageUrl && (
      <Modal visible={previewVisible} footer={null} onCancel={handleCancel}>
        <Img alt={'Avatar'} src={imageUrl} />
      </Modal>
    )}
  </CustomUploadPicture>
)

export default RoundedUploadButton
