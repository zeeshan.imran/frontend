import React from 'react'
import { shallow } from 'enzyme'
import RoundedUploadButton from '.'

describe('RoundedUploadButton', () => {
    window.localStorage = {
        getItem: () => '{}'
    }

    let testRender
    let imageUrl
    let loading
    let previewVisible
    let handlePreview
    let handleBeforeUpload
    let handleRemove
    let handleCancel

    beforeEach(() => {
        imageUrl = ''
        loading = false
        previewVisible = false
        handlePreview = false
        handleBeforeUpload = jest.fn()
        handleRemove = jest.fn()
        handleCancel = jest.fn()

    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render RoundedUploadButton', async () => {
        testRender = shallow(
            <RoundedUploadButton
                imageUrl={imageUrl}
                loading={loading}
                previewVisible={previewVisible}
                handlePreview={handlePreview}
                handleBeforeUpload={handleBeforeUpload}
                handleRemove={handleRemove}
                handleCancel={handleCancel}

            />
        )
        expect(testRender).toMatchSnapshot()
    })
})
