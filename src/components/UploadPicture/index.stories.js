import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import UploadPicture from './'

storiesOf('Upload Picture', module)
  .add('Default', () => <UploadPicture onFileUpload={action} />)
  .add('Rounded Upload', () => <UploadPicture rounded onFileUpload={action} />)
  .add('Rounded Upload With UserPicture', () => (
    <UploadPicture
      rounded
      onFileUpload={action}
      userPicture={'https://pbs.twimg.com/media/DbbgoMZW0AABU9Z.jpg'}
    />
  ))
