import React, { Component } from 'react'
import PropTypes from 'prop-types'
import getBase64 from '../../utils/getBase64'
import UploadButton from './UploadButton'
import RoundedUploadButton from './RoundedUploadButton'

class UploadPicture extends Component {
  state = {
    imageUrl: '' || this.props.userPicture,
    previewVisible: false,
    previewImage: ''
  }

  componentDidUpdate (prevProps) {
    const { userPicture: currentPicture } = this.props
    const { userPicture: previousPicture } = prevProps

    if (currentPicture !== previousPicture) {
      this.setState({ imageUrl: currentPicture })
    }
  }

  handleBeforeUpload = async file => {
    const { onFileUpload } = this.props
    const imageUrl = await getBase64(file)
    this.setState({ imageUrl }, () => onFileUpload({ file }))
    return false
  }

  handleRemove = () => {
    const { onFileUpload } = this.props
    this.setState({ imageUrl: '' }, () => onFileUpload({ file: {} }))
  }

  handlePreview = file => {
    this.setState({
      previewImage: this.state.imageUrl,
      previewVisible: true
    })
  }

  handleCancel = () => this.setState({ previewVisible: false })

  render () {
    const { rounded, loading, error } = this.props
    const { imageUrl, previewVisible } = this.state
    return (
      <React.Fragment>
        {rounded ? (
          <RoundedUploadButton
            imageUrl={imageUrl}
            loading={loading}
            previewVisible={previewVisible}
            handlePreview={this.handlePreview}
            handleBeforeUpload={this.handleBeforeUpload}
            handleRemove={this.handleRemove}
            handleCancel={this.handleCancel}
          />
        ) : (
          <UploadButton
            imageUrl={error ? '' : imageUrl}
            loading={loading}
            handleRemove={this.handleRemove}
            handleBeforeUpload={this.handleBeforeUpload}
          />
        )}
      </React.Fragment>
    )
  }
}

UploadPicture.defaultProps = {
  onFileUpload: () => {},
  rounded: false,
  userPicture: ''
}

UploadPicture.propTypes = {
  onFileUpload: PropTypes.func,
  rounded: PropTypes.bool,
  userPicture: PropTypes.string
}

export default UploadPicture
