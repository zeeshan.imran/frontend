import React from 'react'
import { mount } from 'enzyme'
import UploadPicture from '.'
import { Upload } from 'antd'
import { act } from 'react-dom/test-utils'
import UploadButton from './UploadButton'

describe('UploadPicture', () => {
    window.localStorage = {
        getItem: () => '{}'
    }

    let testRender
    let onFileUpload
    let rounded
    let userPicture

    beforeEach(() => {
        onFileUpload = jest.fn()
        rounded = true
        userPicture = ''
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render UploadPicture', async () => {
        testRender = mount(
            <UploadPicture
                onFileUpload={onFileUpload}
                rounded={rounded}
                userPicture={userPicture}
            />
        )
        expect(testRender.find(UploadPicture)).toHaveLength(1)
    })

    test('should render Upload before', () => {
        testRender = mount(<UploadPicture handleBeforeUpload={onFileUpload} />)

        testRender.find(UploadButton).simulate('change')
        expect(onFileUpload).not.toHaveBeenCalled()
    })

    test('should remove picture', () => {
        testRender = mount(
            <UploadPicture
                onFileUpload={onFileUpload}
                rounded={rounded}
                userPicture={userPicture}
            />
        )

        act(() => {
            testRender.find(Upload).prop('onRemove')()
        })
        expect(onFileUpload).toHaveBeenCalled()
    })
})