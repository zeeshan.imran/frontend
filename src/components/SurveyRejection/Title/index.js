import React from 'react'
import { CustomCol, Prompt } from './styles'
import { withTranslation } from 'react-i18next'

const Title = ({ t }) => (
  <CustomCol lg={{ span: 8, offset: 8 }}>
    <Prompt>{t('components.surveyInfoTest.rejected')}</Prompt>
  </CustomCol>
)

export default withTranslation()(Title)
