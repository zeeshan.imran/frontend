import React from 'react'
import { CustomCol, Prompt } from './styles'

const SubTitle = ({ text }) => (
  <CustomCol lg={{ span: 8, offset: 8 }}>
    <Prompt>{text}</Prompt>
  </CustomCol>
)

export default SubTitle
