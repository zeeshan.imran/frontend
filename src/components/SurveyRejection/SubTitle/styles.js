import styled from 'styled-components'
import { Col } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const CustomCol = styled(Col)`
  display: flex;
  justify-content: center;
  margin-bottom: 2rem;
`

export const Prompt = styled(Text)`
  font-family: ${family.primaryLight};
  font-size: 1.8rem;
  color: ${colors.BLACK};
  text-align: center;
`
