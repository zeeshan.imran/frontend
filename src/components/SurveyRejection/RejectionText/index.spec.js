import React from 'react'
import { shallow } from 'enzyme'
import RejectionText from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import SURVEY_PARTICIPATION_QUERY from '../../../queries/SurveyParticipationQuery'
import { surveyBasicInfo } from '../../../fragments/survey'

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
      screeners {
        id
        uniqueName
        isPaypalSelected
        isGiftCardSelected
        customizeSharingMessage
        referralAmount
      }
    }
  }

  ${surveyBasicInfo}
`

describe('RejectionText', () => {
  let testRender
  let client = createApolloMockClient({
    mocks: [
      {
        request: {
          query: SURVEY_PARTICIPATION_QUERY,
          variables: {}
        },
        result: {
          surveyId: 'survey-id',
          surveyEnrollmentId: 'survey-id'
        }
      },
      {
        request: {
          query: SURVEY_QUERY,
          variables: {
            id: 'survey-id'
          }
        },
        result: {
          uniqueName: 'survey-unique-name'
        }
      }
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render RejectionText', async () => {
    testRender = shallow(
      <ApolloProvider client={client}>
        <RejectionText text={'Enter rejection text here'} />
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()
  })
})
