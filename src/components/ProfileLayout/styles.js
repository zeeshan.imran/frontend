import styled from 'styled-components'
import {
  DEFAULT_MOBILE_PADDING,
  DESKTOP_CONTENT_PADDING_TOP,
  MOBILE_CONTENT_PADDING_TOP
} from '../../utils/Metrics'
import colors from '../../utils/Colors'

export const ProfileContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`

export const ProfileContent = styled.div`
  background-color: ${colors.WHITE};
  flex: 1;
  display: flex;
  flex-direction: column;
  padding-top: ${({ desktop }) =>
    desktop
      ? `${DESKTOP_CONTENT_PADDING_TOP}rem`
      : `${MOBILE_CONTENT_PADDING_TOP}rem`};
  padding-left: ${({ desktop }) =>
    desktop
      ? `${DESKTOP_CONTENT_PADDING_TOP}rem`
      : `${DEFAULT_MOBILE_PADDING}rem`};
  padding-right: ${({ desktop }) =>
    desktop
      ? `${DESKTOP_CONTENT_PADDING_TOP}rem`
      : `${DEFAULT_MOBILE_PADDING}rem`};
`

export const FooterContainer = styled.div`
  padding-left: ${({ desktop }) => (desktop ? '21.5rem' : 0)};
  padding-right: ${({ desktop }) => (desktop ? '21.5rem' : 0)};
`
