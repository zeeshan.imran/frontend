import React from 'react'
import { shallow } from 'enzyme'
import ProfileLayout from '.';
import Person from '../SvgIcons/Person'
import AdditionalInfo from '../SvgIcons/AdditionalInfo'
import PersonalDetails from '../../pages/MyProfile/PersonalDetails'
import AdditionalInformation from '../../pages/MyProfile/AdditionalInformation'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('ProfileLayout', () => {
    let testRender
    let tabs
    let renderTab
    let renderTabContent

    beforeEach(() => {
        tabs = [
            {
                icon: <Person />,
                title: 'Personal Details',
                content: PersonalDetails
            },
            {
                icon: <AdditionalInfo />,
                title: 'Additional Information',
                content: AdditionalInformation
            }
        
        ]
        renderTab = jest.fn(() => a => a);
        renderTabContent = jest.fn(() => a => a);
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render ProfileLayout', async () => {
        testRender = shallow(

            <ProfileLayout
                tabs={tabs}
                renderTab={renderTab}
                renderTabContent={renderTabContent}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});