import React from 'react'
import { Desktop } from '../Responsive'
import Tabs from '../Tabs'
import Footer from '../Footer'
import { ProfileContainer, ProfileContent, FooterContainer } from './styles'
import SectionTitle from '../SectionTitle'
import { useTranslation } from 'react-i18next';

const ProfileLayout = ({ tabs, renderTab, renderTabContent }) => {
  const { t } = useTranslation();
  (
    <Desktop>
      {desktop => {
        return (
          <ProfileContainer>
            <ProfileContent desktop={desktop}>
              <SectionTitle title={t('components.profile.title')} />
              <Tabs
                tabs={tabs}
                renderTab={renderTab}
                renderTabContent={renderTabContent}
              />
            </ProfileContent>
            <FooterContainer desktop={desktop}>
              <Footer />
            </FooterContainer>
          </ProfileContainer>
        )
      }}
    </Desktop>
  )
}

export default ProfileLayout
