import React, { Fragment } from 'react'
import ProductsTable from './ProductsTable'
import SelectedItemsBanner from '../SelectedItemsBanner'

const AdminProductList = ({ products, selectedProducts, ...tableProps }) => (
  <Fragment>
    <SelectedItemsBanner selectedItems={selectedProducts.length} />
    <ProductsTable products={products} {...tableProps} />
  </Fragment>
)

export default AdminProductList
