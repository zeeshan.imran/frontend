import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import AdminProductList from './'

const products = [
  {
    date: '2000-12-16',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg',
    id: 'UHJvZHVjdE5vZGU6VXZ2QlJSakxMWW9iY2VNelFtR1VkQg==',
    brandId: 'DVPQNphWoC86Rsg8PEUC7Y',
    name: 'Schweizer Wurst',
    price: null,
    currency: null,
    isInternal: false,
    size: null,
    sizeUnitId: null,
    barcode: null,
    brand: {
      name: 'Bell'
    },
    sizeUnit: null,
    chains: [
      {
        storechain: {
          name: 'Migros'
        }
      }
    ],
    categories: [
      {
        category: {
          key: 'sausages1',
          value: 'Sausages'
        }
      }
    ],
    iid: 'UvvBRRjLLYobceMzQmGUdB'
  },
  {
    date: '1990-12-16',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg',
    id: 'UHJvZHVjdE5vZGU6Sk53Tm1QclNXOXhhVHFVQmF2eFNSWg==',
    brandId: '3RQwQRtBgJcnTKnyfYZRjJ',
    name: 'Deutschländer Würstchen',
    price: null,
    currency: null,
    isInternal: false,
    size: null,
    sizeUnitId: null,
    barcode: null,
    brand: {
      name: 'Meica'
    },
    sizeUnit: null,
    chains: [
      {
        storechain: {
          name: 'Migros'
        }
      }
    ],
    categories: [
      {
        category: {
          key: 'sausages1',
          value: 'Sausages'
        }
      },
      {
        category: {
          key: 'meat',
          value: 'Meat'
        }
      }
    ],
    iid: 'JNwNmPrSW9xaTqUBavxSRZ'
  },
  {
    date: '2018-12-16',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg',

    id: 'UHJvZHVjdE5vZGU6M2ZqWXlMVkZ6dnVEVnFYOHZid29iMg==',
    brandId: 'B8PqWJEHECNMaeatNT8Erc',
    name: 'Schokolade',
    price: null,
    currency: null,
    isInternal: false,
    size: 100,
    sizeUnitId: '6opskuVDFpG9w9779Dh7vQ',
    barcode: null,
    brand: {
      name: 'Milka'
    },
    sizeUnit: {
      key: 'g',
      value: 'g'
    },
    chains: [],
    categories: [
      {
        category: {
          key: 'chocolate',
          value: 'Chocolate'
        }
      }
    ],
    iid: '3fjYyLVFzvuDVqX8vbwob2'
  }
]

class StateWrapper extends Component {
  state = {
    selectedProducts: []
  }

  render () {
    return this.props.children({
      setSelectedProducts: selectedProducts =>
        this.setState({ selectedProducts }),
      selectedProducts: this.state.selectedProducts
    })
  }
}

storiesOf('AdminProductList', module).add('Story', () => (
  <StateWrapper>
    {({ setSelectedProducts, selectedProducts }) => {
      return (
        <AdminProductList
          products={products}
          selectedProducts={selectedProducts}
          handleChange={setSelectedProducts}
          handleDeleteProduct={id =>
            console.log('...deleting product with the id', id)
          }
          handleEditProduct={id =>
            console.log('...editing product with the id', id)
          }
        />
      )
    }}
  </StateWrapper>
))
