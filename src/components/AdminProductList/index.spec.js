import React from 'react'
import { shallow } from 'enzyme'
import AdminProductList from '.'

window.matchMedia = jest.fn().mockImplementation(query => {
  return {
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn()
  }
})

describe('AdminProductList', () => {
  let testRender
  let products
  let selectedProducts
  let tableProps

  beforeEach(() => {
    products = []
    selectedProducts = []
    tableProps = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AdminProductList', async () => {
    testRender = shallow(
      <AdminProductList
        products={products}
        selectedProducts={selectedProducts}
        tableProps={tableProps}
      />
    )
    expect(testRender).toMatchSnapshot() 
  })
})
