import React from 'react'
import { mount } from 'enzyme'
import ProductsTable, { columns, showDeleteProductAlert } from '.'
import { Action } from './styles'
import AlertModal from '../../AlertModal'

jest.mock('../../AlertModal')

const mockAlertModal = ({ handleOk, handleCancel }) => {
  handleOk()
  handleCancel()
}
AlertModal.mockImplementation(mockAlertModal)

jest.mock('../../../utils/internationalization/i18n', () => ({
  t: text => text
}))

window.matchMedia = jest.fn().mockImplementation(query => {
  return {
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn()
  }
})

describe('ProductsTable', () => {
  let testRender
  let products
  let handleChange
  let handleEditProduct
  let handleDeleteProduct

  beforeEach(() => {
    products = [
      {
        date: null,
        id: 1,
        brandId: 'DVPQNphWoC86Rsg8PEUC7Y',
        name: 'Schweizer Wurst',
        brand: {
          name: 'Bell'
        },
        categories: [
          {
            category: {
              key: 'sausages1',
              value: 'Sausages'
            }
          }
        ]
      }
    ]

    handleChange = jest.fn()
    handleEditProduct = jest.fn()
    handleDeleteProduct = jest.fn()
  })

  test('should render ProductsTable', async () => {
    testRender = mount(
      <ProductsTable
        products={products}
        handleChange={handleChange}
        handleEditProduct={handleEditProduct}
        handleDeleteProduct={handleDeleteProduct}
      />
    )
    expect(testRender.find(ProductsTable)).toHaveLength(1)
  })

  test('should call on click when edit', async () => {
    testRender = mount(
      <ProductsTable
        products={products}
        handleEditProduct={handleEditProduct}
      />
    )

    testRender
      .find(Action)
      .first()
      .simulate('click')

    expect(handleEditProduct).toBeCalledWith(1)
    testRender.unmount()
  })
  test('should call on click when remove', async () => {
    testRender = mount(
      <ProductsTable
        products={products}
        handleDeleteProduct={handleDeleteProduct}
      />
    )

    testRender
      .find(Action)
      .last()
      .simulate('click')

    expect(handleDeleteProduct).toBeCalledWith(1)

    testRender.unmount()
  })

  test('should sort', () => {
    const now = new Date()
    const cols = columns(jest.fn(), jest.fn())
    const sorter = cols[3]['sorter']
    expect(sorter({ date: now }, { date: now })).toBe(0)
  })

  test('should handle ok and cancel', () => {
    showDeleteProductAlert(handleDeleteProduct, 'product-id')
    expect(AlertModal).toHaveBeenCalled()
    expect(handleDeleteProduct).toHaveBeenCalledWith('product-id')
  })
})
