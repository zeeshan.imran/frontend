import React, { Fragment } from 'react'
import { Table as AntTable, Divider } from 'antd'
import moment from 'moment'
import { Picture, FieldText, Action } from './styles'
import AlertModal from '../../AlertModal'
import i18n from '../../../utils/internationalization/i18n'

const getCategoriesName = categories =>
  categories.map(({ category }) => category.value).join(', ')

export const showDeleteProductAlert = (handleRemove, productId) =>
  AlertModal({
    title: i18n.t('components.productsTable.alertModalTitle'),
    description: i18n.t('components.productsTable.alertModaldescription'),
    handleOk: () => handleRemove(productId),
    handleCancel: () => console.log('clicked Cancel')
  })

export const columns = (handleEdit, handleRemove) => [
  {
    title: i18n.t('components.productsTable.columnProductName'),
    dataIndex: 'name',
    render: (_, product) => (
      <Fragment>
        <Picture src={product.photo} />
        <FieldText>{product.name}</FieldText>
      </Fragment>
    )
  },
  {
    title: i18n.t('components.productsTable.columnCategory'),
    dataIndex: 'category',
    render: (_, product) => getCategoriesName(product && product.categories)
  },
  {
    title: i18n.t('components.productsTable.columnClient'),
    dataIndex: 'client',
    render: (_, product) => product.brand.name // FIXME This should be fixed as soon as we get support from the backend
  },
  {
    title: i18n.t('components.productsTable.columnDate'),
    dataIndex: 'date',
    render: (_, product) => product.date,
    sorter: (productA, productB) =>
      moment(productA.date) - moment(productB.date),
    defaultSortOrder: 'descend'
  },
  {
    title: i18n.t('components.productsTable.columnAction'),
    dataIndex: 'actions',
    render: (_, product) => (
      <Fragment>
        <Action onClick={() => handleEdit(product.id)}>
          {i18n.t('components.productsTable.actionEdit')}
        </Action>
        <Divider type='vertical' />
        <Action
          onClick={() => showDeleteProductAlert(handleRemove, product.id)}
        >
          {i18n.t('components.productsTable.actionRemove')}
        </Action>
      </Fragment>
    )
  }
]

const ProductsTable = ({
  products,
  handleChange,
  handleEditProduct,
  handleDeleteProduct
}) => {
  return (
    <AntTable
      rowKey={record => record.id}
      rowSelection={{
        onChange: handleChange
      }}
      columns={columns(handleEditProduct, handleDeleteProduct)}
      dataSource={products}
    />
  )
}

export default ProductsTable
