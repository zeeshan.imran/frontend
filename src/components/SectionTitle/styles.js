import styled from 'styled-components'
import Text from '../Text'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const Container = styled.div`
  display: inline-flex;
  flex-direction: column;
  margin-bottom: ${({ desktop }) => (desktop ? '3.5rem' : '2rem')};
  width: 100%;
`

export const TitleAligner = styled.div`
  display: inline-flex;
  align-items: baseline;
  width: 100%;
`

const Base = styled(Text)`
  margin: 0;

  &::selection {
    color: ${colors.LIGHT_OLIVE_GREEN};
    background-color: #fafafa;
  }
`

export const Title = styled(Base)`
  font-size: ${({ desktop }) => (desktop ? '4.2rem' : '1.8rem')};
  font-family: ${({ desktop }) =>
    desktop ? family.primaryLight : family.primaryBold};
  line-height: ${({ desktop }) => (desktop ? '5.2rem' : '3.2rem')};
  color: ${colors.GENERAL_TITLE_COLOR};
`

export const Subtitle = styled(Base)`
  font-size: 1.6rem;
  font-family: ${family.primaryRegular};
  line-height: 3.2rem;
  margin-left: 2rem;
  color: #8a8a8a;
`

export const Prompt = styled(Subtitle)`
  margin-top: 1.5rem;
  margin-left: 0;
  text-transform: uppercase;
`
