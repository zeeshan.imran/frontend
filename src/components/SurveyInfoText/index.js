import React from 'react'
import { Desktop } from '../Responsive'
import { Container, Title, Text, StyledButton, InfoNotes } from './styles'
import { withTranslation } from 'react-i18next'
import TermsAndPrivacyFooter from '../../containers/TermsAndPrivacyFooter';

const SurveyInfoText = ({ title, description, onClick, t, buttonLabel }) => (
  <Desktop>
    {desktop => (
      <Container desktop={desktop}>
        <Title>{title}</Title>
        <Text>{description}</Text>
        <InfoNotes>
          {t('emailUs')}
          <a href='mailto:support@flavorwiki.com'>support@flavorwiki.com</a>
        </InfoNotes>
        <Text>{t('happyTasting')}</Text>
        <Text>{t('theFlavorWikiTeam')}</Text>
        <StyledButton onClick={onClick} desktop={desktop}>
          {buttonLabel}
        </StyledButton>
        <TermsAndPrivacyFooter />
      </Container>
    )}
  </Desktop>
)

export default withTranslation()(SurveyInfoText)
