import React from 'react'
import { storiesOf } from '@storybook/react'
import SurveyInstructionsScreen from './'

const title = 'Example Title'

const description = 'Example Description'

storiesOf('SurveyInstructionsScreen', module).add('Story', () => (
  <SurveyInstructionsScreen title={title} description={description} />
))
