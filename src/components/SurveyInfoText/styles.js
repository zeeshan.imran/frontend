import styled from 'styled-components'
import TextComponent from '../Text'
import Button from '../Button'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const BaseText = styled(TextComponent)`
  font-family: ${family.primaryLight};
  color: ${colors.SLATE_GREY};
  text-align: center;
`

export const Title = styled(BaseText)`
  font-size: 4.2rem;
  margin-bottom: 3.5rem;
`

export const Text = styled(BaseText)`
  font-size: 1.8rem;
  opacity: 0.7;
`

export const InfoNotes = styled(BaseText)`
  font-size: 1.8rem;
  opacity: 0.7;
  margin: 1rem 0;
`

export const StyledButton = styled(Button)`
  margin-top: 5rem;
  width: ${({ desktop }) => (desktop ? '30%' : '100%')};
`
