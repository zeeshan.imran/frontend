import React from 'react'
import { mount } from 'enzyme'
import SurveyInfoText from '.'
import { BrowserRouter as Router } from 'react-router-dom'
import { StyledButton } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('SurveyInfoText', () => {
  let testRender
  let title
  let description
  let onClick
  let buttonLabel

  beforeEach(() => {
    title = 'Survey title'
    description = 'Survey description'
    onClick = jest.fn()
    buttonLabel = 'Submit'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyInfoText', async () => {
    testRender = mount(
      <Router>
        <SurveyInfoText
          title={title}
          description={description}
          onClick={onClick}
          buttonLabel={buttonLabel}
        />
      </Router>
    )
    expect(testRender.find(SurveyInfoText)).toHaveLength(1)

    testRender.find(StyledButton).prop('onClick')()

    expect(onClick).toHaveBeenCalled()
  })
})
