import React from 'react'
import { Content } from './styles'

const OperatorPageContent = ({ children }) => <Content>{children}</Content>

export default OperatorPageContent
