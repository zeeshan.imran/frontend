import React from 'react'
import { shallow } from 'enzyme'
import OperatorPageContent from '.'

import { Content } from './styles'

describe('OperatorPageContent', () => {
  let testRender
  let children

  beforeEach(() => {
    children = <Content>Operator Page</Content>
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorPageContent', async () => {
    testRender = shallow(<OperatorPageContent children={children} />)
    expect(testRender).toMatchSnapshot()
  })
})
