import styled from 'styled-components'

export const Content = styled.div`
  flex: 1;
  padding: 2.5rem;
  margin: 0 auto;
  width: 100%;
`
