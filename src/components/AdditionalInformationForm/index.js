import React, { Component } from 'react'
import { Form, Row, Col } from 'antd'
import Select from '../Select'
import {
  Container,
  CustomForm,
  FieldTitle,
  FormItem,
  ButtonAligner,
  StyledButton
} from './styles'
import {
  getOptionName,
  getOptionValue
} from '../../utils/getters/OptionGetters'
import { CURRENCIES, BINARY_RESPONSES, CHILDREN_AGES } from '../../utils/Data'
import INCOME_RANGES_QUERY from '../../queries/incomeRanges'
import { Query } from 'react-apollo'
import { isEmpty, isNil } from 'ramda'
import Input from '../Input'
import { withTranslation } from 'react-i18next';

class AdditionalInformationForm extends Component {
  render () {
    const {
      form: { getFieldDecorator, isFieldsTouched, getFieldValue },
      desktop,
      submitForm,
      ethnicity,
      ethnicities,
      currency,
      incomeRange,
      hasChildren,
      childrenAges,
      smokerKinds,
      smokerKind,
      productCategories,
      preferredProductCategories,
      dietaryPreferences,
      dietaryPreference,
      t
    } = this.props

    const shouldRenderChildrenAgesDropdown =
      getFieldValue('hasChildren') === 'Yes' &&
      getFieldValue('numberOfChildren') > 0

    return (
      <Query
        query={INCOME_RANGES_QUERY}
        variables={{ currency: getFieldValue('currency') }}
      >
        {({ loading, error, data: { incomeRanges } = {} }) => (
          <Container>
            <Row type='flex' justify='center'>
              <Col xs={{ span: 24 }} lg={{ span: 16 }} xl={{ span: 12 }}>
                <CustomForm onSubmit={e => submitForm(e, this.props.form)}>
                  <Row>
                    <FieldTitle>{t('components.additionalInfo.ethnicity')}</FieldTitle>
                  </Row>
                  <Row>
                    <Col xs={{ span: 24 }} xl={{ span: 24 }}>
                      <FormItem>
                        {getFieldDecorator('ethnicity', {
                          initialValue: ethnicity
                        })(
                          <Select
                            placeholder={ t('components.additionalInfo.ethnicityPlaceholder') }
                            options={ethnicities}
                            getOptionName={getOptionName}
                            getOptionValue={getOptionValue}
                          />
                        )}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <FieldTitle>{t('components.additionalInfo.income')}</FieldTitle>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                      <FormItem>
                        {getFieldDecorator('currency', {
                          initialValue: currency
                        })(
                          <Select
                            placeholder={ t('components.additionalInfo.incomePlaceholder') }
                            options={CURRENCIES}
                          />
                        )}
                      </FormItem>
                    </Col>
                    <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                      <FormItem>
                        {getFieldDecorator('incomeRange', {
                          initialValue: incomeRange
                        })(
                          <Select
                            placeholder={t('components.additionalInfo.incomeRangePlaceholder')}
                            options={incomeRanges}
                            getOptionName={getOptionName}
                            getOptionValue={getOptionValue}
                            disabled={isEmpty(incomeRanges)}
                          />
                        )}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <FieldTitle>{t('components.additionalInfo.childrenAtHome')}</FieldTitle>
                  </Row>
                  <Row gutter={24}>
                    <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                      <FormItem>
                        {getFieldDecorator('hasChildren', {
                          initialValue: hasChildren
                        })(
                          <Select
                            name='hasChildren'
                            placeholder={t('components.additionalInfo.childrenAtHomePlaceholder')}
                            options={BINARY_RESPONSES}
                          />
                        )}
                      </FormItem>
                    </Col>
                    <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                      <FormItem>
                        {getFieldDecorator('numberOfChildren', {
                          initialValue:
                            isNil(childrenAges) || childrenAges.length === 0
                              ? undefined
                              : childrenAges.length
                        })(
                          <Input
                            name='numberOfChildren'
                            disabled={getFieldValue('hasChildren') === 'No'}
                            placeholder={t('components.additionalInfo.childrenAtHomeMsg')}
                          />
                        )}
                      </FormItem>
                    </Col>
                  </Row>
                  {shouldRenderChildrenAgesDropdown && (
                    <Row gutter={24}>
                      {[
                        ...new Array(
                          parseInt(
                            isNil(childrenAges)
                              ? getFieldValue('numberOfChildren')
                              : childrenAges.length,
                            10
                          )
                        )
                      ].map((_, index) => (
                        <Col key={index} xs={{ span: 6 }}>
                          <FormItem>
                            {getFieldDecorator(`childrenAge-${index}`, {
                              initialValue: isNil(childrenAges)
                                ? null
                                : childrenAges[index]
                            })(
                              <Select
                                label={t('components.additionalInfo.childrenAgeLabel')}
                                name={`children-age-${index}`}
                                options={CHILDREN_AGES}
                                placeholder={t('components.additionalInfo.childrenAgePlaceholder')}
                              />
                            )}
                          </FormItem>
                        </Col>
                      ))}
                    </Row>
                  )}
                  <Row>
                    <FieldTitle>{t('components.additionalInfo.smokerTitle')}</FieldTitle>
                  </Row>
                  <Row>
                    <Col xs={{ span: 24 }} xl={{ span: 24 }}>
                      <FormItem>
                        {getFieldDecorator('smokerKind', {
                          initialValue: smokerKind
                        })(
                          <Select
                            placeholder={t('components.additionalInfo.smokerPlaceholder')}
                            options={smokerKinds}
                            getOptionName={getOptionName}
                            getOptionValue={getOptionValue}
                          />
                        )}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <FieldTitle>{t('components.additionalInfo.productTypeTitle')}</FieldTitle>
                  </Row>
                  <Row>
                    <Col xs={{ span: 24 }} xl={{ span: 24 }}>
                      <FormItem>
                        {getFieldDecorator('productTypes', {
                          initialValue: preferredProductCategories
                        })(
                          <Select
                            mode='multiple'
                            placeholder={t('components.additionalInfo.productTypePlaceholder')}
                            options={productCategories}
                            getOptionName={getOptionName}
                            getOptionValue={getOptionValue}
                          />
                        )}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row>
                    <FieldTitle>{t('components.additionalInfo.dietaryTitle')}</FieldTitle>
                  </Row>
                  <Row>
                    <Col xs={{ span: 24 }} xl={{ span: 24 }}>
                      <FormItem>
                        {getFieldDecorator('dietaryRestrictions', {
                          initialValue: dietaryPreference
                        })(
                          <Select
                            placeholder={t('components.additionalInfo.dietaryPlaceholder')}
                            options={dietaryPreferences}
                            getOptionName={getOptionName}
                            getOptionValue={getOptionValue}
                          />
                        )}
                      </FormItem>
                    </Col>
                  </Row>
                  <Row type='flex' justify='center'>
                    <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                      <ButtonAligner desktop={desktop}>
                        <StyledButton
                          type={isFieldsTouched() ? 'primary' : 'disabled'}
                          htmlType='submit'
                        >
                          {t('components.additionalInfo.updateButton')}
                        </StyledButton>
                      </ButtonAligner>
                    </Col>
                  </Row>
                </CustomForm>
              </Col>
            </Row>
          </Container>
        )}
      </Query>
    )
  }
}

export default withTranslation()(Form.create()(AdditionalInformationForm))
