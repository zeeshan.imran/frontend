import styled from 'styled-components'
import { Form, Col } from 'antd'
import Button from '../Button'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const CustomForm = styled(Form)``

export const FormItem = Form.Item

export const Container = styled.div`
  width: 100%;
`

export const ButtonAligner = styled.div`
  margin-top: 3.5rem;
  margin-bottom: ${({ desktop }) => (desktop ? 0 : `5rem`)};
`

export const CustomCol = styled(Col)`
  display: flex;
  flex-direction: column;
  align-items: ${({ desktop }) => (desktop ? 'left' : 'center')};
`

export const FieldTitle = styled.span`
  display: flex;
  font-family: ${family.primaryLight};
  font-size: 2rem;
  line-height: 1.6;
  letter-spacing: normal;
  color: ${colors.BLACK};
  margin-bottom: 0.6rem;
`

export const StyledButton = styled(Button)`
  width: 100%;
`
