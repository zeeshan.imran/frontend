import React, { useState } from 'react'
import { Form } from 'antd'
import * as Yup from 'yup'
import Select from '../Select'
import { withTranslation } from 'react-i18next'
import useResponsive from '../../utils/useResponsive/index'
import { COUNTRY_PHONE_CODES } from '../../utils/Constants'

const validateRequiredCountry = t => Yup.string().required()

const SelectCountry = ({
  handleChange,
  handleValidityChange = () => {},
  selectedValue,
  t
}) => {
  const [countryErrors, setCountryErrors] = useState('')
  const { desktop } = useResponsive()

  const onChange = (value = '') => {
    const isValid = validateRequiredCountry(t)
    handleValidityChange(isValid)

    if (!isValid) {
      setCountryErrors(t('errors.inputValidCountry'))
    } else {
      setCountryErrors('')
    }

    return handleChange(value)
  }

  return (
    <Form.Item
      help={countryErrors}
      validateStatus={countryErrors ? 'error' : 'success'}
    >
      <Select
        showSearch={desktop ? true : false}
        value={selectedValue}
        options={COUNTRY_PHONE_CODES}
        optionFilterProp='children'
        getOptionName={option => option.name}
        getOptionValue={option => option.name}
        onChange={onChange}
        placeholder={t(
          `components.createTasterAccount.forms.second.country.placeholder`
        )}
      />
    </Form.Item>
  )
}

export default withTranslation()(SelectCountry)
