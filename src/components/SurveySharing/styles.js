import Text from '../Text'
import styled from 'styled-components'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  text-align: ${({ isDesktop, centerAlign }) =>
    centerAlign ? 'center' : isDesktop ? 'left' : 'center'};
  margin: 2.4rem 0;
  background-color: transparent;
`

export const TitleText = styled(Text)`
  color: rgba(0, 0, 0, 0.85);
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
`
export const SimpleText = styled(Text)`
  display: block;
`

export const CopyButton = styled.span`
  cursor: pointer;
  color: ${colors.SURVEY_SHARING_BUTTON};
`
export const TextBold = styled(Text)`
  font-weight: bold;
  font-family: ${family.primaryBold};
`
