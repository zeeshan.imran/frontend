import React from 'react'
import { useTranslation } from 'react-i18next'
import { Container, TitleText, CopyButton, SimpleText } from './styles'
import { CopyToClipboard as Copy } from 'react-copy-to-clipboard'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import defaults from '../../defaults'

const SurveySharingComponent = ({
  isDesktop,
  survey,
  surveyEnrollment,
  centerAlign
}) => {
  const { t } = useTranslation()

  const availableCountry = defaults.countries.find(
    c => c.name === survey.country
  )
  const rewardCurrency = availableCountry || {}
  const currencyPrefix =
    defaults.currencyPrefixes[rewardCurrency.currency] || ''
  const currency =
    defaults.currencySuffixes[rewardCurrency.currency] || currencyPrefix

  let shareSurvey = survey
  if (survey.screeners && survey.screeners.length > 0) {
    shareSurvey = survey.screeners[0]
  }

  const amount = (currency || '$') + shareSurvey.referralAmount


  let isPaymentEnabled =
    shareSurvey.isPaypalSelected || shareSurvey.isGiftCardSelected

  return (
    <Container isDesktop={isDesktop} centerAlign={centerAlign}>
      <TitleText>
        {isPaymentEnabled && parseInt(shareSurvey.referralAmount, 10) ? (
          <SimpleText
            dangerouslySetInnerHTML={{
              __html: t(shareSurvey.customizeSharingMessage, {
                amount
              })
            }}
          />
        ) : null}
        <Copy
          text={`${window.location.origin}/survey/${
            shareSurvey.uniqueName
          }?referral=${surveyEnrollment}`}
          onCopy={() => {
            displaySuccessMessage(t(`containers.surveySharing.copiedMessage`))
          }}
        >
          <CopyButton>
            {t(`containers.surveySharing.shareTextMiddle`)}
          </CopyButton>
        </Copy>
      </TitleText>
    </Container>
  )
}

export default SurveySharingComponent
