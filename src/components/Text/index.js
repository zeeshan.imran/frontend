import React from 'react'
import { StyledText } from './styles'

const Text = ({ className, children, ...otherProps }) => (
  <StyledText className={className} {...otherProps}>
    {children}
  </StyledText>
)

export default Text
