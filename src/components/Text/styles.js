import styled from 'styled-components'
import colors from '../../utils/Colors'

export const StyledText = styled.span`
  &::selection {
    color: ${colors.LIGHT_OLIVE_GREEN};
    background-color: ${colors.TEXT_SELECTION_BACKGROUND};
  }
`
