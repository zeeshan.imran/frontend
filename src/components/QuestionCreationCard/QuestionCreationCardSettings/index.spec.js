import React from 'react'
import { shallow } from 'enzyme'
import QuestionCreationCardSettings from '.'

describe('QuestionCreationCardSettings', () => {
  let testRender
  let questionInfo

  beforeEach(() => {
    questionInfo = {
      questionIndex: 1,
      question: {
        id: 'question-1',
        type: 'choose-product',
        prompt: 'What is your email?'
      },
      mandatoryQuestion: ''
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render QuestionCreationCardSettings', async () => {
    testRender = shallow(
      <QuestionCreationCardSettings questionInfo={questionInfo} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
