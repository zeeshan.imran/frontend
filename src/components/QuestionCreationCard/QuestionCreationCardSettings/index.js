import React from 'react'
import QuestionCreationCardTypeSelect from '../../../containers/QuestionCreationCardTypeSelect'
import { Row, Col } from 'antd'

const QuestionCreationCardSettings = ({ ...questionInfo }) => (
  <Row gutter={24}>
    <Col lg={16} md={24}>
      <QuestionCreationCardTypeSelect {...questionInfo} />
    </Col>
  </Row>
)

export default QuestionCreationCardSettings
