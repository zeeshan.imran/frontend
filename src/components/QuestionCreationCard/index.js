import React, { memo } from 'react'
import Header from '../../containers/QuestionCreationCardHeader'
import SettingsSection from './QuestionCreationCardSettings'
import Body from '../../containers/QuestionCreationCardBody'
import { Container, HeaderContainer } from './styles'

const HIDDEN_QUESTIONS = new Set(['profile'])

const QuestionCreationCard = ({
  questionIndex,
  questionIndexInSection,
  question,
  mandatory,
  unsetMandatory,
  unsetPicture,
  allToMandatory,
  allToPicture
}) => {
  if (HIDDEN_QUESTIONS.has(question.type)) {
    return null
  }

  return (
    <Container className='question-creation-card'>
      <HeaderContainer>
        <Header
          questionIndex={questionIndex}
          questionIndexInSection={questionIndexInSection}
          question={question}
          mandatory={mandatory}
          unsetMandatory={unsetMandatory}
          unsetPicture={unsetPicture}
          allToMandatory={allToMandatory}
          allToPicture={allToPicture}
        />
        <SettingsSection
          questionIndex={questionIndex}
          question={question}
          mandatory={mandatory}
        />
      </HeaderContainer>
      <Body
        mandatory={mandatory}
        questionIndex={questionIndex}
        question={question}
      />
    </Container>
  )
}

export default memo(QuestionCreationCard)
