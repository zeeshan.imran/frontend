import styled from 'styled-components'
import Text from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  margin-bottom: 2.5rem;
`

export const Title = styled(Text)`
  font-size: 1.6rem;
  color: '#5e5e5e';
  font-family: ${family.primaryBold};
  align-items: center;
  display: flex;
`

export const HeaderContainer = styled.div`
  padding: 2.5rem 3.5rem 0.1rem;
`
