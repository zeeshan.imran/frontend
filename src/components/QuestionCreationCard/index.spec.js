import React from 'react'
import { mount } from 'enzyme'
import QuestionCreationCard from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import gql from 'graphql-tag'
jest.mock('../../containers/QuestionCreationCardTypeSelect')

jest.mock('../../containers/QuestionCreationCardTypeSelect')

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      questions
      uniqueQuestionsToCreate
    }
  }
`
const mockFinishSurvey = {
  request: {
    query: gql`
      mutation finishSurvey($input: EnrollmentStateInput) {
        finishSurvey(input: $input)
      }
    `,
    variables: { input: { surveyEnrollment: 'survey-enrollment-1' } }
  },
  result: () => {
    return true
  }
}

describe('QuestionCreationCard', () => {
  let testRender
  let skipToOptions
  let client

  beforeEach(() => {
    skipToOptions = [
      { value: 0, label: '1. 55' },
      { value: 1, label: '2. 55' },
      { value: null, label: null },
      { value: 3, label: 'Finish' },
      { value: 4, label: 'Reject' },
      { value: null, label: 'Remove skip' }
    ]

    client = createApolloMockClient({
      mocks: [mockFinishSurvey]
    })

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          uniqueQuestionsToCreate: [],
          questions: [
            {
              displayOn: 'tasting'
            }
          ]
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render QuestionCreationCard', async () => {
    const questionData = {
      addCustomOption: false,
      chartTitle: '',
      chartTopic: '',
      chartType: '',
      displayOn: 'middle',
      id: '5d5bf1f8d2b65c06d02e7aa3',
      nextQuestion: null,
      options: null,
      order: 2,
      pairs: null,
      pairsOptions: null,
      productsSkip: null,
      prompt: 'kh55',
      range: null,
      relatedQuestions: null,
      required: false,
      secondaryPrompt: '5kjhkjh5',
      type: 'email'
    }

    testRender = mount(
      <ApolloProvider client={client}>
        <QuestionCreationCard
          questionIndex={1}
          question={questionData}
          mandatory={false}
          skipToOptions={skipToOptions}
        />
      </ApolloProvider>
    )

    expect(testRender.find(QuestionCreationCard)).toHaveLength(1)
  })
})
