import React from 'react'
import { useTranslation } from 'react-i18next'
import { Modal, Select, Radio, Col, Form, Alert } from 'antd'
import * as Yup from 'yup'
import FieldLabel from '../FieldLabel'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { Formik } from 'formik'
import { Row, NotFound, AntdRadio } from './styles'

const validationSchema = Yup.object().shape({
  sendToMe: Yup.boolean().required(),
  userId: Yup.string()
    .typeError('Please select the user to send')
    .required('Please select the user to send')
})

const ExportSurveyModal = ({
  tKey,
  sendToOthers,
  users,
  loading,
  saving,
  onSearch,
  onOk,
  onCancel,
  isImpersonating = false
}) => {
  const user = getAuthenticatedUser()
  const { t } = useTranslation()
  return (
    <Formik
      enableReinitialize
      isInitialValid
      validationSchema={validationSchema}
      initialValues={{
        userId: user.id,
        sendToMe: true
      }}
      onSubmit={values => onOk(values.userId)}
    >
      {({
        values,
        setTouched,
        touched,
        errors,
        setFieldValue,
        handleSubmit
      }) => (
        <Modal
          title={t(`containers.${tKey}.alertModalTitle`)}
          visible
          okText={t(`containers.${tKey}.alertModalOkText`)}
          confirmLoading={saving}
          cancelButtonProps={{
            hidden: saving
          }}
          onOk={handleSubmit}
          onCancel={onCancel}
          closable={!saving}
        >
          {t(`containers.${tKey}.alertModalDescription`)}
          {!values.sendToMe &&
            (user.isSuperAdmin || isImpersonating) && (
              <Alert
                message='As administrator, you can send the report to any operator,
          power-user of any organization.'
                type='warning'
                showIcon
              />
            )}
          <Row gutter={24}>
            <Col md={24} lg={24}>
              <AntdRadio
                value={values.sendToMe}
                onChange={e => {
                  const nextSendToMe = e.target.value
                  setFieldValue('sendToMe', nextSendToMe)
                  setFieldValue('userId', nextSendToMe ? user.id : null)
                }}
              >
                <Radio value>
                  Send it to me
                  {isImpersonating
                    ? '(' + user.fullName + ')'
                    : ''}
                </Radio>
                {sendToOthers && <Radio value={false}>Send to other</Radio>}
              </AntdRadio>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col md={24} lg={24}>
              {!values.sendToMe && (
                <Form.Item
                  help={touched.userId && errors.userId}
                  validateStatus={
                    touched.userId && errors.userId ? 'error' : 'success'
                  }
                >
                  <FieldLabel label={t(`containers.${tKey}.selectUser`)}>
                    <Select
                      placeholder={t('placeholders.user')}
                      style={{ width: '100%' }}
                      showSearch
                      loading={loading}
                      value={values.userId}
                      defaultActiveFirstOption={false}
                      filterOption={false}
                      notFoundContent={
                        <NotFound>{t(`containers.${tKey}.notFound`)}</NotFound>
                      }
                      onSearch={keyword => {
                        onSearch(keyword)
                        setTouched({ userId: true })
                      }}
                      onChange={value => setFieldValue('userId', value)}
                      optionLabelProp='label'
                    >
                      {users.map(s => (
                        <Select.Option key={s.id} label={s.emailAddress}>
                          <div>{s.fullName}</div>
                          <div>{s.emailAddress}</div>
                        </Select.Option>
                      ))}
                    </Select>
                  </FieldLabel>
                </Form.Item>
              )}
            </Col>
          </Row>
        </Modal>
      )}
    </Formik>
  )
}

export default ExportSurveyModal
