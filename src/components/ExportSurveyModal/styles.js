import { Row as AntdRow } from 'antd'
import styled from 'styled-components'
import RadioGroup   from 'antd/lib/radio/group'
import colors from '../../utils/Colors'

export const Row = styled(AntdRow)`
  margin-top: 1rem;
`

export const NotFound = styled.div`
  padding: 2rem 1rem;
`
export const AntdRadio = styled(RadioGroup)`
.ant-radio-inner::after{
  background-color : ${colors.RADIO_BUTTON_BACKGROUND_COLOR}
}
`