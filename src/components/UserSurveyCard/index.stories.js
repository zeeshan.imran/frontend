import React from 'react'
import { storiesOf } from '@storybook/react'
import UserSurveyCard from './'
import moment from 'moment'
import { action } from '@storybook/addon-actions'

const surveyExample = {
  inProgress: false,
  numberOfProducts: 2,
  expirationDate: moment().add(1, 'days')
}

const anotherSurveyExample = {
  inProgress: true,
  numberOfProducts: 2,
  expirationDate: moment().add(5, 'hours')
}

storiesOf('UserSurveyCard', module)
  .add('User Survey Card Not in Progress', () => (
    <UserSurveyCard
      inProgress={surveyExample.inProgress}
      numberOfProducts={surveyExample.numberOfProducts}
      expirationDate={surveyExample.expirationDate}
      onClick={action('pressed')}
    />
  ))
  .add('User Survey Card In Progress', () => (
    <UserSurveyCard
      inProgress={anotherSurveyExample.inProgress}
      numberOfProducts={anotherSurveyExample.numberOfProducts}
      expirationDate={anotherSurveyExample.expirationDate}
      onClick={action('pressed')}
    />
  ))
