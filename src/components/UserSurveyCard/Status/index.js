import React from 'react'
import { Container, IndicatorContainer, StateIndicator, Text } from './styles'
import { useTranslation } from 'react-i18next'



const Status = ({ inProgress }) => {
  const { t } = useTranslation();

  const SURVEY_IN_PROGRESS = t('components.userSurvey.inProgress')
  const SURVEY_ONGOING = t('components.userSurvey.productsPurchase')
  return (
    <Container>
      <IndicatorContainer>
        <StateIndicator inProgress={inProgress} />
      </IndicatorContainer>
      <Text inProgress={inProgress}>
        {inProgress ? SURVEY_IN_PROGRESS : SURVEY_ONGOING}
      </Text>
    </Container>
  )
}

export default Status
