import React from 'react'
import { shallow } from 'enzyme'
import Status from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('Status', () => {
  let testRender
  let inProgress

  beforeEach(() => {
    inProgress = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Status', async () => {
    testRender = shallow(<Status inProgress={inProgress} />)
    expect(testRender).toMatchSnapshot()
  })
})
