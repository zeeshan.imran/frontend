import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  align-items: center;
  min-height: 3.2rem;
`

export const IndicatorContainer = styled.div`
  display: flex;
  width: 2.7rem;
  text-align: center;
  justify-content: center;
  margin-right: 1.7rem;
`

export const StateIndicator = styled.div`
  display: inline-block;
  width: 1.7rem;
  height: 1.7rem;
  border-radius: 50%;
  background-color: ${({ inProgress }) =>
    inProgress ? `${colors.LIGHT_OLIVE_GREEN}` : `${colors.PALE_ORANGE}`};
`

export const Text = styled.span`
  font-family: ${family.primaryBold};
  font-size: 1.6rem;
  letter-spacing: 0.05rem;
  color: ${({ inProgress }) =>
    inProgress ? `${colors.LIGHT_OLIVE_GREEN}` : `${colors.PALE_ORANGE}`};
`
