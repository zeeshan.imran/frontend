import React from 'react'
import moment from 'moment'
import { Container, Icon, Text } from './styles'
import ClockIcon from '../../../assets/svg/clock.svg'
import { useTranslation } from 'react-i18next'


const TimeLeft = ({ expirationDate, inProgress }) => {
  const { t } = useTranslation();

  const TIME_LEFT_SURVEY_IN_PROGRESS = t('components.userSurvey.timeLeftInProgress')
  const TIME_LEFT_SURVEY_ONGOING = t('components.userSurvey.timeLeftSurveyOngoing')

  const unit =
    moment(expirationDate).diff(moment(), 'days') <= 1 ? 'hours' : 'days'
  const timeToExpirationDate = moment(expirationDate).diff(moment(), unit)
  return (
    <Container>
      <Icon src={ClockIcon} />
      <Text>{`${timeToExpirationDate}${unit === 'days' ? 'd' : 'h'} ${
        inProgress ? TIME_LEFT_SURVEY_IN_PROGRESS : TIME_LEFT_SURVEY_ONGOING
      }`}</Text>
    </Container>
  )
}

export default TimeLeft
