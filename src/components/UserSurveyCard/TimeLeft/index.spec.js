import React from 'react'
import { mount } from 'enzyme'
import TimeLeft from '.'
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('TimeLeft', () => {
  let testRender
  let expirationDate
  let inProgress

  beforeEach(() => {
    expirationDate = null
    inProgress = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render TimeLeft', () => {
    testRender = mount(
      <TimeLeft expirationDate={expirationDate} inProgress={inProgress} />
    )
    expect(testRender.find(TimeLeft)).toHaveLength(1)
  })
})
