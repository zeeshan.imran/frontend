import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  align-items: center;
  min-height: 3.2rem;
`

export const Icon = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 2.7rem;
  height: 3.2rem;
  margin-right: 1.7rem;
`

export const Text = styled.span`
  font-family: ${family.primaryLight};
  font-size: 1.6rem;
  letter-spacing: 0.05rem;
  color: ${colors.SLATE_GREY};
`
