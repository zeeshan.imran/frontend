import React from 'react'
import { shallow } from 'enzyme'
import Products from '.';

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('Products', () => {
    let testRender
    let inProgress
    let numberOfProducts 
    
    beforeEach(() => {
        inProgress = true
        numberOfProducts = 1
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Products', async () => {
        testRender = shallow(
            <Products
                inProgress={inProgress}
                numberOfProducts={numberOfProducts}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});