import React from 'react'
import BoxIcon from '../../../assets/svg/box.svg'
import { Container, Icon, Text, Link } from './styles'
import { withTranslation } from 'react-i18next'

const Products = ({ numberOfProducts, inProgress, t }) => (
  <Container>
    <Icon src={BoxIcon} />
    <Text>
      {t('components.userSurveyCard.products', {
        number: numberOfProducts,
        plural: numberOfProducts > 1 ? 's' : ''
      })}
    </Text>
    {!inProgress && <Link>{t('components.userSurveyCard.seeShops')}</Link>}
  </Container>
)

export default withTranslation()(Products)
