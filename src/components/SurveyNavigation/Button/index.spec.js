import React from 'react'
import { shallow } from 'enzyme'
import Button from './index'

describe('SurveyNavigationButton', () => {
  let testRender
  let onClick

  beforeEach(() => {
    onClick = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyNavigation Button', async () => {
    testRender = shallow(
      <Button type='primary' disabled={false} onClick={onClick} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
