import styled from 'styled-components'
import Button from '../../Button'

export const Container = styled.div`
  width: ${({ fullWidth }) => (fullWidth ? '100%' : '25rem')};
  margin-bottom: 2rem;
  margin-right: ${({ fullWidth }) => (fullWidth ? '0' : '2.5rem')};
`
export const CustomButton = styled(Button)`
  width: 100%;
`
