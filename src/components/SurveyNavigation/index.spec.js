import React from 'react'
import { mount } from 'enzyme'
import SurveyNavigation from '.'
import Button from './Button'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

describe('SurveyNavigation', () => {
  let testRenderSkip
  let testRender
  let onOk
  let onSkip
  let okDisabled
  let skipDisabled
  let client

  beforeEach(() => {
    onOk = jest.fn()
    onSkip = jest.fn()
    okDisabled = false
    skipDisabled = false
    client = createApolloMockClient()

  })

  afterEach(() => {
    testRender.unmount()
    testRenderSkip.unmount()
  })

  test('should render SurveyNavigation', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
      <SurveyNavigation
        onOk={onOk}
        okText='Ok'
        okDisabled={okDisabled}
        skipText='Skip'
        skipDisabled={skipDisabled}
      />
      </ApolloProvider>
    )
    expect(testRender.find(Button)).toHaveLength(1)

    testRenderSkip = mount(
      <SurveyNavigation
        onOk={onOk}
        okText='Ok'
        okDisabled={okDisabled}
        onSkip={onSkip}
        skipText='Skip'
        skipDisabled={skipDisabled}
      />
    )
    expect(testRenderSkip.find(Button)).toHaveLength(1)

    testRenderSkip = mount(
      <SurveyNavigation
        onOk={onOk}
        okText='Ok'
        okDisabled={okDisabled}
        onSkip={onSkip}
        showSkip
        skipText='Skip'
        skipDisabled={skipDisabled}
      />
    )
    expect(testRenderSkip.find(Button)).toHaveLength(2)
  })
})
