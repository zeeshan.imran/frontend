import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-flow: ${({ mobile }) => (mobile ? 'column' : 'row wrap')};
  margin-bottom: 8rem;
`

export const Break = styled.div`
  width: 100%;
`

export const SkipProgressText = styled.div`
  display: block;
  padding: 0 0 0 0.25rem;
  line-height: 1.2;
  color: #888;
  font-size: 1.3rem;
  font-style: italic;
`
