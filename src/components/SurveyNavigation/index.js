import React from 'react'
import Button from './Button'
import { Container, SkipProgressText, Break } from './styles'
import useResponsive from '../../utils/useResponsive'

const SurveyNavigation = ({
  onOk,
  okText,
  okDisabled = false,
  showSubmit = true,
  showSkip,
  skipProgressText,
  onSkip,
  skipText
}) => {
  const { mobile } = useResponsive()
  return (
    <Container mobile={mobile}>
      {showSubmit && (
        <Button disabled={okDisabled} onClick={onOk}>
          {okText}
        </Button>
      )}
      {onSkip &&
        (showSkip ? (
          <Button type='secondary' onClick={onSkip}>
            {skipText}
          </Button>
        ) : (
          <React.Fragment>
            <Break />
            <SkipProgressText>{skipProgressText}</SkipProgressText>
          </React.Fragment>
        ))}
    </Container>
  )
}

export default SurveyNavigation
