import React from 'react'
import { Section } from '../styles'
import { withTranslation } from 'react-i18next'

const NoPhotosAvailable = ({ t, loading }) => (
  <Section>{!loading && t('components.surveyPhotos.noneAvailable')}</Section>
)

export default withTranslation()(NoPhotosAvailable)
