import React from 'react'
import {
  PhotoSection,
  TextContainer,
  IconContainer,
  FieldText,
  FieldTextHeader
} from '../styles'
import { withTranslation } from 'react-i18next'
import IconButton from '../../IconButton'

const PhotoView = ({ enrollment, setIsModalVisible, setSelectedAnswer, t }) => {
  let { id, paypalEmail, validation, user, answers } = enrollment
  
  if (!paypalEmail) {
    paypalEmail = `-`
  }

  let iconType = 'exclamation-circle'
  let toolTip = t('tooltips.notProcessedSurveyPhotos')

  if (validation === 'valid') {
    iconType = 'check-circle'
    toolTip = t('tooltips.validSurveyPhotos')
  }

  if (validation === 'invalid') {
    iconType = 'close-circle'
    toolTip = t('tooltips.invalidSurveyPhotos')
  }
  let hasImage = false

  if(answers){
    for (let count = 0; count < answers.length; count ++){
      const singleAnswer = answers[count]
      if (
        singleAnswer.value &&
        singleAnswer.value.length &&
        singleAnswer.question &&
        singleAnswer.question.type === 'upload-picture'
      ) {
        hasImage = true
        break
      }
    }
  }
    

  return (
    <PhotoSection
      onClick={() => {
        setSelectedAnswer(enrollment)
        setIsModalVisible(true)
      }}
    >
      <IconContainer state={validation}>
        {hasImage && <IconButton tooltip={`This enrollment has images`} type={`sketch`} size='2rem' /> }
        <IconButton tooltip={toolTip} type={iconType} size='2rem' />
      </IconContainer>
      <TextContainer>
        <FieldTextHeader>{id}</FieldTextHeader>
        <FieldText>{user && user.emailAddress}</FieldText>
        <FieldText>{paypalEmail}</FieldText>
      </TextContainer>
    </PhotoSection>
  )
}

export default withTranslation()(PhotoView)
