import React, { useState } from 'react'
import { Formik } from 'formik'
import { Form, Row } from 'antd'
import { withTranslation } from 'react-i18next'
import InputArea from '../../InputArea'
import RadioButton from '../../RadioButton'
import Carousel from '../../../components/Carousel'
import {
  FieldText,
  FieldTextImage,
  FieldTextImageContainer,
  TextContainer,
  ClearFix,
  CarouselImage,
  RowStarter,
  RadioButtonContainerLeft,
  RadioButtonContainerRight,
  InfoBox,
  Link
} from '../styles'

const PhotoValidationForm = ({
  selectedAnswer,
  setSelectedAnswer,
  canModify
}) => {
  if (!selectedAnswer) {
    return null
  }
  const singleEnrollement = selectedAnswer

  const { processed, validation, comment, answers } = singleEnrollement

  const [isImageValid, setIsImageValid] = useState(validation === 'valid')
  const [isProcessed, setIsProcessed] = useState(processed)
  const [invalidComment, setInvalidComment] = useState(comment)
  const [characterLimit] = useState(200)
  const [charactersRemaining, setCharactersRemaining] = useState(
    comment ? characterLimit - comment.length : characterLimit
  )
  const [step, setStep] = useState(0)

  let images = []
  let products = []
  let timeToAnswer = []

  answers &&
    answers.map(singleAnswer => {
      if (singleAnswer.product) {
        products.push(singleAnswer.product.name)
      }
      if (singleAnswer.timeToAnswer) {
        timeToAnswer.push(singleAnswer.timeToAnswer)
      }
      if (
        singleAnswer.value &&
        singleAnswer.value.length &&
        singleAnswer.question &&
        singleAnswer.question.type === 'upload-picture'
      ) {
        images.push(singleAnswer.value[0])
      }
      return singleAnswer
    })

  const getImageTitle = step => {
    return (
      <FieldTextImageContainer>
        <FieldTextImage>
          {products[step] ? products[step] : ' '}&nbsp;
        </FieldTextImage>
        <FieldTextImage>
          {timeToAnswer[step]
            ? `(Response time: ` +
              new Date(timeToAnswer[step]).getSeconds() +
              `s)`
            : ''}
        </FieldTextImage>
        <Link href={images[step]} target='_blank'>
          {' '}
          Open in new tab{' '}
        </Link>
      </FieldTextImageContainer>
    )
  }

  return (
    <Formik
      render={() => {
        return (
          <React.Fragment>
            <TextContainer>
              <FieldText>Enrollement Id: {singleEnrollement.id}</FieldText>
              <FieldText>
                User:{' '}
                {singleEnrollement.user && singleEnrollement.user.fullName
                  ? singleEnrollement.user.fullName
                  : `-`}
              </FieldText>
              <FieldText>
                Email:{' '}
                {singleEnrollement.user
                  ? singleEnrollement.user.emailAddress
                  : `-`}
              </FieldText>
              <FieldText>
                Paypal:{' '}
                {singleEnrollement.paypalEmail
                  ? singleEnrollement.paypalEmail
                  : `-`}
              </FieldText>
            </TextContainer>
            {images.length ? (
              <React.Fragment>
                <Carousel
                  afterSlide={currentIndex => {
                    setStep(currentIndex)
                  }}
                  slides={images}
                  renderSlideBottomContent={image => {
                    return <CarouselImage src={image} />
                  }}
                />
                <TextContainer>{getImageTitle(step)}</TextContainer>
              </React.Fragment>
            ) : (
              <TextContainer>
                <FieldText>Images:{'-'}</FieldText>
              </TextContainer>
            )}

            {canModify && (
              <RowStarter>
                <Row>
                  <RadioButtonContainerLeft>
                    <RadioButton
                      checked={isProcessed && isImageValid}
                      onClick={() => {
                        if (!isImageValid) {
                          setIsImageValid(true)
                          setIsProcessed(true)
                          singleEnrollement['validation'] = 'valid'
                          singleEnrollement['processed'] = true
                          setSelectedAnswer(singleEnrollement)
                        }
                      }}
                    >
                      Valid
                    </RadioButton>
                  </RadioButtonContainerLeft>
                  <RadioButtonContainerRight>
                    <RadioButton
                      checked={isProcessed && !isImageValid}
                      onClick={() => {
                        if (isImageValid || !isProcessed) {
                          setIsImageValid(false)
                          setIsProcessed(true)
                          singleEnrollement['validation'] = 'invalid'
                          singleEnrollement['processed'] = true
                          setSelectedAnswer(singleEnrollement)
                        }
                      }}
                    >
                      Invalid
                    </RadioButton>
                  </RadioButtonContainerRight>
                  <ClearFix />
                </Row>
              </RowStarter>
            )}
            {isProcessed && !isImageValid ? (
              <RowStarter>
                <Row>
                  <Form.Item>
                    <InputArea
                      name='comment'
                      value={invalidComment}
                      onChange={event => {
                        setInvalidComment(event.target.value)
                        singleEnrollement['comment'] = event.target.value
                        setSelectedAnswer(singleEnrollement)
                        setCharactersRemaining(
                          characterLimit - event.target.value.length
                        )
                      }}
                      label='Comment'
                      size='default'
                      maxLength={`200`}
                    />
                    <InfoBox>Characters Left: {charactersRemaining}</InfoBox>
                  </Form.Item>
                </Row>
              </RowStarter>
            ) : null}
          </React.Fragment>
        )
      }}
    />
  )
}

export default withTranslation()(PhotoValidationForm)
