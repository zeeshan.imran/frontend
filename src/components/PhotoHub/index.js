import React, { useState, useEffect } from 'react'
import gql from 'graphql-tag'
import { useTranslation } from 'react-i18next'
import { useQuery, useMutation } from 'react-apollo-hooks'
import PhotoView from './PhotoView'
import NoPhotosAvailable from './NoPhotosAvailable'
import PhotoValidationForm from './PhotoValidationForm'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import LoadingModal from '../LoadingModal'

import {
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsSuperAdmin
} from '../../utils/userAuthentication'

import { Container, ClearFix, Gallery, StyledModal } from './styles'

const PhotoHub = ({
  survey,
  searchedEmail,
  selectedFilter,
  filterCounts,
  setFilterCounts
}) => {
  const { t } = useTranslation()
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [selectedAnswer, setSelectedAnswer] = useState([])
  const [hasCards, setHasCards] = useState(false)
  const canModify =
    isUserAuthenticatedAsPowerUser() || isUserAuthenticatedAsSuperAdmin()
    
  useEffect(() => {
    hasCards && setHasCards(false)
    filterCounts && setFilterCounts(0)
  }, [selectedFilter, searchedEmail.trim()])

  const {
    data: { surveyEnrollementValidation },
    loading
  } = useQuery(SURVEY_ENROLLEMENT_VALIDATION, {
    variables: {
      surveyId: survey.id
    },
    fetchPolicy: 'network-only'
  })

  const updateAnswer = useMutation(UPDATE_VALIDATE_ANSWER)

  const setAnswerProcessed = async updatedAnswer => {
    let { id, processed, validation, comment } = updatedAnswer
    if (validation === 'valid') comment = ''
    await updateAnswer({
      variables: {
        input: {
          id,
          surveyId: survey.id,
          processed,
          validation,
          comment
        }
      }
    })

    displaySuccessMessage(
      t(`components.surveyPhotos.successPopup`, {
        state: validation
      })
    )
  }

  const surveyImages =
    surveyEnrollementValidation && surveyEnrollementValidation.enrollments

  const filteredImages =
    surveyImages &&
    surveyImages.length &&
    surveyImages.filter(singleEnrollment => {
      let createArea = false
      let { validation, user, processed } = singleEnrollment
      switch (selectedFilter) {
        case `valid`:
        case `invalid`:
          if (validation === selectedFilter) createArea = true
          break
        case `not_processed`:
          if (!processed) createArea = true
          break
        default:
          createArea = true
      }
      if (createArea && searchedEmail.trim()) {
        createArea =
          user &&
          user.emailAddress &&
          user.emailAddress
            .toLowerCase()
            .includes(searchedEmail.trim().toLowerCase())
      }
      if (createArea) {
        !hasCards && setHasCards(true)
        return true
      }
      return false
    })

  setFilterCounts(filteredImages ? filteredImages.length : 0)
  return (
    <Container>
      {loading && <LoadingModal visible />}
      {filteredImages && filteredImages.length ? (
        <React.Fragment>
          <Gallery>
            {filteredImages.map((singleEnrollment, index) => {
              return (
                <PhotoView
                  key={index}
                  enrollment={singleEnrollment}
                  setIsModalVisible={setIsModalVisible}
                  setSelectedAnswer={setSelectedAnswer}
                />
              )
            })}
          </Gallery>
          {!hasCards && <NoPhotosAvailable loading={loading} />}
          {isModalVisible ? (
            <StyledModal
              visible={isModalVisible}
              keyboard={false}
              closable={false}
              onCancel={() => {
                setSelectedAnswer([])
                setIsModalVisible(false)
              }}
              okButtonProps={{
                disabled: !canModify
              }}
              onOk={() => {
                if (selectedAnswer.processed) {
                  setAnswerProcessed(selectedAnswer)
                  setSelectedAnswer([])
                }
                setIsModalVisible(false)
              }}
            >
              <PhotoValidationForm
                canModify={canModify}
                selectedAnswer={selectedAnswer}
                setSelectedAnswer={setSelectedAnswer}
              />
            </StyledModal>
          ) : null}
        </React.Fragment>
      ) : (
        <NoPhotosAvailable loading={loading} />
      )}
      <ClearFix />
    </Container>
  )
}

export const UPDATE_VALIDATE_ANSWER = gql`
  mutation updateValidateSurveyEnrollement(
    $input: EditValidateSurveyEnrollement
  ) {
    updateValidateSurveyEnrollement(input: $input) {
      id
    }
  }
`

export const SURVEY_ENROLLEMENT_VALIDATION = gql`
  query surveyEnrollementValidation($surveyId: ID) {
    surveyEnrollementValidation(surveyId: $surveyId) {
      total
      enrollments {
        id
        paypalEmail
        processed
        validation
        comment
        user {
          id
          emailAddress
          fullName
        }
        answers {
          id
          value
          timeToAnswer
          product {
            id
            name
            photo
            reward
          }
          question {
            id
            type
          }
        }
      }
    }
  }
`

export default PhotoHub
