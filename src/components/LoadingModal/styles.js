import styled, { keyframes } from 'styled-components'
import BaseText from '../Text'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

const colorChange = keyframes`
  0% {
    color: ${colors.DARKER_OLIVE_GREEN};
  }

  50% {
    color: ${colors.LIGHT_OLIVE_GREEN};
  }

  100% {
    color: ${colors.DARKER_OLIVE_GREEN};
  }
`

export const Text = styled(BaseText)`
  font-family: ${family.primaryRegular};
  font-size: 3rem;
  animation: ${colorChange} 2s linear infinite;
  margin-top: 2rem;
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`
