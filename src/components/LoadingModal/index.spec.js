import React from 'react'
import { shallow } from 'enzyme'
import { Modal } from 'antd'
import LoadingModal from '.'



describe('LoadingModal', () => {
    let testRender

    beforeEach(() => {

    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render LoadingModal', () => {
        testRender = shallow(
            <LoadingModal
                text='Test Menu'
                visible={true}
            />
        )
        expect(testRender).toMatchSnapshot()
        
    })
})