import React from 'react'
import { shallow } from 'enzyme'
import OperatorPage from '.'

describe('OperatorPage', () => {
    let testRender
    let children

    beforeAll(() => {
        children = '<Container>Defualt page</Container>'
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render OperatorPage', async () => {
        testRender = shallow(<OperatorPage children={children} />)

        expect(testRender).toMatchSnapshot()
    })
})