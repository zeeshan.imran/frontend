import React from 'react'
import { shallow } from 'enzyme'
import PageContainer from '.'
import { Container } from './styles'

describe('PageContainer', () => {
  let testRender
  let children

  beforeEach(() => {
    children = <Container>Page here</Container>
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PageContainer', async () => {
    testRender = shallow(<PageContainer>{children}</PageContainer>)
    expect(testRender).toMatchSnapshot()
  })
})
 