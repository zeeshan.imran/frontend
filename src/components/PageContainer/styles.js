import styled from 'styled-components'

export const Container = styled.div`
  max-width: inherit;
  width: 88%;
  margin: 0 auto;
  @media (min-width:1500px){
    width: 94%;
    max-width:inherit;
  }
`
