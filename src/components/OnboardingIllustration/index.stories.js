// index.stories.js
// Story for a Component, with a state and a setter
import React from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import OnboardingIllustration from '.'
import illustration from '../../assets/svg/illus2.svg'

const Container = styled.div`
  width: 50%;
`

storiesOf('OnboardingIllustration', module)
  .add('Illustration and Text', () => (
    <Container>
      <OnboardingIllustration
        illustration={illustration}
        title={'Sample Title'}
        text={
          'Purchase selected products at your local store.  Dont worry, were paying!'
        }
      />
    </Container>
  ))
  .add('Illustration only', () => (
    <Container>
      <OnboardingIllustration illustration={illustration} />
    </Container>
  ))
