import React from 'react'
import { shallow } from 'enzyme'
import OnboardingIllustration from '.';

describe('OnboardingIllustration', () => {
    let testRender
    let illustration
    let title
    let text

    beforeEach(() => {
        illustration = 'illus1.svg'
        title = 'Welcome to Flavorwiki'
        text = 'Influence the food products of tomorrow while tasting the best products of today.  Get paid cash and earn cool rewards!'
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render OnboardingIllustration', async () => {

        testRender = shallow(

            <OnboardingIllustration
                illustration={illustration}
                title={title}
                text ={text}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});