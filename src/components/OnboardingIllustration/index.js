import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Container, Illustration, Title, Message } from './styles'

class OnboardingIllustration extends Component {
  render () {
    const { illustration, title, text } = this.props

    return (
      <Container>
        <Illustration src={illustration} />
        {title && <Title>{title}</Title>}
        {text && <Message>{text}</Message>}
      </Container>
    )
  }
}

OnboardingIllustration.propTypes = {
  image: PropTypes.string,
  illustration: PropTypes.string,
  title: PropTypes.string,
  text: PropTypes.string
}

export default OnboardingIllustration
