import styled from 'styled-components'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'
import { COMPONENTS_DEFAULT_MARGIN } from '../../utils/Metrics'

export const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  text-align: center;
  align-items: center;
  justify-content: center;
`

export const Illustration = styled.img`
  width: auto;
  height: 35rem;
`

export const Overlay = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.2);
`

const Text = styled.span`
  font-family: ${family.primaryLight};
  color: ${colors.WHITE};
  user-select: none;
`

export const Title = styled(Text)`
  font-size: 2.8rem;
  line-height: 5.2rem;
  letter-spacing: -0.04rem;
  margin-top: ${COMPONENTS_DEFAULT_MARGIN};
`

export const Message = styled(Text)`
  max-width: 40rem;
  font-size: 2rem;
  line-height: 3.2rem;
  margin-top: 1rem;
`
