import React from 'react'
import { StyledTabs, TabPane, ContentContainer } from './styles'

const Tabs = ({ tabs, renderTab, renderTabContent, className, ...rest }) => (
  <StyledTabs
    className={className}
    animated={false}
    tabBarStyle={{
      display: 'flex',
      justifyContent: 'center'
    }}
    defaultActiveKey='tab-0'
    {...rest}
  >
    {tabs.map((tab, index) => (
      <TabPane key={`tab-${index}`} tab={renderTab(tab)}>
        <ContentContainer>{renderTabContent(tab)}</ContentContainer>
      </TabPane>
    ))}
  </StyledTabs>
)

export default Tabs
