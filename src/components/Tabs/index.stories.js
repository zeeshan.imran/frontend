import React from 'react'
import { storiesOf } from '@storybook/react'
import Tabs from './'
import Person from '../SvgIcons/Person'
import AdditionalInfo from '../SvgIcons/AdditionalInfo'
import Password from '../SvgIcons/Password'
import Privacy from '../SvgIcons/Privacy'

const TABS = {
  1: {
    icon: <Person />,
    title: 'Personal Detail',
    content: <div>Personal Detail Content</div>
  },
  2: {
    icon: <AdditionalInfo />,
    title: 'Additional Information',
    content: <div>Additional Information Content</div>
  },
  3: {
    icon: <Password />,
    title: 'Change Password',
    content: <div>Change Password Content</div>
  },
  4: {
    icon: <Privacy />,
    title: 'Privacy',
    content: <div>Privacy Content</div>
  }
}

storiesOf('Tabs', module).add('Tabs Example', () => (
  <Tabs
    tabsIds={[1, 2, 3, 4]}
    renderTab={id => {
      const { title } = TABS[id]
      return <div>{title}</div>
    }}
    renderTabContent={id => {
      return TABS[id].content
    }}
  />
))
