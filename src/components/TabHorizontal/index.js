import React from 'react'
import { Tabs, Badge } from 'antd'
import { Container, TabText } from './styles'

function TabHorizontal (tabPages, contents, f, tabId, tabBarExtraContent) {
  const TabPane = Tabs.TabPane

  return (
    <Container>
      <Tabs
        defaultActiveKey='1'
        activeKey={tabId}
        onChange={f}
        tabBarExtraContent={tabBarExtraContent}
      >
        {tabPages.map(tabPage => (
          <TabPane
            tab={
              <TabText>
                {tabPage.name}
                {tabPage.count && <Badge count={tabPage.count} />}
              </TabText>
            }
            key={tabPage.path}
          >
            {contents[tabPages.indexOf(tabPage)]}
          </TabPane>
        ))}
      </Tabs>
    </Container>
  )
}

export default TabHorizontal
