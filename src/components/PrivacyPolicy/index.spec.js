import React from 'react'
import { shallow } from 'enzyme'
import PrivacyPolicy from '.'

describe('PrivacyPolicy', () => {
  let testRender
  let goToTermsOfUse
  let isOperator

  beforeEach(() => {
    goToTermsOfUse = () => {
      history.push('/terms-of-use')
    }

    isOperator = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PrivacyPolicy', async () => {
    testRender = shallow(
      <PrivacyPolicy goToTermsOfUse={goToTermsOfUse} isOperator={isOperator} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
