import React from 'react'
import { shallow } from 'enzyme'
import Body from '.'

describe('Body', () => {
  let testRender
  beforeEach(() => {
    
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Body', async () => {
    testRender = shallow(
      <Body
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
