import React from 'react'
import { Container } from './styles'
import Header from './Header'
import Body from './Body'
import Footer from './Footer'

const PrivacyPolicy = ({ goToTermsOfUse, isOperator }) => {
  return (
    <Container>
      {!isOperator && <Header />}
      <Body />
      {!isOperator && <Footer goToTermsOfUse={goToTermsOfUse} />}
    </Container>
  )
}

export default PrivacyPolicy
