import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${colors.WHITE};
  margin-bottom: 2.5rem;
  padding: 2.5rem 3.5rem 0.1rem;
  align-items: center;
`
