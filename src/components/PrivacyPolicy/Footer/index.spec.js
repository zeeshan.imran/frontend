import React from 'react'
import { shallow } from 'enzyme'
import Footer from '.'

describe('Footer', () => {
  let testRender
  let goToTermsOfUse

  beforeEach(() => {
    goToTermsOfUse = () => {
      history.push('/terms-of-use')
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PrivacyPolicy  Footer', async () => {
    testRender = shallow(<Footer goToTermsOfUse={goToTermsOfUse} />)
    expect(testRender).toMatchSnapshot()
  })
})
