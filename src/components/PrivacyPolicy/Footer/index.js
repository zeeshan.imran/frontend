import React from 'react'
import { Container } from './styles'
import { Button } from 'antd'

const Footer = ({goToTermsOfUse}) => {
  return <Container>
    <Button onClick={goToTermsOfUse}>
      Terms Of Use
    </Button>
  </Container>
}

export default Footer