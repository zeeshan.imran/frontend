import React from 'react'
import { mount } from 'enzyme'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import SurveyLogin from './index'
import OpenInstructions from './OpenInstructions'
import PublicInstructions from './PublicInstructions'
import EmailInstructions from './EmailInstructions'
import { LoggedOut, Valid, Invalid } from './SelectedInstructions'
import SurveyLoginLayout from '../SurveyLoginLayout'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('components/SurveyLogin', () => {
  let testRender
  let submit

  beforeEach(() => {
    submit = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('render the public survey', () => {
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <SurveyLogin handleSubmit={submit} authorizationType='public' />
      </Router>
    )

    expect(testRender.find(OpenInstructions)).toHaveLength(1)
  })

  test('render the public, recaptcha survey', () => {
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <SurveyLogin authorizationType='public' requireRecaptcha />
      </Router>
    )

    expect(testRender.find(PublicInstructions)).toHaveLength(1)
  })

  test('render the enrollment survey', () => {
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <SurveyLogin authorizationType='enrollment' requireRecaptcha />
      </Router>
    )

    expect(testRender.find(EmailInstructions)).toHaveLength(0)
  })

  test('render the selected but logged out survey', () => {
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <SurveyLogin
          authorizationType='selected'
          authorizationState='loggedOut'
        />
      </Router>
    )

    expect(testRender.find(LoggedOut)).toHaveLength(1)
    // should hideSubmit button if the taster is logged out
    expect(testRender.find(SurveyLoginLayout).prop('hideSubmit')).toBe(true)
  })

  test('render the selected and valid survey', () => {
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <SurveyLogin authorizationType='selected' authorizationState='valid' />
      </Router>
    )

    expect(testRender.find(Valid)).toHaveLength(1)
  })

  test('render the selected but invalid survey', () => {
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <SurveyLogin
          authorizationType='selected'
          authorizationState='invalid'
        />
      </Router>
    )

    expect(testRender.find(Invalid)).toHaveLength(1)
    // should hideSubmit button if the session is invalid
    expect(testRender.find(SurveyLoginLayout).prop('hideSubmit')).toBe(true)
  })

  test('render the unknown survey', () => {
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <SurveyLogin authorizationType='unknown' />
      </Router>
    )
  })
})
