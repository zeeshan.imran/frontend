import styled from 'styled-components'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const SimpleText = styled(Text)`
  -webkit-font-smoothing: antialiased;
  font-family: ${family.NotoSansKRLight};
  color: ${colors.BLACK};
  text-align: center;
  margin-top: 2.3rem;
`

export const MainText = styled(Text)`
  -webkit-font-smoothing: antialiased;
  font-family: ${family.NotoSansKRLight};
  color: ${colors.BLACK};
  margin-top: 2.3rem;
`