import React from 'react'
import Text from '../Text'
import { withTranslation } from 'react-i18next'

const PublicInstructions = ({ t }) => (
  <Text>{t('components.surveyLogIn.notARobot')}</Text>
)

export default withTranslation()(PublicInstructions)
