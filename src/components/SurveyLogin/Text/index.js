import React from 'react'
import useResponsive from '../../../utils/useResponsive'

import { Container, Text as StyledText } from './styles'

const Text = ({ children }) => {
  const { mobile } = useResponsive()
  return (
    <Container centered={mobile}>
      <StyledText centered={mobile}>{children}</StyledText>
    </Container>
  )
}

export default Text
