import React from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Text from '../../Text'
import LinkText from '../../LinkText'
import { SocialButtonsContainer, DasboardButton } from './styles'

let LoggedOut = ({ history }) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.surveyAvailable')}</Text>
      <Text>{t('components.surveyLogIn.accessLogin')}</Text>
      <LinkText onClick={() => history.push('/onboarding')}>
        {t('components.surveyLogIn.access')}
      </LinkText>
    </React.Fragment>
  )
}
LoggedOut = withRouter(LoggedOut)

let OnlyLoggedIn = ({ history }) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.onlyWithAccount')}</Text>
      <LinkText onClick={() => history.push('/onboarding')}>
        {t('components.surveyLogIn.access')}
      </LinkText>
      <Text>{t('or')}</Text>
      <LinkText onClick={() => history.push('/create-account/taster/welcome')}>
        {t('requestTasterAccount')}
      </LinkText>
      
    </React.Fragment>
  )
}
OnlyLoggedIn = withRouter(OnlyLoggedIn)

const Valid = () => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.welcome')}</Text>
      <Text>{t('components.surveyLogIn.pressToStart')}</Text>
    </React.Fragment>
  )
}

const Invalid = () => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.surveyAvailable')}</Text>
      <Text>{t('components.surveyLogIn.notSelectedToParticipate')}</Text>
    </React.Fragment>
  )
}

const Retake = () => {
  const { t } = useTranslation()
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Text>{t('survey.already_taken')}</Text>
      {/* <Text>{t('components.retakes.header')}</Text>
      <Text>{t('components.retakes.subHeader')}</Text> */}
      {REACT_APP_THEME === 'default' && (
        <SocialButtonsContainer>
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        </SocialButtonsContainer>
      )}
    </React.Fragment>
  )
}

const Expired = () => {
  const { t } = useTranslation()
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.surveyExpired')}</Text>
      {REACT_APP_THEME === 'default' && (
        <SocialButtonsContainer>
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        </SocialButtonsContainer>
      )}
    </React.Fragment>
  )
}

const Suspended = pauseText => {
  const { t } = useTranslation()
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Text>
        <span dangerouslySetInnerHTML={{ __html: pauseText.pauseText }} />
      </Text>
      {REACT_APP_THEME === 'default' && (
        <SocialButtonsContainer>
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        </SocialButtonsContainer>
      )}
    </React.Fragment>
  )
}

export {
  LoggedOut,
  Valid,
  Invalid,
  Retake,
  Expired,
  Suspended,
  OnlyLoggedIn
}
