import React from 'react'
import SectionTitle from '../../SectionTitle'

import { Container } from './styles'

const Title = ({ title, fullWidth }) => (
  <Container fullWidth={fullWidth}>
    <SectionTitle title={title} />
  </Container>
)

export default Title
