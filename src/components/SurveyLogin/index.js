import React from 'react'
import SurveyLoginLayout from '../SurveyLoginLayout'
import OpenInstructions from './OpenInstructions'
import PublicInstructions from './PublicInstructions'
import {
  LoggedOut as LoggedOutInstructions,
  Invalid as InvalidInstructions,
  Valid as ValidInstructions,
  Retake as RetakeInstructions,
  Expired as ExpiredInstructions,
  Suspended as SuspendedInstructions,
  OnlyLoggedIn as OnlyLoggedInInstructions
} from './SelectedInstructions'
import { isUserAuthenticatedAsTaster } from '../../utils/userAuthentication'


const SurveyLogin = ({
  surveyTitle,
  canSubmit,
  handleChange,
  handleSubmit,
  authorizationType,
  authorizationState,
  requireRecaptcha,
  buttonLabel,
  loginText,
  pauseText,
  isSurveyExpired,
  isSurveySuspended,
  allowedDaysToFillTheTasting,
  defaultEmailValue,
  emailDisabled
}) => {

  const isAuthenticatedUser = isUserAuthenticatedAsTaster()

  let typeOfPage = authorizationType
  if (isSurveySuspended) {
    typeOfPage = 'survey.suspended'
  } else if (isSurveyExpired) {
    typeOfPage = 'enrollment.expired'
  } else if (['rejected', 'finished', 'retake'].includes(authorizationState)) {
    typeOfPage='retake'
  } else if (
    authorizationType === 'selected'
  ) {
    typeOfPage = `${authorizationType}.${authorizationState}`
  } 
  let loginProps = {}
  switch (typeOfPage) {
    case 'public':
      loginProps = {
        title: surveyTitle,
        description: requireRecaptcha ? (
          <PublicInstructions />
        ) : (
          <OpenInstructions />
        ),
        requireEmail: false,
        requireRecaptcha
      }
      break
    case 'enrollment':
      if(isAuthenticatedUser){
        loginProps = {
          title: surveyTitle,
          description: (
            <OpenInstructions />
          ),
          requireEmail: false,
          requireRecaptcha
        }
      } else{
        loginProps = {
          title: surveyTitle,
          description: <OnlyLoggedInInstructions />,
          requireEmail: false,
          requireRecaptcha: false,
          hideSubmit: true
        }
      }
      
      break
    case 'survey.suspended':
      loginProps = {
        title: surveyTitle,
        description: <SuspendedInstructions pauseText={pauseText} />,
        requireEmail: false,
        requireRecaptcha: false,
        hideSubmit: true
      }
      break
    case 'enrollment.expired':
      loginProps = {
        title: surveyTitle,
        description: <ExpiredInstructions />,
        requireEmail: false,
        requireRecaptcha: false,
        hideSubmit: true
      }
      break
    case 'selected.loggedOut':
      loginProps = {
        title: surveyTitle,
        description: <LoggedOutInstructions />,
        requireEmail: false,
        requireRecaptcha: false,
        hideSubmit: true
      }
      break
    case 'selected.valid':
      loginProps = {
        title: surveyTitle,
        description: <ValidInstructions />,
        requireEmail: false,
        requireRecaptcha: false
      }
      break
    case 'selected.invalid':
      loginProps = {
        title: surveyTitle,
        description: <InvalidInstructions />,
        requireEmail: false,
        requireRecaptcha: false,
        hideSubmit: true
      }
      break
    case 'retake':
      loginProps = {
        title: surveyTitle,
        description: <RetakeInstructions />,
        requireEmail: false,
        requireRecaptcha: false,
        hideSubmit: true
      }
      break
    default:
      break
  }

  return (
    <SurveyLoginLayout
      handleSubmit={handleSubmit}
      buttonLabel={buttonLabel}
      defaultEmailValue={defaultEmailValue}
      emailDisabled={emailDisabled}
      {...loginProps}
    />
  )
}

export default SurveyLogin
