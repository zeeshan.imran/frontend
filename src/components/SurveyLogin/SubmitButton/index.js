import React from 'react'

import { Container, Button } from './styles'

const SubmitButton = ({ fullWidth, disabled, submit, buttonLabel }) => (
  <Container fullWidth={fullWidth}>
    <Button
      fullWidth={fullWidth}
      type={disabled ? 'disabled' : 'primary'}
      onClick={submit}
    >
      {buttonLabel}
    </Button>
  </Container>
)

export default SubmitButton
