import React from 'react'
import { mount } from 'enzyme'
import SubmitButton from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('SubmitButton', () => {
  let testRender
  let fullWidth
  let disabled
  let submit
  let buttonLabel

  beforeEach(() => {
    fullWidth = true
    disabled = true
    submit = jest.fn()
    buttonLabel = 'submit'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SubmitButton disabled false', () => {
    testRender = mount(
      <SubmitButton
        fullWidth={fullWidth}
        disabled={false}
        submit={submit}
        buttonLabel={buttonLabel}
      />
    )
    expect(testRender.find(SubmitButton)).toHaveLength(1)
  })

  test('should render SubmitButton disabled true', () => {
    testRender = mount(
      <SubmitButton
        fullWidth={fullWidth}
        disabled={disabled} 
        submit={submit}
        buttonLabel={buttonLabel}
      />
    )
    expect(testRender.find(SubmitButton)).toHaveLength(1)
  })
})
