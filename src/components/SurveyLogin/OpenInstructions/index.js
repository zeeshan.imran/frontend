import React from 'react'
import Text from '../Text'
import { withTranslation } from 'react-i18next'

const OpenInstructions = ({ t }) => (
  <React.Fragment>
    <Text>{t('components.surveyLogIn.welcome')}</Text>
    <Text>{t('components.surveyLogIn.pressToStart')}</Text>
  </React.Fragment>
)

export default withTranslation()(OpenInstructions)
