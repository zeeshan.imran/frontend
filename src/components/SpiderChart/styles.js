import styled from 'styled-components'

export const Container = styled.div`
  max-width: 40rem;
  width: 90%;
  padding: 0 1.5rem 10rem 1.5rem;
`
