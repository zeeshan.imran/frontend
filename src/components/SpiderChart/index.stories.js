import React from 'react'
import { storiesOf } from '@storybook/react'
import SpiderChart from './'

const labels = [
  'Cocoa',
  'Sweet',
  'Creamy',
  'Caramel',
  'Dairy Sour',
  'Malty',
  'Fruity'
]

const values = [
  {
    name: 'Cadbury Dairy Milk tablet',
    data: [7.221, 7.0522, 6.9247, 4.5103, 3.2093, 4.0402, 1.7742]
  },
  {
    name: 'Galaxy Smooth Milk tablet',
    data: [6.5105, 6.6667, 7.4336, 5.3425, 2.7716, 4.8701, 1.4351]
  }
]

storiesOf('SpiderChart', module).add('Story', () => (
  <SpiderChart labels={labels} values={values} />
))
