import React from 'react'
import { Radar } from 'react-chartjs-2'
import { Container } from './styles'

const colorMapper = [
  {
    backgroundColor: 'rgba(138, 186, 92, 0.2)',
    borderColor: 'rgba(138, 186, 92, 1)',
    pointBackgroundColor: 'rgba(138, 186, 92, 1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(138, 186, 92, 1)'
  },
  {
    backgroundColor: 'rgba(35, 123, 159, 0.2)',
    borderColor: 'rgba(35, 123, 159, 1)',
    pointBackgroundColor: 'rgba(35, 123, 159, 1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(35, 123, 159, 1)'
  },
  {
    backgroundColor: 'rgba(255, 169, 83, 0.2)',
    borderColor: 'rgba(255, 169, 83, 1)',
    pointBackgroundColor: 'rgba(255, 169, 83, 1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(255, 169, 83, 1)'
  }
]

const formatTooltip = tooltip => {
  if (tooltip.opacity !== 0) {
    const label =
      tooltip.body[0].lines && tooltip.body[0].lines.length
        ? tooltip.body[0].lines[0].split(':')[0]
        : 'label'
    tooltip.body[0].lines = [`${label}`]
    tooltip.labelColors[0].backgroundColor = tooltip.labelColors[0].borderColor
  }
  return tooltip
}

const SpiderChart = ({ labels = [], values = [], question = [] }) => {
  const datasets = values.map((value, index) => {
    const label = value.name
    const data = value.data
    return { label, data, ...colorMapper[index] }
  })
  const data = {
    labels,
    datasets
  }

  let maxLimiter = 10

  if (question && question.type !== 'paired-questions' && question.range.max) {
    maxLimiter = question.range.max
  }

  const options = {
    tooltips: {
      enabled: true,
      custom: formatTooltip
    },
    scale: {
      ticks: {
        beginAtZero: true,
        min: 0,
        max: maxLimiter
      }
    },
    aspectRatio: 1
  }

  return (
    <Container>
      <Radar data={data} height={null} width={null} options={options} />
    </Container>
  )
}

export default SpiderChart
