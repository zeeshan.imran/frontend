import React from 'react'
import { mount } from 'enzyme'
import SpiderChart from '.'
import { Radar } from 'react-chartjs-2'

describe('SpiderChart', () => {
  let testRender
  let labels
  let values
  let question
  beforeEach(() => {
    labels = []
    question = {
      type: 'paired-questions',
      range: {
        max: 10
      }
    }
    values = [
      {
        name: 'val 1',
        data: [300, 50, 100]
      }
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SpiderChart', async () => {
    testRender = mount(<SpiderChart labels={labels} values={values} question={question} />)
    expect(testRender.find(SpiderChart)).toHaveLength(1)
  })

    test('format tooltip', () => {
    // create a tooltip and the expect result after fomating
    const tooltip = {
      opacity: 1,
      body: [{ lines: ['label-1:tooltip'] }],
      labelColors: [{ backgroundColor: 'not green', borderColor: 'green' }]
    }
    const expectedTooltip = {
      opacity: 1,
      body: [{ lines: ['label-1'] }],
      labelColors: [{ backgroundColor: 'green', borderColor: 'green' }]
    }


    
    testRender = mount(<SpiderChart labels={labels} values={values} question={question} />)
    // extract the FormatTooltip function
    const radarOptions = testRender.find(Radar).props('options')
    const formatTooltip = radarOptions.options.tooltips.custom
    // call the formatTooltip function
    const formattedTooltip = formatTooltip(tooltip)
    // compare the function result with the needed result
    //expect(formattedTooltip).toBe(expectedTooltip)
    expect(JSON.stringify(formattedTooltip)).toEqual(JSON.stringify(expectedTooltip))

  })

})
