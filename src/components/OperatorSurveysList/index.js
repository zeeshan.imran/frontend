import React, { useState } from 'react'
import { Table as AntTable, Tooltip, Dropdown, Menu } from 'antd'
import CopySurveyUrlButton from '../../containers/CopySurveyUrl'
import EditSurveyButton from '../../containers/EditSurveyButton'
import PauseSurveyButton from '../../containers/PauseSurveyButton'
import PhotosSurveyButton from '../../containers/PhotosSurveyButton'
import DeleteSurveyButton from '../../containers/DeleteSurveyButton'
import {
  getAuthenticatedOrganization,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsSuperAdmin,
  isUserAuthenticatedAsOperator
} from '../../utils/userAuthentication'
import IconButton from '../../components/IconButton'
import {
  FieldText,
  FieldContainer,
  TitleContainer,
  SurveyName,
  ActionsContainer,
  SurveyNameContainer,
  Item
} from './styles'
import EditDraftButton from '../../containers/EditDraftButton'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'
import CloneSurvey from '../../containers/CloneSurvey'
import ExportSurveySharing from '../../containers/ExportSurveySharing'
import ImportProductIncentives from '../../containers/ImportProductIncentives'
import ExportProductIncentives from '../../containers/ExportProductIncentives'
import ImportSurveyShares from '../../containers/ImportSurveyShares'
import ImportGiftCardIncentives from '../../containers/ImportGiftCardIncentives'
import ExportGiftCardIncentives from '../../containers/ExportGiftCardIncentives'
import ImportGiftCardShares from '../../containers/ImportGiftCardShares'
import ExportGiftCardShares from '../../containers/ExportGiftCardShares'
import StatsButton from './StatsButton'
import ShareStatsButton from './ShareStatsButton'
import FunnelButton from './FunnelButton'
import { useTranslation } from 'react-i18next'
import ExportSurveyButton from '../../containers/ExportSurveyButton'
import ExportSurveyPhotosButton from '../../containers/ExportSurveyPhotosButton'
import moment from 'moment'
import SurveyAnalysisButton from '../../containers/SurveyAnalysisButton'

const getSurveyStateSuffix = state => {
  switch (state) {
    case 'draft':
      return ' (Draft)'
    case 'suspended':
      return ' (Suspended)'
    case 'deprecated':
      return ' (Deleted)'
    default:
      return ''
  }
}

const checkSpecialVisibility = () => {
  return (
    isUserAuthenticatedAsSuperAdmin() ||
    isUserAuthenticatedAsPowerUser() ||
    isUserAuthenticatedAsOperator()
  )
}

const OperatorSurveysList = ({
  surveys,
  organizations,
  total,
  page,
  onChangePage,
  showActions,
  loading,
  onClickRow = () => {}
}) => {
  const [openDropdownId, setOpenDropdownId] = useState(false)
  const { t } = useTranslation()
  const columns = [
    {
      title: t('components.operatorSurveys.title'),
      dataIndex: 'listSurveys',
      render: (_, survey) => {
        const { minimumProducts, products, isScreenerOnly } = survey

        let typeOfProducts = t('components.operatorSurveys.singleProduct')
        if (products && survey.products.length > 1) {
          typeOfProducts = t('components.operatorSurveys.multipleProducts')
          if (minimumProducts > 1) {
            typeOfProducts = t('components.operatorSurveys.multipleTastings')
          }
        }
        if (isScreenerOnly) {
          typeOfProducts = t('components.operatorSurveys.screenerOnly')
        }
        return (
          <FieldContainer>
            <TitleContainer>
              <SurveyNameContainer>
                <Tooltip
                  placement='topLeft'
                  title={survey.name}
                  mouseEnterDelay={0.25}
                  overlayStyle={{ maxWidth: '40vw' }}
                >
                  <SurveyName>{survey.name}</SurveyName>
                </Tooltip>
                <FieldText>{getSurveyStateSuffix(survey.state)}</FieldText>
              </SurveyNameContainer>
              <FieldText>{typeOfProducts}</FieldText>
            </TitleContainer>
          </FieldContainer>
        )
      }
    },
    {
      title: t('components.operatorSurveys.title'),
      dataIndex: 'surveyList',
      render: (_, survey) => {
        const dateFormat = 'YYYY-MM-DD-HH:mm'
        const created = moment(survey.createdAt).format(dateFormat)
        const updated = moment(survey.updatedAt).format(dateFormat)
        return (
          <FieldContainer>
            <TitleContainer>
              <FieldText small>{`Created on: ${created}`}</FieldText>
              <FieldText small>{`Updated on: ${updated}`}</FieldText>
            </TitleContainer>
          </FieldContainer>
        )
      }
    }
  ]

  const actionsColumn = {
    title: t('components.operatorSurveys.actions'),
    dataIndex: 'actions',
    render: (_, survey, __) => {
      const showAll = survey.owner === getAuthenticatedOrganization()

      if (!showAll) {
        return (
          <ActionsContainer>
            {['draft', 'active', 'suspended'].includes(survey.state) && (
              <StatsButton surveyId={survey.id} />
            )}
          </ActionsContainer>
        )
      }
      const exportEligible = () => {
        if (survey.screeners.length) {
          return parseInt(survey.screeners[0].referralAmount, 10)
        } else {
          return parseInt(survey.referralAmount, 10)
        }
      }

      const hasGiftCard = () => {
        if (survey.screeners.length) {
          return survey.screeners[0].isGiftCardSelected
        } else {
          return survey.isGiftCardSelected
        }
      }

      const hasPayPalSelected = () => {
        if (survey.screeners.length) {
          return survey.screeners[0].isPaypalSelected
        } else {
          return survey.isPaypalSelected
        }
      }

      const closeDropdown = () => setOpenDropdownId(false)

      const menu = (
        <React.Fragment>
          <Menu onClick={closeDropdown}>
            <Menu.ItemGroup>
              <Item
                onItemHover={() => {}}
                onClick={closeDropdown}
                rootPrefixCls='ant-menu'
                key='0'
              >
                <CloneSurvey
                  state={survey.state}
                  surveyId={survey.id}
                  onMenuItemClick={closeDropdown}
                />
              </Item>
              {checkSpecialVisibility() && (
                <Item
                  onItemHover={() => {}}
                  onClick={closeDropdown}
                  rootPrefixCls='ant-menu'
                  key='1'
                >
                  <ShareStatsButton
                    organizations={organizations}
                    survey={survey}
                    onMenuItemClick={closeDropdown}
                  />
                </Item>
              )}
              <Item
                onItemHover={() => {}}
                onClick={closeDropdown}
                rootPrefixCls='ant-menu'
                key='2'
              >
                <ExportSurveyButton
                  surveyId={survey.id}
                  onMenuItemClick={closeDropdown}
                />
              </Item>
              <Item
                onItemHover={() => {}}
                onClick={closeDropdown}
                rootPrefixCls='ant-menu'
                key='9'
              >
                <ExportSurveyPhotosButton
                  state={survey.state}
                  surveyId={survey.id}
                  onMenuItemClick={closeDropdown}
                />
              </Item>
            </Menu.ItemGroup>
            {checkSpecialVisibility() && (
              <Menu.ItemGroup>
                <Menu.Divider key='22' rootPrefixCls='ant-menu' />
                <Menu.Item
                  onItemHover={() => {}}
                  onClick={closeDropdown}
                  rootPrefixCls='ant-menu'
                  key='3'
                >
                  <PhotosSurveyButton
                    surveyId={survey.id}
                    onMenuItemClick={closeDropdown}
                  />
                </Menu.Item>
                <Menu.Item
                  onItemHover={() => {}}
                  onClick={closeDropdown}
                  rootPrefixCls='ant-menu'
                  key='4'
                >
                  <SurveyAnalysisButton
                    surveyId={survey.id}
                    onMenuItemClick={closeDropdown}
                  />
                </Menu.Item>
                {exportEligible() && hasPayPalSelected() && (
                  <Item>
                    <ExportProductIncentives
                      state={survey.state}
                      survey={survey}
                      onMenuItemClick={closeDropdown}
                    />
                  </Item>
                )}
                {exportEligible() && hasPayPalSelected() && (
                  <Item
                    onItemHover={() => {}}
                    onClick={closeDropdown}
                    rootPrefixCls='ant-menu'
                    key='5'
                  >
                    <ImportProductIncentives
                      state={survey.state}
                      survey={survey}
                      onMenuItemClick={closeDropdown}
                    />
                  </Item>
                )}
                {exportEligible() && (
                  <Item
                    onItemHover={() => {}}
                    onClick={closeDropdown}
                    rootPrefixCls='ant-menu'
                    key='6'
                  >
                    <ExportSurveySharing
                      state={survey.state}
                      survey={survey}
                      onMenuItemClick={closeDropdown}
                    />
                  </Item>
                )}
                {exportEligible() && (
                  <Item
                    onItemHover={() => {}}
                    onClick={closeDropdown}
                    rootPrefixCls='ant-menu'
                    key='7'
                  >
                    <ImportSurveyShares
                      state={survey.state}
                      survey={survey}
                      onMenuItemClick={closeDropdown}
                    />
                  </Item>
                )}
                {exportEligible() && hasGiftCard() && (
                  <React.Fragment>
                    <Item
                      onItemHover={() => {}}
                      onClick={closeDropdown}
                      rootPrefixCls='ant-menu'
                      key='13'
                    >
                      <ExportGiftCardIncentives
                        state={survey.state}
                        survey={survey}
                        onMenuItemClick={closeDropdown}
                      />
                    </Item>
                    <Item
                      onItemHover={() => {}}
                      onClick={closeDropdown}
                      rootPrefixCls='ant-menu'
                      key='14'
                    >
                      <ImportGiftCardIncentives
                        state={survey.state}
                        survey={survey}
                        onMenuItemClick={closeDropdown}
                      />
                    </Item>

                    <Item
                      onItemHover={() => {}}
                      onClick={closeDropdown}
                      rootPrefixCls='ant-menu'
                      key='15'
                    >
                      <ExportGiftCardShares
                        state={survey.state}
                        survey={survey}
                        onMenuItemClick={closeDropdown}
                      />
                    </Item>
                    <Item
                      onItemHover={() => {}}
                      onClick={closeDropdown}
                      rootPrefixCls='ant-menu'
                      key='16'
                    >
                      <ImportGiftCardShares
                        state={survey.state}
                        survey={survey}
                        onMenuItemClick={closeDropdown}
                      />
                    </Item>
                  </React.Fragment>
                )}
              </Menu.ItemGroup>
            )}
            <Menu.ItemGroup>
              <Menu.Divider key='33' />
              <Item
                onItemHover={() => {}}
                onClick={closeDropdown}
                rootPrefixCls='ant-menu'
                key='8'
              >
                <DeleteSurveyButton
                  state={survey.state}
                  surveyId={survey.id}
                  onMenuItemClick={closeDropdown}
                />
              </Item>
            </Menu.ItemGroup>
          </Menu>
        </React.Fragment>
      )

      return (
        <ActionsContainer>
          {['draft', 'active', 'suspended'].includes(survey.state) && (
            <React.Fragment>
              <StatsButton surveyId={survey.id} />
              <FunnelButton surveyId={survey.id} />
              {survey.state === 'draft' ? (
                <React.Fragment>
                  <CopySurveyUrlButton uniqueName={survey.uniqueName} />
                  <EditDraftButton surveyId={survey.id} />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <PauseSurveyButton survey={survey} />
                  <CopySurveyUrlButton uniqueName={survey.uniqueName} />
                  <EditSurveyButton
                    surveyId={survey.id}
                    surveyState={survey.state}
                  />
                </React.Fragment>
              )}
            </React.Fragment>
          )}
          <Dropdown
            overlay={menu}
            trigger={['click']}
            visible={survey.id === openDropdownId}
            onVisibleChange={change => setOpenDropdownId(change && survey.id)}
          >
            <IconButton tooltip={t('tooltips.moreActions')} type='more' />
          </Dropdown>
        </ActionsContainer>
      )
    }
  }

  return (
    <AntTable
      rowKey={record => record.id}
      columns={showActions ? [...columns, actionsColumn] : columns}
      dataSource={surveys}
      showHeader={false}
      pagination={{
        pageSize: DEFAULT_N_ELEMENTS_PER_PAGE,
        total,
        current: page + 1
      }}
      onChange={paginationConfig => {
        onChangePage(paginationConfig.current - 1)
      }}
      loading={loading}
      onRow={record => ({
        onClick: () => onClickRow(record.id)
      })}
    />
  )
}

export default OperatorSurveysList
