import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'
import Text from '../../Text'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { StyledModal } from '../styles'
import ShareStatsForm from '../../ShareStatsForm'
import { displaySuccessMessage } from '../../../utils/displaySuccessMessage'

const ShareStatsButton = ({ survey, organizations, onMenuItemClick }) => {
  const { t } = useTranslation()
  const shareStatsScreen = useMutation(SHARE_STATS_SCREEN)
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [newShares, setNewShares] = useState([])
  const [isOkDisabled, setIsOkDisabled] = useState()

  const shareSurvey = async () => {
    const {
      data: { shareSurveyStatsScreen }
    } = await shareStatsScreen({
      variables: {
        surveyId: survey.id,
        users: newShares
      }
    })
    survey.sharedStatsUsers = shareSurveyStatsScreen.sharedStatsUsers
    displaySuccessMessage(
      t(`components.dasboard.survey.actions.shareStats.success.message`, {
        name: survey.name,
        num: newShares.length
      })
    )
  }
  return (
    <React.Fragment>
      <Text
        onClick={() => {
          onMenuItemClick()
          setIsModalVisible(true)
        }}
      >
        {t(`tooltips.ShareSurveyStats`)}
      </Text>
      <StyledModal
        visible={isModalVisible}
        title={t(`components.dasboard.survey.actions.shareStats.modal.title`, {
          name: survey.name
        })}
        keyboard={false}
        closable={false}
        onCancel={() => {
          setIsModalVisible(false)
        }}
        onOk={() => {
          shareSurvey()
          setIsModalVisible(false)
        }}
        okButtonProps={{
          disabled: isOkDisabled
        }}
      >
        <ShareStatsForm
          survey={survey}
          organizations={organizations}
          setNewShares={setNewShares}
          setIsOkDisabled={setIsOkDisabled}
          isOkDisabled={isOkDisabled}
        />
      </StyledModal>
    </React.Fragment>
  )
}

export const SHARE_STATS_SCREEN = gql`
  mutation shareSurveyStatsScreen($surveyId: ID, $users: [String]) {
    shareSurveyStatsScreen(surveyId: $surveyId, users: $users) {
      id
      sharedStatsUsers {
        id
        organization {
          id
        }
      }
    }
  }
`

export default withRouter(ShareStatsButton)
