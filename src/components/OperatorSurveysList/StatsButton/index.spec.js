import React from 'react'
import { mount } from 'enzyme'
import StatsButton from '.'
import { Router } from 'react-router-dom'
import IconButton from '../../../components/IconButton'
import wait from '../../../utils/testUtils/waait'

describe('StatsButton', () => {
  let testRender
  let surveyId

  beforeEach(() => {
    surveyId = 1
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render StatsButton', async () => {
    const history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    testRender = mount(
      <Router history={history}>
        <StatsButton surveyId={surveyId} />
      </Router>
    )
    expect(testRender.find(StatsButton)).toHaveLength(1)

    testRender.find(IconButton).simulate('click')

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('/operator/stats/1')
  })
})
