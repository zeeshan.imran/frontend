import React from 'react'
import IconButton from '../../../components/IconButton'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'


const StatsButton = ({ surveyId, history }) => {
  const { t } = useTranslation()
  return (
    <IconButton
      tooltip={t('tooltips.surveyCharts')}
      onClick={() => history.push(`/operator/stats/${surveyId}`)}
      type='bar-chart'
    />
  )}

export default withRouter(StatsButton)
