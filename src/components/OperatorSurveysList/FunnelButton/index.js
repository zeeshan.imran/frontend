import React from 'react'
import IconButton from '../../IconButton'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

const FunnelButton = ({ surveyId, history }) => {
  const { t } = useTranslation()
  return (
    <IconButton
      tooltip={t(`tooltips.funnelActions`)}
      onClick={() => history.push(`/operator/survey/funnel/${surveyId}`)}
      type='funnel-plot'
    />
  )
}

export default withRouter(FunnelButton)
