import React from 'react'
import { Container } from './styles'

const BaseCardSpacer = ({ children }) => <Container>{children}</Container>

export default BaseCardSpacer
