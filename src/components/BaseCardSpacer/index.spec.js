import React from 'react'
import { shallow } from 'enzyme'
import BaseCardSpacer from '.'
import { Container } from './styles'


describe('BaseCardSpacer', () => {
  let testRender
  let children
  

  beforeEach(() => {
    children = <Container>Back Default</Container>
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render BaseCardSpacer', async () => {
    testRender = shallow(
      <BaseCardSpacer
        children={children}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
