import React from 'react'
import { shallow } from 'enzyme'
import ProgressBar from '.'

describe('ProgressBar', () => {
  let testRender
  let percentage
  let status
  let showPercentage
  let otherProps
  beforeEach(() => {
    percentage = 80
    status = 'active'
    showPercentage = true
    otherProps = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ProgressBar', async () => {
    testRender = shallow(
      <ProgressBar
        percentage={percentage}
        status={status}
        showPercentage={showPercentage}
        otherProps={otherProps}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
