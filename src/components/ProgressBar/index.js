import React from 'react'
import { Progress } from './styles'

const ProgressBar = ({
  percentage,
  status,
  showPercentage = true,
  ...otherProps
}) => (
  <Progress
    {...otherProps}
    percent={percentage}
    status={status}
    showInfo={showPercentage}
  />
)

export default ProgressBar
