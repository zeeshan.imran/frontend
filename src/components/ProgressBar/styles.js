import styled from 'styled-components'
import { Progress as AntProgress } from 'antd'

export const Progress = styled(AntProgress)`
  .ant-progress-inner {
    background-color: #d8d8d8;
  }
`
