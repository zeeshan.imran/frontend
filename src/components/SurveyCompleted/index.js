import React from 'react'
import { Row } from 'antd'
import Title from './Title'
import Picture from './Picture'
import ThankYouText from './ThankYouText'
import { withTranslation } from 'react-i18next'
import SurveySharing from '../../containers/SurveySharing'
import useResponsive from '../../utils/useResponsive'
import {
  SocialButtonsContainer,
  FacebookButton,
  TwitterButton,
  LinkedInButton,
  DasboardButton,
  PdfButton
} from './styles'

const SurveyCompleted = ({
  sharingButtons,
  text,
  shareLink,
  t,
  sharingButtonStatus,
  getPdf,
  loading,
  showGeneratePdf
}) => {
  const { mobile } = useResponsive()
  const encText = encodeURIComponent(t('components.surveyComplete.text'))
  const encLink = encodeURIComponent(shareLink)
  const via = 'FlavorWiki'
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Row>
        <Title />
        <Picture />
        <ThankYouText text={text} />
      </Row>
      <SocialButtonsContainer>
        <SurveySharing isDesktop={!mobile} centerAlign />
        {showGeneratePdf && (
          <PdfButton type='primary' onClick={getPdf} loading={loading}>
            {t('containers.surveyStats.generateReport.text')}
          </PdfButton>
        )}

        {sharingButtonStatus && sharingButtons && (
          <React.Fragment>
            <FacebookButton
              href={`http://www.facebook.com/share.php?u=${encLink}`}
              target='_blank'
            >
              {t('components.socialMedia.shareFB')}
            </FacebookButton>
            <TwitterButton
              href={`https://twitter.com/intent/tweet?text=${encText}&via=${via}&url=${encLink}`}
              target='_blank'
            >
              {t('components.socialMedia.shareTW')}
            </TwitterButton>
            <LinkedInButton
              href={`https://www.linkedin.com/shareArticle?mini=true&url=${encLink}`}
              target='_blank'
            >
              {t('components.socialMedia.shareLI')}
            </LinkedInButton>
          </React.Fragment>
        )}
        {REACT_APP_THEME === 'default' && (
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        )}
      </SocialButtonsContainer>
    </React.Fragment>
  )
}

export default withTranslation()(SurveyCompleted)
