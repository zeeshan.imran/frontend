import React from 'react'
import { mount } from 'enzyme'
import SurveyCompleted from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import defaults from '../../defaults'
import { FacebookButton, TwitterButton, LinkedInButton } from './styles'
import ThankYouText from './ThankYouText'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate HoC receive the t function as a prop
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

const SURVEY_ID = 'survey-1'

describe('SurveyCompleted', () => {
  let testRender
  let mockClient

  beforeEach(() => {
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: SURVEY_ID,
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: []
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render DisabledButton Button', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <SurveyCompleted
          sharingButtons
          text={'Thank you for choose this survey'}
          shareLink={'https://www.facebook.com/'}
          sharingButtonStatus
        />
      </ApolloProvider>
    )
    expect(testRender.find(ThankYouText).text()).toBe(
      'Thank you for choose this survey'
    )
  })
  test('should render FacebookButton Button', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <SurveyCompleted
          sharingButtons
          text={'Thank you for choose this survey'}
          shareLink={'https://www.facebook.com/'}
          sharingButtonStatus
        />
      </ApolloProvider>
    )
    expect(testRender.find(FacebookButton)).toHaveLength(1)
  })

  test('should render TwitterButton Button', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <SurveyCompleted
          sharingButtons
          text={'Thank you for choose this survey'}
          shareLink={'https://twitter.com/login?lang=en'}
          sharingButtonStatus
        />
      </ApolloProvider>
    )
    expect(testRender.find(TwitterButton)).toHaveLength(1)
  })

  test('should render LinkedInButton Button', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <SurveyCompleted
          sharingButtons
          text={'Thank you for choose this survey'}
          shareLink={'https://in.linkedin.com/'}
          sharingButtonStatus
        />
      </ApolloProvider>
    )
    expect(testRender.find(LinkedInButton)).toHaveLength(1)
  })
})
