import React from 'react'
import { shallow } from 'enzyme'
import ThankYouText from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import SURVEY_PARTICIPATION_QUERY from '../../../queries/SurveyParticipationQuery'
import { surveyBasicInfo } from '../../../fragments/survey'

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
      screeners {
        id
        uniqueName
        isPaypalSelected
        isGiftCardSelected
        customizeSharingMessage
        referralAmount
      }
    }
  }

  ${surveyBasicInfo}
`

describe('ThankYouText', () => {
  let testRender
  let text = 'Thank you for choose this survey'
  let client = createApolloMockClient({
    mocks: [
      {
        request: {
          query: SURVEY_PARTICIPATION_QUERY,
          variables: {}
        },
        result: {
          surveyId: 'survey-id',
          surveyEnrollmentId: 'survey-id'
        }
      },
      {
        request: {
          query: SURVEY_QUERY,
          variables: {
            id: 'survey-id'
          }
        },
        result: {
          uniqueName: 'survey-unique-name'
        }
      }
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ThankYouText', () => {
    testRender = shallow(
      <ApolloProvider client={client}>
        <ThankYouText text={text} />)
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()
  })
})
