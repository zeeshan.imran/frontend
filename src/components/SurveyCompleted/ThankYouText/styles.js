import styled from 'styled-components'
import { Col } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const CustomCol = styled(Col)`
  display: flex;
  justify-content: center;
`

export const Prompt = styled(Text)`
  -webkit-font-smoothing: antialiased;
  font-family: ${family.primaryLight};
  font-size: 1.8rem;
  color: ${colors.BLACK};
  opacity: 0.7;
  text-align: center;
`
