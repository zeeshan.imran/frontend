import React from 'react'
import Reward from '../../SvgIcons/Reward'
import { CustomCol } from './styles'

const Picture = () => (
  <CustomCol lg={{ span: 4, offset: 10 }}>
    <Reward />
  </CustomCol>
)

export default Picture
