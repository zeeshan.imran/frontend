import React from 'react'
import { shallow } from 'enzyme'
import Picture from '.'

jest.mock('react-i18next', () => ({
    withTranslation: () => Component => {
        Component.defaultProps = { ...Component.defaultProps, t: () => '' }
        return Component
    }
}))

describe('Picture', () => {
    let testRender

    beforeEach(() => { })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Picture', () => {
        testRender = shallow(<Picture />)
        expect(testRender).toMatchSnapshot()
    })
})
