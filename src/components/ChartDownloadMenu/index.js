import React, { useCallback } from 'react'
import { Menu } from 'antd'
import html2canvas from 'html2canvas'
import html2pdf from 'fw-simple-html2pdf'

const ChartDownloadMenu = (chartId, chartName, t) => {
  const downloadImage = useCallback(
    (mimeType = 'image/png') => {
      const elements = document.getElementsByClassName(chartId)[0]
        .lastElementChild
      const format = mimeType.replace('image/', '')
      html2canvas(elements).then(function (canvas) {
        let a = document.createElement('a')
        a.href = canvas.toDataURL(mimeType)
        a.download = `${chartName}.${format}`
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
      })
    },
    [chartId, chartName]
  )

  const downloadPdf = useCallback(() => {
    const element = document.getElementsByClassName(chartId)[0].lastElementChild
    html2pdf(element, {
      filename: `${chartName}.pdf`,
      margin: 40, // default 40, page margin
      save: true, // default true: Save as file
      output: '', // default '', jsPDF output type
      smart: true // default true: Smartly adjust content width
    })
  }, [chartId, chartName])

  return (
    <Menu>
      <Menu.Item key='1' onClick={() => downloadImage('image/png')}>
        {t('components.downloadMenu.downloadPng')}
      </Menu.Item>
      <Menu.Item key='2' onClick={() => downloadImage('image/jpeg')}>
        {t('components.downloadMenu.downloadJpeg')}
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key='4' onClick={downloadPdf}>
        {t('components.downloadMenu.downloadPdf')}
      </Menu.Item>
    </Menu>
  )
}
export default ChartDownloadMenu
