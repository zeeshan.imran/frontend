import React from 'react'
import { mount } from 'enzyme'

import MarketingEmails from './MarketingEmails'
import MarketingPushes from './MarketingPushes'
import PushNotifications from './PushNotifications'
import Privacy from './index'

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: text => text })
}))

describe('Privacy', () => {
  let marketingEmails
  let marketingPushes
  let pushNotifications
  let toggleMarketingEmails
  let toggleMarketingPushes
  let togglePushNotifications
  let testRender

  beforeEach(() => {
    marketingEmails = true
    marketingPushes = true
    pushNotifications = true
    toggleMarketingEmails = jest.fn()
    toggleMarketingPushes = jest.fn()
    togglePushNotifications = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Privacy form', async () => {
    testRender = mount(
      <Privacy
        marketingEmails={marketingEmails}
        marketingPushes={marketingPushes}
        pushNotifications={pushNotifications}
        toggleMarketingEmails={toggleMarketingEmails}
        toggleMarketingPushes={toggleMarketingPushes}
        togglePushNotifications={togglePushNotifications}
      />
    )

    expect(testRender.find(MarketingEmails)).toHaveLength(1)
    expect(testRender.find(MarketingPushes)).toHaveLength(1)
    expect(testRender.find(PushNotifications)).toHaveLength(1)
  })
})
