import React from 'react'
import { shallow } from 'enzyme'
import MarketingEmails from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('MarketingEmails', () => {
  let testRender
  let onChange
  let defaultValue

  beforeEach(() => {
    onChange = jest.fn()
    defaultValue = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render MarketingEmails', async () => {
    testRender = shallow(
      <MarketingEmails onChange={onChange} defaultValue={defaultValue} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
