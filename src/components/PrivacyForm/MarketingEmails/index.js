import React from 'react'
import ToggableSetting from '../../../components/ToggableSetting'
import EmailIcon from '../../../components/SvgIcons/Email'
import { useTranslation } from 'react-i18next';

const MarketingEmails = ({ onChange, defaultValue }) => {
  const { t } = useTranslation();
  return (
    <ToggableSetting
      handleChange={onChange}
      title={t('components.privacyForm.marketingEmails.title')}
      description={t('components.privacyForm.marketingEmails.description')}
      icon={<EmailIcon />}
      defaultChecked={defaultValue}
    />
  )
}

export default MarketingEmails
