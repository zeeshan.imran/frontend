import React from 'react'
import { shallow } from 'enzyme'
import MarketingPushes from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('MarketingPushes', () => {
  let handleChange
  let defaultChecked
  let testRender

  beforeEach(() => {
    handleChange = jest.fn()
    defaultChecked = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render MarketingPushes', async () => {
    testRender = shallow(
      <MarketingPushes
        handleChange={handleChange}
        defaultChecked={defaultChecked}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
