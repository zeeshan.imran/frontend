import React from 'react'
import { shallow } from 'enzyme'
import PushNotifications from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PushNotifications', () => {
  let testRender
  let onChange
  let defaultValue

  beforeEach(() => {
    onChange = jest.fn()
    defaultValue = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PushNotifications', async () => {
    testRender = shallow(
      <PushNotifications onChange={onChange} defaultValue={defaultValue} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
