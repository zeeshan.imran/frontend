import React from 'react'
import ToggableSetting from '../../../components/ToggableSetting'
import NotificationIcon from '../../../components/SvgIcons/Notifications'
import { useTranslation } from 'react-i18next'

const PushNotifications = ({ onChange, defaultValue }) => {
  const { t } = useTranslation()
  return (
    <ToggableSetting
      handleChange={onChange}
      title={t('components.privacyForm.pushNotifications.title')}
      description={t('components.privacyForm.pushNotifications.description')}
      icon={<NotificationIcon />}
      defaultChecked={defaultValue}
    />
  )
}

export default PushNotifications
