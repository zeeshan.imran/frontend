import React from 'react'
import { Row, Col } from 'antd'
import { Container } from './styles'
import SettingsGroup from '../../components/SettingsGroup'
import MarketingEmails from './MarketingEmails'
import MarketingPushes from './MarketingPushes'
import PushNotifications from './PushNotifications'
import { useTranslation } from 'react-i18next'

const Privacy = ({
  marketingEmails,
  marketingPushes,
  pushNotifications,
  toggleMarketingEmails,
  toggleMarketingPushes,
  togglePushNotifications
}) => {
  const { t } = useTranslation()
  return (
    <Container>
      <Row type='flex' justify='center'>
        <Col xs={{ span: 24 }} lg={{ span: 16 }} xl={{ span: 12 }}>
          <SettingsGroup
            title={t('components.privacyForm.marketingNotifications')}
          >
            <MarketingEmails
              onChange={toggleMarketingEmails}
              defaultValue={marketingEmails}
            />
            <MarketingPushes
              onChange={toggleMarketingPushes}
              defaultValue={marketingPushes}
            />
          </SettingsGroup>
          <SettingsGroup
            title={t('components.privacyForm.pushNotifications.title')}
          >
            <PushNotifications
              onChange={togglePushNotifications}
              defaultValue={pushNotifications}
            />
          </SettingsGroup>
        </Col>
      </Row>
    </Container>
  )
}

export default Privacy
