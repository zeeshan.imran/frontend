/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import { Dropdown, Icon, Menu } from 'antd'
import { UserInfo, Picture, Container } from './styles'
import DefaultProfilePicture from '../../assets/svg/User.svg'
import { logout } from '../../utils/userAuthentication'
import { useSlaask } from '../../contexts/SlaaskContext'
import history from '../../history'

// const isProduct = window.location.host === 'app.flavorwiki.com'
// const openProfile = () => history.push('/my-profile')

const MyProfileDropdown = ({
  picture,
  userId,
  displayName,
  hasOperatorAccount
}) => {
  const { reloadSlaask } = useSlaask()
  return (
    <Container>
      <Picture src={picture || DefaultProfilePicture} />
      <Dropdown
        placement='bottomCenter'
        overlay={
          <Menu>
            {/* TODO: disable STAGFW-342 */}

            {hasOperatorAccount && (
              <Menu.Item key='2'>
                <a
                  data-testid='switch-account'
                  onClick={() => history.push('/operator')}
                >
                  Switch to Operator Account
                </a>
              </Menu.Item>
            )}
            <Menu.Item key='1'>
              <a
                data-testid='logout-profile'
                onClick={() => {
                  logout()
                  reloadSlaask()
                }}
              >
                Sign Out
              </a>
            </Menu.Item>
          </Menu>
        }
        trigger={['click']}
      >
        <UserInfo>
          {displayName}
          <Icon type='down' />
        </UserInfo>
      </Dropdown>
    </Container>
  )
}

export default MyProfileDropdown
