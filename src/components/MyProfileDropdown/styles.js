import styled from 'styled-components'
import { family } from '../../utils/Fonts'
import { components } from '../../utils/Metrics'

const PICTURE_SIZE = 2.4

export const UserInfo = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: ${components.NAVBAR_HEIGHT}rem;
  color: rgba(0, 0, 0, 0.65);
  user-select: none;
  cursor: pointer;
  height: 100%;
`

export const Picture = styled.div`
  width: ${PICTURE_SIZE}rem;
  height: ${PICTURE_SIZE}rem;
  border-radius: 50%;
  margin-right: 1.6rem;
  background: url(${props => props.src}) no-repeat center;
  background-size: cover;
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  height: ${components.NAVBAR_HEIGHT}rem;
`
