import React from 'react'
import { storiesOf } from '@storybook/react'
import MyProfileDropdown from './MyProfileDropdown'

storiesOf('MyProfileDropdown', module)
  .add('Profile Dropdown with default user picture', () => (
    <MyProfileDropdown userId={'000'} />
  ))
  .add('Profile Dropdown with an user picture', () => (
    <MyProfileDropdown
      userId={'000'}
      picture={'https://pbs.twimg.com/media/DbbgoMZW0AABU9Z.jpg'}
    />
  ))
