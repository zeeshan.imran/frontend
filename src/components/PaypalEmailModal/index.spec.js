import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import wait from '../../utils/testUtils/waait'
import PaypalEmailModal from './index'
import { FormItem } from './styles'

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: text => text })
}))

describe('PaypalEmailModal', () => {
  let testRender
  let onSubmit
  let errors

  beforeEach(() => { 
    onSubmit = jest.fn()
    errors = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should NOT render PaypalEmailModal when visible is false', async () => {
    testRender = mount(<PaypalEmailModal visible={false} />)
    expect(testRender.find(PaypalEmailModal)).toHaveLength(1)
    expect(testRender.find('form')).toHaveLength(0)
  })

  test('should render PaypalEmailModal', async () => {
    testRender = mount(
      <PaypalEmailModal visible onClick={onSubmit} errors={errors} />
    )
 
    expect(testRender.find(PaypalEmailModal)).toHaveLength(1)
    expect(testRender.find('form')).toHaveLength(1)

    act(() => {
      testRender.find('form').simulate('submit')
    })

    await wait(0)
    testRender.update()

    expect(testRender.find(FormItem).prop('validateStatus')).toBe('error')
    expect(testRender.find(FormItem).prop('help')).toMatchObject([
      'validation.email.required'
    ])

    act(() => {
      testRender
        .find('input')
        .simulate('change', { target: { value: 'paypal@test.com' } })

      testRender.find('form').simulate('submit')
    })

    await wait(0)
    testRender.update()

    expect(testRender.find(FormItem).prop('validateStatus')).toBe('success')
    expect(onSubmit).toHaveBeenCalledWith("paypal@test.com")
  })
})
