import React from 'react'
import { storiesOf } from '@storybook/react'
import CategoryCard from '../CategoryCard'
import HorizontalScroll from './'
import CategoryImage from '../../assets/svg/Baked Goods.svg'

const category = {
  picture: CategoryImage
}

const renderCards = max => {
  let cards = []

  for (let index = 0; index < max; index++) {
    cards.push(
      <CategoryCard
        name={index}
        picture={category.picture}
        onClick={() => console.log('clicked')}
      />
    )
  }
  return cards
}

storiesOf('HorizontalScroll', module).add('Story', () => (
  <HorizontalScroll>{renderCards(30)}</HorizontalScroll>
))
