import React from 'react'
import { shallow } from 'enzyme'
import HorizontalScroll from '.';
import { Container } from './styles'

describe('HorizontalScroll', () => {
    let testRender

    beforeEach(() => {

    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render HorizontalScroll', async () => {

        const children = <Container>
            {'Hello'}
        </Container>

        testRender = shallow(

            <HorizontalScroll
                children={children}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});