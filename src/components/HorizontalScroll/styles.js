import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  overflow-x: auto;
  margin-right: -3rem;
  margin-left: -3rem;
  -webkit-overflow-scrolling: touch;
  -webkit-scrollbar {
    width: 0px;
    background: transparent; /* make scrollbar transparent */
  }
  scroll-snap-type: x mandatory;
`
