import React, { Component } from 'react'
import { Form, Row, Col } from 'antd'
import Input from '../Input'
import {
  Container,
  CurrentPassword,
  PasswordPrompt,
  FormItem,
  ButtonAligner,
  CustomCol,
  StyledButton
} from './styles'
import { ONBOARD_ROOT_PATH } from '../../utils/Constants'
import { compose } from 'ramda'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next';

class ChangePasswordForm extends Component {
  compareToFirstPassword = (rule, value, callback) => {
    const {
      form: { getFieldValue }
    } = this.props
    if (value && value !== getFieldValue('newPassword')) {
      // eslint-disable-next-line
      callback('The two passwords are inconsistent')
    } else {
      callback()
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const {
      form: { validateFields }
    } = this.props
    if (value) {
      validateFields(['confirmPassword'], { force: true })
    }
    callback()
  }

  render () {
    const {
      form: { getFieldDecorator, getFieldError, isFieldTouched },
      desktop,
      submitForm,
      history,
      error,
    } = this.props
    const { t } = useTranslation()

    const shouldEnableSubmitButton = () => {
      const isCurrentPasswordFilled =
        isFieldTouched('currentPassword') && !getFieldError('currentPassword')

      const isNewPasswordFilled =
        isFieldTouched('newPassword') && !getFieldError('newPassword')

      const isConfirmPasswordFilled =
        isFieldTouched('confirmPassword') && !getFieldError('confirmPassword')

      return (
        isCurrentPasswordFilled &&
        isNewPasswordFilled &&
        isConfirmPasswordFilled
      )
    }

    return (
      <Container>
        <Row type='flex' justify='center'>
          <Col xs={{ span: 24 }} lg={{ span: 16 }} xl={{ span: 10 }}>
            <Form onSubmit={e => submitForm(e, this.props.form)}>
              <Row type='flex' justify='center'>
                <Col xs={{ span: 24 }}>
                  <CurrentPassword>
                    {getFieldDecorator('currentPassword', {
                      rules: [
                        {
                          required: true,
                          message: t('validation.currentPassword.required')
                        }
                      ]
                    })(
                      <Input
                        type={'password'}
                        placeholder={t('placeholders.currentPassword')}
                        label={t('components.changePassword.currentPassword')}
                      />
                    )}
                  </CurrentPassword>
                  <PasswordPrompt error={!!error}>
                    {error ||
                      t('components.changePassword.passwordPrompt')}
                  </PasswordPrompt>

                  <FormItem>
                    {getFieldDecorator('newPassword', {
                      rules: [
                        {
                          required: true,
                          message: t('validation.newPassword.required')
                        },
                        {
                          validator: this.validateToNextPassword
                        }
                      ]
                    })(
                      <Input
                        type={'password'}
                        placeholder={t('placeholders.newPassword')}
                        label={t('components.changePassword.newPassword')}
                      />
                    )}
                  </FormItem>

                  <FormItem>
                    {getFieldDecorator('confirmPassword', {
                      rules: [
                        {
                          required: true,
                          message: t('validation.confirmPassword.required') 
                        },
                        {
                          validator: this.compareToFirstPassword
                        }
                      ]
                    })(
                      <Input
                        type={'password'}
                        placeholder={t('placeholders.confirmPassword')}
                        label={t('components.changePassword.confirmPassword')}
                      />
                    )}
                  </FormItem>

                  <ButtonAligner>
                    <Row gutter={24} type='flex' justify='center'>
                      <CustomCol
                        desktop={desktop}
                        xs={{ span: 24 }}
                        xl={{ span: 12 }}
                      >
                        <StyledButton
                          htmlType='submit'
                          type={
                            shouldEnableSubmitButton() ? 'primary' : 'disabled'
                          }
                        >
                          {t('components.changePassword.savePasswordButton')}
                        </StyledButton>
                      </CustomCol>
                      <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                        <StyledButton
                          type='secondary'
                          onClick={
                            () =>
                              history.push(
                                `${ONBOARD_ROOT_PATH}/forgot-password`
                              )
                            // FIXME Ask Francisco about whether we should redirect the user to / when he is logged and trying to access forgot-password
                            // Or if we should just move forgot-password one level up above and make it accessible whether your logged in or not
                          }
                        >
                          {t('components.changePassword.forgotPasswordButton')}
                        </StyledButton>
                      </Col>
                    </Row>
                  </ButtonAligner>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default compose(
  withRouter,
  Form.create()
)(ChangePasswordForm)
