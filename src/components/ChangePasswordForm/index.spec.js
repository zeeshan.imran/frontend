import React from 'react'
import { mount } from 'enzyme'
import ChangePasswordForm from '.'
import { BrowserRouter as Router } from 'react-router-dom'
import { CurrentPassword, ButtonAligner, StyledButton } from './styles'
import { Form } from 'antd'

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: text => text })
}))

describe('ChangePasswordForm', () => {
  let testRender
  let form
  let desktop
  let history
  let submitForm
  let error

  beforeEach(() => {
    form = { 
      getFieldDecorator: jest.fn(() => a => a),
      isFieldTouched: jest.fn(() => b => b),
      getFieldError: jest.fn(() => c => c)
    }
    desktop = 'center'
    history = null
    submitForm = jest.fn()
    error = 'You must provide your current password in order to change it.'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ChangePasswordForm', () => {
    testRender = mount( 
      <Router>
        <ChangePasswordForm
          form={form}
          submitForm={submitForm}
          history={history}
          error={error}
        />
      </Router>
    )
    expect(testRender.find(CurrentPassword)).toHaveLength(1)

    testRender.find(Form).simulate('submit')

    expect(submitForm).toHaveBeenCalled()

    testRender = mount(
      <Router>
        <ChangePasswordForm desktop={desktop} history={history} />
      </Router>
    )
    expect(testRender.find(ButtonAligner)).toHaveLength(1)
    testRender
      .find(StyledButton)
      .last()
      .props('onClick')
      .onClick()
  })
})
 