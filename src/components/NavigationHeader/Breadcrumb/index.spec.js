import React from 'react'
import { shallow } from 'enzyme'
import Breadcrumb from '.'

describe('Breadcrumb', () => {
    let testRender
    let routes

    beforeEach(() => {
        routes = []
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Breadcrumb', async () => {
        testRender = shallow(<Breadcrumb routes={routes} />)
        expect(testRender).toMatchSnapshot()
    })
})
