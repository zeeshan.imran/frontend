import React from 'react'
import { Breadcrumb as AntBreadcrumb } from 'antd'

const Breadcrumb = ({ routes }) => <AntBreadcrumb routes={routes} />

export default Breadcrumb
