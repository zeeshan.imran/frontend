import React from 'react'
import { storiesOf } from '@storybook/react'
import NavigationHeader from './'
import { getRoutesArray } from '../../utils/getRoutesArray'

const exampleRoute = new URL(
  'http://flavorwiki.com/dashboard/products-and-stores/create-product'
)

const routes = getRoutesArray(exampleRoute)

storiesOf('NavigationHeader', module).add('Story', () => (
  <NavigationHeader title='Create Product' routes={routes} />
))
