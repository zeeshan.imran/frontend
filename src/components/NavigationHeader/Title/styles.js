import styled from 'styled-components'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'

export const TitleText = styled(Text)`
  font-family: ${family.primaryBold};
  font-size: 2rem;
  color: rgba(0, 0, 0, 0.85);
  line-height: 2.8rem;
  margin: 1.5rem 0;
  display: flex;
`
