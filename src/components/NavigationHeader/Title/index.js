import React from 'react'
import { TitleText } from './styles'

const Title = ({ title }) => <TitleText>{title}</TitleText>

export default Title
