import React from 'react'
import { shallow } from 'enzyme'
import Title from '.'

describe('Title', () => {
    let testRender
    let title

    beforeEach(() => {
        title = 'Navigation Title'
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Title', async () => {
        testRender = shallow(<Title title={title} />)
        expect(testRender).toMatchSnapshot()
    })
})