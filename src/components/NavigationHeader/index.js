import React, { Fragment } from 'react'
import Breadcrumb from './Breadcrumb'
import Title from './Title'

const NavigationHeader = ({ routes, title }) => (
  <Fragment>
    <Breadcrumb routes={routes} />
    <Title title={title} />
  </Fragment>
)

export default NavigationHeader
