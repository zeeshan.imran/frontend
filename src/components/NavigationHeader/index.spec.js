import React from 'react'
import { shallow } from 'enzyme'
import NavigationHeader from '.'

describe('NavigationHeader', () => {
    let testRender
    let routes
    let title


    beforeEach(() => {
        routes = []
        title = 'Navigation Title'
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render NavigationHeader', async () => {
        testRender = shallow(<NavigationHeader routes={routes} title={title} />)
        expect(testRender).toMatchSnapshot()
    })
})
