import React from 'react'
import { storiesOf } from '@storybook/react'
import Error from './'

storiesOf('Error', module).add('Error Label', () => (
  <Error>{'Example error message'}</Error>
))
