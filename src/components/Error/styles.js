import styled from 'styled-components'
import { Icon as AntIcon } from 'antd'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  align-items: center;
  text-align: left;
`

export const Icon = styled(AntIcon)`
  margin-right: 0.5rem;
  font-size: 1.6rem;
  color: ${({ invisible }) => (invisible ? 'transparent' : colors.ERROR)};
`

export const Text = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 1.2rem;
  line-height: 1.6rem;
  letter-spacing: 0.04rem;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  color: ${({ invisible }) => (invisible ? 'transparent' : colors.ERROR)};
`
