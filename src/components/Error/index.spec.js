import React from 'react'
import { shallow } from 'enzyme'
import Error from '.'
import { Container } from './styles'

describe('Error', () => {
  let testRender
  let children
  let invisible
  let className

  beforeEach(() => {
    children = <Container>Hello</Container>
    invisible = false
    className = 'large'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Error', async () => {
    testRender = shallow(
      <Error children={children} invisible={invisible} className={className} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
