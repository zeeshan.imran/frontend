import React from 'react'
import { Container, Icon, Text } from './styles'

/**
 *
 * @param {boolean} invisible set as true if you want the error occupy a given
 * space but keep it hidden from the user
 */

const Error = ({ invisible, className, children }) => (
  <Container className={className}>
    <Icon invisible={invisible} type='exclamation-circle' />
    <Text invisible={invisible}>{children}</Text>
  </Container> 
)

export default Error
