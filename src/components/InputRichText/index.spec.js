import React from 'react'
import { shallow } from 'enzyme'
import { Editor } from '@tinymce/tinymce-react'
import sinon from 'sinon'
import InputRichText from '.'
import FieldLabel from '../FieldLabel'
import wait from '../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('InputRichText', () => {
  let testRender
  let label
  let required
  let value
  let size
  let onChange
  let spy

  beforeEach(() => {
    label = 'Input here'
    required = false
    value = 'text'
    size = 'sm'
    onChange = jest.fn()
    spy = sinon.spy(global.document, 'createElement')
    // mock tinymce activeEditor
    global.tinymce.activeEditor = {
      windowManager: {
        alert: jest.fn()
      },
      editorUpload: {
        blobCache: {
          create: (id, _file, base64) => {
            return {
              blobUri: () => `blob://${id}&content=${base64}`
            }
          },
          add: () => {}
        }
      }
    }
  })

  afterEach(() => {
    testRender.unmount()
    sinon.restore()
  })

  test('should render InputRichText', () => {
    testRender = shallow(
      <InputRichText
        label={label}
        required={required}
        value={value}
        size={size}
        onChange={onChange}
      />
    )

    expect(testRender).toMatchSnapshot()

    expect(testRender.find(FieldLabel).props()).toMatchObject({
      label: 'Input here'
    })
  })

  test('should render InputRichText on change', () => {
    testRender = shallow(
      <InputRichText
        label={label}
        required={required}
        value={value}
        size={size}
        onChange={onChange}
      />
    )

    testRender
      .find(Editor)
      .props()
      .onEditorChange()

    expect(onChange).toHaveBeenCalled()
  })

  test('test upload the picture', async () => {
    const cb = jest.fn()

    testRender = shallow(
      <InputRichText
        label={label}
        required={required}
        value={value}
        size={size}
        onChange={onChange}
      />
    )

    await wait(0)

    testRender
      .find(Editor)
      .props()
      .init.file_picker_callback(cb)

    await wait(0)

    // send a file to input
    const file = new File(['dummy content'], 'example.png', {
      type: 'image/png'
    })
    const imageInput = spy.returnValues[spy.returnValues.length - 1]
    imageInput.onchange.bind({ files: [file] })()

    await wait(0)
    expect(cb).toHaveBeenCalledWith(
      expect.stringMatching(
        /blob:\/\/blobid[0-9]+&content=ZHVtbXkgY29udGVudA==/
      ),
      { title: 'example.png' }
    )
  })

  test('test upload the picture with over sizing', async () => {
    const cb = jest.fn()

    testRender = shallow(
      <InputRichText
        label={label}
        required={required}
        value={value}
        size={size}
        onChange={onChange}
      />
    )

    await wait(0)

    testRender
      .find(Editor)
      .props()
      .init.file_picker_callback(cb)

    await wait(0)

    // send a file to input
    const file = new File(['1'.repeat(2 * 1024 * 1024 + 1)], 'example.png', {
      type: 'image/png'
    })
    const imageInput = spy.returnValues[spy.returnValues.length - 1]
    imageInput.onchange.bind({ files: [file] })()

    await wait(0)
    expect(
      global.tinymce.activeEditor.windowManager.alert
    ).toHaveBeenCalledWith('errors.pictureTooLarge')
  })
})
