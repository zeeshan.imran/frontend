import React from 'react'
import { CopyToClipboard as Copy } from 'react-copy-to-clipboard'
import IconButton from '../IconButton'

const CopyToClipboard = ({ target, tooltip }) => (
  <Copy text={target}>
    <IconButton type='link' tooltip={tooltip} />
  </Copy>
)

export default CopyToClipboard
