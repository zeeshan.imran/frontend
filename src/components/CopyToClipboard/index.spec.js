import React from 'react'
import { shallow } from 'enzyme'
import CopyToClipboard from '.'

describe('CopyToClipboard', () => {
  let testRender
  let target
  let tooltip

  beforeEach(() => {
    target = 'Copy'
    tooltip = 'Click here for copy'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CopyToClipboard', async () => {
    testRender = shallow(<CopyToClipboard target={target} tooltip={tooltip} />)
    expect(testRender).toMatchSnapshot()
  })
})
 