import styled from 'styled-components'
import { Col } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const CustomCol = styled(Col)`
  display: flex;
  flex-direction: column;
  justify-content: center;
`

export const Title = styled(Text)`
  width: 100%;
  font-family: ${family.primaryLight};
  font-size: 3.2rem;
  color: ${colors.LIGHT_OLIVE_GREEN};
  text-align: center;
  margin-bottom: 0.9rem;
`

export const SurveyTypeLabel = styled(Text)`
  width: 100%;
  font-family: ${family.primaryRegular};
  font-size: 1.6rem;
  color: rgba(20, 20, 20, 0.5);
  text-align: center;
  margin-bottom: 0.5rem;
`

export const InstructionsTitle = styled(Text)`
  width: 100%;
  font-family: ${family.primaryLight};
  font-size: 4.2rem;
  color: ${colors.BLACK};
  line-height: 1.24;
  letter-spacing: -0.6px;
  text-align: center;
  margin-bottom: 2.5rem;
`
