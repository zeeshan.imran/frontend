import React from 'react'
import { shallow } from 'enzyme'
import Header from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Header', () => {
  let testRender
  let title
  let isMultiProduct

  beforeEach(() => {
    title = 'Survey'
    isMultiProduct = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Header', async () => {
    testRender = shallow(
      <Header title={title} isMultiProduct={isMultiProduct} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
