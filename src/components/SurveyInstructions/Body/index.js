import React from 'react'
import { Row } from 'antd'
import { withTranslation } from 'react-i18next'

import {
  CustomCol,
  InstructionContainer,
  InstructionIndex,
  InstructionText,
  ExtraInstructions
} from './styles'

const Body = ({ extraInstructions, instructionSteps = [], t }) => (
  <React.Fragment>
    {instructionSteps && instructionSteps.length > 0 && (
      <Row>
        <CustomCol xs={{ span: 24 }} lg={{ span: 12, offset: 6 }}>
          {instructionSteps.map((instruction, index) => (
            <InstructionContainer key={index}>
              <InstructionIndex>{index + 1}</InstructionIndex>
              <InstructionText>{instruction}</InstructionText>
            </InstructionContainer>
          ))}
        </CustomCol>
      </Row>
    )}
    {extraInstructions && (
      <Row type='flex' justify='center'>
        <CustomCol xs={{ span: 24 }} lg={{ span: 12 }}>
          <ExtraInstructions
            dangerouslySetInnerHTML={{ __html: extraInstructions }}
          />
        </CustomCol>
      </Row>
    )}
  </React.Fragment>
)

export default withTranslation()(Body)
