import React from 'react'
import { shallow } from 'enzyme'
import Body from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Body', () => {
  let testRender
  let extraInstructions
  let instructionSteps

  beforeEach(() => {
    extraInstructions = 'When you are ready, click ‘Start Tasting’ below.'
    instructionSteps = [
      'Please be sure you have all of your selected products before starting the tasting.',
      'Find a quiet place.',
      'You will be given additional instructions during the tasting.'
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Body', async () => {
    testRender = shallow(
      <Body
        extraInstructions={extraInstructions}
        instructionSteps={instructionSteps}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
