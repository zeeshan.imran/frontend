import styled from 'styled-components'
import { Col } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const CustomCol = styled(Col)`
  display: flex;
  justify-content: center;
  flex-direction: column;
`

export const InstructionContainer = styled(Text)`
  display: flex;
  align-items: start;
  margin-bottom: 2.7rem;
`

export const InstructionIndex = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.8rem;
  color: ${colors.LIGHT_OLIVE_GREEN};
  margin-right: 2.5rem;
  line-height: 2.7rem;
`

export const InstructionText = styled(Text)`
  font-family: ${family.primaryLight};
  font-size: 1.8rem;
  color: ${colors.BLACK};
  opacity: 0.7;
`
 
export const ExtraInstructions = styled(Text)`
  -webkit-font-smoothing: antialiased;
  font-family: ${family.NotoSansKRLight};
  font-size: 1.8rem;
  color: ${colors.BLACK};
  opacity: 0.7;
  text-align: center;
  margin-top: 2.3rem;
`
