import React from 'react'
import { mount } from 'enzyme'
import SurveyInstructions from '.'
import Footer from './Footer'
import { BrowserRouter as Router } from 'react-router-dom'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('SurveyInstructions', () => {
  let testRender
  let title
  let isMultiProduct
  let description
  let instructionSteps
  let onClick
  let buttonLabel

  beforeEach(() => {
    title = 'Survey'
    isMultiProduct = false
    description = 'PLease fill all survey questions'
    instructionSteps = [
      'Please be sure you have all of your selected products before starting the tasting.',
      'Find a quiet place.',
      'You will be given additional instructions during the tasting.',
      'When you are ready, click ‘Start Tasting’ below.'
    ]

    onClick = jest.fn()
    buttonLabel = 'next'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyInstructions', async () => {
    testRender = mount(
      <Router>
        <SurveyInstructions
          title={title}
          isMultiProduct={isMultiProduct}
          description={description}
          instructionSteps={instructionSteps}
          onClick={onClick}
          buttonLabel={buttonLabel}
        />
      </Router>
    )
    expect(testRender.find(SurveyInstructions)).toHaveLength(1)
  })

  test('should render SurveyInstructions on click', async () => {
    testRender = mount(
      <Router>
        <SurveyInstructions
          title={title}
          isMultiProduct={isMultiProduct}
          description={description}
          instructionSteps={instructionSteps}
          onClick={onClick}
          buttonLabel={buttonLabel}
        />
      </Router>
    )
    testRender
      .find(Footer)
      .props()
      .onClick()

    expect(onClick).toHaveBeenCalled()
  })
})
