import React from 'react'
import { Row } from 'antd'
import { withTranslation } from 'react-i18next'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'

import { CustomCol, HappyTastingText, StartSurveyButton } from './styles'

const Footer = ({ onClick, t, buttonLabel }) => (
  <Row type='flex' justify='center'>
    <CustomCol xs={{ span: 24 }} lg={{ span: 8 }}>
      <HappyTastingText>{t('happyTasting')}</HappyTastingText>
      <StartSurveyButton onClick={onClick}>{buttonLabel}</StartSurveyButton>
      <TermsAndPrivacyFooter />
    </CustomCol>
  </Row>
)

export default withTranslation()(Footer)
