import React from 'react'
import { mount } from 'enzyme'
import Footer from '.'
import { BrowserRouter as Router } from 'react-router-dom'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Footer', () => {
  let testRender
  let onClick
  let buttonLabel

  beforeEach(() => {
    onClick = jest.fn()
    buttonLabel = 'next'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Footer', async () => {
    testRender = mount(
      <Router>
        <Footer
          onClick={onClick}
          buttonLabel={buttonLabel}
        />
      </Router>
    )
    expect(testRender.find(Footer)).toHaveLength(1)
  })

  test('should render Footer on click', async () => {
    testRender = mount(
      <Router>
        <Footer
          onClick={onClick}
          buttonLabel={buttonLabel}
        />
      </Router>
    )
    testRender
      .find(Footer)
      .props()
      .onClick()

    expect(onClick).toHaveBeenCalled()
  })
})
