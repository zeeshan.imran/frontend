import React from 'react'
import { mount,shallow } from 'enzyme'
import UploadSurveyCover from '.'
import UploadButton from './UploadButton'
import { act } from 'react-dom/test-utils'
import { Upload } from 'antd'

describe('UploadSurveyCover', () => {
  let testRender
  let loading
  let error
  let url
  let onFileUpload
  
  beforeEach(() => {
    loading = 'yes'
    error = {}
    url = 'http://localhost:3000'
    onFileUpload = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UploadSurveyCover', () => {
    testRender = shallow(
      <UploadSurveyCover
        loading={loading}
        error={error}
        url={url}
        onFileUpload={onFileUpload}
      />
    )

    expect(testRender).toMatchSnapshot()
  })

  test('should render Upload before', () => {
    testRender = mount(
      <UploadSurveyCover handleBeforeUpload={onFileUpload} />
    )

    testRender.find(UploadButton).simulate('change')
    expect(onFileUpload).not.toHaveBeenCalled()
  })

  test('should remove picture', () => {
    testRender = mount(
      <UploadSurveyCover
        loading={loading}
        error={null}
        url={url}
        onFileUpload={onFileUpload}
      />
    )

    act(() => {
      testRender.find(Upload).prop('onRemove')()
    })
    expect(onFileUpload).toHaveBeenCalled()
  })

  test('should handle before upload', () => {
    testRender = mount(
      <UploadSurveyCover
        loading={loading}
        error={null}
        url={url}
        onFileUpload={onFileUpload}
      />
    )

    act(() => {
      testRender.find(UploadButton).prop('handleBeforeUpload')()
    })
    expect(onFileUpload).toHaveBeenCalled()
  })
})
