import React from 'react'
import ActionButtons from './ActionButtons'
import DropdownButton from './DropdownButton'
import { ActionBar } from './styles'

const AdminActionBar = ({ buttons, dropdownActions }) => (
  <ActionBar>
    <ActionButtons buttons={buttons} />
    <DropdownButton actions={dropdownActions} />
  </ActionBar>
)

export default AdminActionBar
