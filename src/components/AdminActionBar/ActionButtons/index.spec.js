import React from 'react'
import { shallow } from 'enzyme'
import ActionButtons from '.'

describe('ActionButtons', () => {
  let testRender
  let buttons

  beforeEach(() => {
    buttons = [
      { primary: true, label: 'Back' },
      { primary: false, label: 'Next' }
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ActionButtons', async () => {
    testRender = shallow(<ActionButtons buttons={buttons} />)
    expect(testRender).toMatchSnapshot()
  })
})
 