import styled from 'styled-components'
import Button from '../../Button'

export const ActionBarButton = styled(Button)`
  margin-right: 1rem;
`
