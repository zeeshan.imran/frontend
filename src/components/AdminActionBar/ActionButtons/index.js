import React, { Fragment } from 'react'
import { ActionBarButton } from './styles'

const ActionButtons = ({ buttons }) => (
  <Fragment>
    {buttons.map((button, index) => (
      <ActionBarButton
        key={index}
        size='default'
        type={button.primary ? 'primary' : 'secondary'}
        onClick={button.onClick}
      >
        {button.label}
      </ActionBarButton>
    ))}
  </Fragment>
)

export default ActionButtons
