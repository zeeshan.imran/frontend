import React from 'react'
import { storiesOf } from '@storybook/react'
import AdminActionBar from './'

// {label: 'ab', onClick: () => {}, primary: true}

const buttons = [
  {
    label: 'Primary Button',
    onClick: () => {
      console.log('Clicked Primary Button')
    },
    primary: true
  },
  {
    label: 'Secondary Button',
    onClick: () => {
      console.log('Clicked 1st Secondary Button')
    }
  },
  {
    label: 'Secondary Button',
    onClick: () => {
      console.log('Clicked 2nd Secondary Button')
    }
  },
  {
    label: 'Secondary Button',
    onClick: () => {
      console.log('Clicked 3rd Secondary Button')
    }
  }
]

const dropdownActions = [
  {
    label: '1st Action',
    onClick: () => {
      console.log('Clicked 1st Action')
    }
  },
  {
    label: '2nd Action',
    onClick: () => {
      console.log('Clicked 2nd Action')
    }
  },
  {
    label: '3rd Action',
    onClick: () => {
      console.log('Clicked 3rd Action')
    }
  }
]

storiesOf('AdminActionBar', module)
  .add('Action Bar with Buttons', () => (
    <AdminActionBar buttons={buttons} dropdownActions={dropdownActions} />
  ))
  .add('Action Bar without dropdown button', () => (
    <AdminActionBar buttons={buttons} />
  ))
