import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'
import { Container as RadioButtonContainer } from '../../RadioButton/styles'
import { Container as InputContainer } from '../../FieldLabel/styles'
import { Col as AntCol} from 'antd'

export const Col = styled(AntCol)`
  max-width: ${({ mobile }) => ( mobile ? `50%` : `100%`)};
  padding-bottom: 3rem;
`

export const MainContainer = styled.div`
  margin-top: 2.5rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`

export const Container = styled.div`
  text-align: center;
  user-select: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 100%;
`

export const Image = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
`

export const Picture = styled(Image)`
  width: 100%;
  height: 100%;
`

export const NoImage = styled.span`
  display: block;
  margin-top: 50%;
`

export const Title = styled.span`
  font-family: ${family.LatoRegular};
  font-size: 1.5rem;
  color: ${colors.SLATE_GREY};
`

export const ButtonContainer = styled.div`
  margin-bottom: ${({ hasMargin }) => (hasMargin ? '10px' : 0)};
  flex-direction: row;
  display: flex;
  min-height: 4rem;
  ${RadioButtonContainer} {
    display: flex;
    width: 100%;
    align-items: center !important;
    justify-content: center;
    margin-bottom: 5px;
    margin-top: 5px;
    min-height: 4rem;
    align-items: normal;
  }
  ${InputContainer} {
    margin-left: 20px;
  }
`
export const OtherContainer = styled.div`
  width: 100%;
`