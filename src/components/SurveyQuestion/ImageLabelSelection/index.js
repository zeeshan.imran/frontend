import React, { useState } from 'react'
import { withTranslation } from 'react-i18next'
import { Row } from 'antd'
import useResponsive from '../../../utils/useResponsive'
import RadioButton from '../../RadioButton'
import Input from '../../Input'
import BaseCard from '../../BaseCard'
import StaticUrlOrBase64 from '../../../utils/imagesStaticUrl'

import {
  MainContainer,
  Container,
  Picture,
  Title,
  NoImage,
  ButtonContainer,
  OtherContainer,
  Col
} from './styles'

const ImageLabelSelection = ({
  options,
  optionDisplayType,
  formattedValue,
  isOtherShown,
  onChange,
  onSetIsOtherShown,
  otherValue,
  autoAdvance,
  onSubmitReady,
  onSetOtherValue,
  t
}) => {
  const { REACT_APP_THEME } = process.env

  const { mobile } = useResponsive()
  const [hasOthers, setHasOthers] = useState({})
  const [othersIndex, setOthersIndex] = useState(0)
  return (
    <React.Fragment>
      <Row gutter={16}>
        <MainContainer>
          {options.map((option, index) => {
            const image = StaticUrlOrBase64(option.image800)
            if (option && !option.isOpenAnswer) {
              return (
                <Col
                  span={REACT_APP_THEME === 'chrHansen' ? 12 : ''} 
                  key={`selectOne-${index}`}
                  mobile={mobile}
                >
                  <Container mobile={mobile}>
                    <BaseCard
                      withSelection
                      onClick={() => {
                        onChange({
                          v: option.value,
                          isValid: true
                        })
                        onSetIsOtherShown(false)
                        autoAdvance && onSubmitReady()
                      }}
                      selected={
                        !isOtherShown && formattedValue === option.value
                      }
                      checkType='check'
                    >
                      {image ? (
                        <Picture src={image} />
                      ) : (
                        <NoImage>
                          {optionDisplayType === 'imageOnly' && !image
                            ? option.label
                            : t('components.question.chooseProduct.noImage')}
                        </NoImage>
                      )}
                    </BaseCard>
                    {optionDisplayType === 'imageAndLabel' && (
                      <Title>{option.label}</Title>
                    )}
                  </Container>
                </Col>
              )
            } else {
              if (option && !Object.keys(hasOthers).length) {
                setHasOthers(option)
                setOthersIndex(index)
              }
              return null
            }
          })}
        </MainContainer>
      </Row>
      <Row>
        {Object.keys(hasOthers).length ? (
          <React.Fragment>
            <ButtonContainer
              key={`selectOne-${othersIndex}`}
              hasMargin={othersIndex !== options.length - 1}
            >
              <RadioButton
                checked={isOtherShown}
                onClick={() => {
                  onChange({
                    v: {
                      value: hasOthers.value,
                      desc: otherValue
                    },
                    canSkipAnswer: false,
                    isValid: otherValue.length > 0
                  })
                  onSetIsOtherShown(true)
                }}
                suffix={
                  isOtherShown && (
                    <OtherContainer>
                      <Input
                        autoFocus
                        value={otherValue}
                        onChange={e => {
                          // trim only from start
                          let value = e.target.value.replace(/^\s+/g, '')
                          onSetOtherValue(value)
                          onChange({
                            v: {
                              value: hasOthers.value,
                              desc: value || null
                            },
                            canSkipAnswer: !!value,
                            isValid: !!value
                          })
                        }}
                        onKeyUp={e => {
                          if (e.key === 'Enter') {
                            autoAdvance && onSubmitReady()
                          }
                        }}
                        placeholder={t('placeholders.typeHere')}
                      />
                    </OtherContainer>
                  )
                }
              >
                {t('containers.questionCreationCardBody.choose-one.other')}
              </RadioButton>
            </ButtonContainer>
          </React.Fragment>
        ) : null}
      </Row>
    </React.Fragment>
  )
}

export default withTranslation()(ImageLabelSelection)
