import React from 'react'
import { Desktop } from '../../Responsive'
import UploadPictureContainer from '../../../containers/UploadPicture'
import { Container, Prompt } from './styles'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'
import { useTranslation } from 'react-i18next'

const UploadPicture = ({ onChange }) => {
  const { t } = useTranslation()
  const handleChange = value => {
    onChange({ ...mapSingleValueToAnswer(value), isValid: true })
  }
  return (
    <Desktop>
      {desktop => (
        <Container desktop={desktop}>
          <UploadPictureContainer onChange={handleChange} />
          {/* <Prompt>{t('components.surveyInfoTest.uploadPicture')}</Prompt> */}
          <Prompt>{t('containers.uploadProductPicture.limit')}</Prompt>
        </Container>
      )}
    </Desktop>
  )
}

export default UploadPicture
