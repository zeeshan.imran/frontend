import styled from 'styled-components'

import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const Prompt = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.2rem;
  color: ${colors.SLATE_GREY};
  letter-spacing: 0.03rem;
  width: 29.3rem;
  text-align: center;
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: ${({ desktop }) => (desktop ? 'flex-start' : 'center')};
  max-width: 32.5rem;
  width: 90vw;
`
