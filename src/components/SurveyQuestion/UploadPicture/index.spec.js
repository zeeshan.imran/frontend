import React from 'react'
import { mount } from 'enzyme'
import UploadPicture from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import gql from 'graphql-tag'
import SURVEY_PARTICIPATION_QUERY from '../../../queries/SurveyParticipationQuery'
import defaults from '../../../defaults'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { surveyBasicInfo } from '../../../fragments/survey'
import UploadPictureContainer from '../../../containers/UploadPicture'
import { act } from 'react-dom/test-utils'

jest.mock('../../../utils/internationalization/i18n', () => ({
  t: text => text
}))

jest.mock('browser-image-compression', () => ({
  __esModule: true,
  default: async (file, option) => file
}))

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: text => text })
}))

const QUESTION_ID = 'question-1'
const SURVEY_ID = 'survey-1'

const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      type
    }
  }
`
const SURVEY_QUERY = gql`
  query survey($id: ID) { 
    survey(id: $id) {
      ...surveyBasicInfo
    }
  }

  ${surveyBasicInfo}
`

const mockQuestionQuery = isRequired => ({
  request: {
    query: QUESTION_QUERY,
    variables: { id: QUESTION_ID }
  },
  result: {
    data: {
      question: {
        id: QUESTION_ID,
        required: isRequired,
        products: []
      }
    }
  }
})

describe('UploadPicture', () => {
  let testRender
  let onChange
  let prompt
  let mockClient

  beforeEach(() => {
    onChange = jest.fn()
    prompt = ''

    mockClient = createApolloMockClient({
      mocks: [mockQuestionQuery(false)]
    })
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: SURVEY_ID,
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: []
        }
      }
    })
    mockClient.cache.writeQuery({
      query: SURVEY_QUERY,
      variables: { id: SURVEY_ID },
      data: {
        survey: {
          id: SURVEY_ID,
          products: [],
          minimumProducts: 1,
          customButtons: {},
          state: 'draft'
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UploadPicture', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <UploadPicture onChange={onChange} prompt={prompt} />
        </Router>
      </ApolloProvider>
    )

    expect(testRender.find(UploadPicture)).toHaveLength(1)

    act(() => {
      testRender
        .find(UploadPictureContainer)
        .first()
        .prop('onChange')({ isValid: true, values: ['[object Object]'] })
    })

    expect(onChange).toHaveBeenCalledWith({
      isValid: true,
      values: ['[object Object]']
    })
  })
})
