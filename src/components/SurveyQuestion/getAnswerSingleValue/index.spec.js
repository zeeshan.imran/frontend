import React from 'react'
import { shallow } from 'enzyme'
import surveyStandardValue from '.'

describe('surveyStandardValue', () => {
  let testRender
  
  beforeAll(() => {
    
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render surveyStandardValue', async () => {
    testRender = shallow(
      <surveyStandardValue
      />
    )
     expect(testRender).toMatchSnapshot()
  })
})
