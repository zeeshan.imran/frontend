import React from 'react'
import { mount } from 'enzyme'
import ChooseProduct from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import gql from 'graphql-tag'
import SURVEY_PARTICIPATION_QUERY from '../../../queries/SurveyParticipationQuery'
import defaults from '../../../defaults'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { surveyBasicInfo } from '../../../fragments/survey'
import { act } from 'react-dom/test-utils'
import ChooseProductContainer from '../../../containers/ChooseProduct'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

const QUESTION_ID = 'question-1'
const SURVEY_ID = 'survey-1'

const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      type
    }
  }
`
const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
    }
  }

  ${surveyBasicInfo}
`

const mockQuestionQuery = isRequired => ({
  request: {
    query: QUESTION_QUERY,
    variables: { id: QUESTION_ID }
  },
  result: {
    data: {
      question: {
        id: QUESTION_ID,
        required: isRequired,
        products: []
      }
    }
  }
})

describe('ChooseProduct', () => {
  let testRender
  let prompt
  let secondaryPrompt
  let value
  let onChange
  let productsSkip
  let mockClient

  beforeEach(() => {
    prompt = ''
    secondaryPrompt = ''
    value = ''
    onChange = jest.fn()
    productsSkip = ''

    mockClient = createApolloMockClient({
      mocks: [mockQuestionQuery(false)]
    })
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: SURVEY_ID,
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: []
        }
      }
    })
    mockClient.cache.writeQuery({
      query: SURVEY_QUERY,
      variables: { id: SURVEY_ID },
      data: {
        survey: {
          id: SURVEY_ID,
          products: [],
          minimumProducts: 1,
          customButtons: {},
          state: 'draft'
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ChooseProduct', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <ChooseProduct
            prompt={prompt}
            secondaryPrompt={secondaryPrompt}
            value={value}
            onChange={onChange}
            productsSkip={productsSkip}
          />
        </Router>
      </ApolloProvider>
    )

    expect(testRender.find(ChooseProduct)).toHaveLength(1)

    act(() => {
      testRender
        .find(ChooseProductContainer)
        .first()
        .prop('handleChange')({
          target: {
            value: '1-test'
          }
        })
    })
    expect(onChange).toHaveBeenCalledWith({
      isValid: true,
      skippingTarget: undefined,
      values: ['[object Object]']
    })
  })
})
