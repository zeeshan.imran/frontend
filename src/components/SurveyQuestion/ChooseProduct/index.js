import React from 'react'
import ChooseProductContainer from '../../../containers/ChooseProduct'
import getAnswerSingleValue from '../getAnswerSingleValue'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'

const ChooseProduct = ({
  value,
  onChange,
  country,
  productsSkip,
  autoAdvance,
  onSubmitReady = () => {},
  onSubmitNotReady = () => {}
}) => {
  const selectedProduct = getAnswerSingleValue(value)
  return (
    <ChooseProductContainer
      selectedProduct={selectedProduct}
      productsSkip={productsSkip}
      handleChange={v => {
        onChange({ ...mapSingleValueToAnswer(v), isValid: true })
        !v && onSubmitNotReady()
        v && autoAdvance && onSubmitReady()
      }}
      country={country}
    />
  )
}

export default ChooseProduct
