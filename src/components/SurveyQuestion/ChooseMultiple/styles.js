import styled from 'styled-components'
import BaseButton from '../../Button'
import { StyledCheckbox } from '../../Checkbox/styles'

export const OptionsContainer = styled.div`
  ${StyledCheckbox} {
    display: flex;
    align-items: center;
    margin-bottom: 2.2rem;
  }
  .ant-checkbox-wrapper + span,
  .ant-checkbox + span {
    padding-left: 1rem;
    flex: 1;
  }
  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0;
  }
  margin-bottom: 2.2rem;
  label {
    margin-top: 2.2rem;
  }
`

export const OtherContainer = styled.div`
  width: 50%;
`

export const OptionContainer = styled.div`
  display: flex;
  align-items: center;
  input {
    margin-bottom: 2.2rem;
  }
`

export const ButtonContainer = styled.div`
  width: ${({ fullWidth }) => (fullWidth ? '100%' : '25rem')};
  margin-top: 1.5rem;
`

export const Button = styled(BaseButton)`
  width: 100%;
`
export const InformationText = styled.div`
  display: block;
  padding: 0 0 0 0.25rem;
  line-height: 1.2;
  color: #888;
  font-size: 1.3rem;
  font-style: italic;
`
