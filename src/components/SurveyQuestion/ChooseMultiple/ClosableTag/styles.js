import styled from 'styled-components'
import { Tag as AntTag } from 'antd'

export const StyledTag = styled(AntTag)`
  min-height: 3.2rem;
  height: auto;
  border-radius: 1.6rem;
  justify-content: center;
  align-items: center;
  display: flex;
  user-select: none;
  padding: 0.8rem 1.2rem;
  margin-bottom: 1rem;
  &.ant-tag-checkable {
    border: 1px solid rgb(205, 205, 210);
  }
  max-width: 35rem;
  white-space: pre-wrap;

  &.ant-tag-checkable-checked {
    border: 1px solid transparent;
  }
`
