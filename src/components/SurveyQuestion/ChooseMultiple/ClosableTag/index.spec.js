import React from 'react'
import { mount } from 'enzyme'
import ClosableTag from '.'
import { act } from 'react-dom/test-utils'
import { StyledTag } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('ClosableTag', () => {
  let testRender
  let name
  let handleClose

  beforeEach(() => {
    name = ''
    handleClose = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ClosableTag', async () => {
    testRender = mount(<ClosableTag name={name} handleClose={handleClose} />)

    expect(testRender.find(ClosableTag)).toHaveLength(1)
    
    act(() => {
      testRender
        .find(StyledTag)
        .first()
        .prop('onClose')()
    })

    expect(handleClose).toHaveBeenCalled()
  })
})
