import React from 'react'
import { storiesOf } from '@storybook/react'
import SurveySelectMultiple from './'

const multipleChoiceQuestion = {
  id: '0000000011',
  prompt: 'Example of a multiple choice question prompt',
  secondaryPrompt: 'Select one or more options',
  options: [
    {
      id: 'option1',
      value: 'Option 1'
    },
    {
      id: 'option2',
      value: 'Option 2'
    },
    {
      id: 'option3',
      value: 'Option 3'
    },
    {
      id: 'option4',
      value: 'Option 4'
    }
  ]
}

storiesOf('SurveySelectMultiple', module).add('Story', () => (
  <SurveySelectMultiple
    question={multipleChoiceQuestion}
    onChange={selectedElements =>
      console.log('selectedElements', selectedElements)
    }
  />
))
