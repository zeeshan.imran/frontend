import React from 'react'
import { mount } from 'enzyme'
import AddCustomModal from '.'
import { act } from 'react-dom/test-utils'
import CustomTag from './CustomTag'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('AddCustomModal', () => {
  let testRender
  let visible
  let handleCancel
  let handleOk

  beforeEach(() => {
    visible = true
    handleCancel = jest.fn()
    handleOk = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AddCustomModal', async () => {
    testRender = mount(
      <AddCustomModal
        visible={visible}
        handleCancel={handleCancel}
        handleOk={handleOk}
      />
    )

    expect(testRender.find(AddCustomModal)).toHaveLength(1)

    act(() => {
      testRender
        .find(CustomTag)
        .first()
        .prop('handleCancel')()
    })

    expect(handleCancel).toHaveBeenCalled()
  })
})
