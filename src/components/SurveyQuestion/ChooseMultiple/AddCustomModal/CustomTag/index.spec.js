import React from 'react'
import { mount } from 'enzyme'
import CustomFlavor from '.'
import { act } from 'react-dom/test-utils'
import { StyledButton } from './styles'
import Input from '../../../../Input'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('CustomFlavor', () => {
  let testRender
  let handleCancel
  let handleSubmit
  let mockFunction
  beforeEach(() => {
    handleCancel = jest.fn()
    handleSubmit = jest.fn()
    mockFunction = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CustomFlavor', async () => {
    testRender = mount(
      <CustomFlavor handleCancel={handleCancel} handleSubmit={handleSubmit} />
    )

    expect(testRender.find(CustomFlavor)).toHaveLength(1)

    testRender
      .find(StyledButton)
      .first()
      .prop('onClick')()

    expect(handleCancel).toHaveBeenCalled()

    testRender
      .find(StyledButton)
      .last()
      .prop('onClick')()

    expect(handleSubmit).toHaveBeenCalled()
  })

  test('should render CustomFlavor input onchange', async () => {
    testRender = mount(
      <CustomFlavor handleCancel={handleCancel} handleSubmit={handleSubmit} />
    )

    act(() => {
      testRender.find(Input).prop('onChange')({
        target: { value: 'antInputChange test' }
      })
    })

    testRender.update()
    expect(mockFunction).not.toHaveBeenCalled()
  })
})
