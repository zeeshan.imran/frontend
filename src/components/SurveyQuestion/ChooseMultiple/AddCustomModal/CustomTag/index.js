import React, { useState } from 'react'
import Input from '../../../../Input'
import { ButtonGroup, StyledButton } from './styles'
import { withTranslation } from 'react-i18next'

const CustomFlavor = ({ handleCancel, handleSubmit, t }) => {
  const [customFlavor, setCustomFlavor] = useState('')

  return (
    <React.Fragment>
      <Input
        placeholder={t('placeholders.inputOption')}
        onChange={e => setCustomFlavor(e.target.value)}
      />
      <ButtonGroup>
        <StyledButton onClick={() => handleCancel()} block type='secondary'>
          {t('buttons.cancel')}
        </StyledButton>
        <StyledButton
          block
          type={customFlavor === '' ? 'disabled' : 'primary'}
          onClick={() => handleSubmit(customFlavor)}
        >
          {t('buttons.submit')}
        </StyledButton>
      </ButtonGroup>
    </React.Fragment>
  )
}

export default withTranslation()(CustomFlavor)
