import React from 'react'
import Modal from '../../../Modal'
import CustomTag from './CustomTag'
import { ModalContent, Title, Prompt } from './styles'
import { withTranslation } from 'react-i18next'

const AddCustomModal = ({ visible, handleCancel, handleOk, t }) => (
  <Modal visible={visible}>
    <ModalContent>
      <Title>{t('components.surveyInfoTest.addYourOwn')}</Title>
      <Prompt>{t('components.surveyInfoTest.writeOption')}</Prompt>
      {visible && (
        <CustomTag handleCancel={handleCancel} handleSubmit={handleOk} />
      )}
    </ModalContent>
  </Modal>
)

export default withTranslation()(AddCustomModal)
