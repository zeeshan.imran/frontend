import styled from 'styled-components'
import Text from '../../../Text'
import { family } from '../../../../utils/Fonts'
import colors from '../../../../utils/Colors'

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Title = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 2rem;
  color: rgba(0, 0, 0, 0.85);
  margin-bottom: 1.5rem;
`

export const Prompt = styled(Text)`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  color: ${colors.SLATE_GREY};
  margin-bottom: 2.5rem;
`
