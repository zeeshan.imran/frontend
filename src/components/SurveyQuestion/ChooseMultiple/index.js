import React, { useState } from 'react'
import { findIndex, curry, set, lensIndex, remove, append } from 'ramda'
import Checkbox from '../../Checkbox'
import {
  OptionsContainer,
  OptionContainer,
  InformationText,
  OtherContainer
} from './styles'
import { withTranslation } from 'react-i18next'
import ImageLabelSelectionMultiple from '../ImageLabelSelectionMultiple'
import Input from '../../Input'

export const isSameValueWith = curry((op1, op2) => {
  const value1 = op1.value || op1
  const value2 = op2.value || op2
  return value1 === value2
})

export const isOptionSelected = (selectedOptions, option) =>
  findIndex(isSameValueWith(option))(selectedOptions) >= 0

const ChooseMultiple = ({
  value: selectedOptions = [],
  onChange,
  prompt,
  secondaryPrompt,
  options,
  settings,
  t,
  optionDisplayType
}) => {
  const [otherValue, setOtherValue] = useState('')

  const handleValueChange = values => {
    getErrorMessages(values)
    onChange({
      values,
      isValid: !needCheckMore
    })
  }

  const toggleOption = option => {
    const optionIndex = findIndex(isSameValueWith(option), selectedOptions)
    if (optionIndex > -1) {
      return handleValueChange(remove(optionIndex, 1, selectedOptions))
    } else {
      return handleValueChange(append(option, selectedOptions))
    }
  }

  const handleUpdateOption = option => {
    const optionIndex = findIndex(isSameValueWith(option), selectedOptions)
    const nextSelectedOptions = set(
      lensIndex(optionIndex),
      option,
      selectedOptions
    )
    return handleValueChange(nextSelectedOptions)
  }

  let canCheckMore = true
  let needCheckMore = false
  let errorMessage = ''

  const getErrorMessages = (values = selectedOptions) => {
    if (
      settings !== null &&
      settings.minAnswerValues !== null &&
      settings.maxAnswerValues === null
    ) {
      canCheckMore = true
      needCheckMore = values.length < settings.minAnswerValues
      errorMessage = t(
        'components.surveyQuestion.onlyMinAnswerValues',
        settings
      )
    } else if (
      settings !== null &&
      settings.minAnswerValues == null &&
      settings.maxAnswerValues !== null
    ) {
      canCheckMore = values.length < settings.maxAnswerValues
      needCheckMore = values.length > settings.maxAnswerValues
      errorMessage = t(
        'components.surveyQuestion.onlyMaxAnswerValues',
        settings
      )
    } else if (
      settings !== null &&
      settings.minAnswerValues !== null &&
      settings.maxAnswerValues !== null &&
      settings.minAnswerValues === settings.maxAnswerValues
    ) {
      canCheckMore = values.length < settings.maxAnswerValues
      needCheckMore = values.length < settings.minAnswerValues
      errorMessage = t(
        'components.surveyQuestion.minMaxSameAnswerValues',
        settings
      )
    } else if (
      settings !== null &&
      settings.minAnswerValues !== null &&
      settings.maxAnswerValues !== null &&
      settings.minAnswerValues !== settings.maxAnswerValues
    ) {
      canCheckMore = values.length < settings.maxAnswerValues
      needCheckMore = values.length < settings.minAnswerValues
      errorMessage = t(
        'components.surveyQuestion.minMaxDiffAnswerValues',
        settings
      )
    } else {
      canCheckMore = true
      needCheckMore = false
      errorMessage = ''
    }
  }
  if (settings) {
    getErrorMessages()
  }

  return (
    <React.Fragment>
      <OptionsContainer>
        {optionDisplayType === 'labelOnly' ? (
          <React.Fragment>
            {options.map((option, index) => (
              <OptionContainer>
                <Checkbox
                  disabled={
                    !isOptionSelected(selectedOptions, option.value) &&
                    !canCheckMore
                  }
                  key={`multipleChoice-${index}`}
                  checked={!!isOptionSelected(selectedOptions, option.value)}
                  onChange={() => toggleOption(option.value)}
                  bigger
                >
                  {option.label === 'Other'
                    ? t('containers.questionCreationCardBody.choose-one.other')
                    : option.label}
                </Checkbox>
                {isSameValueWith(option, 'other') &&
                  !!isOptionSelected(selectedOptions, 'other') && (
                    <OtherContainer>
                      <Input
                        autoFocus
                        value={otherValue}
                        onChange={e => {
                          let value = e.target.value.replace(/^\s+/g, '')
                          setOtherValue(value)
                          handleUpdateOption({
                            value: 'other',
                            desc: value || null
                          })
                        }}
                        placeholder={t('placeholders.typeHere')}
                      />
                    </OtherContainer>
                  )}
              </OptionContainer>
            ))}
          </React.Fragment>
        ) : (
          <ImageLabelSelectionMultiple
            options={options}
            optionDisplayType={optionDisplayType}
            canCheckMore={canCheckMore}
            selectedOptions={selectedOptions}
            isOptionSelected={isOptionSelected}
            toggleOption={toggleOption}
            otherValue={otherValue}
            setOtherValue={setOtherValue}
            handleUpdateOption={handleUpdateOption}
            isSameValueWith={isSameValueWith}
          />
        )}
      </OptionsContainer>
      {(needCheckMore || !canCheckMore) && (
        <InformationText>{errorMessage}</InformationText>
      )}
    </React.Fragment>
  )
}

export default withTranslation()(ChooseMultiple)
