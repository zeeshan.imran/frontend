import React from 'react'
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme'
import '../../../utils/internationalization/i18n'
import ChooseMultiple, { isSameValueWith, isOptionSelected } from '.'
import Checkbox from '../../Checkbox'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import wait from '../../../utils/testUtils/waait'

describe('isSameValueWith, isOptionSelected specs', () => {
  const selectedOptions = [{ value: '1' }, { value: '2' }, { value: 'other' }]
  test('{ value: "1" } should be the same "1"', () => {
    expect(isSameValueWith({ value: '1' }, '1')).toBe(true)
    expect(isSameValueWith('1', { value: '1' })).toBe(true)
    expect(isSameValueWith('2', { value: '1' })).toBe(false)
    expect(isSameValueWith({ value: '1' }, 2)).toBe(false)
  })
  test('{ value: "1" } should be the same { value: "1" }', () => {
    expect(isSameValueWith({ value: '1' }, { value: '1' })).toBe(true)
    expect(isSameValueWith({ value: '1' }, { value: '2' })).toBe(false)
  })
  test('"1" should be the same "1', () => {
    expect(isSameValueWith('1')('1')).toBe(true)
    expect(isSameValueWith('1')('2')).toBe(false)
  })

  test('isOptionSelected can match string', () => {
    expect(isOptionSelected(selectedOptions, '1')).toBe(true)
    expect(isOptionSelected(selectedOptions, '3')).toBe(false)
  })

  test('isOptionSelected can match object', () => {
    expect(isOptionSelected(selectedOptions, 'other')).toBe(true)
    expect(
      isOptionSelected(selectedOptions, { value: 'other', desc: 'text' })
    ).toBe(true)
  })
})

describe('survey question -> choose multiple component', () => {
  let testRender
  let handleChange
  let client
  const mockOptions = [
    {
      label: '1',
      value: '1-v'
    },
    {
      label: '2',
      value: '2-v'
    }
  ]

  beforeEach(() => {
    handleChange = jest.fn()
    client = createApolloMockClient()
  })
  afterEach(() => {
    testRender.unmount()
  })
  test('select one value', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseMultiple
          settings={{ minAnswerValues: 1, maxAnswerValues: 2 }}
          onChange={handleChange}
          options={mockOptions}
          optionDisplayType={`labelOnly`}
        />
      </ApolloProvider>
    )
    act(() => {
      testRender
        .find(Checkbox)
        .first()
        .prop('onChange')()
    })
    await wait(0)
    expect(handleChange).toHaveBeenCalledWith({
      values: ['1-v'],
      isValid: true
    })
  })
  test('select multiple value', async () => {
    testRender = mount(
      <ChooseMultiple
        settings={{ minAnswerValues: 1, maxAnswerValues: 2 }}
        onChange={handleChange}
        options={mockOptions}
        value={['1-v']}
        optionDisplayType={`labelOnly`}
      />
    )
    act(() => {
      testRender
        .find(Checkbox)
        .at(1)
        .prop('onChange')()
    })
    expect(
      testRender
        .find(Checkbox)
        .first()
        .prop('checked')
    ).toBe(true)
    expect(handleChange).toHaveBeenCalledWith({
      values: ['1-v', '2-v'],
      isValid: true
    })
  })
  test('deselect one value', async () => {
    testRender = mount(
      <ChooseMultiple
        settings={{ minAnswerValues: 1, maxAnswerValues: 2 }}
        onChange={handleChange}
        options={mockOptions}
        value={['1-v']}
        optionDisplayType={`labelOnly`}
      />
    )
    act(() => {
      testRender
        .find(Checkbox)
        .first()
        .prop('onChange')()
    })
    expect(
      testRender
        .find(Checkbox)
        .first()
        .prop('checked')
    ).toBe(true)
    expect(handleChange).toHaveBeenCalledWith({
      values: [],
      isValid: false
    })
  })
})
