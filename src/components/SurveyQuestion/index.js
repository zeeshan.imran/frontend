import React, { useEffect } from 'react'
import SurveyNavigation from '../../containers/SurveyNavigation'
import SurveySharing from '../../containers/SurveySharing'
import kebabToComponentCase from '../../utils/kebabToComponentCase'
import TermsAndPrivacyFooter from '../../containers/TermsAndPrivacyFooter'
import useResponsive from '../../utils/useResponsive'
import { HIDE_SHARING_LINK_QUESTION_TYPES } from '../../utils/Constants'
import { Row, Col } from 'antd'
import ProductImage from './ProductImage'
import BaseLayout from './BaseLayout'

const AUTO_ADVANCE_SUPPORTED = [
  'slider',
  'vertical-rating',
  'dropdown',
  'choose-one',
  'open-answer',
  'email',
  'numeric',
  'select-and-justify'
]
const SHOW_NEXT_WHEN_AUTO_ADVANCE = [
  'open-answer',
  'email',
  'numeric',
  'select-and-justify'
]

/**
 * @param {Object} props
 * @param {Func} props.onSubmitReady - Trigger after the question is answered & ready to submit. It can be called multiple times when the taster changes the answer's values.
 */

const SurveyQuestion = ({
  type,
  onSubmit,
  onSubmitSuccess,
  onSubmitError,
  onSubmitReady,
  onSubmitNotReady,
  autoAdvanceSettings,
  country,
  productPictureUrl,
  hideProductPicture,
  productName,
  reward,
  prompt,
  secondaryPrompt,
  isBaseLayout = true,
  isSecondaryPrompt,
  optionDisplayType,
  showProductImage,
  ...otherQuestionProps
}) => {
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  if (!type) {
    return null
  }
  const { mobile } = useResponsive()
  const componentName = kebabToComponentCase(type)
  const QuestionComponent = require(`./${componentName}`).default

  const isSubmitHidden =
    autoAdvanceSettings.active &&
    AUTO_ADVANCE_SUPPORTED.includes(type) &&
    !SHOW_NEXT_WHEN_AUTO_ADVANCE.includes(type) &&
    autoAdvanceSettings.hideNextButton

  return (
    <React.Fragment>
      {isBaseLayout ? (
        <BaseLayout
          prompt={prompt}
          secondaryPrompt={isSecondaryPrompt && secondaryPrompt}
        >
          <Row type='flex'>
            <Col
              xs={{ span: 24, order: 1 }}
              lg={{
                span: productPictureUrl ? (hideProductPicture ? 24 : 12) : 24,
                order: 1
              }}
            >
              <QuestionComponent
                type={type}
                autoAdvance={autoAdvanceSettings.active}
                onSubmitReady={onSubmitReady}
                onSubmitNotReady={onSubmitNotReady}
                {...otherQuestionProps}
                country={country}
                prompt={prompt}
                secondaryPrompt={secondaryPrompt}
                optionDisplayType={optionDisplayType}
              />
            </Col>
            {showProductImage && (
              <ProductImage
                reward={reward}
                src={productPictureUrl}
                description={productName}
                productPictureUrl={productPictureUrl}
                hideProductPicture={hideProductPicture}
              />
            )}
          </Row>
        </BaseLayout>
      ) : (
        <Row type='flex'>
          <Col
            xs={{ span: 24, order: 1 }}
            lg={{
              span: productPictureUrl ? (hideProductPicture ? 24 : 12) : 24,
              order: 1
            }}
          >
            <QuestionComponent
              type={type}
              autoAdvance={autoAdvanceSettings.active}
              onSubmitReady={onSubmitReady}
              {...otherQuestionProps}
              country={country}
              prompt={prompt}
              secondaryPrompt={secondaryPrompt}
              optionDisplayType={optionDisplayType}
            />
          </Col>
          {showProductImage && (
            <ProductImage
              reward={reward}
              src={productPictureUrl}
              description={productName}
              productPictureUrl={productPictureUrl}
              hideProductPicture={hideProductPicture}
            />
          )}
        </Row>
      )}
      <SurveyNavigation
        withSkipButton={type === 'choose-product'}
        onSubmit={onSubmit}
        onSubmitSuccess={onSubmitSuccess}
        onSubmitError={onSubmitError}
        country={country}
        showSubmit={!isSubmitHidden}
      />
      {
        !HIDE_SHARING_LINK_QUESTION_TYPES.includes(type)  && <SurveySharing isDesktop={!mobile} />
      }
      <TermsAndPrivacyFooter isSurvey={!mobile} />
    </React.Fragment>
  )
}

export default SurveyQuestion
