import React from 'react'
import ProfileCharts from '../../../containers/ProfileCharts'

import { Title, ChartContainer } from './styles'
import useResponsive from '../../../utils/useResponsive/index'
import { Col, Row } from 'antd'

const Profile = ({ prompt, relatedQuestions }) => {
  const { desktop } = useResponsive()
  return (
    <Row>
      <Col xs={24} md={24}>
        <Title>{prompt}</Title>
        <ChartContainer desktop={desktop}>
          <ProfileCharts questions={relatedQuestions} />
        </ChartContainer>
      </Col>
    </Row>
  )
}

export default Profile
