import React from 'react'
import { storiesOf } from '@storybook/react'
import Info from './'

const prompt = 'Example Title'

const secondaryPrompt = 'Example Description'

storiesOf('Info', module).add('Story', () => (
  <Info prompt={prompt} secondaryPrompt={secondaryPrompt} />
))
