import React from 'react'
import { shallow } from 'enzyme'
import Info from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Info', () => {
  let testRender
  let onClick
  let prompt
  let secondaryPrompt

  beforeEach(() => {
    onClick = jest.fn()
    prompt = ''
    secondaryPrompt = ''
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Info', async () => {
    testRender = shallow(
      <Info
        onClick={onClick}
        prompt={prompt}
        secondaryPrompt={secondaryPrompt}
      />
    )

    expect(testRender).toMatchSnapshot()
  })
})
