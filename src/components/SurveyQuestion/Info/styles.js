import styled from 'styled-components'
import TextComponent from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const BaseText = styled(TextComponent)`
  font-family: ${family.primaryLight};
  color: ${colors.SLATE_GREY};
`

export const Text = styled(BaseText)`
  font-size: 1.8rem;
  opacity: 0.7;
`
