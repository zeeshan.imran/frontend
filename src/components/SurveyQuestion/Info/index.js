import React from 'react'
import { Desktop } from '../../Responsive'
import { Text } from './styles'

const Info = ({ secondaryPrompt }) => {
  return (
    <Desktop>
      {desktop => (
        <Text dangerouslySetInnerHTML={{ __html: secondaryPrompt }} />
      )}
    </Desktop>
  )
}

export default Info
