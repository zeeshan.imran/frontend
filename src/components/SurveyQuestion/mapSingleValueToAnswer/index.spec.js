import React from 'react'
import { shallow } from 'enzyme'
import value from '.'

describe('value', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render value', async () => {
    testRender = shallow(<value />)
    expect(testRender).toMatchSnapshot()
  })
})
