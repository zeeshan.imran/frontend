import { isNil } from 'ramda'

// Answer can be plain string or object with value field
export default value => ({
  values: isNil(value)
    ? []
    : typeof value === 'object' && !isNil(value.value)
      ? [{ value: `${value.value}`, desc: `${value.desc}` }]
      : [`${value}`]
})
