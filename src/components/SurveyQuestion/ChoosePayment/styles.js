import styled from 'styled-components'
import { Container as RadioButtonContainer } from '../../RadioButton/styles'
import { Container as InputContainer } from '../../FieldLabel/styles'

export const ButtonContainer = styled.div`
  margin-bottom: ${({ hasMargin }) => (hasMargin ? '10px' : 0)};
  flex-direction: row;
  display: flex;
  min-height: 4rem;
  ${RadioButtonContainer} {
    display: flex;
    width: 100%;
    margin-bottom: 5px;
    margin-top: 5px;
    min-height: 4rem;
    align-items: normal;
  }
  ${InputContainer} {
    margin-left: 20px;
  }
`
