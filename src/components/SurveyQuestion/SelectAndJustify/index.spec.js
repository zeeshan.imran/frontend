import React from 'react'
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme'
import SelectAndJustify from '.'
import Select from '../../Select'
import '../../../utils/internationalization/i18n'
import Input from '../../Input'
import { canSkipCurrentQuestion } from '../../../containers/SurveyQuestion'

describe('select-and-justify validattion & skiping rule', () => {
  let testRender
  let handleChange

  beforeEach(() => {
    handleChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  const expectCanSubmitMandatory = () =>
    expect(handleChange.mock.calls[0][0].isValid)

  const expectCanSkipNonMandatory = () =>
    expect(
      canSkipCurrentQuestion({
        question: { required: false },
        canSkipAnswer: handleChange.mock.calls[0][0].canSkipAnswer
      })
    )

  const expectCanSkipMandatory = () =>
    expect(
      canSkipCurrentQuestion({
        question: { required: true },
        canSkipAnswer: handleChange.mock.calls[0][0].canSkipAnswer
      })
    )

  test('can skip the non-mandatory question, can not submit the mandatory question if only the select field was filled', async () => {
    testRender = mount(
      <SelectAndJustify
        value={[undefined, undefined]}
        onChange={handleChange}
        options={[]}
        required
      />
    )

    act(() => {
      testRender
        .find(Select)
        .first()
        .prop('onChange')('OP_1')
    })

    expect(handleChange).toHaveBeenCalledWith({
      values: ['OP_1', undefined],
      isValid: false,
      canSkipAnswer: true
    })

    expectCanSubmitMandatory().toBe(false)
    expectCanSkipNonMandatory().toBe(true)
    expectCanSkipMandatory().toBe(false)
  })

  test('can skip the non-mandatory question, can not submit the mandatory question if only the justify field was filled', async () => {
    testRender = mount(
      <SelectAndJustify
        value={[undefined, undefined]}
        onChange={handleChange}
        options={[]}
        required
      />
    )

    act(() => {
      testRender
        .find(Input)
        .first()
        .prop('onChange')({ target: { value: 'justify text' } })
    })

    expect(handleChange).toHaveBeenCalledWith({
      values: [undefined, 'justify text'],
      isValid: false,
      canSkipAnswer: true
    })

    expectCanSubmitMandatory().toBe(false)
    expectCanSkipNonMandatory().toBe(true)
    expectCanSkipMandatory().toBe(false)
  })

  test('can skip the non-mandatory question, can submit the mandatory question if both fields is filled', async () => {
    testRender = mount(
      <SelectAndJustify
        value={['OP_1', undefined]}
        onChange={handleChange}
        options={[]}
        required
      />
    )

    act(() => {
      testRender
        .find(Input)
        .first()
        .prop('onChange')({ target: { value: 'justify text' } })
    })

    expect(handleChange).toHaveBeenCalledWith({
      values: ['OP_1', 'justify text'],
      isValid: true,
      canSkipAnswer: true
    })

    expectCanSubmitMandatory().toBe(true)
    expectCanSkipNonMandatory().toBe(true)
    expectCanSkipMandatory().toBe(false)
  })
})
