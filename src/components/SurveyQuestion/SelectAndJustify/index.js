import React from 'react'
import BaseLayout from '../BaseLayout'
import Select from '../../Select'
import {
  getOptionValue,
  getOptionName
} from '../../../utils/getters/OptionGetters'
import Input from '../../Input'
import { withTranslation } from 'react-i18next'
import { trimSpaces } from '../../../utils/trimSpaces'

const SELECTED_OPTION = 0
const JUSTIFICATION = 1

const isValid = values =>
  !!values[SELECTED_OPTION] &&
  !!values[JUSTIFICATION] &&
  !!trimSpaces(values[JUSTIFICATION])

const SelectAndJustify = ({
  options,
  secondaryPrompt,
  value = {},
  onChange,
  autoAdvance,
  onSubmitReady = () => {},
  t
}) => {
  const handleChange = values => {
    onChange({
      values,
      isValid: isValid(values),
      canSkipAnswer: true
    })
  }
  return (
    <React.Fragment>
      <Select
        value={value[SELECTED_OPTION]}
        onChange={selectedValue => {
          return handleChange([selectedValue, value[JUSTIFICATION]])
        }}
        placeholder={t('placeholders.selectOne')}
        options={options}
        getOptionValue={getOptionValue}
        getOptionName={getOptionName}
      />

      <BaseLayout prompt={secondaryPrompt}>
        <Input
          value={value[JUSTIFICATION]}
          onChange={e => {
            const justification = e.target.value
            return handleChange([value[SELECTED_OPTION], justification])
          }}
          onKeyUp={e => {
            if (e.key === 'Enter' && isValid(value)) {
              autoAdvance && onSubmitReady()
            }
          }}
          placeholder={t('placeholders.addOpinion')}
        />
      </BaseLayout>
    </React.Fragment>
  )
}
export default withTranslation()(SelectAndJustify)
