import React, { useState } from 'react'
import RadioButton from '../../RadioButton'
import ImageLabelSelection from '../ImageLabelSelection'
import getAnswerSingleValue from '../getAnswerSingleValue'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'
import Input from '../../Input'
import { withTranslation } from 'react-i18next'

import { ButtonContainer, OtherContainer } from './styles'

const ChooseOne = ({
  value,
  onChange,
  options,
  autoAdvance,
  optionDisplayType,
  onSubmitReady = () => {},
  t
}) => {
  const [isOtherShown, setIsOtherShown] = useState(false)
  const [otherValue, setOtherValue] = useState('')
  const formattedValue = getAnswerSingleValue(value)
  const handleChange = ({ v, canSkipAnswer, isValid }) => {
    return onChange({
      ...mapSingleValueToAnswer(v),
      canSkipAnswer,
      isValid
    })
  }

  return (
    <React.Fragment>
      {optionDisplayType === 'labelOnly' ? (
        <React.Fragment>
          {options.map((option, index) => (
            <ButtonContainer
              key={`selectOne-${index}`}
              hasMargin={index !== options.length - 1}
            >
              <RadioButton
                checked={
                  option.isOpenAnswer
                    ? isOtherShown
                    : !isOtherShown && formattedValue === option.value
                }
                onClick={() => {
                  if (option.isOpenAnswer) {
                    handleChange({
                      v: {
                        value: option.value,
                        desc: otherValue
                      },
                      canSkipAnswer: false,
                      isValid: otherValue.length > 0
                    })
                    setIsOtherShown(true)
                  } else {
                    handleChange({
                      v: option.value,
                      isValid: true
                    })
                    setIsOtherShown(false)
                    autoAdvance && onSubmitReady()
                  }
                }}
                suffix={
                  option.isOpenAnswer &&
                  isOtherShown && (
                    <OtherContainer>
                      <Input
                        autoFocus
                        value={otherValue}
                        onChange={e => {
                          // trim only from start
                          let value = e.target.value.replace(/^\s+/g, '')
                          setOtherValue(value)
                          handleChange({
                            v: {
                              value: option.value,
                              desc: value || null
                            },
                            canSkipAnswer: !!value,
                            isValid: !!value
                          })
                        }}
                        onKeyUp={e => {
                          if (e.key === 'Enter') {
                            autoAdvance && onSubmitReady()
                          }
                        }}
                        placeholder={t('placeholders.typeHere')}
                      />
                    </OtherContainer>
                  )
                }
              >
                {option.label === 'Other'
                  ? t('containers.questionCreationCardBody.choose-one.other')
                  : option.label}
              </RadioButton>
            </ButtonContainer>
          ))}
        </React.Fragment>
      ) : (
        <ImageLabelSelection
          options={options}
          optionDisplayType={optionDisplayType}
          isOtherShown={isOtherShown}
          formattedValue={formattedValue}
          otherValue={otherValue}
          onChange={handleChange}
          onSetIsOtherShown={setIsOtherShown}
          autoAdvance={autoAdvance}
          onSubmitReady={onSubmitReady}
          onSetOtherValue={setOtherValue}
        />
      )}
    </React.Fragment>
  )
}

export default withTranslation()(ChooseOne)
