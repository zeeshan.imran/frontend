import React from 'react'
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme'
import '../../../utils/internationalization/i18n'
import Input from '../../Input'
import ChooseOne from '.'
import RadioButton from '../../RadioButton'

describe('survey question -> choose one component', () => {
  let testRender
  let handleChange

  beforeEach(() => {
    handleChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('select value', async () => {
    const mockOptionsCanSkip = [
      {
        isValid: true,
        value: '2-v',
        canSkipAnswer: true
      }
    ]

    testRender = mount(
      <ChooseOne
        optionDisplayType={`labelOnly`}
        onChange={handleChange}
        options={mockOptionsCanSkip}
      />
    )

    act(() => {
      testRender
        .find(RadioButton)
        .first()
        .prop('onClick')()
    })

    expect(handleChange).toHaveBeenCalledWith({
      values: ['2-v'],
      canSkipAnswer: undefined,
      isValid: true
    })
  })

  test('Should show input if other option selected', async () => {
    const mockOptions = [
      {
        label: '3',
        values: 'other',
        skipTo: true,
        isOpenAnswer: true,
        canSkipAnswer: false
      }
    ]

    testRender = mount(
      <ChooseOne
        optionDisplayType={`labelOnly`}
        onChange={handleChange}
        options={mockOptions}
      />
    )

    testRender
      .find(RadioButton)
      .last()
      .prop('onClick')()

    testRender.update()

    expect(handleChange).toHaveBeenCalledWith({
      canSkipAnswer: false,
      isValid: false,
      values: ['[object Object]']
    })

    expect(testRender.find(Input).length).toBe(1)

    testRender
      .find(Input)
      .props()
      .onChange({ target: { value: 'v-1' } })

    expect(handleChange).toHaveBeenCalledWith({
      canSkipAnswer: true,
      isValid: true,
      values: ['[object Object]']
    })

    testRender
      .find(RadioButton)
      .first()
      .prop('onClick')()

    testRender.update()

    expect(testRender.find(Input).length).toBe(1)
  })
})
