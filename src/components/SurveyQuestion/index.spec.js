import React from 'react'
import { mount } from 'enzyme'
import SurveyQuestion from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import gql from 'graphql-tag'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import defaults from '../../defaults'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { surveyInfo } from '../../fragments/survey'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ''
}))

const QUESTION_ID = 'question-1'
const SURVEY_ID = 'survey-1'

const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      type
    }
  }
`
const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyInfo
    }
  }

  ${surveyInfo}
`

const mockActiveSurvey = {
  __typename: 'Survey',
  name: 'active survey',
  uniqueName: 'uniqueName',
  coverPhoto: null,
  state: 'active',
  minimumProducts: 1,
  maximumProducts: 1,
  surveyLanguage: 'en',
  settings: {
    recaptcha: false
  },
  authorizationType: 'public',
  id: `survey-1`,
  surveyEnrollment: 'survey-enrollment-1',
  context: {},
  value: {
    value: 'value-1'
  },
  range: {},
  options: {},
  pairsOptions: {},
  pairs: {},
  productsSkip: {},
  products: [],
  exclusiveTasters: [],
  allowRetakes: false,
  forcedAccount: false,
  forcedAccountLocation: `start`,
  isScreenerOnly: false,
  showGeneratePdf: false,
  linkedSurveys: [],
  recaptcha: false,
  savedRewards: [],
  maxProductStatCount: 6,
  emailService: false,
  allowedDaysToFillTheTasting: 5,
  customButtons: {
    __typename: 'CustomButtons',
    continue: 'Continue',
    start: 'Start',
    next: 'Next',
    skip: 'Skip'
  },
  startedAt: Date.now(),
  selectedProduct: 'prod-1',
  instructionSteps: [],
  instructionsText: '',
  thankYouText: '',
  rejectionText: '',
  screeningText: '',
  screeningQuestions: {},
  setupQuestions: {},
  productsQuestions: {},
  finishingQuestions: {},
  customizeSharingMessage: '',
  loginText: '',
  pauseText: '',
  productDisplayType: ''
}

const mockDraftSurvey = {
  ...mockActiveSurvey,
  state: 'draft',
  name: 'drafted survey'
}

const mockQuestionQuery = isRequired => ({
  request: {
    query: QUESTION_QUERY,
    variables: { id: QUESTION_ID }
  },
  result: {
    data: {
      question: {
        id: QUESTION_ID,
        required: isRequired,
        products: []
      }
    }
  }
})

describe('SurveyQuestion', () => {
  let testRender
  let type
  let otherQuestionProps
  let mockClient

  beforeEach(() => {
    type = 'choose-product'
    otherQuestionProps = {
      title: 'Gums tasting',
      surveyId: 'survey-1',
      products: []
    }

    mockClient = createApolloMockClient({
      mocks: [mockQuestionQuery(false)]
    })
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: SURVEY_ID,
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: []
        }
      }
    })
    mockClient.cache.writeQuery({
      query: SURVEY_QUERY,
      variables: { id: SURVEY_ID },
      data: {
        survey: {
          ...mockDraftSurvey
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyQuestion', async () => {
    const history = createBrowserHistory()
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveyQuestion
            type={type}
            autoAdvanceSettings={{
              active: false,
              debounce: 0,
              hideNextButton: false
            }}
            {...otherQuestionProps}
            onChange={jest.fn()}
            isBaseLayout
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(SurveyQuestion)).toHaveLength(1)
  })

  test('should render SurveyQuestion type be null', async () => {
    const history = createBrowserHistory()
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveyQuestion
            type={''}
            {...otherQuestionProps}
            onChange={jest.fn()}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(SurveyQuestion)).toHaveLength(1)
  })
})
