import React from 'react'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'
import Select from '../../Select'
import { withTranslation } from 'react-i18next'
import {
  getOptionValue,
  getOptionName
} from '../../../utils/getters/OptionGetters'

const Dropdown = ({
  options,
  value = [],
  onChange,
  autoAdvance,
  onSubmitReady = () => {},
  t
}) => (
  <Select
    value={value[0]}
    onChange={selectedValue => {
      onChange({
        ...mapSingleValueToAnswer(selectedValue),
        isValid: true
      })

      autoAdvance && onSubmitReady()
    }}
    placeholder={t('placeholders.selectOne')}
    options={options}
    getOptionValue={getOptionValue}
    getOptionName={getOptionName}
  />
)
export default withTranslation()(Dropdown)
