import React from 'react'
import { mount } from 'enzyme'
import OpenAnswer from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import gql from 'graphql-tag'
import SURVEY_PARTICIPATION_QUERY from '../../../queries/SurveyParticipationQuery'
import defaults from '../../../defaults'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { surveyBasicInfo } from '../../../fragments/survey'
import Input from '../../Input'
import { act } from 'react-dom/test-utils'
import { SURVEY_CREATION } from '../../../queries/SurveyCreation'
import { defaultSurvey } from '../../../mocks'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
    }
  }

  ${surveyBasicInfo}
`

const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    name: 'Survey 1',
    uniqueName: 'survey-1',
  },
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ]
}

const createMockSurveyQuery = state => ({
  request: {
    query: SURVEY_QUERY,
    variables: {
      surveyId: 'survey-1'
    } 
  },
  result: {
    data: {
      survey: {
        id: 'survey-1',
        state
      }
    }
  }
})

describe('OpenAnswer', () => {
  let testRender
  let value
  let onChange
  let prompt
  let secondaryPrompt
  let mockClient

  beforeEach(() => {
    value = []
    onChange = jest.fn()
    prompt = null
    secondaryPrompt = null

    mockClient = createApolloMockClient({
      mocks: [createMockSurveyQuery('draft')]
    })

    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-1',
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: []
        }
      }
    })

    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OpenAnswer', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <OpenAnswer
            value={value}
            onChange={onChange}
            prompt={prompt}
            secondaryPrompt={secondaryPrompt}
          />
        </Router>
      </ApolloProvider>
    )

    expect(testRender.find(OpenAnswer)).toHaveLength(1)

    act(() => {
      testRender
        .find(Input)
        .first()
        .prop('onChange')({
        target: {
          value: '1-test'
        }
      })
    })

    expect(onChange).toHaveBeenCalledWith({ isValid: true, values: ['1-test'] })
  })
})
