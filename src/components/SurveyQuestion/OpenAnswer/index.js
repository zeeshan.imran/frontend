import React from 'react'
import Input from '../../Input'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'
import { withTranslation } from 'react-i18next'
import { trimSpaces } from '../../../utils/trimSpaces'

const isValid = value => !!trimSpaces(value)

const OpenAnswer = ({
  t,
  value,
  onChange,
  autoAdvance,
  onSubmitReady = () => {}
}) => {
  const formattedValue = (value || [])[0] // we only use the first of the "array of values" standard
  const handleChange = v => {
    onChange({ ...mapSingleValueToAnswer(v), isValid: isValid(v) })
  }

  return (
    <Input
      defaultValue={formattedValue}
      onChange={e => handleChange(e.target.value)}
      onKeyUp={e => {
        if (e.key === 'Enter' && formattedValue && isValid(formattedValue)) {
          autoAdvance && onSubmitReady()
        }
      }}
      placeholder={t('placeholders.writeAnswer')}
    />
  )
}

export default withTranslation()(OpenAnswer)
