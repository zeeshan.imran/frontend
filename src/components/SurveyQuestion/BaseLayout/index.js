import React from 'react'
import { Container, Prompt, SecondaryPrompt } from './styles'

const BaseLayout = ({ prompt, secondaryPrompt, children }) => (
  <Container>
    <Prompt>{prompt}</Prompt>
    {secondaryPrompt && <SecondaryPrompt>{secondaryPrompt}</SecondaryPrompt>}
    {children}
  </Container>
)

export default BaseLayout
