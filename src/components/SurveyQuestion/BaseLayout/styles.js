import styled from 'styled-components'
import Text from '../../Text'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'
import { COMPONENTS_DEFAULT_MARGIN } from '../../../utils/Metrics'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 5.5rem;
`

export const Prompt = styled(Text)`
  font-family: ${family.primaryLight};
  color: ${colors.SLATE_GREY};
  font-size: 2.2rem;
  margin-bottom: 1.5rem;
`

export const SecondaryPrompt = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.5rem;
  color: ${colors.SLATE_GREY};
  margin-bottom: ${COMPONENTS_DEFAULT_MARGIN}rem;
`
