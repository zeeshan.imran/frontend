import React, { useState } from 'react'
import { withTranslation } from 'react-i18next'
import { Row } from 'antd'
import useResponsive from '../../../utils/useResponsive'
import Checkbox from '../../Checkbox'
import Input from '../../Input'
import BaseCard from '../../BaseCard'
import Overlay from '../../ProductCard/Overlay'
import StaticUrlOrBase64 from '../../../utils/imagesStaticUrl'

import {
  MainContainer,
  Container,
  Picture,
  Title,
  NoImage,
  OptionContainer,
  OtherContainer,
  Col
} from './styles'

const ImageLabelSelectionMultiple = ({
  options,
  optionDisplayType,
  toggleOption,
  selectedOptions,
  isOptionSelected,
  canCheckMore,
  otherValue,
  setOtherValue,
  handleUpdateOption,
  isSameValueWith,
  t
}) => {
  const { REACT_APP_THEME } = process.env
  
  const { mobile } = useResponsive()
  const [hasOthers, setHasOthers] = useState({})
  const [othersIndex, setOthersIndex] = useState(0)
  return (
    <React.Fragment>
      <Row gutter={16}>
        <MainContainer>
          {options.map((option, index) => {
            const image = StaticUrlOrBase64(option.image800)
            if (!option.isOpenAnswer) {
              return (
                <Col
                  span={REACT_APP_THEME === 'chrHansen' ? 12 : ''} 
                  key={`multipleChoice-${index}`}
                  mobile={mobile}
                >
                  <Container mobile={mobile}>
                    <BaseCard
                      checkbox
                      withSelection
                      onClick={() => toggleOption(option.value)}
                      selected={
                        !!isOptionSelected(selectedOptions, option.value)
                      }
                      lockedCursor={
                        !isOptionSelected(selectedOptions, option.value) &&
                        !canCheckMore
                      }
                      clickDisabled={
                        !isOptionSelected(selectedOptions, option.value) &&
                        !canCheckMore
                      }
                      checkType='close'
                    >
                      { (!isOptionSelected(selectedOptions, option.value) &&
                      !canCheckMore) ? (
                        <Overlay maxRanking={0} ranking={0} locked />
                      ) : null}
                      {image ? (
                        <Picture src={image} />
                      ) : (
                        <NoImage>
                          {optionDisplayType === 'imageOnly' && !image
                            ? option.label
                            : t('components.question.chooseProduct.noImage')}
                        </NoImage>
                      )}
                    </BaseCard>
                    {optionDisplayType === 'imageAndLabel' && (
                      <Title>{option.label}</Title>
                    )}
                  </Container>
                </Col>
              )
            } else {
              if (!Object.keys(hasOthers).length) {
                setHasOthers(option)
                setOthersIndex(index)
              }
              return null
            }
          })}
        </MainContainer>
      </Row>
      <Row>
        {Object.keys(hasOthers).length ? (
          <React.Fragment>
            <OptionContainer>
              <Checkbox
                disabled={
                  !isOptionSelected(selectedOptions, hasOthers.value) &&
                  !canCheckMore
                }
                key={`multipleChoice-${othersIndex}`}
                checked={!!isOptionSelected(selectedOptions, hasOthers.value)}
                onChange={() => toggleOption(hasOthers.value)}
                bigger
              >
                {t('containers.questionCreationCardBody.choose-one.other')}
              </Checkbox>
              {isSameValueWith(hasOthers, 'other') &&
                !!isOptionSelected(selectedOptions, 'other') && (
                  <OtherContainer>
                    <Input
                      autoFocus
                      value={otherValue}
                      onChange={e => {
                        let value = e.target.value.replace(/^\s+/g, '')
                        setOtherValue(value)
                        handleUpdateOption({
                          value: 'other',
                          desc: value || null
                        })
                      }}
                      placeholder={t('placeholders.typeHere')}
                    />
                  </OtherContainer>
                )}
            </OptionContainer>
          </React.Fragment>
        ) : null}
      </Row>
    </React.Fragment>
  )
}

export default withTranslation()(ImageLabelSelectionMultiple)
