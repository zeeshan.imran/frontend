import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'
import { StyledCheckbox } from '../../Checkbox/styles'
import { Col as AntCol } from 'antd'

export const Col = styled(AntCol)`
  max-width: ${({ mobile }) => ( mobile ? `50%` : `100%`)};
  padding-bottom: 3rem;
`

export const MainContainer = styled.div`
  margin-top: 2.5rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`

export const Container = styled.div`
  text-align: center;
  user-select: none;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 100%;
`

export const Image = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
`

export const Picture = styled(Image)`
  width: 100%;
  height: 100%;
`

export const NoImage = styled.span`
  display: block;
  margin-top: 35%;
  word-break: break-all;
`

export const Title = styled.span`
  font-family: ${family.LatoRegular};
  font-size: 1.5rem;
  color: ${colors.SLATE_GREY};
  word-break: break-all;
`


export const OptionsContainer = styled.div`
  ${StyledCheckbox} {
    display: block;
    margin-bottom: 2.2rem;
  }
  .ant-checkbox-wrapper + span,
  .ant-checkbox + span {
    padding-left: 2.2rem;
  }
  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0;
  }
`

export const OtherContainer = styled.div`
  width: 80%;
  margin-top: 20px;
`

export const OptionContainer = styled.div`
  display: flex;
  align-items: center;
  input {
    margin-bottom: 2.2rem;
  }
`