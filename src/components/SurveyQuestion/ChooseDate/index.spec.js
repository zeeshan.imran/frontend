import React from 'react'
import { mount } from 'enzyme'
import ChooseDate from '.'
import { act } from 'react-dom/test-utils'
import { DatePicker } from 'antd'
import moment from 'moment'

jest.mock('i18next', () => ({
  init: () => {},
  t: k => k
}))

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('ChooseDate', () => {
  let testRender
  let prompt
  let secondaryPrompt
  let onChange
  let value

  beforeEach(() => {
    prompt = ''
    secondaryPrompt = ''
    onChange = jest.fn()
    value = moment().format('YYYY-MM-DD')
  })
  afterEach(() => {
    testRender.unmount()
  })

  test('should render ChooseDate', async () => {
    testRender = mount(
      <ChooseDate
        prompt={prompt}
        secondaryPrompt={secondaryPrompt}
        onChange={onChange}
        value={value}
      />
    )
    expect(testRender.find(ChooseDate)).toHaveLength(1)
    act(() => {
      testRender
        .find(DatePicker)
        .first()
        .prop('onChange')('OP_1')
    })
    expect(onChange).toHaveBeenCalled()
  })
})
