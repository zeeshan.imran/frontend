import React, { useEffect } from 'react'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'
import { DatePicker } from './styles'
import moment from 'moment'
import 'moment/locale/fr'
import 'moment/locale/de'
import 'moment/locale/ko'
import 'moment/locale/pt'
import 'moment/locale/es'

// use antd/LIB instead of antd/es - there is a problem in the docs
// https://github.com/ant-design/ant-design/issues/18336#issuecomment-522504314
import * as en from 'antd/lib/date-picker/locale/en_US'
import * as de from 'antd/lib/date-picker/locale/de_DE'
import * as kr from 'antd/lib/date-picker/locale/ko_KR'
import * as fr from 'antd/lib/date-picker/locale/fr_FR'
import * as pt from 'antd/lib/date-picker/locale/pt_PT'
import * as es from 'antd/lib/date-picker/locale/es_ES'

const locale = {
  en: en.default,
  de: de.default,
  kr: kr.default,
  fr: fr.default,
  pt: pt.default,
  es: es.default
}
const ChooseDate = ({ value, onChange, language, ...rest }) => {
  const dateFormat = 'YYYY-MM-DD'
  const handleChange = newValue => {
    const formatedNewValue = moment(newValue).format(dateFormat)
    onChange({
      ...mapSingleValueToAnswer(formatedNewValue),
      isValid: newValue && newValue._isAMomentObject
    })
  }
  let date = null
  if (value && moment(value[0], dateFormat).isValid()) {
    date = moment(value[0], dateFormat)
  }
  useEffect(() => {
    moment.locale(language)
  }, [language])

  return (
    <DatePicker
      value={date}
      onChange={handleChange}
      format={dateFormat}
      mode='date'
      showToday
      locale={locale[language]}
    />
  )
}

export default ChooseDate
