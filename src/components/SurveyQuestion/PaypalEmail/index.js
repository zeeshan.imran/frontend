import React from 'react'
import * as Yup from 'yup'
import InputEmail from '../../InputEmail'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'

const validationSchema = Yup.string()
  .email()
  .required()

const PaypalEmail = ({
  onChange,
  required,
  value: questionValue,
  autoAdvance,
  onSubmitReady = () => {}
}) => {
  const handleChange = value => {
    const { values } = mapSingleValueToAnswer(value)

    const isValid = validationSchema.isValidSync(value)

    onChange({
      values,
      isValid,
      canSkipAnswer: isValid || !value
    })
  }

  return (
    <InputEmail
      onKeyUp={e => {
        if (
          e.key === 'Enter' &&
          Array.isArray(questionValue) &&
          validationSchema.isValidSync(questionValue[0])
        ) {
          autoAdvance && onSubmitReady()
        }
      }}
      handleChange={handleChange}
      isRequired={required}
    />
  )
}

export default PaypalEmail
