import React from 'react'
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme'
import PaypalEmail from '.'
import Input from '../../Input'
import '../../../utils/internationalization/i18n'
import { canSkipCurrentQuestion } from '../../../containers/SurveyQuestion'

describe('PaypalEmail validattion & skiping the PaypalEmail question rule', () => {
  let testRender
  let handleChange

  beforeEach(() => {
    handleChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  const expectCanSubmitMandatory = () =>
    expect(handleChange.mock.calls[0][0].isValid)

  const expectCanSkipNonMandatory = () =>
    expect(
      canSkipCurrentQuestion({
        question: { required: false },
        canSkipAnswer: handleChange.mock.calls[0][0].canSkipAnswer
      })
    )

  const expectCanSkipMandatory = () =>
    expect(
      canSkipCurrentQuestion({
        question: { required: true },
        canSkipAnswer: handleChange.mock.calls[0][0].canSkipAnswer
      })
    )

  test('can skip the non-mandatory question, can not submit the mandatory question while the email input is empty', async () => {
    testRender = mount(
      <PaypalEmail onChange={handleChange} prompt='Email prompt' required />
    )

    testRender
      .find(Input)
      .first()
      .prop('onChange')({ target: { value: '' } })

    expect(handleChange).toHaveBeenCalledWith({
      values: [''],
      isValid: false,
      canSkipAnswer: true
    })

    expectCanSubmitMandatory().toBe(false)
    expectCanSkipNonMandatory().toBe(true)
    expectCanSkipMandatory().toBe(false)
  })

  test('can not skip/submit the question while the email input is aaa', async () => {
    testRender = mount(
      <PaypalEmail onChange={handleChange} prompt='Email prompt' required />
    )

    act(() => {
      testRender
        .find(Input)
        .first()
        .prop('onChange')({ target: { value: 'aaa' } })
    })

    expect(handleChange).toHaveBeenCalledWith({
      values: ['aaa'],
      isValid: false,
      canSkipAnswer: false
    })

    expectCanSubmitMandatory().toBe(false)
    expectCanSkipNonMandatory().toBe(false)
    expectCanSkipMandatory().toBe(false)
  })

  test('should send { isValid: true, values: ["email@domain.com"] } while the email input is email@domain.com', async () => {
    testRender = mount(
      <PaypalEmail onChange={handleChange} prompt='Email prompt' required />
    )

    act(() => {
      testRender
        .find(Input)
        .first()
        .prop('onChange')({ target: { value: 'email@domain.com' } })
    })

    expect(handleChange).toHaveBeenCalledWith({
      values: ['email@domain.com'],
      isValid: true,
      canSkipAnswer: true
    })

    expectCanSubmitMandatory().toBe(true)
    expectCanSkipNonMandatory().toBe(true)
    expectCanSkipMandatory().toBe(false)
  })
})
