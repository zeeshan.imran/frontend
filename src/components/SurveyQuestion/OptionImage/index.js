import React from 'react'
import { Image } from './styles'

const OptionImage = ({
  srcImage
}) => (
  <Image
    src={srcImage}
  />
)

export default OptionImage