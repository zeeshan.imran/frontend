import styled from 'styled-components'

export const Image = styled.img`
  width: 100%;
  height: 50px;
  display: inline-block;
  max-width: 50px;
  border-radius: 10%;
  position: relative;
  :hover {
    transition: all 2s ease-in-out;
    transform: scale(5) translate(30px, -30px);
    z-index: 5;
  }
`