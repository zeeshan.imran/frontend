import React, { useState } from 'react'
import { Formik } from 'formik'
import { Form, Row } from 'antd'
import { withTranslation } from 'react-i18next'
import InputArea from '../../InputArea'
import RadioButton from '../../RadioButton'
import {
  ClearFix,
  FullImage,
  RowStarter,
  RadioButtonContainerLeft,
  RadioButtonContainerRight,
  InfoBox
} from '../styles'

const PhotoValidationForm = ({ photo, setSelectedAnswer }) => {
  if (!photo) {
    return null
  }
  const { value, processed, state, comment } = photo

  const [isImageValid, setIsImageValid] = useState(state === 'valid')
  const [isProcessed, setIsProcessed] = useState(processed)
  const [invalidComment, setInvalidComment] = useState(comment)
  const [characterLimit] = useState(200)
  const [charactersRemaining, setCharactersRemaining] = useState(
    comment ? characterLimit - comment.length : characterLimits
  )

  return (
    <Formik
      render={() => {
        return (
          <React.Fragment>
            <FullImage src={value ? value[0] : null} />
            <RowStarter>
              <Row>
                <RadioButtonContainerLeft>
                  <RadioButton
                    checked={isProcessed && isImageValid}
                    onClick={() =>
                      !isImageValid &&
                      (setIsImageValid(true),
                      setIsProcessed(true),
                      (photo['state'] = 'valid'),
                      (photo['processed'] = true),
                      setSelectedAnswer(photo))
                    }
                  >
                    Valid
                  </RadioButton>
                </RadioButtonContainerLeft>
                <RadioButtonContainerRight>
                  <RadioButton
                    checked={isProcessed && !isImageValid}
                    onClick={() =>
                      (isImageValid || !isProcessed) &&
                      (setIsImageValid(false),
                      setIsProcessed(true),
                      (photo['state'] = 'invalid'),
                      (photo['processed'] = true),
                      setSelectedAnswer(photo))
                    }
                  >
                    Invalid
                  </RadioButton>
                </RadioButtonContainerRight>
                <ClearFix />
              </Row>
            </RowStarter>
            {isProcessed && !isImageValid ? (
              <RowStarter>
                <Row>
                  <Form.Item>
                    <InputArea
                      name='comment'
                      value={invalidComment}
                      onChange={event => {
                        setInvalidComment(event.target.value)
                        photo['comment'] = event.target.value
                        setSelectedAnswer(photo)
                        setCharactersRemaining(
                          characterLimit - event.target.value.length
                        )
                      }}
                      label='Comment'
                      size='default'
                      maxLength={`200`}
                    />
                    <InfoBox>Characters Left: {charactersRemaining}</InfoBox>
                  </Form.Item>
                </Row>
              </RowStarter>
            ) : null}
          </React.Fragment>
        )
      }}
    />
  )
}

export default withTranslation()(PhotoValidationForm)
