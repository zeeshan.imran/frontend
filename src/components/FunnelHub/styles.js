import styled from 'styled-components'
import colors from '../../utils/Colors'
import Text from '../Text'
import { Modal as AntModal } from 'antd'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 3rem 3.5rem;
  margin-bottom: 2.5rem;
`

export const FieldText = styled(Text)`
  display: block;
`

export const Section = styled.div`
  padding: 3rem 3.5rem;
  width: 100%;
`

export const PaginationContainer = styled.div`
  text-align: right;
  margin-top: 5rem;
`
export const InfoBox = styled.div`
  font-size: 1rem;
`

export const ClearFix = styled.div`
  clear: both;
`

export const Gallery = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
`
export const PhotoSection = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  margin: 10px;
  height: 150px;
  border: 1px solid #eee;
  justify-content: center;
  cursor: pointer;
  :hover {
    box-shadow: 5px 10px #888888;
  }
`
export const Image = styled.img`
  width: 150px;
  height: 150px;
`
export const FullImage = styled.img`
  width: 100%;
  max-width: 600px;
  max-height: 800px;
`
export const IconContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  background: #fff;
  svg {
    fill: ${({ state }) =>
      state
        ? state === 'valid'
          ? colors.SOFT_GREEN
          : colors.RED
        : colors.BLACK};
  }
`
export const RowStarter = styled.div`
  margin-top: 3rem;
`

export const RadioButtonContainerLeft = styled.div`
  float: left;
`

export const RadioButtonContainerRight = styled.div`
  float: right;
`
export const StyledModal = styled(AntModal)`
  .ant-modal-body {
    padding: 5.5rem 4rem 3.5rem 4rem;
  }
`
