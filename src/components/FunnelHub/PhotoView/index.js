import React from 'react'
import { PhotoSection, Image, IconContainer } from '../styles'
import { withTranslation } from 'react-i18next'
import IconButton from '../../IconButton'

const PhotoView = ({ photo, setIsModalVisible, setSelectedAnswer, t }) => {
  const {value, state} = photo
  
  let iconType = 'exclamation-circle'
  let toolTip = t('tooltips.notProcessedSurveyPhotos')
  
  if(state === 'valid'){
    iconType = 'check-circle'
    toolTip = t('tooltips.validSurveyPhotos')
  }
    
  
  if(state === 'invalid'){
    iconType = 'close-circle'
    toolTip = t('tooltips.invalidSurveyPhotos')
  }
    

  return (
    <PhotoSection onClick={() => {
      setSelectedAnswer(photo)
      setIsModalVisible(true)
    }}>
      <IconContainer state={state}>
        <IconButton
          tooltip={toolTip}
          type={iconType}
          size='2.2rem'
        />
      </IconContainer>
      <Image src={value[0]} />
    </PhotoSection>
  )
}
  

export default withTranslation()(PhotoView)
