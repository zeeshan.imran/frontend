import React, { useState, useCallback, useRef } from 'react'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'
import { Button } from 'antd'
import { useTranslation } from 'react-i18next'

const GeneratePdf = ({ surveyId, jobGroupId, disabled }) => {
  const { t } = useTranslation()
  const [loading, setLoading] = useState(false)
  const getDownloadLink = useRef(useMutation(GET_DOWNLOAD_LINK))
  const getPdf = useCallback(async () => {
    try {
      setLoading(true)
      const { data } = await getDownloadLink.current({
        variables: {
          surveyId,
          jobGroupId
        }
      })
      setLoading(false)

      const { downloadFile } = data

      const url = `${process.env.REACT_APP_BACKEND_API_URL}/pdf/${downloadFile}`
      window.open(url, '_self')
    } catch (error) {
      setLoading(false)
    }
  }, [surveyId, jobGroupId])
  return (
    <Button
      type={'primary'}
      onClick={getPdf}
      loading={loading}
      disabled={disabled}
    >
      {t('containers.surveyStats.generateReport.text')}
    </Button>
  )
}
const GET_DOWNLOAD_LINK = gql`
  mutation getDownloadLink($surveyId: ID!, $jobGroupId: ID!) {
    downloadFile: getDownloadLink(surveyId: $surveyId, jobGroupId: $jobGroupId, templateName: operator)
  }
`

export default GeneratePdf
