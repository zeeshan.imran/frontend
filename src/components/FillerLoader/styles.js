import styled from 'styled-components'
import { components } from '../../utils/Metrics'

export const Filler = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: ${({ fullScreen }) =>
    fullScreen ? `calc(100vh - ${components.NAVBAR_HEIGHT}rem)` : '1.5rem'};
`
