import React from 'react'
import { mount } from 'enzyme'
import FillerLoader from '.'
import { LinkStyledText } from './styles'

describe('FillerLoader', () => {
  let testRender
    let loading
    let fullScreen
    let children


  beforeEach(() => {
    loading = true
    fullScreen = true
    children = '<container>Link here</container>'
    
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render FillerLoader', async () => {
    testRender = mount(
      <FillerLoader
        loading={loading}
        fullScreen={fullScreen}
        children={children}

      />
    )
    expect(testRender.find(FillerLoader)).toHaveLength(1) 
  })
})
