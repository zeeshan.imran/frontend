import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.4rem 3.2rem 3.2rem;
`

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6.4rem;
`

export const SearchBarContainer = styled.div`
  width: 27rem;
`
