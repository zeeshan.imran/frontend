import React from 'react'
import { Tooltip, Icon } from 'antd'
import { useTranslation } from 'react-i18next'

const InputTooltip = ({ titleKey, icon, color }) => {
  const { t } = useTranslation()
  return (
    <Tooltip
      title={<span dangerouslySetInnerHTML={{ __html: t(titleKey) }} />}
      placement='bottomRight'
    >
      <Icon type={icon} theme='twoTone' twoToneColor={color} />
    </Tooltip>
  )
}

export default InputTooltip
