import React from 'react'
import { mount } from 'enzyme'
import { Modal, Input, Button } from 'antd'
import OrganizationForm from './index'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

jest.mock('../../../utils/internationalization/i18n', () => ({
  __esModule: true,
  default: { t: text => text }
}))

describe('OrganizationForm', () => {
  let testRender
  let handleOrganizationIdChange
  let newOrganizationData
  let handleSubmit

  beforeEach(() => {
    handleOrganizationIdChange = jest.fn()
    handleSubmit = jest.fn()
    newOrganizationData = { id: 1, name: 'test', modifiedBy: null }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render empty when no editOrganizationId is passed', async () => {
    testRender = mount(
      <OrganizationForm
        onOrganizationFormClose={handleOrganizationIdChange}
        newOrganizationData={newOrganizationData}
        onSubmit={handleSubmit}
      />
    )

    expect(testRender.html()).toBeNull()
  })

  test('should render OrganizationForm', async () => {
    testRender = mount(
      <OrganizationForm
        editOrganizationId={'1'}
        onOrganizationFormClose={handleOrganizationIdChange}
        newOrganizationData={newOrganizationData}
        onSubmit={handleSubmit}
      />
    )

    expect(testRender.find(Modal)).toHaveLength(1)

    testRender
      .find(Modal)
      .first()
      .prop('onCancel')()
    expect(handleOrganizationIdChange).toHaveBeenCalledWith(false)
  })

  test('should render OrganizationForm', async () => {
    testRender = mount(
      <OrganizationForm
        editOrganizationId
        onOrganizationFormClose={handleOrganizationIdChange}
        onSubmit={handleSubmit}
      />
    )

    expect(testRender).toMatchSnapshot()
    testRender
      .find(Input)
      .find({ name: 'uniqueName' })
      .first()
      .prop('onChange')({ target: { value: 'name' } })

    testRender
      .find(Input)
      .find({ name: 'name' })
      .first()
      .prop('onChange')({ target: { value: 'name' } })

    testRender
      .find(Button)
      .find({ children: 'OK' })
      .first()
      .prop('onClick')()

    expect(handleSubmit).toHaveBeenCalledWith({
      name: 'name',
      uniqueName: 'name'
    })
  })
})
