import React from 'react'
import Table from './Table'
import SearchBar from '../SearchBar'
import Button from '../Button'
import { Container, HeaderContainer, SearchBarContainer } from './styles'
import OrganizationForm from './OrganizationForm'
import { withTranslation } from 'react-i18next'

const UsersListComponent = ({
  editOrganizationId,
  onEditOrganizationIdChange,
  selected,
  newOrganizationData,
  setnewOrganizationData,
  setOrganizationData,
  setModalType,
  modalType,
  onOrganizationFormSubmit,
  handleSearch,
  searchBy,
  t,
  ...tableProps
}) => {
  return (
    <Container>
      <HeaderContainer>
        <Button
          onClick={() => {
            setModalType('add')
            setOrganizationData({})
            onEditOrganizationIdChange(true)
          }}
        >
          {t('components.organizationsList.add')}
        </Button>
        <SearchBarContainer>
          <SearchBar
            placeholder={t('components.organizationsList.search')}
            withIcon
            handleChange={handleSearch}
            value={searchBy}
          />
        </SearchBarContainer>
      </HeaderContainer>
      <Table
        {...tableProps}
        setModalType={setModalType}
        onEditOrganization={org => onEditOrganizationIdChange(org.id)}
      />
      {editOrganizationId && (
        <OrganizationForm
          id='organization form'
          editOrganizationId={editOrganizationId}
          onOrganizationFormClose={() => onEditOrganizationIdChange(false)}
          newOrganizationData={newOrganizationData}
          onSubmit={onOrganizationFormSubmit}
          modalType={modalType}
        />
      )}
    </Container>
  )
}

export default withTranslation()(UsersListComponent)
