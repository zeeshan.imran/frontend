import styled from 'styled-components'
import { DEFAULT_COMPONENTS_MARGIN } from '../../../utils/Metrics'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const Picture = styled.img`
  height: 3.6rem;
  width: 3.6rem;
  border-radius: 50%;
  margin-right: ${DEFAULT_COMPONENTS_MARGIN}rem;
`

export const ColumnContentAligner = styled.div`
  display: inline-flex;
  align-items: center;
`

export const Action = styled.span`
  cursor: pointer;
  color: ${colors.PRODUCT_TABLE_COLOR};
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1;
`

export const Separator = styled.div`
  display: inline-block;
  height: 1.4rem;
  width: 0.1rem;
  margin: 0 0.8rem;
  background-color: #e9e9e9;
`
