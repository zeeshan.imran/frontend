import React, { useState } from 'react'
import { mount } from 'enzyme'
import TasterRoute from '.'
import { Router, Redirect } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import PrivateRoute from '../PrivateRoute'

describe('TasterRoute', () => {
  let testRender
  let history 

  beforeAll(() => {
    history=createBrowserHistory()
    const localStorageMock = (function () {
      let store = {}
      return {
        getItem: function (key) {
          return store[key] || null
        },
        setItem: function (key, value) {
          store[key] = value.toString()
        },
        removeItem: function (key) {
          delete store[key]
        },
        clear: function () {
          store = {}
        }
      }
    })()
    Object.defineProperty(window, 'localStorage', {
      value: localStorageMock
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render TasterRoute', async () => {
    window.localStorage.setItem(
      'flavorwiki-user-details',
      JSON.stringify({ initialized: true })
    )
    testRender = mount(
      <Router history={history}>
        <TasterRoute component={() => <div>content</div>} />
      </Router>
    )
    expect(testRender.find(PrivateRoute)).toHaveLength(1)
  })

  test('redirect to /taster/complete-profile', () => {
    window.localStorage.setItem(
      'flavorwiki-user-details',
      JSON.stringify({ initialized: false })
    )
    testRender = mount(
      <Router history={history}>
        <TasterRoute component={() => <div>content</div>} />
      </Router>
    )
    Redirect
    expect(testRender.find(Redirect)).toHaveLength(1)
  })
})
