import React from 'react'
import PrivateRoute from '../PrivateRoute'

const TasterRoute = props => {
  return <PrivateRoute {...props} authenticateTaster />
}

export default TasterRoute
