import React, { useState } from 'react'
import { Button, Icon, Checkbox, Select, Typography } from 'antd'
import ZoomButton from './ZoomButton'
import { Sider, ConfigItem, Actions, Label, TitleContainer } from './styles'
import { useTranslation } from 'react-i18next'

const Option = Select.Option

const SideBar = ({
  templateName,
  zoom,
  onZoomChange,
  paperSize,
  onPaperSizeChange,
  showLayoutBorder,
  onShowLayoutBorderChange,
  onTemplateSave,
  onClose,
  onDownload
}) => {
  const { t } = useTranslation()
  const [downloading, setDownloading] = useState(false)
  const [saving, setSaving] = useState(false)

  const handleDownload = async () => {
    setDownloading(true)
    await onDownload()
    setDownloading(false)
  }

  const handleTemplateSave = async () => {
    setSaving(true)
    await onTemplateSave()
    setSaving(false)
  }

  return (
    <Sider theme='light' width={300}>
      <TitleContainer>
        <Typography.Title level={4}>
          {t(`pdfLayoutEditor.title.${templateName}`)}
        </Typography.Title>
        <Button size='small' type='default' shape='circle' onClick={onClose}>
          <Icon type='close' />
        </Button>
      </TitleContainer>
      <ConfigItem>
        <Label>{t('pdfLayoutEditor.zoom')}</Label>
        <ZoomButton {...{ zoom, onZoomChange }} />
      </ConfigItem>
      <ConfigItem>
        <Label>{t('pdfLayoutEditor.paperSize')}</Label>
        <Select
          value={paperSize}
          style={{ width: 120 }}
          onChange={onPaperSizeChange}
        >
          <Option value='A4'>A4</Option>
          <Option value='Letter'>Letter</Option>
        </Select>
      </ConfigItem>
      <ConfigItem>
        <Checkbox
          checked={showLayoutBorder}
          onChange={e => onShowLayoutBorderChange(e.target.checked)}
        >
          {t('pdfLayoutEditor.showLayoutBorder')}
        </Checkbox>
      </ConfigItem>
      <Actions>
        <Button type='primary' loading={saving} onClick={handleTemplateSave}>
          {t('pdfLayoutEditor.saveAsTemplate')}
        </Button>
        <Button
          type='primary'
          icon='download'
          loading={downloading}
          onClick={handleDownload}
        />
      </Actions>
    </Sider>
  )
}

export default SideBar
