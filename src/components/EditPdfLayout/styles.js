import styled from 'styled-components'
import { Layout, Typography } from 'antd'

export const EditorContainer = styled.div``

export const PageInfo = styled.div`
  position: absolute;
  top: -35px;
  left: -200px;
  width: 200px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding: 0.5rem;

  > .ant-typography {
    margin-right: 0.5rem;
  }

  > button {
    margin-right: 0.15rem;
    transform: scale(0.8);
  }

  > span > button {
    margin-right: 0.15rem;
    transform: scale(0.9);
  }
`

export const Pages = styled.div`
  width: ${({ width }) => width}px;
  height: ${({ height }) => height}px;
  background-size: auto;
  background-repeat: repeat-x;
  display: inline-block;

  .react-grid-item.cssTransforms {
    transition-property: none;
  }

  .animated .react-grid-item.cssTransforms {
    transition-property: transform;
  }

  .react-grid-item.react-draggable.react-resizable {
    ${({ showLayoutBorder }) => showLayoutBorder && `border: 1px dashed #d00;`}
    * {
      transition-property: none !important;
    }
  }
`

export const Content = styled(Layout.Content)`
  height: 100vh;
  background: #ddd;
  padding: 5rem 2rem;
  overflow-x: auto !important;
  overflow-y: auto !important;
`

export const ZoomItem = styled.div`
  width: 80px;
  display: flex;
  align-items: center;
  span {
    flex: 1;
  }
`

export const Logo = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 18.5rem;
  height: 3.4rem;
  margin-top: 20px;
`

export const ChartLayoutItem = styled.div`
  transform: scale(${({ scale }) => scale || 0.1});
  transform-origin: top left;

  .ant-card {
    ${({ cardWidth }) => `width: ${cardWidth}px !important;`}
    pointer-events: none;
    .ant-card-extra {
      display: none !important;
    }
  }

  .ant-card .ant-card-body {
    padding-left: ${({ cardWidth, chartWidth }) =>
      24 + (cardWidth - chartWidth) / 2}px;
    padding-right: ${({ cardWidth, chartWidth }) =>
      24 + (cardWidth - chartWidth) / 2}px;

    .radar {
      transform: translate(0, 0) !important;
    }
  }
`

/* SideBar */
export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  h4 {
    flex: 1;
    font-size: 16px;
    margin: 0;
  }
`

export const Actions = styled.div`
  margin: 1rem 0;
  > * {
    margin-right: 0.5rem;
  }
`

export const Label = styled(Typography.Text)`
  display: block;
`

export const Sider = styled(Layout.Sider)`
  padding: 2rem;
  border-left: 1px solid #ddd;
`

export const ConfigItem = styled.div`
  margin-top: 1rem;
  span {
  }
`

/* LoadingState */
export const Loading = styled.div`
  background: #eee;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  right: 300px;
  z-index: 99999;
  > div {
    text-align: center;
    .ant-typography {
      color: #666;
    }
    .ant-progress {
      margin-bottom: 0.5rem;
    }
    .ant-spin {
      display: block;
      .anticon,
      .anticon svg {
        width: 60px;
        height: 60px;
      }
    }
  }
`
