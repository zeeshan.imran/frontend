import React from 'react'
import { Spin, Typography, Icon } from 'antd'
import { Loading } from './styles'
import { useTranslation } from 'react-i18next'

const LoadingState = () => {
  const { t } = useTranslation()
  return (
    <Loading>
      <div>
        <Spin indicator={<Icon type='loading' size='large' spin />} />
        <Typography.Title level={4}>
          {t('pdfLayoutEditor.loadingData')}
        </Typography.Title>
      </div>
    </Loading>
  )
}

export default LoadingState
