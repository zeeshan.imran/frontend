import React, { useCallback } from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import ReactGridLayout from 'react-grid-layout'
import { Layout } from 'antd'
import { EditorContainer, Pages, Content } from './styles'
import SideBar from './SideBar'
import LoadingState from './LoadingState'
import RenderingState from './RenderingState'
import { range } from 'ramda'

const Paper = ({ layout: paperSize, colWidth, zoom = 1 }) => {
  const { cols, colsBetweenPages, height } = paperSize
  const outerWidth = (cols + colsBetweenPages) * colWidth

  return (
    <svg
      width={outerWidth}
      height={zoom * height}
      xmlns='http://www.w3.org/2000/svg'
      fill='#ff0000'
    >
      <rect
        x={0}
        y={0}
        width={cols * colWidth}
        height={zoom * height}
        fill='#ffffff'
      />
    </svg>
  )
}

const getPageOfItem = (paperSize, item) => {
  return Math.floor(item.x / (paperSize.cols + paperSize.colsBetweenPages))
}

const EditPdfLayout = ({
  // state props
  loading,
  rendering,
  ready,
  //editor props
  templateName,
  paperSize,
  onPaperSizeChange,
  colsPerPage,
  colWidth,
  rowHeight,
  zoom,
  onZoom,
  showLayoutBorder,
  onShowLayoutBorderChange,
  onDownload,
  onTemplateSave,
  onClose,
  // grid props
  pages,
  pageItems,
  elementItems,
  gridLayout,
  onDeletePage,
  onAddPage,
  onLayoutResizeStop,
  onLayoutChange
}) => {
  const layoutsWithoutPages = gridLayout.filter(
    item => item.x % colsPerPage < paperSize.cols
  )

  const getLayoutData = useCallback(() => {
    const layoutData = range(0, pages).map(page => {
      return layoutsWithoutPages
        .filter(item => getPageOfItem(paperSize, item) === page)
        .map(item => {
          const id = item.i
          const gridItem = elementItems.find(it => it.i === id)
          const ref = gridItem && gridItem.getRef && gridItem.getRef()

          return {
            ...item,
            x: item.x % colsPerPage,
            align: gridItem.align,
            ...(ref ? ref.getChartSize() : {})
          }
        })
    })

    return layoutData
  }, [pages, colsPerPage, elementItems, paperSize, layoutsWithoutPages])

  const handleDownload = useCallback(async () => {
    await onDownload(getLayoutData())
  }, [getLayoutData, onDownload])

  const handleTemplateSave = useCallback(async () => {
    await onTemplateSave(getLayoutData())
  }, [getLayoutData, onTemplateSave])

  const handleClose = useCallback(async () => {
    await onClose(getLayoutData())
  }, [getLayoutData, onClose])

  if (loading) {
    return <LoadingState />
  }

  return (
    <Layout>
      {rendering && <RenderingState />}
      <Content>
        <EditorContainer>
          <Pages
            width={pages * colsPerPage * colWidth}
            height={zoom * paperSize.height}
            showLayoutBorder={showLayoutBorder}
            onShow
            style={{
              backgroundImage: `url('data:image/svg+xml;utf8,${encodeURIComponent(
                renderToStaticMarkup(
                  <Paper layout={paperSize} colWidth={colWidth} zoom={zoom} />
                )
              )}')`
            }}
          >
            {ready && (
              <ReactGridLayout
                layout={gridLayout}
                onLayoutChange={onLayoutChange}
                onResizeStop={(_layout, _oldItem, newItem) => {
                  onLayoutResizeStop(newItem)
                }}
                cols={pages * colsPerPage}
                width={pages * colsPerPage * colWidth}
                rowHeight={rowHeight}
                margin={[0, 0]}
                containerPadding={[0, 0]}
                verticalCompact={false}
                preventCollision
                useCSSTransforms={!rendering}
              >
                {pageItems.map(item => (
                  <div key={`${item.i}`}>
                    {item.content({
                      canDelete: !layoutsWithoutPages.some(
                        i => getPageOfItem(paperSize, i) === item.page
                      ),
                      onDeletePage,
                      onAddPage
                    })}
                  </div>
                ))}
                {elementItems.map(item => (
                  <div key={item.i} id={`pdf-${item.i}`}>
                    {item.content}
                  </div>
                ))}
              </ReactGridLayout>
            )}
          </Pages>
        </EditorContainer>
      </Content>
      <SideBar
        templateName={templateName}
        paperSize={paperSize.name}
        onPaperSizeChange={onPaperSizeChange}
        zoom={zoom}
        onZoomChange={onZoom}
        onDownload={handleDownload}
        onTemplateSave={handleTemplateSave}
        showLayoutBorder={showLayoutBorder}
        onShowLayoutBorderChange={onShowLayoutBorderChange}
        onClose={handleClose}
      />
    </Layout>
  )
}

export default EditPdfLayout
