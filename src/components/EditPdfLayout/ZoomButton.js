import React, { useMemo } from 'react'
import { Button, Dropdown, Menu, Icon } from 'antd'
import { ZoomItem } from './styles'

const ZoomButton = ({ zoom, onZoomChange }) => {
  const menu = useMemo(
    () => (
      <Menu>
        {[0.25, 0.5, 0.75, 1, 1.25, 1.5].map(z => (
          <Menu.Item key={z.toString()} onClick={() => onZoomChange(z)}>
            <ZoomItem>
              <span>{z * 100}%</span>
              {z === zoom && <Icon type='check' />}
            </ZoomItem>
          </Menu.Item>
        ))}
      </Menu>
    ),
    [zoom, onZoomChange]
  )

  return (
    <Dropdown overlay={menu}>
      <Button>{zoom * 100}%</Button>
    </Dropdown>
  )
}

export default ZoomButton
