import React from 'react'
import { Typography, Spin, Icon } from 'antd'
import { Loading } from './styles'
import { useTranslation } from 'react-i18next'

const RenderingState = () => {
  const { t } = useTranslation()
  return (
    <Loading>
      <div>
        <Spin indicator={<Icon type='loading' size='large' spin />} />
        <Typography.Title level={4}>
          {t('pdfLayoutEditor.rendering')}
        </Typography.Title>
      </div>
    </Loading>
  )
}

export default RenderingState
