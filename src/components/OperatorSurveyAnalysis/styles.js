import styled from 'styled-components'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'
import Text from '../../components/Text'
import { Steps } from 'antd'
import { Container as FieldContainer } from '../FieldLabel/styles'

export const Step = Steps.Step

export const StepsContainer = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 1rem;

  .ant-steps-item {
    cursor: pointer;
  }

  .ant-steps-item[disabled] {
    cursor: not-allowed;
    .ant-steps-item-icon {
      border-color: #999;
    }

    .ant-steps-item-title {
      color: #999;
    }

    .anticon {
      color: #999;
    }
  }
`

export const FooterContainer = styled.div`
  text-align: right;
  background-color: ${colors.WHITE};
  padding: 1.5rem 0 0 0;
`

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
  ${FieldContainer} {
    margin-bottom: 1rem;
  }
`

export const PageHeader = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 2.5rem;
  & > :first-child {
    flex-grow: 1;
  }
  & > * {
    margin-left: 1.5rem;
  }
`
export const Title = styled(Text)`
  font-size: 2rem;
  font-family: ${family.LatoBold};
  color: rgba(0, 0, 0, 0.85);
`

export const RowStarter = styled.div`
  margin-top: 3rem;
`
export const RowContent = styled.div`
`
export const SwitchArea = styled.div`
  margin-left: 1.5rem;
  display: inline-block;
`
export const RowHeader = styled(Text)`
  display: block;
  color: #aaa;
`
export const RowLabel = styled(Text)`
  margin-left: 0.5rem;
`