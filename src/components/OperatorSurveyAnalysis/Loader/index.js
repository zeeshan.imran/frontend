import React from 'react'
import { Row, Col, Spin } from 'antd'
import { Container } from '../styles'

const Loader = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Spin />
        </Col>
      </Row>
    </Container>
  )
}

export default Loader
