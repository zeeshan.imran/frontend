import React, { useState, useCallback } from 'react'
import Button from '../../Button'
import { Container, FooterContainer } from '../styles'
import StatsProductSection from '../../StatsProductSection'
import { STATS_FILTER_ACCEPTED_TYPES } from '../../../utils/Constants'
import QuestionsFilter from './QuestionFilter'
import { useTranslation } from 'react-i18next'

const Filter = ({
  filterProducts,
  filterQuestions: initFilterQuestions,
  products,
  questions,
  onBack,
  onFilterSave
}) => {
  const { t } = useTranslation()
  const [selectedProducts, setSelectedProducts] = useState(filterProducts)
  const [filterQuestions, setFilterQuestions] = useState(initFilterQuestions)
  const handleSaveFilter = useCallback(async () => {
    onFilterSave(selectedProducts, filterQuestions)
  }, [onFilterSave, selectedProducts, filterQuestions])

  const questionsToFilter = questions.filter(question => {
    return STATS_FILTER_ACCEPTED_TYPES.indexOf(question.type) >= 0
  })

  return (
    <React.Fragment>
      <StatsProductSection
        products={products}
        selectedProducts={selectedProducts}
        toggleProductSeletion={productId => {
          const selected = selectedProducts.find(p => p === productId)
          let nextSelectedProducts

          if (selected) {
            nextSelectedProducts = selectedProducts.filter(p => p !== productId)
          } else {
            nextSelectedProducts = [...selectedProducts, productId]
          }

          setSelectedProducts(nextSelectedProducts)
        }}
      />

      <Container>
        <QuestionsFilter
          questions={questionsToFilter}
          filterQuestions={filterQuestions}
          onFilterQuestionsChange={setFilterQuestions}
        />
        <FooterContainer>
          <Button type='secondary' onClick={onBack} size='default'>
            {t('surveyAnalysis.filter.back')}
          </Button>{' '}
          <Button onClick={handleSaveFilter} size='default'>
            {t('surveyAnalysis.filter.next')}
          </Button>
        </FooterContainer>
      </Container>
    </React.Fragment>
  )
}

export default Filter
