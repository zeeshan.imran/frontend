import React, { useMemo } from 'react'
import {
  RowStarter,
  RowHeader,
  RowContent,
  RowLabel,
  SwitchArea
} from '../styles'
import { Switch, Form } from 'antd'
import { validationSchema } from './validationSchema'

const QuestionsFilter = ({
  questions,
  filterQuestions,
  onFilterQuestionsChange
}) => {
  const errors = useMemo(() => {
    try {
      validationSchema.validateSync(
        { filterQuestions },
        {
          abortEarly: false,
          context: { questions }
        }
      )
      return {}
    } catch (ex) {
      return Object.assign(
        ...ex.inner.map(({ path, message }) => ({ [path]: message }))
      )
    }
  }, [filterQuestions])

  return questions.map((question, index) => {
    const checkedValues = filterQuestions[question.id] || []
    const error = errors[`filterQuestions.${question.id}`]
    return (
      <RowStarter key={index}>
        <RowHeader>{question.chartTitle}</RowHeader>
        <Form.Item validateStatus={error ? 'error' : 'success'} help={error}>
          <RowContent>
            {question.options.map((option, optionIndex) => {
              const checked = checkedValues.includes(option.value)
              return (
                <SwitchArea key={optionIndex}>
                  <Switch
                    checked={checked}
                    size={`small`}
                    onChange={checked => {
                      if (checked) {
                        onFilterQuestionsChange({
                          ...filterQuestions,
                          [question.id]: [...checkedValues, option.value]
                        })
                      } else {
                        onFilterQuestionsChange({
                          ...filterQuestions,
                          [question.id]: checkedValues.filter(
                            value => value !== option.value
                          )
                        })
                      }
                    }}
                  />
                  <RowLabel>{option.label}</RowLabel>
                </SwitchArea>
              )
            })}
          </RowContent>
        </Form.Item>
      </RowStarter>
    )
  })
}

export default QuestionsFilter
