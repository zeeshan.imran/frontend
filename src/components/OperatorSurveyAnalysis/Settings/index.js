import React from 'react'
import { Row, Col, Form, Spin } from 'antd'
import { Formik } from 'formik'
import Button from '../../Button'
import Select from '../../Select'
import { Container, FooterContainer } from '../styles'
import {
  getOptionName,
  getOptionValue
} from '../../../utils/getters/OptionGetters'
import { validationSchema, shouldRequireReference } from './validationSchema'
import { useTranslation } from 'react-i18next'

const ANALYSIS_TYPES = [
  { label: 'PCA', value: 'pca', pairsOnly: true },
  { label: 'PLS', value: 'pls', pairsOnly: true },
  { label: 'ANOVA', value: 'anova' },
  { label: 'FISHER', value: 'fisher' },
  { label: 'PENALTY', value: 'penalty' },
  { label: 'AHC', value: 'ahc', pairsOnly: true },
  { label: 'TUKEY', value: 'tukey' }
]

const Settings = ({ questions, loading, initSettings, onSettingsSave }) => {
  const { t } = useTranslation()
  return (
    <Formik
      initialValues={initSettings}
      validationSchema={validationSchema}
      onSubmit={onSettingsSave}
      render={({ values, touched, errors, handleSubmit, setFieldValue }) => {
        const requireReference = shouldRequireReference(values.analysisTypes)
        const getItemProps = name => {
          const error = touched[name] && errors[name]
          return {
            help: error,
            validateStatus: error ? 'error' : 'success'
          }
        }
        const selectedQuestion = questions.find(
          question => question.id === values.question
        )
        const analysisTypes = ANALYSIS_TYPES.filter(
          type =>
            !type.pairsOnly ||
            (selectedQuestion && selectedQuestion.type === 'paired-questions')
        )

        return (
          <Container>
            <Spin spinning={loading}>
              <Row gutter={16}>
                <Col span={9}>
                  <Form.Item {...getItemProps('question')}>
                    <Select
                      size='default'
                      label={t('surveyAnalysis.settings.question')}
                      options={questions.map(q => ({
                        value: q.id,
                        label: q.prompt
                      }))}
                      getOptionName={getOptionName}
                      getOptionValue={getOptionValue}
                      value={values.question}
                      onChange={value => {
                        setFieldValue('question', value)
                        setFieldValue('referenceQuestion', null)
                        setFieldValue('analysisTypes', [])
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col span={15}>
                  <Form.Item {...getItemProps('analysisTypes')}>
                    <Select
                      size='default'
                      label={t('surveyAnalysis.settings.analysisTypes')}
                      mode='multiple'
                      allowClear
                      options={analysisTypes}
                      getOptionName={getOptionName}
                      getOptionValue={getOptionValue}
                      value={values.analysisTypes}
                      onChange={value => setFieldValue('analysisTypes', value)}
                    />
                  </Form.Item>
                  {requireReference && (
                    <Form.Item {...getItemProps('referenceQuestion')}>
                      <Select
                        size='default'
                        label={t('surveyAnalysis.settings.referenceQuestion')}
                        options={questions
                          .filter(q => q.id !== values.question)
                          .map(q => ({
                            value: q.id,
                            label: q.prompt
                          }))}
                        getOptionName={getOptionName}
                        getOptionValue={getOptionValue}
                        value={values.referenceQuestion}
                        onChange={value =>
                          setFieldValue('referenceQuestion', value)
                        }
                      />
                    </Form.Item>
                  )}
                </Col>
              </Row>
              <FooterContainer>
                <Button htmlType='submit' size='default' onClick={handleSubmit}>
                  {t('surveyAnalysis.settings.next')}
                </Button>
              </FooterContainer>
            </Spin>
          </Container>
        )
      }}
    />
  )
}

export default Settings
