import React from 'react'
import { mount } from 'enzyme'
import Responsive from 'react-responsive'
import { Desktop, NonDesktop, Tablet, Mobile, Default } from '.'

describe('Responsive', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    // if (testRender) {
    testRender.unmount()
    // }
  })

  test('should render Responsive Desktop', () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 1024,
      height: 800
    })

    testRender = mount(<Desktop>{match => match && 'desktop'}</Desktop>)

    expect(testRender.text()).toBe('desktop')
  })

  test('should render Responsive NonDesktop', () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 900,
      height: 800
    })

    testRender = mount(<NonDesktop>{match => match && 'non desktop'}</NonDesktop>)

    expect(testRender.text()).toBe('non desktop')
  })

  test('should render Responsive Tablet', () => {

    window.matchMedia.setConfig({
        type: 'screen',
        width: 768,
        height: 1024
    })

    testRender = mount(<Tablet>{match => match && 'Tablet'}</Tablet>)
    expect(testRender.text()).toBe('Tablet')
  })

  test('should render Responsive Mobile', () => {
    window.matchMedia.setConfig({
        type: 'screen',
        width: 480,
        height: 640
    })

    testRender = mount(<Mobile>{match => match && 'Mobile'}</Mobile>)
    expect(testRender.text()).toBe('Mobile')
  })

  test('should render Responsive Default', () => {
    window.matchMedia.setConfig({
        type: 'screen',
        width: 1900,
        height: 1080
    })
    testRender = mount(<Default>{match => match && 'Default'}</Default>)
      expect(testRender.text()).toBe('Default')
  })
})
