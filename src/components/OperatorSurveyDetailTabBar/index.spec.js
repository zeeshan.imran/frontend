import React from 'react'
import { mount } from 'enzyme'
import OperatorSurveyDetailTabBar from '.'
import gql from 'graphql-tag'
import TabBar from '../TabBar'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import { act } from 'react-dom/test-utils'
import { createBrowserHistory } from 'history'

jest.mock('../../utils/userAuthentication')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        isScreenerOnly
        showGeneratePdf
      }
    }
  }
`

describe('OperatorSurveyDetailTabBar', () => {
  let testRender
  let match
  let actions
  let location
  let onClick
  let client

  beforeEach(() => {
    match = false
    actions = []
    location = { search: '' }
    onClick = jest.fn()
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorSurveyDetailTabBar without screener', () => {
    const history = createBrowserHistory()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          isScreenerOnly: false,
          showGeneratePdf: false
        }
      }
    })
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorSurveyDetailTabBar
            history={history}
            match={match}
            actions={actions}
            location={location}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(OperatorSurveyDetailTabBar)).toHaveLength(1)

    act(() => {
      testRender.find(TabBar).prop('onTabSelection')('Basic Info')
    })
    expect(onClick).not.toHaveBeenCalled()
  })

  test('should render OperatorSurveyDetailTabBar with screener', () => {
    const history = createBrowserHistory()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          isScreenerOnly: true
        }
      }
    })
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorSurveyDetailTabBar
            history={history}
            match={match}
            actions={actions}
            location={location}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(OperatorSurveyDetailTabBar)).toHaveLength(1)

    act(() => {
      testRender.find(TabBar).prop('onTabSelection')('Basic Info')
    })
    expect(onClick).not.toHaveBeenCalled()
  })
})
