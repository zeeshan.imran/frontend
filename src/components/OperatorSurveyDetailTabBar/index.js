import React from 'react'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import { path } from 'ramda'
import { useQuery } from 'react-apollo-hooks'
import { useTranslation } from 'react-i18next'
import TabBar from '../TabBar'

const isScreenerOnly = path(['surveyCreation', 'basics', 'isScreenerOnly'])

const OperatorSurveyDetailTabBar = ({ history, match, actions, location }) => {
  const {
    params: { step }
  } = match
  const { t } = useTranslation()
  const { data } = useQuery(SURVEY_CREATION)
  const screenerOnly = isScreenerOnly(data) || false
  const { REACT_APP_THEME } = process.env

  const tabPayments = () => ({
    title: t('components.operatorSurveyDetailBar.payments'),
    tooltip: t('tooltips.operatorSurveyDetailBar.payments'),
    key: 'financial?section=payments'
  })

  const tabTastingNotes = () => ({
    title: t('components.operatorSurveyDetailBar.tastingNotes'),
    tooltip: t('tooltips.operatorSurveyDetailBar.tastingNotes'),
    key: 'tasting-notes'
  })

  let tabs = [
    {
      title: t('components.operatorSurveyDetailBar.basics'),
      tooltip: t('tooltips.operatorSurveyDetailBar.basics'),
      key: 'basics'
    },
    {
      title: t('components.operatorSurveyDetailBar.settings'),
      tooltip: t('tooltips.operatorSurveyDetailBar.settings'),
      key: 'settings'
    },
    ...(REACT_APP_THEME === 'chrHansen' ? [tabTastingNotes()] : []),
    ...(REACT_APP_THEME !== 'chrHansen' ? [tabPayments()] : []),
    {
      title: t('components.operatorSurveyDetailBar.products'),
      tooltip: t('tooltips.operatorSurveyDetailBar.products'),
      key: 'products'
    },
    {
      title: t('components.operatorSurveyDetailBar.screening'),
      tooltip: t('tooltips.operatorSurveyDetailBar.screening'),
      key: 'questions?section=screening'
    },
    {
      title: t('components.operatorSurveyDetailBar.before'),
      tooltip: t('tooltips.operatorSurveyDetailBar.before'),
      key: 'questions?section=begin'
    },
    {
      title: t('components.operatorSurveyDetailBar.tasting'),
      tooltip: t('tooltips.operatorSurveyDetailBar.tasting'),
      key: 'questions?section=middle'
    },
    {
      title:
        REACT_APP_THEME === 'chrHansen'
          ? t('components.operatorSurveyDetailBarChrHansen.after')
          : t('components.operatorSurveyDetailBar.after'),
      tooltip: t('tooltips.operatorSurveyDetailBar.after'),
      key: 'questions?section=end'
    }
  ]

  if (screenerOnly) {
    tabs = [
      {
        title: t('components.operatorSurveyDetailBar.basics'),
        tooltip: t('tooltips.operatorSurveyDetailBar.basics'),
        key: 'basics'
      },
      {
        title: t('components.operatorSurveyDetailBar.settings'),
        tooltip: t('tooltips.operatorSurveyDetailBar.settings'),
        key: 'settings'
      },
      ...(REACT_APP_THEME === 'chrHansen' ? [tabTastingNotes()] : []),
      ...(REACT_APP_THEME !== 'chrHansen' ? [tabPayments()] : []),
      {
        title: t('components.operatorSurveyDetailBar.screening'),
        tooltip: t('tooltips.operatorSurveyDetailBar.screening'),
        key: 'questions?section=screening'
      },
      {
        title: t('components.operatorSurveyDetailBar.linkedSurveys'),
        tooltip: t('tooltips.operatorSurveyDetailBar.linkedSurveys'),
        key: 'linked-surveys'
      }
    ]
  }

  const { search } = location

  const activeTab = [step, search].join('')

  return (
    <TabBar
      tabs={tabs}
      activeTab={activeTab}
      onTabSelection={tab => history.push(tab)}
      action={actions}
    />
  )
}

export default withRouter(OperatorSurveyDetailTabBar)

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        isScreenerOnly
        authorizationType
      }
    }
  }
`
