import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

export const SelectContainer = styled.div`
  flex: 1;
  padding-right: 0.4rem;
`

export const InputContainer = styled.div`
  flex: 2;
  padding-left: 0.4rem;
`
