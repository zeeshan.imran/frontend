import React from 'react'
import { mount } from 'enzyme'
import PhoneNumberInput from '.'
import Input from '../Input'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PhoneNumberInput', () => {
    let testRender
    let placeholder
    let code
    let codeName
    let onChangeCode
    let number
    let numberName
    let onChangeNumber
    let onBlurNumber
    let rest

    beforeAll(() => {
        placeholder = 'Your phone number'
        code = '+1'
        codeName = 'code'
        onChangeCode = jest.fn()
        number = '1'
        numberName = 'number'
        onChangeNumber = 1
        onBlurNumber = 1
        rest = {}
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render PhoneNumberInput', async () => {
        testRender = mount(
            <PhoneNumberInput
                placeholder={placeholder}
                code={code}
                codeName={codeName}
                onChangeCode={onChangeCode}
                number={number}
                numberName={numberName}
                onChangeNumber={onChangeNumber}
                onBlurNumber={onBlurNumber}
                rest={rest}
            />
        )

        expect(testRender.find(PhoneNumberInput)).toHaveLength(1)

        testRender.find(Input).simulate('change', { target: { value: '123456' } })

    })
})
