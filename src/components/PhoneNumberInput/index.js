import React from 'react'
import { COUNTRY_PHONE_CODES } from '../../utils/Constants'
import Select from '../Select'
import Input from '../Input'

import { Container, SelectContainer, InputContainer } from './styles'
import { useTranslation } from 'react-i18next';

const getDialFromUniqueValue = uniqueValue =>
  uniqueValue && uniqueValue.split('/')[1]

const PhoneNumberInput = ({
  placeholder,
  code,
  codeName = 'code',
  onChangeCode,
  number,
  numberName = 'number',
  onChangeNumber,
  onBlurNumber,
  ...rest
}) => {
  const { t } = useTranslation();
  return (
    <Container>
      <SelectContainer>
        <Select
          showSearch
          name={codeName}
          value={code}
          placeholder={t('placeholders.country')}
          optionFilterProp='children'
          options={COUNTRY_PHONE_CODES}
          onChange={uniqueValue => {
            onChangeCode(getDialFromUniqueValue(uniqueValue))
          }}
          getOptionName={option => option.name}
          getOptionValue={option => `${option.code}/${option.dial}`}
          autocomplete='off'
          {...rest}
        />
      </SelectContainer>
      <InputContainer>
        <Input
          name={numberName}
          value={number}
          onChange={onChangeNumber}
          onBlur={onBlurNumber}
          placeholder={placeholder}
          {...rest}
        />
      </InputContainer>
    </Container>
  )
}

export default PhoneNumberInput
