import React from 'react'
import { shallow } from 'enzyme'
import AdminStoresList from '.'

describe('AdminStoresList', () => {
  let testRender
  let stores
  let selectedStores
  let tableProps

  beforeEach(() => {
    stores = []
    selectedStores = ''
    tableProps = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AdminStoresList', async () => {
    testRender = shallow(
      <AdminStoresList
        stores={stores}
        selectedStores={selectedStores}
        tableProps={tableProps}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
