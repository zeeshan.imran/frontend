import styled from 'styled-components'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import { COMPONENTS_DEFAULT_MARGIN } from '../../../utils/Metrics'
import colors from '../../../utils/Colors'

export const Picture = styled.img`
  height: 3.6rem;
  width: 3.6rem;
  margin-right: ${COMPONENTS_DEFAULT_MARGIN}rem;
  border-radius: 0.4rem;
  border: solid 1px rgb(143, 143, 143);
`

export const FieldText = styled(Text)`
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  line-height: 2.2rem;
  color: rgba(0, 0, 0, 0.65);
`

export const Action = styled.span`
  cursor: pointer;
  font-family: ${family.primaryRegular};
  color: ${colors.PRODUCT_TABLE_COLOR};
  font-size: 1.4rem;
`
