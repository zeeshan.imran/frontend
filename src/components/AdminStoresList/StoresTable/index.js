import React, { Fragment } from 'react'
import { Table as AntTable, Divider } from 'antd'
import AlertModal from '../../AlertModal'
import { Picture, FieldText, Action } from './styles'
import moment from 'moment'
import i18n from '../../../utils/internationalization/i18n'

export const showDeleteStoreAlert = (handleRemove, storeId) =>
  AlertModal({
    title: i18n.t('components.storesTable.alertModalTitle'),
    description: i18n.t('components.storesTable.alertModaldescription'),
    handleOk: () => handleRemove(storeId),
    handleCancel: () => console.log('clicked Cancel')
  })

export const columns = (handleEdit, handleRemove) => [
  {
    title: i18n.t('components.storesTable.columnCompanyName'),
    dataIndex: 'name',
    render: (_, store) => (
      <Fragment>
        <Picture src={store.picture} />
        <FieldText>{store.name}</FieldText>
      </Fragment>
    )
  },
  {
    title: i18n.t('components.storesTable.columnCountry'),
    dataIndex: 'country',
    render: (_, store) => store.availableInCountry.value
  },
  {
    title: i18n.t('components.storesTable.columnState'),
    dataIndex: 'state',
    render: (_, store) => store.availableInCountry.state
  },
  {
    title: i18n.t('components.storesTable.columnDate'),
    dataIndex: 'date',
    render: (_, store) => store.date,
    sorter: (storeA, storeB) => moment(storeA.date) - moment(storeB.date),
    defaultSortOrder: 'descend'
  },
  {
    title: i18n.t('components.storesTable.columnAction'),
    dataIndex: 'actions',
    render: (_, store) => (
      <Fragment>
        <Action onClick={() => handleEdit(store.id)}>
          {i18n.t('components.storesTable.actionEdit')}
        </Action>
        <Divider type='vertical' />
        <Action onClick={() => showDeleteStoreAlert(handleRemove, store.id)}>
          {i18n.t('components.storesTable.actionRemove')}
        </Action>
      </Fragment>
    )
  }
]

const StoresTable = ({
  stores,
  handleChange,
  handleEditStore,
  handleDeleteStore
}) => {
  return (
    <AntTable
      rowKey={record => record.id}
      rowSelection={{
        onChange: handleChange
      }}
      columns={columns(handleEditStore, handleDeleteStore)}
      dataSource={stores}
    />
  )
}

export default StoresTable
