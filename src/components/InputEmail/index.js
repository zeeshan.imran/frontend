import React, { useState } from 'react'
import { Form } from 'antd'
import * as Yup from 'yup'
import Input from '../Input'
import { withTranslation } from 'react-i18next'

const validateMail = email =>
  Yup.string()
    .email()
    .isValidSync(email)

const validateRequiredEmail = email =>
  Yup.string()
    .required()
    .isValidSync(email)

const InputEmail = ({
  handleChange,
  handleValidityChange = () => {},
  onKeyUp,
  isRequired,
  defaultDisabled = false,
  defaultValue,
  t
}) => {
  const [emailErrors, setEmailErrors] = useState('')

  const onChange = ({ target: { value = '' } }) => {
    const isEmptyValid = !isRequired && value.trim() === ''
    const isValid =
      (validateMail(value) && (!isRequired || validateRequiredEmail(value))) ||
      isEmptyValid
    handleValidityChange(isValid)

    if (!isValid) {
      setEmailErrors(t('errors.inputValidEmail'))
    } else {
      setEmailErrors('')
    }

    return handleChange(value)
  }

  return (
    <Form.Item
      help={emailErrors}
      validateStatus={emailErrors ? 'error' : 'success'}
    >
      <Input
        disabled={defaultDisabled}
        defaultValue={defaultValue}
        placeholder={t('placeholders.email')}
        onChange={onChange}
        onKeyUp={onKeyUp}
      />
    </Form.Item>
  )
}

export default withTranslation()(InputEmail)
