import React from 'react'
import { Tooltip, Icon } from 'antd'

import { ClickableContainer, Ghost } from './styles'

const IconButton = ({ type, size, tooltip, onClick, theme }) => {
  const Wrapper = tooltip ? Tooltip : Ghost
  size = size || '1.2em'

  return (
    <ClickableContainer onClick={onClick}>
      <Wrapper placement='top' title={tooltip}>
        <Icon style={{ fontSize: size }} type={type} theme={theme} />
      </Wrapper>
    </ClickableContainer>
  )
}

export default IconButton
