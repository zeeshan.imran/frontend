import React from 'react'
import { Row, Col } from 'antd'
import { Desktop } from '../Responsive'
import BaseCardSpacer from '../BaseCardSpacer'
import SurveyCard from '../../containers/SurveyCard'
import HorizontalScroll from '../HorizontalScroll'
import PairedCardContainer from '../PairedCardContainer'

const SurveysGrid = ({ surveys }) => (
  <Desktop>
    {desktop =>
      desktop ? (
        <Row gutter={24}>
          {surveys.map((survey, index) => (
            <Col key={index} xs={{ span: 12 }} lg={{ span: 6 }}>
              <BaseCardSpacer>
                <SurveyCard survey={survey} />
              </BaseCardSpacer>
            </Col>
          ))}
        </Row>
      ) : (
        <HorizontalScroll>
          <PairedCardContainer>
            {surveys.map((survey, index) => (
              <SurveyCard key={index} mobile survey={survey} />
            ))}
          </PairedCardContainer>
        </HorizontalScroll>
      )
    }
  </Desktop>
)


export default SurveysGrid
