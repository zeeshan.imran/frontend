import styled from 'styled-components'
import { Col, Modal as AntModal, Icon as AntIcon } from 'antd'

export const StyledModal = styled(AntModal)`
  .ant-modal-body {
    width: 220px;
  }
  .ant-modal-content {
    width: 270px;
  }
`

export const ColorBlock = styled(Col)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  height: 32px;
  line-height: 32px;
  padding-left: 12px;

  .anticon {
    margin-left: 12px;
  }

  :hover {
    .anticon {
      display: inline-block;
    }
  }
`

export const ColorName = styled.span`
  border-radius: 4px;
  flex-grow: 2;
  height: 32px;
  color: ${({ textColor = 'black' }) => textColor};
`

export const AddIcon = styled(AntIcon)``

export const DeleteIcon = styled(AntIcon)`
  height: 32px;
  border-radius: 4px;
  line-height: 32px;
  display: none;
  color: ${({ textColor = 'black' }) => textColor};
`

export const EditIcon = styled(AntIcon)`
  height: 32px;
  border-radius: 4px;
  line-height: 32px;
  display: none;
  color: ${({ textColor = 'black' }) => textColor};
`

export const AddButton = styled.div`
  margin-top: 8px;
  width: 100%;

  .ant-btn {
    width: 100%;
    height: 32px;
  }
`
