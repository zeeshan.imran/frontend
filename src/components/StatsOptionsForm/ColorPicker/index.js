import React, { useState } from 'react'
import Button from '../../Button'
import { SketchPicker } from 'react-color'
import TooltipWrapper from '../../../components/TooltipWrapper'
import FieldLabel from '../../FieldLabel'
import { withTranslation } from 'react-i18next'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

import {
  StyledModal,
  ColorBlock,
  AddIcon,
  DeleteIcon,
  EditIcon,
  ColorName,
  AddButton
} from './styles'

const ColorPicker = ({ settings, option, setSettings, label, t }) => {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [colorToChangeIndex, setColorToChangeIndex] = useState(false)
  const [newColor, setNewColor] = useState(false)

  const onDragEnd = result => {
    if (!result.destination) {
      return
    }

    let destination = result.destination.index
    let source = result.source.index

    if (destination > source) {
      destination = destination + 1
    }

    let newColors = [...settings[option.name]]
    newColors = [
      ...newColors.slice(0, destination),
      settings[option.name][source],
      ...newColors.slice(destination)
    ]

    if (destination < source) {
      source = source + 1
    }

    newColors = [...newColors.slice(0, source), ...newColors.slice(source + 1)]

    setSettings({
      [option.name]: [...newColors]
    })
  }
  return (
    <React.Fragment>
      <FieldLabel label={label} tooltip={option.tooltip && t(option.tooltip)}>
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId='droppable' direction='vertical'>
            {provided => (
              <div
                ref={provided.innerRef}
                {...provided.droppableProps}
                style={{
                  display: 'flex',
                  flexDirection: 'column'
                }}
              >
                {settings[option.name] &&
                  settings[option.name].map((color, index) => {
                    let textColor = 'black'
                    const r = parseInt(`0x${color.slice(1, 3)}`, 16)
                    const g = parseInt(`0x${color.slice(3, 5)}`, 16)
                    const b = parseInt(`0x${color.slice(5, 7)}`, 16)
                    if ((r < 125 && g < 125 && b < 125) || r + g + b < 255) {
                      textColor = 'white'
                    }
                    return (
                      <Draggable
                        key={index}
                        draggableId={`${index}`}
                        index={index}
                      >
                        {innerProvided => (
                          <div
                            ref={innerProvided.innerRef}
                            {...innerProvided.draggableProps}
                            {...innerProvided.dragHandleProps}
                            style={{
                              backgroundColor: color,
                              userSelect: 'none',
                              marginTop: '8px',
                              borderRadius: '4px',
                              height: '32px',
                              ...innerProvided.draggableProps.style
                            }}
                          >
                            <ColorBlock>
                              <ColorName textColor={textColor}>
                                {color}
                              </ColorName>
                              <EditIcon
                                textColor={textColor}
                                type='edit'
                                onClick={() => {
                                  setIsModalVisible(true)
                                  setColorToChangeIndex(index)
                                  setNewColor(color)
                                }}
                              />
                              {settings[option.name].length !== 1 && (
                                <DeleteIcon
                                  textColor={textColor}
                                  type='delete'
                                  onClick={() => {
                                    setSettings({
                                      [option.name]: [
                                        ...settings[option.name].slice(
                                          0,
                                          index
                                        ),
                                        ...settings[option.name].slice(
                                          index + 1
                                        )
                                      ]
                                    })
                                  }}
                                />
                              )}
                            </ColorBlock>
                          </div>
                        )}
                      </Draggable>
                    )
                  })}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
          {option &&
            (!option.colorLimit ||
              option.colorLimit > settings[option.name].length) && (
              <AddButton>
                <TooltipWrapper helperText={t('tooltips.stats.addColor')}>
                  <Button
                    type='default'
                    onClick={() => {
                      setIsModalVisible(true)
                      setColorToChangeIndex(
                        settings[option.name] ? settings[option.name].length : 0
                      )
                      setNewColor('#ffffff')
                    }}
                  >
                    <AddIcon type='plus' />
                  </Button>
                </TooltipWrapper>
              </AddButton>
            )}
        </DragDropContext>
      </FieldLabel>
      <StyledModal
        title={label}
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false)
        }}
        onOk={() => {
          setIsModalVisible(false)
          if (settings[option.name]) {
            setSettings({
              [option.name]: [
                ...settings[option.name].slice(0, colorToChangeIndex),
                newColor,
                ...settings[option.name].slice(colorToChangeIndex + 1)
              ]
            })
          } else {
            setSettings({
              [option.name]: [newColor]
            })
          }
        }}
      >
        <SketchPicker
          color={newColor}
          onChangeComplete={change => {
            setNewColor(change.hex)
          }}
        />
      </StyledModal>
    </React.Fragment>
  )
}

export default withTranslation()(ColorPicker)
