import React from 'react'
import FieldLabel from '../../FieldLabel'
import { withTranslation } from 'react-i18next'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

const Grouping = ({ settings, option, setSettings, label, t }) => {
  const onDragEnd = result => {
    if (!result.destination) {
      return
    }

    let destinationIndex = result.destination.index
    let sourceIndex = result.source.index
    let destinationListIndex = parseInt(result.destination.droppableId, 10)
    let sourceListIndex = parseInt(result.source.droppableId, 10)
    let card = settings[sourceListIndex][sourceIndex]
    let newSettings = [...settings]

    if (destinationListIndex === sourceListIndex) {
      let newSubList = settings[sourceListIndex]

      if (destinationIndex > sourceIndex) {
        destinationIndex = destinationIndex + 1
      }
      newSubList = [
        ...newSubList.slice(0, destinationIndex),
        card,
        ...newSubList.slice(destinationIndex)
      ]

      if (destinationIndex < sourceIndex) {
        sourceIndex = sourceIndex + 1
      }
      newSubList = [
        ...newSubList.slice(0, sourceIndex),
        ...newSubList.slice(sourceIndex + 1)
      ]

      newSettings = [
        ...newSettings.slice(0, destinationListIndex),
        newSubList,
        ...newSettings.slice(destinationListIndex + 1)
      ]
    } else if (
      settings[sourceListIndex] &&
      destinationListIndex !== undefined
    ) {
      let destinationArray = settings[destinationListIndex] || []
      let sourceArray = settings[sourceListIndex]

      if (destinationArray.length) {
        destinationArray = [
          ...settings[destinationListIndex].slice(0, destinationIndex),
          settings[sourceListIndex][sourceIndex],
          ...settings[destinationListIndex].slice(destinationIndex)
        ]
      } else {
        destinationArray = [settings[sourceListIndex][sourceIndex]]
      }
      newSettings = [
        ...newSettings.slice(0, destinationListIndex),
        destinationArray,
        ...newSettings.slice(destinationListIndex + 1)
      ]

      sourceArray = [
        ...settings[sourceListIndex].slice(0, sourceIndex),
        ...settings[sourceListIndex].slice(sourceIndex + 1)
      ]
      if (sourceArray.length) {
        newSettings = [
          ...newSettings.slice(0, sourceListIndex),
          sourceArray,
          ...newSettings.slice(sourceListIndex + 1)
        ]
      } else {
        newSettings = [
          ...newSettings.slice(0, sourceListIndex),
          ...newSettings.slice(sourceListIndex + 1)
        ]
      }
    }
 
    setSettings({
      [option.name]: newSettings
    })
  }

  let shownSettings = settings || []
  if (settings.length) {
    settings.forEach(setting => {
      // if groups contain more than one option in them, then we need to show and empty group at the end
      // so we can expand the options to new groups untill there is one option per group
      if (setting.length >= 2) {
        shownSettings = [...shownSettings, []]
      }
    })
  }

  let canAddGroup = false

  let displayedSettings =
    settings &&
    settings.map(setting => {
      if (setting.length > 1) {
        canAddGroup = true
      }
      return setting
    })

  if (canAddGroup) {
    displayedSettings = [...displayedSettings, []]
  }

  return (
    <React.Fragment>
      <FieldLabel label={label} tooltip={option.tooltip && t(option.tooltip)}>
        <DragDropContext onDragEnd={onDragEnd}>
          {displayedSettings.map((chartColumn, chartColumnIndex) => {
            return (
              <Droppable
                droppableId={`${chartColumnIndex}`}
                key={chartColumnIndex}
                direction='horizontal'
              >
                {provided => (
                  <div
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      width: '100%',
                      backgroundColor: 'rgba(0, 0, 0, 0.25)',
                      minHeight: '44px',
                      marginBottom: '8px',
                      borderRadius: '4px'
                    }}
                  >
                    {chartColumn &&
                      chartColumn.map((dataColumn, dataColumnIndex) => {
                        if (dataColumnIndex > 1) {
                        }
                        return (
                          <Draggable
                            key={dataColumnIndex}
                            draggableId={`${chartColumnIndex}-${dataColumnIndex}`}
                            index={dataColumnIndex}
                          >
                            {innerProvided => (
                              <div
                                ref={innerProvided.innerRef}
                                {...innerProvided.draggableProps}
                                {...innerProvided.dragHandleProps}
                                style={{
                                  userSelect: 'none',
                                  borderRadius: '4px',
                                  height: '32px',
                                  lineHeight: '32px',
                                  margin: '6px',
                                  backgroundColor: 'white',
                                  paddingLeft: '6px',
                                  paddingRight: '6px',
                                  ...innerProvided.draggableProps.style
                                }}
                              >
                                {dataColumn}
                              </div>
                            )}
                          </Draggable>
                        )
                      })}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            )
          })}
        </DragDropContext>
      </FieldLabel>
    </React.Fragment>
  )
}

export default withTranslation()(Grouping)
