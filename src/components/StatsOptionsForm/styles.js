import styled from 'styled-components'
import {
  Row,
  Col,
  Switch,
  Divider as antDivider,
} from 'antd'

export const LabelContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  margin-bottom: 24px;
`

export const Content = styled(Row)`
  padding: 0 24px;
`

export const Setting = styled(Col)`
  height: ${({ isVariable = false }) => (isVariable ? 'auto' : '67px')};
  margin: 24px 0;
`

export const Switcher = styled(Switch)`
  width: 24px;
  margin: 8px 0;
`
export const Divider = styled(antDivider)`
  margin-bottom: 0;
`