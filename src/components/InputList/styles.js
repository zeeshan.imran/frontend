import styled from 'styled-components'
import Text from '../Text'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'
import { Row, Col } from 'antd'

export const InstructionIndex = styled(Text)`
  font-family: ${family.NotoSansKRRegular};
  color: ${colors.LIGHT_OLIVE_GREEN};
  font-size: 1.4rem;
  margin-right: 10px;
  display: block;
  text-align: right;
  line-height: 2.2;
`

export const MarginRow = styled(Row)`
  margin-bottom: 8px;
`

export const BtnCol = styled(Col)`
  line-height: 1;
  padding-left: 4px;
  button {
    padding-left: 11px;
    padding-right: 11px;
  }
`
