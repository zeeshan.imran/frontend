import React from 'react'
import { mount } from 'enzyme'
import InputList from './index'
import wait from '../../utils/testUtils/waait'
import { MarginRow } from './styles'
import Input from '../Input'
import Button from '../Button'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('InputList component', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test(`should display correct amount of list entries `, async () => {
    testRender = mount(<InputList value={['1', '2', '3']} />)
    await wait(0)

    expect(testRender.find(MarginRow).length).toBe(3)
    testRender.setProps({ value: [] })
    expect(testRender.find(MarginRow).length).toBe(0)
  })

  test(`should handle edit and btn clicks`, async () => {
    const mockedCallback = jest.fn()
    testRender = mount(
      <InputList value={['1', '2', '3']} onChange={mockedCallback} />
    )
    await wait(0)

    testRender
      .find(Input)
      .at(0)
      .props()
      .onChange({
        target: {
          value: '1-test'
        }
      })

    expect(mockedCallback).toBeCalledWith(['1-test', '2', '3'])

    testRender
      .find(Button)
      .at(2)
      .props()
      .onClick()
    expect(mockedCallback).toBeCalledWith(['1', '2'])

    testRender
      .find(Button)
      .at(3)
      .props()
      .onClick()
    expect(mockedCallback).toBeCalledWith(['1', '2', '3', ''])
  })
})
