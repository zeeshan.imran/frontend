import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Card = styled.div`
  background-color: ${colors.WHITE};
  border-radius: 0.2rem;
`

export const Divider = styled.div`
  height: 0.1rem;
  background-color: ${colors.BLACK};
  opacity: 0.2;
`
