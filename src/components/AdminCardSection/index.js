import React from 'react'
import Title from './Title'
import Content from './Content'
import { Card, Divider } from './styles'

const AdminCardSection = ({ title, children }) => (
  <Card>
    <Title title={title} />
    <Divider />
    <Content content={children} />
  </Card>
)

export default AdminCardSection
