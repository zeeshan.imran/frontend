import React from 'react'
import { shallow } from 'enzyme'
import Content from '.'

describe('Content', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Content', async () => {
    const content = 'Admin Card Section Content'
    testRender = shallow(<Content content={content} />)
    expect(testRender).toMatchSnapshot()
  })
})
