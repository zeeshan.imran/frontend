import React from 'react'
import { Container } from './styles'

const Content = ({ content }) => <Container>{content}</Container>

export default Content
