import React from 'react'
import { shallow } from 'enzyme'
import AdminCardSection from '.'

describe('AdminCardSection', () => {
  let testRender
  let title

  beforeEach(() => {
    title = 'Card Section Title'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AdminCardSection', async () => {
    const children = 'Admin Card Section Content'

    testRender = shallow(<AdminCardSection title={title} children={children} />)
    expect(testRender).toMatchSnapshot()
  })
})
