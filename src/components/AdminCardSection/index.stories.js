import React from 'react'
import { storiesOf } from '@storybook/react'
import AdminCardSection from './'

storiesOf('AdminCardSection', module).add('Story', () => (
  <AdminCardSection title='Card Section Title'>
    Admin Card Section Content
  </AdminCardSection>
))
