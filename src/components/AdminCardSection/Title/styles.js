import styled from 'styled-components'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'

export const Container = styled.div`
  height: 4.5rem;
  display: flex;
  align-items: center;
  padding-left: 3.5rem;
`

export const TitleText = styled(Text)`
  color: rgba(0, 0, 0, 0.85);
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
`
