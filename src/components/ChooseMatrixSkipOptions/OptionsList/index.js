import React, { useState, useEffect } from 'react'
import { Row, Col, Form, TreeSelect } from 'antd'
import { useTranslation } from 'react-i18next'
import Select from '../../Select'
import Button from '../../Button'
import { reject, includes } from 'ramda'
import {
  getOptionName,
  getOptionValue
} from '../../../utils/getters/OptionGetters'
import { SKIP_MULTIPLE_TYPES } from '../../../utils/Constants'
import useSkipToOptions from '../../../hooks/useSkipToOptions'

const OptionsList = ({
  setCanAddRules,
  canAddRules,
  answers,
  rowData,
  // skipOptions,
  conditionalOptions,
  updateCurrentConditions,
  currentIndex,
  selectedQuestion,
  selectedOptions,
  selectedType,
  id,
  clientGeneratedId,
  parentSelectedAnswers,
  setParentSelectedAnswers,
  isLastIndex
}) => {
  
  const { t } = useTranslation()
  const { SHOW_PARENT } = TreeSelect;
  const skipOptions = useSkipToOptions({ id, clientGeneratedId }, true)
  
  const [maxCondintionalOptions, setMaxCondintionalOptions] = useState(
    selectedType
  )
  const [selectedSkipQuestion, setSelectedSkipQuestion] = useState(
    selectedQuestion
  )
  const [selectedAnswers, setSelectedAnswers] = useState(selectedOptions)
  //   isLastIndex &&
  //   selectedOptions.length !== answers.length
  // )
  
  const condintionalOptions = [
    SKIP_MULTIPLE_TYPES.ALL_OF_THE_FOLLOWING,
    SKIP_MULTIPLE_TYPES.ANY_OF_THE_FOLLOWING,
    SKIP_MULTIPLE_TYPES.ANY_OF_THE_FOLLOWING_NOT_SELECTED
  ]
  const fieldChangeHandler = value => {
    setMaxCondintionalOptions(value)
    updateCurrentConditions(currentIndex, { type: value })
  }

  const selectedAnswersFieldChangeHandler = value => {
    
    if (answers.length === value.length) {
      setCanAddRules(false)
    } else if (!canAddRules) {
      setCanAddRules(true)
    }
    setSelectedAnswers(value)
    updateCurrentConditions(currentIndex, { answers: value })
  }

  const selectedQuestionFieldChangeHandler = value => {
    setSelectedSkipQuestion(value)
    updateCurrentConditions(currentIndex, { skipTo: value })
  }

  const removeValues = (selected, all) => {
    const values = reject(x => {
      return includes(x.value, selected)
    }, all)
    return values
  }

  const addCondition = () => {
    const data = {
      options: rowData,
      selectedAnswers: selectedAnswers,
      skipOptions: removeValues([selectedSkipQuestion], skipOptions),
      skipTo: [],
      answers: [],
      type: ''
    }
    conditionalOptions(data)
  }

  useEffect(() => {
    const skipOptionsValues = skipOptions.map(option => option.value)
    if (
      !skipOptionsValues.includes(selectedSkipQuestion) &&
      selectedSkipQuestion
    ) {
      selectedQuestionFieldChangeHandler()
    }
  })

  return (
    <Row gutter={24} type={'flex'} justify={'space-around'} align={'bottom'}>
      <Col lg={6} md={6}>
        <Form.Item>
          <Select
            label={t(
              'components.chooseMultipleSkipOptions.maxCondintionalOptions'
            )}
            value={maxCondintionalOptions}
            onChange={value => {
              fieldChangeHandler(value)
            }}
            placeholder={t('placeholders.selectConditionType')}
            options={condintionalOptions}
          />
        </Form.Item>
      </Col>
      <Col lg={6} md={6}>
        <Form.Item>
          <TreeSelect
            mode='multiple'
            treeData={answers}
            treeCheckable
            showCheckedStrategy={SHOW_PARENT} 
            placeholder={t('placeholders.selectAnswers')}
            size={'large'}
            value={selectedAnswers}
            onChange={selectedAnswersFieldChangeHandler}
            style={{
              width: '100%'
            }}
          />
        </Form.Item>
      </Col>
      <Col lg={6} md={6}>
        <Form.Item>
          <Select
            label={t('components.chooseMultipleSkipOptions.showQuestions')}
            value={selectedSkipQuestion}
            onChange={selectedQuestionFieldChangeHandler}
            placeholder={t('placeholders.questionJumpTo')}
            options={skipOptions}
            getOptionName={getOptionName}
            getOptionValue={getOptionValue}
          />
        </Form.Item>
      </Col>
      <Col lg={6} md={6}>
        <Form.Item>
          <Button onClick={() => addCondition()}>
            {t('components.questionCreation.addOtherConditions')}
          </Button>
        </Form.Item>
      </Col>
    </Row>
  )
}

export default OptionsList
