import React from 'react'
import { shallow } from 'enzyme'
import StatsFilterModal from '.'

describe('StatsFilterModal', () => {
  let testRender
  let onClick
  let text

  beforeEach(() => {
    onClick = jest.fn()
    text = 'Create survey'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render StatsFilterModal', async () => {
    testRender = shallow(
      <StatsFilterModal onClick={onClick} text={text} rest={{}} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
