import React, { useState } from 'react'
import { Formik } from 'formik'
import { Switch } from 'antd'
import { findIndex, remove, propEq } from 'ramda'
import Button from '../Button'
import { displayErrorPopup, distroyErrorPopup } from '../../utils/displayErrorPopup'
import CreationButtonStatsContainer from '../CreationButtonStatsContainer'
import { useTranslation } from 'react-i18next'
import {
  RowLabel,
  RowHeader,
  RowContent,
  RowStarter,
  SwitchArea,
  StyledModal
} from './styles'
import { PRODUCT_LIMIT_MIN_STATS } from '../../utils/Constants'

const StatsFilterModal = ({
  finalQuestionsSelected,
  setQuestionsSelected,
  questionsSelected,
  modalHeaderTitle,
  questionList,
  submitText,
  resetText,
  onSubmit,
  onReset,
  hasProductSelection,
  shouldGenerateCharts,
  setShouldGenerateCharts,
  productsSelected,
  data
}) => {
  const [showFilterModal, setShowFilterModal] = useState(false)
  const { t } = useTranslation()

  return (
    <React.Fragment>
      <CreationButtonStatsContainer
        hasProductSelection={hasProductSelection}
        shouldGenerateCharts={shouldGenerateCharts}
        productButtonClick={() => {
          if (shouldGenerateCharts) {
            setShouldGenerateCharts(false)
          } else {
            if (
              productsSelected.length < PRODUCT_LIMIT_MIN_STATS ||
              productsSelected.length > data.survey.maxProductStatCount
            ) {
              return displayErrorPopup(
                t('containers.surveyStats.productsTab.maxNumberError', {
                  minNumber: PRODUCT_LIMIT_MIN_STATS,
                  maxNumber: data.survey.maxProductStatCount,
                  currentNumber: productsSelected.length
                })
              )
            }else{
              distroyErrorPopup()
            }
            setShouldGenerateCharts(true)
          }
        }}
        productButtonText={
          shouldGenerateCharts
            ? t('containers.surveyStats.productsTab.backToProductSelection')
            : t('containers.surveyStats.productsTab.buttonText')
        }
        filterButtonClick={() => {
          if (questionList && questionList.length) {
            setShowFilterModal(true)
          } else {
            return displayErrorPopup(
              t('containers.surveyStats.otherFilters.noFilter')
            )
          }
        }}
        filterButtonText={t('containers.surveyStats.otherFilters.buttonText')}
      />
      {showFilterModal && (
        <StyledModal
          title={modalHeaderTitle}
          visible
          width={800}
          footer={[
            <Button
              key={`reset`}
              type={`secondary`}
              onClick={() => {
                onReset()
                setShowFilterModal(false)
              }}
            >
              {resetText}
            </Button>,
            <Button
              key={`submit`}
              onClick={() => {
                onSubmit()
                setShowFilterModal(false)
              }}
            >
              {submitText}
            </Button>
          ]}
          onCancel={() => {
            setShowFilterModal(false)
          }}
        >
          <Formik
            render={() => {
              return (
                <React.Fragment>
                  {questionList.map((question, index) => {
                    return (
                      <RowStarter key={index}>
                        <RowHeader>{question.chartTitle}</RowHeader>
                        <RowContent>
                          {question.options.map((option, optionIndex) => {
                            const existsInFinal = findIndex(
                              propEq('question', question.id),
                              finalQuestionsSelected
                            )
                            const checkByDefault =
                              existsInFinal >= 0
                                ? finalQuestionsSelected[existsInFinal][
                                    'options'
                                  ].indexOf(option.value) >= 0
                                : false
                            return (
                              <SwitchArea key={optionIndex}>
                                <Switch
                                  defaultChecked={checkByDefault}
                                  size={`small`}
                                  onChange={checked => {
                                    const isAlreadyAdded = findIndex(
                                      propEq('question', question.id),
                                      questionsSelected
                                    )
                                    if (checked) {
                                      if (isAlreadyAdded < 0) {
                                        questionsSelected.push({
                                          question: question.id,
                                          options: [option.value]
                                        })
                                      } else {
                                        questionsSelected[isAlreadyAdded][
                                          'options'
                                        ].push(option.value)
                                      }
                                    } else {
                                      questionsSelected[isAlreadyAdded][
                                        'options'
                                      ] = remove(
                                        questionsSelected[isAlreadyAdded][
                                          'options'
                                        ].indexOf(option.value),
                                        1,
                                        questionsSelected[isAlreadyAdded][
                                          'options'
                                        ]
                                      )
                                    }

                                    setQuestionsSelected(questionsSelected)
                                  }}
                                />
                                <RowLabel>{option.label}</RowLabel>
                              </SwitchArea>
                            )
                          })}
                        </RowContent>
                      </RowStarter>
                    )
                  })}
                </React.Fragment>
              )
            }}
          />
        </StyledModal>
      )}
    </React.Fragment>
  )
}

export default StatsFilterModal
