import styled from 'styled-components'
import Text from '../Text'
import { Modal as AntModal } from 'antd'

export const RowStarter = styled.div`
  margin-top: 3rem;
`
export const RowContent = styled.div`
`
export const SwitchArea = styled.div`
  margin-left: 1.5rem;
  display: inline-block;
`
export const StyledModal = styled(AntModal)`
`
export const RowHeader = styled(Text)`
  display: block;
  color: #aaa;
`
export const RowLabel = styled(Text)`
  margin-left: 0.5rem;
`