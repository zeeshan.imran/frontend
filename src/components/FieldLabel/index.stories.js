import React from 'react'
import { storiesOf } from '@storybook/react'
import FieldLabel from './'

storiesOf('FieldLabel', module).add('Labeled Component', () => (
  <FieldLabel label='My Label'>COMPONENT</FieldLabel>
))
