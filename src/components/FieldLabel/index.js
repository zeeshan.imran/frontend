import React from 'react'
import PropTypes from 'prop-types'
import { isEmpty, isNil } from 'ramda'
import Label from './Label'
import { Container, LabelWrapper } from './styles'
import HelperIcon from '../HelperIcon'

const FieldLabel = ({
  tooltip = false,
  tooltipPlacement,
  label,
  required,
  children,
  postFill = false
}) => {
  const shouldRenderLabel = !isEmpty(label) && !isNil(label)

  return (
    <Container>
      <LabelWrapper>
        {shouldRenderLabel && <Label required={required} label={label} />}
        {tooltip && (
          <HelperIcon
            placement={tooltipPlacement}
            helperText={tooltip}
            isinlabel={`true`}
          />
        )}
      </LabelWrapper>
      {children}
      {postFill && (
        <LabelWrapper>
          <Label label={postFill} />
        </LabelWrapper>
      )}
    </Container>
  )
}

FieldLabel.propTypes = {
  label: PropTypes.string,
  required: PropTypes.bool
}

export default FieldLabel
