import React from 'react'
import { shallow } from 'enzyme'
import FieldLabel from '.'

describe('FieldLabel', () => {
  let testRender
  let label
  let required
  let children

  beforeEach(() => {
    label = 'Name'
    required = false
    children = '<container>Enter name here</container>'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render FieldLabel', async () => {
    testRender = shallow(
      <FieldLabel label={label} required={required} children={children} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
