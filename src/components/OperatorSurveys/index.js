import React from 'react'
import Button from '../Button'
import SearchBar from '../SearchBar'
import OperatorSurveysList from '../OperatorSurveysList'
import Select from '../Select'

import { Container, HeaderContainer, SearchBarContainer } from './styles'
import { useTranslation } from 'react-i18next'
import { isUserAuthenticatedAsOperator, isUserAuthenticatedAsSuperAdmin } from '../../utils/userAuthentication'

const surveySearchStateOptions = ['All', 'Draft', 'Published', 'Suspended']

const OperatorSurveys = ({
  loading,
  searchTerm,
  onSearch,
  createNew,
  surveys,
  organizations,
  total,
  page,
  onChangePage,
  onClickRow,
  surveyState,
  setSurveyState
}) => {
  const { t } = useTranslation()
  const canCreateSurvey = isUserAuthenticatedAsOperator() || isUserAuthenticatedAsSuperAdmin()
  return (
    <Container>
      <HeaderContainer>
        {canCreateSurvey && (
          <Button data-testid='create-survey-button' onClick={createNew}>
            {t('components.operatorSurveys.createNew')}
          </Button>
        )}
        <SearchBarContainer>
          <Select
            name='surveySearchState'
            size='default'
            value={surveyState}
            options={surveySearchStateOptions}
            onChange={newValue => {
              setSurveyState(newValue)
            }}
            placeholder={t(
              `components.createTasterAccount.forms.third.foodAllergies.placeholder`
            )}
          />
        </SearchBarContainer>
        <SearchBarContainer>
          <SearchBar
            placeholder={t('placeholders.search')}
            withIcon
            handleChange={onSearch}
            value={searchTerm}
          />
        </SearchBarContainer>
      </HeaderContainer>
      <OperatorSurveysList
        loading={loading}
        surveys={surveys}
        organizations={organizations}
        page={page}
        total={total}
        showHeader={false}
        onChangePage={onChangePage}
        showActions
      />
    </Container>
  )
}

export default OperatorSurveys
