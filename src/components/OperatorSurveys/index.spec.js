import React from 'react'
import { mount } from 'enzyme'
import OperatorSurveys from '.'
import { createBrowserHistory } from 'history'
import { Router } from 'react-router-dom'
import { act } from 'react-dom/test-utils'
import Button from '../Button'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { isUserAuthenticatedAsOperator } from '../../utils/userAuthentication'

jest.mock('../../utils/userAuthentication')
describe('OperatorSurveys', () => {
  let testRender
  let loading
  let searchTerm
  let onSearch
  let createNew
  let surveys
  let total
  let page
  let onChangePage
  let onClickRow
  let history
  let client

  beforeEach(() => {
    loading = false
    searchTerm = 'a'
    onSearch = jest.fn()
    createNew = jest.fn()
    surveys = [
      {
        id: '1',
        products: [],
        screeners: []
      },
      {
        id: '2',
        products: [],
        screeners: []
      }
    ]

    total = 10
    page = 1
    onChangePage = jest.fn()
    onClickRow = jest.fn()
    history = createBrowserHistory()
    client = createApolloMockClient()
    isUserAuthenticatedAsOperator.mockImplementation(() => true)
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorSurveys', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorSurveys
            loading={loading}
            searchTerm={searchTerm}
            onSearch={onSearch}
            createNew={createNew}
            surveys={surveys}
            total={total}
            page={page}
            onChangePage={onChangePage}
            onClickRow={onClickRow}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(OperatorSurveys)).toHaveLength(1)

    act(() => {
      testRender.find(Button).simulate('click')
    })

    expect(createNew).toHaveBeenCalled()
  })
})
