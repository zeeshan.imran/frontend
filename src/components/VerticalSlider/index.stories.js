import React from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import VerticalSlider from './'

const defaultValue = 2
const step = 1

const Container = styled.div`
  margin-top: 2rem;
  height: 33.2rem;
`

const handleChange = value => console.log('selected value', value)

storiesOf('VerticalSlider', module)
  .add('Slider From 1 to 9, default at 1', () => (
    <Container>
      <VerticalSlider
        min={1}
        max={9}
        value={defaultValue}
        step={step}
        onChange={handleChange}
      />
    </Container>
  ))
  .add('Slider From 1 to 5, default at 1', () => (
    <Container>
      <VerticalSlider
        min={1}
        max={5}
        value={defaultValue}
        step={step}
        onChange={value => console.log('value', value)}
      />
    </Container>
  ))
  .add('Slider initial state, with no handler showing', () => (
    <Container>
      <VerticalSlider
        min={1}
        max={9}
        step={step}
        onChange={value => console.log('value', value)}
      />
    </Container>
  ))
