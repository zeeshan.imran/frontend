import React from 'react'
import { mount, shallow } from 'enzyme'
import VerticalSlider from '.'
import { CustomSlider } from './styles'

describe('VerticalSlider', () => {
  let testRender
  let min
  let max
  let value
  let step
  let onChange
  let labels

  beforeEach(() => {
    min = 7
    max = 5
    value = 6
    step = 2
    onChange = jest.fn()
    labels = ['Base Label', 'Middle Label', 'Last Label']
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render VerticalSlider', () => {
    testRender = shallow(
      <VerticalSlider
        min={min}
        max={max}
        value={value}
        step={step}
        onChange={onChange}
        labels={labels}
      />
    )
    expect(testRender).toMatchSnapshot()
  })

  test('should render mark', () => {
    testRender = mount(
      <VerticalSlider
        min={0}
        max={5}
      />
    )
    const marktest = testRender.find(CustomSlider).prop('marks')
    expect(marktest).toEqual({0: 0, 1:1, 2:2, 3:3, 4:4, 5:5})
  })
})
