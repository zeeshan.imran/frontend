import React from 'react'
import PropTypes from 'prop-types'
import RightLabels from './RightLabels'
import { Container, CustomSlider, SliderPadding } from './styles'


const getTicks = (min, max) => {
  let accumulator = {}
  for (let index = min; index <= max; index++) {
    accumulator[index] = index
  }
  return accumulator
}

const VerticalSlider = ({ min, max, value, step, onChange, onAfterChange = () => {}, labels }) => (
  <Container>
    <SliderPadding>
      <CustomSlider
        styled={{ ghostMarker: typeof value !== 'number' }}
        tooltipVisible={false}
        max={max}
        min={min}
        value={value || min}
        defaultValue={min + 1}
        vertical
        marks={getTicks(min, max)}
        step={step}
        onChange={onChange}
        onAfterChange={value => {
          onChange(value)
          onAfterChange()
        }}
      />
    </SliderPadding>
    {labels && <RightLabels labels={labels} />}
  </Container>
)

VerticalSlider.defaultProps = {
  onChange: () => {},
  defaultValue: 1,
  step: 1
}

VerticalSlider.propTypes = {
  onChange: PropTypes.func,
  min: PropTypes.number,
  max: PropTypes.number,
  defaultValue: PropTypes.number,
  step: PropTypes.number
}

export default VerticalSlider
