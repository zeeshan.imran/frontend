import styled from 'styled-components'
import { Slider as AntSlider } from 'antd'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'
import { components } from '../../utils/Metrics'

export const SliderPadding = styled.div`
  padding: 1.1rem;
`

export const Container = styled.div`
  display: flex;
  flex-direction: row;
`

export const CustomSlider = styled(AntSlider)`
  left: 1.5rem;
  .ant-slider-track {
    background-color: ${colors.LIGHT_OLIVE_GREEN} !important;
  }
  .ant-slider-rail {
    background-color: ${colors.PALE_LIGHT_GREY};
  }
  .ant-slider-handle {
    width: ${({ styled: { ghostMarker } }) =>
    ghostMarker ? 0 : components.SLIDER_HANDLE_SIZE}rem;
    height: ${({ styled: { ghostMarker } }) =>
    ghostMarker ? 0 : components.SLIDER_HANDLE_SIZE}rem;
    margin-left: -0.9rem;
    border: ${({ styled: { ghostMarker } }) =>
    ghostMarker
      ? 'none'
      : `solid 0.2rem ${colors.LIGHT_OLIVE_GREEN}`} !important;
    box-shadow: none;
    background-color: ${colors.LIGHT_OLIVE_GREEN};
    transform: translateY(0%) !important;;
  }
  .ant-slider-dot {
    width: ${components.SLIDER_DOT_SIZE}rem;
    height: ${components.SLIDER_DOT_SIZE}rem;
    left: auto;
    right: calc(
      -1 * calc(${components.SLIDER_DOT_SIZE - components.SLIDER_WIDTH}rem / 2)
    );
    @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
      right: -0.6rem;
    };
    border-color: ${colors.LIGHT_GREY};
  }
  &.ant-slider-vertical {
    top: -0.2rem;
  }
  &.ant-slider:hover .ant-slider-rail {
    background-color: ${colors.PALE_LIGHT_GREY};
  }
  &.ant-slider:hover .ant-slider-track {
    background-color: ${colors.ERROR};
  }
  .ant-slider-mark-text {
    font-family: ${family.primaryBold};
    font-size: 1.7rem;
    color: rgba(0, 0, 0, 0.65);
  }
  .ant-slider-mark {
    left: -4rem;
  }
`
