import React from 'react'
import { shallow } from 'enzyme'
import RightLabels from '.'

describe('RightLabels', () => {
    let testRender
    let labels

    beforeEach(() => {
        labels = ['Base Label', 'Middle Label', 'Last Label'];
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render RightLabels', () => {
        testRender = shallow(
            <RightLabels
                labels={labels}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
})
