import styled from 'styled-components'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'

export const Container = styled.div`
  margin-left: 1rem;
  display: flex;
  flex-direction: column-reverse;
  justify-content: space-between;
`

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
`
