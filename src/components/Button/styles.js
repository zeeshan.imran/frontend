import styled from 'styled-components'
import { Button } from 'antd'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const CustomButtonStyle = styled(Button)`
  background-color: ${colors.PRIMARY_BUTTON_BACKGROUND_COLOR};
  border-radius: 0.65rem;
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  letter-spacing: 0.04rem;

  /* Color that is applied when its hovered */
  &:hover {
    background-color: ${colors.PRIMARY_BUTTON_BACKGROUND_HOVER_COLOR};
    color: ${colors.PRIMARY_BUTTON_HOVER_COLOR};
  }

  /* Color that stays when it is clicked */
  &:focus {
    background-color: ${colors.PRIMARY_BUTTON_BACKGROUND_COLOR};
    color: ${colors.WHITE};
  }

  /* Color that flashes when is clicked */
  &:active {
    background-color: ${colors.PRIMARY_BUTTON_HOVER_COLOR};
    color: ${colors.WHITE};
  }

  &[ant-click-animating-without-extra-node]:after {
    border-color: ${colors.SOFT_GREEN} !important;
  }
`

export const FacebookButton = styled(CustomButtonStyle)`
  background-color: ${colors.FACEBOOK_BLUE};
  color: ${colors.WHITE};
  border: none;

  /* Color that is applied when its hovered */
  &:hover {
    background-color: ${colors.DARKER_FACEBOOK_BLUE};
    color: ${colors.WHITE};
  }

  /* Color that stays when it is clicked */
  &:focus {
    background-color: ${colors.DARKER_FACEBOOK_BLUE};
    color: ${colors.WHITE};
  }

  /* Color that flashes when is clicked */
  &:active {
    background-color: ${colors.FACEBOOK_BLUE};
    color: ${colors.WHITE};
  }

  &[ant-click-animating-without-extra-node]:after {
    border-color: ${colors.FACEBOOK_BLUE} !important;
  }
`

export const ThirdButton = styled(CustomButtonStyle)`
  background-color: ${colors.SALMON};
  color: ${colors.WHITE};
  border: none;

  /* Color that is applied when its hovered */
  &:hover {
    background-color: ${colors.RED};
    color: ${colors.WHITE};
  }

  /* Color that stays when it is clicked */
  &:focus {
    background-color: ${colors.RED};
    color: ${colors.WHITE};
  }

  /* Color that flashes when is clicked */
  &:active {
    background-color: ${colors.SALMON};
    color: ${colors.WHITE};
  }

  &[ant-click-animating-without-extra-node]:after {
    border-color: ${colors.SALMON} !important;
  }
`

export const RedButton = styled(CustomButtonStyle)`
  background-color: ${colors.RED};
  color: ${colors.WHITE};
  border: none;

  /* Color that is applied when its hovered */
  &:hover {
    background-color: ${colors.DARKER_RED};
    color: ${colors.WHITE};
  }

  /* Color that stays when it is clicked */
  &:focus {
    background-color: ${colors.DARKER_RED};
    color: ${colors.WHITE};
  }

  /* Color that flashes when is clicked */
  &:active {
    background-color: ${colors.RED};
    color: ${colors.WHITE};
  }

  &[ant-click-animating-without-extra-node]:after {
    border-color: ${colors.RED} !important;
  }
`

export const DisabledButton = styled(CustomButtonStyle)`
  background-color: ${colors.LIGHT_OLIVE_GREEN} !important;
  color: ${colors.WHITE} !important;
  opacity: 0.6;
  border: none;
`

export const SecondaryButton = styled(CustomButtonStyle)`
  background-color: ${colors.WHITE};
  color: ${colors.SLATE_GREY};
  border-color: ${colors.LIGHT_GREY};

  /* Color that is applied when its hovered */
  &:hover {
    background-color: ${colors.LIGHT_GREY};
    color: ${colors.SLATE_GREY};
    border-color: ${colors.LIGHT_GREY};
  }

  /* Color that stays when it is clicked */
  &:focus {
    background-color: ${colors.LIGHT_GREY};
    color: ${colors.SLATE_GREY};
    border-color: ${colors.LIGHT_GREY};
  }

  /* Color that flashes when is clicked */
  &:active {
    background-color: ${colors.WHITE};
    color: ${colors.SLATE_GREY};
    border-color: ${colors.LIGHT_GREY};
  }

  &[ant-click-animating-without-extra-node]:after {
    border-color: ${colors.LIGHT_GREY} !important;
  }
`

export const PrimaryButton = styled(CustomButtonStyle)`
  border: none;
  color: ${colors.WHITE};
  &:hover {
    background-color: ${colors.PRIMARY_BUTTON_BACKGROUND_HOVER_COLOR};
    color: ${colors.PRIMARY_BUTTON_HOVER_COLOR};
  }
  &:active {
    background-color: ${colors.ICON_BUTTON_COLOR_ACTIVE};
    color: ${colors.WHITE};
  }
`
