import React from 'react'
import { mount } from 'enzyme'
import CustomButton from '.'
import {
  DisabledButton,
  FacebookButton,
  SecondaryButton,
  RedButton
} from './styles'

describe('CustomButton', () => {
  let testRender
  let size
  let type
  let desktop
  let rest

  beforeEach(() => {
    size = 'large'
    type = 'disabled'
    desktop = null
    rest = {}
  })

  afterEach(() => {
    // if (testRender) {
    testRender.unmount()
    // }
  })

  test('should render DisabledButton Button', () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 1024,
      height: 800
    })

    testRender = mount(
      <CustomButton size={size} type={type} desktop={desktop} rest={rest} />
    )

    expect(testRender.find(DisabledButton).prop('size')).toBe('large')

    expect(testRender.find(DisabledButton).prop('disabled')).toBe(true)
  })

  test('should render FacebookButton Button', () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 1024,
      height: 800
    })

    testRender = mount(
      <CustomButton
        size={size}
        type={'facebook'}
        desktop={desktop}
        rest={rest}
      />
    )

    expect(testRender.find(FacebookButton).prop('size')).toBe('large')
  })

  test('should render secondary Button', () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 1024,
      height: 800
    })

    testRender = mount(
      <CustomButton
        size={size}
        type={'secondary'}
        desktop={desktop}
        rest={rest}
      />
    )

    expect(testRender.find(SecondaryButton).prop('size')).toBe('large')
  })

  test('should render RedButton Button', () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 1024,
      height: 800
    })

    testRender = mount(
      <CustomButton size={size} type={'red'} desktop={desktop} rest={rest} />
    )

    expect(testRender.find(RedButton).prop('size')).toBe('large')
  })
})
