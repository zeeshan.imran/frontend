import React from 'react'
import PropTypes from 'prop-types'
import {
  DisabledButton,
  FacebookButton,
  SecondaryButton,
  PrimaryButton,
  ThirdButton,
  RedButton
} from './styles'

const CustomButton = props => {
  const { size, type, desktop, ...rest } = props
  if (type === 'disabled') {
    return <DisabledButton disabled size={size || 'large'} {...rest} />
  }
  if (type === 'facebook') {
    return <FacebookButton size={size || 'large'} {...rest} />
  }
  if (type === 'secondary') {
    return <SecondaryButton size={size || 'large'} {...rest} />
  }
  if (type === 'red') {
    return <RedButton size={size || 'large'} {...rest} />
  }
  if (type === 'third') {
    return <ThirdButton size={size || 'large'} {...rest} />
  }
  if (type === 'thirdDisabled') {
    return <ThirdButton disabled size={size || 'large'} {...rest} />
  }
  return <PrimaryButton size={size || 'large'} {...rest} />
}

CustomButton.propTypes = {
  onClick: PropTypes.func,
  type: PropTypes.string,
  size: PropTypes.string
}

export default CustomButton
