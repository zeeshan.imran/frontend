import React from 'react'
import { mount } from 'enzyme'
import SurveyBasicInfoForm from '.'
import Input from '../Input'
import { act } from 'react-dom/test-utils'
import InputList from '../InputList'
import InputRichText from '../InputRichText'
import UploadSurveyCoverPicture from '../../containers/UploadSurveyCoverPicture'

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: text => text }),
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
}))
jest.mock('../../utils/internationalization/i18n', () => ({
  t: text => text,
}))

jest.mock('../../containers/UploadSurveyCoverPicture', () => () => null)
 
describe('SurveyBasicInfoForm', () => {
  let testRender
  let numberOfProducts
  let onChange 
  let name
  let instructionsText
  let instructionSteps
  let thankYouText
  let rejectionText
  let uniqueName
  let authorizationType
  let exclusiveTasters 
  let recaptcha
  let surveyLanguage 
  let customButtons
  let handleCustomButtonChange
  let handleLanguageChange

  beforeEach(() => {
    numberOfProducts = 5
    onChange = jest.fn()
    name = 'Product one'
    instructionsText = 'Please open products carefully'
    instructionSteps = ['1', '2', '3']
    thankYouText = 'Thank you for purchasing product'
    rejectionText = 'Product is crashed'
    uniqueName = '1 - test - 201539'
    authorizationType = 'auth'
    exclusiveTasters = ['gums']
    recaptcha = true
    surveyLanguage = ['eng', 'de', 'kr']
    customButtons = {
      continue: 'continue',
      start: 'start',
      next: 'next',
      skip: 'skip'
    }
    handleCustomButtonChange = jest.fn()
    handleLanguageChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyBasicInfoForm', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        numberOfProducts={numberOfProducts}
        onChange={onChange}
        name={name}
        instructionsText={instructionsText}
        instructionSteps={instructionSteps}
        thankYouText={thankYouText}
        rejectionText={rejectionText}
        uniqueName={uniqueName}
        authorizationType={authorizationType}
        exclusiveTasters={exclusiveTasters}
        recaptcha={recaptcha}
        surveyLanguage={surveyLanguage}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
        handleLanguageChange={handleLanguageChange}
      />
    )
    expect(testRender.find(SurveyBasicInfoForm)).toHaveLength(1)

    testRender
      .find('input[type="checkbox"]')
      .simulate('change', { target: { checked: true } })

    act(() => {
      testRender
        .find(Input)
        .at(0)
        .props()
        .onChange({
          target: {
            value: '1-test'
          }
        })
    })
    expect(onChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm InputList', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'instructionSteps'}
        instructionSteps={instructionSteps}
        authorizationType={'selected'}
        exclusiveTasters={exclusiveTasters}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
      />
    )
    expect(testRender.find(SurveyBasicInfoForm)).toHaveLength(1)

    act(() => {
      testRender
        .find(InputList)
        .at(0)
        .props()
        .onChange()
    })
    expect(onChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm InputRichText', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'instructionsText'}
        instructionsText={instructionsText}
        authorizationType={'selected'}
        exclusiveTasters={exclusiveTasters}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
      />
    )
    expect(testRender.find(SurveyBasicInfoForm)).toHaveLength(1)

    act(() => {
      testRender
        .find(InputRichText)
        .at(0)
        .props()
        .onChange()
    })
    expect(onChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm InputRichText for thankYouText', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'thankYouText'}
        thankYouText={thankYouText}
        authorizationType={'selected'}
        exclusiveTasters={exclusiveTasters}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
      />
    )
    expect(testRender.find(SurveyBasicInfoForm)).toHaveLength(1)

    act(() => {
      testRender
        .find(InputRichText)
        .at(1)
        .props()
        .onChange()
    })
    expect(onChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm InputRichText for rejectionText', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'rejectionText'}
        rejectionText={rejectionText}
        authorizationType={'selected'}
        exclusiveTasters={exclusiveTasters}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
      />
    )
    expect(testRender.find(SurveyBasicInfoForm)).toHaveLength(1)

    act(() => {
      testRender
        .find(InputRichText)
        .at(2)
        .props()
        .onChange()
    })
    expect(onChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm Input for exclusiveTasters', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'exclusiveTasters'}
        authorizationType={'selected'}
        exclusiveTasters={exclusiveTasters}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
      />
    )
    act(() => {
      testRender
        .findWhere(
          c => c.name() === 'InputEmails' && c.prop('name') === 'exclusiveTasters'
        )
        .first()
        .prop('onChange')(['taster@flavorwiki.com'])
    })
    expect(onChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm select for surveyLanguage', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'surveyLanguage'}
        authorizationType={authorizationType}
        surveyLanguage={surveyLanguage}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
        handleLanguageChange={handleLanguageChange}
      />
    )
    act(() => {
      testRender
        .findWhere(
          c => c.name() === 'Select' && c.prop('name') === 'surveyLanguage'
        )
        .first()
        .prop('onChange')(['eng', 'de'])
    })
  })

  test('should render SurveyBasicInfoForm select for surveyLanguage', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'authorizationType'}
        authorizationType={authorizationType}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
        handleLanguageChange={handleLanguageChange}
      />
    )

    act(() => {
      testRender
        .findWhere(
          c => c.name() === 'Select' && c.prop('name') === 'authorizationType'
        )
        .first()
        .prop('onChange')(['admin', 'user'])
    })
    expect(onChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm select for uniqueName', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'uniqueName'}
        authorizationType={authorizationType}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
      />
    )
    act(() => {
      testRender
        .findWhere(c => c.name() === 'Input' && c.prop('name') === 'uniqueName')
        .first()
        .prop('onChange')({ target: { value: 'Survey-1' } })
    })

    // expect(global.document.querySelector('.ant-modal-wrap')).not.toBe(null)

    // click Change button on the dialog
    // expect(
    //   global.document.querySelectorAll('.ant-modal-wrap .ant-btn')
    // ).toHaveLength(2)
    // act(() => {
    //   global.document.querySelectorAll('.ant-modal-wrap .ant-btn')[1].click()
    // })

    expect(onChange).toHaveBeenCalledWith({
      uniqueName: 'Survey-1'
    })
  })

  test('should render SurveyBasicInfoForm continue customButtons', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'customButtons.continue'}
        authorizationType={'selected'}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
        exclusiveTasters={exclusiveTasters}
      />
    )

    act(() => {
      testRender
        .findWhere(
          c =>
            c.name() === 'Input' && c.prop('name') === 'customButtons.continue'
        )
        .first()
        .prop('onChange')({ target: { customButtonsContinue: 'continue' } })
    })
    expect(handleCustomButtonChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm Start of customButtons', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'customButtons.start'}
        authorizationType={'selected'}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
        exclusiveTasters={exclusiveTasters}
      />
    )
    act(() => {
      testRender
        .findWhere(
          c => c.name() === 'Input' && c.prop('name') === 'customButtons.start'
        )
        .first()
        .prop('onChange')({ target: { customButtonsStart: 'start' } })
    })
    expect(handleCustomButtonChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm next button of customButtons', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'customButtons.next'}
        authorizationType={'selected'}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
        exclusiveTasters={exclusiveTasters}
      />
    )
    act(() => {
      testRender
        .findWhere(
          c => c.name() === 'Input' && c.prop('name') === 'customButtons.next'
        )
        .first()
        .prop('onChange')({ target: { customButtonsNext: 'next' } })
    })
    expect(handleCustomButtonChange).toHaveBeenCalled()
  })

  test('should render SurveyBasicInfoForm skip button of customButtons', async () => {
    testRender = mount(
      <SurveyBasicInfoForm
        onChange={onChange}
        name={'customButtons.skip'}
        authorizationType={'selected'}
        customButtons={customButtons}
        handleCustomButtonChange={handleCustomButtonChange}
        exclusiveTasters={exclusiveTasters}
      />
    )

    act(() => {
      testRender
        .findWhere(
          c => c.name() === 'Input' && c.prop('name') === 'customButtons.skip'
        )
        .first()
        .prop('onChange')({ target: { customButtonsSkip: 'skip' } })
    })
    expect(handleCustomButtonChange).toHaveBeenCalled()
  })
})
