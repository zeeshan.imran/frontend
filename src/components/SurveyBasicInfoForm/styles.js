import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
`

export const CheckboxContainer = styled.div`
`
