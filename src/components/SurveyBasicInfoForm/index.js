import React, { useState } from 'react'
import { Formik } from 'formik'
import { Row, Col, Form } from 'antd'
import Input from '../Input'
import Select from '../Select'
import InputRichText from '../InputRichText'
import InputList from '../InputList'
import Checkbox from '../Checkbox'
import FieldLabel from '../FieldLabel'
import toKebabCase from 'lodash.kebabcase'
import { getOptionValue } from '../../utils/getters/OptionGetters'
import { options } from '../../utils/internationalization/index'
import { useTranslation } from 'react-i18next'
import defaults from '../../defaults'
import UploadSurveyCoverPicture from '../../containers/UploadSurveyCoverPicture'

import { Container, CheckboxContainer } from './styles'
import { validateSurveyBasics } from '../../validates'
import InputEmails from '../InputEmails'
import capitalize from 'lodash.capitalize'

const SurveyBasicInfoForm = ({
  isNewSurvey,
  coverPhoto,
  onChange,
  name,
  instructionsText,
  instructionSteps,
  thankYouText,
  rejectionText,
  screeningText,
  uniqueName,
  authorizationType,
  exclusiveTasters,
  allowRetakes,
  // forcedAccount,
  // forcedAccountLocation,
  recaptcha,
  surveyLanguage,
  customButtons = {},
  handleCustomButtonChange,
  handleLanguageChange,
  isUniqueNameDuplicated,
  customizeSharingMessage,
  loginText,
  pauseText,
  allowedDaysToFillTheTasting,
  country,
  isScreenerOnly,
  optionDisplayType
}) => {
  const { t } = useTranslation()
  const [isValidate, setisValidate] = useState(false)
  const [isUniqueNameTouched, setisUniqueNameTouched] = useState(false)
  const countriesNames = defaults.countries.map(country => country || '')

  return (
    <Formik
      enableReinitialize
      validationSchema={validateSurveyBasics(isValidate)}
      initialValues={{
        name,
        uniqueName,
        authorizationType,
        exclusiveTasters,
        instructionsText,
        instructionSteps,
        thankYouText,
        rejectionText,
        screeningText,
        surveyLanguage,
        customizeSharingMessage,
        loginText,
        pauseText,
        customButtonsContinue: customButtons.continue,
        customButtonsStart: customButtons.start,
        customButtonsNext: customButtons.next,
        customButtonsSkip: customButtons.skip,
        allowedDaysToFillTheTasting,
        country,
        coverPhoto
      }}
      render={({ values, errors, setFieldValue, handleBlur }) => {
        const {
          name: nameError,
          uniqueName: uniqueNameError,
          allowedDaysToFillTheTasting: allowedDaysToFillTheTastingErrors,
          instructionSteps: instructionStepsError
        } = errors

        const exclusiveTastersError = Array.isArray(errors.exclusiveTasters)
          ? errors.exclusiveTasters.map(message => (
            <div key={message}>{message}</div>
            ))
          : errors.exclusiveTasters

        return (
          <Container>
            <Row gutter={24}>
              <Col xs={{ span: 16 }}>
                <Row gutter={24}>
                  <Col xs={{ span: 8 }}>
                    <Form.Item
                      help={nameError}
                      validateStatus={nameError ? 'error' : 'success'}
                    >
                      <Input
                        name='name'
                        value={values.name}
                        onChange={event => {
                          setFieldValue('name', event.target.value)
                          const changes = { name: event.target.value }

                          if (isNewSurvey) {
                            const date = new Date()
                            const uniqueName = !isUniqueNameTouched
                              ? event.target.value
                                ? `${toKebabCase(
                                    event.target.value
                                  )}-${date.getDate()}${date.getHours()}${date.getMinutes()}`
                                : ''
                              : values.uniqueName

                            setFieldValue('uniqueName', uniqueName)
                            changes.uniqueName = uniqueName
                          }
                          onChange(changes)
                          setisValidate(true)
                        }}
                        label={t('components.surveyBasicInfoForm.name')}
                        tooltip={t('tooltips.surveyBasicInfoForm.name')}
                        size='default'
                        required
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 8 }}>
                    <Form.Item
                      help={
                        uniqueNameError ||
                        (isUniqueNameDuplicated &&
                          t('components.surveyBasicInfoForm.uniqueNameExists'))
                      }
                      validateStatus={
                        isUniqueNameDuplicated || uniqueNameError
                          ? 'error'
                          : 'success'
                      }
                    >
                      <Input
                        disabled={!isNewSurvey}
                        tooltip={
                          isNewSurvey
                            ? t('tooltips.surveyBasicInfoForm.uniqueName')
                            : t('tooltips.surveyBasicInfoForm.staticUniqueName')
                        }
                        name='uniqueName'
                        value={values.uniqueName}
                        onChange={event => {
                          const newUniqueName = event.target.value
                          setFieldValue('uniqueName', newUniqueName)
                          setisUniqueNameTouched(!!newUniqueName)
                          onChange({ uniqueName: newUniqueName })
                        }}
                        label={t('components.surveyBasicInfoForm.uniqueName')}
                        size='default'
                        required
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 8 }} justify='center'>
                    <Select
                      data-testid='authorization-type-select'
                      name='authorizationType'
                      value={authorizationType}
                      onChange={type => {
                        onChange({ authorizationType: type })
                      }}
                      label={t(
                        'components.surveyBasicInfoForm.authorizationType'
                      )}
                      tooltip={t(
                        'tooltips.surveyBasicInfoForm.authorizationType'
                      )}
                      size='default'
                      options={['public', 'enrollment', 'selected']}
                      getOptionName={option => {
                        return capitalize(option)
                      }}
                    />
                  </Col>
                </Row>
              </Col>
              <Col xs={{ span: 8 }}>
                <UploadSurveyCoverPicture
                  isSurvey
                  onChange={photo => {
                    onChange({ coverPhoto: `${photo}` })
                  }}
                  value={values.coverPhoto}
                />
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <FieldLabel
                    label={t('components.surveyBasicInfoForm.securitySettings')}
                    tooltip={t('tooltips.surveyBasicInfoForm.securitySettings')}
                  >
                    {authorizationType !== 'selected' && (
                      <CheckboxContainer>
                        <Checkbox
                          checked={recaptcha}
                          onChange={value => {
                            onChange({ recaptcha: value })
                          }}
                        >
                          {t('components.surveyBasicInfoForm.requireRecaptcha')}
                        </Checkbox>
                      </CheckboxContainer>
                    )}
                    {/* Need to remove because we don't need forced account for now */}
                    {/* {(authorizationType === 'public' ||
                      authorizationType === 'enrollment') && (
                      <CheckboxContainer>
                        <Checkbox
                          checked={forcedAccount}
                          disabled={isScreenerOnly}
                          onChange={value => {
                            onChange({ forcedAccount: value })
                          }}
                        >
                          {t(
                            'components.surveyBasicInfoForm.forceTasterAccount'
                          )}
                        </Checkbox>
                        {forcedAccount && (
                          <Radio.Group
                            value={forcedAccountLocation}
                            onChange={e => {
                              onChange({
                                forcedAccountLocation: e.target.value
                              })
                            }}
                          >
                            <Radio.Button value={`start`}>
                              {t(
                                'components.surveyBasicInfoForm.forceAccount.start'
                              )}
                            </Radio.Button>
                            <Radio.Button value={`end`}>
                              {t(
                                'components.surveyBasicInfoForm.forceAccount.end'
                              )}
                            </Radio.Button>
                          </Radio.Group>
                        )}
                      </CheckboxContainer>
                    )} */}
                    {(authorizationType === 'selected' ||
                      authorizationType === 'enrollment') && (
                      <CheckboxContainer>
                        <Checkbox
                          checked={allowRetakes}
                          onChange={value => {
                            onChange({ allowRetakes: value })
                          }}
                        >
                          {t(
                            'components.surveyBasicInfoForm.takeSurveyMultipleTimes'
                          )}
                        </Checkbox>
                      </CheckboxContainer>
                    )}
                  </FieldLabel>
                </Form.Item>
              </Col>
            </Row>
            {authorizationType === 'selected' && (
              <Row gutter={24}>
                <Col xs={{ span: 24 }}>
                  <Form.Item
                    help={exclusiveTastersError}
                    validateStatus={exclusiveTastersError ? 'error' : 'success'}
                  >
                    <InputEmails
                      label={t('components.surveyBasicInfoForm.allowedUsers')}
                      name='exclusiveTasters'
                      value={values.exclusiveTasters}
                      onBlur={handleBlur}
                      onChange={emails => {
                        setFieldValue('exclusiveTasters', emails)
                        onChange({
                          exclusiveTasters: emails
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
              </Row>
            )}
            {authorizationType === 'enrollment' && (
              <Row gutter={8}>
                <Col xs={{ span: 8 }}>
                  <Form.Item
                    help={allowedDaysToFillTheTastingErrors}
                    validateStatus={
                      allowedDaysToFillTheTastingErrors ? 'error' : 'success'
                    }
                  >
                    <Input
                      label={t(
                        'components.surveyBasicInfoForm.allowedDaysToFillTheTasting'
                      )}
                      name='allowedDaysToFillTheTasting'
                      value={values.allowedDaysToFillTheTasting}
                      onBlur={handleBlur}
                      type={`number`}
                      onChange={event => {
                        const newAllowedDaysToFillTheTasting = parseInt(
                          event.target.value,
                          10
                        )
                        if (
                          newAllowedDaysToFillTheTasting.toString().length < 10
                        ) {
                          setFieldValue(
                            'allowedDaysToFillTheTasting',
                            newAllowedDaysToFillTheTasting
                          )
                          onChange({
                            allowedDaysToFillTheTasting: newAllowedDaysToFillTheTasting
                          })
                        }
                      }}
                      required
                      min={1}
                      max={712}
                      size={`default`}
                    />
                  </Form.Item>
                </Col>
              </Row>
            )}
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputList
                    errors={instructionStepsError}
                    label={t('components.surveyBasicInfoForm.instructionSteps')}
                    tooltip={t('tooltips.surveyBasicInfoForm.instructionSteps')}
                    name='instructionSteps'
                    value={values.instructionSteps}
                    onChange={value => {
                      setFieldValue('instructionSteps', value)
                      onChange({ instructionSteps: value })
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.instruction')}
                    tooltip={t('tooltips.surveyBasicInfoForm.instruction')}
                    name='instructionsText'
                    value={values.instructionsText}
                    onChange={value => {
                      setFieldValue('instructionsText', value)
                      onChange({ instructionsText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.thankYou')}
                    tooltip={t('tooltips.surveyBasicInfoForm.thankYou')}
                    name='thankYouText'
                    value={values.thankYouText}
                    onChange={value => {
                      setFieldValue('thankYouText', value)
                      onChange({ thankYouText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.rejection')}
                    tooltip={t('tooltips.surveyBasicInfoForm.rejection')}
                    name='rejectionText'
                    value={values.rejectionText || ''}
                    onChange={value => {
                      setFieldValue('rejectionText', value)
                      onChange({ rejectionText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.screening')}
                    tooltip={t('tooltips.surveyBasicInfoForm.screening')}
                    name='screeningText'
                    value={values.screeningText || ''}
                    onChange={value => {
                      setFieldValue('screeningText', value)
                      onChange({ screeningText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t(
                      'components.surveyBasicInfoForm.customizeSharing.title'
                    )}
                    tooltip={t('tooltips.surveyBasicInfoForm.customizeSharing')}
                    name='customizeSharingMessage'
                    value={values.customizeSharingMessage || ''}
                    onChange={value => {
                      setFieldValue('customizeSharingMessage', value)
                      onChange({ customizeSharingMessage: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.loginText')}
                    tooltip={t('tooltips.surveyBasicInfoForm.loginText')}
                    name='loginText'
                    value={values.loginText || ''}
                    onChange={value => {
                      setFieldValue('loginText', value)
                      onChange({ loginText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.pauseText')}
                    tooltip={t('tooltips.surveyBasicInfoForm.pauseText')}
                    name='pauseText'
                    value={values.pauseText || ''}
                    onChange={value => {
                      setFieldValue('pauseText', value)
                      onChange({ pauseText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 6 }}>
                <Form.Item>
                  <Select
                    name='surveyLanguage'
                    label={t('components.surveyBasicInfoForm.selectLanguage')}
                    tooltip={t('tooltips.surveyBasicInfoForm.selectLanguage')}
                    size={`default`}
                    value={surveyLanguage}
                    onChange={type => {
                      handleLanguageChange(
                        type,
                        setFieldValue,
                        'surveyLanguage'
                      )
                    }}
                    placeholder={t('components.surveyBasicInfoForm.language')}
                    options={options}
                    getOptionValue={getOptionValue}
                    getOptionName={(option = {}) =>
                      t(option.label) ? t(option.label) : option.label
                    }
                  />
                </Form.Item>
              </Col>
              <Col xs={{ span: 6 }}>
                <Form.Item>
                  <Select
                    options={countriesNames}
                    name='country'
                    value={values.country}
                    onChange={value => {
                      setFieldValue('country', value || '$')
                      onChange({ country: value || '$' })
                    }}
                    getOptionName={option => option.name}
                    getOptionValue={option => option.code}
                    label={t('components.surveyBasicInfoForm.country')}
                    tooltip={t('tooltips.surveyBasicInfoForm.country')}
                    placeholder={t('placeholders.availableInCountry')}
                    size={`default`}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 6 }}>
                <Form.Item
                  help={errors && errors.customButtonsContinue}
                  validateStatus={
                    errors && errors.customButtonsContinue ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t('components.surveyBasicInfoForm.continueButton')}
                    tooltip={t('tooltips.surveyBasicInfoForm.continueButton')}
                    name='customButtons.continue'
                    value={values.customButtonsContinue}
                    placeholder={t('defaultValues.customButtons.continue')}
                    size={`default`}
                    onChange={event => {
                      setFieldValue('customButtonsContinue', event.target.value)
                      handleCustomButtonChange('continue', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={{ span: 6 }}>
                <Form.Item
                  help={errors && errors.customButtonsStart}
                  validateStatus={
                    errors && errors.customButtonsStart ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t(
                      'components.surveyBasicInfoForm.startSurveyButton'
                    )}
                    tooltip={t(
                      'tooltips.surveyBasicInfoForm.startSurveyButton'
                    )}
                    name='customButtons.start'
                    value={values.customButtonsStart}
                    placeholder={t('defaultValues.customButtons.start')}
                    size={`default`}
                    onChange={event => {
                      setFieldValue('customButtonsStart', event.target.value)
                      handleCustomButtonChange('start', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={{ span: 6 }}>
                <Form.Item
                  help={errors && errors.customButtonsNext}
                  validateStatus={
                    errors && errors.customButtonsNext ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t('components.surveyBasicInfoForm.nextButton')}
                    tooltip={t('tooltips.surveyBasicInfoForm.nextButton')}
                    name='customButtons.next'
                    value={values.customButtonsNext}
                    placeholder={t('defaultValues.customButtons.next')}
                    size={`default`}
                    onChange={event => {
                      setFieldValue('customButtonsNext', event.target.value)
                      handleCustomButtonChange('next', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={{ span: 6 }}>
                <Form.Item
                  help={errors && errors.customButtonsSkip}
                  validateStatus={
                    errors && errors.customButtonsSkip ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t('components.surveyBasicInfoForm.skipButton')}
                    tooltip={t('tooltips.surveyBasicInfoForm.skipButton')}
                    name='customButtons.skip'
                    value={values.customButtonsSkip}
                    placeholder={t('defaultValues.customButtons.skip')}
                    size={`default`}
                    onChange={event => {
                      setFieldValue('customButtonsSkip', event.target.value)
                      handleCustomButtonChange('skip', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Container>
        )
      }}
    />
  )
}

export default SurveyBasicInfoForm
