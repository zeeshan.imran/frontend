import styled from 'styled-components'
import { Icon as AntIcon } from 'antd'
import Text from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  color: ${colors.BLACK};
  margin-bottom: 2rem;
  :hover {
    color: ${colors.LIGHT_OLIVE_GREEN};
    opacity: 1;
  }
`

export const Label = styled(Text)`
  font-size: 1.8rem;
  font-family: ${family.primaryLight};
  letter-spacing: normal;
  line-height: 1.56;
  opacity: 0.7;
  user-select: none;
`

export const Icon = styled(AntIcon)`
  font-size: 1.8rem;
  margin-right: 1.5rem;
`
