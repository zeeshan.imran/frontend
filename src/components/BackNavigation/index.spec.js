import React from 'react'
import { shallow } from 'enzyme'
import BackNavigation from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('BackNavigation', () => {
  let testRender
  let action
  let title

  beforeEach(() => {
    title = 'Back nav'
    action = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render BackNavigation', async () => {
    testRender = shallow(<BackNavigation title={title} action={action} />)
    expect(testRender).toMatchSnapshot()
  })
})
