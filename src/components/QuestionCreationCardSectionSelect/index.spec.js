import React from 'react'
import { shallow } from 'enzyme'
import QuestionCreationCardSectionSelect from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
})) 

describe('QuestionCreationCardSectionSelect', () => {
  let testRender
  let onChangeSection
  let section

  beforeEach(() => {
    onChangeSection = jest.fn()
    section = 'Test Display'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render QuestionCreationCardSectionSelect', () => {
    testRender = shallow(
      <QuestionCreationCardSectionSelect
        section={section}
        onChangeSection={onChangeSection}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
