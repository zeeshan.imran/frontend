import React from 'react'
import { Form } from 'antd'
import Select from '../Select'
import { useTranslation } from 'react-i18next'

const QuestionCreationCardSectionSelect = ({ section, onChangeSection }) =>{
  const { t } = useTranslation();
  const SURVEY_SECTIONS = {
    screening: t('components.questionCreation.questionSectionSelectOption.screening'),
    begin: t('components.questionCreation.questionSectionSelectOption.begin'),
    middle: t('components.questionCreation.questionSectionSelectOption.middle'),
    end: t('components.questionCreation.questionSectionSelectOption.end')
  }
  return (
    <Form.Item>
      <Select
        value={section}
        onChange={onChangeSection}
        options={Object.keys(SURVEY_SECTIONS)}
        getOptionName={sectionKey => SURVEY_SECTIONS[sectionKey]}
        size='default'
        label={t('components.questionCreation.questionSectionSelect')}
        placeholder={t('placeholders.questionSectionSelect')}
      />
    </Form.Item>
  )
}

export default QuestionCreationCardSectionSelect
