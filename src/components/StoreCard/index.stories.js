import React, { Component } from 'react'
import { storiesOf, action } from '@storybook/react'
import styled from 'styled-components'
import StoreCard from './'

const Container = styled.div`
  width: 19.3rem;
`
const Migros = {
  logo:
    'https://www.turkeycentral.com/uploads/company_cover/monthly_2016_06/migros-logo.png.46880231a2f5d525ff19814d0cf7373e.png'
}

const Lidl = {
  logo: 'https://cdn.worldvectorlogo.com/logos/lidl.svg'
}

class StateWrapper extends Component {
  state = {
    selected: false
  }

  render () {
    return this.props.children({
      toggleSelected: () => this.setState({ selected: !this.state.selected }),
      selected: this.state.selected
    })
  }
}

storiesOf('StoreCard', module)
  .add('Store Card With Selection', () => (
    <StateWrapper>
      {({ toggleSelected, selected }) => {
        return (
          <Container>
            <StoreCard
              logo={Migros.logo}
              withSelection
              selected={selected}
              onClick={toggleSelected}
            />
          </Container>
        )
      }}
    </StateWrapper>
  ))
  .add('Store Card Without Selection', () => (
    <Container>
      <StoreCard logo={Lidl.logo} onClick={action('pressed')} />
    </Container>
  ))
