import React from 'react'
import PropTypes from 'prop-types'
import BaseCard from '../BaseCard'
import { Container, Logo } from './styles'

const StoreCard = ({ logo, onClick, withSelection, selected }) => (
  <Container>
    <BaseCard
      noPadding
      onClick={onClick}
      withSelection={withSelection}
      selected={selected}
    >
      <Logo src={logo} />
    </BaseCard>
  </Container>
)

StoreCard.propTypes = {
  logo: PropTypes.string,
  withSelection: PropTypes.bool,
  selected: PropTypes.bool,
  onClick: PropTypes.func
}

StoreCard.defaultProps = {
  withSelection: false
}

export default StoreCard
