import React from 'react'
import { shallow } from 'enzyme'
import StoreCard from '.';
import FlavorWikiLogo from '../../assets/png/FlavorWiki_Full_Logo.png'


window.matchMedia = jest.fn().mockImplementation(query => {
    return {
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(),
        removeListener: jest.fn(),
    };
});

describe('StoreCard', () => {
    let testRender
    let logo
    let onClick
    let withSelection
    let selected

    beforeEach(() => {
        logo = FlavorWikiLogo
        onClick = jest.fn()
        withSelection = true
        selected = true

    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render StoreCard', async () => {


        testRender = shallow(
            
                <StoreCard
                    logo={logo}
                    onClick={onClick}
                    withSelection={withSelection}
                    selected={selected}
                />
            
        )
        expect(testRender).toMatchSnapshot()
    })
});