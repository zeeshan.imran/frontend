import styled from 'styled-components'
import { Alert as AntAlert } from 'antd'
import Text from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Alert = styled(AntAlert)`
  margin-bottom: 1.6rem;
`

export const Counter = styled(Text)`
  color: ${colors.LIGHT_OLIVE_GREEN};
`

export const AlertMessage = styled(Text)`
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  color: rgba(0, 0, 0, 0.65);
  line-height: 2.2rem;
  margin-right: 0.9rem;
`
