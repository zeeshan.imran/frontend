import React, { Fragment } from 'react'
import { Icon } from 'antd'
import { Alert, AlertMessage, Counter } from './styles'

const SelectedItemsBanner = ({ selectedItems }) => (
  <Alert
    message={
      <Fragment>
        <AlertMessage>Selected</AlertMessage>
        <Counter>{selectedItems}</Counter>
      </Fragment>
    }
    type='success'
    icon={<Icon type='info-circle' />}
    showIcon
  />
)

export default SelectedItemsBanner
