import styled from 'styled-components'
import { Form } from 'antd'
import Button from '../Button'

export const CustomForm = styled(Form)``

export const FormItem = Form.Item

export const Container = styled.div`
  width: 100%;
`

export const ButtonAligner = styled.div`
  margin-top: 3.5rem;
  margin-bottom: ${({ desktop }) => (desktop ? 0 : `5rem`)};
`

export const StyledButton = styled(Button)`
  width: 100%;
`
