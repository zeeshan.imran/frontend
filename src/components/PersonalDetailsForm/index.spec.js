import React from 'react'
import { mount } from 'enzyme'
import PersonalDetailsForm from '.'
import Select from '../Select'
import {
  getOptionValue,
  getOptionName
} from '../../utils/getters/OptionGetters'
import DatePicker from '../DatePicker'
import { ButtonAligner } from './styles'
import UploadPictureWithButton from '../UploadPictureWithButton'
import { Form } from 'antd'
import { withTranslation } from 'react-i18next'

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: text => text }),
  withTranslation: () => (component) => component
}))

describe('PersonalDetailsForm', () => {
  let testRender
  let form
  let desktop
  let submitForm
  let firstname
  let lastname
  let email
  let birthday 
  let imageUrl
  let language
  let gender
  let genders
  let languages

  beforeEach(() => {
    form = {
      getFieldDecorator: jest.fn(() => a => a),
      isFieldsTouched: jest.fn(() => a => a),
      setFieldsValue: jest.fn(() => a => a),
      getFieldError: jest.fn(() => a => a) 
    }
    desktop = null
    submitForm = jest.fn()
    firstname = 'flover'
    lastname = 'wiki'
    email = 'floverwiki@gmail.com'
    birthday = '01-01-1993'
    imageUrl = ''
    language - 'english'
    gender = 'male'
    genders = ['male', 'female']
    languages = ['eng', 'gn']
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PersonalDetailsForm', async () => {
    testRender = mount(
      <PersonalDetailsForm
        form={form}
        desktop={desktop}
        submitForm={submitForm}
        firstname={firstname}
        lastname={lastname}
        email={email}
        birthday={birthday}
        imageUrl={imageUrl}
        language={language}
        gender={gender}
        genders={genders}
        languages={languages}
      />
    )
    expect(testRender.find(PersonalDetailsForm)).toHaveLength(1)

    testRender.find(Form).simulate('submit')

    expect(submitForm).toHaveBeenCalled()
  })

  test('should render with Select without crash', () => {
    testRender = mount(
      <Select
        options={languages}
        getOptionName={getOptionName}
        getOptionValue={getOptionValue}
        label={'Language'}
        placeholder={'Select Language'}
        desktop={desktop}
      />
    )

    expect(testRender.find(Select)).toHaveLength(1)
  })

  test('should render with DatePicker without crash', () => {
    testRender = mount(
      <DatePicker desktop={desktop} type={'date'} label={'Date of Birth'} />
    )

    expect(testRender.find(DatePicker)).toHaveLength(1)
  })

  test('should render with ButtonAligner without crash', () => {
    testRender = mount(<ButtonAligner desktop={desktop} />)
    expect(testRender.find(ButtonAligner)).toHaveLength(1)
  })

  test('should handle before upload', async () => {
    const mockSubmitForm = jest.fn()
    testRender = mount(
      <PersonalDetailsForm
        desktop={desktop}
        imageUrl={imageUrl}
        submitForm={mockSubmitForm}
      />
    )

    var file = new File([], { type: 'image/png' })
    // not sure - this had to be a blob because i get an error that the argument of handleBeforeUpload has to be a blob, but i think i used it wrongly

    testRender
      .find(UploadPictureWithButton)
      .props()
      .handleBeforeUpload(file)
    /// nothing important, just find the function and call it. Using consoles.log in the handleBeforeUpload, i can see that it goes to the end of the function despite the warning regarding the blob.

    const setFieldsValue = jest.spyOn(testRender.instance(), 'setFieldsValue')
    // this had to spy on the function that is executed at the end of the handleBeforeUpload, but the spy doesn't work

    expect(setFieldsValue).not.toHaveBeenCalled()
    // just testing if the spy was called - but it not called
  })
})
