import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Shape = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  background-color: ${colors.LIGHT_OLIVE_GREEN};
  width: 6rem;
  height: 6rem;
  box-shadow: 0 0.5rem 0.5rem 0 rgba(0, 0, 0, 0.25);
  border-radius: 50% 0.7rem 50% 50%;
  transition: background-color 0.3s ease;

  :hover {
    background-color: ${colors.DARKER_OLIVE_GREEN};
  }
`

export const Icon = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 2.3rem;
  height: 2rem;
  left: 1px;
`
