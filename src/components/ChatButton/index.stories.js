import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import ChatButton from './'

storiesOf('ChatButton', module).add('Button', () => (
  <ChatButton onClick={action('pressed')} />
))
