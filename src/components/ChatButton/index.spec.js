import React from 'react'
import { mount } from 'enzyme'
import ChatButton from '.'
import { Shape } from './styles'
import { act } from 'react-dom/test-utils'

describe('ChatButton', () => {
    let testRender
    let onClick

    beforeAll(() => {
        onClick = jest.fn()
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render ChatButton', async () => {
        testRender = mount(<ChatButton onClick={onClick} />)
        act(() => {
            testRender.find(Shape).prop('onClick')()
        })
        expect(onClick).toHaveBeenCalled()
    })
})