import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import BaseCard from './'

class StateWrapper extends Component {
  state = {
    selected: false
  }

  render () {
    return this.props.children({
      toggleSelected: () => this.setState({ selected: !this.state.selected }),
      selected: this.state.selected
    })
  }
}

storiesOf('BaseCard', module)
  .add('Base Card Without Selection', () => (
    <BaseCard onClick={action('pressed')} />
  ))
  .add('Base Card With Selection', () => (
    <StateWrapper>
      {({ toggleSelected, selected }) => {
        return (
          <BaseCard
            selected={selected}
            onClick={toggleSelected}
            withSelection
          />
        )
      }}
    </StateWrapper>
  ))
