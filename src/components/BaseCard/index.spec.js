import React from 'react'
import { mount } from 'enzyme'
import BaseCard from '.'
import { Container } from '../FieldLabel/styles'
import { Card } from './styles'

describe('BaseCard', () => {
  let testRender
  let children
  let withSelection
  let selected
  let onClick
  let lockedCursor

  beforeEach(() => {
    children = <Container>Basecard child</Container>
    withSelection = false
    selected = true
    onClick = jest.fn()
    lockedCursor = 'not - allowed'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render BaseCard', async () => {
    testRender = mount(
      <BaseCard
        children={children}
        withSelection={withSelection}
        selected={selected}
        onClick={onClick}
        lockedCursor={lockedCursor}
      />
    )
    expect(testRender.find(BaseCard)).toHaveLength(1)
  })

  test('should render BaseCard onclick', async () => {
    testRender = mount(
      <BaseCard onClick={onClick} lockedCursor={lockedCursor} />
    )
    testRender.find(Card).prop('onClick')()

    expect(onClick).toHaveBeenCalled()
  })
})
