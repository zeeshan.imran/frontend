import React from 'react'
import { Card, SelectionDot, CheckedIcon } from './styles'
import useResponsive from '../../utils/useResponsive/index'

const BaseCard = ({
  children,
  withSelection = false,
  selected,
  onClick,
  lockedCursor,
  checkbox = false,
  clickDisabled = false,
  checkType
}) => {
  const { desktop, mobile } = useResponsive()
  return (
    <Card
      desktop={desktop}
      mobile={mobile}
      lockedCursor={lockedCursor}
      onClick={!clickDisabled ? onClick : () => {}}
    >
      {withSelection && (
        <SelectionDot checkbox={checkbox} selected={selected}>
          {(checkType && selected) ? (
            <CheckedIcon type={checkType} />
          ) : null}
        </SelectionDot>
      )}
      {children}
    </Card>
  )
}

export default BaseCard
