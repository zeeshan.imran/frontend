import React from 'react'
import { shallow } from 'enzyme'
import OnboardingForm from '.'
import { Container } from '../MyProfileDropdown/styles'

describe('OnboardingForm', () => {
  let testRender
  let children
  let formProps

  beforeEach(() => {
    children=<Container>Onboard form</Container>
    formProps= {}

  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OnboardingForm', async () => {
    testRender = shallow(
      <OnboardingForm children={children} formProps={formProps} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
