import styled from 'styled-components'
import { Form } from 'antd'

export const StyledForm = styled(Form)`
  width: 100%;
  margin-top: 3.5rem;
`
