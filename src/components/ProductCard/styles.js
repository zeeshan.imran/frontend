import styled from 'styled-components'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  text-align: center;
  user-select: none;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Title = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 1.5rem;
  color: ${colors.SLATE_GREY};
  `

export const Image = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
`

export const Picture = styled(Image)`
  width: 100%;
  height: 100%;
`

export const NoImage = styled.span`
  display: block;
  margin-top: 50%;
`
