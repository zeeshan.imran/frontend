import React from 'react'
import PropTypes from 'prop-types'
import BaseCard from '../BaseCard'
import Overlay from './Overlay'
import { Container, Picture, Title, NoImage } from './styles'
import { useTranslation } from 'react-i18next'

const handleClick = ({ onClick, locked, maxRanking, ranking }) => {
  if (locked) return
  if (maxRanking) {
    return onClick((ranking + 1) % (maxRanking + 1))
  }
  onClick()
}

const ProductCard = ({
  name,
  image,
  withSelection,
  selected,
  locked,
  maxRanking,
  ranking,
  onClick,
  mobile,
  reward,
  selectionDisable
}) => {
  const { t } = useTranslation()
  const shouldRenderOverlay = maxRanking || locked

  return (
    <Container mobile={mobile} locked={locked}>
      <BaseCard
        onClick={() => handleClick({ onClick, locked, maxRanking, ranking })}
        withSelection={withSelection}
        selected={selected}
        lockedCursor={locked || selectionDisable}
      >
        {!shouldRenderOverlay && selectionDisable && (
          <Overlay
            selectionDisable={selectionDisable}
            productDisableText={t(
              'components.productDisplayOptions.productDisable'
            )}
          />
        )}
        {shouldRenderOverlay && (
          <Overlay maxRanking={maxRanking} ranking={ranking} locked={locked} />
        )}
        {image ? (
          <Picture src={image} />
        ) : (
          <NoImage>{t('components.question.chooseProduct.noImage')}</NoImage>
        )}
      </BaseCard>
      <Title>{name}</Title>
      {reward && (
        <Title>
          {t('components.question.chooseProduct.reward') + ' ' + reward}
        </Title>
      )}
    </Container>
  )
}

ProductCard.propTypes = {
  name: PropTypes.string,
  image: PropTypes.string,
  selected: PropTypes.bool,
  withSelection: PropTypes.bool,
  locked: PropTypes.bool,
  ranking: PropTypes.number,
  maxRanking: PropTypes.number,
  onClick: PropTypes.func
}

ProductCard.defaultProps = {
  withSelection: false,
  locked: false
}

export default ProductCard
