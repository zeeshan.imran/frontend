import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const Image = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
`

export const Lock = styled(Image)`
  width: 3.88rem;
  height: 5.49rem;
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  border-radius: 1rem;
  background-color: rgba(78, 78, 86, 0.6);
`

export const Ranking = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 4.8rem;
  color: ${colors.WHITE};
`

export const AnswerLastPrductToUnlock = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 1.2rem;
  color: ${colors.WHITE};
`