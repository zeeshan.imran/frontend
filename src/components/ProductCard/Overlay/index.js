import React from 'react'
import LockIcon from '../../../assets/svg/lock.svg'
import { Container, Ranking, Lock, AnswerLastPrductToUnlock } from './styles'

const Overlay = ({
  maxRanking,
  ranking,
  locked,
  selectionDisable,
  productDisableText
}) => (
  <Container>
    {selectionDisable && productDisableText && (
      <AnswerLastPrductToUnlock>{productDisableText}</AnswerLastPrductToUnlock>
    )}
    {maxRanking && <Ranking>{ranking}</Ranking>}
    {locked && <Lock src={LockIcon} />}
  </Container>
)

export default Overlay
