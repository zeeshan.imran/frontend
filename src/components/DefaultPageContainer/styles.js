import styled from 'styled-components'
import { DEFAULT_MOBILE_PADDING } from '../../utils/Metrics'

export const Container = styled.div`
  padding: ${({ desktop }) =>
    desktop ? '5rem 5rem 8rem 5rem' : `5rem ${DEFAULT_MOBILE_PADDING}rem`};
`
