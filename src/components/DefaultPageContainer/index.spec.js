import React from 'react'
import { shallow } from 'enzyme'
import DefaultPageContainer from '.';

describe('DefaultPageContainer', () => {
    let testRender
    let children
    
    beforeEach(() => {
        children = '<Container>Defualt page</Container>'
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render DefaultPageContainer', async () => {


        testRender = shallow(

            <DefaultPageContainer
                children={children}
            />

        )
        expect(testRender).toMatchSnapshot()
    })
});