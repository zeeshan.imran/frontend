import React from 'react'
import { Title, Container } from './styles'

const SettingsGroup = ({ title, children, className }) => (
  <Container className={className}>
    <Title>{title}</Title>
    {children}
  </Container>
)

export default SettingsGroup
