import styled from 'styled-components'
import Text from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'
import { COMPONENTS_DEFAULT_MARGIN } from '../../utils/Metrics'

export const Title = styled(Text)`
  display: flex;
  color: ${colors.PALE_GREY};
  font-family: ${family.primaryRegular};
  font-size: 1.3rem;
  letter-spacing: 0.05rem;
  margin-bottom: ${COMPONENTS_DEFAULT_MARGIN}rem;
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 5rem;
`
