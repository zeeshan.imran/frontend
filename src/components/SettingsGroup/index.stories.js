import React from 'react'
import { storiesOf } from '@storybook/react'
import SettingsGroup from './'
import ToggableSetting from '../ToggableSetting'
import Email from '../SvgIcons/Email'

storiesOf('SettingsGroup', module).add('Story', () => (
  <SettingsGroup title='Marketing Notifications'>
    <ToggableSetting
      handleChange={checked => console.log('setting checked', checked)}
      title='Marketing emails'
      description='Businesses often become known today through effective marketing. '
      icon={<Email />}
    />
  </SettingsGroup>
))
