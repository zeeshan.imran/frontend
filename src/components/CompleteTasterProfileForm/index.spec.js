import React from 'react'
import { mount } from 'enzyme'
import CompleteTasterProfileForm from '.'
import { createBrowserHistory } from 'history'
import gql from 'graphql-tag'
import { Router } from 'react-router-dom'
import { MockedProvider } from 'react-apollo/test-utils'
import { SubmitButton } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const query = gql`
  query hello {
    hello
  }
`

const mocks = [
  {
    request: { query },
    result: { data: { hello: 'world' } }
  }
]

describe('CompleteTasterProfileForm', () => {
  let testRender
  let fullName
  let birthYear
  let gender
  let nationality
  let errors
  let touched
  let fieldChangeHandler
  let dropdownChangeHandler
  let handleSubmit
  let setSubmitting
  let loading
  let isValid

  beforeEach(() => {
    fullName = 'Test'
    birthYear = 1993
    gender = 'Male'
    nationality = 'Usa'
    errors = {}
    touched = false
    fieldChangeHandler = jest.fn()
    dropdownChangeHandler = jest.fn()
    handleSubmit = jest.fn()
    setSubmitting = jest.fn()
    loading = false
    isValid = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CompleteTasterProfileForm', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <MockedProvider mocks={mocks}>
        <Router history={history}>
          <CompleteTasterProfileForm
            fullName={fullName}
            birthYear={birthYear}
            gender={gender}
            nationality={nationality}
            errors={errors}
            touched={touched}
            fieldChangeHandler={fieldChangeHandler}
            dropdownChangeHandler={dropdownChangeHandler}
            handleSubmit={handleSubmit}
            setSubmitting={setSubmitting}
            loading={loading}
            isValid={isValid}
          />
        </Router>
      </MockedProvider>
    )
    expect(testRender.find(CompleteTasterProfileForm)).toHaveLength(1)
  })

  test('should render on submit form of CompleteTasterProfileForm', async () => {
    testRender = mount(
      <CompleteTasterProfileForm
        fullName={fullName}
        birthYear={birthYear}
        gender={gender}
        nationality={nationality}
        errors={errors}
        touched={touched}
        fieldChangeHandler={fieldChangeHandler}
        dropdownChangeHandler={dropdownChangeHandler}
        handleSubmit={handleSubmit}
        setSubmitting={setSubmitting}
      />
    )

    testRender
      .find(SubmitButton)
      .props()
      .onClick()

    expect(handleSubmit).toHaveBeenCalled()
  })
})
