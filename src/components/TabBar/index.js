import React, { useEffect, useRef } from 'react'
import { Tabs } from './styles'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'
import TooltipWrapper from '../TooltipWrapper'

const TabPane = Tabs.TabPane

const TabBar = ({ activeTab, onTabSelection, tabs, action }) => {
  const currentActiveTab = useRef()

  useEffect(() => {
    if (
      activeTab &&
      currentActiveTab &&
      activeTab !== currentActiveTab.current
    ) {
      currentActiveTab.current = activeTab
      setTimeout(() => PubSub.publish(PUBSUB.VALIDATE_SURVEY_QUESTIONS), 100)
    }
  })

  return (
    <Tabs
      activeKey={activeTab}
      onChange={onTabSelection}
      tabBarExtraContent={action}
    >
      {tabs.map(tab => (
        <TabPane
          data-testid={`${tab.key}-tab`}
          tab={
            <TooltipWrapper helperText={tab.tooltip} isTab>
              {tab.title}
            </TooltipWrapper>
          }
          key={tab.key}
        />
      ))}
    </Tabs>
  )
}
 
export default TabBar
