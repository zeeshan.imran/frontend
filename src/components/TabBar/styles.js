import styled from 'styled-components'
import { Tabs as AntTabs } from 'antd'
import colors from '../../utils/Colors'

export const Tabs = styled(AntTabs)`
  background-color: ${colors.WHITE};
  margin-bottom: 2.5rem;
  .ant-tabs-bar {
    margin: 0;
  }
  .ant-tabs-extra-content {
    margin-right: 1.5rem;
  }

  .ant-tabs-nav .ant-tabs-tab {
    padding: 0;
  }
`
