import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import TooltipWrapper from '../../components/TooltipWrapper'
import { Avatar, Icon, Upload, IconAligner, RemoveIconAligner } from './styles'

const UploadOptionsPicture = ({
  name,
  image,
  onAddImageUrl,
  uploadStyle = {},
  onRemoveImageUrl,
  removeButton = true
}) => {
  const { t } = useTranslation()
  const [loading, setLoading] = useState(false)
  const handleBeforeUpload = async file => {
    setLoading(true)
    await onAddImageUrl({ [name]: file })
    setLoading(false)
    return false
  }

  const handleRemove = async () => {
    await onAddImageUrl({ [name]: '' })
  }

  return (
    <React.Fragment>
      <Upload
        name='avatar'
        accept='.jpg,.jpeg,.png'
        showUploadList={false}
        listType='picture-card'
        className='avatar-uploader'
        onRemove={handleRemove}
        beforeUpload={handleBeforeUpload}
        customRequest={() => {}}
        uploadStyle={uploadStyle}
      >
        {image && !loading ? (
          <Avatar src={image} alt={'avatar'} />
        ) : (
          <IconAligner>
            <Icon type={loading ? 'loading' : 'plus'} />
          </IconAligner>
        )}
      </Upload>
      {image && removeButton && (
        <RemoveIconAligner uploadStyle={uploadStyle}>
          <TooltipWrapper
            helperText={t('tooltips.deletePicture')}
            placement='right'
          >
            <Icon
              type='delete'
              onClick={e => {
                onRemoveImageUrl()
                e.stopPropagation()
              }}
            />
          </TooltipWrapper>
        </RemoveIconAligner>
      )}
    </React.Fragment>
  )
}

export default UploadOptionsPicture
