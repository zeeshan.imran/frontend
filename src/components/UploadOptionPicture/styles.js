import styled from 'styled-components'
import { Upload as AntUpload, Icon as AntIcon } from 'antd'
import colors from '../../utils/Colors'

export const Upload = styled(AntUpload)`
  .ant-upload {
    height: 10rem;
    width: 10rem;
    ${({ uploadStyle }) =>
      uploadStyle && uploadStyle.width && uploadStyle.height
        ? `height: ${uploadStyle.height}; width: ${uploadStyle.width};`
        : ``}
  }
`

export const Avatar = styled.div`
  display: flex;
  background: url(${({ src }) => src}) no-repeat center;
  background-size: contain;
  border-radius: ${({ rounded }) => (rounded ? '50%' : '0%')};
  width: 100%;
  height: 100%;
`

export const Icon = styled(AntIcon)`
  font-size: 2.4rem;
`

export const IconAligner = styled.div`
  display: flex;
  flex-direction: column;
`

export const ImageRemoveButton = styled.span`
  color: ${colors.RED};
  text-decoration: underline;
  :hover {
    cursor:pointer;
   }
}`

export const RemoveIconAligner = styled.div`
  height: 30px;
  width: 30px;
  position: absolute;
  top: 3px;
  left: ${({ uploadStyle }) =>
    uploadStyle && uploadStyle.width
      ? `calc(${uploadStyle.width} + 15px)`
      : 'calc(10rem + 15px)'};
  cursor: pointer;
`
