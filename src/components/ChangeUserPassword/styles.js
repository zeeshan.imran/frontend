import styled from 'styled-components'
import { Form, Col } from 'antd'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'
import Text from '../Text'
import Button from '../Button'
import {
  DEFAULT_MOBILE_MARGIN,
  COMPONENTS_DEFAULT_MARGIN
} from '../../utils/Metrics'

export const FormItem = Form.Item

export const Container = styled.div`
  width: 100%;
`

export const ButtonAligner = styled.div`
  margin-top: ${COMPONENTS_DEFAULT_MARGIN}rem;
`

export const PasswordPrompt = styled(Text)`
  display: flex;
  color: ${({ error }) => (error ? colors.ERROR : colors.BLACK)};
  opacity: ${({ error }) => (error ? 1 : 0.5)};
  font-family: ${({ error }) => (error ? family.primaryBold : family.primaryRegular)};
  font-size: 1.4rem;
  margin-bottom: 5rem;
`

export const CurrentPassword = styled(Form.Item)`
  margin-bottom: 1rem;
`

export const CustomCol = styled(Col)`
  display: flex;
  margin-bottom: ${({ desktop }) =>
    desktop ? 0 : `${DEFAULT_MOBILE_MARGIN}rem`};
`

export const StyledButton = styled(Button)`
  width: 100%;
`
