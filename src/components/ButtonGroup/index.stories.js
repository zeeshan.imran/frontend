import React from 'react'
import { storiesOf } from '@storybook/react'
import ButtonGroup from './'

storiesOf('ButtonGroup', module).add('Story', () => (
  <ButtonGroup
    defaultSelected={'Button1'}
    buttonLabels={['Button1', 'Button2', 'Button3', 'Button4']}
    handleChange={e => console.log(`Selected: ${e.target.value}`)}
  />
))
