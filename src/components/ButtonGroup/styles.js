import styled from 'styled-components'
import { Radio } from 'antd'

export const Group = Radio.Group

export const Button = styled(Radio.Button)`
  user-select: none;
`
