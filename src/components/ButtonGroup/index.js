import React from 'react'
import { Group, Button } from './styles'

const ButtonGroup = ({ handleChange, buttonLabels, defaultSelected }) => (
  <Group onChange={handleChange} defaultValue={defaultSelected}>
    {buttonLabels.map(label => (
      <Button key={`buttonGroup-${label}`} value={label}>
        {label}
      </Button>
    ))}
  </Group>
)

export default ButtonGroup
