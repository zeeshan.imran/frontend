import React from 'react'
import { Table as AntTable } from 'antd'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../../utils/Constants'
import './index.css'
import { withTranslation } from 'react-i18next'

import { ColumnContentAligner, Action, Separator } from './styles'

const Table = ({
  users, 
  handleChangeSelection,
  handleRemove,
  toggleEditUserForm,
  setModalType,
  page,
  userCount,
  onTableChange,
  currentUserId,
  t,
  orderBy,
  orderDirection,
  isAdmin,
  history,
  location
}) => {
  const KEEP_ROUTERS = [
    '/operator/dashboard',
    '/operator/surveys',
    '/operator/terms-of-use',
    '/operator/privacy-policy'
  ]
  const getImpersonatePath = (impersonateUserId, pathname = '/') => {
    const shouldKeepUrl = KEEP_ROUTERS.includes(pathname)
    return `/impersonate/${impersonateUserId}?redirect=${
      shouldKeepUrl ? pathname : '/'
    }`
  }
  
  const nameColumn = {
    title: t('components.usersList.table.name'),
    dataIndex: 'fullName',
    width: "20%",
    render: (_, rowData) => (
      <span>
        {rowData.firstName && rowData.lastName
          ? `${rowData.firstName} ${rowData.lastName}`
          : rowData.fullName}
      </span>
    ),
    sorter: () => false,
    className: 'column'
  }

  const emailColumn = {
    title: t('components.usersList.table.email'),
    dataIndex: 'emailAddress',
    width: "20%",
    sorter: () => false,
    className: 'column'
  }

  const organizationColumn = {
    title: t('components.usersList.table.organization'),
    dataIndex: 'organization.name',
    sorter: () => false,
    width: "20%",
    className: 'column'
  }

  const userTypeColumn = {
    title: t('components.usersList.table.type'),
    dataIndex: 'type',
    width: "20%",
    sorter: () => false
  }

  let columns = [nameColumn, emailColumn]
  if (isAdmin) {
    columns.push(organizationColumn)
  }

  columns = [
    ...columns,
    userTypeColumn,
    {
      title: t('components.usersList.table.actions'),
      dataIndex: '',
      width: "20%",
      render: (_, rowData) => (
        <ColumnContentAligner fixed='210px'>
          {isAdmin && currentUserId !== rowData.id && ['operator', 'taster', 'power-user'].includes(rowData.type) && (
            <React.Fragment>
              <Action
                onClick={async () => {
                  history.replace(
                    getImpersonatePath(rowData.id, location.pathname)
                  )
                }}
              >
                {t(`components.impersonate.impersonate`, { user: '' })}
              </Action>
              <Separator />
            </React.Fragment>
          )}
          <Action
            onClick={() => {
              setModalType('edit')
              toggleEditUserForm(rowData.id)
            }}
          >
            {t('components.usersList.table.edit')}
          </Action>
          {currentUserId !== rowData.id && (
            <React.Fragment>
              <Separator />
              <Action onClick={() => handleRemove(rowData.id)}>
                {t('components.usersList.table.delete')}
              </Action>
            </React.Fragment>
          )}
        </ColumnContentAligner>
      )
    }
  ]

  columns = columns.map(col => ({
    ...col,
    sortOrder: col.dataIndex === orderBy ? orderDirection : undefined
  }))

  return (
    <AntTable
      key='usersTable'
      rowKey={record => record.id}
      columns={columns}
      pagination={
        userCount > 10
          ? {
            pageSize: DEFAULT_N_ELEMENTS_PER_PAGE,
            total: userCount,
            current: parseInt(page, 10)
          }
          : false
      }
      onChange={(paginationConfig, filtersConfig, sortingConfig) => {
        onTableChange(
          paginationConfig.current,
          sortingConfig.field,
          sortingConfig.order
        )
      }}
      dataSource={users}
    />
  )
}

export default withTranslation()(Table)
