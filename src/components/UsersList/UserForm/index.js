import React, { useEffect, useRef, useState } from 'react'
import * as Yup from 'yup'
import { Formik } from 'formik'
import { Modal, Form, Row, Col } from 'antd'
import Input from '../../Input'
import Select from '../../Select'
import { useTranslation } from 'react-i18next'
import { Title, StyledCheckbox } from './styles.js'

const UserForm = ({
  isEditUser,
  handelAdd,
  toggleEditUserForm,
  newUserData,
  setnewUserData,
  handleEdit,
  userTypes,
  isAdmin,
  organizationsList,
  currentUserId,
  setEnableSubmit,
  enableSubmit,
  emailAlreadyInUse,
  modalType
}) => {
  const capitalize = text => text.charAt(0).toUpperCase() + text.slice(1)
  const { t } = useTranslation()

  if (
    organizationsList &&
    organizationsList.length === 1 &&
    !newUserData.organization
  ) {
    setnewUserData({
      ...newUserData,
      organization: organizationsList[0].id
    })
  }

  const formRef = useRef()
  const [isMounted, setIsMounted] = useState()
  const [isTouched, setIsTouched] = useState(modalType !== 'add')
  const [enableEmail,] = useState( isEditUser !== true)

  const setData = ({ ...newData }) => {
    if (!isTouched) {
      setIsTouched(true)
    }
    setnewUserData(newData)
  }

  useEffect(() => {
    if (!isMounted && formRef.current && formRef.current.validateForm) {
      setIsMounted(true)
      formRef.current.validateForm()
    } else if (!isMounted) {
      setIsMounted(false)
    }
  })

  return (
    <Modal
      title={
        modalType === 'add' ? (
          <Title>{t('components.userList.addFormTitle')}</Title>
        ) : (
          <Title>{t('components.userList.editFormTitle')}</Title>
        )
      }
      visible={!!isEditUser}
      onOk={isEditUser === true ? handelAdd : handleEdit}
      okButtonProps={{ disabled: !enableSubmit }}
      onCancel={() => toggleEditUserForm(false)}
    >
      <Formik
        ref={formRef}
        enableReinitialize
        validationSchema={Yup.object().shape({
          emailAddress: Yup.string()
            .test(
              'max-length',
              t('validation.userForm.email.maxLength'),
              value => value && value.length <= 50
            )
            .email(t('validation.email.email'))
            .required(t('validation.userForm.email.required')),
          fullName: Yup.string()
            .test(
              'max-length',
              t('validation.userForm.fullName.maxLength'),
              value => value && value.length <= 50
            )
            .test(
              'no-spaces',
              t('validation.userForm.fullName.required'),
              value => !/^[ ]+$/.test(value)
            )
            .required(t('validation.userForm.fullName.required'))
            .test(
              'not-empty',
              t('validation.userForm.fullName.nameWithoutSpaces'),
              value => /^(?! ).*/.test(value)
            ),
          type: Yup.string(),
          organization: Yup.string()
            .typeError(t('validation.userForm.organization.typeError'))
            .nullable()
        })}
        initialValues={{
          ...newUserData
        }}
        render={({ errors, setFieldValue, values }) => {
          if (emailAlreadyInUse) {
            errors['emailAddress'] = emailAlreadyInUse
          }
          return (
            <React.Fragment>
              <Row>
                <Form.Item
                  help={isTouched ? errors.emailAddress : null}
                  validateStatus={
                    isTouched && errors.emailAddress ? 'error' : 'success'
                  }
                >
                  <Input
                    name='Email Address'
                    value={values.emailAddress}
                    onBlur={event => {
                      setEnableSubmit(errors, newUserData)
                    }}
                    onChange={event => {
                      setFieldValue('emailAddress', event.target.value)
                      setEnableSubmit(errors, newUserData)
                      setData({
                        ...newUserData,
                        emailAddress: event.target.value
                      })
                    }}
                    label={t('components.usersList.emailAddress')}
                    placeholder={t('placeholders.emailAddress')}
                    size='default'
                    required
                    disabled={enableEmail}
                  />
                </Form.Item>
              </Row>
              <Row>
                <Form.Item
                  help={isTouched ? errors.fullName : null}
                  validateStatus={
                    isTouched && errors.fullName ? 'error' : 'success'
                  }
                >
                  <Input
                    name='Full name'
                    value={values.fullName}
                    onBlur={event => {
                      setEnableSubmit(errors, newUserData)
                    }}
                    onChange={event => {
                      setFieldValue('fullName', event.target.value)
                      setEnableSubmit(errors, newUserData)
                      setData({
                        ...newUserData,
                        fullName: event.target.value
                      })
                    }}
                    label={t('components.usersList.fullName')}
                    placeholder={t('placeholders.fullName')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Row>
              {isAdmin && (
                <Row>
                  <Form.Item
                    help={isTouched ? errors.organization : null}
                    validateStatus={
                      isTouched && errors.organization ? 'error' : 'success'
                    }
                  >
                    <Select
                      label={t('components.usersList.selectOrganization')}
                      value={values.organization}
                      onChange={change => {
                        setFieldValue('organization', change)
                        setData({
                          ...newUserData,
                          organization: change
                        })
                      }}
                      placeholder={t('placeholders.selectOrganization')}
                      options={organizationsList}
                      getOptionName={organization => organization.name}
                      getOptionValue={organization => organization.id}
                      showSearch
                      optionFilterProp={`children`}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              )}
              {currentUserId !== values.id && (
                <Row>
                  <Form.Item
                    help={isTouched ? errors.type : null}
                    validateStatus={
                      isTouched && errors.type ? 'error' : 'success'
                    }
                  >
                    <Select
                      label={t('components.usersList.selectType')}
                      value={values.type}
                      onChange={change => {
                        setFieldValue('type', change)
                        setData({
                          ...newUserData,
                          type: change
                        })
                      }}
                      placeholder={t('placeholders.selectType')}
                      options={userTypes}
                      getOptionName={type => capitalize(type)}
                      getOptionValue={type => type}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              )}
              {isAdmin && values.type === 'operator' && (
                <Row>
                  <Col xs={{ span: 12 }}>
                    <StyledCheckbox
                      name='isTaster'
                      checked={values.isTaster}
                      onChange={e => {
                        const { checked: value } = e.target
                        setFieldValue('isTaster', value)
                        setEnableSubmit(errors, newUserData)
                        setData({
                          ...newUserData,
                          isTaster: value
                        })
                      }}
                    >
                      Is taster
                    </StyledCheckbox>
                  </Col>
                  {currentUserId !== values.id &&
                    /[^@]+@flavorwiki.com$/i.test(values.emailAddress) && (
                      <Col xs={{ span: 12 }}>
                        <StyledCheckbox
                          name='isSuperAdmin'
                          checked={values.isSuperAdmin}
                          onChange={e => {
                            const { checked: value } = e.target
                            setFieldValue('isSuperAdmin', value)
                            setEnableSubmit(errors, newUserData)
                            setData({
                              ...newUserData,
                              isSuperAdmin: value
                            })
                          }}
                        >
                          Is super admin
                        </StyledCheckbox>
                      </Col>
                    )}
                </Row>
              )}
            </React.Fragment>
          )
        }}
      />
    </Modal>
  )
}

export default UserForm
