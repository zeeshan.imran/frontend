import styled from 'styled-components'
import { family } from '../../../utils/Fonts'
import { Checkbox } from 'antd'
import colors from '../../../utils/Colors'

export const Title = styled.span`
  font-family: ${family.primaryBold};
  font-size: 2rem;
  color: rgba(0, 0, 0, 0.85);
`
export const StyledCheckbox = styled(Checkbox)`
  max-width: 100%;

  .ant-checkbox-wrapper + span {
    max-width: 100%;
  }

  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner {
    border-color: ${colors.CHECKBOX_HOVER_COLOR};
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${colors.CHECKBOX_BACKGROUND_COLOR};
    border-color: ${colors.CHECKBOX_HOVER_COLOR};
  }
  .ant-checkbox-disabled.ant-checkbox-checked .ant-checkbox-inner::after {
    border-color: ${colors.CHECKBOX_HOVER_COLOR} !important;
  }
  .ant-checkbox-checked:after {
    border: 1px solid ${colors.CHECKBOX_DISABLE_COLOR};
  }
`
