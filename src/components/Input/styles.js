import styled from 'styled-components'
import { Input as AntInput } from 'antd'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const InputStyled = styled(AntInput)`
  font-family: ${family.NotoSansKRRegular};
  letter-spacing: 0.04rem;
  color: ${colors.BLACK};
  font-size: 1.4rem;
  caret-color: ${colors.LIGHT_OLIVE_GREEN};

  /* No need since we already have the primary color */
  /* &.ant-input:hover {
    border: 1px solid ${colors.LIGHT_OLIVE_GREEN};
  }
  &.ant-input:focus {
    border: 1px solid ${colors.LIGHT_OLIVE_GREEN};
    box-shadow: 0 0 0 2px rgba(138, 186, 92, 0.2);
  } */
`
export const InputStyledForPrefix = styled(AntInput)`
  font-family: ${family.NotoSansKRRegular};
  color: ${colors.BLACK};
  letter-spacing: 0.04rem;
  font-size: 1.4rem;
  caret-color: ${colors.LIGHT_OLIVE_GREEN};

  .ant-input:hover {
    border: 1px solid ${colors.LIGHT_OLIVE_GREEN} !important;
  }
  .ant-input:focus {
    border: 1px solid ${colors.LIGHT_OLIVE_GREEN};
    box-shadow: 0 0 0 2px rgba(138, 186, 92, 0.2);
  }

  .ant-input-prefix {
    color: rgba(0, 0, 0, 0.25);
  }
  .ant-input::-moz-placeholder {
    font-family: ${family.primaryLight};
    font-size: 1.4rem;
    color: rgba(0, 0, 0, 0.45);
    opacity: 1;
  }
  .ant-input:-ms-input-placeholder {
    font-family: ${family.primaryLight};
    font-size: 1.4rem;
    color: rgba(0, 0, 0, 0.45);
  }
  .ant-input::-webkit-input-placeholder {
    font-family: ${family.primaryLight};
    font-size: 1.4rem;
    color: rgba(0, 0, 0, 0.45);
  }
`
