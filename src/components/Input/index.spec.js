import React from 'react'
import { shallow } from 'enzyme'
import Input from '.';

describe('Input', () => {
    let testRender
    let prefix
    let label
    let required
    let size
    let fieldDecorator
    let rest

    beforeEach(() => {
        prefix= 'data'
        label = 'Enter input label here'
        required =true
        size ='large'
        fieldDecorator = jest.fn(() => a => a)
        
        rest= {
            readonly : true,
            children: '<Container>hello</Container>',
        }
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Input', async () => {
        testRender = shallow(

            <Input
                prefix={prefix}
                label={label}
                required={required}
                size={size}
                fieldDecorator={fieldDecorator}
                rest = {rest}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});