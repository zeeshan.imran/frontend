import React, { Component } from 'react'
import PropTypes from 'prop-types'
import FieldLabel from '../FieldLabel'
import { InputContainer, InputStyledForPrefix, InputStyled } from './styles'

class Input extends Component {
  render () {
    const {
      prefix,
      label,
      required,
      size,
      fieldDecorator,
      tooltip,
      tooltipPlacement,
      disabledInput = false,
      ...rest
    } = this.props
    return (
      <FieldLabel
        required={required}
        label={label}
        tooltip={tooltip}
        tooltipPlacement={tooltipPlacement}
      >
        <InputContainer>
          {fieldDecorator(
            prefix ? (
              <InputStyledForPrefix
                prefix={prefix}
                size={size || 'large'}
                {...rest}
              />
            ) : (
              <InputStyled
                disabled={disabledInput}
                size={size || 'large'}
                {...rest}
              />
            )
          )}
        </InputContainer>
      </FieldLabel>
    )
  }
}

Input.defaultProps = {
  label: '',
  fieldDecorator: Component => Component
}

Input.propTypes = {
  label: PropTypes.string,
  required: PropTypes.bool,
  fieldDecorator: PropTypes.func
}

export default Input
