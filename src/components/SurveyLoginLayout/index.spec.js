import React from 'react'
import { mount } from 'enzyme'
import { Router } from 'react-router-dom'
import { act } from 'react-dom/test-utils'
import { createBrowserHistory } from 'history'
import SurveyLoginLayout from './index'
import ResponsiveButton from '../ResponsiveButton'
import InputEmail from '../InputEmail'
import wait from '../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('components/SurveyLoginLayout', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render without crash', async () => {
    const history = createBrowserHistory()
    const submit = jest.fn()

    testRender = mount(
      <Router history={history}>
        <SurveyLoginLayout
          requireEmail
          hideSubmit={false}
          handleSubmit={submit}
        />
      </Router>
    )

    act(() => {
      testRender.find(InputEmail).prop('handleChange')('taster@gmail.com')
    })

    await wait(0)
    testRender.update()

    act(() => {
      testRender.find(ResponsiveButton).prop('onClick')()
    })

    expect(submit).toHaveBeenCalledWith('taster@gmail.com', undefined, undefined)
  })
})
