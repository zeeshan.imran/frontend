import React, { useState, useEffect } from 'react'
import { Row, Col } from 'antd'
import InputEmail from '../InputEmail'
import InputPassword from '../InputPassword'
import SelectCountry from '../SelectCountry'
import RecaptchaWidget from '../RecaptchaWidget'
import ResponsiveButton from '../ResponsiveButton'
import SectionTitle from '../SectionTitle'
import useResponsive from '../../utils/useResponsive'

import { Container, Description } from './styles'
import TermsAndPrivacyFooter from '../../containers/TermsAndPrivacyFooter'

const SurveyLoginLayout = ({
  title,
  description,
  handleSubmit,
  requireEmail,
  requireRegistration,
  requireRecaptcha,
  hideSubmit,
  buttonLabel,
  loginText,
  pauseText,
  defaultEmailValue = '',
  emailDisabled = false
}) => {
  const { mobile } = useResponsive()

  const [isRecaptchaValid, setIsRecaptchaValid] = useState(!requireRecaptcha)
  const [isEmailValid, setIsEmailValid] = useState(
    defaultEmailValue ? true : !requireEmail
  )
  const [isPasswordValid, setIsPasswordValid] = useState(!requireRegistration)
  const [isCountryValid, setIsCountryValid] = useState(!requireRegistration)

  useEffect(() => {
    setIsPasswordValid(!requireRegistration)
    setIsCountryValid(!requireRegistration)
  }, [requireRegistration])

  const [email, setEmail] = useState(defaultEmailValue)
  const [password, setPassword] = useState('')
  const [country, setCountry] = useState('')

  const isLoginValid =
    isEmailValid && isRecaptchaValid && isPasswordValid && isCountryValid

  return (
    <Container fullWidth={mobile}>
      <SectionTitle title={title} />
      <Row type='flex'>
        <Description fullWidth={mobile}>{description}</Description>
      </Row>
      <Row type='flex'>
        <Col xs={{ span: 24 }} md={{ span: 16 }} lg={{ span: 12 }}>
          {requireEmail && (
            <React.Fragment>
              <InputEmail
                handleChange={setEmail}
                handleValidityChange={setIsEmailValid}
                defaultValue={defaultEmailValue}
                defaultDisabled={emailDisabled}
                isRequired
              />
              {requireRegistration && (
                <React.Fragment>
                  <InputPassword
                    handleChange={setPassword}
                    handleValidityChange={setIsPasswordValid}
                    isRequired
                  />
                  <SelectCountry
                    selectedValue={country}
                    handleChange={setCountry}
                    handleValidityChange={setIsCountryValid}
                    isRequired
                  />
                </React.Fragment>
              )}
            </React.Fragment>
          )}
          {requireRecaptcha && (
            <RecaptchaWidget
              fullWidth={mobile}
              handleChange={setIsRecaptchaValid}
            />
          )}
          {!hideSubmit && (
            <ResponsiveButton
              fullWidth={mobile}
              onClick={() =>
                handleSubmit(
                  requireEmail && email,
                  requireRegistration && password,
                  requireRegistration && country
                )
              }
              type={isLoginValid ? 'primary' : 'disabled'}
            >
              {buttonLabel}
            </ResponsiveButton>
          )}
          <TermsAndPrivacyFooter isSurvey={!mobile} />
        </Col>
      </Row>
    </Container>
  )
}

export default SurveyLoginLayout
