import styled from 'styled-components'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'
import { COMPONENTS_DEFAULT_MARGIN } from '../../utils/Metrics'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: ${({ fullWidth }) => (fullWidth ? 'center' : 'stretch')};
  max-width: 140rem;
  padding: ${({ fullWidth }) => (fullWidth ? 0 : `8vh 10vw 0 10vw`)};
  margin: 0 auto;
  text-align: ${({ fullWidth }) => (fullWidth ? 'center' : 'lefts')};
`

export const Description = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: ${({ fullWidth }) => (fullWidth ? 'center' : 'flex-start')};
  margin-bottom: ${COMPONENTS_DEFAULT_MARGIN}rem;

  * {
    font-family: ${family.primaryRegular};
    ${'' /* font-size: 1.5rem !important; */}
    line-height: 1.5 !important;
    color: ${colors.SLATE_GREY};
  }
`
