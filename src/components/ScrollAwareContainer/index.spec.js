import React from 'react'
import { mount } from 'enzyme'
import ScrollAwareContainer from '.'

describe('ScrollAwareContainer', () => {
  let testRender
  let threshold
  let mockScrollTo = jest.fn()

  beforeAll(() => {
    global.window.scrollTo = mockScrollTo
  })

  beforeEach(() => {
    threshold = 8
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ScrollAwareContainer', async () => {
    testRender = mount(
      <ScrollAwareContainer>
        {children => <div>{'scroll'}</div>}
      </ScrollAwareContainer>
    )
    expect(testRender.find(ScrollAwareContainer)).toHaveLength(1)
  })

  test('test Scroll to top', async () => {
    testRender = mount(
      <ScrollAwareContainer>
        {props => {
          return <div {...props}>{'scroll'}</div>
        }}
      </ScrollAwareContainer>
    )
    testRender
      .find('div')
      .props()
      .scrollToTop()
    expect(mockScrollTo).toHaveBeenCalledWith(0, 0)
    expect(testRender.state('scrollY')).toBe(0)
  })

  test('test scroll', async () => {
    testRender = mount(
      <ScrollAwareContainer>
        {props => {
          return <div {...props}>{'scroll'}</div>
        }}
      </ScrollAwareContainer>
    )
    global.window.scrollY = 10
    testRender.instance().handleScroll()
    expect(testRender.state('scrollY')).toBe(10)
  })

  test('test scroll', async () => {
    testRender = mount(
      <ScrollAwareContainer threshold = {threshold}>
        {props => {
          return <div {...props}>{'scroll'}</div>
        }}
      </ScrollAwareContainer>
    )
    global.window.scrollY = 10
    testRender.instance().handleScroll()
    expect(testRender.state('scrollY')).toBe(8)
  })
})
