import React from 'react'
import { mount } from 'enzyme'
import RequestAccountForm from '.'
import { act } from 'react-dom/test-utils'
import Input from '../Input'
import PhoneNumberInput from '../PhoneNumberInput'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('RequestAccountForm', () => { 
  let testRender
  let firstName
  let email
  let lastName
  let companyName
  let code
  let number
  let setFieldValue
  let handleChange
  let handleBlur
  let canSubmit
  let handleSubmit
  let errors
  let touched
  let loading

  beforeEach(() => {
    firstName = 'Alex'
    email = 'flower-wiki@gmail.com'
    lastName = 'al'
    companyName = 'flower-wiki'
    code = '91'
    number = '123456789'
    setFieldValue = jest.fn()
    handleChange = jest.fn()
    handleBlur = jest.fn()
    canSubmit = true
    handleSubmit = jest.fn()
    errors = {}
    touched = false
    loading = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render RequestAccountForm', async () => {
    testRender = mount(
      <RequestAccountForm
        firstName={firstName}
        email={email}
        lastName={lastName}
        companyName={companyName}
        code={code}
        number={number}
        setFieldValue={setFieldValue}
        handleChange={handleChange}
        handleBlur={handleBlur}
        canSubmit={canSubmit}
        handleSubmit={handleSubmit}
        errors={errors}
        touched={touched}
        loading={loading}
      />
    )
    act(() => {
      testRender
        .find(Input)
        .first()
        .prop('onBlur')()
    })
    expect(handleBlur).toHaveBeenCalled()

    act(() => {
      testRender
        .find(Input)
        .first()
        .prop('onChange')()
    })
    expect(handleChange).toHaveBeenCalled()

    act(() => {
      testRender
        .find(PhoneNumberInput)
        .first()
        .prop('onChangeCode')({
          target: {
            value: 'prompt'
          }
        })
    })
    expect(setFieldValue).toHaveBeenCalled()
  })
})
