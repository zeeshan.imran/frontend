import React from 'react'
import { Form } from 'antd'
import OnboardingForm from '../OnboardingForm'
import Input from '../Input'
import Loader from '../Loader'
import PhoneNumberInput from '../PhoneNumberInput'

import { ButtonContainer, StyledButton } from './styles'
import { useTranslation } from 'react-i18next';

const RequestAccountForm = ({
  email,
  firstName,
  lastName,
  companyName,
  code,
  number,
  setFieldValue,
  handleChange,
  handleBlur,
  canSubmit,
  handleSubmit,
  errors,
  touched,
  loading
}) => {
  const getFormItemProps = name => {
    return {
      help: touched[name] && errors[name],
      validateStatus: touched[name] && errors[name] ? 'error' : 'success'
    }
  }
  const { t } = useTranslation();
  return (
    <OnboardingForm onSubmit={handleSubmit}>
      <Form.Item {...getFormItemProps('email')}>
        <Input
          name='email'
          value={email}
          onBlur={handleBlur}
          onChange={handleChange}
          placeholder={t('placeholders.emailToReach')}
        />
      </Form.Item>
      <Form.Item {...getFormItemProps('firstName')}>
        <Input
          name='firstName'
          value={firstName}
          onBlur={handleBlur}
          onChange={handleChange}
          placeholder={t('placeholders.firstName')}
        />
      </Form.Item>
      <Form.Item {...getFormItemProps('lastName')}>
        <Input
          name='lastName'
          value={lastName}
          onBlur={handleBlur}
          onChange={handleChange}
          placeholder={t('placeholders.lastName')}
        />
      </Form.Item>
      <Form.Item {...getFormItemProps('companyName')}>
        <Input
          name='companyName'
          value={companyName}
          onBlur={handleBlur}
          onChange={handleChange}
          placeholder={t('placeholders.companyName')}
        />
      </Form.Item>
      <Form.Item
        help={
          (touched.code && errors.code) || (touched.number && errors.number)
        }
        validateStatus={
          (touched.number && errors.number) || (touched.code && errors.code)
            ? 'error'
            : 'success'
        }
      >
        <PhoneNumberInput
          codeName='code'
          code={code}
          onChangeCode={value => setFieldValue('code', value)}
          numberName='number'
          number={number}
          onBlurNumber={handleBlur}
          onChangeNumber={handleChange}
          placeholder={t('placeholders.phoneNumber')}
        />
      </Form.Item>
      <ButtonContainer>
        <Loader loading={loading}>
          <StyledButton
            type='primary'
            disabled={!canSubmit}
            onClick={handleSubmit}
          >
            {t('components.requestAccountForm.submit')}
          </StyledButton>
        </Loader>
      </ButtonContainer>
    </OnboardingForm>
  )
}

export default RequestAccountForm
