import React from 'react'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import { Container, CheckboxContainer } from './styles'
import { Row, Col, Form } from 'antd'
import { StyledCheckbox } from '../StyledCheckBox'
import FieldLabel from '../FieldLabel'
import Input from '../Input'
import settingsSchema from '../../validates/settings'

const { REACT_APP_THEME } = process.env
const themeBasedSettingsStatus = REACT_APP_THEME !== 'chrHansen' ? true : false

const AutoAdvanceSettings = ({
  autoAdvanceSettings,
  onSettingsChange,
  sharingButtons,
  onSharingButtonsChange,
  screenerOnly,
  editing = false,
  handleValidatedDataChange,
  validatedData,
  pdfFooterSettings,
  handlePdfSetting,
  showGeneratePdf,
  handleGeneratePdfButton,
  surveyAuthorizationType,
  showOnTasterDashboard,
  handleShowOnTasterDashboard
}) => {
  const { t } = useTranslation()
  return (
    <Formik
      enableReinitialize
      validationSchema={settingsSchema}
      initialValues={autoAdvanceSettings}
    >
      {({ errors, values, setFieldValue }) => (
        <Container>
          {themeBasedSettingsStatus && (
            <React.Fragment>
              <Row gutter={24}>
                <Col xs={{ span: 8 }}>
                  <Form.Item>
                    <FieldLabel
                      tooltip={t('tooltips.screenerOnly')}
                      label={t('surveySettings.screenerOnly.title')}
                    >
                      {editing ? (
                        <span>- {screenerOnly ? `Yes` : `No`}</span>
                      ) : (
                        <CheckboxContainer>
                          <StyledCheckbox
                            disabled={editing}
                            checked={screenerOnly}
                            onChange={e => {
                              onSettingsChange(values, e.target.checked)
                            }}
                          >
                            {t('surveySettings.screenerOnly.active')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      )}
                    </FieldLabel>
                  </Form.Item>
                </Col>
              </Row>

              {!screenerOnly && (
                <Row gutter={24}>
                  <Col xs={{ span: 8 }}>
                    <Form.Item>
                      <FieldLabel
                        tooltip={t('tooltips.showGeneratePdf')}
                        label={t('surveySettings.showGeneratePdf.title')}
                      >
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={showGeneratePdf}
                            onChange={e => {
                              handleGeneratePdfButton(e.target.checked)
                            }}
                          >
                            {t('surveySettings.showGeneratePdf.active')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}

              <Row gutter={24}>
                <Col xs={{ span: 8 }}>
                  <Form.Item>
                    <FieldLabel
                      label={t(
                        'surveySettings.autoAdvanceSettings.autoAdvance'
                      )}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={values.active}
                          onChange={e => {
                            onSettingsChange(
                              {
                                ...values,
                                active: e.target.checked
                              },
                              screenerOnly
                            )
                          }}
                        >
                          {t('surveySettings.autoAdvanceSettings.active')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
                {!!values.active && (
                  <React.Fragment>
                    <Col xs={{ span: 8 }}>
                      <Form.Item>
                        <FieldLabel
                          label={t(
                            'surveySettings.autoAdvanceSettings.nextButton'
                          )}
                          tooltip={t('tooltips.hideNextButtonTooltip')}
                        >
                          <CheckboxContainer>
                            <StyledCheckbox
                              checked={values.hideNextButton}
                              onChange={e => {
                                onSettingsChange({
                                  ...values,
                                  hideNextButton: e.target.checked
                                })
                              }}
                            >
                              {t(
                                'surveySettings.autoAdvanceSettings.hideNextButton'
                              )}
                            </StyledCheckbox>
                          </CheckboxContainer>
                        </FieldLabel>
                      </Form.Item>
                    </Col>
                    <Col xs={{ span: 8 }}>
                      <Form.Item
                        help={errors.debounce}
                        validateStatus={errors.debounce ? 'error' : 'success'}
                      >
                        <Input
                          label={t(
                            'surveySettings.autoAdvanceSettings.debounce'
                          )}
                          tooltip={t('tooltips.debounceTooltip')}
                          type='number'
                          size='default'
                          value={values.debounce}
                          onChange={e => {
                            const original = parseFloat(e.target.value)
                            const truncated = Math.floor(original * 10) / 10
                            const value =
                              isNaN(original) || original === truncated
                                ? e.target.value
                                : truncated

                            if (value > 1e4) {
                              return
                            }

                            setFieldValue('debounce', value)
                            onSettingsChange({
                              ...values,
                              debounce: value
                            })
                          }}
                        />
                      </Form.Item>
                    </Col>
                  </React.Fragment>
                )}
              </Row>
              <Row gutter={24}>
                <Col xs={{ span: 12 }}>
                  <Form.Item>
                    <FieldLabel
                      label={t('surveySettings.sharingButtons.label')}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={sharingButtons}
                          onChange={e => {
                            onSharingButtonsChange(e.target.checked)
                          }}
                        >
                          {t('surveySettings.sharingButtons.yes')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={24}>
                <Col xs={{ span: 12 }}>
                  <Form.Item>
                    <FieldLabel
                      label={t('surveySettings.valdatedChartData.label')}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={validatedData}
                          onChange={e => {
                            handleValidatedDataChange(e.target.checked)
                          }}
                        >
                          {t('surveySettings.valdatedChartData.yes')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
              </Row>
            </React.Fragment>
          )}
          <Row gutter={24}>
            <Col xs={{ span: 8 }}>
              <Form.Item>
                <FieldLabel label={t('surveySettings.pdfFooterData.label')}>
                  <CheckboxContainer>
                    <StyledCheckbox
                      checked={pdfFooterSettings.active}
                      onChange={e => {
                        handlePdfSetting({
                          ...pdfFooterSettings,
                          active: e.target.checked
                        })
                      }}
                    >
                      {t('surveySettings.valdatedChartData.yes')}
                    </StyledCheckbox>
                  </CheckboxContainer>
                </FieldLabel>
              </Form.Item>
            </Col>
            {!!pdfFooterSettings.active && (
              <Col xs={{ span: 16 }}>
                <Form.Item
                  help={errors.debounce}
                  validateStatus={errors.debounce ? 'error' : 'success'}
                >
                  <Input
                    label={t('surveySettings.pdfFooterSettings.note')}
                    type='TextArea'
                    size='default'
                    value={pdfFooterSettings.footerNote}
                    onChange={e => {
                      handlePdfSetting({
                        ...pdfFooterSettings,
                        footerNote: e.target.value
                      })
                    }}
                  />
                </Form.Item>
              </Col>
            )}
          </Row>
          {surveyAuthorizationType === 'public' && (
            <Row gutter={24}>
              <Col xs={{ span: 12 }}>
                <Form.Item>
                  <FieldLabel
                    label={t('surveySettings.showOnTasterDashboard.label')}
                  >
                    <CheckboxContainer>
                      <StyledCheckbox
                        checked={showOnTasterDashboard}
                        onChange={e => {
                          handleShowOnTasterDashboard(e.target.checked)
                        }}
                      >
                        {t('surveySettings.showOnTasterDashboard.yes')}
                      </StyledCheckbox>
                    </CheckboxContainer>
                  </FieldLabel>
                </Form.Item>
              </Col>
            </Row>
          )}
        </Container>
      )}
    </Formik>
  )
}

export default AutoAdvanceSettings
