import React from 'react'
import { shallow } from 'enzyme'
import ForgotPasswordForm from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('ForgotPasswordForm', () => {
  window.localStorage = {
    getItem: () => '{}'
  }

  let testRender
  let email
  let errorMessage
  let infoMessage
  let errors
  let isValid
  let handleChange
  let handleSubmit
  let handleGoBack
  let loading

  beforeEach(() => {
    email = 'test@test.com'
    errorMessage = 'Enter valid email'
    infoMessage = 'Email here'
    errors = {}
    isValid = true
    handleChange = jest.fn()
    handleSubmit = jest.fn()
    handleGoBack = jest.fn()
    loading = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ForgotPasswordForm', async () => {
    testRender = shallow(
      <ForgotPasswordForm
        email={email}
        errorMessage={errorMessage}
        infoMessage={infoMessage}
        errors={errors}
        isValid={isValid}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        handleGoBack={handleGoBack}
        loading={loading}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
