import React from 'react'
import { Form, Icon } from 'antd'
import Input from '../Input'
import OnboardingForm from '../OnboardingForm'
import Error from '../Error'
import SuccessMessage from '../SuccessMessage'
import Loader from '../Loader'

import {
  FormInfoContainer,
  FormLinkContainer,
  LoginLink,
  StyledButton
} from './styles'
import { useTranslation } from 'react-i18next';

const FormItem = Form.Item

const ForgotPasswordForm = ({
  email,
  errorMessage,
  infoMessage,
  errors,
  isValid,
  handleChange,
  handleSubmit,
  handleGoBack,
  loading
}) => {
  const { t } = useTranslation();
  return (
    <OnboardingForm>
      <FormItem
        validateStatus={errors.email ? 'error' : 'success'}
        help={errors.email}
      >
        <Input
          name='email'
          value={email}
          onChange={handleChange}
          prefix={<Icon type='mail' />}
          placeholder={t('placeholders.email')}
        />
      </FormItem>
      <FormInfoContainer>
        {!!infoMessage && <SuccessMessage>{infoMessage}</SuccessMessage>}
        {!!errorMessage && <Error>{errorMessage}</Error>}
      </FormInfoContainer>
      <Loader loading={loading}>
        <StyledButton
          type={isValid ? 'primary' : 'disabled'}
          onClick={handleSubmit}
        >
          {t('components.forgotPasswordForm.sendButton')}
        </StyledButton>
      </Loader>
      <FormLinkContainer>
        <LoginLink onClick={handleGoBack}>Back to Login</LoginLink>
      </FormLinkContainer>
    </OnboardingForm>
  )
}

export default ForgotPasswordForm
