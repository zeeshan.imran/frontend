import React, { useState, useCallback } from 'react'
import { Icon as AntIcon } from 'antd'
import debounce from 'lodash.debounce'
import { Search } from './styles'

const SearchBar = ({ handleChange, placeholder, withIcon, value }) => {
  const [localValue, setLocalValue] = useState(value)
  const debouncedChangeHandler = useCallback(
    debounce(handleChange, 800, { leading: false }),
    [handleChange]
  )
  const onChange = value => {
    setLocalValue(value)
    debouncedChangeHandler.cancel()
    debouncedChangeHandler(value)
  }

  return (
    <Search
      placeholder={placeholder}
      onChange={event => onChange(event.target.value)}
      suffix={withIcon ? <AntIcon type='search' /> : null}
      value={localValue}
    />
  )
}

export default SearchBar
