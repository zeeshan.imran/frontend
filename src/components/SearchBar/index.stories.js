import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import SearchBar from './'

class StateWrapper extends Component {
  state = {
    search: ''
  }

  render () {
    return this.props.children({
      setSearch: search => this.setState({ search }),
      search: this.state.search
    })
  }
}

storiesOf('SearchBar', module).add('Search Bar with Icon', () => (
  <StateWrapper>
    {({ setSearch, search }) => {
      return (
        <SearchBar
          withIcon
          placeholder='Search Placeholder'
          handleChange={setSearch}
          value={search}
        />
      )
    }}
  </StateWrapper>
))
