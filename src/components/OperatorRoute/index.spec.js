import React from 'react'
import { shallow } from 'enzyme'
import OperatorRoute from '.'

describe('OperatorRoute', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorRoute', async () => {
    testRender = shallow(<OperatorRoute />)
    expect(testRender).toMatchSnapshot()
  })
})
