import React from 'react'
import PrivateRoute from '../PrivateRoute'

const OperatorRoute = props => <PrivateRoute {...props} authenticateOperator authenticateAnalytics authenticatePowerUser />

export default OperatorRoute
