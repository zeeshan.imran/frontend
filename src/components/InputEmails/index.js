import React from 'react'
import FieldLabel from '../FieldLabel'
import { Select } from 'antd'

const InputEmails = ({ label, required, value, onBlur, onChange }) => {
  return (
    <FieldLabel required={required} label={label}>
      <Select
        mode='tags'
        autoClearSearchValue
        dropdownStyle={{ display: 'none' }}
        tokenSeparators={[',', ';', ' ']}
        value={value}
        onBlur={onBlur}
        onChange={onChange}
      />
    </FieldLabel>
  )
}

export default InputEmails
