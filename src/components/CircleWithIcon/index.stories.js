import React from 'react'
import { storiesOf } from '@storybook/react'
import CircleWithIcon from './'

storiesOf('CircleWithIcon', module).add('Circle With Icon', () => (
  <CircleWithIcon backgroundColor={'#FF0000'}>A</CircleWithIcon>
))
