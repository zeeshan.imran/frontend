import React from 'react'
import { shallow } from 'enzyme'
import CircleWithIcon from '.'
import { Shape } from './styles'


describe('CircleWithIcon', () => {
  let testRender
  let backgroundColor
  let children

  beforeEach(() => {
    children = <Shape>Click here</Shape>
    backgroundColor = 'white'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CircleWithIcon', async () => {
    testRender = shallow(
      <CircleWithIcon backgroundColor={backgroundColor}>{children}</CircleWithIcon>
    )
    expect(testRender).toMatchSnapshot()
  })
})
