import styled from 'styled-components'
import Text from '../Text'
import Button from '../Button'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 2.5rem;
`

export const Title = styled(Text)`
  flex-grow: 1;
  font-size: 1.6rem;
  color: '#5e5e5e';
  font-family: ${family.primaryBold};
  align-items: center;
  display: flex;
`

export const WideButton = styled(Button)`
  min-width: 9.2rem;
  max-height: 3.2rem;
  text-align: center;
  margin-right: ${({ withmargin }) => (withmargin ? '1.5rem' : '0')};
`

export const ButtonText = styled(Text)`
  font-size: 1.4rem;
  font-weight: normal;
  line-height: 1.71;
  color: white;
  padding: 0;
`
export const ActionContainer = styled.div`
  display: inline-flex;
`

export const TooltipContent = styled.div`
  ul {
    margin: 0.5rem 0 0 0;
    padding-left: 2rem;
    min-width: 180px;
    li {
      padding-bottom: 0.5rem;
    }
  }
`