import React, { memo } from 'react'
import startCase from 'lodash.startcase'
import capitalize from 'lodash.capitalize'
import { useTranslation } from 'react-i18next'
import HelperIcon from '../HelperIcon'
import {
  Container,
  Title,
  WideButton,
  ButtonText,
  ActionContainer
} from './styles'
import { StyledCheckbox } from '../StyledCheckBox'
import { NEVER_REQUIRED_QUESTIONS_TYPES } from '../../utils/Constants'
import TooltipWrapper from '../../components/TooltipWrapper'

const QuestionCreationCardHeader = ({
  question,
  mandatory,
  onMandatoryChange,
  handleRemove,
  handleCopy,
  questionIndex,
  onProductChange
}) => {
  const isStrictMode = question.editMode === 'strict'
  const { t } = useTranslation()

  const { type = '' } = question || {}
  const title = `${questionIndex + 1} - ${
    type
      ? `Type: ${capitalize(startCase(type))}`
      : t('components.questionCreation.newQuestion')
  }`

  const hint = t(`tooltips.questionHeader.${question.type}`, {
    defaultValue: null
  })

  const restrictedTypes = ['paypal-email', 'choose-product', 'choose-payment']
  const showButtons = !(restrictedTypes.indexOf(type) >= 0)

  return (
    <React.Fragment>
      <Container>
        <Title>
          {title}
          {hint && (
            <HelperIcon
              placement='rightTop'
              helperText={hint}
              overlayStyle={{ minWidth: '280px' }}
            />
          )}
        </Title>
        {showButtons && (
          <React.Fragment>
            {question && question.displayOn === 'middle' && (
              <StyledCheckbox
                checked={question && question.showProductImage}
                onChange={e => onProductChange(e.target.checked)}
              >
                Show Product Image
              </StyledCheckbox>
            )}
            <StyledCheckbox
              disabled={NEVER_REQUIRED_QUESTIONS_TYPES.has(question.type)}
              checked={mandatory}
              onChange={e => onMandatoryChange(e.target.checked)}
            >
              {t('components.questionCreation.mandatory')}
            </StyledCheckbox>
            <ActionContainer>
              <TooltipWrapper helperText={t('tooltips.duplicateQuestion')}>
                <WideButton withmargin={`true`} onClick={handleCopy}>
                  <ButtonText>
                    {t('components.questionCreation.duplicateButton')}
                  </ButtonText>
                </WideButton>
              </TooltipWrapper>
              {isStrictMode ? (
                <HelperIcon
                  placement='bottomRight'
                  helperText={t(
                    'components.questionCreation.removeButtonHelper'
                  )}
                />
              ) : (
                <TooltipWrapper helperText={t('tooltips.removeQuestion')}>
                  <WideButton type='red' onClick={handleRemove}>
                    <ButtonText>
                      {t('components.questionCreation.removeButton')}
                    </ButtonText>
                  </WideButton>
                </TooltipWrapper>
              )}
            </ActionContainer>
          </React.Fragment>
        )}
      </Container>
    </React.Fragment>
  )
}

export default memo(QuestionCreationCardHeader)
