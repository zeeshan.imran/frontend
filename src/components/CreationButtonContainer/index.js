import React, { useState } from 'react'
import Button from '../Button'
import { Container } from './styles'
import TooltipWrapper from '../../components/TooltipWrapper'

const CreationButtonContainer = ({ onClick, text, tooltip, ...rest }) => {
  const [tooltipVisibility, setTooltipVisibility] = useState(false)
  return (
    <Container>
      <TooltipWrapper
        helperText={tooltip}
        visible={tooltipVisibility}
        onVisibleChange={newState => {
          setTooltipVisibility(newState)
        }}
      >
        <Button
          {...rest}
          onClick={() => {
            onClick()
            setTooltipVisibility(false)
          }}
        >
          {text}
        </Button>
      </TooltipWrapper>
    </Container>
  )
}

export default CreationButtonContainer
