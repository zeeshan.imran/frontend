import React from 'react'
import { shallow } from 'enzyme'
import CreationButtonContainer from '.'

describe('CreationButtonContainer', () => {
  let testRender
  let onClick
  let text

  beforeEach(() => {
    onClick = jest.fn()
    text = 'Create survey'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CreationButtonContainer', async () => {
    testRender = shallow(
      <CreationButtonContainer onClick={onClick} text={text} rest={{}} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
