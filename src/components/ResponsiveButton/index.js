import React from 'react'

import { Container, Button } from './styles'
import useResponsive from '../../utils/useResponsive'

const ResponsiveButton = ({ children, ...otherProps }) => {
  const { mobile } = useResponsive()

  return (
    <Container fullWidth={mobile}>
      <Button styled={{ fullWidth: mobile }} {...otherProps}>
        {children}
      </Button>
    </Container>
  )
}

export default ResponsiveButton
