import React from 'react'
import { mount } from 'enzyme'
import ResponsiveButton from '.'

describe('ResponsiveButton', () => {
  let testRender
  let children
  let otherProps

  beforeEach(() => {
    children = '<Container>Submit</Container>'
    otherProps = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ResponsiveButton', async () => {
    testRender = mount(
      <ResponsiveButton children={children} otherProps={otherProps} />
    )
    expect(testRender.find(ResponsiveButton)).toHaveLength(1)
  })
})
