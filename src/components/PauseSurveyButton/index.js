import React from 'react'
import IconButton from '../IconButton'
import Loader from '../Loader'

import { useTranslation } from 'react-i18next'


const PauseSurveyButton = ({ state, loading, onClickPause, onClickResume }) => {
  const { t } = useTranslation();
  if (!state) {
    return (
      <Loader size={16}>
        <IconButton type='play-circle' />
      </Loader>
    )
  }

  if (state === 'suspended') {
    return (
      <IconButton
        tooltip={t('tooltips.resumeSurvey')}
        onClick={onClickResume}
        type='play-circle'
      />
    )
  }

  if (state === 'active') {
    return (
      <IconButton
        tooltip={t('tooltips.pauseSurvey')} 
        onClick={onClickPause}
        type='pause-circle'
      />
    )
  }

  return null
}

export default PauseSurveyButton
