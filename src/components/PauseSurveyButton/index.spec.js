import React from 'react'
import { mount } from 'enzyme'
import PauseSurveyButton from '.'
import IconButton from '../IconButton'
import Loader from '../Loader'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PauseSurveyButton', () => {
  let testRender
  let state
  let loading
  let onClickPause
  let onClickResume

  beforeEach(() => {
    state = 'active'
    loading = true
    onClickPause = jest.fn()
    onClickResume = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PauseSurveyButton', async () => {
    testRender = mount(
      <PauseSurveyButton
        state={state}
        loading={loading}
        onClickPause={onClickPause}
        onClickResume={onClickResume}
      />
    )
    expect(testRender.find(PauseSurveyButton)).toHaveLength(1)
  })

  test('should render PauseSurveyButton oclick active state', async () => {
    testRender = mount(
      <PauseSurveyButton state={state} onClickResume={onClickResume} />
    )
    testRender.find(IconButton).simulate('click')
  })

  test('should render PauseSurveyButton oclick suspended state', async () => {
    testRender = mount(
      <PauseSurveyButton state={'suspended'} onClickPause={onClickPause} />
    )
    testRender.find(IconButton).simulate('click')
  })

  test('should render PauseSurveyButton state be null', async () => {
    testRender = mount(<PauseSurveyButton state={''} loading={false} />)
    expect(testRender.find(Loader)).toHaveLength(1)
  })

  test('should render null when state is not active OR suspended', async () => {
    testRender = mount(
      <PauseSurveyButton state={'no-active'} loading={false} />
    )
    expect(testRender.isEmptyRender()).toBe(true)
  })
})
