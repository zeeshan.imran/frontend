import React from 'react'
import { mount } from 'enzyme'
import SurveyScreeningInfoText from '.'
import { BrowserRouter as Router } from 'react-router-dom'
import { StyledButton } from './styles'
import { ApolloProvider } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { surveyBasicInfo } from '../../fragments/survey'

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
      screeners {
        id
        uniqueName
        isPaypalSelected
        isGiftCardSelected
        customizeSharingMessage
        referralAmount
      }
    }
  }

  ${surveyBasicInfo}
`

describe('SurveyInfoText', () => {
  let testRender
  let description = 'Survey description'
  let onClick = jest.fn()
  let buttonLabel = 'Submit'
  let client = createApolloMockClient({
    mocks: [
      {
        request: {
          query: SURVEY_PARTICIPATION_QUERY,
          variables: {}
        },
        result: {
          surveyId: 'survey-id',
          surveyEnrollmentId: 'survey-id'
        }
      },
      {
        request: {
          query: SURVEY_QUERY,
          variables: {
            id: 'survey-id'
          }
        },
        result: {
          uniqueName: 'survey-unique-name'
        }
      }
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyScreeningInfoText', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router>
          <SurveyScreeningInfoText
            description={description}
            onClick={onClick}
            buttonLabel={buttonLabel}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(SurveyScreeningInfoText)).toHaveLength(1)

    testRender.find(StyledButton).prop('onClick')()

    expect(onClick).toHaveBeenCalled()
  })
})
