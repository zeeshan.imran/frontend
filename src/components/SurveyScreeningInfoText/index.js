import React from 'react'
import { Desktop } from '../Responsive'
import { Container, Text, StyledButton } from './styles'
import { withTranslation } from 'react-i18next'
import TermsAndPrivacyFooter from '../../containers/TermsAndPrivacyFooter'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { surveyBasicInfo } from '../../fragments/survey'

const SurveyScreeningInfoText = ({ description, onClick, t, buttonLabel }) => {
  const {
    data: {
      currentSurveyParticipation: { surveyId, surveyEnrollmentId } = {}
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const {
    data: { survey = {} }
  } = useQuery(SURVEY_QUERY, { variables: { id: surveyId } })

  const formatedText = description
    .replace(
      '{{sharing_link}}',
      `${window.location.origin}/survey/${survey.uniqueName}?referral=${surveyEnrollmentId}`
    )
    .replace('{{amount}}', survey.referralAmount)

  return (
    <Desktop>
      {desktop => (
        <Container desktop={desktop}>
          <Text dangerouslySetInnerHTML={{ __html: formatedText }} />
          <StyledButton onClick={onClick} desktop={desktop}>
            {buttonLabel}
          </StyledButton>
          <TermsAndPrivacyFooter />
        </Container>
      )}
    </Desktop>
  )
}

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
      screeners {
        id
        uniqueName
        isPaypalSelected
        isGiftCardSelected
        customizeSharingMessage
        referralAmount
      }
    }
  }

  ${surveyBasicInfo}
`

export default withTranslation()(SurveyScreeningInfoText)
