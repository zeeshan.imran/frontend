import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import SelectableElements from '../SelectableElements'

const StyledContainer = styled.div``

class TabNavigation extends Component {
  render () {
    const {
      className: containerStyle,
      initiallyActive,
      tabComparator
    } = this.props
    return (
      <SelectableElements
        initiallySelected={initiallyActive}
        elementComparator={tabComparator}
        render={({ handleSelect, isSelected, selectedElement }) => (
          <StyledContainer className={containerStyle}>
            {this.props.renderTabs({
              handleNavigate: handleSelect,
              isActive: isSelected
            })}
            {!!selectedElement && this.props.renderTabContent(selectedElement)}
          </StyledContainer>
        )}
      />
    )
  }
}

TabNavigation.propTypes = {
  renderTabs: PropTypes.func.isRequired,
  renderTabContent: PropTypes.func.isRequired,
  tabComparator: PropTypes.func.isRequired,
  style: PropTypes.object,
  initiallyActive: PropTypes.any
}

export default TabNavigation
