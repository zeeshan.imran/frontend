import React from 'react'
import { shallow } from 'enzyme'
import TabNavigation from '.'

describe('TabNavigation', () => {
    let testRender
    let className
    let initiallyActive
    let tabComparator
    let renderTabs
    let renderTabContent
    
    beforeEach(() => {
        className = jest.fn();
        initiallyActive = 'Active';
        tabComparator = jest.fn();
        renderTabs = jest.fn();
        renderTabContent = jest.fn();
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render TabNavigation', () => {
        testRender = shallow(
            <TabNavigation
                className={className}
                initiallyActive={initiallyActive}
                tabComparator={tabComparator}
                renderTabs={renderTabs}
                renderTabContent={renderTabContent}
            />
        )
        expect(testRender).toMatchSnapshot()
        
    })
})
