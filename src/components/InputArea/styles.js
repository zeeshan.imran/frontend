import styled from 'styled-components'
import { Input as AntInput } from 'antd'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const InputStyled = styled(AntInput.TextArea)`
  font-family: ${family.NotoSansKRRegular};
  letter-spacing: 0.04rem;
  color: ${colors.BLACK};
  font-size: 1.4rem;
  caret-color: ${colors.LIGHT_OLIVE_GREEN};
  resize: none;
`