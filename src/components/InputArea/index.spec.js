import React from 'react'
import { mount } from 'enzyme'
import InputArea from '.'
import { InputStyled } from './styles'

describe('InputArea', () => {
  let testRender
  let label
  let required
  let size
  let fieldDecorator
  let rest

  beforeEach(() => {
    label = 'Address'
    required = true
    size = 'sm'
    fieldDecorator = a => a
    rest = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render InputArea', async () => {
    testRender = mount(
      <InputArea
        label={label}
        required={required}
        size={size}
        fieldDecorator={fieldDecorator}
        rest={rest}
      />
    )
    expect(testRender.find(InputArea)).toHaveLength(1)

    expect(
      testRender
        .find(InputArea)
        .first()
        .text()
    ).toBe('* Address')
  })
})
