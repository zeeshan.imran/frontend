import styled from 'styled-components'
import { DEFAULT_MOBILE_PADDING } from '../../utils/Metrics'

export const Container = styled.div`
  padding: ${({ desktop }) =>
    desktop ? `8vh 10vw 0 10vw` : `5rem ${DEFAULT_MOBILE_PADDING}rem`};
  max-width: 140rem;
  margin: 0 auto;
`

export const ProgressBarContainer = styled.div`
  margin-bottom: 1.5rem;
  width: 100%;
  min-width: 100%;
`
 