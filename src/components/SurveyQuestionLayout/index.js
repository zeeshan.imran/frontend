import React from 'react'
import { Row, Col } from 'antd'
import SurveyProgressBar from '../../containers/SurveyProgressBar'
import SectionTitle from '../SectionTitle'
import { Desktop } from '../Responsive'

import { Container, ProgressBarContainer } from './styles'

const SurveyQuestionLayout = ({ title, children }) => (
  <Desktop>
    {desktop => (
      <Container desktop={desktop}>
        <SectionTitle title={title} />
        <Row>
          <Col xs={{ span: 24 }} md={{ span: 24 }}>
            <ProgressBarContainer>
              <SurveyProgressBar />
            </ProgressBarContainer>
          </Col>
        </Row>
        {children}
      </Container>
    )}
  </Desktop>
)

export default SurveyQuestionLayout
