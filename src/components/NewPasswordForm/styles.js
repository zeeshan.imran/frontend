import styled from 'styled-components'
import Button from '../Button'
import { DEFAULT_COMPONENTS_MARGIN } from '../../utils/Metrics'

export const InfoContainer = styled.div`
  margin: ${DEFAULT_COMPONENTS_MARGIN}rem 0;
`

export const CustomButton = styled(Button)`
  width: 100%;
`
