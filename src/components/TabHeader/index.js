import React from 'react'
import { TitleContainer, IconContainer, Title } from './styles.js'

const TabHeader = ({ icon, title }) => (
  <TitleContainer>
    <IconContainer>{icon}</IconContainer>
    <Title>{title}</Title>
  </TitleContainer>
)

export default TabHeader
