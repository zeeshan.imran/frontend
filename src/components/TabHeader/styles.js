import styled from 'styled-components'
import { family } from '../../utils/Fonts'

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
`

export const IconContainer = styled.div`
  margin-right: 1rem;
  height: 3rem;
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    height: 2.5rem;
  }
`

export const Title = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1.57;
`
