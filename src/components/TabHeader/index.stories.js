import React from 'react'
import { storiesOf } from '@storybook/react'
import TabHeader from './'
import Person from '../SvgIcons/Person'

storiesOf('TabHeader', module).add('Person Tab', () => (
  <TabHeader title={'Title'} icon={<Person />} />
))
