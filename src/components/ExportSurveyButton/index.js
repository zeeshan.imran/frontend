import React from 'react'
import Text from '../../components/Text'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
 
const ExportSurveyButton = ({
  icon,
  tKey,
  onClick
}) => {
  const { t } = useTranslation()

  return (
    <Text onClick={onClick}>
      {t(`tooltips.${tKey}`)}
    </Text>
  )
}

export default withRouter(ExportSurveyButton)
