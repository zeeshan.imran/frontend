import React from 'react'
import { Modal, Icon } from 'antd'
import colors from '../../utils/Colors'
import i18n from '../../utils/internationalization/i18n'

const confirm = Modal.confirm

const AlertModal = ({ title, description, handleOk, handleCancel, okText }) => {
  return confirm({
    title,
    content: description,
    okText: okText || i18n.t('components.alertModal.delete'),

    okButtonProps: {
      style: { backgroundColor: colors.ERROR, borderColor: colors.ERROR }
    },
    cancelButtonProps: {
      style: { color: '#5e5e5e', borderColor: '#d9d9d9' }
    },
    cancelText: i18n.t('components.alertModal.cancel'),
    onOk: handleOk,
    onCancel: handleCancel,
    centered: true,
    icon: (
      <Icon
        type='close-circle'
        theme='filled'
        style={{ color: colors.ERROR }}
      />
    ),
    maskClosable: true
  })
}

export default AlertModal
