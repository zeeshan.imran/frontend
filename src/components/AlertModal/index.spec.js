import React, { useEffect } from 'react'
import { mount } from 'enzyme'
import AlertModal from '.'
import wait from '../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: text => text })
}))

jest.mock('../../utils/internationalization/i18n', () => ({
  t: text => text
}))

describe('AlertModal', () => {
  let testRender
  let handleCancel
  let title
  let description
  let myMockFn
  let okText

  beforeEach(() => {
    title = 'Survey open'
    description = 'Please fill all questions'
    handleCancel = jest.fn()
    myMockFn = jest.fn(cb => cb(null, true))
    okText = 'Ok'
  })

  afterEach(() => {
    testRender.unmount()
  })

  const TestComponent = () => {
    useEffect(() => {
      AlertModal({
        title,
        description,
        handleOk: myMockFn,
        handleCancel,
        okText
      })
    }, [])

    return null
  }

  test('should render AlertModal', async () => {
    testRender = mount(<TestComponent />)

    await wait(0)

    expect(testRender.find(TestComponent)).toHaveLength(1)
    expect(global.document.querySelectorAll('.ant-modal')).toHaveLength(1)
    expect(
      global.document.querySelector('.ant-modal-confirm-title')
    ).not.toBeNull()
    expect(
      global.document.querySelector('.ant-modal-confirm-title').textContent
    ).toBe(title)
  })
})
