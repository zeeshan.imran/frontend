import React from 'react'
import { storiesOf } from '@storybook/react'
import AlertModal from './'

storiesOf('AlertModal', module).add('Story', () => (
  <div
    onClick={() =>
      AlertModal({
        title: 'Alert title',
        description: 'Alert description',
        handleOk: () => console.log('clicked Ok'),
        handleCancel: () => console.log('clicked Cancel')
      })
    }
  >
    Click me to show the alert
  </div>
))
