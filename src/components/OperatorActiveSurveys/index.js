import React from 'react'
import SearchBar from '../SearchBar'
import OperatorSurveysList from '../OperatorSurveysList'

import { Container, Title, HeaderContainer, SearchBarContainer } from './styles'
import { useTranslation } from 'react-i18next'

const OperatorActiveSurveys = ({
  handleSearch,
  searchTerm,
  surveys,
  organizations,
  total,
  page,
  onChangePage,
  loading
}) => {
  const { t } = useTranslation()
  return (
    <Container>
      <HeaderContainer>
        <Title>{t('components.operatorSurveys.activeSurveys')}</Title>
        <SearchBarContainer>
          <SearchBar
            placeholder={t('placeholders.search')}
            withIcon
            handleChange={handleSearch}
            value={searchTerm}
          />
        </SearchBarContainer>
      </HeaderContainer>
      <OperatorSurveysList
        loading={loading}
        surveys={surveys}
        organizations={organizations}
        page={page}
        total={total}
        showHeader={false}
        onChangePage={onChangePage}
        showActions
      />
    </Container>
  )
}

export default OperatorActiveSurveys
