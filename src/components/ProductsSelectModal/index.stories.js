import React, { Component, Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import Button from '../Button'
import ProductsSelectModal from './'

const products = [
  {
    id: '1',
    name: 'Product 1',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '2',
    name: 'Product 2',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '3',
    name: 'Product 3',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '4',
    name: 'Product 4',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '5',
    name: 'Product 5',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '6',
    name: 'Product 6',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '7',
    name: 'Product 7',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '8',
    name: 'Product 8',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '9',
    name: 'Product 9',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  },
  {
    id: '10',
    name: 'Product 10',
    picture:
      'https://photos5.appleinsider.com/gallery/28527-44534-applewatch4-redsportloop-l.jpg'
  }
]

const categories = [
  {
    id: 'LfoGgUso4pWRkqqUSSMEAC',
    key: 'products',
    value: 'Products'
  },
  {
    id: 'SPR6DpTP7MqZpmF6oGa2Sz',
    key: 'modalities',
    value: 'Modalities'
  },
  {
    id: 'nTGLUGAaeMxz3fXA34nT6F',
    key: 'dairy',
    value: 'Dairy'
  },
  {
    id: 'sy7K6uYjMMBSnbHgfTZc4b',
    key: 'backedGoods',
    value: 'Backed Goods'
  },
  {
    id: 'SwZ9c3V7uySPWc8uxUDsCL',
    key: 'softDrinks',
    value: 'Soft Drinks'
  },
  {
    id: '33Ef7yG9VQGxTepkGHomHyB',
    key: 'snacks',
    value: 'Snacks'
  },
  {
    id: '37ottTrdFZpym5fxHJEu7Vc',
    key: 'frozenFood',
    value: 'Frozen Food'
  },
  {
    id: 'VJ3SVLfjfRfhzmPpnJaQLv',
    key: 'candies',
    value: 'Candies'
  },
  {
    id: 'QyMv5ngMWM8JakxFTCfPHP',
    key: 'cannedGoods',
    value: 'Canned Goods'
  },
  {
    id: 'pKMRHoSr6o9FjTxe57qrq5',
    key: 'restaurants',
    value: 'Restaurants'
  },
  {
    id: 'w4HvhyuaWvLpLdcVPgzCAg',
    key: 'wineAndBeer',
    value: 'Wine And Beer'
  },
  {
    id: 'JWtaeGb6UVVVHHthdxGRCZ',
    key: 'recipes',
    value: 'Recipes'
  },
  {
    id: 'rsfjUf2EGNJmaD6KJSjpfU',
    key: 'others',
    value: 'Others'
  }
]

class StateWrapper extends Component {
  state = {
    visible: false,
    search: ''
  }

  render () {
    return this.props.children({
      toggleVisibility: visible =>
        this.setState({ visible: !this.state.visible }),
      visible: this.state.visible,
      search: this.state.search,
      setSearch: search => this.setState({ search })
    })
  }
}

storiesOf('ProductsSelectModal', module).add('Story', () => (
  <StateWrapper>
    {({ toggleVisibility, visible, search, setSearch }) => (
      <Fragment>
        <Button onClick={toggleVisibility}>Toggle Modal</Button>
        <ProductsSelectModal
          handleOk={toggleVisibility}
          handleCancel={toggleVisibility}
          visible={visible}
          search={search}
          handleSearch={setSearch}
          categories={categories}
          products={products}
          handleItemAction={product => console.log('product.id', product.id)}
        />
      </Fragment>
    )}
  </StateWrapper>
))
