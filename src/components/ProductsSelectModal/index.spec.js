import React from 'react'
import { mount, shallow } from 'enzyme'
import ProductsSelectModal from './index'
import SelectCategory from './SelectCategory'
import ProductsTable from './ProductsTable'
import wait from '../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('ProductsSelectModal', () => {
  let render
  let visible
  let handleOk
  let handleCancel
  let search
  let handleSearch
  let categories
  let products
  let handleItemAction

  beforeEach(() => {
    visible = true
    handleOk = jest.fn()
    handleCancel = jest.fn()
    search = ''
    handleSearch = jest.fn()
    categories = ['cate-1', 'cate-2']
    products = ['product-1', 'product-2']
    handleItemAction = jest.fn()
  })

  afterEach(() => {
    render.unmount()
  })

  test('should check ProductsSelectModal', () => {
    render = mount(
      <ProductsSelectModal
        visible={visible}
        handleOk={handleOk}
        handleCancel={handleCancel}
        search={search}
        handleSearch={handleSearch}
        categories={categories}
        products={products}
        handleItemAction={handleItemAction}
      />
    )
    expect(render).toMatchSnapshot()
  })

  test('should check ProductsSelectModal handle handleSelectCategory', async () => {
    const setState = jest.fn()
    const useStateSpy = jest.spyOn(React, 'useState')
    useStateSpy.mockImplementation(init => [init, setState])

    render = mount(
      <ProductsSelectModal
        visible={visible}
        handleOk={handleOk}
        handleCancel={handleCancel}
        search={search}
        handleSearch={handleSearch}
        categories={categories}
        products={products}
        handleItemAction={handleItemAction}
        selectedCategory={'cate-1'}
      />
    )

    render.setState({ selectedCategory: 'cate-1' })

    render
      .find(SelectCategory)
      .props('handleSelectCategory')
      .handleSelectCategory(['cate-1'])

    render
      .find(ProductsTable)
      .props('handleSelection')
      .handleSelection(['product-1'])
  })
})
