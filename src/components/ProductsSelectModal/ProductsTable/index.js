import React, { Fragment } from 'react'
import { Table } from 'antd'
import { Container, Divider, Picture, Action } from './styles'
const ANT_DEFAULT_ROW_HEIGHT = 64
const PRODUCTS_TABLE_VISIBLE_ROWS = 7 * ANT_DEFAULT_ROW_HEIGHT
import { useTranslation } from 'react-i18next'


const ProductsTable = ({ products, handleSelection, handleItemAction }) => {
  const { t } = useTranslation();

  const columns = handleItemAction => [
    {
      title: t('components.products.columns.photo'),
      dataIndex: 'photo',
      render: value => <Picture src={value} />,
      width: 36
    },
    {
      title: t('components.products.columns.name'),
      dataIndex: 'name'
    },
    {
      title: t('components.products.columns.action'),
      dataIndex: 'action',
      render: (text, row, index) => (
        <Action onClick={() => handleItemAction(row)}>{t('components.products.columns.add')}</Action>
      )
    }
  ]

  return (
    <Fragment>
      <Divider />
      <Container>
        <Table
          rowKey={row => row.id}
          showHeader={false}
          pagination={false}
          scroll={{ y: PRODUCTS_TABLE_VISIBLE_ROWS }}
          rowSelection={{
            onChange: handleSelection
          }}
          columns={columns(handleItemAction)}
          dataSource={products}
        />
      </Container>
    </Fragment>
  )
}

export default ProductsTable
