import React from 'react'
import { shallow } from 'enzyme'
import ProductsTable from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

window.matchMedia = jest.fn().mockImplementation(query => {
    return {
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(),
        removeListener: jest.fn()
    }
})

describe('ProductsTable', () => {
    let testRender
    let products
    let handleChange
    let handleEditProduct
    let handleDeleteProduct

    beforeEach(() => {
        products = []
        handleChange = jest.fn()
        handleEditProduct = jest.fn()
        handleDeleteProduct = jest.fn()
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render ProductsTable', async () => {
        testRender = shallow(
            <ProductsTable
                rowKey={1}
                products={products}
                handleChange={handleChange}
                handleEditProduct={handleEditProduct}
                handleDeleteProduct={handleDeleteProduct}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
})
