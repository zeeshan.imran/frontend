import styled from 'styled-components'
import { Divider as AntDivider } from 'antd'
import { components } from '../../../utils/Metrics'
import { family } from '../../../utils/Fonts'

export const Container = styled.div`
  padding: 0 ${components.MODAL_PADDING}rem;
`

export const Divider = styled(AntDivider)`
  margin: 0;
`

export const Picture = styled.img`
  height: 3.6rem;
  width: 3.6rem;
`

export const Action = styled.span`
  cursor: pointer;
  display: flex;
  justify-content: flex-end;
  font-family: ${family.primaryRegular};
  color: rgb(24, 144, 255);
  font-size: 1.4rem;
`
