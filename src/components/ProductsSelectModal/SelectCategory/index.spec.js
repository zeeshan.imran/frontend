import React from 'react'
import { mount, shallow } from 'enzyme'
import SelectCategory from './index'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('SelectCategory', () => {
  let render
  let categories
  let handleSelectCategory
  let selectedCategory

  beforeEach(() => {
    categories = ['cate-1', 'cate-2']
    handleSelectCategory = jest.fn()
    selectedCategory = 'cate-2'
  })

  afterEach(() => {
    render.unmount()
  })

  test('should check SelectCategory', () => {
    render = shallow(
      <SelectCategory
        categories={categories}
        handleSelectCategory={handleSelectCategory}
        selectedCategory={selectedCategory}
      />
    )
    expect(render).toMatchSnapshot()
  })
})
