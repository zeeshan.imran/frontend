import React, { Fragment } from 'react'
import Select from '../../Select'
import { Spacer, DividerLine } from './styles'
import {
  getOptionName,
  getOptionValue
} from '../../../utils/getters/OptionGetters'
import { useTranslation } from 'react-i18next';

const SelectCategory = ({
  categories,
  handleSelectCategory,
  selectedCategory
}) => {
  const { t } = useTranslation();
  return (
    <Fragment>
      <Spacer>
        <Select
          size='default'
          label={t('components.products.category')}
          placeholder={t('placeholders.category')} 
          value={selectedCategory}
          options={categories}
          onChange={handleSelectCategory}
          getOptionName={getOptionName}
          getOptionValue={getOptionValue}
        />
      </Spacer>
      <DividerLine />
    </Fragment>
  )
}

export default SelectCategory
