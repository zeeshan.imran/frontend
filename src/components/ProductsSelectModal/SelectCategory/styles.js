import styled from 'styled-components'
import { components } from '../../../utils/Metrics'
import { Divider } from 'antd'

export const Spacer = styled.div`
  margin-bottom: 1.5rem;
  padding: 1.2rem ${components.MODAL_PADDING}rem 0
    ${components.MODAL_PADDING}rem;
`

export const DividerLine = styled(Divider)`
  margin-bottom: 1.5rem;
`
