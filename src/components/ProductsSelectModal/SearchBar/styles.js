import styled from 'styled-components'
import { components } from '../../../utils/Metrics'

export const Spacer = styled.div`
  margin-bottom: 1.5rem;
  padding: 0 ${components.MODAL_PADDING}rem 0 ${components.MODAL_PADDING}rem;
`
