import React from 'react'
import { shallow } from 'enzyme'
import SearchBar from './index'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('SearchBar', () => {
  let render
  let value
  let handleChange

  beforeEach(() => {
    value = 'product'
    handleChange = jest.fn()
  })

  afterEach(() => {
    render.unmount()
  })

  test('should check SearchBar', () => {
    render = shallow(<SearchBar value={value} handleChange={handleChange} />)
    expect(render).toMatchSnapshot()
  })
})
