import React from 'react'
import SearchBarComponent from '../../SearchBar'
import { Spacer } from './styles'
import { useTranslation } from 'react-i18next';

const SearchBar = ({ value, handleChange }) => {
  const { t } = useTranslation();
  return (
    <Spacer>
      <SearchBarComponent
        placeholder={t('components.products.search')}
        value={value}
        handleChange={handleChange}
      />
    </Spacer>
  )
}

export default SearchBar
