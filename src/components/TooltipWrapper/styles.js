import styled from 'styled-components'
import { Tooltip as Tt } from 'antd'

export const Tooltip = styled(Tt)`
  display: ${({ isTab }) => (isTab ? 'block' : 'inline')};
  padding: ${({ isTab }) => (isTab ? '12px 16px' : 'revert')};
`

export const TooltipContent = styled.div`
  ul {
    margin: 0.5rem 0 0 0;
    padding-left: 2rem;
    min-width: 180px;
    li {
      padding-bottom: 0.5rem;
    }
  }
`