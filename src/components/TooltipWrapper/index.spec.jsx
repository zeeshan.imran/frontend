import React from 'react'
import { shallow } from 'enzyme'
import TooltipWrapper from '.'

describe('TooltipWrapper', () => {
    let testRender
    let placement
    let helperText
    
    beforeEach(() => {
        placement = 'placement'
        helperText = 'Click here'
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render TooltipWrapper', () => {
        testRender = shallow(            
            <TooltipWrapper
                placement={placement}
                helperText={helperText}
            >
                'this is a tooltip children'
            </TooltipWrapper>
        )
        expect(testRender).toMatchSnapshot()
    })
})
