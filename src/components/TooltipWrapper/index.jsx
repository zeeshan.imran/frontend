import React from 'react'
import { Tooltip, TooltipContent } from './styles'

const TooltipWrapper = ({
  children,
  placement = 'top',
  helperText,
  style,
  isTab = false,
  ...props
}) => {
  if (helperText) {
    return (
      <Tooltip
        isTab={isTab}
        title={
          <TooltipContent
            dangerouslySetInnerHTML={{
              __html: helperText
            }}
          />
        }
        placement={placement}
        arrowPointAtCenter
        {...props}
      >
        {children}
      </Tooltip>
    )
  } else {
    return children
  }
}

export default TooltipWrapper
