import React from 'react'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import { Container } from './styles'
import { Row, Col, Form, Transfer } from 'antd'
import FieldLabel from '../FieldLabel'

const OperatorLinkedSurveys = ({
  linkedSurveys,
  allSurveys,
  handleSettingsChange
}) => {
  const { t } = useTranslation()
  const allSurveysMock =
    allSurveys && allSurveys.surveys && allSurveys.surveys.length
      ? allSurveys.surveys.map(singleSurvey => {
          return {
            key: singleSurvey.id,
            title: singleSurvey.name.trim()
          }
        })
      : []

  return (
    <React.Fragment>
      <Formik>
        {() => (
          <Container>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <FieldLabel
                    label={t(
                      'containers.page.operatorCreateSurveyLinkedSurveys.label'
                    )}
                  >
                    <Transfer
                      titles={[
                        t(
                          `containers.page.operatorCreateSurveyLinkedSurveys.tabs.all`
                        ),
                        t(
                          `containers.page.operatorCreateSurveyLinkedSurveys.tabs.selected`
                        )
                      ]}
                      listStyle={{
                        width: '45%',
                        height: 500
                      }}
                      dataSource={allSurveysMock}
                      targetKeys={linkedSurveys}
                      onChange={targetKeys => {
                        handleSettingsChange(targetKeys)
                      }}
                      render={item => item.title}
                    />
                  </FieldLabel>
                </Form.Item>
              </Col>
            </Row>
          </Container>
        )}
      </Formik>
    </React.Fragment>
  )
}

export default OperatorLinkedSurveys
