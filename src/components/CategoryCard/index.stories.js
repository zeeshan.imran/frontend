import React from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import colors from '../../utils/Colors'
import CategoryCard from './'
import CategoryImage from '../../assets/svg/Baked Goods.svg'

const category = {
  name: 'Name',
  picture: CategoryImage
}

const Wrapper = styled.div`
  background-color: ${colors.PALE_GREY};
  width: 100%;
  height: 100%;
  position: absolute;
  padding-top: 10rem;
`

storiesOf('CategoryCard', module).add('Card', () => (
  <Wrapper>
    <CategoryCard
      name={category.name}
      picture={category.picture}
      onClick={action('pressed')}
    />
  </Wrapper>
))
