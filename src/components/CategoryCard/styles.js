import styled from 'styled-components'
import Text from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${colors.WHITE};
  box-shadow: 0 0.4rem 1.6rem 0 rgba(0, 0, 0, 0.14);
  height: ${({ mobile }) => (mobile ? '11.5rem' : '16.1rem')};
  width: ${({ mobile }) => (mobile ? '15.5rem' : '21.7rem')};
  padding-top: 2.1rem;
  padding-bottom: 2.7rem;
  border-radius: 0.5rem;
  border: solid 0.2rem transparent;
  cursor: pointer;
  
  :hover {
    border: ${({ mobile }) =>
      mobile
        ? 'solid 0.2rem transparent'
        : `solid 0.2rem ${colors.LIGHT_OLIVE_GREEN}`};
  }
`

export const Image = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 100%;
  height: 9.1rem;
  color: ${colors.LIGHT_OLIVE_GREEN};
`

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.2rem;
  color: ${colors.LIGHT_OLIVE_GREEN};
`
