import React, { memo, useState, useEffect, useCallback } from 'react'
import { Form } from 'antd'
import startCase from 'lodash.startcase'
import capitalize from 'lodash.capitalize'
import PubSub from 'pubsub-js'
import Select from '../Select'
import { PUBSUB } from '../../utils/Constants'
import { exists } from 'i18next'
import { useTranslation } from 'react-i18next'

const QuestionCreationCardTypeSelect = ({
  disabled,
  types,
  type,
  existingUniqueTypes,
  onSelect
}) => {
  const [uniqueTypesSet, setSelectedTypesSet] = useState(
    new Set(existingUniqueTypes)
  )
  const [error, setError] = useState(null)
  const { t } = useTranslation()

  const [focusedOption, setFocusedOption] = useState(false)

  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.VALIDATE_SURVEY_QUESTIONS, () => {
      setError(
        type ? null : t('validation.questionCreation.questionType.required')
      )
    })

    return () => {
      PubSub.unsubscribe(token)
    }
  }, [setError, type])

  useEffect(() => {
    setSelectedTypesSet(new Set(existingUniqueTypes))
  }, [existingUniqueTypes])

  const isDisabledType = useCallback(type => uniqueTypesSet.has(type), [
    uniqueTypesSet
  ]) 

  return (
    <Form.Item help={error} validateStatus={error ? 'error' : 'success'}>
      <Select
        focusedOption={focusedOption}
        setFocusedOption={setFocusedOption}
        disabled={disabled}
        data-testid={'question-type-select'}
        name={'questionType'}
        label={t('components.questionCreation.questionType')}
        value={type}
        onChange={change => {
          onSelect(change)
          if (error) {
            setError(false)
          }
        }}
        placeholder={t('placeholders.questionType')}
        options={types}
        isDisabledOption={isDisabledType}
        getOptionName={type =>
          exists(`dropDownOptions.${type}`)
            ? t(`dropDownOptions.${type}`)
            : capitalize(startCase(type))
        }
        size='default'
        sideDetails
      />
    </Form.Item>
  )
}

export default memo(QuestionCreationCardTypeSelect)
