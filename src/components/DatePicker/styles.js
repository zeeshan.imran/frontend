import styled from 'styled-components'
import { DatePicker as AntDatePicker } from 'antd'
import { family } from '../../utils/Fonts'

export const StyledDatePicker = styled(AntDatePicker)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem !important;
  width: 100%;

  .ant-input {
    font-size: 1.4rem;
  }
`
