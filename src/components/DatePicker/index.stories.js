import React from 'react'
import { storiesOf } from '@storybook/react'
import DatePicker from './'

storiesOf('DatePicker', module)
  .add('DatePicker', () => <DatePicker size='large' />)
  .add('DatePicker with Label', () => (
    <DatePicker size='large' label='Example Label' />
  ))
  .add('DatePicker with field decorator', () => (
    <DatePicker
      fieldDecorator={cmp => console.log('component', cmp) || cmp}
      size='large'
      label='Example Label'
    />
  ))
