import React from 'react'
import { shallow } from 'enzyme'
import DatePicker from '.'

describe('DatePicker', () => {
  let testRender
  let label
  let size
  let required
  let rest

  beforeEach(() => {
    label = 'Choose date'
    size = 'sm'
    required = true
    rest = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render DatePicker', async () => { 
    testRender = shallow(
      <DatePicker
        label={label}
        size={size}
        required={required}
        value={'2019-11-10T18:30:00.000Z'}
        rest={rest}
      />
    )
    expect(testRender).toMatchSnapshot() 
  })
})
