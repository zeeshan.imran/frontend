import React, { Component } from 'react'
import moment from 'moment'
import PropTypes from 'prop-types'
import FieldLabel from '../FieldLabel'
import { StyledDatePicker } from './styles'

class DatePicker extends Component {
  render () {
    const { label, size, required, value, ...rest } = this.props
    return (
      <FieldLabel required={required} label={label}>
        <StyledDatePicker
          value={value ? moment(value) : undefined} // Needed to show placeholder when value is empty
          size={size || 'large'}
          {...rest}
        />
      </FieldLabel>
    )
  }
}

DatePicker.propTypes = {
  label: PropTypes.string,
  desktop: PropTypes.bool
}

DatePicker.defaultProps = {}

export default DatePicker
