import React from 'react'
import { mount } from 'enzyme'
import { MemoryRouter as Router } from 'react-router-dom'
import mockLocalStorage from '../../utils/testUtils/mockLocalStorage'
import PrivateRoute from './index'
import {
  setAuthenticatedUser,
  setAuthenticationToken
} from '../../utils/userAuthentication'

describe('private route component', () => {
  let render

  beforeEach(() => {
    mockLocalStorage()
  })

  afterEach(() => {
    render.unmount()
  })

  test('should check authentication', async () => {
    setAuthenticationToken('testTocken')
    setAuthenticatedUser({ type: 'taster' })
    const MockComponent = jest.fn().mockImplementation(() => null)

    render = mount(
      <Router initialEntries={['/testRoute']} initialIndex={0}>
        <PrivateRoute
          exact
          path={`/testRoute`}
          authenticateOperator
          component={MockComponent}
        />
      </Router>
    )
    expect(MockComponent).not.toHaveBeenCalled()

    render.unmount()
    render = mount(
      <Router initialEntries={['/testRoute']} initialIndex={0}>
        <PrivateRoute
          exact
          path={`/testRoute`}
          authenticateTaster
          component={MockComponent}
        />
      </Router>
    )
    expect(MockComponent).toHaveBeenCalledTimes(1)
  })
})
