import React from 'react'
import { Form } from 'antd'
import { useTranslation } from 'react-i18next'
import OnboardingForm from '../../OnboardingForm'
import Input from '../../Input'

const WelcomeFormComponent = ({
  emailAddress,
  password,
  confirmPassword,
  handleChange,
  handleBlur,
  canSubmit,
  handleSubmit,
  errors,
  touched,
  loading
}) => {
  const { t } = useTranslation()
  const getFormItemProps = name => {
    return {
      help: touched[name] && errors[name],
      validateStatus: touched[name] && errors[name] ? 'error' : 'success'
    }
  }
  return (
    <OnboardingForm onSubmit={handleSubmit}>
      <Form.Item {...getFormItemProps('emailAddress')}>
        <Input
          required
          name='emailAddress'
          value={emailAddress}
          label={t(`components.createTasterAccount.forms.first.email.label`)}
          onBlur={handleBlur}
          onChange={handleChange}
          placeholder={t(`components.createTasterAccount.forms.first.email.placeholder`)}
        />
      </Form.Item>
      <Form.Item {...getFormItemProps('password')}>
        <Input
          required
          name='password'
          value={password}
          label={t(`components.createTasterAccount.forms.first.password.label`)}
          type={'password'}
          onBlur={handleBlur}
          onChange={handleChange}
          placeholder={t(`components.createTasterAccount.forms.first.password.placeholder`)}
        />
      </Form.Item>
      <Form.Item {...getFormItemProps('confirmPassword')}>
        <Input
          required
          name='confirmPassword'
          value={confirmPassword}
          label={t(`components.createTasterAccount.forms.first.confirm.label`)}
          type={'password'}
          onBlur={handleBlur}
          onChange={handleChange}
          placeholder={t(`components.createTasterAccount.forms.first.confirm.placeholder`)}
        />
      </Form.Item>
    </OnboardingForm>
  )
}

export default WelcomeFormComponent
