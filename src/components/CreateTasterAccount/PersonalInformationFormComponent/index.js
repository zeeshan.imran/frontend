import React from 'react'
import { Form } from 'antd'
import { useTranslation } from 'react-i18next'
import OnboardingForm from '../../OnboardingForm'
import Input from '../../Input'
import DatePicker from '../../DatePicker'
import Select from '../../Select'
import {GENDERS, LANGUAGES, COUNTRY_PHONE_CODES, CURRENT_STATES} from '../../../utils/Constants'
import useResponsive from '../../../utils/useResponsive/index'

import { FullWidthDiv, ItemLeft, ItemRight, ClearFloats } from './styles'
import moment from 'moment'


const PersonalInformationFormComponent = ({
  firstName,
  lastName,
  dateofbirth,
  gender,
  language,
  country,
  state,
  handleChange,
  handleBlur,
  dropdownChangeHandler,
  dropdownChangeCountryHandler,
  getDatePickerChangeHandler,
  handleSubmit,
  errors,
  touched
}) => {
  const { t } = useTranslation()
  const getFormItemProps = name => {
    return {
      help: touched[name] && errors[name],
      validateStatus: touched[name] && errors[name] ? 'error' : 'success'
    }
  }
  
  const { desktop } = useResponsive()

  return (
    <OnboardingForm onSubmit={handleSubmit}>
      <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item {...getFormItemProps('firstName')}>
            <Input
              required
              name='firstName'
              value={firstName}
              label={t(`components.createTasterAccount.forms.second.firstName.label`)}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(`components.createTasterAccount.forms.second.firstName.placeholder`)}
            />
          </Form.Item>
        </ItemLeft>
        <ItemRight desktop={desktop}>
          <Form.Item {...getFormItemProps('lastName')}>
            <Input
              required
              name='lastName'
              value={lastName}
              label={t(`components.createTasterAccount.forms.second.lastName.label`)}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(`components.createTasterAccount.forms.second.lastName.placeholder`)}
            />
          </Form.Item>
        </ItemRight>
        <ClearFloats />
      </FullWidthDiv>
      
      <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item {...getFormItemProps('dateofbirth')}>
            <DatePicker
              required
              mode='date'
              name='dateofbirth'
              value={dateofbirth}
              defaultPickerValue={dateofbirth ? dateofbirth : moment().subtract(18, 'years')}
              label={t(`components.createTasterAccount.forms.second.dateofbirth.label`)}
              onChange={getDatePickerChangeHandler('dateofbirth')}
              showToday
              format={'DD-MMM-YYYY'}
              placeholder={t(`components.createTasterAccount.forms.second.dateofbirth.placeholder`)}
              disabledDate={currentDate => currentDate > moment().subtract(18, 'years')}
            />
          </Form.Item>
        </ItemLeft>
        <ItemRight desktop={desktop}>
          <Form.Item>
            <Select
              showSearch={desktop ? true : false}
              required
              name='gender'
              value={gender}
              options={GENDERS}
              getOptionName={gender =>
                gender.charAt(0).toUpperCase() + gender.slice(1)
              }
              onChange={dropdownChangeHandler('gender')}
              label={t(`components.createTasterAccount.forms.second.gender.label`)}
              placeholder={t(`components.createTasterAccount.forms.second.gender.placeholder`)}
            />
          </Form.Item>
        </ItemRight>
        <ClearFloats />
      </FullWidthDiv>
      <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item>
            <Select
              showSearch={desktop ? true : false}
              required
              name='country'
              value={country}
              options={COUNTRY_PHONE_CODES}
              optionFilterProp='children'
              getOptionName={option => option.name}
              getOptionValue={option => option.code}
              onChange={dropdownChangeCountryHandler('country')}
              label={t(`components.createTasterAccount.forms.second.country.label`)}
              placeholder={t(`components.createTasterAccount.forms.second.country.placeholder`)}
            />
          </Form.Item>
        </ItemLeft>
        <ItemRight desktop={desktop}>
          <Form.Item>
            <Select
              showSearch={desktop ? true : false}
              required
              name='state'
              value={state}
              options={CURRENT_STATES[country] ? CURRENT_STATES[country] : CURRENT_STATES['others']}
              getOptionName={option => option.name}
              getOptionValue={option => option.name}
              onChange={dropdownChangeHandler('state')}
              label={t(`components.createTasterAccount.forms.second.state.label`)}
              placeholder={t(`components.createTasterAccount.forms.second.state.placeholder`)}
            />
          </Form.Item>
        </ItemRight>
        <ClearFloats />
      </FullWidthDiv>
      <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item>
            <Select
              showSearch={desktop ? true : false}
              required
              name='langugage'
              value={language}
              options={LANGUAGES}
              getOptionName={language =>
                language.charAt(0).toUpperCase() + language.slice(1)
              }
              onChange={dropdownChangeHandler('language')}
              label={t(`components.createTasterAccount.forms.second.langugage.label`)}
              placeholder={t(`components.createTasterAccount.forms.second.langugage.placeholder`)}
            />
          </Form.Item>
        </ItemLeft>
        <ClearFloats />
      </FullWidthDiv>
    </OnboardingForm>
  )
}

export default PersonalInformationFormComponent
