import styled from 'styled-components'
import { Desktop } from '../Responsive'
import colors from '../../utils/Colors'

export const Container = styled(Desktop)``

export const FooterContainer = styled.div`
  padding: 5.5rem 0;
  background-color: ${colors.WHITE};
  width: 100%;
`
