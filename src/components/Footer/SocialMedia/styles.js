import styled from 'styled-components'
import { Icon } from 'antd'
import colors from '../../../utils/Colors'
import { DEFAULT_MOBILE_MARGIN } from '../../../utils/Metrics'

export const SocialMediaContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: ${({ desktop }) =>
    desktop ? 0 : `${DEFAULT_MOBILE_MARGIN}rem`};
`
export const CustomIcon = styled(Icon)`
  font-size: 1.6rem;
  color: ${colors.BLACK};
  margin-right: ${({ styled = {} }) => (styled.last ? 0 : `2.6rem`)};
`
