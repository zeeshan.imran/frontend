import React from 'react'
import { shallow } from 'enzyme'
import SocialMedia from '.';

describe('SocialMedia', () => {
    let testRender;
    let desktop;

    beforeEach(() => {
        desktop ="5rem 5rem 8rem 5rem";
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render SocialMedia', async () => {
        testRender = shallow(
            <SocialMedia
                desktop={desktop}
            />
        )
        expect(testRender).toMatchSnapshot()
        
    })
});