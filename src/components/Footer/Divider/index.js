import React from 'react'
import { Row, Col } from 'antd'
import DividerLine from '../../DividerLine'

const Divider = () => (
  <Row>
    <Col xs={{ span: 24 }}>
      <DividerLine />
    </Col>
  </Row>
)

export default Divider
