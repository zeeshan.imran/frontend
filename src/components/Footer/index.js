import React from 'react'
// import Divider from './Divider'
import TopRow from './TopRow'
// import BottomRow from './BottomRow'
import { Container, FooterContainer } from './styles'

const Footer = () => (
  <Container>
    {desktop => {
      return (
        <FooterContainer desktop={desktop}>
          <TopRow desktop={desktop} />
        </FooterContainer>
      )
    }}
  </Container>
)

export default Footer
