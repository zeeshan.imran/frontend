import React from 'react'
import { Row, Col } from 'antd'
// import SocialMedia from '../SocialMedia'
import { TopContainer, Logo, LogoAligner } from './styles'
import { imagesCollection } from '../../../assets/png'

const TopRow = ({ desktop }) => (
  <TopContainer desktop={desktop}>
    <Row
      type='flex'
      align='middle'
      justify={desktop ? 'space-between' : 'center'}
    >
      <Col xs={{ span: 12 }} lg={{ span: 4 }}>
        <LogoAligner desktop={desktop}>
          <Logo desktop={desktop} src={imagesCollection.poweredBy} />
        </LogoAligner>
      </Col>
      {/* {desktop && <SocialMedia desktop={desktop} />} */}
    </Row>
  </TopContainer>
)

export default TopRow
