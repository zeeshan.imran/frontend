import React from 'react'
import { shallow } from 'enzyme'
import TopRow from '.';

describe('TopRow', () => {
    let testRender;
    let desktop;

    beforeEach(() => {
        desktop = "center";
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render TopRow', async () => {
        testRender = shallow(
            <TopRow
                desktop={desktop}
            />
        )
        expect(testRender).toMatchSnapshot()
        
    })
});