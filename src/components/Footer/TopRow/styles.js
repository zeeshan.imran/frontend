import styled from 'styled-components'
import { DEFAULT_MOBILE_MARGIN } from '../../../utils/Metrics'

export const TopContainer = styled.div`
  margin-bottom: ${({ desktop }) =>
    desktop ? `6rem` : `${DEFAULT_MOBILE_MARGIN}rem`};
`
export const Logo = styled.img`
  height: 2.7rem;
`

export const LogoAligner = styled.div`
  display: flex;
  justify-content: ${({ desktop }) => (desktop ? 'left' : 'center')};
`
