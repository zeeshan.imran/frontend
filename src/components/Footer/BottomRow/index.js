import React from 'react'
import { Row, Col } from 'antd'
import { AboutText, LinkText, RightCol } from './styles'
import SocialMedia from '../SocialMedia'
import { useTranslation } from 'react-i18next';

const PrivacyPolicyLink = ({ desktop }) => {
  const { t } = useTranslation();
  return (
    <LinkText
      href='https://www.flavorwiki.com/'
      target='_blank'
      rel='noopener noreferrer'
      desktop={desktop}
    >
      {t('components.footer.privacyPolicy')}
    </LinkText>
  )
}

const TermsAndConditionsLink = ({ desktop }) => {
  const { t } = useTranslation();
  return (
    <LinkText
      href='https://www.flavorwiki.com/'
      target='_blank'
      rel='noopener noreferrer'
      last
      desktop={desktop}
    >
      {t('components.footer.termsAndConditions')}
    </LinkText>
  )
}

const Links = ({ desktop }) => {
  return (
    <React.Fragment>
      <PrivacyPolicyLink desktop={desktop} />
      <TermsAndConditionsLink desktop={desktop} />
    </React.Fragment>
  )
}

const BottomRow = ({ desktop }) => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      <Row type='flex' align='middle'>
        <Col xs={{ span: 24 }} lg={{ span: 12 }}>
          <AboutText desktop={desktop}>
            {t('components.footer.aboutText')}
          </AboutText>
        </Col>
        <RightCol styled={{ desktop }} xs={{ span: 24 }} lg={{ span: 12 }}>
          <Links desktop={desktop} />
        </RightCol>
      </Row>
      {!desktop && (
        <Row type='flex' justify='center'>
          <SocialMedia desktop={desktop} />
        </Row>
      )}
    </React.Fragment>
  )
}

export default BottomRow
