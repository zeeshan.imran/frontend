import React from 'react'
import { shallow } from 'enzyme'
import BottomRow from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('BottomRow', () => {
  let testRender
  let desktop

  beforeEach(() => {
    desktop = 'middle'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render BottomRow', async () => {
    testRender = shallow(<BottomRow desktop={desktop} />)
    expect(testRender).toMatchSnapshot()
  })
})
