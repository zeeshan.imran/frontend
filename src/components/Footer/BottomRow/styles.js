import styled from 'styled-components'
import { Col } from 'antd'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'
import { DEFAULT_MOBILE_MARGIN } from '../../../utils/Metrics'

export const RightCol = styled(Col)`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  flex-direction: ${({ styled = {} }) => (styled.desktop ? 'row' : 'column')};
`

export const AboutText = styled.span`
  display: flex;
  text-align: ${({ desktop }) => (desktop ? 'left' : 'center')};
  font-family: ${family.primaryLight};
  font-size: 1.6rem;
  line-height: 1.63;
  letter-spacing: normal;
  color: ${colors.BLACK};
  opacity: 0.7;
  margin-bottom: ${({ desktop }) =>
    desktop ? 0 : `${DEFAULT_MOBILE_MARGIN}rem`};
`

export const LinksContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`

export const LinkText = styled.a`
  font-family: ${family.primaryRegular};
  font-size: 1.2rem;
  line-height: normal;
  letter-spacing: 0.17rem;
  text-transform: uppercase;
  margin-right: ${({ last, desktop }) => (last || !desktop ? 0 : `5.7rem`)};
  opacity: 0.3;
  color: ${colors.BLACK};
  text-decoration: none;
  user-select: none;
  margin-bottom: ${({ desktop }) =>
    desktop ? 0 : `${DEFAULT_MOBILE_MARGIN}rem`};

  :hover {
    color: ${colors.BLACK};
    opacity: 1;
  }
`
