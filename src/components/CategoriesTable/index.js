import React from 'react'
import Table from './Table'
import SelectedItemsBanner from '../SelectedItemsBanner'

const CategoriesTable = ({ selected, ...tableProps }) => (
  <React.Fragment>
    <SelectedItemsBanner selectedItems={(selected || []).length} />
    <Table {...tableProps} />
  </React.Fragment>
)

export default CategoriesTable
