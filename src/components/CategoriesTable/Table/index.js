import React from 'react'
import moment from 'moment'
import { Table as AntTable } from 'antd'
import AlertModal from '../../AlertModal'
import LinkText from '../../LinkText'

import { ColumnContentAligner, Picture, Separator } from './styles'
import { useTranslation } from 'react-i18next'
import i18n from '../../../utils/internationalization/i18n'

export const nameColumn = {
  title: i18n.t('components.table.nameColumnTitle'),
  render: (_, rowData) => (
    <React.Fragment>
      <Picture
        src={
          'https://www.akc.org/wp-content/themes/akc/component-library/assets/img/welcome.jpg'
        }
      />
      <span>{rowData.name}</span>
    </React.Fragment>
  ),
  sorter: ({ name: nameA }, { name: nameB }) => {
    if (nameA.toUpperCase() < nameB.toUpperCase()) {
      return -1
    }
    if (nameA.toUpperCase() > nameB.toUpperCase()) {
      return 1
    }

    return 0
  }
}

export const numberOfProductsColumn = {
  title: i18n.t('components.table.productsColumnTitle'),
  dataIndex: 'numberOfProducts',
  sorter: (
    { numberOfProducts: nProductsA },
    { numberOfProducts: nProductsB }
  ) => Number.parseInt(nProductsA, 10) - Number.parseInt(nProductsB, 10)
}

export const dateColumn = {
  title: i18n.t('components.table.dateColumnTitle'),
  dataIndex: 'date',
  sorter: ({ date: dateA }, { date: dateB }) => moment(dateA) - moment(dateB)
}

const Table = ({
  handleChangeSelection,
  categories,
  handleEdit,
  handleRemove
}) => {
  const { t } = useTranslation()
  const nameColumn = {
    title: t('components.table.nameColumnTitle'),
    render: (_, rowData) => (
      <React.Fragment>
        <Picture
          src={
            'https://www.akc.org/wp-content/themes/akc/component-library/assets/img/welcome.jpg'
          }
        />
        <span>{rowData.name}</span>
      </React.Fragment>
    ),
    sorter: ({ name: nameA }, { name: nameB }) => {
      if (nameA.toUpperCase() < nameB.toUpperCase()) {
        return -1
      }
      if (nameA.toUpperCase() > nameB.toUpperCase()) {
        return 1
      }

      return 0
    }
  }

  const numberOfProductsColumn = {
    title: t('components.table.productsColumnTitle'),
    dataIndex: 'numberOfProducts',
    sorter: (
      { numberOfProducts: nProductsA },
      { numberOfProducts: nProductsB }
    ) => Number.parseInt(nProductsA, 10) - Number.parseInt(nProductsB, 10)
  }
  
  const dateColumn = {
    title: t('components.table.dateColumnTitle'),
    dataIndex: 'date',
    sorter: ({ date: dateA }, { date: dateB }) => moment(dateA) - moment(dateB)
  }

  const showRemoveAlert = category =>
    AlertModal({
      title: t('components.table.alertModalTitle'),
      description: t('components.table.alertModalDescription'),
      handleOk: () => handleRemove(category),
      handleCancel: () => {}
    })
  return (
    <AntTable
      rowKey={record => record.id}
      rowSelection={{
        onChange: handleChangeSelection
      }}
      dataSource={categories}
      columns={[
        nameColumn,
        numberOfProductsColumn,
        dateColumn,
        {
          title: t('components.table.dateColumnActionTitle'),
          dataIndex: '',
          render: (_, rowData) => (
            <ColumnContentAligner>
              <LinkText onClick={() => handleEdit(rowData)}>Edit</LinkText>
              <Separator />
              <LinkText onClick={() => showRemoveAlert(rowData)}>
                {t('components.table.actionRemove')}
              </LinkText>
            </ColumnContentAligner>
          )
        }
      ]}
    />
  )
}

export default Table
