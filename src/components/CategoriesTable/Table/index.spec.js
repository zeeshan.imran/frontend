import React from 'react'
import { mount } from 'enzyme'
import Table, { numberOfProductsColumn, dateColumn, nameColumn } from '.'
import LinkText from '../../LinkText'
import AlertModal from '../../AlertModal'
import moment from 'moment'

jest.mock('../../AlertModal')

const mockAlertModal = ({ handleOk, handleCancel }) => {
  handleOk()
  handleCancel()
}
AlertModal.mockImplementation(mockAlertModal)

describe('Table', () => {
  let testRender
  let handleChangeSelection
  let categories
  let handleEdit
  let handleRemove
  let tableRecords

  beforeEach(() => {
    handleChangeSelection = jest.fn()
    categories = [
      {
        numberOfProducts: 5,
        date: moment('2010-01-01'),
        name: 'AA'
      },
      {
        numberOfProducts: 4,
        date: moment('2010-05-01'),
        name: 'B'
      }
    ]
    handleEdit = jest.fn()

    handleRemove = jest.fn()

    tableRecords = [
      {
        numberOfProducts: 5,
        date: new Date('2019-01-01T00:00:00.000Z'),
        name: 'AA'
      },
      {
        numberOfProducts: 4,
        date: new Date('2019-01-01T00:00:00.000Z'),
        name: 'B'
      }
    ]
  })

  test('should render Table', async () => {
    testRender = mount(
      <Table
        handleChangeSelection={handleChangeSelection}
        categories={categories}
        handleEdit={handleEdit}
        handleRemove={handleRemove}
      />
    )
    expect(testRender.find(Table)).toHaveLength(1)

    // moved unmount here
    testRender.unmount()
  })

  test('should render Table with date sorter', () => {
    const tableRecordsource_date = [
      {
        numberOfProducts: 5,
        date: moment('2019-01-09'),
        name: 'a'
      },
      {
        numberOfProducts: 4,
        date: moment('2019-01-02'),
        name: 'b'
      }
    ]

    testRender = mount(
      <Table
        handleChangeSelection={handleChangeSelection}
        categories={tableRecordsource_date}
        handleEdit={handleEdit}
        handleRemove={handleRemove}
      />
    )

    testRender
      .find('.ant-table-column-sorter-inner')
      .first()
      .simulate('click')

    testRender
      .find('.ant-table-column-sorter-inner')
      .last()
      .simulate('click')

    testRender.unmount()
  })
  test('should render Table with name sorter', () => {
    const tableRecordsource_date = [
      {
        numberOfProducts: 5,
        date: moment('2019-01-09'),
        name: 'b'
      },
      {
        numberOfProducts: 4,
        date: moment('2019-01-02'),
        name: 'a'
      }
    ]

    testRender = mount(
      <Table
        handleChangeSelection={handleChangeSelection}
        categories={tableRecordsource_date}
        handleEdit={handleEdit}
        handleRemove={handleRemove}
      />
    )

    testRender
      .find('.ant-table-column-sorter-inner')
      .first()
      .simulate('click')

    testRender.unmount()
  })

  test('should render Table with name sorter with return 0 ', () => {
    const tableRecordsource_date = [
      {
        numberOfProducts: 5,
        date: moment('2019-01-09'),
        name: 'a'
      },
      {
        numberOfProducts: 4,
        date: moment('2019-01-02'),
        name: 'a'
      }
    ]

    testRender = mount(
      <Table
        handleChangeSelection={handleChangeSelection}
        categories={tableRecordsource_date}
        handleEdit={handleEdit}
        handleRemove={handleRemove}
      />
    )

    testRender
      .find('.ant-table-column-sorter-inner')
      .first()
      .simulate('click')

    testRender.unmount()
  })

  test('should render Table with date sorter', () => {
    const tableRecordsource_date = [
      {
        numberOfProducts: 5,
        date: moment('2019-01-09'),
        name: 'a'
      },
      {
        numberOfProducts: 4,
        date: moment('2019-01-02'),
        name: 'a'
      }
    ]

    testRender = mount(
      <Table
        handleChangeSelection={handleChangeSelection}
        categories={tableRecordsource_date}
        handleEdit={handleEdit}
        handleRemove={handleRemove}
      />
    )

    testRender
      .find('.ant-table-column-sorter-inner')
      .at(1)
      .simulate('click')

    testRender.unmount()
  })

  test('should call on click when remove', async () => {
    testRender = mount(
      <Table categories={categories} handleRemove={handleRemove} />
    )

    testRender
      .find(LinkText)
      .last()
      .simulate('click')

    expect(handleRemove).toHaveBeenCalled()

    // moved unmount here
    testRender.unmount()
  })

  test('should call on click when edit', async () => {
    testRender = mount(
      <Table categories={categories} handleEdit={handleEdit} />
    )

    testRender
      .find(LinkText)
      .first()
      .simulate('click')

    expect(handleEdit).toHaveBeenCalled()
    // moved unmount here
    testRender.unmount()
  })

  /// added the following tests
  test('product number sorter', () => {
    const result = numberOfProductsColumn.sorter(...tableRecords)
    expect(result).toBe(1)
  })

  test('date sorter', () => {
    // they will be the same date, so we'll receive 0
    const result = dateColumn.sorter(...tableRecords)
    expect(result).toBe(0)
  })

  test('date sorter', () => {
    const result = nameColumn.sorter(...tableRecords)
    expect(result).toBe(-1)
  })

  test('date sorter', () => {
    const tableRecordsource = [
      {
        numberOfProducts: 5,
        date: new Date(),
        name: 'b'
      },
      {
        numberOfProducts: 4,
        date: new Date(),
        name: 'a'
      }
    ]

    const result = nameColumn.sorter(...tableRecordsource)
    expect(result).toBe(1)
  })

  test('date sorter return -1', () => {
    const tableRecordNamesource = [
      {
        numberOfProducts: 5,
        date: new Date(),
        name: 'a'
      },
      {
        numberOfProducts: 4,
        date: new Date(),
        name: 'b'
      }
    ]

    const result = nameColumn.sorter(...tableRecordNamesource)
    expect(result).toBe(-1)
  })
  test('date sorter return 0', () => {
    const tableRecordNamesource = [
      {
        numberOfProducts: 5,
        date: new Date(),
        name: 'a'
      },
      {
        numberOfProducts: 4,
        date: new Date(),
        name: 'a'
      }
    ]

    const result = nameColumn.sorter(...tableRecordNamesource)
    expect(result).toBe(0)
    nameColumn.render({}, ...tableRecordNamesource)
  })

  test('should handle ok and cancel', () => {
    const showRemoveAlert = category =>
      AlertModal({
        title: 'You are about to delete a category',
        description:
          'This will delete the category from the database. Are you sure?',
        handleOk: () => handleRemove(category),
        handleCancel: () => {}
      })

    showRemoveAlert(handleRemove, 'category-id')
    expect(AlertModal).toHaveBeenCalled()
  })
})
