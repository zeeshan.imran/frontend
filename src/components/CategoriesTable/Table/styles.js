import styled from 'styled-components'
import { DEFAULT_COMPONENTS_MARGIN } from '../../../utils/Metrics'

export const ColumnContentAligner = styled.div`
  display: inline-flex;
  align-items: center;
`

export const Picture = styled.div`
  display: inline-block;
  height: 3.6rem;
  width: 3.6rem;
  border-radius: 4px;
  border: solid 0.1rem #8f8f8f;
  margin-right: ${DEFAULT_COMPONENTS_MARGIN}rem;
  background: url(${props => props.src}) no-repeat center;
  background-size: cover;
  vertical-align: middle;
`

export const Separator = styled.div`
  display: inline-block;
  height: 1.4rem;
  width: 0.1rem;
  margin: 0 0.8rem;
  background-color: #e9e9e9;
`
