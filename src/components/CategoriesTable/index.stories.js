import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import CategoriesTable from './'

class TableWithState extends Component {
  state = {
    selected: []
  }

  render () {
    return (
      <CategoriesTable
        categories={[
          {
            id: 'a',
            name: 'catgA',
            numberOfProducts: 123,
            date: '2018-10-01',
            children: [
              {
                id: 'aa',
                name: 'subCatgA',
                numberOfProducts: 12,
                date: '2018-10-02'
              },
              {
                id: 'ab',
                name: 'subCatgB',
                numberOfProducts: 14,
                date: '2018-10-02'
              }
            ]
          },
          {
            id: 'b',
            name: 'catgB',
            numberOfProducts: 43,
            date: '2018-11-01'
          }
        ]}
        selected={this.state.selected}
        handleChangeSelection={selected => this.setState({ selected })}
        handleEdit={targetCategory =>
          console.log('would navigate to edit page for:', targetCategory)
        }
        handleRemove={targetCategory =>
          console.log('would remove:', targetCategory)
        }
      />
    )
  }
}

storiesOf('CategoriesTable', module).add('CategoriesTable', () => (
  <TableWithState />
))
