import styled from 'styled-components'

export const ImpersonateContainer = styled.div`
  border-top: 1px solid #ddd;
  padding: 8px 16px;
  label {
    padding-bottom: 4px;
  }
  .ant-layout-sider-collapsed & {
    display: none;
  }
`

export const ButtonGroup = styled.div`
  padding-top: 8px;
  display: flex;
  justify-content: flex-end;
  button {
    margin: 0 4px;
  }
`
