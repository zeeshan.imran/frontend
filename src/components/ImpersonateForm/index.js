import React from 'react'
import { Select } from 'antd'
import { ImpersonateContainer } from './style'
import Label from '../FieldLabel/Label'
import { useTranslation } from 'react-i18next';

const Impersonate = ({
  loading,
  users,
  impersonateUserId,
  onSearch,
  onImpersonUserIdChange
}) => {
  const { t } = useTranslation();
  return (
    <ImpersonateContainer>
      <Label label={t('components.impersonate.impersonateUser')} />
      <Select
        placeholder={t('placeholders.user')}
        style={{ width: '100%' }}
        showSearch
        loading={loading}
        value={impersonateUserId}
        defaultActiveFirstOption={false}
        showArrow={false}
        filterOption={false}
        onSearch={onSearch}
        onChange={onImpersonUserIdChange}
        optionLabelProp='label'
        notFoundContent={null}
      >
        {users
          .map(s => (
            <Select.Option key={s.id} label={s.emailAddress}>
              <div>{s.fullName}</div>
              <div>{s.emailAddress}</div>
            </Select.Option>
          ))}
      </Select>
    </ImpersonateContainer>
  )
}

export default Impersonate
