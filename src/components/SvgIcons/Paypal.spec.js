import React from 'react'
import { mount } from 'enzyme'
import Paypal from './Paypal.js'

describe('Paypal', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Paypal', async () => {
    testRender = mount(<Paypal />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 64 75"]')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(3)
  })
})
