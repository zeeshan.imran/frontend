import React from 'react'
import { mount } from 'enzyme'
import Notifications from './Notifications.js'

describe('Notifications', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Notifications', async () => {
    testRender = mount(<Notifications />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 32 32"]')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(1)
  })
})
