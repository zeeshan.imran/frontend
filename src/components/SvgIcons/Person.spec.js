import React from 'react'
import { mount } from 'enzyme'
import Person from './Person.js'

describe('Person', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Person', async () => {
    testRender = mount(<Person color={'grey'} />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('g')).toHaveLength(2)
    expect(testRender.find('svg[viewBox="0 0 20 22"]')).toHaveLength(1)
  })
})
