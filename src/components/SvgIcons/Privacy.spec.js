import React from 'react'
import { mount } from 'enzyme'
import Privacy from './Privacy.js'

describe('Privacy', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Privacy', async () => {
    testRender = mount(<Privacy color={'grey'} />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('g')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 19 22"]')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(1)
  })
})
