import React from 'react'
import { mount } from 'enzyme'
import AdditionalInfo from './AdditionalInfo.js'

describe('AdditionalInfo', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AdditionalInfo', async () => {
    testRender = mount(<AdditionalInfo />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 16 19"]')).toHaveLength(1)
    expect(testRender.find('g')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(1)
  })
})
