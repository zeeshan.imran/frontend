import React from 'react'
import { mount } from 'enzyme'
import Email from './Email.js'

describe('Email', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Email', async () => {
    testRender = mount(<Email />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 30 32"]')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(1)
  })
})
