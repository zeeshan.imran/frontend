import React from 'react'
import { mount } from 'enzyme'
import PaypalLogo from './PaypalLogo.js'

describe('PaypalLogo', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PaypalLogo', async () => {
    testRender = mount(<PaypalLogo />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 124 33"]')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(6)

  })
})
