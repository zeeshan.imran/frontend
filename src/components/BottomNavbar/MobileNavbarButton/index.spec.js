import React from 'react'
import { shallow } from 'enzyme'
import MobileNavbarButton from '.';

window.matchMedia = jest.fn().mockImplementation(query => {
    return {
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(),
        removeListener: jest.fn(),
    };
});

describe('MobileNavbarButton', () => {
    let testRender
    
    beforeEach(() => {
        
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render MobileNavbarButton', async () => {


        testRender = shallow(
            <MobileNavbarButton
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});