import React from 'react'
import { CustomNavbarButton } from './styles'

const MobileNavbarButton = props => <CustomNavbarButton {...props} />

export default MobileNavbarButton
