import React from 'react'
import { Desktop } from '../Responsive'
import NavbarButton from './NavbarButton'
import MobileNavbarButton from './MobileNavbarButton'
import { withTranslation } from 'react-i18next'

import { Container, Text } from './styles'

const BottomNavbar = ({
  loading,
  onOk,
  okText,
  okDisabled = false,
  onCancel,
  cancelText,
  cancelDisabled = false,
  totalSteps,
  currentStep,
  hasExtraAction,
  onExtraAction,
  extraAction,
  t
}) => ( 
  <Desktop>
    {desktop => {
      const shouldRenderSteps = totalSteps && currentStep
      const singleButton = !onCancel && !shouldRenderSteps
      const Button = desktop ? NavbarButton : MobileNavbarButton
      return (
        <Container singleButton={singleButton} desktop={desktop}>
          {shouldRenderSteps && (
            <Text desktop={desktop}>
              {t(`components.bottomNavBar.steps`, { currentStep, totalSteps })}
            </Text>
          )}
          {onCancel && (
            <Button
              order={1}
              disabled={cancelDisabled}
              onClick={onCancel}
              loading={loading}
            >
              {cancelText}
            </Button>
          )}
          {hasExtraAction && desktop && (
            <Button
              type='secondary'
              order={2}
              loading={loading}
              onClick={onExtraAction}
            >
              {extraAction}
            </Button>
          )}
          {onOk && (
            <Button
              order={4}
              disabled={okDisabled}
              onClick={onOk}
              loading={loading}
            >
              {okText}
            </Button>
          )}
        </Container>
      )
    }}
  </Desktop>
)

export default withTranslation()(BottomNavbar)
