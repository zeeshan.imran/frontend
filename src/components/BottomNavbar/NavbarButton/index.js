import React from 'react'
import Loader from '../../Loader'

import { Container, CustomButton } from './styles'

const NavbarButton = ({
  className,
  order,
  loading = false,
  disabled,
  ...otherProps
}) => (
  <Container order={order} className={className}>
    <Loader loading={loading}>
      <CustomButton type={disabled ? 'disabled' : 'primary'} {...otherProps} />
    </Loader>
  </Container>
)

export default NavbarButton
