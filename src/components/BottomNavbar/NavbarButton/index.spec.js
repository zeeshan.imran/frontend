import React from 'react'
import { shallow } from 'enzyme'
import NavbarButton from '.';

window.matchMedia = jest.fn().mockImplementation(query => {
    return {
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(),
        removeListener: jest.fn(),
    };
});

describe('NavbarButton', () => {
    let testRender
    let className
    let order
    let loading
    let disabled

    beforeEach(() => {
        className = 'active'
        order = 2
        loading = true
        disabled =false
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render NavbarButton', async () => {


        testRender = shallow(

            <NavbarButton
                className={className}
                order={order}
                loading={loading}
                disabled={disabled}
            />

        )
        expect(testRender).toMatchSnapshot()
    })
});