import styled from 'styled-components'
import Button from '../../Button'

export const Container = styled.div`
  margin-left: 1.5rem;
  margin-right: 0;
  order: ${({ order }) => order || 0};
  width: 15.5rem;
`

export const CustomButton = styled(Button)`
  width: 100%;
`
