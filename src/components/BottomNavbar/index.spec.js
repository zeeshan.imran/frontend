import React from 'react'
import { shallow } from 'enzyme'
import BottomNavbar from '.'

window.matchMedia = jest.fn().mockImplementation(query => {
  return {
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn()
  }
})

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('BottomNavbar', () => {
  let testRender
  let loading
  let onOk
  let okText
  let okDisabled
  let onCancel
  let cancelText
  let cancelDisabled
  let totalSteps
  let currentStep
  let hasExtraAction
  let onExtraAction
  let extraAction

  beforeEach(() => {
    loading = true
    onOk = jest.fn()
    okText = 'Survey Compeleted'
    okDisabled = false
    onCancel = jest.fn()
    cancelText = 'Cancel'
    cancelDisabled = false
    totalSteps = 5
    currentStep = 1
    hasExtraAction = jest.fn()
    onExtraAction = jest.fn()
    extraAction = 'back'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render BottomNavbar', async () => {
    testRender = shallow(
      <BottomNavbar
        loading={loading}
        onOk={onOk}
        okText={okText}
        okDisabled={okDisabled}
        onCancel={onCancel}
        cancelText={cancelText}
        cancelDisabled={cancelDisabled}
        totalSteps={totalSteps}
        currentStep={currentStep}
        hasExtraAction={hasExtraAction}
        onExtraAction={onExtraAction}
        extraAction={extraAction}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
