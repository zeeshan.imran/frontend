import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import BottomNavbar from './'

storiesOf('BottomNavbar', module)
  .add('Bottom Navbar with one button', () => (
    <BottomNavbar onOk={action('pressed')} okText={'OK'} />
  ))
  .add('Bottom Navbar with two buttons', () => (
    <BottomNavbar
      onOk={action('pressed')}
      okText={'OK'}
      onCancel={action('pressed')}
      cancelText={'CANCEL'}
    />
  ))
  .add('Bottom Navbar with two buttons and step indicator', () => (
    <BottomNavbar
      onOk={action('pressed')}
      okText={'Next'}
      onCancel={action('pressed')}
      cancelText={'Prev'}
      totalSteps={1000}
      currentStep={999}
    />
  ))
  .add('Bottom Navbar in loading state', () => (
    <BottomNavbar
      loading
      onOk={action('pressed')}
      okText={'Next'}
      onCancel={action('pressed')}
      cancelText={'Prev'}
    />
  ))
