import styled from 'styled-components'
import BaseText from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  position: fixed;
  flex-direction: row;
  bottom: 0;
  height: 5.6rem;
  width: 100%;
  background-color: ${colors.WHITE};
  box-shadow: 0.1rem 0 0.8rem 0 rgba(0, 21, 41, 0.12);
  padding: ${({ desktop }) => (desktop ? '0 5rem 0 0' : 0)};
  align-items: center;
  justify-content: ${({ desktop, singleButton }) =>
    desktop ? 'flex-end' : singleButton ? 'center' : 'space-between'};
`

export const Text = styled(BaseText)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1.43;
  letter-spacing: 0.05rem;
  color: ${colors.SLATE_GREY};
  order: ${({ desktop }) => (desktop ? 0 : 3)};
`
