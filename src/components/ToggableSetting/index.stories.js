import React from 'react'
import { storiesOf } from '@storybook/react'
import Email from '../SvgIcons/Email'
import ToggableSetting from './'

storiesOf('ToggableSetting', module).add('Email', () => (
  <ToggableSetting
    handleChange={checked => console.log('setting checked', checked)}
    title='Marketing emails'
    description='Businesses often become known today through effective marketing. '
    icon={<Email />}
  />
))
