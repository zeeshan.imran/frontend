import React from 'react'
import { mount } from 'enzyme'
import ToggableSetting from '.';
import { TextContainer, Icon } from './styles'
import { Switch } from 'antd'

describe('ToggableSetting', () => {
    let handleChange;
    let testRender;

    beforeEach(() => {
        handleChange = jest.fn();
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render ToggableSetting', async () => {
        testRender = mount(
            <ToggableSetting
                title="Test"
                description="Test description"
                icon="icon"
                handleChange={handleChange}
            />
        )
        expect(testRender.find(Icon)).toHaveLength(1)
        expect(testRender.find(TextContainer)).toHaveLength(1)
        expect(testRender.find(Switch)).toHaveLength(1)
    })
});