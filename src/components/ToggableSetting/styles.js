import styled from 'styled-components'
import Text from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'
import { COMPONENTS_DEFAULT_MARGIN } from '../../utils/Metrics'

export const Container = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: ${COMPONENTS_DEFAULT_MARGIN}rem;
`

export const TextContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: 0 ${COMPONENTS_DEFAULT_MARGIN}rem;
`

export const Icon = styled.div`
  height: 2.5rem;
  svg {
    height: 2.5rem;
  }
`

export const Title = styled(Text)`
  color: ${colors.SLATE_GREY};
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  letter-spacing: 0.04rem;
  line-height: 1.71;
`

export const Description = styled(Text)`
  color: ${colors.PALE_GREY};
  font-family: ${family.primaryRegular};
  font-size: 1.2rem;
  letter-spacing: 0.04rem;
  line-height: 1.33;
`
