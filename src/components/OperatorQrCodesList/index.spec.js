import React from 'react'
import { mount } from 'enzyme'
import OperatorQrCodesList from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('OperatorQrCodesList', () => {
  let testRender
  let loading
  let qrCodes
  let onCopy
  let onEdit
  let onDelete
  let onDownload

  beforeEach(() => {
    loading = false
    qrCodes = [
      {
        targetType: 'survey',
        survey: ''
      }
    ]
    onCopy = jest.fn()
    onEdit = jest.fn()
    onDelete = jest.fn()
    onDownload = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorQrCodesList', async () => {
    testRender = mount(
      <OperatorQrCodesList
        loading={loading}
        qrCodes={qrCodes}
        onCopy={onCopy}
        onEdit={onEdit}
        onDelete={onDelete}
        onDownload={onDownload}
      />
    )
    expect(testRender.find(OperatorQrCodesList)).toHaveLength(1)
  })
})
