import React from 'react'
import { Table } from 'antd'
import {
  ActionsContainer,
  QrCode,
  TitleWrapper,
  QrCodeName,
  QrCodeDesc
} from './styles'
import IconButton from '../IconButton'
import { withTranslation } from 'react-i18next'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'

const TableContext = React.createContext({})

const OperatorQrCodesList = ({
  loading,
  qrCodes,
  onCopy,
  onEdit,
  onDelete,
  t,
  onDownload,
  onTableChange,
  total,
  page
}) => {

  const actionsColumn = {
    title: t('components.qrCode.actionsTitle'),
    dataIndex: 'actions',
    render: (_, qrCode, __) => {
      return (
        <TableContext.Consumer>
          {({ onDelete, onEdit, onDownload, onCopy }) => (
            <ActionsContainer>
              <IconButton
                data-testid='download-qr'
                tooltip={t('tooltips.downloadQr')}
                onClick={() => onDownload(qrCode)}
                type='download'
              />
              <IconButton
                data-testid='copy-qr'
                tooltip={t('tooltips.copyQr')}
                onClick={() => onCopy(qrCode)}
                type='link'
              />
              <IconButton
                data-testid='edit-qr'
                tooltip={t('tooltips.editQr')}
                onClick={() => onEdit(qrCode)}
                type='edit'
              />
              <IconButton
                data-testid='delete-qr'
                tooltip={t('tooltips.deleteQr')}
                onClick={() => onDelete(qrCode)}
                type='delete'
              />
            </ActionsContainer>
          )}
        </TableContext.Consumer>
      )
    }
  }

  const qrDesc = qrCode => {
    if (qrCode.targetType === 'survey') {
      if (!qrCode.survey) {
        return t('components.qrCode.missed')
      }
      return `${qrCode.survey.name} (${qrCode.survey.uniqueName})`
    }

    return qrCode.targetLink
  }

  const columns = [
    {
      title: '',
      key: '0',
      render: qrCode => {
        return <QrCode src={qrCode.qrCodePhoto} />
      },
      width: 80
    },
    {
      title: t('components.qrCode.nameTitle'),
      render: qrCode => {
        return (
          <TitleWrapper>
            <QrCodeName>{qrCode.name}</QrCodeName>
            <QrCodeDesc>{qrDesc(qrCode)}</QrCodeDesc>
          </TitleWrapper>
        )
      }
    },
    actionsColumn
  ]

  return (
    <TableContext.Provider value={{ onCopy, onEdit, onDelete, onDownload }}>
      <Table
        rowKey={qrCode => qrCode.id}
        loading={loading}
        showHeader={false}
        columns={columns}
        dataSource={qrCodes}
        pagination={{
          pageSize: DEFAULT_N_ELEMENTS_PER_PAGE,
          total: total,
          current: page
        }}
        onChange={(page) => onTableChange(page.current)}
      />
    </TableContext.Provider>
  )
}

export default withTranslation()(OperatorQrCodesList)
