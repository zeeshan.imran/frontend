import React from 'react'
import { Link } from 'react-router-dom'
import { Container } from './styles'
import i18next from 'i18next'
import { withTranslation } from 'react-i18next';

const TermsAndPrivacyFooterComponent = ({ isSurvey, t }) => {
  return (
    <Container isSurvey={!!isSurvey}>
      <Link to='/terms-of-use' target='_blank'>{i18next.t('survey.termsOfUse')}</Link>
    </Container>
  )
}

export default withTranslation()(TermsAndPrivacyFooterComponent)
