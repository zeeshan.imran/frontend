import styled from 'styled-components'

export const Container = styled.div`
  text-align: ${({ isSurvey }) => (isSurvey ? 'left' : 'center')};
  margin: 2.4rem 0;
  background-color: transparent;
`
