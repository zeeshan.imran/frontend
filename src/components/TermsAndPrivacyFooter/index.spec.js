import React from 'react'
import { shallow } from 'enzyme'
import TermsAndPrivacyFooterComponent from '.'

describe('TermsAndPrivacyFooterComponent', () => {
  let testRender
  let isSurvey

  beforeEach(() => {
    isSurvey = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render TermsAndPrivacyFooterComponent', async () => {
    testRender = shallow(<TermsAndPrivacyFooterComponent isSurvey={isSurvey} />)
    expect(testRender).toMatchSnapshot()
  })
})
