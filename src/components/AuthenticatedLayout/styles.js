import styled from 'styled-components'
import { components } from '../../utils/Metrics'

export const Page = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`

export const Content = styled.div`
  flex: 1;
  padding-top: ${components.NAVBAR_HEIGHT}rem;
`
