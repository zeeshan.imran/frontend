import React from 'react'
import { shallow } from 'enzyme'
import AuthenticatedLayout from '.';
import { logout, getAuthenticatedUser } from '../../utils/userAuthentication'

window.matchMedia = jest.fn().mockImplementation(query => {
    return {
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(),
        removeListener: jest.fn(),
    };
});

describe('AuthenticatedLayout', () => {
    window.localStorage = {
        getItem: () => '{}'
    }

    let testRender
    let children
    let withFooter
    let handleNavigate
    let navigationEntries
    let mergeNavbarToContent
    let user

    beforeEach(() => {
        const NAVIGATION_ENTRIES = [{
            title: 'Home',
            route: '/'
        },
        {
            title: 'Sign Out',
            action: logout
        }
        ]
        children = '<Container>hello</Container>'
        withFooter = false
        handleNavigate = entry => entry.action ? entry.action() : history.push(entry.route)
        navigationEntries = NAVIGATION_ENTRIES
        mergeNavbarToContent = "test"
        user = getAuthenticatedUser()
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render AuthenticatedLayout', async () => {
        const getEntryName = ({ title }) => title

        testRender = shallow(

            <AuthenticatedLayout
                children={children}
                withFooter={withFooter}
                getEntryName={getEntryName}
                handleNavigate={handleNavigate}
                navigationEntries={navigationEntries}
                mergeNavbarToContent={mergeNavbarToContent}
                user={user}
            />

        )
        expect(testRender).toMatchSnapshot()
    })
});