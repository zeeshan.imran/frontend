import React from 'react'
import { StyledHeader, HeaderLeft, SideBarToggle } from './styles'
import ActionsDropdown from '../ActionsDropdown'
import ImpersonateHeader from '../../../containers/ImpersonateHeader'
import { Icon } from 'antd'

const Header = ({
  username,
  sideBarCollapsed,
  onSideBarToggle,
  hasTasterAccount
}) => (
  <React.Fragment>
    <StyledHeader>
      <HeaderLeft>
        <SideBarToggle collapsed={sideBarCollapsed} onClick={onSideBarToggle}>
          <Icon type='menu-fold' />
        </SideBarToggle>
      </HeaderLeft>
      <ActionsDropdown
        username={username}
        hasTasterAccount={hasTasterAccount}
      />
    </StyledHeader>
    <ImpersonateHeader />
  </React.Fragment>
)

export default Header
