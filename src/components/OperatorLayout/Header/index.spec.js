import React from 'react'
import { shallow } from 'enzyme'
import Header from '.'

describe('Header', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Header', async () => {
    const username = 'Flovor-wiki'
    testRender = shallow(<Header username={username} />)
    expect(testRender).toMatchSnapshot()
  })
})
