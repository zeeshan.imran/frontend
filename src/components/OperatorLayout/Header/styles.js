import styled from 'styled-components'
import { Layout as AntLayout } from 'antd'
import colors from '../../../utils/Colors'

const { Header: AntHeader } = AntLayout

export const StyledHeader = styled(AntHeader)`
  background-color: ${colors.OPERATOR_HEADER};
  box-shadow: 0 1px 4px 0 rgba(0, 21, 41, 0.12);
  display: flex;
  justify-content: flex-end;
  padding-right: 2.8rem;
  padding-left: 2rem;
  z-index: 1;
`

export const HeaderLeft = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
`

export const SideBarToggle = styled.button`
  border: 1px solid ${colors.SIDEBAR_TOGGLE_BORDER_COLOR};
  border-radius: 3px;
  padding: 0.5rem 0.5rem;
  display: flex;
  color: ${colors.SIDEBAR_TOGGLE_COLOR};
  cursor: pointer;
  background: #fff;
  svg {
    width: 2rem;
    height: 2rem;
    transform: rotateY(${({ collapsed }) => (collapsed ? '180' : '0')}deg);
    transition: transform 750ms;
  }
  &:hover {
    background: ${colors.SIDEBAR_HOVER_COLOR};
  }
  &:active,
  &:focus {
    outline: 3px solid ${colors.SIDEBAR_HOVER_COLOR};
  }
`
