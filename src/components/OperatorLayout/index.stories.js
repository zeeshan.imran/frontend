import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import OperatorLayout from './'
import AdminCardSection from '../AdminCardSection'
import AdminFooter from '../AdminFooter'
import AdminPage from '../OperatorPage'
import AdminPageContent from '../OperatorPageContent'

const View = styled.div`
  display: flex;
  height: 100vh;
  width: 100%;
`

const menu = [
  {
    icon: 'dashboard',
    title: 'Dashboard',
    path: 'dashboard',
    content: (
      <AdminPage>
        <AdminPageContent>
          <AdminCardSection title='Dashboard Title'>
            Dashboard Content
          </AdminCardSection>
        </AdminPageContent>

        <AdminFooter
          okText='Save'
          cancelText='Cancel'
          onOk={() => console.log('Clicked Ok Action')}
          onCancel={() => console.log('Clicked Cancel Action')}
          okDisabled
          cancelDisabled={false}
        />
      </AdminPage>
    )
  },
  {
    icon: 'user',
    title: 'Users',
    path: 'users',
    content: (
      <AdminPage>
        <AdminPageContent>
          <AdminCardSection title='Users Title'>Users Content</AdminCardSection>
        </AdminPageContent>
      </AdminPage>
    )
  },
  {
    icon: 'copy',
    title: 'Products & Stores',
    path: 'products-and-stores',
    content: (
      <AdminPage>
        <AdminPageContent>
          <AdminCardSection title='Products & Stores Title'>
            Products & Stores Content
          </AdminCardSection>
        </AdminPageContent>

        <AdminFooter
          okText='Save'
          cancelText='Cancel'
          onOk={() => console.log('Clicked Ok Action')}
          onCancel={() => console.log('Clicked Cancel Action')}
          okDisabled
          cancelDisabled={false}
        />
      </AdminPage>
    )
  },
  {
    icon: 'form',
    title: 'Categories',
    path: 'categories',
    content: (
      <AdminPage>
        <AdminPageContent>
          <AdminCardSection title='Categories Title'>
            Categories Content
          </AdminCardSection>
        </AdminPageContent>

        <AdminFooter
          okText='Save'
          cancelText='Cancel'
          onOk={() => console.log('Clicked Ok Action')}
          onCancel={() => console.log('Clicked Cancel Action')}
          okDisabled
          cancelDisabled={false}
        />
      </AdminPage>
    )
  },
  {
    icon: 'book',
    title: 'Surveys',
    path: 'surveys',
    content: (
      <AdminPage>
        <AdminPageContent>
          <AdminCardSection title='Surveys Title'>
            Surveys Content
          </AdminCardSection>
        </AdminPageContent>

        <AdminFooter
          okText='Save'
          cancelText='Cancel'
          onOk={() => console.log('Clicked Ok Action')}
          onCancel={() => console.log('Clicked Cancel Action')}
          okDisabled
          cancelDisabled={false}
        />
      </AdminPage>
    )
  },
  {
    icon: 'line-chart',
    title: 'Status',
    path: 'status',
    content: (
      <AdminPage>
        <AdminPageContent>
          <AdminCardSection title='Status Title'>
            Status Content
          </AdminCardSection>
        </AdminPageContent>

        <AdminFooter
          okText='Save'
          cancelText='Cancel'
          onOk={() => console.log('Clicked Ok Action')}
          onCancel={() => console.log('Clicked Cancel Action')}
          okDisabled
          cancelDisabled={false}
        />
      </AdminPage>
    )
  },
  {
    icon: 'contacts',
    title: 'Testers Groups',
    path: 'testers-groups',
    content: (
      <AdminPage>
        <AdminPageContent>
          <AdminCardSection title='Testers Groups Title'>
            Testers Groups Content
          </AdminCardSection>
        </AdminPageContent>

        <AdminFooter
          okText='Save'
          cancelText='Cancel'
          onOk={() => console.log('Clicked Ok Action')}
          onCancel={() => console.log('Clicked Cancel Action')}
          okDisabled
          cancelDisabled={false}
        />
      </AdminPage>
    )
  }
]
class StateWrapper extends Component {
  state = {
    selectedMenuEntry: 0
  }

  render () {
    return this.props.children({
      setSelectedMenuEntry: menuEntry =>
        this.setState({ selectedMenuEntry: menuEntry }),
      selectedMenuEntry: this.state.selectedMenuEntry
    })
  }
}

storiesOf('OperatorLayout', module).add('Story', () => (
  <StateWrapper>
    {({ setSelectedMenuEntry, selectedMenuEntry }) => {
      return (
        <View>
          <OperatorLayout
            menu={menu}
            handleMenuEntryClick={({ item, key, keyPath }) =>
              setSelectedMenuEntry(key)
            }
            activeMenuEntry={selectedMenuEntry}
            username='Username'
          >
            {menu[selectedMenuEntry].content}
          </OperatorLayout>
        </View>
      )
    }}
  </StateWrapper>
))
