import React, { useState } from 'react'
import { Layout as AntLayout } from 'antd'
import Header from './Header'
import Sider from './Sider'
import Content from './Content'
import { ContentContainer, TooltipLayer } from './styles'
import { getUIState, updateUIState } from '../../utils/userAuthentication'
import { TOOLTIP_LAYER_ID } from '../../utils/chartUtils'

const OperatorLayout = ({
  menu,
  handleMenuEntryClick,
  activeMenuEntry,
  username,
  hasTasterAccount,
  children
}) => {
  const [collapsed, setCollapsed] = useState(getUIState().isSidebarCollapsed)

  return (
    <AntLayout style={{ width: '100vw' }}>
      <TooltipLayer id={TOOLTIP_LAYER_ID} />
      <Sider
        menu={menu}
        collapsed={collapsed}
        activeMenuEntry={activeMenuEntry}
        handleEntryClick={handleMenuEntryClick}
      />
      <ContentContainer>
        <Header
          hasTasterAccount={hasTasterAccount}
          username={username}
          sideBarCollapsed={collapsed}
          onSideBarToggle={() => {
            const newCollapsed = !collapsed
            setCollapsed(newCollapsed)
            updateUIState({ isSidebarCollapsed: newCollapsed })
          }}
        />
        <Content>{children}</Content>
      </ContentContainer>
    </AntLayout>
  )
}
export default OperatorLayout
