const pngFilters = {
  default:
    'invert(52%) sepia(4%) saturate(5429%) hue-rotate(48deg) brightness(138%) contrast(71%)',
  bunge:
    'invert(11%) sepia(98%) saturate(5692%) hue-rotate(197deg) brightness(90%) contrast(106%)',
  chrHansen:
    'invert(33%) sepia(96%) saturate(2524%) hue-rotate(192deg) brightness(100%) contrast(89%)'
}
// https://codepen.io/sosuke/pen/Pjoqqp

export default pngFilters
