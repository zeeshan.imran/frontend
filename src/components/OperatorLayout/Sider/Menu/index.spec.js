import React from 'react'
import { mount } from 'enzyme'
import { Menu as AntMenu } from 'antd'
import { ApolloProvider } from 'react-apollo-hooks'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import Menu from './index'

describe('Menu', () => {
  window.localStorage = {
    getItem: () => '{}'
  }

  let testRender
  let client
  let entries
  let handleMenuClick
  let activeMenuEntry

  beforeEach(() => {
    entries = [
      {
        icon: 'dashboard',
        title: 'Dashboard',
        path: 'dashboard',
        content: null,
        submenu: [
          {
            path: '/sub-dashboard',
            title: 'Sub dashboard'
          }
        ]
      },
      {
        icon: 'survey',
        title: 'Survey',
        path: 'survey',
        content: null
      }
    ]
    handleMenuClick = jest.fn()
    activeMenuEntry = null
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Menu', async () => {
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <ApolloProvider client={client}>
          <Menu
            entries={entries}
            handleMenuClick={handleMenuClick}
            activeMenuEntry={activeMenuEntry}
          />
        </ApolloProvider>
      </Router>
    )

    expect(testRender.find(Menu)).toHaveLength(1)
    expect(testRender.find(AntMenu.SubMenu)).toHaveLength(1)
    expect(testRender.find(AntMenu.SubMenu).prop('title')).toMatchSnapshot()
    expect(testRender.find(AntMenu.Item).text()).toBe('Survey')

    testRender.find(AntMenu.Item).simulate('click')
    expect(handleMenuClick).toHaveBeenCalledWith('survey')
  })
})
