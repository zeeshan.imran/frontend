import styled from 'styled-components'
import { Layout as AntLayout, Divider as AntDivider } from 'antd'
import colors from '../../../utils/Colors'

const { Sider: AntSider } = AntLayout

export const StyledSider = styled(AntSider)`
  background-color: ${colors.SIDES_COLOR};
  box-shadow: 1px 0 8px 0 rgba(0, 21, 41, 0.12);
  z-index: 1;
  
  .ant-layout-sider-children {
    display: flex;
    flex-direction: column;
    height: 100vh;
  }
`

export const Logo = styled.div`
  background: url('${({ src }) => src}') no-repeat;
  background-size: contain;
  margin-top: 1.7rem;
  margin-left: ${({ collapsed }) => (collapsed ? '3rem' : '2.5rem')};
  width: ${({ collapsed }) => (collapsed ? '2rem' : '15.4rem')};
  height: ${({ collapsed }) => (collapsed ? '2.3rem' : '3.6rem')};
  min-height: ${({ collapsed }) => (collapsed ? '2.3rem' : '3rem')};
`

export const Divider = styled(AntDivider)`
  height: 0.2rem;
  min-height: 0.2rem;
  margin-top: ${({ styled: { collapsed } }) =>
    collapsed ? '2.4rem' : '1.7rem'};
  margin-bottom: ${({ styled: { collapsed } }) =>
    collapsed ? '2.8rem' : '1.4rem'};
`
