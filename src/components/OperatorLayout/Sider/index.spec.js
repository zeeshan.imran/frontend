import React from 'react'
import { shallow } from 'enzyme'
import Sider from '.'

describe('Sider', () => {
  let testRender
  let menu
  let handleEntryClick
  let activeMenuEntry

  beforeEach(() => {
    menu = [
      {
        title: 'Menu 1',
        visible: true
      },
      {
        title: 'Menu 2',
        visible: true
      }
    ]

    handleEntryClick = jest.fn()
    activeMenuEntry = 'Menu 1'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Sider', async () => {
    testRender = shallow(
      <Sider
        collapsed
        menu={menu}
        handleEntryClick={handleEntryClick}
        activeMenuEntry={activeMenuEntry}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
