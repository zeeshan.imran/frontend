import React from 'react'
import Menu from './Menu'
import { StyledSider, Logo, Divider } from './styles'
import { getImages } from '../../../utils/getImages'
import { imagesCollection } from '../../../assets/png'

const { desktopImage, mobileImage } = getImages(imagesCollection)

const Sider = ({ menu, handleEntryClick, activeMenuEntry, collapsed }) => {
  return (
    <StyledSider collapsed={collapsed} theme='light' width={256}>
      {collapsed ? (
        <Logo collapsed src={mobileImage} />
      ) : (
        <Logo src={desktopImage} />
      )}
      <Divider styled={{ collapsed }} />
      {/* This is passed with styled so we don't get the warning
      Check: https://github.com/styled-components/styled-components/issues/1198 */}
      <Menu
        entries={menu}
        activeMenuEntry={activeMenuEntry}
        handleMenuClick={handleEntryClick}
      />
    </StyledSider>
  )
}

export default Sider
