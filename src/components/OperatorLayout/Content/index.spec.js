import React from 'react'
import { shallow } from 'enzyme'
import Content from '.'

describe('Content', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Content', async () => {
    const children = <div>test content</div>
    testRender = shallow(<Content>{children}</Content>)
    expect(testRender).toMatchSnapshot()
  })
})
