import React from 'react'
import { mount } from 'enzyme'
import ActionsDropdown from '.'
import gql from 'graphql-tag'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../../queries/SurveyCreation'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { logout } from '../../../utils/userAuthentication'
import { useSlaask } from '../../../contexts/SlaaskContext'

jest.mock('../../../contexts/SlaaskContext')
jest.mock('../../../utils/userAuthentication')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mockSurveyCreation = {
  products: [],
  questions: [],
  basics: [],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}

const mockFinishSurvey = {
  request: {
    query: gql`
      mutation finishSurvey($input: EnrollmentStateInput) {
        finishSurvey(input: $input)
      }
    `,
    variables: { input: { surveyEnrollment: 'survey-enrollment-1' } }
  },
  result: () => {
    return true
  }
}

describe('ActionsDropdown', () => {
  window.localStorage = {
    getItem: () => '{}'
  }

  let testRender
  let reloadSlaask
  let client

  beforeEach(() => {
    client = createApolloMockClient({
      mocks: [mockFinishSurvey]
    })

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
    reloadSlaask = jest.fn()
    useSlaask.mockImplementation(() => ({ reloadSlaask: reloadSlaask }))
    logout.mockImplementation(() => {})
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ActionsDropdown', async () => {
    const username = 'flovor-wiki'
    const history = createBrowserHistory()
    testRender = mount(
      <Router history={history}>
        <ApolloProvider client={client}>
          <ActionsDropdown username={username} />
        </ApolloProvider>
      </Router>
    )
    expect(testRender.find(ActionsDropdown)).toHaveLength(1)

    testRender.find(ActionsDropdown).simulate('click')
    testRender.find('[data-testid="logout-item"]').simulate('click')

    expect(logout).toHaveBeenCalled()
    expect(reloadSlaask).toHaveBeenCalled()
  })
})
