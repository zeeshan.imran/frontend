import React from 'react'
import { Dropdown, Menu } from 'antd'
import { logout } from '../../../utils/userAuthentication'
import { Label } from './styles'
import { useSlaask } from '../../../contexts/SlaaskContext'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

const ActionsDropdown = ({ username, hasTasterAccount, history }) => {
  const { t } = useTranslation()
  const { reloadSlaask } = useSlaask()
  return (
    <Dropdown
      placement='bottomCenter'
      overlay={
        <Menu>
          {hasTasterAccount && (
            <Menu.Item key='switchToTaster'>
              <a
                onClick={() => {
                  history.push('/taster')
                }}
              >
                Switch to Taster Account
              </a>
            </Menu.Item>
          )}
          <Menu.Item key='logout'>
            <a
              data-testid='logout-item'
              onClick={() => {
                logout()
                reloadSlaask()
              }}
            >
              {t('signOut')}
            </a>
          </Menu.Item>
        </Menu>
      }
      trigger={['click']}
    >
      <Label>{username}</Label>
    </Dropdown>
  )
}

export default withRouter(ActionsDropdown)
