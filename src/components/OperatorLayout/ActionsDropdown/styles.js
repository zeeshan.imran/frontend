import styled from 'styled-components'
import { family } from '../../../utils/Fonts'

export const Label = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
  cursor: pointer;
  user-select: none;
`
