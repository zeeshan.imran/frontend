import React from 'react'
import { Spin, Typography } from 'antd'
import {
  InfoContainer
} from '../styles'

const { Title } = Typography

const Running = () => {
  return (
    <InfoContainer>
      <Spin />
      <Title level={4}>We are analyzing the information...</Title>
    </InfoContainer>
  )
}

export default Running
