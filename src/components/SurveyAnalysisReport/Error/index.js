import React from 'react'
import { Icon, Typography } from 'antd'
import { InfoContainer } from '../styles'
import getErrorMessage from '../../../utils/getErrorMessage'

const { Title } = Typography

const Error = ({ error }) => (
  <InfoContainer>
    <Title level={4}>
      <Icon type='warning' theme='twoTone' twoToneColor='#eb2f96' />{' '}
      {getErrorMessage(error, 'ANALYSIS_ERROR')}
    </Title>
  </InfoContainer>
)

export default Error
