import React, { useState } from 'react'
import { Tree, Icon, Col } from 'antd'
import SurveyAnalysisFile from '../../../containers/SurveyAnalysisFile'
import { Row } from '../styles'

const FileBrower = ({ jobId, files }) => {
  const [selectedFile, selectFile] = useState(null)
  const treeData = {
    key: 'files',
    title: 'Files',
    children: files.map(file => ({
      key: file.name,
      title: file.name,
      switcherIcon: <Icon type='file-excel' />
    }))
  }
  return (
    <Row type='flex'>
      <Col span={5}>
        <Tree
          showLine
          showIcon={false}
          expandedKeys={['files']}
          onSelect={keys => {
            const file = files.find(f => keys.includes(f.name))
            selectFile(file)
          }}
          treeData={treeData}
        />
      </Col>
      <Col span={19}>
        {selectedFile && (
          <SurveyAnalysisFile jobId={jobId} file={selectedFile} />
        )}
      </Col>
    </Row>
  )
}

export default FileBrower
