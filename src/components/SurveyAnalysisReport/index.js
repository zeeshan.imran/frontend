import React from 'react'
import { Icon, Spin } from 'antd'
import { Tabs, TabPane, CompactContainer } from './styles'
import Running from './Running'
import FileBrower from './FileBrowser'
import Error from './Error'

const TAB_ICONS = {
  RUNNING: <Spin size='small' />,
  ERROR: <Icon type='warning' theme='twoTone' twoToneColor='#eb2f96' />,
  OK: <Icon type='check-circle' theme='twoTone' twoToneColor='#52c41a' />
}

const SurveyAnalysisReport = ({ jobResults }) => {
  return (
    <Tabs animated={{ inkBar: true, tabPane: false }}>
      {jobResults.map(job => (
        <TabPane
          key={job.id}
          tab={
            <span>
              {TAB_ICONS[job.status]} {job.name}
            </span>
          }
        >
          <CompactContainer>
            {job.status === 'RUNNING' && <Running />}
            {job.status === 'ERROR' && <Error error={job.message} />}
            {job.status === 'OK' && (
              <FileBrower jobId={job.id} files={job.files} />
            )}
          </CompactContainer>
        </TabPane>
      ))}
    </Tabs>
  )
}

export default SurveyAnalysisReport
