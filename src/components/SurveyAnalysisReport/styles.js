import { Row as AntdRow, Tabs as AntdTabs } from 'antd'
import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Row = styled(AntdRow)`
  min-height: 400px;
  > *:first-child {
    padding: 0.5rem;
    border-right: 1px solid #ddd;
    overflow-x: hidden;
  }
`
export const Tabs = styled(AntdTabs)`
  .ant-tabs-bar {
    margin: 0 0;
    background-color: #fff;
  }
`

export const TabPane = AntdTabs.TabPane

export const CompactContainer = styled.div`
  background-color: ${colors.WHITE};
`

export const InfoContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${colors.WHITE};
  min-height: 280px;

  .ant-spin {
    margin-right: 1em;
  }

  .ant-typography {
    margin-bottom: 0.25em;
  }
`
