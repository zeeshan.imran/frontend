import styled from 'styled-components'
import { Icon } from 'antd'

export const StyledIcon = styled(Icon)`
  cursor: pointer;
  padding: 0 1rem;
  margin-top: ${({ isinlabel }) => (isinlabel ? '0.3rem' : 0)};
`

export const TooltipContent = styled.div`
  ul {
    margin: 0.5rem 0 0 0;
    padding-left: 2rem;
    min-width: 180px;
    li {
      padding-bottom: 0.5rem;
    }
  }
`
