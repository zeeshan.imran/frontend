import React from 'react'
import Csv from './CSV'
import Html from './Html'
import Image, { isSupportedImage } from './Image'
import NotSupport from './NotSupport'

const SurveyAnalysisFile = ({ file, content }) => {
  if (file.type === 'csv') {
    return <Csv file={file} content={content} />
  }

  if (file.type === 'html' && content) {
    return <Html file={file} content={content} />
  }

  if (isSupportedImage(file) && content) {
    return <Image file={file} content={content} />
  }

  return <NotSupport file={file} />
}

export default SurveyAnalysisFile
