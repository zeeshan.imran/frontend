import React from 'react'
import { IFrame } from '../styles'
import base64ToUrl from '../../../utils/base64ToUrl'

const Html = ({ content }) => (
  <IFrame
    title='Survey Analysis Report'
    src={base64ToUrl(content, 'text/html')}
  />
)

export default Html
