import React from 'react'
import { ContentWrapper } from '../styles'
import base64ToUrl from '../../../utils/base64ToUrl'

const SUPPORTED_MIME_TYPES = {
  png: 'image/png'
}

const Image = ({ file, content }) => (
  <ContentWrapper>
    <img
      className='chart'
      alt='Survey Analysis Report'
      src={base64ToUrl(content, SUPPORTED_MIME_TYPES[file.type])}
    />
  </ContentWrapper>
)

export const isSupportedImage = (file) => !!SUPPORTED_MIME_TYPES[file.type]
export default Image
