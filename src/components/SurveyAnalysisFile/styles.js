import styled from 'styled-components'

export const IFrame = styled.iframe`
  border: none;
  width: 100%;
  height: 100%;
`

export const ContentWrapper = styled.div`
  padding: 1rem;
  .chart {
    width: 100%;
    height: 100%;
  }
`
