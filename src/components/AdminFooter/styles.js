import styled from 'styled-components'
import { Layout as AntLayout } from 'antd'
import Button from '../Button'
import colors from '../../utils/Colors'
import { COMPONENTS_DEFAULT_MARGIN } from '../../utils/Metrics'

const { Footer: AntFooter } = AntLayout

export const StyledFooter = styled(AntFooter)`
  display: flex;
  justify-content: flex-end;
  background-color: ${colors.WHITE};
  box-shadow: 1px 0 8px 0 rgba(0, 21, 41, 0.12);
  height: 5.6rem;
  padding: 1.2rem ${COMPONENTS_DEFAULT_MARGIN}rem 1.2rem 0;
  width: 100%;
`

const StyledButton = styled(Button)`
  width: 14.5rem;
`

export const OkButton = styled(StyledButton)`
  margin-left: 1rem;
`

export const CancelButton = styled(StyledButton)``
