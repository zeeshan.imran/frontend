import React from 'react'
import { Formik } from 'formik'
import { Row, Col, Form } from 'antd'
import OptionsSection from '../OptionsSection'
import { getDefaultSelectOptions } from '../../../utils/defaultQuestionValues'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { minOptions } from '../../../validates/questions/select-and-justify'
import ChartSection from '../ChartSection'
import { useTranslation } from 'react-i18next'
import useValidateWithContext from '../../../hooks/useValidateWithContext'

const SelectAndJustify = ({
  id,
  clientGeneratedId,
  validationSchema,
  chartTypes,
  prompt,
  secondaryPrompt,
  handleFieldChange,
  options = [],
  skipFlow,
  chartTitle = '',
  chartTopic = '',
  chartType = ''
}) => {
  if (options.length < minOptions) {
    options = getDefaultSelectOptions(minOptions)
  }
  const { t } = useTranslation()
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes }),
    [chartTypes]
  )

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        options: options || [],
        secondaryPrompt,
        chartTitle,
        chartTopic,
        chartType
      }}
      render={({ errors, setFieldValue, values }) => (
        <React.Fragment>
          <Row gutter={24}>
            <Col lg={16} md={24}>
              <Form.Item
                help={errors.prompt}
                validateStatus={errors.prompt ? 'error' : 'success'}
              >
                <InputArea
                  autoSize
                  name='prompt'
                  value={values.prompt}
                  onChange={event => {
                    setFieldValue('prompt', event.target.value)
                    handleFieldChange({ prompt: event.target.value })
                  }}
                  label={t(
                    'components.questionCreation.selectAndJustify.prompt'
                  )}
                  tooltip={t('tooltips.questionCreation.prompt')}
                  size='default'
                  required
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col lg={16} md={24}>
              <Form.Item
                help={errors.secondaryPrompt}
                validateStatus={errors.secondaryPrompt ? 'error' : 'success'}
              >
                <InputArea
                  autoSize
                  name='secondaryPrompt'
                  value={values.secondaryPrompt}
                  onChange={event => {
                    setFieldValue('secondaryPrompt', event.target.value)
                    handleFieldChange({ secondaryPrompt: event.target.value })
                  }}
                  label={t(
                    'components.questionCreation.selectAndJustify.secondaryPrompt'
                  )}
                  tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                  size='default'
                  required
                />
              </Form.Item>
            </Col>
          </Row>
          <OptionsSection
            id={id}
            clientGeneratedId={clientGeneratedId}
            label={t('components.questionCreation.selectAndJustify.options')}
            options={values.options}
            onChangeOptions={updatedOptions => {
              setFieldValue('options', updatedOptions)
              handleFieldChange({
                options: updatedOptions
              })
            }}
            errors={errors.options}
            skipFlow={skipFlow}
            onSkipFlowChange={nextSkipFlow => {
              setFieldValue('skipFlow', nextSkipFlow)
              handleFieldChange({
                skipFlow: nextSkipFlow
              })
            }}
          />
          <ChartSection
            isNewQuestion={!id}
            chartTypes={chartTypes}
            values={values}
            errors={errors}
            onFieldChange={(name, value) => {
              setFieldValue(name, value)
              handleFieldChange({ [name]: value })
            }}
          />
        </React.Fragment>
      )}
    />
  )
}

export default SelectAndJustify
