import React from 'react'
import { mount } from 'enzyme'
import SelectAndJustify from '.'
import OptionsSection from '../OptionsSection'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

describe('SelectAndJustify', () => {
  let testRender
  let validationSchema
  let prompt
  let secondaryPrompt
  let handleFieldChange
  let options
  let skipToOptions
  let client

  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    secondaryPrompt = ''
    handleFieldChange = jest.fn()
    options = []
    skipToOptions = []
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SelectAndJustify', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SelectAndJustify
          validationSchema={validationSchema}
          prompt={prompt}
          secondaryPrompt={secondaryPrompt}
          handleFieldChange={handleFieldChange}
          options={options}
          skipToOptions={skipToOptions}
        />
      </ApolloProvider>
    )
    expect(testRender.find(SelectAndJustify)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })
  })

  test('should render SelectAndJustify of InputArea for secondaryPrompt', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SelectAndJustify
          validationSchema={validationSchema}
          secondaryPrompt={secondaryPrompt}
          handleFieldChange={handleFieldChange}
          options={options}
          skipToOptions={skipToOptions}
        />
      </ApolloProvider>
    )
    testRender
      .findWhere(
        c => c.name() === 'InputArea' && c.prop('name') === 'secondaryPrompt'
      )
      .last()
      .prop('onChange')({
      target: {
        value: 'secondaryPrompt'
      }
    })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })

    testRender.find(OptionsSection).prop('onChangeOptions')()

    expect(handleFieldChange).toHaveBeenCalled()
  })

  test('should render ChartSection select onchange event', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SelectAndJustify
          validationSchema={validationSchema}
          secondaryPrompt={secondaryPrompt}
          handleFieldChange={handleFieldChange}
          options={options}
          skipToOptions={skipToOptions}
        />
      </ApolloProvider>
    )

    testRender
      .findWhere(c => c.name() === 'ChartSection')
      .first()
      .prop('onFieldChange')('chartTopic', 'pie')

    expect(handleFieldChange).toHaveBeenCalledWith({
      chartTopic: 'pie'
    })

    testRender
      .findWhere(c => c.name() === 'OptionsSection')
      .last()
      .prop('onSkipFlowChange')({ skipFlow: 'skipFlow' })
  })
})
