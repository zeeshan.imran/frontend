import React from 'react'
import { mount } from 'enzyme'
import ChooseProduct from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import gql from 'graphql-tag'
import SkipToPicker from '../SkipToPicker'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
}))

const mockSurveyCreation = {
  products: [
    {
      id: 1,
      brandId: 'DVPQNphWoC86Rsg8PEUC7Y',
      name: 'Schweizer Wurst',
      brand: {
        name: 'Bell'
      }
    },
    {
      id: 2,
      brandId: 'DVPQNphWoC86Rsg8PEUC7Y',
      name: 'Schweizer Wurst',
      brand: {
        name: 'Bell'
      }
    }
  ],
  questions: [],
  basics: [],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
    }
  }
`

describe('ChooseProduct', () => {
  let testRender
  let validationSchema
  let prompt
  let secondaryPrompt
  let handleFieldChange
  let chooseProductOptions
  let skipToOptions
  let productsSkip
  let client

  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    secondaryPrompt = ''
    handleFieldChange = jest.fn()
    chooseProductOptions = {}
    skipToOptions = []
    productsSkip = []

    client = createApolloMockClient()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ChooseProduct', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseProduct
          validationSchema={validationSchema}
          prompt={prompt}
          secondaryPrompt={secondaryPrompt}
          handleFieldChange={handleFieldChange}
          chooseProductOptions={chooseProductOptions}
          skipToOptions={skipToOptions}
          productsSkip={productsSkip}
        />
      </ApolloProvider>
    )
    expect(testRender.find(ChooseProduct)).toHaveLength(1)
  })

  test('should edit fields', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseProduct
          validationSchema={validationSchema}
          prompt={prompt}
          secondaryPrompt={secondaryPrompt}
          handleFieldChange={handleFieldChange}
          chooseProductOptions={chooseProductOptions}
          skipToOptions={skipToOptions}
          productsSkip={productsSkip}
        />
      </ApolloProvider>
    )

    testRender
      .find({ name: 'prompt' })
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })
    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })

    handleFieldChange.mockClear()
    testRender
      .find({ name: 'secondaryPrompt' })
      .first()
      .prop('onChange')({ target: { value: 'secondaryPrompt' } })
    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })

    handleFieldChange.mockClear()
    testRender
      .find({ name: 'chooseProductOptions.minimumProducts' })
      .first()
      .prop('onChange')({ target: { value: '1' } })
    expect(handleFieldChange).toHaveBeenCalledWith({
      chooseProductOptions: {
        ...chooseProductOptions,
        minimumProducts: '1'
      }
    })

    handleFieldChange.mockClear()
    testRender
      .find({ name: 'chooseProductOptions.maximumProducts' })
      .first()
      .prop('onChange')({ target: { value: '4' } })
    expect(handleFieldChange).toHaveBeenCalledWith({
      chooseProductOptions: {
        ...chooseProductOptions,
        maximumProducts: '4'
      }
    })
  })

  test('should not render number inputs when there only one product', () => {
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          ...mockSurveyCreation,
          products: []
        }
      }
    })
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseProduct
          validationSchema={validationSchema}
          prompt={prompt}
          secondaryPrompt={secondaryPrompt}
          handleFieldChange={handleFieldChange}
          chooseProductOptions={chooseProductOptions}
          skipToOptions={skipToOptions}
          productsSkip={productsSkip}
        />
      </ApolloProvider>
    )

    expect(
      testRender.find({ name: 'chooseProductOptions.minimumProducts' })
    ).toHaveLength(0)
    expect(
      testRender.find({ name: 'chooseProductOptions.maximumProducts' })
    ).toHaveLength(0)
  })

  test('show and edit skip to values', () => {
    const skipFlow = {
      rules: [{ minValue: 1, maxValue: 20 }]
    }

    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseProduct
          validationSchema={validationSchema}
          prompt={prompt}
          secondaryPrompt={secondaryPrompt}
          handleFieldChange={handleFieldChange}
          chooseProductOptions={chooseProductOptions}
          skipToOptions={skipToOptions}
          productsSkip={productsSkip}
          skipFlow={skipFlow}
        />
      </ApolloProvider>
    )
    expect(testRender.find({ name: 'skipTo' })).toHaveLength(0)
    testRender
      .find('Button')
      .last()
      .prop('onClick')()

    testRender.update()

    testRender
      .find(SkipToPicker)
      .first()
      .props()
      .onSkipChange()

    testRender
      .find(SkipToPicker)
      .first()
      .props()
      .onSkipRemove()
  })
})
