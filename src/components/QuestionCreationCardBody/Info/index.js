import React from 'react'
import { Formik } from 'formik'
import { Form, Row, Col } from 'antd'
import InputRichText from '../../InputRichText'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'

const InfoForm = ({
  validationSchema,
  handleFieldChange,
  prompt,
  secondaryPrompt
}) => {
  const formRef = useQuestionValidate()
  const { t } = useTranslation();
  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validationSchema={validationSchema}
      initialValues={{
        prompt,
        secondaryPrompt
      }}
      render={({ errors, setFieldValue, values }) => {
        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={errors.prompt}
                  validateStatus={errors.prompt ? 'error' : 'success'}
                >
                  <InputArea
                    autoSize
                    name='prompt'
                    value={values.prompt}
                    onChange={event => {
                      setFieldValue('prompt', event.target.value)
                      handleFieldChange({
                        prompt: event.target.value,
                        requiredQuestion: false
                      })
                    }}
                    label={t('components.questionCreation.infoForm.prompt')}
                    tooltip={t('tooltips.questionCreation.prompt')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={errors.secondaryPrompt}
                  validateStatus={errors.secondaryPrompt ? 'error' : 'success'}
                >
                  <InputRichText
                    outerAlign='left'
                    name='secondaryPrompt'
                    value={values.secondaryPrompt}
                    onChange={value => {
                      setFieldValue('secondaryPrompt', value)
                      handleFieldChange({
                        secondaryPrompt: value,
                        requiredQuestion: false
                      })
                    }}
                    label={t('components.questionCreation.infoForm.secondaryPrompt')} 
                    tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
          </React.Fragment>
        )
      }}
    />
  )
}

export default InfoForm
