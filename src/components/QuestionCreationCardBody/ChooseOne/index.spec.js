import React from 'react'
import { mount } from 'enzyme'
import ChooseOne from './index'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

describe('skipTo support', () => {
  let testRender
  let client

  const mockOptions = [
    {
      label: 'Option 1',
      value: 'OP_1'
    },
    {
      label: 'Option 2',
      value: 'OP_2'
    }
  ]

  beforeEach(() => {
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should assign skipTo = null (not (string)"null") when click on Remove skip', async () => {
    let handleChange = jest.fn()
    const mockOptions_min = [
      {
        label: 'Option 1',
        value: 'OP_1'
      }
    ]

    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseOne
          prompt=''
          handleFieldChange={handleChange}
          options={mockOptions_min}
          questionIndex={0}
        />
      </ApolloProvider>
    )

    expect(testRender.find(ChooseOne)).toHaveLength(1)
  })

  test('should call handleChange after changing input fields', async () => {
    let handleChange = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseOne
          prompt=''
          handleFieldChange={handleChange}
          options={mockOptions}
          questionIndex={0}
        />
      </ApolloProvider>
    )

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    testRender
      .findWhere(c => c.name() === 'ChartSection')
      .first()
      .prop('onFieldChange')('chartTopic', 'pie')

    expect(handleChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })

    expect(handleChange).toHaveBeenCalledWith({
      chartTopic: 'pie'
    })
  })

  test('should call secondaryPrompt inputArea', async () => {
    let handleChange = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseOne
          secondaryPrompt=''
          handleFieldChange={handleChange}
          options={mockOptions}
          questionIndex={0}
        />
      </ApolloProvider>
    )

    testRender
      .findWhere(
        c => c.name() === 'InputArea' && c.prop('name') === 'secondaryPrompt'
      )
      .first()
      .prop('onChange')({ target: { value: 'secondaryPrompt' } })

    expect(handleChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })
  })

  test('should call OptionsSection', () => {
    let handleChange = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseOne
          secondaryPrompt=''
          handleFieldChange={handleChange}
          options={mockOptions}
          questionIndex={0}
        />
      </ApolloProvider>
    )

    testRender
      .findWhere(c => c.name() === 'OptionsSection')
      .first()
      .prop('onChangeOptions')(['1', '2'])

    expect(handleChange).toHaveBeenCalledWith({ options: ['1', '2'] })

    testRender
      .findWhere(c => c.name() === 'OptionsSection')
      .first()
      .prop('onSkipFlowChange')(['1', '2'])
  })
})
