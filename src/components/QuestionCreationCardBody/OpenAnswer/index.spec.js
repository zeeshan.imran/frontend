import React from 'react'
import { mount } from 'enzyme'
import OpenAnswer from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('OpenAnswer', () => {
  let testRender
  let validationSchema
  let prompt
  let secondaryPrompt
  let handleFieldChange

  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    secondaryPrompt = ''
    handleFieldChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OpenAnswer', async () => {
    testRender = mount(
      <OpenAnswer
        validationSchema={validationSchema}
        secondaryPrompt={secondaryPrompt}
        prompt={prompt}
        handleFieldChange={handleFieldChange}
      />
    )
    expect(testRender.find(OpenAnswer)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })
  })

  test('should render OpenAnswer of InputArea for secondaryPrompt', async () => {
    testRender = mount(
      <OpenAnswer
        validationSchema={validationSchema}
        secondaryPrompt={secondaryPrompt}
        prompt={prompt}
        handleFieldChange={handleFieldChange}
      />
    )
    testRender
      .findWhere(
        c => c.name() === 'InputArea' && c.prop('name') === 'secondaryPrompt'
      )
      .last()
      .prop('onChange')({
        target: {
          value: 'secondaryPrompt'
        }
      })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })
  })
})
