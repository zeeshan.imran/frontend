import React from 'react'
import { mount } from 'enzyme'
import OptionalSection from '.'
import { Container } from './styles'
import Checkbox from '../../Checkbox'
import { act } from 'react-dom/test-utils'

describe('OptionalSection', () => {
  let testRender
  let label
  let onClickCheckbox
  let isExpanded
  let children

  beforeEach(() => {
    label = 'Please select product'
    onClickCheckbox = jest.fn()
    isExpanded = false
    children = <Container>Products Description</Container>
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OptionalSection', async () => {
    testRender = mount(
      <OptionalSection
        label={label}
        onClickCheckbox={onClickCheckbox}
        isExpanded={isExpanded}
        children={children}
      />
    )
    expect(testRender.find(OptionalSection)).toHaveLength(1)

    act(() => {
      testRender
        .find(Checkbox)
        .first()
        .prop('onChange')({ isValid: true, value: 1 })
    })

    expect(onClickCheckbox).toHaveBeenCalledWith({
      value: 1,
      isValid: true
    })
  })
})
