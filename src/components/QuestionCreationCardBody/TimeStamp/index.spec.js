import React from 'react'
import { mount } from 'enzyme'
import TimeStamp from '.'
import { BrowserRouter as Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

describe('TimeStamp', () => {
  let testRender
  let validationSchema
  let prompt
  let secondaryPrompt
  let handleFieldChange
  let chartTitle
  let chartTopic
  let chartType
  let region
  let client
  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    secondaryPrompt = ''
    handleFieldChange = jest.fn()
    chartTitle = ''
    chartTopic = ''
    chartType = ''
    region = ''
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render TimeStamp', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router>
          <TimeStamp
            validationSchema={validationSchema}
            prompt={prompt}
            secondaryPrompt={secondaryPrompt}
            handleFieldChange={handleFieldChange}
            chartTitle={chartTitle}
            chartTopic={chartTopic}
            chartType={chartType}
            region={region}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(TimeStamp)).toHaveLength(1)
  })
})
