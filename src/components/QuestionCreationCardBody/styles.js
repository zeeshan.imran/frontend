import styled from 'styled-components'

export const Container = styled.div`
  border-top: solid 0.1rem #e9e9e9;
  padding: 2.5rem 3.5rem 0.1rem;
`
