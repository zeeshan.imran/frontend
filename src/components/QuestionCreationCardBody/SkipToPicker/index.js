import React, { useState } from 'react'
import Select from '../../Select'
import { Divider, Icon } from 'antd'
import { RemoveSkipButton } from '../OptionsSection/styles'
import {
  getOptionValue,
  getOptionName
} from '../../../utils/getters/OptionGetters'
import { useTranslation } from 'react-i18next'

const SkipToPicker = ({ value, options, onSkipRemove, onSkipChange }) => {
  const { t } = useTranslation()
  const [open, setOpen] = useState(false)
  return (
    <Select
      name='skipTo'
      size='default'
      open={open}
      value={value}
      placeholder='Jump to question'
      dropdownRender={menu => (
        <div>
          {menu}
          <Divider style={{ margin: '0 0' }} />
          <div>
            <RemoveSkipButton
              onMouseDown={e => e.preventDefault()}
              onClick={() => {
                onSkipRemove()
                setOpen(false)
              }}
            >
              <Icon type='delete' />{' '}
              {t('containers.questionsList.skipToOptions.removeSkip')}
            </RemoveSkipButton>
          </div>
        </div>
      )}
      options={options}
      getOptionValue={getOptionValue}
      getOptionName={getOptionName}
      onDropdownVisibleChange={value => setOpen(value)}
      onChange={onSkipChange}
    />
  )
}

export default SkipToPicker
