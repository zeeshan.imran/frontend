import React from 'react'
import { Row, Col, Form } from 'antd'
import { Formik } from 'formik'
import OptionsSection from '../OptionsSection'
import { getDefaultSelectOptions } from '../../../utils/defaultQuestionValues'
import { minOptions } from '../../../validates/questions/choose-one'
import InputArea from '../../InputArea'
import { withTranslation } from 'react-i18next'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import Input from '../../Input'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import ChartSection from '../ChartSection'
import ImageAndLabelSetting from '../ImageAndLabelSetting'

const ChooseMultiple = ({
  id,
  clientGeneratedId,
  validationSchema,
  chartTypes,
  prompt = '',
  handleFieldChange,
  options = [],
  settings,
  chartTitle = '',
  chartTopic = '',
  chartType = '',
  secondaryPrompt,
  t,
  skipFlow,
  optionDisplayType
}) => {
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes }),
    [chartTypes]
  )
  if (options.length < minOptions) {
    options = getDefaultSelectOptions(minOptions)
  }

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        options,
        settings,
        chartTitle,
        chartTopic,
        chartType,
        secondaryPrompt,
        t
      }}
      render={({ errors, setFieldValue, values }) => (
        <React.Fragment>
          <Row gutter={24}>
            <Col lg={16} md={24}>
              <Form.Item
                help={errors.prompt}
                validateStatus={errors.prompt ? 'error' : 'success'}
              >
                <InputArea
                  data-testid={'choose-multiple-question-text'}
                  autoSize
                  name='prompt'
                  value={values.prompt}
                  onChange={event => {
                    setFieldValue('prompt', event.target.value)
                    handleFieldChange({ prompt: event.target.value })
                  }}
                  label={t('components.questionCreation.prompt')}
                  tooltip={t('tooltips.questionCreation.prompt')}
                  size='default'
                  required
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col lg={16} md={24}>
              <Form.Item>
                <InputArea
                  autoSize
                  name='secondaryPrompt'
                  value={values.secondaryPrompt}
                  onChange={event => {
                    setFieldValue('secondaryPrompt', event.target.value)
                    handleFieldChange({ secondaryPrompt: event.target.value })
                  }}
                  label={t('components.questionCreation.secondaryPrompt')}
                  tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                  size='default'
                />
              </Form.Item>
            </Col>
          </Row>
          <OptionsSection 
            id={id}
            clientGeneratedId={clientGeneratedId}
            label={'Answers'}
            options={values.options}
            onChangeOptions={updatedOptions => {
              setFieldValue('options', updatedOptions)
              handleFieldChange({
                options: updatedOptions
              })
            }}
            errors={errors.options}
            skipFlow={skipFlow}
            onSkipFlowChange={nextSkipFlow => {
              setFieldValue('skipFlow', nextSkipFlow)
              handleFieldChange({
                skipFlow: nextSkipFlow
              })
            }}
            isOpenAnswerPossible
            attachedImage
          />
          <Row gutter={24}>
            <Col lg={8} md={12}>
              <Form.Item
                help={errors.settings && errors.settings.minAnswerValues}
                validateStatus={
                  errors.settings && errors.settings.minAnswerValues
                    ? 'error'
                    : 'success'
                }
              >
                <Input
                  name='settings.minAnswerValues'
                  type='number'
                  min='1'
                  step='1'
                  value={values.settings && values.settings.minAnswerValues}
                  onChange={event => {
                    const value = event.target.value || null
                    setFieldValue('settings.minAnswerValues', value)
                    handleFieldChange({
                      settings: {
                        ...settings,
                        minAnswerValues: value
                      }
                    })
                  }}
                  label={t(
                    'components.questionCreation.chooseMultiple.minAnswerValues',
                    {}
                  )}
                  size='default'
                />
              </Form.Item>
            </Col>
            <Col lg={8} md={12}>
              <Form.Item
                help={errors.settings && errors.settings.maxAnswerValues}
                validateStatus={
                  errors.settings && errors.settings.maxAnswerValues
                    ? 'error'
                    : 'success'
                }
              >
                <Input
                  name='settings.maxAnswerValues'
                  type='number'
                  min='1'
                  step='1'
                  value={values.settings && values.settings.maxAnswerValues}
                  onChange={event => {
                    const value = event.target.value || null
                    setFieldValue('settings.maxAnswerValues', value)
                    handleFieldChange({
                      settings: {
                        ...settings,
                        maxAnswerValues: value
                      }
                    })
                  }}
                  label={t(
                    'components.questionCreation.chooseMultiple.maxAnswerValues',
                    {}
                  )}
                  size='default'
                />
              </Form.Item>
            </Col>
          </Row>
          <ImageAndLabelSetting
            isNewQuestion={!id}
            optionDisplayType={optionDisplayType}
            onFieldChange={handleFieldChange}
          />
          <ChartSection
            isNewQuestion={!id}
            chartTypes={chartTypes}
            values={values}
            errors={errors}
            onFieldChange={(name, value) => {
              setFieldValue(name, value)
              handleFieldChange({ [name]: value })
            }}
          />
        </React.Fragment>
      )}
    />
  )
}

export default withTranslation()(ChooseMultiple)
