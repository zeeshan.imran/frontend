import React from 'react'
import { mount } from 'enzyme'
import ChooseMultiple from './index'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

describe('QuestionCreation -> ChooseMultiple', () => {
  let testRender
  const mockOptions = [
    {
      label: '1',
      value: '1-v'
    },
    {
      label: '2',
      value: '2-v'
    }
  ]
  let client

  beforeEach(() => {
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should call handleChange after changing input fields', async () => {
    let handleChange = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseMultiple
          prompt=''
          handleFieldChange={handleChange}
          options={mockOptions}
          questionIndex={0}
        />
      </ApolloProvider>
    )

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    testRender
      .findWhere(c => c.name() === 'ChartSection')
      .first()
      .prop('onFieldChange')('chartTopic', 'pie')

    testRender
      .findWhere(c => c.name() === 'OptionsSection')
      .first()
      .prop('onChangeOptions')(['1', '2'])

    expect(handleChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })

    expect(handleChange).toHaveBeenCalledWith({
      chartTopic: 'pie'
    })

    expect(handleChange).toHaveBeenCalledWith({
      options: ['1', '2']
    })
  })

  test('should set default values', async () => {
    let handleChange = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseMultiple 
          handleFieldChange={handleChange} 
          questionIndex={0}
        />
      </ApolloProvider>
    )

    const initialValues = testRender
      .findWhere(c => c.name() === 'Formik')
      .first()
      .prop('initialValues')

    expect(initialValues.options.length).toBe(2)
    expect(initialValues.prompt).toBe('')
  })

  test('should call secondaryPrompt inputArea', async () => {
    let handleChange = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <ChooseMultiple
          secondaryPrompt=''
          handleFieldChange={handleChange}
          options={mockOptions}
          questionIndex={0}
        />
      </ApolloProvider>
    )

    testRender
      .findWhere(
        c => c.name() === 'InputArea' && c.prop('name') === 'secondaryPrompt'
      )
      .first()
      .prop('onChange')({ target: { value: 'secondaryPrompt' } })

    expect(handleChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })
  })
})
