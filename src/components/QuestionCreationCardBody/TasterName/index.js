import React, { useCallback } from 'react'
import { Formik } from 'formik'
import { Form, Row, Col } from 'antd'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import Input from '../../Input'
import Select from '../../Select'
import Checkbox from '../../Checkbox'
import { CheckboxContainer } from './styles'
import { withTranslation } from 'react-i18next'
import ChartSection from '../ChartSection'
import useValidateWithContext from '../../../hooks/useValidateWithContext'

const answerFormats = ['any', 'letters-and-digits', 'digits']
const componentKey = 'components.questionCreation.tasterName'

const TasterName = ({
  id,
  validationSchema,
  prompt,
  settings,
  secondaryPrompt,
  handleFieldChange,
  chartTypes,
  chartTitle,
  chartTopic,
  chartType,
  t
}) => {
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes }),
    [chartTypes]
  )
  const getFormatName = useCallback(
    format => (format ? t(`${componentKey}.answerFormats.${format}`) : ''),
    [t]
  )

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        settings,
        chartTitle,
        chartTopic,
        chartType
      }}
      render={({ errors, setFieldValue, values }) => {
        const { prompt: promptError } = errors

        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={promptError}
                  validateStatus={promptError ? 'error' : 'success'}
                >
                  <InputArea
                    autoSize
                    name='prompt'
                    value={values.prompt}
                    onChange={event => {
                      setFieldValue('prompt', event.target.value)
                      handleFieldChange({ prompt: event.target.value })
                    }}
                    label={t('components.questionCreation.prompt')}
                    tooltip={t('tooltips.questionCreation.prompt')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item>
                  <InputArea
                    autoSize
                    name='secondaryPrompt'
                    value={values.secondaryPrompt}
                    onChange={event => {
                      setFieldValue('secondaryPrompt', event.target.value)
                      handleFieldChange({ secondaryPrompt: event.target.value })
                    }}
                    label={t('components.questionCreation.secondaryPrompt')}
                    tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                    size='default'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={8} md={12}>
                <Form.Item
                  help={errors.settings && errors.settings.answerFormat}
                  validateStatus={
                    errors.settings && errors.settings.answerFormat
                      ? 'error'
                      : 'success'
                  }
                >
                  <Select
                    name='settings.answerFormat'
                    value={
                      values.settings &&
                      values.settings.answerFormat && {
                        label: getFormatName(values.settings.answerFormat)
                      }
                    }
                    labelInValue
                    onChange={value => {
                      const key = value && value.key
                      setFieldValue('settings.answerFormat', key)
                      handleFieldChange({
                        settings: {
                          ...settings,
                          answerFormat: key
                        }
                      })
                    }}
                    options={answerFormats}
                    getOptionName={getFormatName}
                    label={t(`${componentKey}.answerFormat`)}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
              <Col lg={4} md={12}>
                <Form.Item
                  help={errors.settings && errors.settings.minLength}
                  validateStatus={
                    errors.settings && errors.settings.minLength
                      ? 'error'
                      : 'success'
                  }
                >
                  <Input
                    name='settings.minLength'
                    type='number'
                    min='1'
                    step='1'
                    value={values.settings && values.settings.minLength}
                    onChange={event => {
                      const value = event.target.value || null
                      setFieldValue('settings.minLength', value)
                      handleFieldChange({
                        settings: {
                          ...settings,
                          minLength: value
                        }
                      })
                    }}
                    label={t(`${componentKey}.minLength`, {})}
                    size='default'
                  />
                </Form.Item>
              </Col>
              <Col lg={4} md={12}>
                <Form.Item
                  help={errors.settings && errors.settings.maxLength}
                  validateStatus={
                    errors.settings && errors.settings.maxLength
                      ? 'error'
                      : 'success'
                  }
                >
                  <Input
                    name='settings.maxLength'
                    type='number'
                    min='1'
                    step='1'
                    value={values.settings && values.settings.maxLength}
                    onChange={event => {
                      const value = event.target.value || null
                      setFieldValue('settings.maxLength', value)
                      handleFieldChange({
                        settings: {
                          ...settings,
                          maxLength: value
                        }
                      })
                    }}
                    label={t(`${componentKey}.maxLength`, {})}
                    size='default'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={8} md={12}>
                <Form.Item
                  help={errors.settings && errors.settings.answerFormat}
                  validateStatus={
                    errors.settings && errors.settings.answerFormat
                      ? 'error'
                      : 'success'
                  }
                >
                  <CheckboxContainer>
                    <Checkbox
                      name='settings.allowSpaces'
                      checked={values.settings && values.settings.allowSpaces}
                      onChange={value => {
                        setFieldValue('settings.allowSpaces', value)
                        handleFieldChange({
                          settings: {
                            ...settings,
                            allowSpaces: value
                          }
                        })
                      }}
                    >
                      {t(`${componentKey}.allowSpaces`)}
                    </Checkbox>
                  </CheckboxContainer>
                </Form.Item>
              </Col>
            </Row>
            <ChartSection
              isNewQuestion={!id}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue(name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default withTranslation()(TasterName)
