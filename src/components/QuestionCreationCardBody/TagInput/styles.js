import styled from 'styled-components'
import { Tag as AntTag, Col } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
  display: flex;
  margin-bottom: 0.5rem;
  margin-top: 1.5rem;
`

export const Tag = styled(AntTag)`
  height: 3.2rem;
  padding: 0 0.8rem;
  display: inline-flex;
  align-items: center;
`

export const NewTagButton = styled(Tag)`
  background: '#fff';
  border-style: dashed;
`

export const uploadStyle = {
  width: `8rem`,
  height: `8rem`
}

export const TagInputStyle = styled(Col)`
  min-width: 20rem;
`