import React from 'react'
import { mount } from 'enzyme'
import TagInput from '.'
import { act } from 'react-dom/test-utils'
import { Input as AntInput } from 'antd'
import { Tag, NewTagButton } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('TagInput', () => {
    let testRender
    let label
    let tags
    let handleAddTag
    let handleRemoveTag
    let canAddMoreTags

    beforeEach(() => {
        label = 'input text'
        tags = ['tag 1', 'tag 2']
        handleAddTag = jest.fn()
        handleRemoveTag = jest.fn()
        canAddMoreTags = tags => true
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render TagInput and antd ', async () => {
        testRender = mount(
            <TagInput
                label={label}
                tags={tags}
                handleAddTag={handleAddTag}
                handleRemoveTag={handleRemoveTag}
                canAddMoreTags={canAddMoreTags}
            />
        )

        expect(testRender.find(TagInput)).toHaveLength(1)

        act(() => {
            testRender
                .find(NewTagButton)
                .first()
                .prop('onClick')()
        })
        expect(handleAddTag).not.toHaveBeenCalled()
    })

    test('should render antInput', async () => {
        testRender = mount(
            <TagInput
                label={label}
                tags={tags}
                handleAddTag={handleAddTag}
                handleRemoveTag={handleRemoveTag}
                canAddMoreTags={canAddMoreTags}
                inputFieldVisible
            />
        )

        // test the onPressEnter
        act(() => {
            testRender
                .find(NewTagButton)
                .first()
                .prop('onClick')()
        })
        testRender.update()
        expect(testRender.find(AntInput)).toHaveLength(1)

        act(() => {
            testRender.find(AntInput).prop('onChange')({
                target: { value: 'antInputChange test' }
            })
        })
        testRender.update()
        expect(handleAddTag).not.toHaveBeenCalled()

        act(() => {
            testRender.find(AntInput).prop('onPressEnter')()
        })
        testRender.update()
        expect(handleAddTag).toHaveBeenCalledWith('antInputChange test')

        // redo the same steps as above to use onBlur instead of onPressEnter in the last step
        act(() => {
            testRender
                .find(NewTagButton)
                .first()
                .prop('onClick')()
        })
        testRender.update()
        act(() => {
            testRender.find(AntInput).prop('onChange')({
                target: { value: 'antInputChange test' }
            })
        })
        testRender.update()

        //test onBlur
        act(() => {
            testRender.find(AntInput).prop('onBlur')()
        })
        testRender.update()
        expect(handleAddTag).toHaveBeenCalledWith('antInputChange test')

        act(() => {
        testRender.find(Tag).first().prop('onClose')()
        })
        testRender.update()
        expect(handleRemoveTag).toHaveBeenCalled()

    })
})