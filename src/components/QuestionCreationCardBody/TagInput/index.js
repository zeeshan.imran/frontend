import React, { useState, useEffect } from 'react'
import { Row, Input as AntInput, Form, Col } from 'antd'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'

import { Tag, NewTagButton, uploadStyle, TagInputStyle } from './styles'
import FieldLabel from '../../FieldLabel'
import UploadOptionPicture from '../../UploadOptionPicture'
import StaticUrlOrBase64 from '../../../utils/imagesStaticUrl'

const TagInput = ({
  label,
  tags,
  handleAddTag,
  handleRemoveTag,
  onAddImageUrl,
  onRemoveImageUrl,
  canAddMoreTags = tags => true
}) => {
  const { t } = useTranslation()
  const [inputFieldVisible, setInputFieldVisible] = useState(false)
  const [inputFieldValue, setInputFieldValue] = useState('')
  const [inputError, setInputError] = useState()
  const handleTagInput = () => {
    if (inputFieldValue) {
      handleAddTag(inputFieldValue)
    }
    setInputFieldVisible(false)
    setInputFieldValue('')
  }

  useEffect(() => {
    (async () => {
      try {
        await Yup.string()
          .test(
            'not-empty',
            t('validation.vertical.range.labels.required'),
            value => !/^[ ]+$/.test(value)
          )
          .required(t('validation.vertical.range.labels.required'))
          .validate(inputFieldValue)
        setInputError(false)
      } catch (error) {
        if (error.errors && error.errors.length) {
          setInputError(error.errors[0])
        }
      }
    })()
  })

  return (
    <FieldLabel label={label}>
      <Row>
        {tags.map((tag, index) => (
          <Row gutter={24} type='flex' align='middle'>
            <TagInputStyle flex='auto'>
              <Tag
                key={index}
                closable
                onClose={() => {
                  handleRemoveTag(index)
                }}
              >
                {tag.pair}
              </Tag>
            </TagInputStyle>
            <Col flex='auto'>
              <UploadOptionPicture
                name={index}
                uploadStyle={uploadStyle}
                onAddImageUrl={onAddImageUrl}
                image={tag && tag.image800 && StaticUrlOrBase64(tag.image800)}
                onRemoveImageUrl={() => {
                  onRemoveImageUrl(index)
                }}
              />
            </Col>
          </Row>
        ))}

        {canAddMoreTags(tags) && (
          <React.Fragment>
            {inputFieldVisible ? (
              <Form.Item
                help={inputError}
                validateStatus={inputError ? 'error' : 'success'}
              >
                <AntInput
                  type='text'
                  size='default'
                  data-testid={'add-value-text'}
                  style={{ width: 78 }}
                  value={inputFieldValue}
                  onChange={e => setInputFieldValue(e.target.value)}
                  onBlur={handleTagInput}
                  onPressEnter={handleTagInput}
                />
              </Form.Item>
            ) : (
              <NewTagButton
                data-testid={'add-value-button'}
                onClick={() => {
                  setInputFieldVisible(true)
                }}
              >
                + {t('components.questionCreation.tagInput.add')}
              </NewTagButton>
            )}
          </React.Fragment>
        )}
      </Row>
    </FieldLabel>
  )
}

export default TagInput
