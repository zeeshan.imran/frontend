import React from 'react'
import { mount } from 'enzyme'
import EmailForm from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('EmailForm', () => {
  let testRender
  let validationSchema
  let prompt
  let handleFieldChange

  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    handleFieldChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render EmailForm', async () => {
    testRender = mount(
      <EmailForm
        validationSchema={validationSchema}
        prompt={prompt}
        handleFieldChange={handleFieldChange}
      />
    )
    expect(testRender.find(EmailForm)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })
  })

  test('should render EmailForm of InputArea for secondaryPrompt', async () => {
    testRender = mount(
      <EmailForm
        validationSchema={validationSchema}
        prompt={prompt}
        handleFieldChange={handleFieldChange}
      />
    )
    testRender
      .findWhere(
        c => c.name() === 'InputArea' && c.prop('name') === 'secondaryPrompt'
      )
      .last()
      .prop('onChange')({
        target: {
          value: 'secondaryPrompt'
        }
      })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })
  })
})
