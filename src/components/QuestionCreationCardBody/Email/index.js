import React from 'react'
import { Formik } from 'formik'
import { Form, Row, Col } from 'antd'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'
import ChartSection from '../ChartSection'
import useValidateWithContext from '../../../hooks/useValidateWithContext'

const EmailForm = ({
  id,
  validationSchema,
  handleFieldChange,
  prompt,
  secondaryPrompt,
  chartTypes,
  chartTitle,
  chartTopic,
  chartType
}) => {
  const formRef = useQuestionValidate()
  const { t } = useTranslation()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes }),
    [chartTypes]
  )

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        chartTitle,
        chartTopic,
        chartType
      }}
      render={({ errors, setFieldValue, values }) => {
        const { prompt: promptError } = errors

        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={promptError}
                  validateStatus={promptError ? 'error' : 'success'}
                >
                  <InputArea
                    autoSize
                    name='prompt'
                    value={values.prompt}
                    onChange={event => {
                      setFieldValue('prompt', event.target.value)
                      handleFieldChange({ prompt: event.target.value })
                    }}
                    label={t('components.questionCreation.prompt')}
                    tooltip={t('tooltips.questionCreation.prompt')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item>
                  <InputArea
                    autoSize
                    name='secondaryPrompt'
                    value={values.secondaryPrompt}
                    onChange={event => {
                      setFieldValue('secondaryPrompt', event.target.value)
                      handleFieldChange({ secondaryPrompt: event.target.value })
                    }}
                    label={t('components.questionCreation.secondaryPrompt')}
                    tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                    size='default'
                  />
                </Form.Item>
              </Col>
            </Row>
            <ChartSection
              isNewQuestion={!id}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue(name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default EmailForm
