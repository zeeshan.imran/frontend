import React, { useEffect, useCallback, useState } from 'react'
import { Row, Col, Form, Collapse, Icon } from 'antd'
import { useTranslation } from 'react-i18next'
import startCase from 'lodash.startcase'
import capitalize from 'lodash.capitalize'
import Input from '../../Input'
import Select from '../../Select'
import { StyledCollapse } from './styles'

const ChartSection = ({
  isNewQuestion,
  values,
  errors,
  chartTypes,
  onFieldChange,
  style
}) => {
  const { t } = useTranslation()
  const isEmpty = !values || !values.chartType
  const [collapsed, setCollapsed] = useState(!isNewQuestion)

  useEffect(() => {
    if (chartTypes && chartTypes.length === 1 && isEmpty) {
      onFieldChange('chartType', chartTypes[0])
    }
  }, [chartTypes, isEmpty, onFieldChange])

  const getNameOfChartType = useCallback(
    type =>
      type !== null && type !== undefined
        ? t(`chartTypes.${type || 'no-chart'}`)
        : '',
    [t]
  )

  useEffect(() => {
    if (errors.chartTitle || errors.chartTopic || errors.chartType) {
      setCollapsed(false)
    }
  }, [errors])

  if (!chartTypes || chartTypes.length === 0) {
    return null
  }
  const isUnknownChartType =
    values.chartType && !chartTypes.includes(values.chartType)
  const error = isUnknownChartType
    ? t('validation.chartType.wrongChartType', {
        chartType: capitalize(startCase(values.chartType))
      })
    : undefined

  return (
    <StyledCollapse
      style={style}
      activeKey={collapsed ? [] : ['chart-settings']}
      expandIconPosition='right'
      onChange={() => setCollapsed(!collapsed)}
    >
      <Collapse.Panel
        header={
          <React.Fragment>
            <Icon type='bar-chart' />
            {`${t('components.questionCreation.chartSection.chartSettings')}`}
          </React.Fragment>
        }
        key='chart-settings'
      >
        <Row gutter={24}>
          <Col lg={8} md={12}>
            <Form.Item
              help={error || errors.chartType}
              validateStatus={error || errors.chartType ? 'error' : 'success'}
            >
              <Select
                data-testid='type-of-chart-select'
                value={{
                  key: values.chartType,
                  label: getNameOfChartType(values.chartType)
                }}
                labelInValue
                onChange={value => onFieldChange('chartType', value.key)}
                placeholder='Choose type of chart'
                options={chartTypes}
                getOptionName={getNameOfChartType}
                label={t('components.questionCreation.chartSection.chartType')}
                size='default'
                required
              />
            </Form.Item>
          </Col>
        </Row>
        {!!values.chartType && (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={errors.chartTopic}
                  validateStatus={errors.chartTopic ? 'error' : 'success'}
                >
                  <Input
                    name='chartTopic'
                    value={values.chartTopic}
                    onChange={event =>
                      onFieldChange('chartTopic', event.target.value)
                    }
                    label={t(
                      'components.questionCreation.chartSection.chartTopic'
                    )}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={errors.chartTitle}
                  validateStatus={errors.chartTitle ? 'error' : 'success'}
                >
                  <Input
                    name='chartTitle'
                    value={values.chartTitle}
                    onChange={event =>
                      onFieldChange('chartTitle', event.target.value)
                    }
                    label={t(
                      'components.questionCreation.chartSection.chartTitle'
                    )}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
          </React.Fragment>
        )}
      </Collapse.Panel>
    </StyledCollapse>
  )
}

export default ChartSection
