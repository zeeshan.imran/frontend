import { Collapse } from 'antd'
import styled from 'styled-components'

export const StyledCollapse = styled(Collapse)`
  margin: 0 -3.5rem;
  border: none;
  font-size: 1.4rem;

  & > .ant-collapse-item {
    background: #f5faeb;
    border-bottom: none !important;
  }
`
