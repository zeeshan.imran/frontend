import styled from 'styled-components'
import { Row, Radio } from 'antd'
import colors from '../../../utils/Colors'

export const SettingWrapper = styled(Row)`
  margin-top: 5px;
  margin-bottom: 20px;
  gutter: 24;
  align: middle;
  justify: space-between;
`
export const RadioButton = styled(Radio)`
  .ant-radio-inner::after {
    background-color: ${colors.RADIO_BUTTON_BACKGROUND_COLOR};
  }
`
