import styled from 'styled-components'

export const CheckboxContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const NumberSample = styled.div`
  line-height: 30px;
  height: 30px;
`