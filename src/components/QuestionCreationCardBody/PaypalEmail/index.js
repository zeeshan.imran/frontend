import React from 'react'
import { Formik } from 'formik'
import { Form, Row, Col } from 'antd'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'

const PaypalEmailForm = ({ validationSchema, handleFieldChange, prompt, secondaryPrompt }) => {
  const formRef = useQuestionValidate()
  const { t } = useTranslation();
  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validationSchema={validationSchema}
      initialValues={{
        prompt,
        secondaryPrompt
      }}
      render={({ errors, setFieldValue, values }) => {
        const { prompt: promptError } = errors

        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={promptError}
                  validateStatus={promptError ? 'error' : 'success'}
                >
                  <InputArea
                    autoSize
                    name='prompt'
                    value={values.prompt}
                    onChange={event => {
                      setFieldValue('prompt', event.target.value)
                      handleFieldChange({ prompt: event.target.value })
                    }}
                    label={t('components.questionCreation.prompt')}
                    tooltip={t('tooltips.questionCreation.prompt')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item>
                  <InputArea
                    autoSize
                    name='secondaryPrompt'
                    value={values.secondaryPrompt}
                    onChange={event => {
                      setFieldValue('secondaryPrompt', event.target.value)
                      handleFieldChange({ secondaryPrompt: event.target.value })
                    }}
                    label={t('components.questionCreation.secondaryPrompt')}
                    tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                    size='default'
                  />
                </Form.Item>
              </Col>
            </Row>
          </React.Fragment>
        )
      }}
    />
  )
}

export default PaypalEmailForm
