import React from 'react'
import { mount } from 'enzyme'
import ChooseDate from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('ChooseDate', () => {
  let testRender
  let validationSchema
  let prompt
  let secondaryPrompt
  let handleFieldChange

  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    secondaryPrompt = ''
    handleFieldChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ChooseDate', async () => {
    testRender = mount(
      <ChooseDate
        validationSchema={validationSchema}
        prompt={prompt}
        secondaryPrompt={secondaryPrompt}
        handleFieldChange={handleFieldChange}
      />
    )
    expect(testRender.find(ChooseDate)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })
  })

  test('should render ChooseDate of Inpurarea for secondaryPrompt', async () => {
    testRender = mount(
      <ChooseDate
        validationSchema={validationSchema}
        secondaryPrompt={secondaryPrompt}
        handleFieldChange={handleFieldChange}
      />
    )
    testRender
      .findWhere(
        c => c.name() === 'InputArea' && c.prop('name') === 'secondaryPrompt'
      )
      .last()
      .prop('onChange')({ target: { value: 'secondaryPrompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })
  })
})
