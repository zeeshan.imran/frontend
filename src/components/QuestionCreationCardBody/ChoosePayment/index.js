import React from 'react'
import { Row, Col, Form } from 'antd'
import { Formik } from 'formik'
import OptionsSection from '../OptionsSection'
import { getDefaultSelectOptions } from '../../../utils/defaultQuestionValues'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'
import { minOptions } from '../../../validates/questions/choose-one'

const ChoosePayment = ({
  id,
  clientGeneratedId,
  validationSchema,
  prompt,
  handleFieldChange,
  options = [],
  secondaryPrompt,
  skipFlow,
  hasPaymentOptions
}) => {
  const { t } = useTranslation()
  const formRef = useQuestionValidate()
  options = options || []

  if (options.length < minOptions) {
    options = getDefaultSelectOptions(minOptions)
  }

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validationSchema={validationSchema}
      initialValues={{
        prompt,
        secondaryPrompt,
        options
      }}
      render={({ errors, setFieldValue, values }) => {
        const { prompt: promptError } = errors
        
      return (
        <React.Fragment>
          <Row gutter={24}>
            <Col lg={16} md={24}>
              <Form.Item
                help={promptError}
                validateStatus={promptError ? 'error' : 'success'}
              >
                <InputArea
                  data-testid={'choose-one-question-text'}
                  autoSize
                  name='prompt'
                  value={values.prompt}
                  onChange={event => {
                    setFieldValue('prompt', event.target.value)
                    handleFieldChange({ prompt: event.target.value })
                  }}
                  label={t('components.questionCreation.prompt')}
                  tooltip={t('tooltips.questionCreation.prompt')}
                  size='default'
                  required
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col lg={16} md={24}>
              <Form.Item>
                <InputArea
                  autoSize
                  name='secondaryPrompt'
                  value={values.secondaryPrompt}
                  onChange={event => {
                    setFieldValue('secondaryPrompt', event.target.value)
                    handleFieldChange({ secondaryPrompt: event.target.value })
                  }}
                  label={t('components.questionCreation.secondaryPrompt')}
                  tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                  size='default'
                />
              </Form.Item>
            </Col>
          </Row>
          <OptionsSection
            id={id}
            clientGeneratedId={clientGeneratedId}
            label={t('components.questionCreation.chooseOne.optionsSection')}
            options={values.options}
            onChangeOptions={updatedOptions => {
              setFieldValue('options', updatedOptions)
              handleFieldChange({
                options: updatedOptions
              })
            }}
            errors={errors.options}
            skipFlow={skipFlow}
            onSkipFlowChange={nextSkipFlow => {
              setFieldValue('skipFlow', nextSkipFlow)
              handleFieldChange({
                skipFlow: nextSkipFlow
              })
            }}
            isOpenAnswerPossible={false}
            hasPaymentOptions
          />
        </React.Fragment>
      )}}
    />
  )
}

export default ChoosePayment
