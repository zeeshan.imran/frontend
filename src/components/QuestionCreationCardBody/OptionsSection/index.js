import React, { useState, useEffect } from 'react'
import { type, isNil, append, insert, dropLast } from 'ramda'
import { Icon, Row, Col, Form } from 'antd'
import Input from '../../Input'
import Button from '../../Button'
import Checkbox from '../../Checkbox'
import { CheckboxContainer } from '../PairedQuestions/styles'
import { AddOptionButtonContainer, LabelContainer, Label } from './styles'
import { useTranslation } from 'react-i18next'
import useSkipToOptions from '../../../hooks/useSkipToOptions'
import SkipToPicker from '../SkipToPicker'
import skipFlowUtils from '../../../utils/skipFlowUtils'
import HelperIcon from '../../HelperIcon'
import ChooseMultipleSkipOptions from '../../ChooseMultipleSkipOptions'
import ChooseMatrixSkipOptions from '../../ChooseMatrixSkipOptions'
import UploadOptionPicture from '../../UploadOptionPicture/index'
import getBase64 from '../../../utils/getBase64'
import ImageResize from '../../../utils/imageResize'
import StaticUrlOrBase64 from '../../../utils/imagesStaticUrl'

const isRuleOfOption = index => rule => rule.index === index

const OptionsSection = ({
  id,
  clientGeneratedId,
  label,
  options = [],
  onChangeOptions,
  errors,
  noAnalytics,
  noSkipOptions,
  minOptions = 2,
  isOpenAnswerPossible,
  skipFlow,
  onSkipFlowChange,
  addAnswerLabel,
  hasPaymentOptions,
  attachedImage = false,
  columnsData = []
}) => {
  const [canAddRules, setCanAddRules] = useState(true)
  const { t } = useTranslation()
  const skipToOptions = useSkipToOptions({ id, clientGeneratedId })
  const [isSkipOptionsShown, setIsSkipOptionsShown] = useState(
    !skipFlowUtils.isAllSkipToEmpty(skipFlow) &&
      skipFlow.type !== 'MATCH_MULTIPLE_OPTION' &&
      skipFlow.type !== 'MATCH_MATRIX_OPTION'
  )
  const [isMartrixQuestion] = useState(
    skipFlow && skipFlow.type === 'MATCH_MATRIX_OPTION'
  )
  const [showConditions, setShowConditions] = useState(
    skipFlow &&
      (skipFlow.type === 'MATCH_MULTIPLE_OPTION' ||
        skipFlow.type === 'MATCH_MATRIX_OPTION') &&
      skipFlow.rules.length
      ? true
      : false
  )
  const [conditions, setConditions] = useState([])
  useEffect(
    () => {
      if (
        skipFlow &&
        (skipFlow.type === 'MATCH_MULTIPLE_OPTION' ||
          skipFlow.type === 'MATCH_MATRIX_OPTION')
      ) {
        if (skipFlow.rules && skipFlow.rules.length) {
          setConditions(skipFlow.rules)
        } else {
          setConditions([
            {
              options: options,
              skipOptions: skipToOptions,
              skipTo: '',
              answers: [],
              type: ''
            }
          ])
        }
        if (!showConditions) {
          onSkipFlowChange({ ...skipFlow, rules: [] })
          setCanAddRules(true)
          setConditions([])
        }
      }
    },
    // we only run this effect when the showConditions changes
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [showConditions]
  )

  const canRemoveOption = options.length > minOptions

  const addNewConditions = value => {
    setConditions([...conditions, value])
  }

  const updateCurrentConditions = (index, value) => {
    conditions[index] = { ...conditions[index], ...value }
    setConditions(conditions)
    skipFlow.rules = conditions
    onSkipFlowChange(skipFlow)
  }
  const SkipToOptions = (_option, index) => {
    const skipRule = skipFlowUtils
      .fromSkipFlow(skipFlow)
      .findRule(isRuleOfOption(index))
    return (
      <Row gutter={24}>
        <Col xs={2} lg={8}>
          <LabelContainer>
            <Label>{t('components.questionCreation.jumpToQuestions')}</Label>
          </LabelContainer>
        </Col>
        <Col xs={2} lg={8}>
          <Form.Item
            help={
              type(errors) === 'Array' && !isNil(errors[index])
                ? errors[index].skipTo
                : null
            }
            validateStatus={
              errors && errors[index] && errors[index].skipTo
                ? 'error'
                : 'success'
            }
          >
            <SkipToPicker
              value={(skipRule && skipRule.skipTo) || null}
              options={skipToOptions}
              onSkipRemove={() => {
                const nextSkipFlow = skipFlowUtils
                  .fromSkipFlow(skipFlow)
                  .removeRules(isRuleOfOption(index))
                  .getNextSkipFlow()

                onSkipFlowChange(nextSkipFlow)
              }}
              onSkipChange={skipTo => {
                const nextSkipFlow = skipFlowUtils
                  .fromSkipFlow(skipFlow)
                  .removeRules(isRuleOfOption(index))
                  .addRule({ index, skipTo })
                  .getNextSkipFlow()

                onSkipFlowChange(nextSkipFlow)
              }}
            />
          </Form.Item>
        </Col>
      </Row>
    )
  }

  const addImageToOption = async (index, fileData, options) => {
    if (fileData[index]) {
      const base64Image = await getBase64(fileData[index])
      const ImageType = base64Image
        .match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0]
        .split('/')
      const ImageTypeQuality = ImageType[1].toLowerCase() === 'jpeg' ? 70 : 100
      const image800 = await ImageResize(
        fileData[index],
        800,
        800,
        ImageType[1].toLowerCase(),
        ImageTypeQuality
      )
      options[index].image200 = ''
      options[index].image800 = image800
      onChangeOptions(options)
    } else {
      delete options[index].image200
      delete options[index].image800
      onChangeOptions(options)
    }
  }

  const onRemoveImageUrl = (index, options) => {
    options[index].image200 = ''
    options[index].image800 = ''
    onChangeOptions(options)
  }
  const showSkipConditions = () => {
    if (showConditions) {
      if (isMartrixQuestion && columnsData && columnsData.length) {
        return (
          <ChooseMatrixSkipOptions
            columnsData={columnsData}
            conditions={conditions}
            canAddRules={canAddRules}
            setCanAddRules={setCanAddRules}
            conditionalOptions={addNewConditions}
            updateCurrentConditions={updateCurrentConditions}
            id={id}
            clientGeneratedId={clientGeneratedId}
          />
        )
      } else {
        return conditions.map((condition, index) => {
          return (
            <ChooseMultipleSkipOptions
              key={index}
              answers={condition.options.filter(option => {
                return option.label && option.label.length
              })}
              canAddRules={canAddRules}
              setCanAddRules={setCanAddRules}
              skipOptions={condition.skipOptions}
              conditionalOptions={addNewConditions}
              updateCurrentConditions={updateCurrentConditions}
              selectedQuestion={condition.skipTo}
              selectedOptions={condition.answers}
              currentIndex={index}
              selectedType={condition.type}
              isLastIndex={conditions.length - 1 === index}
              id={id}
              clientGeneratedId={clientGeneratedId}
              columnsData={columnsData}
              isMartrixQuestion={isMartrixQuestion}
            />
          )
        })
      }
    }
    return null
  }

  return (
    <React.Fragment>
      <Row gutter={24}>
        <Col lg={16} md={24}>
          <LabelContainer
            help={type(errors) === 'String' && errors}
            validateStatus={
              type(errors) === 'String' && errors ? 'error' : 'success'
            }
          >
            <Label>{label}</Label>
          </LabelContainer>
        </Col>
      </Row>
      {(options || []).map((option, index) => {
        if (option.isOpenAnswer) {
          return null
        }
        const isStrictMode = option.editMode === 'strict'
        return (
          <React.Fragment key={index}>
            <Row gutter={24}>
              <Col xs={noAnalytics ? 20 : 10} lg={noAnalytics ? 16 : 8}>
                <Form.Item
                  help={
                    type(errors) === 'Array' && !isNil(errors[index])
                      ? errors[index].label
                      : null
                  }
                  validateStatus={
                    (errors && errors[index] && errors[index].label) ||
                    (errors && type(errors) === 'String')
                      ? 'error'
                      : 'success'
                  }
                >
                  <Input
                    required
                    name={`${index}-label`}
                    size='default'
                    placeholder={t('placeholders.questionLabel')}
                    value={option.label}
                    onChange={event => {
                      const updatedOptions = [
                        ...options.slice(0, index),
                        { ...options[index], label: event.target.value },
                        ...options.slice(index + 1)
                      ]
                      onChangeOptions(updatedOptions, event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={10} lg={8}>
                {!noAnalytics && (
                  <Form.Item
                    help={
                      type(errors) === 'Array' && !isNil(errors[index])
                        ? errors[index].value
                        : null
                    }
                    validateStatus={
                      (errors && errors[index] && errors[index].value) ||
                      (errors && type(errors) === 'String')
                        ? 'error'
                        : 'success'
                    }
                  >
                    <Input
                      required
                      type={`number`}
                      name={`${index}-value`}
                      disabled={isStrictMode || hasPaymentOptions}
                      size='default'
                      placeholder={t('placeholders.analyticsValue')}
                      value={option.value}
                      onChange={event => {
                        const updatedOptions = [
                          ...options.slice(0, index),
                          { ...options[index], value: event.target.value },
                          ...options.slice(index + 1)
                        ]
                        onChangeOptions(updatedOptions)
                      }}
                    />
                  </Form.Item>
                )}
              </Col>
              {attachedImage && (
                <Col xs={5} lg={4} flex='auto'>
                  <UploadOptionPicture
                    name={index}
                    image={
                      option &&
                      option.image800 &&
                      StaticUrlOrBase64(option.image800)
                    }
                    onAddImageUrl={fileData =>
                      addImageToOption(index, fileData, options)
                    }
                    onRemoveImageUrl={() => onRemoveImageUrl(index, options)}
                  />
                </Col>
              )}
              {isStrictMode && (
                <Col xs={5} lg={4} flex='auto'>
                  <HelperIcon
                    placement='bottomRight'
                    helperText={t('tooltips.strictOptions')}
                  />
                </Col>
              )}
              {!isStrictMode && canRemoveOption && (
                <Col xs={5} lg={4} flex='auto'>
                  <Button
                    size='default'
                    type='red'
                    onClick={() => {
                      onChangeOptions([
                        ...options.slice(0, index),
                        ...options.slice(index + 1)
                      ])
                      if (skipFlow && !noSkipOptions) {
                        const nextSkipFlow = skipFlowUtils
                          .fromSkipFlow(skipFlow)
                          .removeRules(isRuleOfOption(index))
                          .getNextSkipFlow()

                        onSkipFlowChange({
                          ...nextSkipFlow,
                          rules: nextSkipFlow.rules.map(rule => {
                            if (rule.index > index) {
                              return {
                                ...rule,
                                index: rule.index - 1
                              }
                            }
                            return rule
                          })
                        })
                      }
                    }}
                  >
                    <Icon type='close' />
                  </Button>
                </Col>
              )}
            </Row>
            {isSkipOptionsShown && SkipToOptions(option, index)}
          </React.Fragment>
        )
      })}
      {!hasPaymentOptions && isOpenAnswerPossible && (
        <React.Fragment>
          <Form.Item>
            <Row gutter={24}>
              <Col lg={8} md={12}>
                <CheckboxContainer>
                  <Checkbox
                    checked={
                      options[options.length - 1] &&
                      options[options.length - 1].isOpenAnswer
                    }
                    onChange={value => {
                      if (value) {
                        onChangeOptions(
                          append(
                            {
                              label: 'Other',
                              skipTo: 'null',
                              value: 'other',
                              isOpenAnswer: true
                            },
                            options
                          )
                        )
                      } else {
                        onChangeOptions(dropLast(1, options))
                      }
                    }}
                  >
                    {t(
                      'containers.questionCreationCardBody.OptionsSection.includeOthers'
                    )}
                  </Checkbox>
                </CheckboxContainer>
              </Col>
            </Row>
            {options[options.length - 1] &&
              options[options.length - 1].isOpenAnswer &&
              isSkipOptionsShown &&
              SkipToOptions(options[options.length - 1], options.length - 1)}
          </Form.Item>
        </React.Fragment>
      )}
      {!hasPaymentOptions && (
        <AddOptionButtonContainer>
          <Row>
            <Col lg={8} md={8}>
              <Button
                onClick={() => {
                  onChangeOptions(
                    insert(
                      options.length -
                        (options[options.length - 1] &&
                        options[options.length - 1].isOpenAnswer
                          ? 1
                          : 0),
                      { label: null },
                      options
                    )
                  )
                }}
              >
                {addAnswerLabel ||
                  t('components.questionCreation.optionsSection.addAnswer')}
              </Button>
            </Col>
            {skipFlow &&
            (skipFlow.type === 'MATCH_MULTIPLE_OPTION' ||
              skipFlow.type === 'MATCH_MATRIX_OPTION') ? (
              <Col lg={8} md={8}>
                <Button
                  onClick={() => {
                    setShowConditions(!showConditions)
                  }}
                >
                  {!showConditions
                    ? t('components.questionCreation.showConditionButton')
                    : t('components.questionCreation.hideConditionButton')}
                </Button>
              </Col>
            ) : (
              !(noSkipOptions || !skipFlow) && (
                <Col lg={8} md={8}>
                  <Button
                    onClick={() => {
                      setIsSkipOptionsShown(!isSkipOptionsShown)
                    }}
                  >
                    {!isSkipOptionsShown
                      ? t('components.questionCreation.showSkipOptions')
                      : t('components.questionCreation.hideSkipOptions')}
                  </Button>
                </Col>
              )
            )}
          </Row>
        </AddOptionButtonContainer>
      )}
      {showSkipConditions()}
    </React.Fragment>
  )
}

export default OptionsSection
