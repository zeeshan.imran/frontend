import styled from 'styled-components'
import { Row, Form } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'

export const AddOptionButtonContainer = styled(Row)`
  margin-bottom: 1.5rem;
`

export const RemoveSkipButton = styled.button`
  background: #fff;
  color: #ff4d4f;
  border: none;
  display: block;
  cursor: pointer;
  width: 100%;
  padding: 0.5rem 0;
  margin-top: -1px;
  &:hover {
    background: #f5faeb;
    color: #ff7875
  }
`

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
`

export const LabelContainer = styled(Form.Item)`
  margin-bottom: 0;
`
