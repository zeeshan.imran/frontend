import React from 'react'
import { mount } from 'enzyme'
import Dropdown from '.'
import { BrowserRouter as Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

describe('Dropdown', () => {
  let testRender
  let validationSchema
  let prompt
  let secondaryPrompt
  let handleFieldChange
  let options
  let chartTitle
  let chartTopic
  let chartType
  let skipToOptions
  let nextQuestion
  let questionIndex
  let client
  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    secondaryPrompt = ''
    handleFieldChange = jest.fn()
    options = []
    chartTitle = ''
    chartTopic = ''
    chartType = ''
    skipToOptions = []
    nextQuestion = 'q2'
    questionIndex = 'q1'
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Dropdown', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router>
          <Dropdown
            validationSchema={validationSchema}
            prompt={prompt}
            secondaryPrompt={secondaryPrompt}
            handleFieldChange={handleFieldChange}
            options={options}
            chartTitle={chartTitle}
            chartTopic={chartTopic}
            chartType={chartType}
            skipToOptions={skipToOptions}
            nextQuestion={nextQuestion}
            questionIndex={questionIndex}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(Dropdown)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })

    testRender
      .findWhere(c => c.name() === 'ChartSection')
      .first()
      .prop('onFieldChange')('chartTopic', 'pie')

    expect(handleFieldChange).toHaveBeenCalledWith({
      chartTopic: 'pie'
    })

    testRender
      .findWhere(c => c.name() === 'OptionsSection')
      .first()
      .prop('onChangeOptions')(['1', '2'])

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })
  })

  test('should render Dropdown of Inpurarea for secondaryPrompt', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router>
          <Dropdown
            validationSchema={validationSchema}
            prompt={prompt}
            secondaryPrompt={secondaryPrompt}
            handleFieldChange={handleFieldChange}
            options={options}
            chartTitle={chartTitle}
            chartTopic={chartTopic}
            chartType={chartType}
            skipToOptions={skipToOptions}
            nextQuestion={nextQuestion}
            questionIndex={questionIndex}
          />
        </Router>
      </ApolloProvider>
    )
    testRender
      .findWhere(
        c => c.name() === 'InputArea' && c.prop('name') === 'secondaryPrompt'
      )
      .last()
      .prop('onChange')({ target: { value: 'secondaryPrompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })

    testRender
      .findWhere(c => c.name() === 'OptionsSection')
      .last()
      .prop('onSkipFlowChange')({ skipFlow: 'skipFlow' })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })
  })
})
