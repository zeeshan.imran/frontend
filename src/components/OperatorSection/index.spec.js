import React from 'react'
import { mount } from 'enzyme'
import OperatorSection from '.'
import { Title } from './styles'

describe('OperatorSection', () => {
  let testRender
  let title

  beforeEach(() => {
    title = 'Operator Title'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorSection', async () => {
    testRender = mount(<OperatorSection title={title} />)
    expect(testRender.find(OperatorSection)).toHaveLength(1)

    expect(
      testRender
        .find(Title)
        .text()
    ).toBe('Operator Title')
  })
})
