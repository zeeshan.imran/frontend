import styled from 'styled-components'
import Text from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 3rem 3.5rem;
  margin-bottom: 2.5rem;
`

export const Title = styled(Text)`
  font-size: 2rem;
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.85);
`
export const CheckboxContainer = styled.div`
  float: right
`
export const ClearFloats = styled.div`
  clear: both
`
