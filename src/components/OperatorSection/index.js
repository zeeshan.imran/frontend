import React from 'react'
import { Container, Title, CheckboxContainer, ClearFloats } from './styles'
import { StyledCheckbox } from '../StyledCheckBox'
import { withTranslation } from 'react-i18next'

const OperatorSection = ({
  title,
  showMandatoryBox,
  showPictureBox,
  allToMandatory,
  allToPicture,
  settingAllToUnRequiredOnClick,
  settingAllToPictureOnClick,
  t
}) => {
  return (
    <Container>
      <Title>{title}</Title>
      {showMandatoryBox ? (
        <CheckboxContainer>
          <StyledCheckbox
            checked={allToMandatory}
            onChange={e => settingAllToUnRequiredOnClick(e.target.checked)}
          >
            {t('components.questionCreation.markAll')}
          </StyledCheckbox>
          <ClearFloats />
        </CheckboxContainer>
      ) : null}
      {showPictureBox ? (
        <CheckboxContainer>
          <StyledCheckbox
            checked={allToPicture}
            onChange={e => settingAllToPictureOnClick(e.target.checked)}
          >
            {t('components.questionCreation.pictureOnAll')}
          </StyledCheckbox>
          <ClearFloats />
        </CheckboxContainer>
      ) : null}
    </Container>
  )
}

export default withTranslation()(OperatorSection)
