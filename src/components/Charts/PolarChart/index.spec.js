import React from 'react'
import { mount } from 'enzyme'
import PolarChart from '.'
import wait from '../../../utils/testUtils/waait'

jest.mock('./Chart')

describe('PolarChart', () => {
  let testRender
  let inputData
  let container

  beforeEach(() => {
    container = global.document.createElement('div')
    global.document.body.appendChild(container)

    inputData = {
      chart_id: '121',
      chart_type: 'polar',
      title: 'Texture Profile',
      labels: [
        'Slippery',
        'Firm',
        'Smooth',
        'Thick',
        'Creamy',
        'Cohesive',
        'Dense',
        'Chalky'
      ],
      series: [
        {
          name: 'Activia Regular Yogurt (Strawberry)',
          data: [
            5.23106159,
            3.31157415,
            7.78493879,
            4.70637027,
            7.90083473,
            4.67347224,
            3.70461927,
            2.80186612
          ]
        },
        {
          name: 'Dannon Whole Milk Yogurt (Strawberry)',
          data: [
            5.09025735,
            3.74322381,
            7.7914562,
            5.57833011,
            8.04769184,
            3.79798967,
            4.47044115,
            2.09026503
          ]
        },
        {
          name: 'Yoplait Original Yogurt (Strawberry)',
          data: [
            5.75635796,
            3.42562329,
            7.96004941,
            4.67783329,
            7.85964032,
            4.67252571,
            3.63001937,
            2.04427923
          ]
        },
        {
          name: 'Great Value (Walmart) Original Yogurt (Strawberry)',
          data: [
            5.8657288,
            3.46138114,
            6.86267904,
            4.65781666,
            6.81921911,
            4.91758898,
            3.93334984,
            3.6464028
          ]
        },
        {
          name: 'Liberte Whole Milk Yogurt (Strawberry)',
          data: [
            3.75349527,
            4.63229555,
            7.12204376,
            5.94953814,
            7.49953032,
            4.98541075,
            4.19431583,
            1.83706138
          ]
        },
        {
          name: 'Oui French Style Yogurt (Strawberry)',
          data: [
            2.72270579,
            5.42713706,
            6.46552413,
            7.07379193,
            7.19344126,
            4.47150305,
            4.61515041,
            2.27020332
          ]
        },
        {
          name: 'Yoplait Fruitside Yogurt (Strawberry)',
          data: [
            4.41304309,
            4.29291785,
            6.63744634,
            6.16686979,
            7.70123205,
            4.35450429,
            4.5309424,
            1.97680902
          ]
        },
        {
          name: 'Trader Joes Regular Yogurt (Strawberry)',
          data: [
            5.09175749,
            4.39003538,
            6.99855665,
            5.22383059,
            8.02850529,
            4.70544371,
            3.4071619,
            2.41715415
          ]
        }
      ],
      range: [0, 10]
    }
  })

  afterEach(() => {
    testRender.detach()
    testRender.unmount()
    global.document.body.removeChild(container)
    container = null
  })

  test('should render PolarChart', async () => {
    testRender = mount(
      <PolarChart inputData={inputData} />,
      { attachTo: container }
    )

    expect(testRender).toMatchSnapshot()

    expect(testRender.find('#polarchart_121').html()).toMatchSnapshot()

    await wait(500)
    expect(testRender.find('#polarchart_121').html()).toMatchSnapshot()
  })
})
