import * as d3 from 'd3'
import { tooltipLayer } from '../../../../utils/ChartsHelperFunctions'
import { NOT_PRODUCT } from '../../../../utils/chartUtils'

export const initChart = (
  parent_selector,
  data = [],
  range = [0, 10],
  chartSettings
) => {
  const svgWidth = 640
  const svgHeight = 600
  const svgMargin = 0
  const wordWrapWidth = 70
  const svgRadius = Math.min(svgWidth / 2, svgHeight / 2)
  const standardOpacity = 0
  const onHoverOpacity = 0.5
  const shapeSize = 5
  const referenceLinesNumber = 4

  // add the svg to the page
  const parent = d3.select(parent_selector)

  // necessary function for wrapping the words
  const wrap = text => {
    text.each(function (d) {
      const text = d3.select(this)
      let words = text
        .text()
        .split(/\s+/)
        .reverse()

      const lineHeight = 1.4
      let word

      let line = []
      let lineNumber = 0
      // ems
      const y = text.attr('y')

      const x = text.attr('x')

      const dy = -0.35
      let tspan = text
        .text(null)
        .append('tspan')
        .attr('x', x)
        .attr('y', y)
        .attr('dy', dy + 'em')

      while ((word = words.pop())) {
        line.push(word)
        tspan.text(line.join(' '))
        if (tspan.node().getComputedTextLength() > wordWrapWidth) {
          if (lineNumber === 1 || line.length === 1) {
            line.pop()
            line.push(line.length === 0 ? word.slice(0, 5) + '...' : ' ...')
            tspan.text(line.join(' '))
            text
              .on('mouseover', () =>
                tooltipLayer.showTooltip({ title: d.axis })
              )
              .on('mousemove', tooltipLayer.updateTooltip)
              .on('mouseleave', tooltipLayer.clearTooltip)
            break
          }
          line.pop()
          tspan.text(line.join(' '))
          line = [word]
          tspan = text
            .append('tspan')
            .attr('x', x)
            .attr('y', y)
            .attr('dy', ++lineNumber * lineHeight + dy + 'em')
            .text(word)
        }
      }
    })
  }

  const [minChartValue, maxChartValue] = range

  // the 'circle' reference lines
  let referenceLinesValue = []
  const referenceLinesStep =
    (maxChartValue - minChartValue) / referenceLinesNumber
  let step = minChartValue
  while (step <= maxChartValue) {
    referenceLinesValue.push(step)
    step = step + referenceLinesStep
  }

  // calculate the chart radial axes inclinations
  const spokesNumber = data[0]['axes'].length
  const spokesDegree = (Math.PI * 2) / spokesNumber

  // useful functions
  const rScale = d3
    .scaleLinear()
    .range([0, svgRadius])
    .domain([minChartValue, maxChartValue])

  const svgFullWidth = svgWidth + 2 * svgMargin + 2 * wordWrapWidth
  parent.select('svg').remove()
  let svgChart = parent
    .append('svg')
    .attr('width', svgFullWidth)
    .attr('height', svgHeight + 2 * svgMargin + wordWrapWidth * 1.1)
    .attr('class', 'radar')
    .attr(
      'transform',
      'translate(' +
        (d3
          .select(parent_selector)
          .node()
          .getBoundingClientRect().width -
          svgFullWidth) /
          2 +
        ', 0)'
    )

  // Hardfix for triangle, otherwise looks wrong aligned
  const verticalTranslate =
    data[0]['axes'].length === 3
      ? svgHeight / 2.5 + svgMargin + wordWrapWidth / 2
      : svgHeight / 2 + svgMargin + wordWrapWidth / 2

  // draw the chart lines (the chart without data)
  let chartLines = svgChart
    .append('g')
    .attr(
      'transform',
      'translate(' +
        (svgWidth / 2 + svgMargin + wordWrapWidth) +
        ',' +
        verticalTranslate +
        ')'
    )
    .attr('class', 'spokesWrapper')
    .attr('width', svgWidth + 2 * svgMargin)
    .attr('height', svgHeight + svgMargin)

  // draw the charts spoke lines (the one going outwards from the center)
  chartLines
    .selectAll('.spokeLine')
    .data(data[0]['axes'])
    .enter()
    .append('line')
    .attr('x1', 0)
    .attr('y1', 0)
    .attr(
      'x2',
      (spoke, index) =>
        rScale(maxChartValue) *
        Math.cos(spokesDegree * index + spokesDegree / 2 - Math.PI / 2)
    )
    .attr(
      'y2',
      (spoke, index) =>
        rScale(maxChartValue) *
        Math.sin(spokesDegree * index + spokesDegree / 2 - Math.PI / 2)
    )
    .attr('class', 'line')
    .style('stroke', '#CDCDCD')

  // add the label needed for each spoke
  let chartLabels = svgChart
    .append('g')
    .attr('class', 'labelsWrapper')
    .attr(
      'transform',
      'translate(' +
        (svgWidth / 2 + svgMargin + wordWrapWidth) +
        ',' +
        verticalTranslate +
        ')'
    )
    .attr('width', svgWidth + 2 * svgMargin)
    .attr('height', svgHeight + svgMargin)

  // the labels for the chart spokes
  chartLabels
    .selectAll('.spokeLabel')
    .data(data[0]['axes'])
    .enter()
    .append('text')
    .attr('class', 'spokeLabel')
    .attr('text-anchor', 'middle')
    .attr('dy', '0.35em')
    .text(data => data.axis.trim && data.axis.trim())
    .attr('x', (label, index) => {
      const degree = spokesDegree * index + spokesDegree / 2
      const outStand =
        degree < Math.PI / 4 ||
        degree > (7 * Math.PI) / 4 ||
        (degree > (3 * Math.PI) / 4 && degree < (5 * Math.PI) / 4)
          ? 1.08
          : 1.15
      return (
        rScale(maxChartValue * outStand) *
        Math.cos(spokesDegree * index + spokesDegree / 2 - Math.PI / 2)
      )
    })
    .attr('y', (label, index) => {
      const degree = spokesDegree * index + spokesDegree / 2
      const outStand =
        degree < Math.PI / 4 ||
        degree > (7 * Math.PI) / 4 ||
        (degree > (3 * Math.PI) / 4 && degree < (5 * Math.PI) / 4)
          ? 1.08
          : 1.15
      return (
        rScale(maxChartValue * outStand) *
        Math.sin(spokesDegree * index + spokesDegree / 2 - Math.PI / 2)
      )
    })
    .call(wrap)

  // calculate the reference Lines coordinate so that the d3 code is simpler
  const calculateReferenceLineCoordinates = (
    value,
    firstSpokeIndex,
    secondSpokeIndex
  ) => {
    return {
      x1:
        rScale(value) *
        Math.cos(
          spokesDegree * firstSpokeIndex + spokesDegree / 2 - Math.PI / 2
        ),
      x2:
        rScale(value) *
        Math.cos(
          spokesDegree * secondSpokeIndex + spokesDegree / 2 - Math.PI / 2
        ),
      y1:
        rScale(value) *
        Math.sin(
          spokesDegree * firstSpokeIndex + spokesDegree / 2 - Math.PI / 2
        ),
      y2:
        rScale(value) *
        Math.sin(
          spokesDegree * secondSpokeIndex + spokesDegree / 2 - Math.PI / 2
        )
    }
  }
  let referenceLinesCoordinates = referenceLinesValue.map((value, index) => {
    let spokeIndex = 0
    let referenceLineCoordinates = [
      calculateReferenceLineCoordinates(value, 0, spokesNumber - 1)
    ]
    while (spokeIndex < spokesNumber - 1) {
      referenceLineCoordinates.push(
        calculateReferenceLineCoordinates(value, spokeIndex, spokeIndex + 1)
      )
      spokeIndex += 1
    }
    return referenceLineCoordinates
  })

  // draw the reference lines for value (the lines that intersect the spokes at the same value / distance from teh center)
  let referenceLines = chartLines.append('g').attr('class', 'referenceLines')
  const decimals = !isNaN(chartSettings.howManyDecimals)
    ? parseInt(chartSettings.howManyDecimals, 10)
    : 2

  referenceLinesCoordinates.forEach((referenceLineCoordinates, i) => {
    referenceLines
      .selectAll('.referenceLines')
      .attr('class', 'referenceLine')
      .data(referenceLineCoordinates)
      .enter()
      .append('line')
      .attr('x1', data => data.x1)
      .attr('x2', data => data.x2)
      .attr('y1', data => data.y1)
      .attr('y2', data => data.y2)
      .attr('class', 'line')
      .style('stroke', 'currentColor')
    chartLabels
      .append('text')
      .attr('class', 'spokeLabel spokeLabelValue')
      .attr('text-anchor', 'middle')
      .attr('dy', '-6')
      .datum(referenceLinesValue[i])
      .text(referenceLinesValue[i].toFixed(decimals))
      .attr('y', referenceLineCoordinates[0].y1)
  })

  // add the wrapper for the chart data
  let chartData = svgChart
    .append('g')
    .attr('class', 'chartData')
    .attr(
      'transform',
      'translate(' +
        (svgWidth / 2 + svgMargin + wordWrapWidth) +
        ',' +
        verticalTranslate +
        ')'
    )
    .attr('width', svgWidth + 2 * svgMargin)
    .attr('height', svgHeight + svgMargin)

  // add the graph path generator
  const drawPath = d3
    .radialLine()
    .curve(d3.curveLinearClosed)
    .radius(dataEntry => rScale(dataEntry.value))
    .angle((dataEntry, index) => spokesDegree * index + spokesDegree / 2)

  chartData
    .selectAll('.productChart')
    .data(
      data.sort((el1, el2) => {
        let diff = 0
        el1.axes.forEach(
          (e, i) => (diff += el1.axes[i].value - el2.axes[i].value)
        )
        return -diff
      }),
      d => d.productIndex
    )
    .enter()
    .append('g')
    .attr('class', 'productChart')
    .on('mouseover', d => {
      tooltipLayer.showTooltip({
        title: d.name === NOT_PRODUCT ? null : d.name,
        results: d.axes.map(res => ({
          label: res.axis,
          value: res.value.toFixed(decimals)
        }))
      })
    })
    .on('mousemove', tooltipLayer.updateTooltip)
    .on('mouseleave', tooltipLayer.clearTooltip)
    .each(function (d) {
      const element = d3.select(this)

      element
        .append('path')
        .attr('class', 'productChartPath')
        .attr('d', function (product, i) {
          return drawPath(product.axes)
        })
        .style('stroke', product => product.color)
        .style('stroke-width', '2px')
        .style('fill', 'none')

      element
        .append('path')
        .attr('class', 'productChartBackground')
        .attr('d', function (product, i) {
          return drawPath(product.axes)
        })
        .style('fill', product => product.color)
        .style('fill-opacity', standardOpacity)
        // add change in opacity when mouseover
        .on('mouseover', function (d, i) {
          parent
            .selectAll('.productChartBackground')
            .transition()
            .duration(200)
            .style('fill-opacity', standardOpacity)
          d3.select(this)
            .transition()
            .duration(200)
            .style('fill-opacity', onHoverOpacity)
        })
        .on('mouseout', () => {
          parent
            .selectAll('.productChartBackground')
            .transition()
            .duration(200)
            .style('fill-opacity', standardOpacity)
        })

      let index = 0
      for (let dataEntry of d.axes) {
        let dot = null
        const posx =
          rScale(dataEntry.value) *
          Math.cos(spokesDegree * index + spokesDegree / 2 - Math.PI / 2)
        const posy =
          rScale(dataEntry.value) *
          Math.sin(spokesDegree * index + spokesDegree / 2 - Math.PI / 2)
        if (d.shape === 'rect') {
          dot = element
            .append('rect')
            .attr('height', shapeSize * 2)
            .attr('width', shapeSize * 2)
            .attr('x', posx - shapeSize)
            .attr('y', posy - shapeSize)
        } else if (d.shape === 'circle') {
          dot = element
            .append('circle')
            .attr('r', shapeSize)
            .attr('cx', posx)
            .attr('cy', posy)
        } else if (d.shape === 'triangle') {
          const pos1x = posx
          const pos2x = posx - shapeSize
          const pos3x = posx + shapeSize
          const pos1y = posy - shapeSize
          const pos2y = posy + shapeSize
          const pos3y = posy + shapeSize
          dot = element.append('polyline').attr(
            'points',
            `${pos1x} ${pos1y}, 
          ${pos2x} ${pos2y}, 
          ${pos3x} ${pos3y}`
          )
        }

        dot
          .style('fill', 'none')
          .style('stroke', 'none')
          .style(d.shapeType, d.color)
          .attr('class', 'graphShape')
          .style('pointer-events', 'all')

        index += 1
      }
    })
}

export const updateChart = (parent_selector, data = [], chartSettings) => {
  const selectedProducts = data.map(el => el.name)
  d3.select(parent_selector)
    .selectAll('.productChart')
    .each(function (d) {
      const element = d3.select(this)
      if (selectedProducts.indexOf(d.name) === -1) {
        element.style('display', 'none')
      } else {
        element.style('display', 'block')
      }
    })
    .data(data, d => d.productIndex)
    .each(function (d) {
      const element = d3.select(this)
      element.select('.productChartPath').style('stroke', d.color)
      element.select('.productChartBackground').style('fill', d.color)
      element.selectAll('.graphShape').style(d.shapeType, d.color)
    })

  const decimals = !isNaN(chartSettings.howManyDecimals)
    ? parseInt(chartSettings.howManyDecimals, 10)
    : 2

  const decimalsAxis = !isNaN(chartSettings.howManyDecimals_axis)
    ? parseInt(chartSettings.howManyDecimals_axis, 10)
    : 2

  d3.select(parent_selector)
    .selectAll('.spokeLabelValue')
    .text(d => d.toFixed(decimalsAxis))

  d3.select(parent_selector)
    .selectAll('.productChart')
    .on('mouseover', d => {
      tooltipLayer.showTooltip({
        title: d.name === NOT_PRODUCT ? null : d.name,
        results: d.axes.map(res => ({
          label: res.axis,
          value: res.value.toFixed(decimals)
        }))
      })
    })
}
