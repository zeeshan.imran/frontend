import React from 'react'

import ChartSwitch from './ChartSwitch'

const ChartsPdf = ({
  charts,
  hideFilter = true,
  idx,
  chartsSettings,
  t,
  fullWidth = '850px',
}) => {
  const fullCardWidth = fullWidth
  const halfCardWidth = '445px'
  const cardMargin = '15px auto'
  let syncedCharts = null
  if (charts) {
    syncedCharts = { ...charts, ...{ loading: false } }
  }
  return (
    <React.Fragment>
      <ChartSwitch
        el={syncedCharts}
        idx={idx}
        chartsSettings={chartsSettings}
        fullCardWidth={fullCardWidth}
        halfCardWidth={halfCardWidth}
        cardMargin={cardMargin}
        hideFilter={hideFilter}
        t={t}
      />
    </React.Fragment>
  )
}

export default ChartsPdf
