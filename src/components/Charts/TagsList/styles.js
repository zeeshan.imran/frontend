import styled from 'styled-components'
import { DefaultChart } from '../styles'
import { Tag } from 'antd'

export const Container = styled(DefaultChart)``

export const StyledTag = styled(Tag)`
  overflow: hidden;
  position:relative;
  max-width: 100%;
  width : 100%;
  margin-right: 0px;
  &::after {
    position: absolute;
    content : '...';
    top : 50%;
    transform:translateY(-52%);
    right : 5px;
  }
`
export const StyledSpan = styled.span`
  overflow: hidden;
  max-width: 90%;
  display: inline-block;
  vertical-align: middle;
`
export const SimpleTag = styled(Tag)`
  overflow: hidden;
  position:relative;
  max-width: 100%;
  width : 100%;
  margin-right: 0px;
`