import React, { useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { Container } from './styles'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import { getAnalyticValuesExtent } from '../../../containers/Charts/GetData'
import { tooltipLayer } from '../../../utils/ChartsHelperFunctions'
import { NOT_PRODUCT } from '../../../utils/chartUtils'

const ColumnsChartMean = ({ cardWidth, inputData, chartSettings = {}, t }) => {
  const chartName = 'columnchartmean_' + inputData.chart_id
  const chartMargin = { top: 16, right: 20, bottom: 25, left: 26 }
  const svgWidth = parseInt(cardWidth, 10) * 0.95 + 'px'
  const svgHeight = parseInt(cardWidth, 10) * 0.28 + 'px'
  const chartWidth =
    parseInt(svgWidth, 10) - chartMargin.left - chartMargin.right
  const chartHeight =
    parseInt(svgHeight, 10) - chartMargin.top - chartMargin.bottom

  const products = inputData.avg.map((el, i) => ({
    name: el.label,
    index: i,
    value: el.value
  }))

  const productsIndexList = products.map(el => el.index)

  const valuesExtent = useMemo(() => getAnalyticValuesExtent(inputData), [
    inputData
  ])

  const xScale = d3
    .scaleBand()
    .rangeRound([0, chartWidth])
    .paddingInner(0.2)
    .paddingOuter(0.1)
    .domain(productsIndexList)

  const colorsScale = d3
    .scaleOrdinal()
    .range(chartSettings.colorsArr || defaultChartSettings.colorsArr)
    .domain(productsIndexList)

  const mounted = useRef()

  useEffect(() => {
    if (!mounted.current) {
      initChart(inputData)
      mounted.current = true
    } else {
      updateChart()
    }
  }, [
    products,
    chartSettings.hasOutline,
    chartSettings.hasOutline_color,
    chartSettings.hasOutline_width,
    chartSettings.colorsArr,
    chartSettings.isCountProLabelShown,
    chartSettings.howManyDecimals,
    chartSettings.sortOrder
  ])

  const initChart = async inputData => {
    d3.select('.' + chartName).remove()

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)
      .append('div')
      .attr('class', 'columns-mean-container')

    const chartSvg = chartsContainer
      .append('div')
      .attr('class', 'columns-mean')
      .style('width', svgWidth)
      .append('svg')

    const g = chartSvg
      .attr('width', chartWidth + chartMargin.left + chartMargin.right)
      .attr('height', chartHeight + chartMargin.top + chartMargin.bottom)
      .append('g')
      .attr(
        'transform',
        'translate(' + chartMargin.left + ',' + chartMargin.top + ')'
      )

    g.append('g').attr('class', 'axisY')

    g.append('g')
      .attr('class', 'axisX')
      .attr('transform', 'translate(0,' + chartHeight + ')')

    g.append('g').attr('class', 'columns-mean-g-cont')

    chartsContainer
      .select('.columns-mean')
      .append('div')
      .attr('class', 'xLabels')
      .style('position', 'relative')
      .style('left', chartMargin.left + 'px')
      .style('top', -chartMargin.bottom + 'px')
      .selectAll('.xLabel')
      .data(productsIndexList, d => d)
      .enter()
      .append('div')
      .attr('class', 'xLabel')
      .style('width', xScale.step() + 'px')
      .style('position', 'absolute')

    updateChart()
  }

  const updateChart = () => {
    const maximalBarWidth = chartWidth / 6

    const chartsContainer = d3
      .select('#' + chartName)
      .select('.columns-mean-container')

    const y = d3
      .scaleLinear()
      .rangeRound([chartHeight, 0])
      .domain(valuesExtent)
      .nice()

    const yAxis = d3
      .axisLeft(y)
      .ticks(5)
      .tickSizeOuter(0)
      .tickSizeInner(-chartWidth)

    const customYAxis = g => {
      const s = g.selection ? g.selection() : g
      g.call(yAxis)
      s.select('.domain').remove()
      s.selectAll('.tick line').attr('x2', chartWidth)
    }

    chartsContainer
      .select('.axisX')
      .call(d3.axisBottom(xScale))
      .each(function () {
        d3.select(this)
          .selectAll('line')
          .attr('transform', 'translate(' + xScale.step() / 2 + ', 0)')

        d3.select(this)
          .selectAll('text')
          .remove()
      })

    chartsContainer
      .select('.axisY')
      .transition()
      .duration(300)
      .call(customYAxis)
      .on('end', function () {
        d3.select(this)
          .selectAll('.tick line')
          .attr('x2', chartWidth)
      })

    chartsContainer
      .selectAll('.xLabel')
      .data(productsIndexList, d => d)
      .style(
        'left',
        (d, i) =>
          `${1 +
            xScale(d) -
            (xScale.bandwidth() / (1 - xScale.paddingInner()) -
              xScale.bandwidth()) /
              2}px`
      )
      .each(function (d) {
        d3.select(this)
          .select('span')
          .remove()
        const txtSpan = d3.select(this).append('span')
        let txt = products.find(el => el.index === d).name
        if (txt === NOT_PRODUCT) {
          txt = t('charts.table.mean')
        }
        for (let i = 0; i <= 5; i++) {
          if (i === 1) {
            d3.select(this)
              .on('mouseover', () => tooltipLayer.showTooltip({ title: d }))
              .on('mousemove', tooltipLayer.updateTooltip)
              .on('mouseleave', tooltipLayer.clearTooltip)
          }
          const txtW = txtSpan
            .text(txt)
            .node()
            .getBoundingClientRect().width
          if (txtW > xScale.step() - 10) {
            txt = txt.substring(0, txt.length / 2) + '..'
          } else {
            break
          }
        }
      })

    const columnsSelection = chartsContainer
      .select('.columns-mean-g-cont')
      .selectAll('.outerContainer')
      .data(products)

    columnsSelection
      .enter()
      .append('g')
      .classed('outerContainer', true)
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', tooltipLayer.clearTooltip)
      .each(function (d) {
        d3.select(this)
          .append('rect')
          .attr('y', chartHeight)
          .attr('height', 0)
        d3.select(this)
          .append('text')
          .attr('text-anchor', 'middle')
          .style('font-weight', 'bold')
          .style('font-size', '10px')
          .style('fill', '#595959')
      })
      .merge(columnsSelection)
      .on('mouseover', (d, i) =>
        tooltipLayer.showTooltip({
          title: !inputData.results.data ? d.label : null,
          results: [
            {
              label: t('charts.table.mean'),
              value: parseFloat(d.value).toFixed(2)
            }
          ]
        })
      )
      .each(function (d, i) {
        const width =
          maximalBarWidth < xScale.bandwidth()
            ? maximalBarWidth
            : xScale.bandwidth()
        const xStart =
          maximalBarWidth < xScale.bandwidth()
            ? xScale(d.index) + (xScale.bandwidth() - maximalBarWidth) / 2
            : xScale(d.index)
        const yStart = y(d.value)

        d3.select(this)
          .select('rect')
          .attr('x', xStart)
          .attr('width', width)
          .transition()
          .duration(400)
          .attr('y', yStart)
          .attr('height', chartHeight - y(d.value))
          .attr('fill', colorsScale(d.index))
          .attr(
            'stroke',
            chartSettings.hasOutline_color
              ? chartSettings.hasOutline_color[0]
              : defaultChartSettings.outlineColor
          )
          .attr(
            'stroke-width',
            chartSettings.hasOutline ? chartSettings.hasOutline_width || 1 : 0
          )

        const labelDecimals = !isNaN(chartSettings.howManyDecimals)
          ? chartSettings.howManyDecimals
          : 2
        d3.select(this)
          .select('text')
          .attr('x', xStart + width / 2 - 1)
          .attr('y', yStart - 4)
          .style('opacity', 0)
          .transition(300)
          .style('opacity', chartSettings.isCountProLabelShown ? 1 : 0)
          .text(d.value.toFixed(labelDecimals))
      })
  }

  return (
    <Container>
      <div id={chartName} />
    </Container>
  )
}

export default ColumnsChartMean
