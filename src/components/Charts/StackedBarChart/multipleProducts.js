import React from 'react'
import { Container, ProductName, ProductSection } from './styles'
import StackedBarChart from './index'
import DataTable from '../DataTable'
import {
  getTableData,
  getStatisticsData,
  groupResultsByLabels
} from '../../../containers/Charts/GetData'

// Stacked or horizontal bar chart divided in product secions
const MultipleProductsStackedBarChart = ({
  cardWidth,
  inputData,
  chartSettings,
  t
}) => {
  const isStackedBar = inputData.chart_type === 'stacked-bar'
  return (
    <Container>
      {inputData.results.map((productResult, i) => {
        const productInputData = {
          ...inputData,
          labels: inputData.labels,
          chart_id: inputData.chart_id + '_' + i,
          results: isStackedBar ? productResult.attributes : productResult
        }
        const groupedData = groupResultsByLabels(
          productInputData,
          chartSettings.groupLabels,
          chartSettings.sortOrder
        )
        return (
          <ProductSection>
            <ProductName>
              {isStackedBar ? productResult.productName : productResult.name}
            </ProductName>
            <StackedBarChart
              {...{
                cardWidth,
                inputData: groupedData,
                chartSettings,
                t
              }}
            />
            {chartSettings.isStatisticsTableShown && (
              <DataTable
                {...getStatisticsData(groupedData, chartSettings, t)}
              />
            )}
            {chartSettings.isDataTableShown && (
              <DataTable {...getTableData(groupedData, chartSettings, t)} />
            )}
          </ProductSection>
        )
      })}
    </Container>
  )
}

export default MultipleProductsStackedBarChart
