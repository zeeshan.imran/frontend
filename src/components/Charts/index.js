import React from 'react'
import { ChartsRow } from './styles'

import ChartSwitch from './ChartSwitch'

const Charts = ({
  charts,
  chartsSettings,
  saveChartsSettings,
  hideFilter = false,
  isOwner,
  t
}) => {
  const fullCardWidth = '930px'
  const halfCardWidth = '445px'
  const cardMargin = '15px auto'

  return (
    <React.Fragment>
      <ChartsRow>
        {charts.map((el, idx) => {
          return (
            <ChartSwitch
              key={idx}
              el={el}
              idx={idx}
              fullCardWidth={fullCardWidth}
              halfCardWidth={halfCardWidth}
              cardMargin={cardMargin}
              chartsSettings={chartsSettings}
              saveChartsSettings={saveChartsSettings}
              hideFilter={hideFilter}
              isOwner={isOwner}
              t={t}
            />
          )
        })}
      </ChartsRow>
    </React.Fragment>
  )
}

export default Charts
