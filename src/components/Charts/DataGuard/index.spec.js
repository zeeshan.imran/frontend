import React from 'react'
import { mount } from 'enzyme'
import DataGuard from '.'
import * as Yup from 'yup'

jest.mock('../../../containers/StatsOptions', () => {
  return <div></div>
})


describe('DataGuard', () => {
  let testRender
  let component
  let props
  let container

  beforeEach(() => {
    container = global.document.createElement('div', { class: 'tooltip-row' })
    global.document.body.appendChild(container)

    component = () => <div>mocked component</div>
    props = {
      cardWidth: 100,
      cardMargin: 10,
      validationSchema: Yup.object().shape({
        title: Yup.string().required(),
        otherTitle: Yup.string().required()
      }),
      chartType: 'all',
      inputData: { title: 'someTitle', otherTitle: null }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render DataGuard', () => {
    testRender = mount(<DataGuard component={component} {...props} />, {
      attachTo: container
    })
    expect(testRender.find(DataGuard)).toHaveLength(1)
  })

  test('should NOT render Component', () => {
    testRender = mount(
      <DataGuard
        component={component}
        {...props}
        inputData={{ title: 'someTitle', otherTitle: null }}
      />,
      {
        attachTo: container
      }
    )
    expect(testRender.find(DataGuard)).toHaveLength(1)
  })
})
