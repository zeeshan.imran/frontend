// this file need console.log, it will help us to print the root cause of errors
/* eslint-disable no-console */
import React, { useMemo } from 'react'
import { Card, Icon, Spin } from 'antd'
import { uniq } from 'ramda'
import { DefaultChart } from '../styles'
import Title from 'antd/lib/typography/Title'
import { ErrorWrapper, SpinContainer, TotalRespondents } from './styles'
import StatsOptions from '../../../containers/StatsOptions'
import DataTable from '../DataTable'
import {
  getTableData,
  getStatisticsData,
  swapProductsWithLabels,
  groupResultsByLabels
} from '../../../containers/Charts/GetData'
import { getAvailableChartSettings } from '../../../utils/chartSettings'

const DataGuard = ({ component, t, ...props }) => {
  const {
    cardWidth,
    cardMargin,
    bodyStyle,
    validationSchema,
    inputData,
    chartSettings = {},
    saveChartsSettings,
    hideFilter = false,
    isOwner,
    showAll = false,
    chartId
  } = props

  if (inputData.loading) {
    return (
      <Card
        title={inputData.title}
        style={{ width: cardWidth, margin: cardMargin, padding: 0 }}
        bodyStyle={bodyStyle}
      >
        <SpinContainer>
          <Spin size='default' />
        </SpinContainer>
      </Card>
    )
  }

  const filteredSettings = useMemo(() => {
    const availableSettings = getAvailableChartSettings({
      chartType: inputData.chart_type,
      questionType: inputData.question_type,
      hasProducts: inputData.question_with_products
    }).map(el => el.name)

    const toRet = Object.keys(chartSettings)
      .filter(key => availableSettings.includes(key))
      .reduce((obj, key) => {
        obj[key] = chartSettings[key]
        return obj
      }, {})

    return toRet
  }, [chartSettings])

  const groupedData = useMemo(() => {
    return !inputData.attributes // Special case for multiple stacked bars
      ? groupResultsByLabels(
          inputData,
          filteredSettings.groupLabels,
          filteredSettings.sortOrder
        )
      : inputData
  }, [filteredSettings.groupLabels, filteredSettings.sortOrder, inputData])

  const swappedInput = useMemo(() => {
    return Array.isArray(groupedData.results) && !groupedData.attributes // Special case for multiple stacked bars
      ? swapProductsWithLabels(groupedData)
      : groupedData
  }, [groupedData, filteredSettings.isProductLabelsSwapped])

  const chartType =
    groupedData.chart_type && groupedData.chart_type.length
      ? groupedData.chart_type
      : 'PieChart'

  try {
    if (validationSchema)
      validationSchema.validateSync(groupedData, { abortEarly: false })

    const ChartComponent = component
    return (
      <Card
        title={groupedData.title}
        className={`svg-chart-${chartId}`}
        extra={
          !hideFilter && (
            <StatsOptions
              isOwner={isOwner}
              chartSettings={filteredSettings}
              saveChartsSettings={saveChartsSettings}
              chartType={chartType}
              question_with_products={groupedData.question_with_products}
              questionType={groupedData.question_type}
              chartName={groupedData.title}
              question={groupedData.question}
              chartId={`svg-chart-${chartId}`}
            />
          )
        }
        style={{ width: cardWidth, margin: cardMargin, padding: 0 }}
        bodyStyle={bodyStyle}
      >
        {filteredSettings.isTotalRespondentsShown && (
          <TotalRespondents>
            {t('charts.totalRespondents')}: {inputData.totalRespondents}
          </TotalRespondents>
        )}
        <ChartComponent
          {...{
            ...props,
            chartSettings: filteredSettings,
            t,
            inputData: filteredSettings.isProductLabelsSwapped
              ? swappedInput
              : groupedData,
            showAllInTable: showAll
          }}
        />
        {filteredSettings.isStatisticsTableShown &&
        !groupedData.attributes && ( // Special case for multiple stacked bars
            <DataTable
              showAll={showAll}
              {...getStatisticsData(groupedData, filteredSettings, t)}
            />
          )}
        {filteredSettings.isDataTableShown &&
        !groupedData.attributes && ( // Special case for multiple stacked bars
            <DataTable
              showAll={showAll}
              {...getTableData(groupedData, filteredSettings, t)}
            />
          )}
      </Card>
    )
  } catch (ex) {
    console.groupCollapsed('Chart data input is invalid')
    console.log({ groupedData, errors: (ex.errors && uniq(ex.errors)) || ex })
    console.groupEnd('Chart data input is invalid')

    return (
      <DefaultChart>
        <Card
          title={groupedData.title}
          style={{ width: cardWidth, margin: cardMargin }}
        >
          <ErrorWrapper>
            <Title level={3}>
              <Icon type='api' /> Sorry, we can not render this chart.
            </Title>
          </ErrorWrapper>
        </Card>
      </DefaultChart>
    )
  }
}

export default DataGuard
