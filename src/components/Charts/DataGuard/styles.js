import styled from 'styled-components'

export const ErrorWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: rgba(0, 0, 0, 0.45);
  min-height: 80px;
`

export const SpinContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 80px;
`

export const TotalRespondents = styled.div`
  margin: 0 0 12px 6px;
`
