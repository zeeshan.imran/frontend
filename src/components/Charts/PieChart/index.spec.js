import React from 'react'
import { mount } from 'enzyme'
import PieChart from './index'
import wait from '../../../utils/testUtils/waait'

describe('PieChart', () => {
  let testRender
  let chartIndex
  let inputData
  let container

  beforeEach(() => {
    container = global.document.createElement('div', { class: 'tooltip-row' })
    global.document.body.appendChild(container)
    chartIndex = 1
    inputData = {
      chart_id: '121',
      chart_type: 'pie',
      title: 'Consumption Frequency',
      trial: '',
      results: {
        data: [
          {
            label: '2-3 times a day',
            value: 25
          },
          {
            label: 'Daily',
            value: 103
          },
          {
            label: '2-3 times a week',
            value: 144
          }
        ]
      },
      labels: ['2-3 times a day', 'Daily', '2-3 times a week']
    }
  })

  afterEach(() => {
    // testRender.detach()
    // testRender.unmount()
    global.document.body.removeChild(container)
    container = null
  })

  test('should render PieChart', async () => {
    testRender = mount(
      <PieChart chartIndex={chartIndex} inputData={inputData} />,
      { attachTo: container }
    )
    expect(testRender).toMatchSnapshot()
    expect(testRender.find('#piechart_121').html()).toMatchSnapshot()
    await wait(500)
    expect(testRender.find('#piechart_121').html()).toMatchSnapshot()
  })
})
