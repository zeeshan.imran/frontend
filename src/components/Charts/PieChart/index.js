import React, { useState, useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { Container } from './styles'
import { parseData } from '../../../containers/Charts/PieChart/GetData'
import {
  tooltipLayer,
  hasCollision,
  appendChartLegend,
  updateChartLegendColorScheme
} from '../../../utils/ChartsHelperFunctions'
import { getTotalSum } from '../../../containers/Charts/GetData'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import { DISABLED_COLOR } from '../../../utils/chartUtils'

const PieChart = ({ cardWidth, inputData, t, chartSettings = {} }) => {
  const cardPad = 24
  const svgWidth = parseInt(cardWidth, 10) - 2 * cardPad
  const svgHeight = 0.764 * parseInt(cardWidth, 10)
  const height = svgHeight
  const radius = svgHeight / 2
  const chartName = 'piechart_' + inputData.chart_id
  const color = d3
    .scaleOrdinal()
    .range(chartSettings.colorsArr || defaultChartSettings.colorsArr)

  const [labelsSelection, setLabelsSelection] = useState([])
  const mounted = useRef()
  const totalSum = useMemo(() => getTotalSum(inputData), [inputData])

  useEffect(() => {
    initPieChart()
  }, [inputData])

  useEffect(() => {
    updatePieChart()
  }, [labelsSelection, chartSettings.howManyDecimals, chartSettings.valueType])

  useEffect(() => {
    if (mounted.current) {
      updatePieChart()
      updateChartLegendColorScheme(
        d3.select('#' + chartName),
        color.copy().domain(labelsSelection.map(el => el.index))
      )
    }
  }, [chartSettings.colorsArr])

  const initPieChart = () => {
    d3.select('.' + chartName).remove()
    d3.select('#' + chartName)
      .select('.chart-legend')
      .remove()

    const svgChart = d3
      .select('#' + chartName)
      .append('svg')
      .attr('class', chartName)
      .attr('width', svgWidth)
      .attr('height', svgHeight)
      .append('g')
      .attr('class', 'donutcontainer')

    svgChart.append('g').attr('class', 'slices')
    svgChart.append('g').attr('class', 'labels')

    d3.select('#' + chartName)
      .append('div')
      .attr('class', 'msgInner')
      .style('display', 'none')

    const initLabelSelection = inputData.labels.map((el, i) => ({
      name: el,
      index: i,
      isSelected: true
    }))

    setLabelsSelection(initLabelSelection)

    appendChartLegend(
      d3.select('#' + chartName),
      initLabelSelection,
      color.copy().domain(initLabelSelection.map(el => el.index)),
      setLabelsSelection
    )

    svgChart.attr(
      'transform',
      'translate(' + svgWidth / 2 + ',' + height / 2 + ')'
    )
  }

  const updatePieChart = () => {
    const svgChart = d3.select('.' + chartName).select('.donutcontainer')
    let dataPie = parseData(inputData, labelsSelection)
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? parseInt(chartSettings.howManyDecimals, 10)
      : 2

    color.domain(inputData.labels.map((el, i) => i))
    if (totalSum === 0) {
      dataPie = [
        {
          value: t('charts.noResults'),
          fakeValue: 1
        }
      ]
    } else {
      // replace 0 with small value to make all results visible
      dataPie.forEach(
        el => (el.fakeValue = el.value === 0 ? totalSum / 500 : el.value)
      )
    }

    const pie = d3
      .pie()
      .sort(null)
      .value(d => d.fakeValue)
    const arc = d3
      .arc()
      .innerRadius(radius * 0.67)
      .outerRadius(radius * 0.42)

    const outerArc = d3
      .arc()
      .outerRadius(radius * 0.75)
      .innerRadius(radius * 0.75)

    const slices = svgChart.selectAll('path.slices').data(pie(dataPie))

    slices
      .enter()
      .append('path')
      .attr('class', 'slices')
      .merge(slices)
      .attr('d', arc)
      .attr('fill', (d, i) =>
        totalSum === 0 ? DISABLED_COLOR : color(d.data.index)
      )

    if (totalSum !== 0) {
      svgChart
        .selectAll('path.slices')
        .on('mouseover', function (d, i) {
          if (d.data.index === undefined) {
            return
          }
          d3.select(this).classed('active', true)
          tooltipLayer.showTooltip({
            title: labelsSelection[d.data.index].name,
            results: [
              {
                label: t('charts.tooltip.total'),
                value: d.value % 1 === 0 ? d.value : d.value.toFixed(decimals)
              },
              {
                label: t('charts.tooltip.percentageOfTotal'),
                value:
                  ((100 * d.value) / Math.max(totalSum, 1)).toFixed(decimals) +
                  '%'
              }
            ]
          })
        })
        .on('mouseout', function (d) {
          d3.select(this).classed('active', false)
          tooltipLayer.clearTooltip()
        })
        .on('mousemove', tooltipLayer.updateTooltip)
    }

    slices.exit().remove()

    const lastPartLength = 20

    const labels = svgChart
      .select('.labels')
      .html('')
      .selectAll('.label')
      .data(pie(dataPie))

    if (totalSum !== 0) {
      d3.select('#' + chartName)
        .select('.msgInner')
        .style('display', 'none')
      labels
        .enter()
        .append('g')
        .attr('class', 'label')
        .merge(labels)
        .each(function (d, i) {
          const point_1 = arc.centroid(d)
          const point_2 = outerArc.centroid(d)
          const sliceAngle = midAngle(d)
          let labelAngle =
            (sliceAngle + (Math.PI / 2) * (sliceAngle < Math.PI ? -1 : 1)) / 2

          let tryCounter = 13
          while (tryCounter > 0) {
            const point_4 = [
              point_2[0] + Math.cos(labelAngle) * (lastPartLength + 15),
              point_2[1] + Math.sin(labelAngle) * (lastPartLength + 15)
            ]
            let overlap = false
            d3.select(this)
              .select('text')
              .remove()

            d3.select(this)
              .append('text')
              .attr('dy', '.35em')
              .merge(labels)
              .text(d =>
                chartSettings.valueType === 'percentage'
                  ? ((100 * d.data.value) / Math.max(totalSum, 1)).toFixed(
                      decimals
                    ) + '%'
                  : d.data.value
              )
              .attr('transform', `translate(${point_4})`)
              .style('text-anchor', 'middle')

            const bbox = d3
              .select(this)
              .select('text')
              .node()
              .getBoundingClientRect()

            svgChart
              .select('.labels')
              .selectAll('.label text')
              .each(function (d2) {
                if (d2.endAngle === d.endAngle) return
                const bbox2 = d3
                  .select(this)
                  .node()
                  .getBoundingClientRect()
                if (hasCollision(bbox, bbox2)) {
                  overlap = true
                  return true
                }
              })

            if (!overlap) {
              break
            } else {
              labelAngle = labelAngle + Math.PI / 14
              if (
                tryCounter === 1 ||
                Math.abs(labelAngle + Math.PI / 2 - sliceAngle) > Math.PI / 2
              ) {
                // cannot place labels
                labelAngle = null
                break
              }
            }
            tryCounter--
          }
          if (labelAngle === null) {
            d3.select(this).remove()
          } else {
            const point_3 = [
              point_2[0] + Math.cos(labelAngle) * lastPartLength,
              point_2[1] + Math.sin(labelAngle) * lastPartLength
            ]

            let pathD3 = d3.path()
            pathD3.moveTo(point_1[0], point_1[1])
            pathD3.quadraticCurveTo(
              point_2[0],
              point_2[1],
              point_3[0],
              point_3[1]
            )
            d3.select(this)
              .append('path')
              .attr('d', pathD3.toString())
              .attr('stroke', color(d.data.index))
              .attr('opacity', 1)
              .attr('stroke-width', '2px')
              .attr('fill', 'none')
          }
        })
    } else {
      d3.select('#' + chartName)
        .select('.msgInner')
        .style('display', 'block')
        .text(t('charts.noResults'))
    }
  }

  const midAngle = d => d.startAngle + (d.endAngle - d.startAngle) / 2

  return (
    <Container>
      <div id={chartName} className='pieChartCont' />
    </Container>
  )
}

export default PieChart
