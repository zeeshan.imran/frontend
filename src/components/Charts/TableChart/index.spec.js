import React from 'react'
import { mount } from 'enzyme'
import TableChart from './index'
import wait from '../../../utils/testUtils/waait'

describe('TableChart', () => {
  let testRender
  let inputData
  let container

  beforeEach(() => {
    container = global.document.createElement('div', { class: 'tooltip-row' })
    global.document.body.appendChild(container)
    inputData = {
      chart_id: '121',
      chart_type: 'table',
      title: 'Table',
      results: [
        ['Product Name 1', 1],
        ['Product Name 2', 2]
      ],
      labels: ['Products', 'Average']
    }
  })

  afterEach(() => {
    global.document.body.removeChild(container)
    container = null
  })

  test('should render Table', async () => {
    testRender = mount(<TableChart inputData={inputData} />, {
      attachTo: container
    })
    expect(testRender).toMatchSnapshot()
    expect(testRender.find('#tablechart_121').html()).toMatchSnapshot()
    await wait(500)
    expect(testRender.find('#tablechart_121').html()).toMatchSnapshot()
  })
})
