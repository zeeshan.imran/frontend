import styled from 'styled-components'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'
import { Row } from 'antd'

export const ChartsRow = styled(Row)`
  width: 965px;
  margin: 0 auto;
`

export const DefaultChart = styled.div`
  .axisY {
    color: ${colors.PALE_LIGHT_GREY};
  }

  .axisY text {
    color: ${colors.SLATE_GREY};
  }

  .axisX {
    color: ${colors.PALE_LIGHT_GREY};
  }

  .axisX text {
    font-weight: 700;
    font-size: 10px;
    font-family: ${family.primaryBold};
    color: #7f7f7f;
    line-height: 1.2;
  }

  .axisX .tick:last-child {
    display: none;
  }

  .axisX.allTicks .tick:last-child {
    display: block;
  }

  .referenceLines,
  .spokesWrapper {
    color: ${colors.PALE_LIGHT_GREY};
    line {
      stroke-width: 1.5px;
    }
  }

  .spokeLabel {
    font-weight: 700;
    font-size: 11px;
    font-family: ${family.primaryBold};
    color: #7f7f7f;
    line-height: 1.2;
  }

  .xLabel {
    text-align: center;
    font-weight: 700;
    font-size: 9px;
    font-family: ${family.primaryBold};
    color: #7f7f7f;
    padding: 0 5px;
    line-height: 1.2;
  }

  .yLabel {
    text-align: right;
    font-weight: 700;
    font-size: 9px;
    font-family: ${family.primaryBold};
    color: #7f7f7f;
    line-height: 1.2;
  }

  .chart-legend {
    line-height: 1.2;
    padding-top: 8px;
    text-align: center;
    &.padded {
      padding: 8px 20px;
    }
  }

  .chart-legend .legend {
    display: inline-block;
    position: relative;
    font-size: 11px;
    font-weight: 700;
    font-family: ${family.primaryBold};
    padding-left: 12px;
    cursor: pointer;
    margin: 2px 8px;
  }

  .chart-legend .legend.faded-legend {
    opacity: 0.5;
  }

  .chart-legend .legend .legend-marker {
    position: absolute;
    top: 2px;
    left: 0;
    border-radius: 50%;
    width: 8px;
    height: 8px;
  }

  .horizontal-barchart rect,
  .columns-barchart-g rect {
    transition: all 0.4s;
  }
`
