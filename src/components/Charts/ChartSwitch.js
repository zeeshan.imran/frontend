import React, { useMemo } from 'react'
import { Col } from 'antd'
import DataGuard from './DataGuard'
import { getSingleChartSettings } from '../../utils/ChartSettingsUtils'
import {
  CHART_SIZES,
  CARD_WIDTH,
  getChartSchema,
  getChartComponent
} from '../../utils/chartConfigs'

const ChartSwitch = ({
  el,
  idx,
  chartsSettings,
  saveChartsSettings,
  hideFilter,
  halfCardWidth,
  fullCardWidth,
  cardMargin,
  isOwner,
  t
}) => {
  const colSpan = CHART_SIZES[el.chart_type] || CHART_SIZES.DEFAULT
  const fullOrHalf = CARD_WIDTH[el.chart_type] || CARD_WIDTH.DEFAULT
  const cardWidth = { halfCardWidth, fullCardWidth }[`${fullOrHalf}CardWidth`]
  const chartProps = useMemo(
    () => ({
      t,
      chartType: el.chart_type,
      chartId: el.chart_id,
      inputData: el,
      cardWidth,
      cardMargin,
      validationSchema: getChartSchema(el),
      component: getChartComponent(el),
      saveChartsSettings,
      hideFilter,
      chartSettings: getSingleChartSettings(chartsSettings, el),
      isOwner
    }),
    [
      t,
      el,
      chartsSettings,
      saveChartsSettings,
      hideFilter,
      cardWidth,
      cardMargin,
      isOwner
    ]
  )
  switch (el.chart_type) {
    case '':
    case 'pie':
    case 'stacked-column-horizontal-bars':
    case 'stacked-column':
    case 'map':
    case 'column':
    case 'columns-mean':
    case 'polar':
    case 'line':
    case 'table':
    case 'spearmann-table':
    case 'pearson-table':
    case 'tags-list':
    case 'stacked-bar':
    case 'horizontal-bars-mean':
    case 'bar':
      return (
        <Col span={colSpan} key={el.title.replace(/[^a-z0-9]/gi, '') + idx}>
          <DataGuard {...chartProps} />
        </Col>
      )
    default:
      return (
        <Col span={12} key={el.title.replace(/[^a-z0-9]/gi, '') + idx}>
          {el.title}
        </Col>
      )
  }
}

export default ChartSwitch
