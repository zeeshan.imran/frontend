import React from 'react'
import { mount } from 'enzyme'
import LineChart from '.'
import wait from '../../../utils/testUtils/waait'

describe('LineChart', () => {
  let testRender
  let chartIndex
  let inputData
  let container

  beforeEach(() => {
    container = global.document.createElement('div', { class: 'tooltip-row' })
    global.document.body.appendChild(container)

    chartIndex = 1
    inputData = {
      chart_type: 'line',
      wide: true,
      chart_id: '121',
      as_percentage: true,
      title: 'Consumption Time (CST) (% of Total)',
      labels: [
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23
      ],
      results: {
        data: [
          {
            label: 0,
            analytics_value: 0,
            value: 0
          },
          {
            label: 1,
            analytics_value: 1,
            value: 0
          },
          {
            label: 2,
            analytics_value: 2,
            value: 0
          },
          {
            label: 3,
            analytics_value: 3,
            value: 0
          },
          {
            label: 4,
            analytics_value: 4,
            value: 0
          },
          {
            label: 5,
            analytics_value: 5,
            value: 0
          },
          {
            label: 6,
            analytics_value: 6,
            value: 0
          },
          {
            label: 7,
            analytics_value: 7,
            value: 0
          },
          {
            label: 8,
            analytics_value: 8,
            value: 0
          },
          {
            label: 9,
            analytics_value: 9,
            value: 0
          },
          {
            label: 10,
            analytics_value: 10,
            value: 0
          },
          {
            label: 11,
            analytics_value: 11,
            value: 0
          },
          {
            label: 12,
            analytics_value: 12,
            value: 0
          },
          {
            label: 13,
            analytics_value: 13,
            value: 0
          },
          {
            label: 14,
            analytics_value: 14,
            value: 0
          },
          {
            label: 15,
            analytics_value: 15,
            value: 0
          },
          {
            label: 16,
            analytics_value: 16,
            value: 0
          },
          {
            label: 17,
            analytics_value: 17,
            value: 3
          },
          {
            label: 18,
            analytics_value: 18,
            value: 0
          },
          {
            label: 19,
            analytics_value: 19,
            value: 0
          },
          {
            label: 20,
            analytics_value: 20,
            value: 0
          },
          {
            label: 21,
            analytics_value: 21,
            value: 0
          },
          {
            label: 22,
            analytics_value: 22,
            value: 0
          },
          {
            label: 23,
            analytics_value: 23,
            value: 0
          }
        ]
      }
    }
  })

  afterEach(() => {
    testRender.detach()
    testRender.unmount()
    global.document.body.removeChild(container)
    container = null
  })

  test('should render LineChart', async () => {
    testRender = mount(
      <LineChart
        chartIndex={chartIndex}
        inputData={inputData}
        chartSettings={{ singleColor: [] }}
      />,
      { attachTo: container }
    )

    expect(testRender).toMatchSnapshot()

    expect(testRender.find('#linechart_121').html()).toMatchSnapshot()

    await wait(500)
    expect(testRender.find('#linechart_121').html()).toMatchSnapshot()
  })

  // test('should render LineChart onmouseover', async () => {
  //   testRender = mount(
  //     <LineChart chartIndex={chartIndex} inputData={inputData} />,
  //     { attachTo: container }
  //   )
  //   testRender
  //     .find({ r: 5 })
  //     .prop('onmouseover')()
  // })
})
