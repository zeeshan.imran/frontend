import React, { useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { tooltipLayer } from '../../../utils/ChartsHelperFunctions'
import { getTotalSum } from '../../../containers/Charts/GetData'
import { Container } from './styles'
import { defaultChartSettings } from '../../../defaults/chartSettings'

const LineChart = ({ cardWidth, inputData, chartSettings = {}, t }) => {
  const chartName = 'linechart_' + inputData.chart_id
  const margin = { top: 20, right: 20, bottom: 25, left: 26 }

  const chartColor =
    (chartSettings &&
      chartSettings.singleColor &&
      chartSettings.singleColor[0]) ||
    defaultChartSettings.colorsArr[0]
  const chartWidth = parseInt(cardWidth, 10) * 0.95 + 'px'

  const mounted = useRef()
  const totalSum = useMemo(() => getTotalSum(inputData), [inputData])

  const width = parseInt(chartWidth, 10) - margin.left - margin.right
  const height =
    (parseInt(chartWidth, 10) * 320) / 1000 - margin.top - margin.bottom
  const xScale = d3
    .scalePoint()
    .domain(inputData.labels)
    .range([0, width])
    .padding(0.5)

  useEffect(() => {
    if (mounted.current) {
      updateLineChart()
    } else {
      initLineChart()
      mounted.current = true
    }
  }, [
    chartSettings.singleColor,
    chartSettings.valueType,
    chartSettings.howManyDecimals,
    chartSettings.isCountProLabelShown
  ])

  const initLineChart = () => {
    d3.select('.' + chartName).remove()

    const svg = d3
      .select('#' + chartName)
      .append('svg')
      .attr('class', chartName)
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('class', `g-${chartName}`)
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

    const xAxis = d3.axisBottom(xScale).ticks(inputData)

    const customXAxis = g => {
      g.call(xAxis).each(function () {
        d3.select(this)
          .selectAll('line')
          .attr('transform', 'translate(' + xScale.step() / 2 + ', 0)')
      })
      g.insert('g', ':first-child')
        .attr('class', 'tick')
        .append('line')
        .attr('y2', 6)
        .attr('stroke', 'currentColor')
      g.select('.domain').remove()
    }

    svg
      .append('g')
      .attr('class', 'axisX allTicks')
      .attr('transform', 'translate(0,' + height + ')')
      .call(customXAxis)

    svg.append('g').attr('class', 'axisY')

    updateLineChart()
  }

  const updateLineChart = () => {
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? chartSettings.howManyDecimals
      : 2

    const svg = d3.select('#' + chartName).select(`.g-${chartName}`)

    let yDomain

    if (chartSettings.valueType === 'percentage') {
      yDomain =
        totalSum === 0
          ? [0, 1]
          : [0, d3.max(inputData.results.data, d => d.value) / totalSum]
    } else {
      yDomain =
        totalSum === 0
          ? [0, 1]
          : [0, d3.max(inputData.results.data, d => d.value)]
    }
    const yScale = d3
      .scaleLinear()
      .domain(yDomain)
      .range([height, 0])

    const yAxis = d3
      .axisLeft(yScale)
      .tickSizeOuter(0)
      .tickSizeInner(-width)
      .ticks(5)
      .tickFormat(
        chartSettings.valueType === 'percentage' ? d3.format('.0%') : null
      )

    const customYAxis = g => {
      g.call(yAxis)
      g.select('.domain').remove()
    }

    svg.select('.axisY').call(customYAxis)

    const line = d3
      .line()
      .x(d => xScale(d.label))
      .y(d =>
        yScale(
          chartSettings.valueType === 'percentage'
            ? d.value / Math.max(totalSum, 1)
            : d.value
        )
      )
      .curve(d3.curveMonotoneX)

    svg
      .append('path')
      .datum(inputData.results.data)
      .attr('class', 'line')
      .attr('d', line)

    svg.select('.line').attr('stroke', chartColor)

    const dotSelection = svg.selectAll('.dot').data(inputData.results.data)

    dotSelection
      .enter()
      .append('g')
      .append('circle')
      .attr('class', 'dot')
      .attr('cx', function (d) {
        return xScale(d.label)
      })
      .attr('r', 5)
      .on('mouseout', function (d) {
        d3.select(this).classed('active', false)
        tooltipLayer.clearTooltip()
      })
      .on('mousemove', tooltipLayer.updateTooltip)
      .merge(dotSelection)
      .attr('fill', chartColor)
      .attr('stroke', chartColor)
      .attr('cy', function (d) {
        return yScale(
          chartSettings.valueType === 'percentage'
            ? d.value / Math.max(totalSum, 1)
            : d.value
        )
      })
      .on('mouseover', function (d) {
        d3.select(this).classed('active', true)
        tooltipLayer.showTooltip({
          title:
            d.label + ':00 – ' + (parseInt(d.label, 10) + (1 % 24)) + ':00',
          titleColor: chartColor,
          results: [
            {
              label: t('charts.tooltip.total'),
              value: d.value % 1 === 0 ? d.value : d.value.toFixed(decimals)
            },
            {
              label: t('charts.tooltip.percentageOfTotal'),
              value:
                ((100 * d.value) / Math.max(totalSum, 1)).toFixed(decimals) +
                '%'
            }
          ]
        })
      })

    const textSelection = svg
      .selectAll('.textLabel')
      .data(inputData.results.data)

    textSelection
      .enter()
      .append('text')
      .attr('class', 'textLabel')
      .style('text-anchor', 'middle')
      .attr('x', d => xScale(d.label) + 'px')
      .attr('font-size', '9px')
      .attr('fill', '#333333')
      .attr('font-weight', '700')
      .merge(textSelection)
      .attr(
        'y',
        d =>
          yScale(
            chartSettings.valueType === 'percentage'
              ? d.value / Math.max(totalSum, 1)
              : d.value
          ) -
          10 +
          'px'
      )
      .text(d =>
        chartSettings.valueType === 'percentage'
          ? ((100 * d.value) / Math.max(1, totalSum)).toFixed(decimals)
          : d.value
      )
      .style('opacity', chartSettings.isCountProLabelShown ? 1 : 0)
  }

  return (
    <Container>
      <div id={chartName} />
    </Container>
  )
}

export default LineChart
