import styled from 'styled-components'
import { DefaultChart } from '../styles'

export const Container = styled(DefaultChart)`
  .line {
    fill: none;
    stroke-width: 3;
  }

  .dot {
    transition: all 0.4s;
    &.active {
      fill-opacity: 1;
      stroke-width: 10px;
      stroke-opacity: 0.35;
    }
  }
`
