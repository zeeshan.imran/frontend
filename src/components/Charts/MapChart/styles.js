import styled from 'styled-components'
import { family } from '../../../utils/Fonts'
import { DefaultChart } from '../styles'

export const Container = styled(DefaultChart)`
  .mapLabels text {
    fill: #fff;
    font-family: ${family.primaryBold};
    font-size: 12px;
    paint-order: stroke;
    stroke: #000000;
    stroke-width: 1.5px;
    stroke-linecap: butt;
    stroke-linejoin: miter;
  }
`
