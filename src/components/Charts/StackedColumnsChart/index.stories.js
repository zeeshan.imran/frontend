import React from 'react'
import { storiesOf } from '@storybook/react'
import StackedBarChart from './index'

const inputData = {
  chart_id: '121',
  chart_type: 'stacked-bar',
  title: 'Willingness to purchase (% of Total)',
  trial: '',
  labels: ['Yes', 'No', 'Unknown'],
  chart_avg: true,
  avg: [
    {
      label: 'Activia Regular Yogurt (Strawberry)',
      value: 2.51
    },
    {
      label: 'Dannon Whole Milk Yogurt (Strawberry)',
      value: 2.45
    },
    {
      label: 'Yoplait Original Yogurt (Strawberry)',
      value: 2.96
    },
    {
      label: 'Great Value (Walmart) Original Yogurt (Strawberry)',
      value: 2.78
    },
    {
      label: 'Liberte Whole Milk Yogurt (Strawberry)',
      value: 2.16
    },
    {
      label: 'Oui French Style Yogurt (Strawberry)',
      value: 2.71
    },
    {
      label: 'Yoplait Fruitside Yogurt (Strawberry)',
      value: 4.45
    },
    {
      label: 'Trader Joes Regular Yogurt (Strawberry)',
      value: 2.04
    }
  ],
  avg_label: 'Mean',
  tableData: {
    columns: ['A', 'B'],
    rows: [
      ['x', '-'],
      ['x', '-'],
      ['x', '-'],
      ['-', 'x'],
      ['x', '-'],
      ['x', '-'],
      ['x', '-'],
      ['x', '-']
    ]
  },
  results: [
    {
      data: [
        {
          label: 'Yes',
          value: 62.38
        },
        {
          label: 'No',
          value: 37.62
        }
      ],
      name: 'Activia Regular Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 70.45
        },
        {
          label: 'No',
          value: 29.55
        }
      ],
      name: 'Dannon Whole Milk Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 74.53
        },
        {
          label: 'No',
          value: 25.47
        }
      ],
      name: 'Yoplait Original Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 48.6
        },
        {
          label: 'No',
          value: 51.4
        }
      ],
      name: 'Great Value (Walmart) Original Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 58.82
        },
        {
          label: 'No',
          value: 41.18
        }
      ],
      name: 'Liberte Whole Milk Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 70.34
        },
        {
          label: 'No',
          value: 29.66
        }
      ],
      name: 'Oui French Style Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 70.68
        },
        {
          label: 'No',
          value: 29.32
        }
      ],
      name: 'Yoplait Fruitside Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 64.93
        },
        {
          label: 'No',
          value: 35.07
        }
      ],
      name: 'Trader Joes Regular Yogurt (Strawberry)'
    }
  ]
}

storiesOf('Charts', module)
  .add('StackedBarChart', () => (
    <StackedBarChart chartIndex={1} inputData={inputData} />
  ))