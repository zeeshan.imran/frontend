import React from 'react'
import { shallow } from 'enzyme'
import Charts from './index'
import DataGuard from './DataGuard'
import PieChart from './PieChart'

describe('Charts', () => {
  let testRender
  let charts

  beforeEach(() => {
    charts = [
      {
        title: 'Gender',
        chart_type: 'pie'
      },
      {
        title: 'What state do you live in?',
        chart_type: 'map'
      },
      {
        title: 'Overall Liking - Scale (% of Total)',
        chart_type: 'stacked-column'
        // not suported - will not render
      },
      {
        title: 'Brand Description',
        chart_type: 'column'
      },
      {
        title: 'Texture Profile',
        chart_type: 'polar'
      },
      {
        title: 'Consumption Time (CST) (% of Total)',
        chart_type: 'line'
      }
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Charts', async () => {
    testRender = shallow(<Charts charts={charts} />)
    expect(testRender.find(DataGuard)).toMatchSnapshot()
  })

  // test('should render Charts when null', async () => {
  //   const chart = [
  //     {
  //       title: 'should pie chart',
  //       chart_type: '',
  //       results: [],
  //       labels: []
  //     }
  //   ]
  //   testRender = shallow(<Charts charts={chart} />)
  //   expect(testRender).toHaveLength(1)
  //   expect(testRender.find(DataGuard)).toHaveLength(1)
  //   expect(
  //     testRender
  //       .find(DataGuard)
  //       .first()
  //       .props()
  //   ).toMatchObject({
  //     component: PieChart
  //   })
  // })

  test('should render defualt Charts', async () => {
    const chart = [
      {
        title: 'Defualt chart',
        chart_type: 'unknown_chart'
      }
    ]
    testRender = shallow(<Charts charts={chart} />)
    expect(testRender.find(DataGuard)).toHaveLength(0)
  })
})
