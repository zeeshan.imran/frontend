import React from 'react'
import { mount } from 'enzyme'
import wait from '../../../utils/testUtils/waait'
import ColumnsChart from './index'

describe('ColumnsChart', () => {
  let testRender
  let chartIndex
  let inputData
  let container

  beforeEach(() => {
    container = global.document.createElement('div')
    global.document.body.appendChild(container)

    chartIndex = 1
    inputData = {
      type: 'multi',
      wide: true,
      as_percentage: false,
      visible: true,
      chart_type: 'column',
      title: 'Brand-Description',
      chart_id: '121',
      question_with_products: 1,
      results: [
        {
          data: [
            {
              label:
                'Affordable \u2013 The brand is affordable; it is not expensive.',
              value: 138
            },
            {
              label: 'label_2',
              value: 120
            }
          ],
          name: 'Activia Regular Yogurt (Strawberry)'
        },
        {
          data: [
            {
              label:
                'Affordable \u2013 The brand is affordable; it is not expensive.',
              value: 70
            },
            {
              label: 'label_2',
              value: 60
            }
          ],
          name: 'product_2'
        }
      ],
      labels: [
        'Affordable \u2013 The brand is affordable; it is not expensive.',
        'label_2'
      ]
    }
  })

  afterEach(() => {
    testRender.detach()
    testRender.unmount()
    global.document.body.removeChild(container)
    container = null
  })

  test('should render ColumnsChart', async () => {
    testRender = mount(
      <ColumnsChart chartIndex={chartIndex} inputData={inputData} />,
      { attachTo: container }
    )

    expect(testRender).toMatchSnapshot()

    expect(testRender.find('#columnchart_121').html()).toMatchSnapshot()

    await wait(500)
    expect(testRender.find('#columnchart_121').html()).toMatchSnapshot()
  })
})
