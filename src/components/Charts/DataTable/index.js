import React from 'react'
import { Table } from 'antd'
import { Container } from './styles'

const DataTable = ({ columns, dataSource, showAll = false }) => {
  const pageSize = showAll ? (dataSource && dataSource.length) || 0 : undefined
  return (
    <Container>
      <Table
        dataSource={dataSource}
        columns={columns}
        useFixedHeader={showAll}
        pagination={{
          hideOnSinglePage: true,
          pageSize
        }}
      />
    </Container>
  )
}

export default DataTable
