import styled from 'styled-components'
import { DefaultChart } from '../styles'

export const Container = styled(DefaultChart)`
  margin-top:25px;
  .ant-table-thead > tr > th, .ant-table-tbody > tr > td {
    padding: 6px 5px 5px 6px;
    font-size: 11px;
    color: #f7f7f;
    text-align: center;
    &:first-child {
      text-align: left;
    }
  }
  .ant-table-content {
    margin-bottom: 10px;
  }
  .ant-table-thead > tr > th {
    font-weight: bold;
  }
`
