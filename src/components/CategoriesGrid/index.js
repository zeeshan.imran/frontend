import React from 'react'
import { Row, Col } from 'antd'
import { Desktop } from '../Responsive'
import CategoryCard from '../../containers/CategoryCard'
import BaseCardSpacer from '../BaseCardSpacer'
import HorizontalScroll from '../HorizontalScroll'
import PairedCardContainer from '../PairedCardContainer'

const CategoriesGrid = ({ categories }) => (
  <Desktop>
    {desktop =>
      desktop ? (
        <Row gutter={24}>
          {categories.map((category, index) => (
            <Col key={index} xs={{ span: 12 }} lg={{ span: 6 }}>
              <BaseCardSpacer>
                <CategoryCard category={category} />
              </BaseCardSpacer>
            </Col>
          ))}
        </Row>
      ) : (
        <HorizontalScroll>
          <PairedCardContainer>
            {categories.map((category, index) => (
              <CategoryCard key={index} mobile category={category} />
            ))}
          </PairedCardContainer>
        </HorizontalScroll>
      )
    }
  </Desktop>
)

export default CategoriesGrid
