import React from 'react'
import { storiesOf } from '@storybook/react'
import CategoriesGrid from './'
import CategoryImage from '../../assets/svg/Baked Goods.svg'
import StoryRouter from 'storybook-react-router'

const getCategories = number => {
  let cards = []

  for (let index = 0; index < number; index++) {
    cards.push({
      name: index,
      picture: CategoryImage
    })
  }
  return cards
}

storiesOf('CategoriesGrid', module)
  .addDecorator(StoryRouter())
  .add('Story', () => <CategoriesGrid categories={getCategories(10)} />)
