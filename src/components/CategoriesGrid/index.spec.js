import React from 'react'
import { mount } from 'enzyme'
import CategoriesGrid from '.'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import BaseCardSpacer from '../BaseCardSpacer'

import { Row, Col } from 'antd'

const resizeWindow = (x, y) => {
  window.innerWidth = x
  window.innerHeight = y
  window.dispatchEvent(new Event('resize'))
}

describe('CategoriesGrid', () => {
  let testRender
  let categories

  window.localStorage = {
    getItem: () => '{}'
  }

  beforeEach(() => {
    categories = ['category 1', 'category 2']
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CategoriesGrid', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <Router history={history}>
        <CategoriesGrid categories={categories} />
      </Router>
    )
    expect(testRender.find(CategoriesGrid)).toHaveLength(1)
  })

  test('should render CategoriesGrid for desktop', async () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 1024,
      height: 800
    })
    const history = createBrowserHistory()

    testRender = mount(
      <Router history={history}>
        <CategoriesGrid categories={categories} />
      </Router>
    )
    testRender.update()
    expect(testRender.find(Row)).toHaveLength(1)
  })
})
