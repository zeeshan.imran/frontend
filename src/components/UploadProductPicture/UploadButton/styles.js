import styled from 'styled-components'
import { Upload as AntUpload, Icon as AntIcon } from 'antd'

export const Upload = styled(AntUpload)``

export const Avatar = styled.div`
  display: flex;
  background: url(${({ src }) => src}) no-repeat center;
  background-size: contain;
  border-radius: ${({ rounded }) => (rounded ? '50%' : '0%')};
  width: 100%;
  height: 100%;
`

export const Icon = styled(AntIcon)`
  font-size: 2.4rem;
`

export const IconAligner = styled.div`
  display: flex;
  flex-direction: column;
`

export const RemoveIconAligner = styled.div`
  height: 30px;
  width: 30px;
  position: absolute;
  top: 30px;
  left: calc(10rem + 15px);
  cursor: pointer;
`