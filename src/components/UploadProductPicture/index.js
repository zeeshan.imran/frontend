import React from 'react'
import UploadButton from './UploadButton'

const UploadProductPicture = ({
  loading,
  error,
  url,
  onFileUpload,
  ...rest
}) => {
  const handleBeforeUpload = async file => {
    onFileUpload({ file })
    return false
  }

  const handleRemove = () => onFileUpload({})
 
  return (
    <UploadButton
      {...rest}
      imageUrl={error ? '' : url}
      loading={loading}
      handleRemove={handleRemove}
      handleBeforeUpload={handleBeforeUpload}
      removeButton={true}
    />
  )
}

export default UploadProductPicture
