import React from 'react'
import { Row, Col } from 'antd'
import { Desktop } from '../Responsive'
import UserSurveyCard from '../../containers/UserSurveyCard'
import Text from '../../components/Text'

import { CardContainer, Container } from './styles'
import HorizontalScroll from '../HorizontalScroll'
import { useTranslation } from 'react-i18next';

const OngoingSurveysGrid = ({ surveys }) => {
  const { t } = useTranslation();
  return (
    <Desktop>
      {desktop =>
        desktop ? (
          <Row gutter={24}>
            {surveys.length === 0 ? (
              <Col xs={24}>
                <Text>{t('components.ongoingSurveysGrid.notFound')}</Text>
              </Col>
            ) : (
              surveys.map((survey, index) => (
                <Col xs={24} sm={12} lg={8}>
                  <CardContainer>
                    <UserSurveyCard key={index} data={survey} />
                  </CardContainer>
                </Col>
              ))
            )}
          </Row>
        ) : (
          <HorizontalScroll>
            <Container>
              {surveys.map((survey, index) => (
                <UserSurveyCard mobile data={survey} />
              ))}
            </Container>
          </HorizontalScroll>
        )
      }
    </Desktop>
  )
}

export default OngoingSurveysGrid
