import React from 'react'
import { Spin, Icon } from 'antd'

const Loader = ({ children, loading = true, size }) => {
  const SpinIcon = <Icon type='loading' style={{ fontSize: size || 24 }} spin />
  return (
    <Spin spinning={loading} indicator={SpinIcon}>
      {children}
    </Spin>
  )
}

export default Loader
