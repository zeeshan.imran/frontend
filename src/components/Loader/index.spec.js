import React from 'react'
import { mount } from 'enzyme'
import Loader from '.'

describe('Loader', () => {
  let testRender
    let children
    let loading
    let size


  beforeEach(() => {
    children = '<container>Loading...</container>'
    loading = false
    size = 2

  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Loader', async () => {
    testRender = mount(
      <Loader children={children} loading={loading} size={size} />
    )
    expect(testRender.find(Loader)).toHaveLength(1)
  })
})
