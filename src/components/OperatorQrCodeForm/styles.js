import styled from 'styled-components'
import Text from '../../components/Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'
import { Radio } from 'antd'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
`

export const FormHeader = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 2.5rem;
  & > :first-child {
    flex-grow: 1;
  }
  & > * {
    margin-left: 1.5rem;
  }
`

export const FormWrapper = styled.div`
  display: flex;
`

export const AddOnFieldLabel = styled.div`
  display: flex;
  align-items: center;
  width: 100%;

  & > :first-child {
    flex-grow: 1;
  }
  & > :nth-child(2) {
    text-align: right;
    line-height: 14px;
    margin-bottom: 5px;
  }
`

export const Visibility = styled.div`
  visibility: ${({ isVisible }) => (isVisible ? `visible` : `hidden`)};
`

export const NotFound = styled.div`
  padding: 2rem 1rem;
`

export const Title = styled(Text)`
  font-size: 2rem;
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.85);
`

export const CopyButton = styled.div`
  cursor: pointer;
`

export const QrCodeSection = styled.div`
  flex-grow: 0;
  text-align: center;
  min-width: 160px;
  max-width: 160px;
  margin-right: 3.5rem;

  div.qr-code {
    position: relative;
    min-width: 160px;
    min-height: 160px;
    transition: all 400ms ease-in-out;
    border: 1px solid #f9f9f9;
    cursor: pointer;
    border-radius: 0;
    box-sizing: content-box;
    background-size: cover;

    .download {
      position: absolute;
      display: block;
      bottom: 0;
      color: #fff;
      font-size: 1.2rem;
      background: rgba(0, 0, 0, 0.6);
      padding: 1.5rem 0.5rem;
      width: 100%;
      opacity: 0;
      transition: all 200ms ease-in-out;
    }

    &:hover {
      cursor: pointer;
      border: 1px solid rgba(0, 0, 0, 0.75);
      border-radius: 3px;

      .download {
        opacity: 1;
      }
    }
  }

  .outdated-note {
    display: none;
  }

  &.outdated {
    .qr-code {
      opacity: 0.3;
      pointer-events: none;
    }

    .outdated-note {
      display: block;
      font-size: 9pt;
      color: #666;
    }
  }
`

export const PlaceHolder = styled.div`
  display: flex;
  width: 140px;
  height: 140px;
  margin: 10px;
  border: 1px dashed #ddd;
  align-items: center;
  justify-content: center;
  svg {
    width: 48px;
    height: 48px;
  }
`

export const SurveyStatus = styled.span`
  margin-left: 0.5rem;
  font-weight: 500;
  color: ${({ color }) => color};
`
export const RadioButton = styled(Radio)`
.ant-radio-inner::after{
  background-color: ${colors.RADIO_BUTTON_BACKGROUND_COLOR};
}
`