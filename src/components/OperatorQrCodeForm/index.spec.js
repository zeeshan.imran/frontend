import React from 'react'
import { mount } from 'enzyme'
import OperatorQrCodeForm from '.'
import { CopyToClipboard as Copy } from 'react-copy-to-clipboard'

describe('OperatorQrCodeForm', () => {
  let testRender
  let saving
  let loadingSurveys
  let title
  let surveys
  let values
  let onSearch
  let onSubmit
  let onCancel
  let organizationUniqueName

  beforeEach(() => {
    saving = ''
    loadingSurveys = false
    title = 'Qrcode survey'
    surveys = [{}] 
    values = {
      targetType: 'survey',
      survey: ''
    }
    onSearch = jest.fn()
    onSubmit = jest.fn()
    onCancel = jest.fn()
    organizationUniqueName = ''
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorQrCodeForm', async () => {
    testRender = mount(
      <OperatorQrCodeForm
        
        saving={saving}
        loadingSurveys={loadingSurveys}
        title={title}
        surveys={surveys}
        values={values}
        onSearch={onSearch}
        onSubmit={onSubmit}
        onCancel={onCancel}
        organizationUniqueName={organizationUniqueName}
      />
    )
    expect(testRender.find(OperatorQrCodeForm)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'Select' && c.prop('name') === 'survey')
      .first()
      .prop('onChange')()
    expect(onSearch).toHaveBeenCalled()

    testRender
      .findWhere(c => c.name() === 'Select' && c.prop('name') === 'survey')
      .first()
      .prop('onBlur')()
    expect(onSearch).toHaveBeenCalled()
  })

  // test('should render OperatorQrCodeForm', async () => {
  //   testRender = mount(
  //     <OperatorQrCodeForm
  //       saving={saving}
  //       loadingSurveys={loadingSurveys}
  //       title={title}
  //       surveys={surveys}
        
  //       onSearch={onSearch}
  //       onSubmit={onSubmit}
  //       onCancel={onCancel}
  //       organizationUniqueName={'test'}
  //     />
  //   )
  //   testRender
  //     .find('[data-testid="redirect-link"]')
  //     .prop('addonAfter')()
  //   expect(onSearch).toHaveBeenCalled()
  // })
})
