import styled from 'styled-components'
import { Select as AntSelect } from 'antd'
import { family } from '../../utils/Fonts'
import  colors  from '../../utils/Colors'

export const Option = styled(AntSelect.Option)``

export const StyledSelect = styled(AntSelect)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  width: 100%;
`

export const DropdownWrapper = styled.div`
  display:flex;
  flex-direction: row;
  max-width: 100%;
`

export const DropdownStyle = styled.div`
  flex-grow: 1;
  max-width: 100%;
  .ant-select-dropdown-menu-item {
    /* padding-top: ${({ desktop }) => (desktop ? 0.5 : 1.5)}rem;
    padding-bottom: ${({ desktop }) => (desktop ? 0.5 : 1.5)}rem; */
    padding: 5px 30px !important;
    &:hover {
      background-color: ${colors.DROPDOWN_HOVER_BACKGROUND_COLOR};
      color: ${colors.DROPDOWN_HOVER_ITEM_COLOR};
    }
  }
  .ant-select-dropdown-menu-item-selected {
    &:before {
      content: '';
    background-color: transparent;
    position: absolute;
    left: 12px;
    top: 9px;
    width: 7px;
    border-bottom: 3px solid #151515;
    height: 11px;
    border-right: 3px solid #151515;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    -o-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);

    }
  }
`

export const DropdownSideInfo = styled.div`
  display: 'flex';
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: ${({ isVisible }) => (isVisible ? '50%' : '0%')};
  padding: ${({ isVisible }) => (isVisible ? '12px 16px' : '0px')};
`

export const SideInfoPicture = styled.img`
  width: 100%;
  height: auto;
`

export const SideInfoText = styled.span`
`