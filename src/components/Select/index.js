import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Desktop } from '../Responsive'
import FieldLabel from '../FieldLabel'
import { withTranslation } from 'react-i18next'
import { 
  StyledSelect,
  DropdownStyle,
  Option,
  DropdownWrapper,
  DropdownSideInfo,
  SideInfoPicture,
  SideInfoText
} from './styles'

const isEmpty = value => value === null || value === undefined || value === ''

class Select extends Component {

  renderSideInfo () {
    const { focusedOption, t } = this.props 
    const translatedText = t(`tooltips.questionType.${focusedOption}`)
    let picture = false
    if (focusedOption) {
      try {
        // picture = require(`../../assets/pictures/questions/flavorwiki/${focusedOption}.png`)
      } catch(e) {}
    }


    return (
      <DropdownSideInfo 
        isVisible={
          translatedText !== `tooltips.questionType.${focusedOption}` && 
          translatedText
      }>
        <SideInfoText>
          {
            translatedText !== `tooltips.questionType.${focusedOption}` && 
            translatedText
          }
        </SideInfoText>
        {
          picture &&
          <SideInfoPicture src={picture} />
        }
      </DropdownSideInfo>
    )
    
  }

  render () {
    const {
      label,
      required,
      size,
      options,
      value,
      getOptionValue,
      getOptionName,
      fieldDecorator,
      isDisabledOption,
      name,
      showSearch,
      tooltip,
      tooltipPlacement,
      sideDetails=false,
      setFocusedOption=false,
      focusedOption=false,
      ...rest
    } = this.props

    return (
      <Desktop>
        {desktop => (
          <FieldLabel required={required} label={label} tooltip={tooltip} tooltipPlacement={tooltipPlacement}>
            {fieldDecorator(
              <StyledSelect
                showSearch={showSearch}
                dropdownRender={menu => (
                  <DropdownWrapper>
                    <DropdownStyle 
                      desktop={desktop} 
                      sideDetails={sideDetails}
                    >
                      {menu}
                    </DropdownStyle>
                    {sideDetails && focusedOption && this.renderSideInfo()}
                  </DropdownWrapper>
                )}
                size={size || 'large'}
                value={isEmpty(value) ? undefined : value} // Needed to show placeholder when value is empty
                choiceTransitionName='123'
                name={name}
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) >= 0
                  }
                  {...rest}
                  >
                {options.map((option, index) => {
                  const value = getOptionValue(option)
                  if (option.label === null) {
                    return null
                  }
                  return (
                    <Option
                      key={index}
                      data-testid={`${name}-select-option-${index}`}
                      value={value}
                      disabled={isDisabledOption(option)}
                      onMouseOver={() => {setFocusedOption && setFocusedOption(value)}}
                    >
                      {option.label === 'divider' ? (
                        <hr />
                        ) : getOptionName(option) ? (
                          getOptionName(option)
                          ) : (
                            ''
                          )}
                    </Option>
                  )
                })}
              </StyledSelect>
            )}
          </FieldLabel>
        )}
      </Desktop>
    )
  }
}

Select.propTypes = {
  fieldDecorator: PropTypes.func,
  options: PropTypes.array,
  label: PropTypes.string,
  required: PropTypes.bool,
  isDisabledOption: PropTypes.func
}

Select.defaultProps = {
  options: [],
  isDisabledOption: value => false,
  getOptionValue: value => value,
  getOptionName: name => name,
  fieldDecorator: a => a
}

export default withTranslation()(Select)
