import React from 'react'
import { storiesOf } from '@storybook/react'
import Select from './'

const OPTIONS = ['A', 'B', 'C']

storiesOf('Select', module)
  .add('Select without label (default)', () => <Select options={OPTIONS} />)
  .add('Select with label (large)', () => (
    <Select
      label={'Example Label'}
      placeholder={'Select one option'}
      options={OPTIONS}
    />
  ))
  .add('Select with field decorator', () => (
    <Select
      fieldDecorator={cmp => console.log('component', cmp) || cmp}
      label={'Example Label'}
      placeholder={'Select one option'}
      options={OPTIONS}
    />
  ))
