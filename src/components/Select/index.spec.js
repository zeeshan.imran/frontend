import React from 'react'
import { mount } from 'enzyme'
import Select from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Select', () => {
  let testRender
  let label
  let required
  let size
  let options
  let value
  let getOptionValue
  let getOptionName
  let fieldDecorator
  let isDisabledOption
  let name
  let rest

  function countErrors () {
    return testRender.findWhere(component => {
      return (
        component.name() === 'FormItem' &&
        component.html().includes('has-error')
      )
    }).length
  }

  beforeEach(() => {
    label = 'Select country'
    required = false
    size = 'small'
    options = [{ label: 'option1', id: 1 }, { label: 'option2', id: 2 }]
    value = 'opt1'
    ;(getOptionValue = value => value),
    (getOptionName = name => name),
    (fieldDecorator = a => a)
    isDisabledOption = jest.fn()
    name = 'country'
    rest = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Select', async () => {
    testRender = mount(
      <Select
        label={label}
        required={required}
        size={size}
        options={options}
        value={value}
        getOptionValue={getOptionValue}
        getOptionName={getOptionName}
        fieldDecorator={fieldDecorator}
        isDisabledOption={isDisabledOption}
        name={name}
        rest={rest}
      />
    )
    expect(testRender.find(Select)).toHaveLength(1)

    expect(testRender.find(Select).text()).toBe('Select countryopt1')

    expect(countErrors()).toBe(0)

  })
})
