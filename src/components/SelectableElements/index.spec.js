import React from 'react'
import { mount } from 'enzyme'
import SelectableElements from '.'

import PayPalCard from '../PaymentDetailsForm/PayPalCard'
import PayPalForm from '../PaymentDetailsForm/PayPalForm'

import styled from 'styled-components'
const StyledContainer = styled.div``

describe('SelectableElements', () => {
  let testRender
  let mockRender

  beforeAll(() => {
    mockRender = jest.fn(
      ({ handleSelect, clearSelection, isSelected, selectedElement }) => (
        <StyledContainer>
          {isSelected({
            type: 'paypal',
            tabComponent: PayPalCard,
            content: PayPalForm
          })}
          {selectedElement && clearSelection()}
          {/* {handleSelect()}f */}
        </StyledContainer>
      )
    )
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SelectableElements', () => {
    testRender = mount(
      <SelectableElements
        initiallySelected={{
          type: 'paypal',
          tabComponent: PayPalCard,
          content: PayPalForm
        }}
        render={mockRender}
      />
    )
    expect(mockRender).toHaveBeenCalled()
    expect(testRender.find(StyledContainer)).toHaveLength(1)
  })
})
