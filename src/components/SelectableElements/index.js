import { Component } from 'react'
import PropTypes from 'prop-types'

class SelectableElements extends Component {
  state = {
    selectedElement: undefined
  }

  componentDidMount () {
    const { initiallySelected } = this.props

    if (initiallySelected) {
      this.handleSelect(initiallySelected)
    }
  }

  handleSelect = elem => {
    this.setState({ selectedElement: elem })
  }

  clearSelection = () => {
    this.setState({ selectedElement: null })
  }

  isSelected = elem =>
    this.props.elementComparator(this.state.selectedElement, elem)

  render () {
    return this.props.render({
      handleSelect: this.handleSelect,
      clearSelection: this.clearSelection,
      isSelected: this.isSelected,
      selectedElement: this.state.selectedElement
    })
  }
}

SelectableElements.defaultProps = {
  elementComparator: (selected, other) => selected === other
}

SelectableElements.propTypes = {
  elementComparator: PropTypes.func,
  initiallySelected: PropTypes.any
}

export default SelectableElements
