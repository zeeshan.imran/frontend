import styled from 'styled-components'
import Text from '../../../Text'
import colors from '../../../../utils/Colors'
import { family } from '../../../../utils/Fonts'
import { COMPONENTS_DEFAULT_MARGIN } from '../../../../utils/Metrics'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100vw;
  height: 100%;
  position: fixed;
  top: 0;
  right: ${({ visible }) => (visible ? 0 : `-100vw`)};
  background-color: ${colors.BLACK};
  transition: right 0.3s ease-in-out;
  padding-top: 30%;
`

export const Title = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 2.4rem;
  color: ${colors.SLATE_GREY};
  margin-bottom: ${COMPONENTS_DEFAULT_MARGIN}rem;
`

export const Entry = styled.div`
  font-family: ${family.primaryRegular};
  font-size: 2.4rem;
  color: ${colors.WHITE};
  cursor: pointer;
  margin-bottom: 3rem;
  user-select: none;
`
