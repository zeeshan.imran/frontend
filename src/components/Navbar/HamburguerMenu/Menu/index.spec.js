import React from 'react'
import { mount } from 'enzyme'
import Menu from '.'
import { Entry } from './styles'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../../utils/createApolloMockClient'

describe('HamburguerMenu Menu', () => {
  let testRender
  let onClickEntry
  let entriesData
  let getEntryName
  let title
  let client
  beforeEach(() => {
    title = 'Test Menu'
    onClickEntry = jest.fn()
    entriesData = [{ url: '/test', name: 'Test' }]
    getEntryName = jest.fn()

    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Menu', () => {
    testRender = mount(
      <Menu
        title={title}
        visible
        entriesData={entriesData}
        getEntryName={getEntryName}
        onClickEntry={onClickEntry}
      />
    )
    expect(testRender).toMatchSnapshot()
  })

  test('should call on click entry', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Menu
          title={title}
          visible
          entriesData={entriesData}
          getEntryName={getEntryName}
          onClickEntry={onClickEntry}
        />
      </ApolloProvider>
    )
    testRender.find(Entry).simulate('click')
    expect(onClickEntry).toHaveBeenCalled()
  })
})
