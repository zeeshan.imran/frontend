import React from 'react'
import { Container, Entry, Title } from './styles'

const Menu = ({
  title = 'Menu',
  visible,
  entriesData,
  getEntryName,
  onClickEntry
}) => (
  <Container visible={visible}>
    <Title>{title}</Title>
    {entriesData.map((entry, index) => (
      <Entry key={index} onClick={() => onClickEntry(entry)}>
        {getEntryName(entry)}
      </Entry>
    ))}
  </Container>
)

export default Menu
