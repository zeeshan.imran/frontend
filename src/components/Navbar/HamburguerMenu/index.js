import React, { Component } from 'react'
import HamburgerIcon from 'react-hamburger-menu'
import Menu from './Menu'
import colors from '../../../utils/Colors'
import { IconContainer } from './styles'

class HamburguerMenu extends Component {
  state = {
    visible: false
  }

  render () {
    const { displayName, getEntryName, onClickEntry, entriesData } = this.props

    const { visible } = this.state

    return (
      <React.Fragment>
        <IconContainer visible={visible}>
          <HamburgerIcon
            isOpen={visible}
            menuClicked={() => this.setState({ visible: !visible })}
            width={20}
            height={15}
            strokeWidth={2}
            color={visible ? colors.WHITE : colors.BLACK}
            animationDuration={0.5}
          />
        </IconContainer>
        <Menu
          title={displayName}
          visible={visible}
          entriesData={entriesData}
          getEntryName={getEntryName}
          onClickEntry={onClickEntry}
        />
      </React.Fragment>
    )
  }
}
export default HamburguerMenu
