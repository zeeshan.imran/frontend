import React from 'react'
import { storiesOf } from '@storybook/react'
import Navbar from './'

storiesOf('Navbar', module).add('Navbar', () => (
  <Navbar height={7} maxHeight={7} />
))
