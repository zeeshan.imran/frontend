import React from 'react'
import { mount } from 'enzyme'
import Navbar from '.'
import HamburguerMenu from './HamburguerMenu'
import { Logo } from './styles'
import { act } from 'react-dom/test-utils'
import history from '../../history'

jest.mock('../../history')

describe('Navbar', () => {
    let testRender
    let onClickEntry
    let mergeNavbarToContent
    let entriesData
    let user
    let getEntryName
    

    beforeEach(() => {
        onClickEntry = jest.fn();
        mergeNavbarToContent = true;
        entriesData = [
            {url : "/test", name: "Test"},
            {url : "/test-1", name: "Test 1"},
            {url : "/test-2", name: "Test 2"}
        ];
        getEntryName = jest.fn();
        user = {
            id: "1",
            fullName:"Test",
            emailAddress:"test@gmail.com",
        };
        
        history.push.mockImplementation(jest.fn())
    })

    afterEach(() => {
        testRender.unmount()
    })


    test('should render Navbar', () => {
        testRender = mount(
            <Navbar
                entriesData={entriesData}
                getEntryName={getEntryName}
                onClickEntry={onClickEntry}
                mergeNavbarToContent={mergeNavbarToContent}
                user={user}
            />
        )

        expect(testRender.find(HamburguerMenu)).toHaveLength(1)
        
        act(() => {
            testRender.find(Logo).simulate('click')
        })

        expect(history.push).toHaveBeenCalledWith('/')
    })
})
