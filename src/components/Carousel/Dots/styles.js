import styled from 'styled-components'
import colors from '../../../utils/Colors'

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 3rem;
`

export const Dot = styled.div`
  width: 0.8rem;
  height: 0.8rem;
  opacity: ${({ active }) => (active ? 1 : 0.4)};
  transition: opacity 0.3s ease;
  border-radius: 50%;
  background-color: ${colors.WHITE};
  margin-right: ${({ last }) => (last ? 0 : `1rem`)};
  cursor: pointer;
`
