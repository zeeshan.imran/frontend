import React from 'react'
import { mount } from 'enzyme'
import Dots from '.'
import { Dot } from './styles'

describe('Dots', () => {
  let testRender
  let currentSlide
  let goToSlide
  let slideCount

  beforeEach(() => {
    currentSlide = 1
    goToSlide = jest.fn()
    slideCount = 5
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Dots', async () => {
    testRender = mount(
      <Dots
        currentSlide={currentSlide}
        goToSlide={goToSlide}
        slideCount={slideCount}
      />
    )
    expect(testRender.find(Dots)).toHaveLength(1)
  })

  test('should render when click on dot', async () => {
    testRender = mount(
      <Dots
        currentSlide={currentSlide}
        goToSlide={goToSlide}
        slideCount={slideCount}
      />
    )
    testRender
      .find(Dot)
      .at(1)
      .simulate('click')

    expect(goToSlide).toHaveBeenCalled()
  })
})
