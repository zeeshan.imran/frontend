import React from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import Carousel from './'
import colors from '../../utils/Colors'
import firstCarouselImage from '../../assets/pictures/onboarding/1.png'
import thirdCarouselImage from '../../assets/pictures/onboarding/3.png'

const Container = styled.div`
  height: 100vh;
  width: 60rem;
`

const Image = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: cover;
  height: 100vh;
  width: 100%;
`

const TextExample = styled.div`
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
  text-align: center;
  background-color: transparent;
  color: ${colors.WHITE};
`

storiesOf('Carousel', module).add('Carousel Example', () => (
  <Container>
    <Carousel
      slides={[firstCarouselImage, thirdCarouselImage, 'withText']}
      renderSlideTopContent={slide =>
        slide === 'withText' && <TextExample>Carousel With Text</TextExample>
      }
      renderSlideBottomContent={slide =>
        slide === 'withText' ? (
          <Image src={firstCarouselImage} />
        ) : (
          <Image src={slide} />
        )
      }
    />
  </Container>
))
