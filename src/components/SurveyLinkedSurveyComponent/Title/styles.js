import styled from 'styled-components'
import { Col } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const CustomCol = styled(Col)`
  display: flex;
  justify-content: center;
  margin-bottom: 5rem;
`

export const Prompt = styled(Text)`
  font-family: ${family.primaryLight};
  font-size: 4.2rem;
  color: ${colors.BLACK};
  line-height: 1.24;
  letter-spacing: -0.06rem;
  text-align: center;
`
