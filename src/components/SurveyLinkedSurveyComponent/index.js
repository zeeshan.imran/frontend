import React from 'react'
import { Row } from 'antd'
import Title from './Title'
import Picture from './Picture'
import ThankYouText from './ThankYouText'
import { withTranslation } from 'react-i18next'
import SurveySharing from '../../containers/SurveySharing'
import useResponsive from '../../utils/useResponsive'
import { SocialButtonsContainer, TwitterButton } from './styles'

const SurveyLinkedSurveyComponent = ({
  sharingButtons,
  text,
  linkedSurveys
}) => {
  const { mobile } = useResponsive()

  return (
    <React.Fragment>
      <Row>
        <Title />
        <Picture />
        <ThankYouText text={text} />
      </Row>
      <SocialButtonsContainer>
        <SurveySharing isDesktop={!mobile} centerAlign />
        {sharingButtons &&
          linkedSurveys.map((singleSurvey, index) => {
            return (
              <TwitterButton
                key={index}
                href={`${window.location.origin}/survey/${
                  singleSurvey.uniqueName
                }`}
                target='_blank'
              >
                {singleSurvey.name}
              </TwitterButton>
            )
          })}
      </SocialButtonsContainer>
    </React.Fragment>
  )
}

export default withTranslation()(SurveyLinkedSurveyComponent)
