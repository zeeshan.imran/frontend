import React from 'react'
import { storiesOf } from '@storybook/react'
import SurveyLinkedSurveyComponent from './'

storiesOf('SurveyLinkedSurveyComponent', module).add('Story', () => (
  <SurveyLinkedSurveyComponent text='Example of a Thank You Text' linkedSurveys={[]} />
))
