import styled from 'styled-components'
import { Col } from 'antd'

export const CustomCol = styled(Col)`
  display: flex;
  justify-content: center;
  margin-bottom: 4rem;
`
