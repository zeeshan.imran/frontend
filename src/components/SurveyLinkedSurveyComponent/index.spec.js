import React from 'react'
import { mount } from 'enzyme'
import SurveyLinkedSurveyComponent from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import defaults from '../../defaults'
import { TwitterButton } from './styles'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate HoC receive the t function as a prop
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

const SURVEY_ID = 'survey-1'

describe('SurveyLinkedSurveyComponent', () => {
  let testRender
  let mockClient

  beforeEach(() => {
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: SURVEY_ID,
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: []
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Linked Survey Button', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <SurveyLinkedSurveyComponent
          sharingButtons
          text={'Thank you for choose this survey'}
          shareLink={'https://twitter.com/login?lang=en'}
          linkedSurveys={[
            {
              id: 'linked-survey-1',
              name: 'Linked Survey 1',
              uniqueName: 'linked-survey-1'
            }
          ]}
        />
      </ApolloProvider>
    )
    expect(testRender.find(TwitterButton)).toHaveLength(1)
  })
})
