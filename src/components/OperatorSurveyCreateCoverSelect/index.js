import React, { useState, useCallback, useMemo, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { move, pluck, uniq } from 'ramda'
import {
  Container,
  NotSelected,
  TopContainer,
  TopContainerAction,
  LinkButton
} from './styles'
import FieldLabel from '../../components/FieldLabel'
import DragAndDropSortAbleItem from './ProductSorting'
import ProductDisplayType from './ProductDisplayType'

const getUniqueBrands = products => {
  const getBrands = pluck('brand')
  return uniq(getBrands(products)).length
}

const hasPicture = product => !!product.photo

const getProductId = product => product.clientGeneratedId || product.id

const AUTO_SELECT = 'auto'

const getSelectedProductId = (products, selectedProduct) => {
  if (selectedProduct) {
    return getProductId(selectedProduct)
  }

  // not selected, but has no pictures => auto (to auto select when adding picture)
  if (!products.some(hasPicture)) {
    return AUTO_SELECT
  }

  // not selected, but has picture => really not selected
  return null
}

const OperatorSurveyCreateCoverSelectComponent = ({
  selectedProduct: selectedProductProp,
  onSelectProductChange = () => {},
  products: allProducts,
  productSorting = () => {},
  productDisplayType,
  setProductDisplayType = () => {},
  stricted
}) => {
  const { t } = useTranslation()
  const [allproductsList, setAllProducts] = useState(allProducts)
  const [selectedProductId, setSelectedProductId] = useState(
    getSelectedProductId(allproductsList, selectedProductProp)
  )

  useEffect(() => {
    setAllProducts(
      allProducts.sort((a, b) => a.sortingOrderId - b.sortingOrderId)
    )
  }, [allProducts])

  const products = useMemo(() => allproductsList.filter(hasPicture), [
    allproductsList
  ])

  const handleSelectedProductIdChange = useCallback(
    productId => {
      setSelectedProductId(productId)
      onSelectProductChange(products.find(p => getProductId(p) === productId))
    },
    [onSelectProductChange, products]
  )

  const selectedProduct = useMemo(
    () => products.find(p => getProductId(p) === selectedProductId),
    [products, selectedProductId]
  )

  useEffect(() => {
    const isProductRemoved = selectedProductId && !selectedProduct

    const firstProduct = products.find(hasPicture)
    if (isProductRemoved) {
      handleSelectedProductIdChange(
        firstProduct ? getProductId(firstProduct) : AUTO_SELECT
      )
    }

    if (selectedProductId === AUTO_SELECT && firstProduct) {
      handleSelectedProductIdChange(getProductId(firstProduct))
    }
  }, [
    selectedProductId,
    selectedProduct,
    products,
    handleSelectedProductIdChange
  ])
  const allProductsWithoutImages = allproductsList.filter(
    item => !products.includes(item)
  )
  if (
    (!products && !allProductsWithoutImages) ||
    (products.length === 0 && allProductsWithoutImages.length === 0)
  ) {
    return null
  }
  const onSortEnd = ({ oldIndex, newIndex }) => {
    const sortedProduts = move(oldIndex, newIndex, allproductsList)
    const sortingOrderIdSet =
      sortedProduts.length &&
      sortedProduts.map((product, index) => ({
        ...product,
        sortingOrderId: index + 1
      }))
    setAllProducts(
      sortingOrderIdSet.sort((a, b) => a.sortingOrderId - b.sortingOrderId)
    )
    productSorting(
      sortingOrderIdSet.sort((a, b) => a.sortingOrderId - b.sortingOrderId)
    )
  }

  return (
    <Container>
      {allproductsList.length > 1 && (
        <TopContainer>
          <FieldLabel
            tooltip={t('tooltips.productDisplayDragAndDrop')}
            label={t('components.productDisplayOptions.dragAndDropSort')}
          />
        </TopContainer>
      )}
      {products.length > 0 && (
        <TopContainer>
          <React.Fragment>
            <FieldLabel>{t('components.coverProduct.prompt')}</FieldLabel>
            <TopContainerAction>
              {!!selectedProduct && (
                <LinkButton
                  onClick={() => {
                    handleSelectedProductIdChange(null)
                  }}
                >
                  {t('components.coverProduct.remove')}
                </LinkButton>
              )}
              {!selectedProduct && (
                <NotSelected>{t('components.coverProduct.empty')}</NotSelected>
              )}
            </TopContainerAction>
          </React.Fragment>
        </TopContainer>
      )}
      {allproductsList.length && (
        <DragAndDropSortAbleItem
          shouldUseDragHandle
          useDragHandle
          axis='xy'
          distance={1}
          onSortEnd={onSortEnd}
          products={allproductsList}
          selectedProduct={selectedProduct}
          getProductId={getProductId}
          handleSelectedProductIdChange={handleSelectedProductIdChange}
          stricted={stricted || getUniqueBrands(allproductsList) > 1}
        />
      )}   
      {allProducts.length > 1 && (
        <ProductDisplayType
          t={t}
          products={allproductsList}
          productDisplayType={productDisplayType}
          setProductDisplayType={setProductDisplayType}
          stricted={stricted}
          disableSelection={getUniqueBrands(allproductsList) > 1}
        />
      )}
    </Container>
  )
}

export default OperatorSurveyCreateCoverSelectComponent
