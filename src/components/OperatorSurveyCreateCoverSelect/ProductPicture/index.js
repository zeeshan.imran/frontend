import React, { useCallback } from 'react'
import { ProductPictureContainer } from '../styles'

const ProductPicture = ({ product, isSelected, onClick }) => {
  const handleClick = useCallback(() => {
    onClick(product)
  }, [onClick, product])

  return (
    <ProductPictureContainer isSelected={isSelected} onClick={handleClick}>
      <img alt={product.name} src={product.photo} />
    </ProductPictureContainer>
  )
}

export default ProductPicture
