import styled, { css } from 'styled-components'
import colors from '../../utils/Colors'
import {family} from '../../utils/Fonts'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
`
export const ProductContainer = styled.div``

export const TopContainer = styled.div`
  display: flex;
  :first-child {
    flex-grow: 1;
  }
`

export const TopContainerAction = styled.div`
  text-align: right;
  flex-basis: 250px;
  flex-grow: 0;
`

export const ProductPicturesContainer = styled.div`
  display: flex;
  padding: 0.5rem 0;
  overflow-x: auto;
`

export const ProductPictureContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  min-width: 100px;
  min-height: 100px;
  padding: 5px;
  margin-right: 10px;
  border: 1px dashed #ddd;
  background: #f9f9f9;
  img {
    max-width: 80px;
    max-height: 80px;
    opacity: 0.5;
  }
  ${({ isSelected }) =>
    isSelected &&
    css`
      border: 1px solid #999;
      img {
        opacity: 1;
      }
    `}
`

export const LinkButton = styled.button`
  font-size: 12px;
  display: inline;
  border: none;
  color: ${colors.LINK_BUTTON_COLOR};
  outline: none;
  cursor: pointer;
  padding: 0;
  :hover {
    text-decoration: underline;
  }
`

export const NotSelected = styled.span`
  color: #d0d0d0;
  font-size: 12px;
  font-family: ${family.primaryRegular};
  font-variant: tabular-nums;
`

export const ProductWithOutPictureContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  min-width: 100px;
  min-height: 100px;
  padding: 5px;
  margin-right: 10px;
  border: 1px solid #999;
  background: #f9f9f9;
  span {
    display: block;
    word-break: break-all;
  }
`