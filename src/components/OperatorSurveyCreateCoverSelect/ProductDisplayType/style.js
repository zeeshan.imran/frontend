import styled from 'styled-components'
import { Radio, Row } from 'antd'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const DisplayTypeContainer = styled(Row)`
  margin-top: 20px;
  .ant-radio-group {
    display: block;
  }
  .ant-radio-wrapper {
    margin-bottom: 10px;
    display: block;
  }
`

export const RadioButton = styled(Radio)`
  .ant-radio-inner::after {
    background-color: ${colors.RADIO_BUTTON_BACKGROUND_COLOR};
  }
`

export const DisplayNoteContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
  margin-bottom: 10px;
`

export const DisplayNote = styled.span`
  color: ${colors.RED};
  font-family: ${family.primaryRegular};
`
