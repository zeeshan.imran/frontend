import React, { useState, useEffect } from 'react'
import { Radio } from 'antd'
import FieldLabel from '../../../components/FieldLabel'
import {
  RadioButton,
  DisplayTypeContainer,
  DisplayNoteContainer,
  DisplayNote
} from './style'

const ProductDisplayType = ({
  t,
  productDisplayType,
  setProductDisplayType,
  products,
  stricted,
  disableSelection
}) => {
  const [displayOption, setDisplayOption] = useState(
    productDisplayType ? productDisplayType : 'none'
  )
  const [disableButton, setDisableButton] = useState(false)
  useEffect(() => {
    if (disableSelection) {
      setDisplayOption('none')
      setDisableButton(true)
    } else {
      setDisableButton(false)
    }
  }, [disableSelection])

  const displayOptionValue = type => {
    setDisplayOption(type.target.value)
    setProductDisplayType(type.target.value, products)
  }

  return (
    <DisplayTypeContainer>
      <DisplayNoteContainer>
        <DisplayNote>{t('components.productDisplayOptions.note')}</DisplayNote>
        <FieldLabel
          tooltip={t('tooltips.productDisplayOptions')}
          label={t('components.productDisplayOptions.productDisplayType')}
        />
      </DisplayNoteContainer>

      <Radio.Group
        size='large'
        buttonStyle='solid'
        onChange={displayOptionValue}
        value={displayOption}
        defaultChecked={displayOption}
        disabled={stricted}
      >
        <RadioButton disabled={disableButton} value='reverse'>
          {t('components.productDisplayOptions.reverse')}
        </RadioButton>
        <RadioButton disabled={disableButton} value='permutation'>
          {t('components.productDisplayOptions.permutation')}
        </RadioButton>
        <RadioButton disabled={disableButton} value='forced'>
          {t('components.productDisplayOptions.forced')}
        </RadioButton>
        <RadioButton value='none'>
          {t('components.productDisplayOptions.none')}
        </RadioButton>
      </Radio.Group>
    </DisplayTypeContainer>
  )
}

export default ProductDisplayType
