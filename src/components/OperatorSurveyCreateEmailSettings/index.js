import React, { useRef, useEffect } from 'react'
import { Formik } from 'formik'
import { useTranslation } from 'react-i18next'
import camelCase from 'lodash.camelcase'
import { path } from 'ramda'
import { Container, CheckboxContainer } from './styles'
import { Row, Col, Form, Tabs, Icon, Tooltip } from 'antd'
import * as Yup from 'yup'
import FieldLabel from '../FieldLabel'
import InputRichText from '../InputRichText'
import InputArea from '../InputArea'
import Input from '../Input'
import colors from "../../utils/Colors"
import { StyledCheckbox } from '../StyledCheckBox'

const SURVEY_WAITING = 'survey-waiting'
const SURVEY_COMPLETED = 'survey-completed'
const SURVEY_REJECTED = 'survey-rejected'
const emailTypes = [SURVEY_WAITING, SURVEY_COMPLETED, SURVEY_REJECTED]

const ownerCss = `
.email {
  max-width: 720px;
  width: 100% !important;
  border: 1px #edfbdf solid;
  background: #fff;
  border-radius: 3px;
  margin: 10px auto 0 auto;
}

.emailContent {
  font-size: 20px;
  font-weight: 300;
  color: #000;
  padding: 15px 0;
  width: 720px;
  font-family: Atlas Grotesk, Open Sans, HelveticaNeue-Light, Helvetica Neue Light, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;
  line-height: 1.45em;
}

.emailContent td {
  vertical-align: top;
}

.header {
  padding: 20px 25px 10px;
  border-bottom: 1px solid #f0f0f0
}

.footer {
  min-height: 50px;
  margin-top: 22px;
  max-width: 720px;
  font-family: Atlas Grotesk, Open Sans, HelveticaNeue-Light, Helvetica Neue Light, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;
  margin: 0 auto;
  background-color: #f7f8f8;
  text-align: left;
}

.footer td {
  font-size: 11px;
  color: #adb1b4;
  border-collapse: collapse;
  padding: 0 22px;
  -webkit-text-size-adjust: none;
  font-family: Atlas Grotesk, Open Sans, HelveticaNeue-Light, Helvetica Neue Light, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;
  line-height: 1.6em;
}

.footer a {
  text-decoration: none;
}

.heading1 {
  font-size: 28px;
  line-height: 32px;
  font-weight: 600;
  color: #4e4e56;
  text-align: center;
  max-width: 400px;
  margin: 16px auto;
}

.heading2 {
  font-size: 22px;
  line-height: 32px;
  font-weight: 600;
  color: #4e4e56;
  text-align: center;
  max-width: 600px;
  margin: 0 auto;
}

.text {
  font-size: 15px;
  line-height: 1.4em;
  font-weight: 300;
  color: #4e4e56;
  text-align: center;
  max-width: 600px;
  margin: 12px auto 0 auto;
}

.text-header {
  font-size: 24px;
  line-height: 1.4em;
  font-weight: 300;
  color: #4e4e56;
  text-align: center;
  max-width: 600px;
  margin: 12px auto 0 auto;
}

.text-left {
  font-size: 14px;
  line-height: 1.4em;
  font-weight: 300;
  color: #4e4e56;
  text-align: left;
  max-width: 600px;
  margin: 12px auto 0 auto;
}


.happyTasting {
  font-size: 20px;
  line-height: 30px;
  font-weight: 300;
  color: #9e9e9e;
  text-align: center;
}

.team {
  font-size: 14px;
  line-height: 27px;
  font-weight: 400;
  color: #4e4e56;
  text-align: center;
}

.btn {
  width: max-content;
  padding: 8px 16px;
  border-radius: 4px;
  font-size: 14px;
  line-height: 24px;
  font-weight: 500;
  text-decoration: none;
  cursor: pointer;
  border: none;
}

.btn-primary {
  color: #fff !important;
  background-color: ${colors.SURVEY_EMAIL_BUTTON_COLOR} !important;
}

.btn-primary:hover {
  background-color: ${colors.SURVEY_EMAIL_BUTTON_HOVER_COLOR} !important
}

.btn-group {
  text-align: center;
  margin: 24px auto 24px auto;
}

.align-left {
  text-align: left !important;
}

.margin-top-30 {
  margin-top: 30px !important;
}

.margin-top-50 {
  margin-top: 30px !important;
}

.align-center {
  text-align: center !important;
}

.full-width {
  width: 100%;
}

.btn-share {
  display: block;
  width: 200px !important;
  margin: 0 auto 10px auto;
  color: #fff !important;
}

.facebook {
  background-color: #3b5a97 !important;
}

.twitter {
  background-color: #38a1f3 !important;
}

.linkedin {
  background-color: #0277b5 !important;
}
`

const emailValidationSchema = Yup.object().shape({
  subject: Yup.string().required('validation.emailTemplate.subject.required'),
  html: Yup.string().required('validation.emailTemplate.html.required'),
  text: Yup.string().required('validation.emailTemplate.text.required')
})

export const emailsValidationSchema = Yup.lazy(({ enabledEmailTypes }) => {
  const emailsShape = {
    surveyWaiting: enabledEmailTypes.includes(SURVEY_WAITING)
      ? emailValidationSchema.clone()
      : Yup.object().nullable(),
    surveyCompleted: enabledEmailTypes.includes(SURVEY_COMPLETED)
      ? emailValidationSchema.clone()
      : Yup.object().nullable(),
    surveyRejected: enabledEmailTypes.includes(SURVEY_REJECTED)
      ? emailValidationSchema.clone()
      : Yup.object().nullable()
  }
  return Yup.object().shape({
    emails: Yup.object().shape(emailsShape)
  })
})

const OperatorSurveyCreateEmailSettings = ({
  emailSettings,
  onEmailTypesChange,
  onEmailTemplateChange
}) => {
  const { t } = useTranslation()
  const ref = useRef()

  useEffect(() => {
    ref.current && ref.current.validateForm()
  }, [])

  return (
    <Formik
      ref={ref}
      enableReinitialize
      initialValues={emailSettings}
      validationSchema={emailsValidationSchema}
    >
      {({ errors, values, setFieldValue }) => (
        <Container>
          <Row gutter={24}>
            <Col xs={{ span: 24 }}>
              <Form.Item>
                <FieldLabel
                  label={t('surveyEmailSettings.enabledEmailTypesCaption')}
                >
                  {emailTypes.map(emailType => {
                    const handleEnableEmailTypeChange = e => {
                      const lastEnabledEmailTypes = values.enabledEmailTypes

                      let enabledEmailTypes
                      if (!e.target.checked) {
                        enabledEmailTypes = lastEnabledEmailTypes.filter(
                          type => emailType !== type
                        )
                      } else {
                        enabledEmailTypes = [
                          ...lastEnabledEmailTypes,
                          emailType
                        ]
                      }

                      onEmailTypesChange(enabledEmailTypes)
                      setFieldValue('enabledEmailTypes', enabledEmailTypes)
                    }

                    const isChecked = values.enabledEmailTypes.includes(
                      emailType
                    )

                    const templateProp = camelCase(emailType)
                    const getSubject = path(['emails', templateProp, 'subject'])
                    const getHtml = path(['emails', templateProp, 'html'])
                    const getText = path(['emails', templateProp, 'text'])

                    return (
                      <CheckboxContainer key={emailType} open={isChecked}>
                        <StyledCheckbox
                          checked={isChecked}
                          onChange={handleEnableEmailTypeChange}
                        >
                          {t(
                            `surveyEmailSettings.enabledEmailTypes.${emailType}`
                          )}
                        </StyledCheckbox>
                        {isChecked && (
                          <Tabs defaultActiveKey='2'>
                            <Tabs.TabPane
                              tab={
                                <span>
                                  Subject{' '}
                                  {getSubject(errors) && (
                                    <Tooltip title={t(getSubject(errors))}>
                                      <Icon
                                        type='warning'
                                        theme='twoTone'
                                        twoToneColor='#eb2f96'
                                      />
                                    </Tooltip>
                                  )}
                                </span>
                              }
                              key='1'
                            >
                              <Input
                                value={getSubject(values)}
                                onChange={e => {
                                  const value = e.target.value
                                  onEmailTemplateChange(templateProp, {
                                    subject: value
                                  })
                                  setFieldValue(
                                    `emails.${templateProp}.subject`,
                                    value
                                  )
                                }}
                              />
                            </Tabs.TabPane>
                            <Tabs.TabPane
                              tab={
                                <span>
                                  Email content (Formatted text){' '}
                                  {getHtml(errors) && (
                                    <Tooltip title={t(getHtml(errors))}>
                                      <Icon
                                        type='warning'
                                        theme='twoTone'
                                        twoToneColor='#eb2f96'
                                      />
                                    </Tooltip>
                                  )}
                                </span>
                              }
                              key='2'
                            >
                              <InputRichText
                                ownerCss={ownerCss}
                                noUpload
                                value={getHtml(values)}
                                onChange={value => {
                                  onEmailTemplateChange(templateProp, {
                                    html: value
                                  })
                                  setFieldValue(
                                    `emails.${templateProp}.html`,
                                    value
                                  )
                                }}
                              />
                            </Tabs.TabPane>
                            <Tabs.TabPane
                              tab={
                                <span>
                                  Email content (Plain text){' '}
                                  {getText(errors) && (
                                    <Tooltip title={t(getText(errors))}>
                                      <Icon
                                        type='warning'
                                        theme='twoTone'
                                        twoToneColor='#eb2f96'
                                      />
                                    </Tooltip>
                                  )}
                                </span>
                              }
                              key='3'
                            >
                              <InputArea
                                autoSize
                                defaultValue={getText(values)}
                                onChange={e => {
                                  const value = e.target.value
                                  onEmailTemplateChange(templateProp, {
                                    text: e.target.value
                                  })
                                  setFieldValue(
                                    `emails.${templateProp}.text`,
                                    value
                                  )
                                }}
                              />
                            </Tabs.TabPane>
                          </Tabs>
                        )}
                      </CheckboxContainer>
                    )
                  })}
                </FieldLabel>
              </Form.Item>
            </Col>
          </Row>
        </Container>
      )}
    </Formik>
  )
}

export default OperatorSurveyCreateEmailSettings
