import styled, { css } from 'styled-components'
import colors from '../../utils/Colors'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
`
export const CheckboxContainer = styled.div`
  line-height: 32px;
  ${({ open }) =>
    open
      ? css`
          background: #f0f0f0;
          padding: 0.5rem 1rem 1rem 1rem;
          margin-bottom: 1.5rem;
        `
      : css`
          padding: 0 1rem;
        `}
`

export const TooltipContent = styled.div`
  ul {
    margin: 0.5rem 0 0 0;
    padding-left: 2rem;
    min-width: 180px;
    li {
      padding-bottom: 0.5rem;
    }
  }
`
