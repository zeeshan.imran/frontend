import React from 'react'
import { shallow } from 'enzyme'
import CreationButtonStatsContainer from '.'

describe('CreationButtonStatsContainer', () => {
  let testRender
  let onClick
  let text

  beforeEach(() => {
    onClick = jest.fn()
    text = 'Create survey'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CreationButtonStatsContainer', async () => {
    testRender = shallow(
      <CreationButtonStatsContainer onClick={onClick} text={text} rest={{}} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
