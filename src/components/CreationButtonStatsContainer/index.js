import React from 'react'
import Button from '../Button'
import { Container, InnerContainer } from './styles'

const CreationButtonStatsContainer = ({
  hasProductSelection,
  productButtonClick,
  productButtonText,
  filterButtonClick,
  filterButtonText,
  ...rest
}) => (
  <Container>
    {hasProductSelection && (
      <InnerContainer>
        <Button {...rest} onClick={productButtonClick}>
          {productButtonText}
        </Button>
      </InnerContainer>
    )}

    <InnerContainer>
      <Button {...rest} onClick={filterButtonClick}>
        {filterButtonText}
      </Button>
    </InnerContainer>
  </Container>
)

export default CreationButtonStatsContainer
