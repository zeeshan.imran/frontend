import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2rem 0;
  border: dashed 1px #8f8f8f;
  justify-content: center;
  display: flex;
`
export const InnerContainer = styled.div`
  margin: 1rem;
`