import React from 'react'
import { mount } from 'enzyme'
import PairedQuestionsInfoModal from '.'
import { Button } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('PairedQuestionsInfoModal', () => {
  let testRender
  let visible
  let onClick

  beforeEach(() => {
    visible = true
    onClick = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PairedQuestionsInfoModal', async () => {
    testRender = mount(
      <PairedQuestionsInfoModal visible={visible} onClick={onClick} />
    )
    expect(testRender.find(PairedQuestionsInfoModal)).toHaveLength(1)
  })

  test('should render IconButton on click', async () => {
    testRender = mount(
      <PairedQuestionsInfoModal visible={visible} onClick={onClick} />
    )

    testRender
      .find(Button)
      .props()
      .onClick()

    expect(onClick).toHaveBeenCalled()
  })
})
