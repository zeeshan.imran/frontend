import styled from 'styled-components'
import Text from '../Text'
import BaseButton from '../Button'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Title = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 2rem;
  color: ${colors.BLACK};
  opacity: 0.85;
  margin-bottom: 1.5rem;
`

export const Dialog = styled(Text)`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  letter-spacing: 0.05rem;
  color: ${colors.SLATE_GREY};
  text-align: center;
  margin-bottom: 4rem;
`

export const Button = styled(BaseButton)`
  width: 70%;
`

export const Icon = styled.div`
  width: 4.8rem;
  height: 4.8rem;
  margin-bottom: 2.5rem;
`
