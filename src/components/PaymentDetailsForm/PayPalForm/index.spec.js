import React from 'react'
import { shallow } from 'enzyme'
import PayPalForm from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PayPalForm', () => {
  let testRender
  let getFieldDecorator
  beforeEach(() => {
    getFieldDecorator = jest.fn(() => a => a)
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PayPalForm', async () => {
    testRender = shallow(
      <PayPalForm
        getFieldDecorator={getFieldDecorator}
        initialValues={'flower-wiki@paypal.com'}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
