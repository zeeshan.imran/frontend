import React from 'react'
import { Form } from 'antd'
import Input from '../../../components/Input'
import { useTranslation } from 'react-i18next'


const PayPalForm = ({ getFieldDecorator, initialValues: { paypalEmail } }) => {
  const { t } = useTranslation();
  return (
    <Form.Item>
      {getFieldDecorator('paypalEmail', {
        initialValue: paypalEmail,
        rules: [
          {
            required: true,
            type: 'email',
            message: t('validation.email.email')
          }
        ]
      })(
        <Input
          label={t('components.paymentDetailsForm.paypalEmail')}
          placeholder={t('placeholders.paypalEmail')}
        />
      )}
    </Form.Item>
  )
}

export default PayPalForm
