import React from 'react'
import { mount } from 'enzyme'
import PaymentDetails from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PaymentDetails', () => {
  let testRender
  let form
  let paypalEmail
  let submitForm

  beforeEach(() => {
    form = {
      getFieldDecorator: jest.fn(() => a => a),
      isFieldsTouched: jest.fn(() => b => b),
      getFieldError: jest.fn(() => c => c)
    }
    paypalEmail = 'flovorwiki@gmail.com'
    submitForm = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PaymentDetails', () => {
    testRender = mount(
      <PaymentDetails
        form={form}
        submitForm={submitForm}
        paypalEmail={paypalEmail}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
