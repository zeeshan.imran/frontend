import React from 'react'
import { Form } from 'antd'
import Input from '../../../components/Input'
import { useTranslation } from 'react-i18next';

const BankForm = ({ getFieldDecorator }) => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      <Form.Item>
        {getFieldDecorator('bankAccountName')(
          <Input
            label={t('components.paymentDetailsForm.bankAccountName')}
            placeholder={t('placeholders.bankAccountName')}
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('bankAccountNumber')(
          <Input
            label={t('components.paymentDetailsForm.bankAccountNumber')}
            placeholder={t('placeholders.bankAccountNumber')}
          />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('bankName')(
          <Input label={t('components.paymentDetailsForm.bankName')} placeholder={t('placeholders.bankName')} />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('bankDescription')(
          <Input label={t('components.paymentDetailsForm.bankDescription')} placeholder={t('placeholders.bankDescription')} />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('bankIBAN')(
          <Input label={t('components.paymentDetailsForm.bankIBAN')} placeholder={t('placeholders.bankIBAN')} />
        )}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator('bankBIC')(
          <Input label={t('components.paymentDetailsForm.bankBIC')} placeholder={t('placeholders.bankBIC')} />
        )}
      </Form.Item>
    </React.Fragment>
  )
}

export default BankForm
