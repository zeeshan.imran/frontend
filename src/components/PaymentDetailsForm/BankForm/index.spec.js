import React from 'react'
import { shallow } from 'enzyme'
import BankForm from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('BankForm', () => {
  let testRender
  let getFieldDecorator

  beforeEach(() => {
    getFieldDecorator = jest.fn(() => a => a)
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render BankForm', async () => {
    testRender = shallow(<BankForm getFieldDecorator={getFieldDecorator} />)
    expect(testRender).toMatchSnapshot()
  })
})
