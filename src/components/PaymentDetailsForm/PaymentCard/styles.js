import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'
import { components, DEFAULT_MOBILE_MARGIN } from '../../../utils/Metrics'

export const Shadow = styled.div`
  box-shadow: 0 0.4rem 1.6rem 0 rgba(0, 0, 0, 0.14);
  margin-right: ${({ isLast }) => (isLast ? 0 : `2.6rem`)};
  margin-bottom: ${({ desktop, isLast }) =>
    desktop || isLast ? 0 : `${DEFAULT_MOBILE_MARGIN}rem`};
  border-radius: 1rem;
  width: ${({ desktop }) =>
    desktop ? `${components.PAYMENT_CARD_WIDTH}rem` : `100%`};
`

export const Card = styled.div`
  display: flex;
  flex-direction: row;
  background-color: ${colors.WHITE};
  height: 8rem;
  border-radius: 1rem;
  overflow: hidden;
  cursor: pointer;
  box-shadow: ${({ isActive, borderColor }) =>
    isActive ? `inset 0 0 0 0.2rem ${borderColor}` : `none`};
`

export const Logo = styled.div`
  background-image: ${({ gradient }) =>
    `linear-gradient(to right, ${gradient[0]}, ${gradient[1]})`};
  display: flex;
  justify-content: center;
  align-items: center;
  width: 7.4rem;
  svg {
    width: 100%;
    height: 3.6rem;
  }
`

export const TextAligner = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  margin-left: 1.6rem;
`

export const Text = styled.span`
  font-family: ${family.primaryLight};
  font-size: 2rem;
  user-select: none;
  color: ${({ textColor }) => textColor};
`
