import React from 'react'
import { Desktop } from '../../../components/Responsive'
import { Card, Logo, Text, TextAligner, Shadow } from './styles'

const PaymentCard = ({
  logo,
  title,
  colors,
  handleClick,
  isActive,
  isLast
}) => (
  <Desktop>
    {desktop => (
      <Shadow desktop={desktop} isLast={isLast}>
        <Card isActive={isActive} borderColor={colors[0]} onClick={handleClick}>
          <Logo gradient={colors}>{logo}</Logo>
          <TextAligner>
            <Text textColor={colors[0]}>{title}</Text>
          </TextAligner>
        </Card>
      </Shadow>
    )}
  </Desktop>
)

export default PaymentCard
