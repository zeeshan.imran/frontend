import React from 'react'
import { storiesOf } from '@storybook/react'
import Paypal from '../../../components/SvgIcons/Paypal'
import PaymentCard from './'

storiesOf('PaymentCard', module).add('Paypal Card', () => (
  <PaymentCard
    title={'With Paypal'}
    logo={<Paypal />}
    colors={['#0000FF', '#0000FF']}
  />
))
