import React from 'react';
import { shallow } from 'enzyme';
import PaymentCard from '.';
import Paypal from '../../../components/SvgIcons/Paypal'

describe('PaymentCard', () => {
    let testRender;
    let logo
    let title
    let colors
    let handleClick
    let isActive
    let isLast

    beforeEach(() => {
        logo = <Paypal />
        title = 'With Paypal'
        colors = ['#0000FF', '#0000FF']
        handleClick = jest.fn()
        isActive = true;
        isLast = true;
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render PaymentCard', () => {
        testRender = shallow(

            <PaymentCard
                logo={logo}
                title={title}
                colors={colors}
                handleClick={handleClick}
                isActive={isActive}
                isLast={isLast}
            />

        )
        expect(testRender).toMatchSnapshot()

    })
})