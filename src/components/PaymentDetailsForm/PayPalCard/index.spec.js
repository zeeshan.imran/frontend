import React from 'react'
import { shallow } from 'enzyme'
import PayPalCard from '.'
import PayPal from '../../../components/SvgIcons/Paypal'
jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: text => text })
}))

describe('PayPalCard', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PayPalCard', async () => {
    testRender = shallow(
      <PayPalCard
        logo={<PayPal />}
        title='With Paypal'
        colors={['#009cde', '#003087']}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
