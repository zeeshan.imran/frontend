import React, { Component } from 'react'
import { Form, Col, Row } from 'antd'
import { Desktop } from '../../components/Responsive'
import PayPalCard from './PayPalCard'
import PayPalForm from './PayPalForm'
// import BankAccountCard from './BankAccountCard'
// import BankForm from './BankForm'
import TabNavigation from '../../components/TabNavigation'
import {
  Container,
  ButtonAligner,
  TabsAligner,
  FormContainer,
  StyledButton
} from './styles'
import { withTranslation } from 'react-i18next';

const PAYMENT_OPTIONS = [
  {
    type: 'paypal',
    tabComponent: PayPalCard,
    content: PayPalForm
  }
  // {
  //   type: 'bank',
  //   tabComponent: BankAccountCard,
  //   content: BankForm
  // }
]

class PaymentDetails extends Component {
  render () {
    const {
      form: { getFieldDecorator, isFieldsTouched, getFieldError },
      paypalEmail,
      submitForm,
      t
    } = this.props

    const hasFieldErrors = !!getFieldError('paypalEmail')
    const shouldEnableSubmitButton = isFieldsTouched() && !hasFieldErrors

    return (
      <Desktop>
        {desktop => (
          <Container>
            <Row type='flex' justify='center'>
              <Col xs={{ span: 24 }} lg={{ span: 16 }} xl={{ span: 10 }}>
                <Form>
                  <TabNavigation
                    initiallyActive={PAYMENT_OPTIONS[0]}
                    tabComparator={(tabA, tabB) =>
                      tabA && tabA.type === tabB.type
                    }
                    renderTabs={({ handleNavigate, isActive }) => (
                      <TabsAligner desktop={desktop}>
                        {PAYMENT_OPTIONS.map((option, index) => (
                          <option.tabComponent
                            key={index}
                            isLast={index === PAYMENT_OPTIONS.length - 1}
                            isActive={isActive(option)}
                            handleClick={() => handleNavigate(option)}
                          />
                        ))}
                      </TabsAligner>
                    )}
                    renderTabContent={currentStep => (
                      <FormContainer>
                        <currentStep.content
                          getFieldDecorator={getFieldDecorator}
                          initialValues={{ paypalEmail }}
                        />
                      </FormContainer>
                    )}
                  />
                </Form>
                <Row type='flex' justify='center'>
                  <Col xs={{ span: 24 }} lg={{ span: 12 }}>
                    <ButtonAligner desktop={desktop}>
                      <StyledButton
                        type={shouldEnableSubmitButton ? 'primary' : 'disabled'}
                        onClick={() => submitForm('a', this.props.form)}
                      >
                        {t('components.paymentDetailsForm.submitButton')}
                      </StyledButton>
                    </ButtonAligner>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Container>
        )}
      </Desktop>
    )
  }
}

export default withTranslation()(Form.create()(PaymentDetails))
