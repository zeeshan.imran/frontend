import React from 'react'
import colors from '../../../utils/Colors'
import Bank from '../../../components/SvgIcons/Bank'
import PaymentCard from '../PaymentCard'

import { useTranslation } from 'react-i18next'

const BankAccountCard = props => {
  const { t } = useTranslation();
  return (
    <PaymentCard
      {...props}
      logo={<Bank color={colors.WHITE} />}
      title={t('components.paymentDetailsForm.bankAccount')}
      colors={[colors.LIGHT_OLIVE_GREEN, '#407b07']}
    />
  )
}

export default BankAccountCard
