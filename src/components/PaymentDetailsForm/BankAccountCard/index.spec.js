import React from 'react'
import { shallow } from 'enzyme'
import BankAccountCard from '.'
import colors from '../../../utils/Colors'
import Bank from '../../../components/SvgIcons/Bank'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('BankAccountCard', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render BankAccountCard', async () => {
    testRender = shallow(
      <BankAccountCard
        logo={<Bank color={colors.WHITE} />}
        title='Your Bank Account'
        colors={[colors.LIGHT_OLIVE_GREEN, '#407b07']}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
