import React from 'react'
import ProgressBar from '../ProgressBar'
import { Label } from './styles'

const LabeledProgressBar = ({ percentage, label }) => (
  <React.Fragment>
    <Label>{label}</Label>
    <ProgressBar
      strokeWidth={5}
      percentage={percentage}
      showPercentage={false}
    />
  </React.Fragment>
)

export default LabeledProgressBar
