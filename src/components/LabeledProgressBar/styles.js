import styled from 'styled-components'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const Label = styled.div`
  font-family: ${family.primaryLight};
  font-size: 1.6rem;
  color: ${colors.SLATE_GREY};
`
