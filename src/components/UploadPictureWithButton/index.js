import React, { Component } from 'react'
import { Upload, Button, Icon } from 'antd'
import { isEmpty } from 'ramda'
import UploadPicture from '../UploadPicture'
import { ProfilePictureUpload, ProfilePicture, DefaultIcon } from './styles'

class UploadPictureWithButton extends Component {
  render () {
    const { desktop, imageUrl, handleBeforeUpload } = this.props
    const hasImageToDisplay = !isEmpty(imageUrl)

    if (desktop) {
      return (
        <ProfilePictureUpload desktop={desktop}>
          <ProfilePicture hasImageToDisplay={hasImageToDisplay} src={imageUrl}>
            {hasImageToDisplay ? null : <DefaultIcon type='user' />}
          </ProfilePicture>
          <Upload showUploadList={false} beforeUpload={handleBeforeUpload}>
            <Button>
              <Icon type='upload' /> Upload
            </Button>
          </Upload>
        </ProfilePictureUpload>
      )
    }
    return (
      <ProfilePictureUpload desktop={desktop}>
        <UploadPicture rounded onFileUpload={this.handleUpload} />
      </ProfilePictureUpload>
    )
  }
}

export default UploadPictureWithButton
