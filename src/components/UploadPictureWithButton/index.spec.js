import React from 'react'
import { mount } from 'enzyme'
import UploadPictureWithButton from '.'
import { act } from 'react-dom/test-utils'
import { Upload } from 'antd'
import FlavorWikiLogo from '../../assets/png/FlavorWiki_Full_Logo.png'
import { ProfilePictureUpload } from './styles'


describe('UploadPictureWithButton', () => {
  let testRender
  let desktop
  let imageUrl
  let handleBeforeUpload

  beforeEach(() => {
    desktop = 'yes'
    imageUrl = FlavorWikiLogo
    handleBeforeUpload = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UploadPictureWithButton', () => {
    testRender = mount(
      <UploadPictureWithButton
        desktop={desktop}
        imageUrl={imageUrl}
        handleBeforeUpload={handleBeforeUpload}
      />
    )

    expect(testRender.find(UploadPictureWithButton)).toHaveLength(1)
  })

  test('should render Upload before', () => {
    testRender = mount(
      <UploadPictureWithButton
        desktop={desktop}
        imageUrl={imageUrl}
        handleBeforeUpload={handleBeforeUpload}
      />
    )

    act(() => {
      testRender.find(Upload).simulate('change')
    })
    expect(handleBeforeUpload).not.toHaveBeenCalled()
  })

  test('should render UploadPictureWithButton without desktop', () => {
    const desktop = null
    testRender = mount(
      <UploadPictureWithButton
        desktop={desktop}
        imageUrl={imageUrl}
        handleBeforeUpload={handleBeforeUpload}
      />
    )

    expect(testRender.find(ProfilePictureUpload)).toHaveLength(1)
  })
})
