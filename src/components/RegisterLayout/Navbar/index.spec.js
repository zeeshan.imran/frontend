import React from 'react'
import { mount } from 'enzyme'
import Navbar from '.'
import { createBrowserHistory } from 'history'
import gql from 'graphql-tag'
import { Router } from 'react-router-dom'
import { MockedProvider } from 'react-apollo/test-utils'
import { Logo } from './styles'
import history from '../../../history'
import { act } from 'react-dom/test-utils'

jest.mock('../../../history') 

const query = gql`
  query hello {
    hello
  }
`

const mocks = [
  {
    request: { query },
    result: { data: { hello: 'world' } }
  }
]

describe('Navbar', () => {
  let testRender
  let entriesData
  let getEntryName
  let onClickEntry

  beforeEach(() => {
    entriesData
    getEntryName
    onClickEntry
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Navbar', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <MockedProvider mocks={mocks}>
        <Router history={history}>
          <Navbar
            entriesData={entriesData}
            getEntryName={getEntryName}
            onClickEntry={onClickEntry}
          />
        </Router>
      </MockedProvider>
    )
    expect(testRender.find(Navbar)).toHaveLength(1)
  })

  test('should render Navbar onclick logo', () => {
    testRender = mount(
      <Navbar
        entriesData={entriesData}
        getEntryName={getEntryName}
        onClickEntry={onClickEntry}
      />
    )

    act(() => {
      testRender.find(Logo).simulate('click')
    })

    expect(history.push).toHaveBeenCalledWith('/')
  })
})
