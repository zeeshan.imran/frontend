import React from 'react'
import history from '../../../history'
import { Desktop } from '../../Responsive'
import { NavbarContainer, ContentContainer, Logo } from './styles'
import { getImages } from '../../../utils/getImages'
import { imagesCollection } from '../../../assets/png'
const { desktopImage } = getImages(imagesCollection)

const Navbar = ({ entriesData, getEntryName, onClickEntry }) => (
  <Desktop>
    {desktop => (
      <NavbarContainer>
        <ContentContainer desktop={desktop}>
          <Logo onClick={() => history.push('/')} src={desktopImage} />
        </ContentContainer>
      </NavbarContainer>
    )}
  </Desktop>
)

export default Navbar
