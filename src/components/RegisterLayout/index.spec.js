import React from 'react'
import { mount } from 'enzyme'
import RegisterLayout from '.'
import { createBrowserHistory } from 'history'
import { Router } from 'react-router-dom'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo'
import SIGN_UP_FORM_QUERY from '../../queries/signUpForm'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('RegisterLayout', () => {
  let testRender
  let steps
  let currentPath
  let firstStepValid

  beforeEach(() => {
    steps = [
      {
        title: 'Title 1',
        description: `Description 1`,
        path: 'path-1'
      },
      {
        title: 'Title 2',
        description: `Description 2`,
        path: 'path-2'
      },
      {
        title: 'Title 3',
        description: `Description 3`,
        path: 'path-3'
      }
    ]

    currentPath = 'path - 1'
    firstStepValid = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render RegisterLayout', async () => {
    const history = createBrowserHistory()
    const client = createApolloMockClient()
    client.cache.writeQuery({
      query: SIGN_UP_FORM_QUERY,
      data: {
        signUpForm: {
          firstStepValid: null,
          secondStepValid: null,
          lastStepValid: null,
          userId: null,
          email: null,
          password: null,
          firstname: null,
          lastname: null,
          birthday: null,
          gender: null,
          language: null,
          ethnicity: null,
          currency: null,
          incomeRange: null,
          hasChildren: null,
          numberOfChildren: null,
          childrenAges: null,
          smokerKind: null,
          productTypes: null,
          dietaryRestrictions: null,
          recaptchaResponseToken: null
        }
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <RegisterLayout
            steps={steps}
            currentPath={currentPath}
            firstStepValid={firstStepValid}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(RegisterLayout)).toHaveLength(1)
  })
})
