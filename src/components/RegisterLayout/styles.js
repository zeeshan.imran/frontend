import styled from 'styled-components'
import BaseText from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const HEADER_HEIGHT = 7.5

export const Container = styled.div`
  width: 100%;
  background-color: ${colors.WHITE};
  display: flex;
  flex-direction: column;
  position: static;
`

export const Content = styled.div`
  background-color: ${colors.WHITE};
  padding-top: ${({ desktop }) => (desktop ? '16.6rem' : '13rem')};
  overflow-y: auto;
  min-height: 100vh;
  padding-left: ${({ desktop }) => (desktop ? 0 : '2.3rem')};
  padding-right: ${({ desktop }) => (desktop ? 0 : '2.3rem')};
  padding-bottom: ${({ desktop }) => (desktop ? '5.6rem' : '10rem')};
`

export const Text = styled(BaseText)`
  display: flex;
  font-family: ${family.primaryLight};
  color: ${colors.BLACK};
  user-select: none;
`
export const Title = styled(Text)`
  font-size: ${({ desktop }) => (desktop ? 4.2 : 3)}rem;
  line-height: 1.24;
  margin-bottom: 1.5rem;
  justify-content: ${({ desktop }) => (desktop ? 'left' : 'center')};
`

export const GreyText = styled(Text)`
  font-size: ${({ desktop }) => (desktop ? 1.8 : 1.4)}rem;
  line-height: 1.56;
  opacity: 0.7;
`

export const StepsText = styled(GreyText)`
  margin-bottom: 3rem;
`

export const DescriptionText = styled(GreyText)`
  margin-bottom: 3.5rem;
  justify-content: ${({ desktop }) => (desktop ? 'left' : 'center')};
`
