import React from 'react'
import { Modal } from 'antd'
import { useTranslation } from 'react-i18next'
import { DownloadSection } from './styles'
import PngFile from '../../assets/png/PngFile.png'
import SvgFile from '../../assets/png/SvgFile.png'
import EpsFile from '../../assets/png/EpsFile.png'

const OperatorQrCodeDownloadComponent = ({
  qrCode,
  onCancel,
  onDownload
}) => {
  const { t } = useTranslation()
  const visible = !!qrCode
  return (
    <Modal
      title={t('components.operatorQrCodeDownload.title')}
      visible={visible}
      onCancel={onCancel}
      footer={null}
    >
      <p>{t('components.operatorQrCodeDownload.prompt')}</p>
      <DownloadSection>
        <button onClick={() => onDownload(qrCode, 'png')}>
          <img src={PngFile} alt='Portable Network Graphics' />
          <div>Portable Network Graphics</div>
        </button>
        <button onClick={() => onDownload(qrCode, 'svg')}>
          <img src={SvgFile} alt='Scalable Vector Graphics' />
          <div>Scalable Vector Graphics</div>
        </button>
        <button onClick={() => onDownload(qrCode, 'eps')}>
          <img src={EpsFile} alt='Encapsulated PostScript/PostScript' />
          <div>Encapsulated PostScript/PostScript</div>
        </button>
      </DownloadSection>
    </Modal>
  )
}
export default OperatorQrCodeDownloadComponent
