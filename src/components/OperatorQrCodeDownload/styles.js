import styled from 'styled-components'
import colors from '../../utils/Colors'

export const DownloadSection = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  img {
    max-height: 64px;
  }

  button {
    border: none;
    cursor: pointer;
    outline: none;
    &:hover {
      color: ${colors.QR_CODE_MODAL_HOVER_COLOR};
      img {
        filter: drop-shadow(0px 0px 5px ${colors.QR_CODE_MODAL_HOVER_COLOR});
      }
    }
  }
`
