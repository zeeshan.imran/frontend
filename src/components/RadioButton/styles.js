import styled from 'styled-components'
import { Icon as AntIcon } from 'antd'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  align-items: center;
`

export const Text = styled.span`
  user-select: none;
  flex: 1;
  font-family: ${family.primaryRegular};
  color: ${colors.SLATE_GREY};
  font-size: 1.6rem;
  opacity: ${({ checked }) => (checked ? 1 : 0.8)};
  :hover {
    opacity: 1;
  }
`

export const Circle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 3rem;
  max-width: 3rem;
  min-height: 3rem;
  max-height: 3rem;
  background-color: ${({ checked, disabled }) =>
    disabled
      ? `${colors.PALE_LIGHT_GREY}`
      : checked
        ? `${colors.LIGHT_OLIVE_GREEN}`
        : `${colors.WHITE}`};
  border: ${({ checked }) =>
    checked ? 'none' : `solid 0.2rem ${colors.PALE_LIGHT_GREY}`};
  border-radius: 50%;
  margin-right: 2.2rem;
  cursor: pointer;
`

export const CheckedIcon = styled(AntIcon)`
  color: ${colors.WHITE};
`
