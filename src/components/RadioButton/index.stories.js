import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import RadioButton from './'

const elementId = '12345'

class Wrapper extends Component {
  state = {
    buttonChecked: true,
    buttonUnchecked: false
  }

  render () {
    return this.props.children({
      buttonChecked: this.state.buttonChecked,
      buttonUnchecked: this.state.buttonUnchecked,
      handleButtonChecked: () =>
        this.setState({ buttonChecked: !this.state.buttonChecked }),
      handleButtonUnchecked: () =>
        this.setState({ buttonUnchecked: !this.state.buttonUnchecked })
    })
  }
}

storiesOf('RadioButton', module)
  .add('Radio Button without onClick', () => (
    <Wrapper>
      {({ buttonChecked, handleButtonChecked }) => {
        return (
          <RadioButton checked={buttonChecked} id={elementId}>
            Radio Button Without OnClick
          </RadioButton>
        )
      }}
    </Wrapper>
  ))
  .add('Radio Button with onClick', () => (
    <Wrapper>
      {({ buttonChecked, handleButtonChecked }) => {
        return (
          <RadioButton
            checked={buttonChecked}
            id={elementId}
            onClick={handleButtonChecked}
          >
            Radio Button With OnClick
          </RadioButton>
        )
      }}
    </Wrapper>
  ))
  .add('Radio Button with default unchecked', () => (
    <Wrapper>
      {({ buttonUnchecked, handleButtonUnchecked }) => {
        return (
          <RadioButton
            id={elementId}
            checked={buttonUnchecked}
            onClick={handleButtonUnchecked}
          >
            Radio Button With default checked
          </RadioButton>
        )
      }}
    </Wrapper>
  ))
  .add('Radio Button disabled', () => (
    <RadioButton id={elementId} disabled>
      Radio Button Disabled
    </RadioButton>
  ))
