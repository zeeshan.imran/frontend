import React from 'react'
import { mount } from 'enzyme'
import RadioButton from '.';
import { Container, Circle, Text } from './styles'
describe('RadioButton', () => {
   let testRender;
   let onClick;
   let checked;
   let children;
   let disabled

   beforeEach(() => {
       onClick = jest.fn();                  // you all ready have an onClick mocked
       checked = true;
       disabled = false
       children = null;
   })
   afterEach(() => {
       testRender.unmount()
   })
    test('should render Radio Button Circle', async () => {
        testRender = mount(
            <RadioButton
                checked={checked}
                children={children}
            />
        )
        expect(testRender.find(Circle)).toHaveLength(1)
    })




    test('should render Radio Button Text', async () => {
        testRender = mount(
            <RadioButton
                checked={checked}
                children={children}
            />
        )
        expect(testRender.find(Text)).toHaveLength(1)      // i thonk this line will fail
    })




    test('should call on click when enabled', async () => {
        testRender = mount(
            <RadioButton
                checked={checked}
                onClick={onClick}
                children={children}
                disabled={false}
            />
        )
    testRender.find(Container).simulate('click')
    expect(onClick).toHaveBeenCalled()
})


    test('should not call on click when disabled', async () => {
    testRender = mount(
        <RadioButton
            checked={checked}
            onClick={onClick}
            children={children}
            disabled={true}
        />
        )
    testRender.find(Container).simulate('click')
        expect(onClick).not.toHaveBeenCalled()
    })
});