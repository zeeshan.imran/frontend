import React from 'react'
import PropTypes from 'prop-types'
import { Container, Circle, CheckedIcon, Text } from './styles'

const handleClick = ({ disabled, onClick }) => {
  if (disabled) return

  onClick()
}

const RadioButton = ({ checked, disabled, onClick, children, suffix = null }) => (
  <Container onClick={() => handleClick({ onClick, disabled })}>
    <Circle disabled={disabled} checked={checked}>
      {checked && <CheckedIcon type='check' />}
    </Circle>
    <Text checked={checked}>{children}</Text>
    {suffix}
  </Container>
)

RadioButton.defaultProps = {
  onClick: () => {},
  disabled: false
}

RadioButton.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool
}

export default RadioButton
