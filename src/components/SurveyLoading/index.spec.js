import React from 'react'
import { shallow } from 'enzyme'
import SurveyLoadingComponent from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('SurveyLoadingComponent', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyLoadingComponent', async () => {
    testRender = shallow(<SurveyLoadingComponent />)
    expect(testRender).toMatchSnapshot()
  })
})
