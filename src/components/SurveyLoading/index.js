import React from 'react'
import Loader from '../Loader'
import { LoaderWrapper } from './styles'

const SurveyLoadingComponent = () => (
  <LoaderWrapper>
    <Loader size={150} />
  </LoaderWrapper>
)

export default SurveyLoadingComponent
