import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import { Form, Row } from 'antd'
import { useTranslation } from 'react-i18next'
import { StyledText, StyledTransfer } from './styles'
import { pluck, equals } from 'ramda'

const ShareStatsForm = ({
  survey,
  organizations,
  setNewShares,
  setIsOkDisabled,
  isOkDisabled
}) => {
  const { t } = useTranslation()
  const [userTransferProps, setUserTransferProps] = useState({})
  const { sharedStatsUsers } = survey
  const membersByOrg = []
  const organizationDataMock = organizations.map(org => {
    let memberLength = 0
    org.members.map(member => {
      if (member.isSuperAdmin || member.type !== 'taster') {
        memberLength++
        membersByOrg.push({
          key: member.id,
          title: member.fullName || member.emailAddress,
          org: org.id
        })
      }
      return member
    })
    return {
      key: org.id,
      title: org.name.trim(),
      memberLength
    }
  })

  const initialMemberDataSelected = sharedStatsUsers.length
    ? pluck('id', sharedStatsUsers)
    : []
  
  const [organizationDataSelected, setOrganizationDataSelected] = useState(
    sharedStatsUsers.length ? pluck('id' ,pluck('organization', sharedStatsUsers)) : []
  )

  const initialMemberDataMock = organizationDataSelected.length
    ? membersByOrg.filter(
        member => organizationDataSelected.indexOf(member.org) >= 0
      )
    : []
  const [memberDataMock, setMemberDataMock] = useState(initialMemberDataMock)

  const [memberDataSelected, setMemberDataSelected] = useState(
    sharedStatsUsers.length ? pluck('id', sharedStatsUsers) : []
  )

  useEffect(() => {
    if (Object.keys(userTransferProps).length) {
    setUserTransferProps({})
    }
    if (!isOkDisabled) {
      if (
        equals(initialMemberDataSelected, memberDataSelected) &&
        equals(initialMemberDataMock, memberDataMock)
      ) {
        setIsOkDisabled(true)
      }
    } else if (
      !equals(initialMemberDataSelected, memberDataSelected) ||
      !equals(initialMemberDataMock, memberDataMock)
    ) {
      setIsOkDisabled(false)
    }
  })

  return (
    <Formik
      render={() => {
        return (
          <React.Fragment>
            <Row>
              <Form.Item>
                <StyledText>
                  {t(`components.shareStatsForm.organizations.label`)}
                </StyledText>
                <StyledTransfer
                  titles={[
                    t(`components.shareStatsForm.organizations.tabs.all`),
                    t(`components.shareStatsForm.organizations.tabs.selected`)
                  ]}
                  dataSource={organizationDataMock.filter(
                    org => org.memberLength > 0
                  )}
                  targetKeys={organizationDataSelected}
                  onChange={targetKeys => {
                    setOrganizationDataSelected(targetKeys)
                    const newSelectedMembers = []
                    const newMembersList = membersByOrg.filter(member => {
                      const inIndex = targetKeys.indexOf(member.org) >= 0
                      if (
                        inIndex &&
                        memberDataSelected.indexOf(member.key) >= 0
                      ) {
                        newSelectedMembers.push(member.key)
                      }
                      return inIndex
                    })
                    setMemberDataMock(newMembersList)
                    setMemberDataSelected(newSelectedMembers)
                    setNewShares(newSelectedMembers)
                    setUserTransferProps({
                      selectedKeys: []
                    })
                  }}
                  render={item => item.title}
                />
              </Form.Item>
            </Row>
            <Row>
              <Form.Item>
                <StyledText>
                  {t(`components.shareStatsForm.users.label`)}
                </StyledText>
                <StyledTransfer
                  titles={[
                    t(`components.shareStatsForm.users.tabs.all`),
                    t(`components.shareStatsForm.users.tabs.selected`)
                  ]}
                  dataSource={memberDataMock}
                  targetKeys={memberDataSelected}
                  onChange={targetKeys => {
                    setMemberDataSelected(targetKeys)
                    setNewShares(targetKeys)
                    setUserTransferProps({
                      selectedKeys: []
                    })
                  }}
                  render={item => item.title}
                  {...userTransferProps}
                />
              </Form.Item>
            </Row>
          </React.Fragment>
        )
      }}
    />
  )
}

export default ShareStatsForm
