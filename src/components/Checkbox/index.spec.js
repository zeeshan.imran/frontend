import React from 'react'
import { mount } from 'enzyme'
import CustomCheckbox from '.'

describe('CustomCheckbox', () => {
  let testRender
  let children
  let checked
  let onChange
  let bigger

  beforeEach(() => {
    children = 'New checkbox'
    checked = true
    onChange = jest.fn()
    bigger = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CustomCheckbox', async () => {
    testRender = mount(
      <CustomCheckbox
        children={children}
        checked={checked}
        onChange={onChange}
        bigger={bigger}
      />
    )
    expect(testRender.find(CustomCheckbox)).toHaveLength(1)
  })

  test('should render Upload before', () => {
    testRender = mount(
      <CustomCheckbox checked={checked} onChange={onChange} bigger={bigger} />
    )

    testRender
      .find('input[type="checkbox"]')
      .simulate('change', { target: { checked: true } })
  })
})
