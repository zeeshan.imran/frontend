import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Label, StyledCheckbox } from './styles'

class CustomCheckbox extends Component {
  render () {
    const { children, checked, onChange, bigger, disabled } = this.props

    return (
      <StyledCheckbox
        disabled={disabled}
        checked={checked}
        onChange={event => onChange(event.target.checked)}
        bigger={bigger}
      >
        <Label bigger={bigger}>{children}</Label>
      </StyledCheckbox>
    )
  }
}

CustomCheckbox.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func
}

CustomCheckbox.defaultProps = {
  checked: false
}

export default CustomCheckbox
