import React from 'react'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'
import {
  ColumnContainer,
  SurveyName,
  SurveyInitials,
  AntTable,
  ImagePlaceHolder,
  AllowedStatus,
  NotAllowedStatus
} from './styles'
import { useTranslation } from 'react-i18next'

const NOT_COMPLETED = 'not-completed'

const SurveyStatus = ({ state, allowRetakes }) => {
  const { t } = useTranslation()
  if (state === NOT_COMPLETED) {
    return <AllowedStatus>{t('components.surveyList.ready')}</AllowedStatus>
  }

  if (allowRetakes) {
    return (
      <AllowedStatus>
        {t('components.surveyList.retake', { state })}
      </AllowedStatus>
    )
  }

  return (
    <NotAllowedStatus>{t(`components.surveyList.${state}`)}</NotAllowedStatus>
  )
}

const SurveysList = ({
  surveys,
  handleNavigateToSurvey,
  loading,
  findClosedEnrollments
}) => {
  const { t } = useTranslation()

  const getEnrollmentState = survey => {
    const enrollment = findClosedEnrollments(survey)

    if (
      enrollment &&
      (enrollment.state === 'rejected' || enrollment.state === 'finished')
    ) {
      return enrollment.state
    }

    return NOT_COMPLETED
  }

  const maxReward = (survey) => {
    const {isScreenerOnly, linkedSurveys, products} = survey
    let sum = 0
    if(isScreenerOnly) {
      linkedSurveys.forEach((s) => {
        s.products.forEach((p) => {
          sum += p.reward
        })
      })
      return `Qualify to earn upto ${sum}`
    }else{
      products.forEach((p) => {
        sum += p.reward
      })
      return `Earn upto ${sum}`
    }
   
    
  }

  const columns = [
    {
      title: t('components.surveysList.survey'),
      render: (_, survey) => {
        return (
          <ColumnContainer>
            <SurveyName>
              <ImagePlaceHolder photo={survey.coverPhoto} />
              <SurveyInitials>{survey.name}</SurveyInitials>
            </SurveyName>
          </ColumnContainer>
        )
      }
    },{
      title: t('components.surveysList.status'),
      render: (_, survey) => {
        return (
          <ColumnContainer>
            <SurveyName>
              <SurveyStatus
                allowRetakes={survey.allowRetakes}
                state={getEnrollmentState(survey)}
                />
            </SurveyName>
          </ColumnContainer>
        )
      }
    },{
      title: t('components.surveysList.reward'),
      render: (_, survey) => {
        return (
          <ColumnContainer>
            <SurveyName>
              {maxReward(survey)}
            </SurveyName>
          </ColumnContainer>
        )
      }
    }
  ]

  return (
    <AntTable
      rowKey={record => record.id}
      rowClassName={survey => {
        if (!survey.allowRetakes && findClosedEnrollments(survey)) {
          return `survey-completed-ant-desgin-table disabled`
        } else {
          return `clickable`
        }
      }}
      columns={columns}
      dataSource={surveys}
      showHeader
      pagination={{
        pageSize: DEFAULT_N_ELEMENTS_PER_PAGE
      }}
      loading={loading}
      onRow={survey => ({
        onClick: () => {
          if (
            !getEnrollmentState(survey) !== NOT_COMPLETED ||
            survey.allowRetakes
          ) {
            handleNavigateToSurvey(survey)
          }
        }
      })}
    />
  )
}

export default SurveysList
