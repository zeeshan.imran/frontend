import React from 'react'
import { mount } from 'enzyme'
import SurveysList from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Table as AntTable } from 'antd'

window.matchMedia = jest.fn().mockImplementation(query => {
  return {
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn()
  }
})
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('SurveysList', () => {
  let testRender
  let surveys
  let allSurveyEnrollements
  let handleNavigateToSurvey
  let loading
  let client
  let findClosedEnrollments

  beforeEach(() => {
    surveys = [
      {
        id: '1',
        products: [],
        state: 'draft',
        allowRetakes: false
      },
      {
        id: '2',
        products: [],
        state: 'suspended',
        allowRetakes: false
      },
      {
        id: '3',
        products: [],
        state: 'deprecated',
        allowRetakes: false
      },
      {
        id: '4',
        products: [],
        state: 'active',
        allowRetakes: false
      }
    ]
    const surveyEnrollments = [
      { survey: { id: '1' }, state: 'finished' },
      { survey: { id: '2' }, state: 'wait' },
      { survey: { id: '3' }, state: 'rejected' },
      { survey: { id: '4' }, state: 'wait' }
    ]
    findClosedEnrollments = survey => {
      return surveyEnrollments.find(enrollment => {
        return (
          enrollment.survey.id === survey.id &&
          (enrollment.state === 'finished' || enrollment.state === 'rejected')
        )
      })
    }
    allSurveyEnrollements = ['1', '2', '3', '4']
    handleNavigateToSurvey = jest.fn()
    loading = true
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveysList', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SurveysList
          findClosedEnrollments={findClosedEnrollments}
          surveys={surveys}
          allSurveyEnrollements={allSurveyEnrollements}
          handleNavigateToSurvey={handleNavigateToSurvey}
          loading={loading}
        />
      </ApolloProvider>
    )
    expect(testRender.find(SurveysList)).toHaveLength(1)
  })

  test('should render SurveysList', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SurveysList
          findClosedEnrollments={findClosedEnrollments}
          surveys={surveys}
          allSurveyEnrollements={allSurveyEnrollements}
          handleNavigateToSurvey={handleNavigateToSurvey}
        />
      </ApolloProvider>
    )

    testRender.find(AntTable).prop('onRow')({ target: { id: 1 } })
  })
})
