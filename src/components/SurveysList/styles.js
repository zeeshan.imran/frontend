import styled, { css } from 'styled-components'
import Text from '../Text'
import { family } from '../../utils/Fonts'
import { Table } from 'antd'
import colors from '../../utils/Colors'

export const FieldText = styled(Text)`
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  line-height: 2.2rem;
  color: rgba(0, 0, 0, 0.45);
  user-select: none;
  white-space: pre-wrap;
`

export const SurveyName = styled(FieldText)`
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.65);
  width: 100%;
  display: flex;
  align-self: center;
`

export const ImagePlaceHolder = styled.div`
  max-width: 44px;
  width: 44px;
  max-height: 44px;
  height: 44px;
  margin-right: 1rem;
  border-radius: 3px;
  ${({ photo }) =>
    photo &&
    css`
      background-size: cover;
      background-image: url(${photo});
    `}
`

export const AntTable = styled(Table)`
  td {
    padding: 0.5rem;
  }

  tr.disabled {
    ${SurveyName} {
      color: #999;
      font-family: ${family.primaryRegular};
    }
    ${ImagePlaceHolder} {
      opacity: 0.5;
    }
  }
`

export const ColumnContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`

export const SurveyInitials = styled.span`
  flex: 1;
  align-self: center;
`

export const SurveyCompleted =  styled(FieldText)`
  font-family: ${family.primaryLight};
`

export const NotAllowedStatus = styled.div`
  font-family: ${family.primaryRegular};
  font-size: 12px;
  color: #ff914d;
  line-height: 12px;
`
export const AllowedStatus = styled.div`
  font-family: ${family.primaryRegular};
  font-size: 11px;
  color: ${colors.ALLOWED_STATUS_COLOR};
  line-height: 14px;
`
