import React from 'react'
import Button from '../Button'
import { Container, HeaderContainer, SearchBarContainer } from './styles'
import SearchBar from '../SearchBar'
import OperatorQrCodesList from '../OperatorQrCodesList'
import { withTranslation } from 'react-i18next'

const OperatorQrCodes = ({
  loading,
  qrCodes,
  searchTerm,
  onSearch,
  onCreate,
  onEdit,
  onDelete,
  onCopy,
  t,
  onDownload,
  onTableChange,
  total,
  page
}) => (
  <Container>
    <HeaderContainer>
      <Button data-testid='create-qr' onClick={onCreate}
      >
        {t('components.operatorQrCode.createQr')}
      </Button>
      <SearchBarContainer>
        <SearchBar
          placeholder='Search'
          withIcon
          handleChange={onSearch}
          value={searchTerm}
        />
      </SearchBarContainer>
    </HeaderContainer>
    <OperatorQrCodesList
      loading={loading}
      qrCodes={qrCodes}
      onEdit={onEdit}
      onDelete={onDelete}
      onCopy={onCopy}
      onDownload={onDownload}
      onTableChange={onTableChange}
      total={total}
      page={page}
    />
  </Container>
)

export default withTranslation()(OperatorQrCodes)
