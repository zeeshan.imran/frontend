import React from 'react'
import { mount } from 'enzyme'
import LinkText from '.'
import { LinkStyledText } from './styles'

describe('LinkText', () => {
  let testRender
  let onClick
  let classname
  let children
  let otherProps

  beforeEach(() => {
    onClick = jest.fn()
    classname = 'center'
    children = '<container>Link here</container>'
    otherProps = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render LinkText', async () => {
    testRender = mount(
      <LinkText
        onClick={onClick}
        classname={classname}
        children={children}
        otherProps={otherProps}
      />
    )
    expect(testRender.find(LinkText)).toHaveLength(1)

    testRender
      .find(LinkStyledText)
      .props()
      .onClick()
    expect(onClick).toBeCalled()
  })
})
