import React from 'react'

import { LinkStyledText } from './styles'

const LinkText = ({ onClick, classname, children, ...otherProps }) => (
  <LinkStyledText onClick={onClick} classname={classname}>
    {children}
  </LinkStyledText>
)

export default LinkText
