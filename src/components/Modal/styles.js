import styled from 'styled-components'
import { Modal as AntModal } from 'antd'

export const StyledModal = styled(AntModal)`
  .ant-modal-body {
    padding: 5.5rem 4rem 3.5rem 4rem;
  }
`
