import React from 'react'
import PropTypes from 'prop-types'
import { StyledModal } from './styles'

const Modal = ({ visible, toggleModal, children }) => (
  <StyledModal
    maskStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}
    visible={visible}
    closable={false}
    footer={null}
    maskClosable
    onCancel={toggleModal}
    centered
  >
    {children}
  </StyledModal>
)

Modal.propTypes = {
  visible: PropTypes.bool,
  children: PropTypes.node,
  toggleModal: PropTypes.func
}

export default Modal
