import React, { Component } from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'
import Modal from './'

const Container = styled.div``
const ToggleText = styled.div`
  cursor: pointer;
`

class StateWrapper extends Component {
  state = {
    visible: ''
  }

  render () {
    return this.props.children({
      toggleModal: () => this.setState({ visible: !this.state.visible }),
      visible: this.state.visible
    })
  }
}

storiesOf('Modal', module).add('Default Modal', () => (
  <StateWrapper>
    {({ toggleModal, visible }) => {
      return (
        <Container>
          <ToggleText onClick={toggleModal}>Click here to open</ToggleText>
          <Modal visible={visible} toggleModal={toggleModal}>
            Modal Content
          </Modal>
        </Container>
      )
    }}
  </StateWrapper>
))
