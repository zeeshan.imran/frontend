import gql from 'graphql-tag'

export const genderInfo = gql`
  fragment genderInfo on GenderType {
    id: iid
    key
    value
  }
`
