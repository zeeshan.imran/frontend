import gql from 'graphql-tag'

export const loginInfo = gql`
  fragment loginInfo on Login {
    token
    ok
  }
`
