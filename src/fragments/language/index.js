import gql from 'graphql-tag'

export const languageInfo = gql`
  fragment languageInfo on LanguageType {
    id: iid
    key
    value
  }
`
