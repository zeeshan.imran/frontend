import gql from 'graphql-tag'

export const enrollmentStateInfo = gql`
  fragment enrollmentStateInfo on SurveyEnrollment {
    id
    state
    lastAnsweredQuestion {
      id
    }
    user {
      id
    }
    selectedProducts {
      id
      name
      photo
    }
  }
`
