import gql from 'graphql-tag'

export const surveyInfo = gql`
  fragment surveyInfo on SurveyNode {
    id: iid
    title
    createdById
    state
    isPaidSurvey
    isPointsSurvey
    expirationTime
    completionSecs
    surveySecs
  }
`
