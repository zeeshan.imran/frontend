import gql from 'graphql-tag'
import { languageInfo } from '../language'

export const userInfo = gql`
  fragment userInfo on UserNode {
    id: iid
    firstname
    lastname
    email
    language {
      ...languageInfo
    }
  }
  ${languageInfo}
`
