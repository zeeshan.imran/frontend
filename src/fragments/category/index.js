import gql from 'graphql-tag'

export const categoryInfo = gql`
  fragment categoryInfo on ProductCategoryType {
    id: iid
    value
    key
  }
`

export const categoryCardInfo = gql`
  fragment categoryCardInfo on ProductCategoryType {
    id: iid
    value
    key
    children {
      id: iid
      value
      key
    }
  }
`
