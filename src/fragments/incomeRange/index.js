import gql from 'graphql-tag'

export const incomeRangeInfo = gql`
  fragment incomeRangeInfo on IncomeRangeType {
    id: iid
    value: label
    currency
  }
`
