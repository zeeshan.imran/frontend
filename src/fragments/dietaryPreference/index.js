import gql from 'graphql-tag'

export const dietaryPreferenceInfo = gql`
  fragment dietaryPreferenceInfo on DietaryPreferenceType {
    id: iid
    key
    value
  }
`
