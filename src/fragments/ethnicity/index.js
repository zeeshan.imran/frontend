import gql from 'graphql-tag'

export const ethnicityInfo = gql`
  fragment ethnicityInfo on EthnicityType {
    id: iid
    key
    value
  }
`
