import gql from 'graphql-tag'
import { genderInfo } from '../gender'
import { ethnicityInfo } from '../ethnicity'
import { incomeRangeInfo } from '../incomeRange'
import { smokerKindInfo } from '../smokerKind'
import { dietaryPreferenceInfo } from '../dietaryPreference'
import { preferredProductCategoryInfo } from '../preferredProductCategories'

export const testerInfo = gql`
  fragment testerInfo on TesterInfoNode {
    birthday
    genderId
    ethnicityId
    incomeRangeId
    childrenAges
    smokerKindId
    dietaryPreferenceId
    gender {
      ...genderInfo
    }
    ethnicity {
      ...ethnicityInfo
    }
    incomeRange {
      ...incomeRangeInfo
    }
    smokerKind {
      ...smokerKindInfo
    }
    dietaryPreference {
      ...dietaryPreferenceInfo
    }
    preferredProductCategories {
      ...preferredProductCategoryInfo
    }
    paypalEmail
    privacyMarketingEmails
    privacyMarketingPushNotifications
    privacyOtherPushNotifications
  }
  ${genderInfo}
  ${ethnicityInfo}
  ${incomeRangeInfo}
  ${smokerKindInfo}
  ${dietaryPreferenceInfo}
  ${preferredProductCategoryInfo}
`
