import gql from 'graphql-tag'

export const smokerKindInfo = gql`
  fragment smokerKindInfo on SmokerKindType {
    id: iid
    key
    value
  }
`
