import gql from 'graphql-tag'
import { testerInfo } from '../testerInfo'
import { userInfo } from '../user'

export const userSubscribedSurveysInfo = gql`
  fragment userSubscribedSurveysInfo on UserNode {
    testerInfo {
      surveys {
        survey {
          title
          id: iid
          expirationTime
        }
      }
    }
  }
`

export const currentUserInfo = gql`
  fragment currentUserInfo on UserNode {
    ...userInfo
    testerInfo {
      ...testerInfo
    }
  }
  ${userInfo}
  ${testerInfo}
`
