import i18n from '../utils/internationalization/i18n'
import { getMessage } from '../utils/getCustomSharingMessage'
import { getLoginText } from '../utils/getLoginText'
import { getPauseText } from '../utils/getPauseText'
import { getAuthenticatedOrganization } from '../utils/userAuthentication'
import { DEFAULT_TASTING_NOTES } from '../utils/Constants'

export default {
  editMode: 'default',
  products: [],
  basics: {
    name: null,
    coverPhoto: null,
    instructionsText: null,
    instructionSteps: [
      i18n.t('defaultValues.instructionSteps.0'),
      i18n.t('defaultValues.instructionSteps.1'),
      i18n.t('defaultValues.instructionSteps.2'),
      i18n.t('defaultValues.instructionSteps.3')
    ],
    thankYouText: getMessage('defaultValues.thankYouText'),
    rejectionText: getMessage('defaultValues.rejectionText'),
    screeningText: getMessage('defaultValues.screeningText'),
    uniqueName: null,
    authorizationType: 'public',
    exclusiveTasters: [],
    allowRetakes: false,
    isScreenerOnly: false,
    showGeneratePdf: false,
    optionDisplayType: 'labelOnly',
    productDisplayType: 'none',
    linkedSurveys: [],
    forcedAccount: false,
    forcedAccountLocation: 'start',
    tastingNotes: DEFAULT_TASTING_NOTES,
    referralAmount: 1,
    recaptcha: false,
    savedRewards: [],
    minimumProducts: 1,
    maximumProducts: null,
    surveyLanguage: 'en',
    country: 'US',
    maxProductStatCount: 6,
    customizeSharingMessage: getMessage(
      'defaultValues.customizeSharingMessage'
    ),
    loginText: getLoginText(),
    pauseText: getPauseText(),
    owner: getAuthenticatedOrganization(),
    allowedDaysToFillTheTasting: 5,
    isPaypalSelected: false,
    isGiftCardSelected: false,
    customButtons: {
      continue: 'Continue',
      start: 'Start',
      next: 'Next',
      skip: 'Skip'
    },
    autoAdvanceSettings: {
      active: false,
      debounce: 0,
      hideNextButton: false
    },
    pdfFooterSettings: {
      active: false,
      footerNote: ''
    },
    sharingButtons: true,
    validatedData: false,
    showOnTasterDashboard: false,
    enabledEmailTypes: [],
    emails: {
      surveyWaiting: {
        subject: i18n.t('email.surveyWaiting.subject'),
        html: i18n.t('email.surveyWaiting.html'),
        text: i18n.t('email.surveyWaiting.text')
      },
      surveyCompleted: {
        subject: i18n.t('email.surveyCompleted.subject'),
        html: i18n.t('email.surveyCompleted.html'),
        text: i18n.t('email.surveyCompleted.text')
      },
      surveyRejected: {
        subject: i18n.t('email.surveyRejected.subject'),
        html: i18n.t('email.surveyRejected.html'),
        text: i18n.t('email.surveyRejected.text')
      }
    }
  },
  questions: [],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}
