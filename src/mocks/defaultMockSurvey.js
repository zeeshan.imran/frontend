import i18n from '../utils/internationalization/i18n'
import { getScreeningPromptText } from '../utils/getScreeningPromptText'
import { getScreeningRejectionText } from '../utils/getScreeningRejectionText'
import { getCustomSharingMessage } from '../utils/getCustomSharingMessage'
import { getLoginText } from '../utils/getLoginText'
import { getPauseText } from '../utils/getPauseText'

export default {
  editMode: 'default',
  products: [],
  basics: {
    name: null,
    coverPhoto: null,
    instructionsText: null,
    instructionSteps: [
      i18n.t('defaultValues.instructionSteps.0'),
      i18n.t('defaultValues.instructionSteps.1'),
      i18n.t('defaultValues.instructionSteps.2'),
      i18n.t('defaultValues.instructionSteps.3')
    ],
    thankYouText: null,
    rejectionText: getScreeningRejectionText(),
    screeningText: getScreeningPromptText(),
    uniqueName: null,
    authorizationType: 'public',
    exclusiveTasters: [],
    allowRetakes: false,
    isScreenerOnly: false,
    showGeneratePdf: false,
    optionDisplayType: 'labelOnly',
    productDisplayType: 'none',
    linkedSurveys: [],
    forcedAccount: false,
    forcedAccountLocation: 'start',
    referralAmount: 5,
    recaptcha: false,
    savedRewards: [],
    minimumProducts: 1,
    maximumProducts: null,
    surveyLanguage: 'en',
    country: 'US',
    customizeSharingMessage: getCustomSharingMessage(),
    loginText: getLoginText(),
    pauseText: getPauseText(),
    allowedDaysToFillTheTasting: 5,
    isPaypalSelected: false,
    isGiftCardSelected: false,
    customButtons: {
      continue: 'Continue',
      start: 'Start',
      next: 'Next',
      skip: 'Skip'
    },
    autoAdvanceSettings: {
      active: false,
      debounce: 0,
      hideNextButton: false
    },
    pdfFooterSettings: {
      active: false,
      footerNote: ''
    }
  },
  questions: [],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}
