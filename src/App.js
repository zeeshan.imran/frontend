import React, { Component } from 'react'
import { Redirect, Router, Switch, Route } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import { ApolloProvider as ApolloHooksProvider } from 'react-apollo-hooks'
import history from './history'
import bootstrapApollo from './config/ApolloClient'
import PageNotFound from './pages/PageNotFound'
import Onboarding from './pages/Onboarding'
import Survey from './pages/Survey'
import Taster from './pages/Taster'
import Operator from './pages/Operator'
import CompleteTasterProfile from './pages/CompleteTasterProfile'
import PrivateRoute from './components/PrivateRoute'
import TermsOfUsePage from './pages/TermsOfUse'
import PrivacyPolicyPage from './pages/PrivacyPolicy'
import UserProfile from './pages/UserProfile'
import './utils/internationalization/i18n'
import Impersonate from './containers/Impersonate'
import { SlaaskProvider } from './contexts/SlaaskContext'
import CreateTasterAccount from '../src/pages/CreateTasterAccount'
// import { hotjar } from 'react-hotjar';

import SurveyStatsPdf from './containers/SurveyStatsPdf'
import TasterPdf from './containers/TasterPdf'
import PreviewOperatorPdf from './pages/PreviewOperatorPdf'
import PreviewTasterPdf from './pages/PreviewTasterPdf'
import EditOperatorPdfLayout from './pages/EditOperatorPdfLayout'
import EditTasterPdfLayout from './pages/EditTasterPdfLayout'

const isProduct = window.location.host === 'app.flavorwiki.com'
class App extends Component {
  state = {
    client: null,
    isLoading: true
  }

  async componentDidMount () {
    const currentUrl = window.location.href.split('/')
    let usePersistentStorage = true
    if (currentUrl.includes('survey') && !currentUrl.includes('operator')) {
      usePersistentStorage = false

      // ENABLING HOTJAR
      // hotjar.initialize('1767607', '6');
    }

    const client = await bootstrapApollo(usePersistentStorage)

    this.setState({ client, isLoading: false })
  }

  logOutRedirect (e) {
    const historyArray = history.location.pathname.split('/')
    if (
      !e.storageArea[`flavorwiki-authenticated-org`] &&
      !historyArray.includes('onboarding') &&
      (historyArray.includes('operator') || historyArray.includes('taster'))
    ) {
      history.push(`/onboarding/login`)
    }
  }

  render () {
    window.addEventListener('storage', this.logOutRedirect)
    const { isLoading, client } = this.state
    const { REACT_APP_SLAASK } = process.env
    if (isLoading) return null

    return (
      <ApolloProvider client={client}>
        <ApolloHooksProvider client={client}>
          <SlaaskProvider flag={REACT_APP_SLAASK} history={history}>
            <Router history={history}>
              <Switch>
                <Redirect exact path='/' to='/onboarding' />
                <PrivateRoute
                  exact
                  authenticateTaster
                  path={`/taster/complete-profile`}
                  component={CompleteTasterProfile}
                />
                <Route path='/taster' component={Taster} />
                {/* TODO: disable STAGFW-342 */}
                {!isProduct && (
                  <Route path='/my-profile' component={UserProfile} />
                )}
                <Route path='/onboarding' component={Onboarding} />
                <Route path='/survey/:surveyId' component={Survey} />
                <Redirect exact path='/operator' to='/operator/dashboard' />
                <PrivateRoute
                  authenticateOperator
                  authenticateAnalytics
                  authenticatePowerUser
                  path='/operator'
                  component={Operator}
                />
                <Route
                  path={`/stats-pdf/:surveyId`}
                  component={SurveyStatsPdf}
                />
                <Route
                  path={`/edit-operator-pdf/:surveyId/:jobGroupId`}
                  component={EditOperatorPdfLayout}
                />
                <Route
                  path={`/edit-taster-pdf/:surveyId/:jobGroupId`}
                  component={EditTasterPdfLayout}
                />
                <Route
                  path={`/preview-operator-pdf/:surveyId/:jobGroupId`}
                  component={PreviewOperatorPdf}
                />
                <Route
                  path={`/preview-taster-pdf/:surveyId/:jobGroupId`}
                  component={PreviewTasterPdf}
                />
                <Route path='/taster-pdf/:surveyId' component={TasterPdf} />
                <Route path='/terms-of-use' component={TermsOfUsePage} />
                <Route path='/privacy-policy' component={PrivacyPolicyPage} />
                <Route path='/impersonate/exit' component={Impersonate} />
                <Route
                  path='/impersonate/:impersonateUserId'
                  component={Impersonate}
                />
                {/* Route For Taster Account Signup */}
                <Route
                  path='/create-account/taster'
                  component={CreateTasterAccount}
                />

                <Route component={PageNotFound} />
              </Switch>
            </Router>
          </SlaaskProvider>
        </ApolloHooksProvider>
      </ApolloProvider>
    )
  }
}
export default App
