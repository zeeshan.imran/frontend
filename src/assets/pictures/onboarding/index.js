let images = {}
if (process.env.REACT_APP_THEME) {
  images = require(`./${process.env.REACT_APP_THEME}Images`)
} else {
  images = require('./defaultImages')
}

module.exports = images
