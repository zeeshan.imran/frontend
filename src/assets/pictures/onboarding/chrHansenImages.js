const chrHansenImages = {
    1: require('./CH_Crumbled.png'),
    2: require('./CH_Emmentaler.png'),
    3: require('./CH_Mozzarella_Pizza.png'),
    4: require('./CH_Long_Gouda.png'),
    5: require('./CH_Feta.png'),
    6: require('./Feta_High.png')
  }
  
  module.exports = chrHansenImages
  