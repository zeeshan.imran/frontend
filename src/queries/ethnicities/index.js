import gql from 'graphql-tag'
import { ethnicityInfo } from '../../fragments/ethnicity'

const ETHNICITIES_QUERY = gql`
  {
    ethnicities {
      ...ethnicityInfo
    }
  }
  ${ethnicityInfo}
`

export default ETHNICITIES_QUERY
