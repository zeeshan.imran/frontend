import gql from 'graphql-tag'
import { surveyQuestionsBasicInfo } from '../../fragments/survey'

export const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyQuestionsBasicInfo
    }
  }
  ${surveyQuestionsBasicInfo}
`
