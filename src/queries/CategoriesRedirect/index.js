import gql from 'graphql-tag'

import { categoryCardInfo } from '../../fragments/category'

const CATEGORIES_QUERY = gql`
  query productCategories($key: String) {
    productCategories(rootOnly: false, key: $key) {
      ...categoryCardInfo
    }
  }
  ${categoryCardInfo}
`

export default CATEGORIES_QUERY
