import gql from 'graphql-tag'
import { surveyInfo } from '../../fragments/surveys'

export const PAID_SURVEY_QUERY = gql`
  {
    surveys(surveyType: PAID) {
      ...surveyInfo
    }
  }
  ${surveyInfo}
`
