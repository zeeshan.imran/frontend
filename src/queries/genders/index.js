import gql from 'graphql-tag'
import { genderInfo } from '../../fragments/gender'

const GENDERS_QUERY = gql`
  {
    genders {
      ...genderInfo
    }
  }
  ${genderInfo}
`

export default GENDERS_QUERY
