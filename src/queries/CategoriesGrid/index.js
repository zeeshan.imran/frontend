import gql from 'graphql-tag'
import { categoryCardInfo } from '../../fragments/category'

export const ROOT_CATEGORIES_QUERY = gql`
  {
    productCategories(rootOnly: true, key: "products") {
      ...categoryCardInfo
    }
  }
  ${categoryCardInfo}
`
