import gql from 'graphql-tag'

const SURVEY_PARTICIPATION_QUERY = gql`
  query {
    currentSurveyParticipation @client {
      surveyId
      surveyEnrollmentId
      state
      selectedProduct
      selectedProducts
      paypalEmail
      currentSurveyStep
      currentSurveySection
      isCurrentAnswerValid
      canSkipCurrentQuestion
      answers
      lastAnsweredQuestion
      savedRewards
      email
      productDisplayOrder
      productDisplayType
    }
  }
`

export default SURVEY_PARTICIPATION_QUERY
