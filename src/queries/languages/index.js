import gql from 'graphql-tag'
import { languageInfo } from '../../fragments/language'

const LANGUAGES_QUERY = gql`
  {
    languages {
      ...languageInfo
    }
  }
  ${languageInfo}
`

export default LANGUAGES_QUERY
