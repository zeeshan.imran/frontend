import gql from 'graphql-tag'
import { categoryInfo } from '../../fragments/category'

const PRODUCT_CATEGORIES_QUERY = gql`
  {
    productCategories(rootOnly: false) {
      ...categoryInfo
    }
  }
  ${categoryInfo}
`

export default PRODUCT_CATEGORIES_QUERY
