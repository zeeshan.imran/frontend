import gql from 'graphql-tag'

const SIGN_UP_FORM_QUERY = gql`
  {
    signUpForm @client {
      firstStepValid
      secondStepValid
      lastStepValid
      userId
      email
      password
      firstname
      lastname
      birthday
      gender
      language
      ethnicity
      currency
      incomeRange
      hasChildren
      numberOfChildren
      childrenAges
      smokerKind
      productTypes
      dietaryRestrictions
      recaptchaResponseToken
    }
  }
`

export default SIGN_UP_FORM_QUERY
