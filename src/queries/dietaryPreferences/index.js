import gql from 'graphql-tag'
import { dietaryPreferenceInfo } from '../../fragments/dietaryPreference'

const DIETARY_PREFERENCES_QUERY = gql`
  {
    dietaryPreferences {
      ...dietaryPreferenceInfo
    }
  }
  ${dietaryPreferenceInfo}
`

export default DIETARY_PREFERENCES_QUERY
