import gql from 'graphql-tag'
import { currentUserInfo } from '../../fragments/currentUser'

export const CURRENT_USER_QUERY = gql`
  {
    currentUser {
      ...currentUserInfo
    }
  }
  ${currentUserInfo}
`
