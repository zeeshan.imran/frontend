import gql from 'graphql-tag'
import { userSubscribedSurveysInfo } from '../../fragments/currentUser'

export const ONGOING_SURVEYS_QUERY = gql`
  {
    currentUser {
      ...userSubscribedSurveysInfo
    }
  }
  ${userSubscribedSurveysInfo}
`
