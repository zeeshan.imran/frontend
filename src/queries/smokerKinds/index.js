import gql from 'graphql-tag'
import { smokerKindInfo } from '../../fragments/smokerKind'

const SMOKER_KINDS_QUERY = gql`
  {
    smokerKinds {
      ...smokerKindInfo
    }
  }
  ${smokerKindInfo}
`

export default SMOKER_KINDS_QUERY
