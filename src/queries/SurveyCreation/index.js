import gql from 'graphql-tag'

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
      basics {
        name
        coverPhoto
        instructionsText
        instructionSteps
        thankYouText
        rejectionText
        screeningText
        uniqueName
        authorizationType
        exclusiveTasters
        allowRetakes
        forcedAccount
        forcedAccountLocation
        tastingNotes {
          tastingId
          tastingLeader
          customer
          country
          dateOfTasting
          otherInfo
        }
        enabledEmailTypes
        referralAmount
        savedRewards
        recaptcha
        minimumProducts
        maximumProducts
        surveyLanguage
        country
        customizeSharingMessage
        loginText
        pauseText
        isScreenerOnly
        productDisplayType
        showGeneratePdf
        linkedSurveys
        sharingButtons
        validatedData
        showOnTasterDashboard
        pdfFooterSettings {
          active
          footerNote
        }
        autoAdvanceSettings {
          active
          debounce
          hideNextButton
        }
        emails {
          surveyWaiting {
            subject
            html
            text
          }
          surveyCompleted {
            subject
            html
            text
          }
          surveyRejected {
            subject
            html
            text
          }
        }
        maxProductStatCount
        allowedDaysToFillTheTasting
        isPaypalSelected
        isGiftCardSelected
        customButtons {
          continue
          start
          next
          skip
        }
      }
      questions
      mandatoryQuestions
      uniqueQuestionsToCreate
      editMode
    }
  }
`
