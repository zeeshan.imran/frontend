import gql from 'graphql-tag'
import { surveyInfo } from '../../fragments/surveys'

export const POINTS_SURVEY_QUERY = gql`
  {
    surveys(surveyType: POINTS) {
      ...surveyInfo
    }
  }
  ${surveyInfo}
`
