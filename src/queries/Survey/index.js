import gql from 'graphql-tag'
import { surveyInfo } from '../../fragments/survey'

export const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyInfo
    }
  }
  ${surveyInfo}
`
