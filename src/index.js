import redirectQrCode from './QrRedirect'

(async () => {
  const isRedirect = await redirectQrCode()
  if (!isRedirect) {
    import('./bootstrap')
  }
})()