import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { questionInfo } from '../../fragments/survey'

const useAllSurveyQuestions = (survey = {}) => {
  const { screeningQuestions = [] } = survey
  const { setupQuestions = [] } = survey
  const { productsQuestions = [] } = survey
  const { finishingQuestions = [] } = survey
  const { paymentQuestions = [] } = survey
  const allQuestions = [
    ...paymentQuestions,
    ...screeningQuestions,
    ...setupQuestions,
    ...productsQuestions,
    ...finishingQuestions
  ]

  const { data = {}, loading } = useQuery(GET_QUESTIONS, {
    variables: { ids: allQuestions.map(({ id }) => id) },
    fetchPolicy: 'cache-and-network'
  })

  return { data, loading }
}

const GET_QUESTIONS = gql`
  query questions($ids: [ID]) {
    questions(ids: $ids) {
      ...questionInfo
    }
  }

  ${questionInfo}
`

export default useAllSurveyQuestions
