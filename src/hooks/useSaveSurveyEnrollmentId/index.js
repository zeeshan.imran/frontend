import { useCallback } from 'react'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'

const useSaveSurveyEnrollmentId = () => {
  const saveSurveyEnrollmentId = useMutation(SAVE_SURVEY_ENROLLMENT)

  return useCallback(async surveyEnrollmentId => {
    await saveSurveyEnrollmentId({
      variables: { surveyEnrollmentId }
    })
  })
}

export const SAVE_SURVEY_ENROLLMENT = gql`
  mutation saveSurveyEnrollmentId($surveyEnrollmentId: ID) {
    saveCurrentSurveyParticipation(surveyEnrollmentId: $surveyEnrollmentId)
      @client
  }
`

export default useSaveSurveyEnrollmentId
