import React from 'react'
import { create } from 'react-test-renderer'
import { ApolloProvider } from 'react-apollo-hooks'
import defaults from '../../defaults'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import useSaveSurveyEnrollmentId, { SAVE_SURVEY_ENROLLMENT } from '.'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import wait from '../../utils/testUtils/waait'
import { defaultSurvey } from '../../mocks'

const TestHook = ({ callback }) => {
  callback()
  return null
}
const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    name: 'Survey 1',
    uniqueName: 'survey-1',
  },
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ]
}

describe('option to have only 1 product without the choose product page', () => {
  let testRender
  let client
  let callSetSelectedProductsMutation = jest.fn()

  beforeEach(() => {
    client = createApolloMockClient()

    client.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-1',
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: []
        }
      }
    })

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    client.cache.writeQuery({
      query: SAVE_SURVEY_ENROLLMENT,
      data: {
        saveCurrentSurveyParticipation: {
          ...defaults.saveCurrentSurveyParticipation,

          surveyEnrollmentId: 'survey-enrollment-1',
          state: 'active',
          paypalEmail: '',
          selectedProduct: null,
          selectedProducts: [],
          currentSurveyStep: null,
          currentSurveySection: null,
          isCurrentAnswerValid: true,
          canSkipCurrentQuestion: false,
          currentAnswerSkipTarget: null,
          answers: [],
          lastAnsweredQuestion: null
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should call setSelectedProducts on server side', async () => {
    testRender = create(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            const saveSurveyEnrollmentId = useSaveSurveyEnrollmentId()
            await saveSurveyEnrollmentId({
              surveyEnrollmentId: 'survey-enrollment-1'
            })
          }}
        />
      </ApolloProvider>
    )

    await wait(0)

    expect(callSetSelectedProductsMutation).toHaveBeenCalledTimes(0)
  })
})
