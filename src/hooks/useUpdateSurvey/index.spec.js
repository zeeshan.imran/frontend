// import React from 'react'
// import { create } from 'react-test-renderer'
// import { ApolloProvider } from 'react-apollo-hooks'
// import sinon from 'sinon'
// import useUpdateSurvey, {
//   trimFields,
//   UPDATE_SURVEY_PRODUCTS,
//   RESET_CREATION_FORM,
//   UPDATE_SURVEY_BASICS
// } from './index'
// import { SURVEY_CREATION } from '../../queries/SurveyCreation'
// import { createApolloMockClient } from '../../utils/createApolloMockClient'
// import wait from '../../utils/testUtils/waait'

// const mockSurveyCreation = {
//   surveyId: 'survey-1',
//   basics: {
//     name: null,
//     instructionsText: null,
//     instructionSteps: [
//       'Please be sure you have all of your selected products before starting the tasting.',
//       'Find a quiet place.',
//       'You will be given additional instructions during the tasting.',
//       'When you are ready, click ‘Start Tasting’ below.'
//     ],
//     thankYouText: null,
//     rejectionText: null,
//     screeningText: null,
//     uniqueName: null,
//     authorizationType: 'public',
//     exclusiveTasters: [],
//     allowRetakes: false,
//     forcedAccount: false,
//     forcedAccountLocation: `start`,
//     recaptcha: false,
//     minimumProducts: 1,
//     maximumProducts: 1,
//     surveyLanguage: 'en',
//     customButtons: {
//       continue: 'Continue',
//       start: 'Start',
//       next: 'Next',
//       skip: 'Skip'
//     }
//   },
//   products: [],
//   questions: [],
//   mandatoryQuestions: [],
//   uniqueQuestionsToCreate: []
// }

// const TestHook = ({ callback }) => {
//   callback()
//   return null
// }

describe('advanceInSurveyResolver', () => {
  test('TODO: update this test', () => {})
  // let spyReset
  // let testRender
  // let client

  // beforeEach(() => {
  //   client = createApolloMockClient()
  //   client.cache.writeQuery({
  //     query: SURVEY_CREATION,
  //     data: {
  //       surveyCreation: mockSurveyCreation
  //     }
  //   })

  //   const stub = sinon.stub(client, 'mutate')

  //   stub
  //     .withArgs(sinon.match({ mutation: UPDATE_SURVEY_PRODUCTS }))
  //     .returns({ data: {} })

  //   stub
  //     .withArgs(sinon.match({ mutation: UPDATE_SURVEY_BASICS }))
  //     .returns({ data: {} })

  //   spyReset = jest.fn()
  //   stub
  //     .withArgs(sinon.match({ mutation: RESET_CREATION_FORM }))
  //     .callsFake(spyReset)

  //   stub.callThrough()
  // })

  // test('should NOT resetCreationForm if updateSurvey(false) is called', async () => {
  //   testRender = create(
  //     <ApolloProvider client={client}>
  //       <TestHook
  //         callback={() => {
  //           const updateSurvey = useUpdateSurvey({
  //             surveyId: 'survey-1',
  //             successMessage: '',
  //             errorMessage: ''
  //           })

  //           expect(updateSurvey(false)).resolves.toBeUndefined()
  //         }}
  //       />
  //     </ApolloProvider>
  //   )

  //   await wait(0)
  //   expect(spyReset).not.toHaveBeenCalled()
  //   testRender.unmount()
  // })
  // test('should resetCreationForm if updateSurvey() is called', async () => {
  //   testRender = create(
  //     <ApolloProvider client={client}>
  //       <TestHook
  //         callback={() => {
  //           const updateSurvey = useUpdateSurvey({
  //             surveyId: 'survey-1',
  //             successMessage: '',
  //             errorMessage: ''
  //           })

  //           expect(updateSurvey()).resolves.toBeUndefined()
  //         }}
  //       />
  //     </ApolloProvider>
  //   )

  //   await wait(0)
  //   expect(spyReset).toHaveBeenCalled()
  //   testRender.unmount()
  // })

  // test(`test trimFields function`, () => {
  //   const data = [
  //     {
  //       var7: '   var7    ',
  //       var8: '     var8     ',
  //       var9: '           '
  //     },
  //     [
  //       {
  //         var5: '    var        5    ',
  //         var6: 'var      6'
  //       },
  //       '    var3',
  //       'var4     '
  //     ],
  //     '      var1     ',
  //     '     var2    '
  //   ]
  //   const expectedData = [
  //     {
  //       var7: 'var7',
  //       var8: 'var8',
  //       var9: ''
  //     },
  //     [
  //       {
  //         var5: 'var 5',
  //         var6: 'var 6'
  //       },
  //       'var3',
  //       'var4'
  //     ],
  //     'var1',
  //     'var2'
  //   ]
  //   const formatedData = trimFields(data)
  //   expect(formatedData).toEqual(expectedData)
  // })

  // test(`test trimFields function`, () => {
  //   const data = [
  //     {
  //       var7: '   var7    ',
  //       var8: '     var8     ',
  //       var9: '           '
  //     },
  //     [
  //       {
  //         var5: '    var        5    ',
  //         var6: 'var      6'
  //       },
  //       '    var3',
  //       'var4     '
  //     ],
  //     '      var1     ',
  //     '     var2    '
  //   ]
  //   const expectedData = [
  //     {
  //       var7: 'var7',
  //       var8: 'var8',
  //       var9: ''
  //     },
  //     [
  //       {
  //         var5: 'var 5',
  //         var6: 'var 6'
  //       },
  //       'var3',
  //       'var4'
  //     ],
  //     'var1',
  //     'var2'
  //   ]
  //   const formatedData = trimFields(data)
  //   expect(formatedData).toEqual(expectedData)
  // })
})
