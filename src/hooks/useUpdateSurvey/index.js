import { useQuery, useMutation } from 'react-apollo-hooks'
import { generate } from 'shortid'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import gql from 'graphql-tag'
import { pipe, map, flatten } from 'ramda'
import formatBasicsForUpdating from '../../utils/formatBasicsForPublishing'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import formatProductsForPublishing from '../../utils/formatProductsForPublishing'
import formatQuestionsForUpdating from '../../utils/formatQuestionsForUpdating'
import sortQuestionsForPublishing from '../../utils/sortQuestionsForPublishing'
import { questionInfo } from '../../fragments/survey'
import { trimSpaces } from '../../utils/trimSpaces'
import logger from '../../utils/logger'

export const trimFields = field => {
  if (!field) {
    return field
  }
  switch (field.constructor) {
    case Object: {
      let newField = {}
      for (let subfield of Object.keys(field)) {
        Object.assign(newField, {
          [subfield]: trimFields(field[subfield])
        })
      }
      return newField
    }
    case Array: {
      let newField = []
      for (let subfield of field) {
        newField = [...newField, trimFields(subfield)]
      }
      return newField
    }
    case Number:
      return field
    case String:
      return trimSpaces(field)
    default:
      return field
  }
}

const isPairedQuestionFollowedByProfile = question =>
  question.type === 'paired-questions' &&
  question.pairsOptions &&
  question.pairsOptions.hasFollowUpProfile

const isSliderQuestionFollowedByProfile = question =>
  question.type === 'slider' &&
  question.sliderOptions &&
  question.sliderOptions.hasFollowUpProfile

const useUpdateSurvey = ({ surveyId, successMessage, errorMessage }) => {
  const {
    data: {
      surveyCreation: { products, basics, questions, mandatoryQuestions }
    }
  } = useQuery(SURVEY_CREATION)

  const updateSurvey = useMutation(UPDATE_SURVEY)
  const reloadSurveyProductOnForm = useMutation(RELOAD_SURVEY_PRODUCT_ON_FORM)
  const reloadSurveyQuestionOnForm = useMutation(RELOAD_SURVEY_QUESTION_ON_FORM)

  const resetCreationForm = useMutation(RESET_CREATION_FORM)

  return async (clearAfterSave = true) => {
    try {
      const formattedProducts = formatProductsForPublishing(products)

      // CREATE OR UPDATE QUESTIONS
      const allQuestions = sortQuestionsForPublishing(
        questions,
        mandatoryQuestions
      )
      const formattedQuestions = formatQuestionsForUpdating({
        questions: allQuestions,
        surveyId
      })

      const updateQuestions = pipe(
        map(question => {
          const questionId = question.id || question.clientGeneratedId
          if (isPairedQuestionFollowedByProfile(question)) {
            // return two questions on paired, then flatten later
            return [
              question,
              {
                clientGeneratedId: generate(),
                ...question.pairsOptions.profileQuestion,
                relatedQuestions: [questionId],
                id: undefined
              }
            ]
          } else if (isSliderQuestionFollowedByProfile(question)) {
            // return two questions on slider, then flatten later
            return [
              question,
              {
                clientGeneratedId: generate(),
                ...question.sliderOptions.profileQuestion,
                relatedQuestions: [questionId],
                id: undefined
              }
            ]
          }

          return [question]
        }),
        flatten
      )(formattedQuestions)

      // UPDATE SURVEY BASICS
      const formattedBasics = formatBasicsForUpdating({ basics, products })
      const updateSurveyInput = {
        basics: {
          ...trimFields({
            id: surveyId,
            ...formattedBasics
          }),
          // don't trim emails
          emails: formattedBasics.emails
        },
        products: trimFields(formattedProducts),
        questions: trimFields(updateQuestions)
      }
      logger.setContext('updateSurveyInput', updateSurveyInput)

      const {
        data: {
          updateSurvey: { replacedQuestions, replacedProducts }
        }
      } = await updateSurvey({
        variables: {
          input: updateSurveyInput
        }
      })

      for (const { clientGeneratedId, product } of replacedProducts) {
        // NOTE: STAGFW-443
        await reloadSurveyProductOnForm({
          variables: { clientGeneratedId, product }
        })
      }

      for (const { clientGeneratedId, question } of replacedQuestions) {
        await reloadSurveyQuestionOnForm({
          variables: { clientGeneratedId, question }
        })
      }

      displaySuccessMessage(successMessage)
      if (clearAfterSave) {
        resetCreationForm()
      }
      logger.clearContext()
    } catch (error) {
      logger.error(error)
      if (
        error.graphQLErrors &&
        error.graphQLErrors.length > 0 &&
        error.graphQLErrors[0].message === 'E_UNIQUE'
      ) {
        throw new Error(
          'That "Unique name" is already being used by some other survey'
        )
      } else {
        throw new Error(errorMessage)
      }
    }
  }
}

export const RELOAD_SURVEY_PRODUCT_ON_FORM = gql`
  mutation($clientGeneratedId: ID, $product: Object) {
    reloadSurveyProductOnForm(
      clientGeneratedId: $clientGeneratedId
      product: $product
    ) @client
  }
`

export const RELOAD_SURVEY_QUESTION_ON_FORM = gql`
  mutation($clientGeneratedId: ID, $question: Object) {
    reloadSurveyQuestionOnForm(
      clientGeneratedId: $clientGeneratedId
      question: $question
    ) @client
  }
`

export const UPDATE_SURVEY = gql`
  mutation updateSurvey($input: UpdateSurveyInput!) {
    updateSurvey(input: $input) {
      survey {
        id
      }
      replacedQuestions {
        clientGeneratedId
        question {
          ...questionInfo
        }
      }
      replacedProducts {
        clientGeneratedId
        product {
          id
          name
          brand
          photo
          isSurveyCover
          sortingOrderId
        }
      }
    }
  }

  ${questionInfo}
`

export const RESET_CREATION_FORM = gql`
  mutation resetCreationForm {
    resetCreationForm @client
  }
`

export default useUpdateSurvey
