import { useQuery } from 'react-apollo-hooks'
import { SURVEY_QUERY } from '../../queries/Survey'
import { useTranslation } from 'react-i18next'

const useCustomButton = (surveyId, buttonName) => {
  const { t } = useTranslation()
  const {
    data: { survey = {} },
    loading,
    error
  } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId },
    fetchPolicy: 'cache-and-network'
  })
  const isButtonCustomized =
    survey.customButtons && survey.customButtons[buttonName]

  let buttonLabel = t(`defaultValues.customButtons.${buttonName}`)
  if (!loading && isButtonCustomized) {
    buttonLabel = survey.customButtons[buttonName]
  }

  return { buttonLabel, loading, error }
}

export default useCustomButton
