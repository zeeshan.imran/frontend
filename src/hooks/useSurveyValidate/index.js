import { useCallback } from 'react'
import { useApolloClient } from 'react-apollo-hooks'
import { all, concat } from 'ramda'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'
import {
  validateProductSchema,
  validateQuestionSchemas,
  validateSurveyBasics
} from '../../validates'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { useTranslation } from 'react-i18next'
import getChartTypes from '../../utils/getChartTypes'

const isProductValid = validateProductSchema.isValidSync.bind(
  validateProductSchema
)

const useSurveyValidate = () => {
  const { t } = useTranslation()
  const apollo = useApolloClient()

  const validateBasics = useCallback(async () => {
    const {
      data: {
        surveyCreation: { basics }
      }
    } = await apollo.query({
      query: SURVEY_CREATION,
      fetchPolicy: 'cache-only'
    })

    return validateSurveyBasics(true).isValidSync(basics)
  }, [apollo])

  const validateProducts = useCallback(async () => {
    const {
      data: {
        surveyCreation: {
          products,
          basics: { isScreenerOnly }
        }
      }
    } = await apollo.query({
      query: SURVEY_CREATION,
      fetchPolicy: 'cache-only'
    })
    
    return all(isProductValid, products, !isScreenerOnly)
  }, [apollo])

  const validateQuestions = useCallback(
    async (fromPublish = false) => {
      const {
        data: {
          surveyCreation: { products, questions, mandatoryQuestions }
        }
      } = await apollo.query({ query: SURVEY_CREATION })

      const allQuestions = concat(questions, mandatoryQuestions)

      for (let question of allQuestions) {
        const validateQuestionSchema = validateQuestionSchemas[question.type]

        if (!question.type) {
          return { isValid: false, invalidQuestion: question }
        }

        if (validateQuestionSchema) {
          const chartTypes = getChartTypes(question)
          try {
            await validateQuestionSchema.validate(question, {
              context: { products, chartTypes }
            })
          } catch (e) {
            return { isValid: false, invalidQuestion: question }
          }
        }
      }

      if (fromPublish && (!questions || !questions.length)) {
        return { isValid: false, noQuestionError: true }
      }

      return { isValid: true }
    },
    [apollo]
  )

  const showProductsErrors = () => {
    displayErrorPopup(t('containers.useSurveyValidate.showProductsErrors'))
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_PRODUCTS)
  }

  const showQuestionsErrors = () => {
    displayErrorPopup(t('containers.useSurveyValidate.showQuestionsErrors'))
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_QUESTIONS)
  }

  const showOnlyMandatoryQuestionErrors = () => {
    displayErrorPopup(
      t('containers.useSurveyValidate.showOnlyMandatoryQuestionErrors')
    )
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_QUESTIONS)
  }

  return {
    validateBasics,
    validateProducts,
    validateQuestions,
    showProductsErrors,
    showQuestionsErrors,
    showOnlyMandatoryQuestionErrors
  }
}

export default useSurveyValidate
