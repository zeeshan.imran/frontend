import React from 'react'
import { create } from 'react-test-renderer'
import { ApolloProvider } from 'react-apollo-hooks'
import defaults from '../../defaults'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import useSelectSingleProduct from '.'
import gql from 'graphql-tag'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import wait from '../../utils/testUtils/waait'

const TestHook = ({ callback }) => {
  callback()
  return null
}

describe('option to have only 1 product without the choose product page', () => {
  let testRender
  let client
  let callSetSelectedProductsMutation = jest.fn()

  const mockSurveyCreation = {
    ...defaults.surveyCreation,
    products: [],
    questions: [],
    mandatoryQuestions: [],
    uniqueQuestionsToCreate: []
  }

  const mockSetSelectedProductsMutation = {
    request: {
      query: gql`
        mutation setSelectedProducts(
          $surveyEnrollmentId: ID!
          $selectedProduct: ID!
        ) {
          setSelectedProducts(
            surveyEnrollment: $surveyEnrollmentId
            selectedProducts: [$selectedProduct]
          ) {
            id
            selectedProducts {
              id
            }
          }
        }
      `,
      variables: {
        surveyEnrollmentId: 'survey-enrollment-1',
        selectedProduct: 'product-1'
      }
    },
    result: () => {
      callSetSelectedProductsMutation()

      return {
        data: {
          setSelectedProducts: {
            id: 'survey-enrollment-1',
            selectedProducts: { id: 'product-1' }
          }
        }
      }
    }
  }

  beforeEach(() => {
    client = createApolloMockClient({
      mocks: [mockSetSelectedProductsMutation]
    })

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    client.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-1',
          surveyEnrollmentId: 'survey-enrollment-1',
          state: 'active',
          paypalEmail: '',
          selectedProduct: null,
          selectedProducts: [],
          currentSurveyStep: null,
          currentSurveySection: null,
          isCurrentAnswerValid: true,
          canSkipCurrentQuestion: false,
          currentAnswerSkipTarget: null,
          answers: [],
          savedRewards: [],
          country: 'United States of America',
          lastAnsweredQuestion: null
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should call setSelectedProducts on server side', async () => {
    testRender = create(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            const selectSingleProduct = useSelectSingleProduct()
            await selectSingleProduct({
              surveyEnrollmentId: 'survey-enrollment-1',
              productId: 'product-1'
            })
          }}
        />
      </ApolloProvider>
    )

    await wait(0)

    expect(callSetSelectedProductsMutation).toHaveBeenCalledTimes(1)
  })

  test('should automatic choose products for currentSurveyParticipation', async () => {
    testRender = create(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            const selectSingleProduct = useSelectSingleProduct()
            await selectSingleProduct({
              surveyEnrollmentId: 'survey-enrollment-1',
              productId: 'product-1'
            })
          }}
        />
      </ApolloProvider>
    )

    await wait(0)

    const {
      currentSurveyParticipation: surveyParticipation
    } = client.cache.readQuery({ query: SURVEY_PARTICIPATION_QUERY }, false)

    expect(surveyParticipation).toHaveProperty('selectedProduct', 'product-1')

    expect(surveyParticipation).toHaveProperty('selectedProducts', [
      'product-1'
    ])
  })
})
