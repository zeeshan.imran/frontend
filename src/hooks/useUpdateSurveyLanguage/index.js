import { useTranslation } from 'react-i18next'

const useUpdateLocale = () => {
  const { i18n } = useTranslation()
  return language => {
    const languageCode = language || 'en'

    if (i18n.language !== languageCode) {
      i18n.changeLanguage(languageCode)
    }

    window.recaptchaOptions = {
      lang: languageCode,
      useRecaptchaNet: true,
      removeOnUnmount: true
    }
  }
}

export default useUpdateLocale
