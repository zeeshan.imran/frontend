import { useRef, useEffect } from 'react'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'

const useQuestionValidate = () => {
  const formRef = useRef()
  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.VALIDATE_SURVEY_QUESTIONS, () => {
      if (formRef.current) {
        formRef.current.validateForm()
      }
    })
    return () => {
      PubSub.unsubscribe(token)
    }
  }, [])

  return formRef
}

export default useQuestionValidate
