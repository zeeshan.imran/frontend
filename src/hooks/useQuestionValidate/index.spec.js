import React from 'react'
import { mount } from 'enzyme'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'
import useQuestionValidate from './index'
import { Formik } from 'formik'
import wait from '../../utils/testUtils/waait'
describe('useQuestionValidate', () => {
  test('useQuestionValidate hooks', async () => {
    // mock handle validation
    const handleValidate = jest.fn()
    const TestComponent = () => {
      const formRef = useQuestionValidate()
      return (
        <Formik
          validate={handleValidate}
          ref={formRef}
          initialValues={{}}
          render={() => <div>Empty form</div>}
        />
      )
    }
    const render = mount(<TestComponent />)
    // wait until useEffect finishes
    await wait(0)
    // handle is not called
    expect(handleValidate).not.toHaveBeenCalled()
    // trigger the survey validation process
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_QUESTIONS)
    expect(render).toHaveLength(1)
    // wait until trigger is subscribe
    // NOTED: without wait, unsubscribe can be called before receiving the trigger
    await wait(0)
    // handle is called after trigger the survey validation
    expect(handleValidate).toHaveBeenCalled()
    // unmount: cause clean up useEffect (PubSub.unsubscribe)
    render.unmount()
  })
})