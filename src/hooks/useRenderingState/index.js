import { useState, useCallback, useMemo, useRef } from 'react'

const useRenderingState = () => {
  const [rendering, setRendering] = useState(true)
  const timer = useRef(null)
  const showRendering = useCallback(totalMs => {
    setRendering(true)
    
    clearTimeout(timer.current)
    
    timer.current = setTimeout(() => {
      setRendering(false)
    }, totalMs)
  }, [])

  return useMemo(
    () => ({
      rendering,
      showRendering
    }),
    [rendering, showRendering]
  )
}

export default useRenderingState