import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { surveyBasicInfo } from '../../fragments/survey'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'

const useSurveys = ({ page = 0, state, keyword }) => {
  const skip = page * DEFAULT_N_ELEMENTS_PER_PAGE
  const { data: surveysData = {}, loading: surveysLoading } = useQuery(
    SURVEYS,
    {
      variables: { input: { skip, state, keyword } },
      fetchPolicy: 'cache-and-network'
    }
  )
  const { data: totalData = {}, loading: totalLoading } = useQuery(
    SURVEYS_TOTAL,
    {
      variables: { input: { state, keyword } },
      fetchPolicy: 'cache-and-network'
    }
  )

  return {
    surveys: surveysData.surveys,
    total: totalData.surveysTotal,
    loading: surveysLoading || totalLoading
  }
}

const SURVEYS_TOTAL = gql`
  query surveysTotal($input: CountSurveysInput!) {
    surveysTotal(input: $input)
  }
`

const SURVEYS = gql`
  query surveys($input: SurveysInput!) {
    surveys(input: $input) {
      ...surveyBasicInfo
      screeners {
        id
        uniqueName
        referralAmount
        isPaypalSelected
        isGiftCardSelected
      }
    }
  }

  ${surveyBasicInfo}
`

export default useSurveys
