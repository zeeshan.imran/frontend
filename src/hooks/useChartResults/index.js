import { path } from 'ramda'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { useMemo } from 'react'

const getChartSettings = path(['getChartsSettings', 'chartsSettings'])
const getOperatorLayout = path(['getChartsSettings', 'operatorPdfLayout'])
const getTasterLayout = path(['getChartsSettings', 'tasterPdfLayout'])

const useChartResults = ({ surveyId, jobGroupId }) => {
  const { data, loading, refetch: reloadChartResults } = useQuery(QUERY_STATS_RESULTS, {
    variables: {
      surveyId,
      jobGroupId
    }
  })

  return {
    loading,
    reloadChartResults,
    operatorLayout: useMemo(() => getOperatorLayout(data), [data]),
    tasterLayout: useMemo(() => getTasterLayout(data), [data]),
    settings: useMemo(() => getChartSettings(data) || {}, [data]),
    stats: useMemo(() => data.getStatsResults || [], [data])
  }
}

const QUERY_STATS_RESULTS = gql`
  query getStatsResults($surveyId: ID!, $jobGroupId: ID!) {
    getChartsSettings(surveyId: $surveyId)
    getStatsResults(jobGroupId: $jobGroupId)
  }
`

export default useChartResults
