import { useCallback } from 'react'
import { useApolloClient } from 'react-apollo-hooks'
import { CachePersistor } from 'apollo-cache-persist'
import defaults from '../../defaults'

const usePurgeCache = () => {
  const client = useApolloClient()

  return useCallback(
    async () => {
      const persistor = new CachePersistor({
        key: process.env.REACT_APP_FLAVORWIKI_APOLLO_CACHE,
        cache: client.cache,
        storage: window.localStorage
      })

      client.cache.reset()

      client.cache.writeData({
        data: {
          ...defaults
        }
      })

      await persistor.purge()
    },
    [client]
  )
}

export default usePurgeCache
