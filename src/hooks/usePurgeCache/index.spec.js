import React from 'react'
import { mount } from 'enzyme'
import wait from '../../utils/testUtils/waait'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import defaults from '../../defaults'
import { ApolloProvider } from 'react-apollo-hooks'
import usePurgeCache from './index'

describe('usePurgeCache', () => {
  let render

  afterEach(() => {
    render.unmount()
  })

  test('should clear cache', async () => {
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: jest.fn(),
        setItem: jest.fn(),
        removeItem: jest.fn(),
        clear: jest.fn()
      }
    })
    const client = createApolloMockClient()
    // create dirty cache
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          ...defaults.surveyCreation,
          questions: [
            {
              id: 'question-1',
              type: 'dropdown'
            }
          ]
        }
      }
    })

    // make sure the cache is dirty
    const data = client.cache.readQuery({ query: SURVEY_CREATION })
    expect(data).not.toBeNull()
    expect(data.surveyCreation.questions).toHaveLength(1)

    const TextComponent = () => {
      const purgeCache = usePurgeCache()
      purgeCache()
      return null
    }

    render = mount(
      <ApolloProvider client={client}>
        <TextComponent />
      </ApolloProvider>
    )

    await wait(1000)

    // make sure the cache was cleaned
    const cleanedCache = client.cache.readQuery({ query: SURVEY_CREATION })
    expect(cleanedCache).not.toBeNull()
    expect(cleanedCache.surveyCreation.questions).toHaveLength(0)
    // make sure CachePersistor will remove the persistent cache
    expect(window.localStorage.removeItem).toHaveBeenCalledWith(
      process.env.REACT_APP_FLAVORWIKI_APOLLO_CACHE
    )
  })
})
