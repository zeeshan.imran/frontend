// import React from 'react'
// import { create } from 'react-test-renderer'
// import { ApolloProvider } from 'react-apollo-hooks'
// import sinon from 'sinon'
// import usePublishSurvey, {
//   UPDATE_SURVEY_PRODUCTS,
//   RESET_CREATION_FORM,
//   UPDATE_SURVEY_BASICS,
//   CREATE_PRODUCTS,
//   CREATE_SURVEY,
//   CREATE_QUESTION,
//   LINK_TO_QUESTION
// } from './index'
// import { trimFields } from '.'
// import { SURVEY_CREATION } from '../../queries/SurveyCreation'
// import { createApolloMockClient } from '../../utils/createApolloMockClient'
// import wait from '../../utils/testUtils/waait'

// const mockSurveyCreation = {
//   surveyId: 'survey-1',
//   basics: {
//     name: null,
//     instructionsText: null,
//     instructionSteps: [
//       'Please be sure you have all of your selected products before starting the tasting.',
//       'Find a quiet place.',
//       'You will be given additional instructions during the tasting.',
//       'When you are ready, click ‘Start Tasting’ below.'
//     ],
//     thankYouText: null,
//     rejectionText: null,
//     screeningText: null,
//     uniqueName: null,
//     authorizationType: 'public',
//     exclusiveTasters: [],
//     recaptcha: false,
//     emailService: false,
//     minimumProducts: 1,
//     maximumProducts: 1,
//     surveyLanguage: 'en',
//     customButtons: {
//       continue: 'Continue',
//       start: 'Start',
//       next: 'Next',
//       skip: 'Skip'
//     }
//   },
//   products: [],
//   questions: [],
//   mandatoryQuestions: [],
//   uniqueQuestionsToCreate: []
// }

// const TestHook = ({ callback }) => {
//   callback()
//   return null
// }

describe('advanceInSurveyResolver', () => {
  test('TODO: update this test', () => {})
  // let spyReset
  // let testRender
  // let client

  // beforeAll(() => {
  //   const localStorageMock = (function () {
  //     let store = {}
  //     return {
  //       getItem: function (key) {
  //         return store[key] || null
  //       },
  //       setItem: function (key, value) {
  //         store[key] = value.toString()
  //       },
  //       removeItem: function (key) {
  //         delete store[key]
  //       },
  //       clear: function () {
  //         store = {}
  //       }
  //     }
  //   })()
  //   Object.defineProperty(window, 'localStorage', {
  //     value: localStorageMock
  //   })
  // })

  // beforeEach(() => {
  //   client = createApolloMockClient()
  //   client.cache.writeQuery({
  //     query: SURVEY_CREATION,
  //     data: {
  //       surveyCreation: mockSurveyCreation
  //     }
  //   })

  //   const stub = sinon.stub(client, 'mutate')

  //   stub
  //     .withArgs(sinon.match({ mutation: UPDATE_SURVEY_PRODUCTS }))
  //     .returns({ data: {} })
  //   stub
  //     .withArgs(sinon.match({ mutation: UPDATE_SURVEY_BASICS }))
  //     .returns({ data: {} })
  //   stub
  //     .withArgs(sinon.match({ mutation: CREATE_PRODUCTS }))
  //     .returns({ data: { createProducts: [] } })
  //   stub
  //     .withArgs(sinon.match({ mutation: CREATE_SURVEY }))
  //     .returns({ data: { createSurvey: { id: 'survey-1' } } })
  //   stub
  //     .withArgs(sinon.match({ mutation: CREATE_QUESTION }))
  //     .returns({ data: {} })
  //   stub
  //     .withArgs(sinon.match({ mutation: LINK_TO_QUESTION }))
  //     .returns({ data: {} })

  //   spyReset = jest.fn()
  //   stub
  //     .withArgs(sinon.match({ mutation: RESET_CREATION_FORM }))
  //     .callsFake(spyReset)

  //   stub.callThrough()
  // })

  // test('should resetCreationForm if publishSurvey() is called', async () => {
  //   testRender = create(
  //     <ApolloProvider client={client}>
  //       <TestHook
  //         callback={async () => {
  //             const publishSurvey = usePublishSurvey({
  //               surveyInitialState: 'draft',
  //               predecessorSurvey: 'survey-1',
  //               successMessage: '',
  //               errorMessage: ''
  //             })
  //             const result = await publishSurvey()
  //             expect(result).toBe('survey-1')
  //         }}
  //       />
  //     </ApolloProvider>
  //   )

  //   await wait(1000)

  //   expect(spyReset).toHaveBeenCalled()
  //   testRender.unmount()
  // })

  // test(`test trimFields function`, () => {
  //   const data = [
  //     {
  //       var7: '   var7    ',
  //       var8: '     var8     ',
  //       var9: '           '
  //     },
  //     [
  //       {
  //         var5: '    var        5    ',
  //         var6: 'var      6'
  //       },
  //       '    var3',
  //       'var4     '
  //     ],
  //     '      var1     ',
  //     '     var2    '
  //   ]
  //   const expectedData = [
  //     {
  //       var7: 'var7',
  //       var8: 'var8',
  //       var9: ''
  //     },
  //     [
  //       {
  //         var5: 'var 5',
  //         var6: 'var 6'
  //       },
  //       'var3',
  //       'var4'
  //     ],
  //     'var1',
  //     'var2'
  //   ]
  //   const formatedData = trimFields(data)
  //   expect(formatedData).toEqual(expectedData)
  // })
})
