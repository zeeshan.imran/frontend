import qs from 'qs'
import getQueryParams from '../../utils/getQueryParams'

const useSearch = ({ location, history }) => {
  const params = getQueryParams(location)

  const { keyword, orderBy, orderDirection, page } = params

  // only sort when orderBy is available
  let orderQuery = orderBy
    ? {
      orderBy,
      orderDirection
    }
    : {}

  const handleKeywordChange = keyword => {
    history.push({
      search: qs.stringify({
        ...params,
        keyword: keyword || undefined,
        page: 1
      })
    })
  }

  const handleTableChange = (page, orderBy, orderDirection) => {
    history.push({
      search: qs.stringify({
        ...params,
        orderBy,
        orderDirection,
        page: page
      })
    })
  }

  return {
    keyword: keyword || '',
    page: parseInt(page, 10) || 1,
    handleKeywordChange,
    handleTableChange,
    ...orderQuery
  }
}

export default useSearch
