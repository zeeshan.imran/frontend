import { useRef, useCallback, useMemo } from 'react'
import { uniq } from 'ramda'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import getAnswerLocalId from '../../utils/getAnswerLocalId'

const useAdvanceStep = inputStepId => {
  const {
    data: { currentSurveyParticipation }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)
  const handleAdvance = useRef(useMutation(ADVANCE_IN_SURVEY))
  const setSelectedProducts = useMutation(SET_SELECTED_PRODUCTS)
  const handleSubmitAnswer = useRef(useMutation(SUBMIT_ANSWER))
  const saveCurrentSurveyParticipation = useRef(
    useMutation(SAVE_SURVEY_PARTICIPATION)
  )

  const {
    answers,
    selectedProducts,
    surveyEnrollmentId,
    selectedProduct
  } = currentSurveyParticipation

  const stepId = inputStepId || currentSurveyParticipation.currentSurveyStep

  let currentAnswer =
    answers.find(a => a.id === getAnswerLocalId(stepId, selectedProducts)) || []
  if (currentAnswer && currentAnswer.values && !currentAnswer.values.length) {
    const newCurrentTestAnswer =
      answers.find(a => a.id === getAnswerLocalId(stepId)) || []
    if (
      newCurrentTestAnswer &&
      newCurrentTestAnswer.values &&
      newCurrentTestAnswer.values.length
    ) {
      currentAnswer = newCurrentTestAnswer
    }
  }
  
  const advanceStep = useCallback(
    async currentStep => {
      if (selectedProduct) {
        await saveCurrentSurveyParticipation.current({
          variables: {
            selectedProducts: uniq([...selectedProducts, selectedProduct])
          }
        })
        // Updating NEW PRODUCT in DB AS WELL, AS IT IS BEING USED LATER
        await setSelectedProducts({
          variables: {
            surveyEnrollmentId,
            selectedProducts: uniq([...selectedProducts, selectedProduct])
          }
        })
      }

      await handleSubmitAnswer.current({
        variables: {
          input: {
            question: stepId,
            value: currentAnswer.values,
            context: currentAnswer.context,
            startedAt: currentAnswer.startedAt,
            surveyEnrollment: surveyEnrollmentId,
            selectedProduct
          }
        }
      })
      await handleAdvance.current({
        variables: { from: currentStep || stepId }
      })
    },
    [
      stepId,
      currentAnswer,
      selectedProducts,
      selectedProduct,
      surveyEnrollmentId
    ]
  )

  return useMemo(
    () => ({
      currentSurveyParticipation,
      advanceStep
    }),
    [currentSurveyParticipation, advanceStep]
  )
}

const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation($selectedProducts: [String]) {
    saveCurrentSurveyParticipation(selectedProducts: $selectedProducts) @client
  }
`

const ADVANCE_IN_SURVEY = gql`
  mutation advanceInSurvey($from: ID, $skipLoop: Boolean) {
    advanceInSurvey(fromStep: $from, skipLoop: $skipLoop) @client
  }
`

const SUBMIT_ANSWER = gql`
  mutation submitAnswer($input: SubmitAnswerInput!) {
    submitAnswer(input: $input)
  }
`

export const SET_SELECTED_PRODUCTS = gql`
  mutation setSelectedProducts(
    $surveyEnrollmentId: ID!
    $selectedProducts: [ID!]
  ) {
    setSelectedProducts(
      surveyEnrollment: $surveyEnrollmentId
      selectedProducts: $selectedProducts
    ) {
      id
      selectedProducts {
        id
      }
    }
  }
`

export default useAdvanceStep
