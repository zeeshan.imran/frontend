import { useRef, useMemo, useEffect } from 'react'
import debounce from 'lodash.debounce'
import useAdvanceStep from '../useAdvanceStep'

const useDebounceAdvanceStep = debounceInSeconds => {
  const { advanceStep } = useAdvanceStep()
  const advanceStepRef = useRef(advanceStep)
  advanceStepRef.current = advanceStep

  const debounceAdvanceStep = useMemo(
    () =>
      debounce(() => {
        advanceStepRef.current()
      }, debounceInSeconds * 1000),
    [debounceInSeconds]
  )

  useEffect(() => {
    return () => {
      debounceAdvanceStep && debounceAdvanceStep.cancel()
    }
  }, [debounceAdvanceStep])

  return debounceAdvanceStep
}

export default useDebounceAdvanceStep
