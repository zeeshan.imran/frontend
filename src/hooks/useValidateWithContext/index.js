import { useMemo, useCallback } from 'react'
import { yupToFormErrors } from 'formik'

const useValidateWithContext = (validationSchema, contextBuilder, deps) => {
  const context = useMemo(contextBuilder, deps)
  return useCallback(
    async values => {
      try {
        await validationSchema.validate(values, {
          abortEarly: false,
          context
        })
      } catch (e) {
        throw yupToFormErrors(e)
      }
    },
    [validationSchema, context]
  )
}

export default useValidateWithContext
