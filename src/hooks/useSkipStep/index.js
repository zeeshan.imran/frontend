import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'

const useSkipStep = () => {
  const handleAdvance = useMutation(ADVANCE_IN_SURVEY)

  const skipLoop = () => {
    handleAdvance({ variables: { skipLoop: true } })
  }

  return skipLoop
}

const ADVANCE_IN_SURVEY = gql`
  mutation advanceInSurvey($from: ID, $skipLoop: Boolean) {
    advanceInSurvey(fromStep: $from, skipLoop: $skipLoop) @client
  }
`

export default useSkipStep
