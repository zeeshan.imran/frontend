import React from 'react'
import { create } from 'react-test-renderer'
import { ApolloProvider } from 'react-apollo-hooks'
import defaults from '../../defaults'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import useProductsBrands, { BRANDS_QUERY } from '.'
import wait from '../../utils/testUtils/waait'
const TestHook = ({ callback }) => {
  callback()
  return null
}
describe('option to have only 1 product without the choose product page', () => {
  let testRender
  let client
  let callSetSelectedProductsMutation = jest.fn()
  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: BRANDS_QUERY,
      data: {
        brands: {
          ...defaults.brands,
          id: 'brand-1',
          name: 'brand',
          logo: ''
        }
      }
    })
  })
  afterEach(() => {
    testRender.unmount()
  })
  test('should call BRANDS_QUERY on server side', async () => {
    const products = [
      {
        id: 'product-1',
        name: 'product',
        brand: 'brand'
      }
    ]
    testRender = create(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            await useProductsBrands(products)
          }}
        />
      </ApolloProvider>
    )
    await wait(0)
    expect(callSetSelectedProductsMutation).toHaveBeenCalledTimes(0)
  })
})
