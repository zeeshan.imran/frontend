import { useCallback } from 'react'
import { useMutation } from 'react-apollo-hooks'
import { IS_USER_REGISTERED } from '../../mutations/isUserRegistered'

const useIsUserRegistered = () => {
  const handleUserRegistration = useMutation(IS_USER_REGISTERED)

  return useCallback(
    async ({ email }) => {
      try {
        await handleUserRegistration({
          variables: {
            email
          }
        })
        return true
      } catch (e) {
        return false
      }
    },
    [handleUserRegistration]
  )
}

export default useIsUserRegistered
