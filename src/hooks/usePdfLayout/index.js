import { useCallback } from 'react'
import { getGridSize } from '../../utils/paperSizes'

export const usePdfLayout = () => {
  const fitGridItem = useCallback(
    (elementItem, layoutItem, { colWidth, rowHeight }) => {
      if (elementItem) {
        const width = Math.round(layoutItem.w * colWidth)
        const height = Math.round(layoutItem.h * rowHeight)
        const ref = elementItem.getRef && elementItem.getRef()
        if (ref) {
          console.log(layoutItem.w, 'x', layoutItem.h, '->', width, 'x', height)
          ref.updateGridItemSize({
            width: width - 4,
            height: height - 4
          })
        }
      }
    },
    []
  )

  const fixLayout = useCallback(
    (elementItems, gridLayout, paperSize, zoom) => {
      const { colWidth, rowHeight } = getGridSize(paperSize, zoom)
      console.log({ paperSize, zoom })
      for (let layoutItem of gridLayout) {
        const id = layoutItem.i

        const elementItem = elementItems.find(item => item.i === id)
        if (elementItem) {
          if (elementItem.align === 'fit') {
            const ref = elementItem.getRef && elementItem.getRef()
            if (ref) {
              fitGridItem(elementItem, layoutItem, { colWidth, rowHeight })
            }
          }

          if (elementItem.align === 'scale') {
            const gridItemDom = document.getElementById(`pdf-${id}`)
            if (gridItemDom) {
              const [wrapper] = gridItemDom.querySelectorAll('div')
              wrapper.style.transform = `scale(${zoom})`
            }
          }
        }
      }
    },
    [fitGridItem]
  )

  return { fitGridItem, fixLayout }
}
