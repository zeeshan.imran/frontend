import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import SurveyAnalysisFileComponent from '../../components/SurveyAnalysisFile'
import Showing from './Showing'

const SurveyAnalysisFile = ({ jobId, file }) => {
  const { data, loading } = useQuery(GET_SURVEY_ANALYSIS_FILE, {
    variables: { jobId, file: file.name },
    skip: !file,
    fetchPolicy: 'cache-and-network'
  })

  if (loading) {
    return <Showing />
  }

  return <SurveyAnalysisFileComponent file={file} content={data.content} />
}

const GET_SURVEY_ANALYSIS_FILE = gql`
  query getSurveyAnalysisFile($jobId: ID!, $file: String!) {
    content: getAnalysisFile(id: $jobId, file: $file)
  }
`

export default SurveyAnalysisFile
