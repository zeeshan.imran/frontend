import React, { Component } from 'react'
import { Desktop } from '../../components/Responsive'
import PaymentDetailsForm from '../../components/PaymentDetailsForm'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { withTranslation } from 'react-i18next'

class PaymentDetails extends Component {
  handleSubmit = (e, form) => {
    const {
      user: { id },
      updateTesterInfo,
      t
    } = this.props

    form.validateFields(async (err, values) => {
      if (!err) {
        const updateTesterInfoData = {
          id,
          ...(!!form.isFieldTouched('paypalEmail') && {
            paypalEmail: values.paypalEmail
          })
        }

        const shouldUpdateTesterInfo =
          Object.keys(updateTesterInfoData).length > 1

        if (shouldUpdateTesterInfo) {
          try {
            const {
              data: {
                updateTesterInfo: {
                  ok: updateTesterInfoOk,
                  error: updateTesterInfoError
                }
              }
            } = await updateTesterInfo({ variables: updateTesterInfoData })
            if (!updateTesterInfoOk) {
              return displayErrorPopup(
                t('error.update', { error: updateTesterInfoError })
              )
            }
          } catch (error) {
            return displayErrorPopup(t('containers.paymentDetails.error'))
          }
        }
      }
    })

    return displaySuccessMessage(t('containers.paymentDetails.success'))
  }

  render () {
    const { user: { testerInfo: { paypalEmail } } = {} } = this.props
    return (
      <Desktop>
        {desktop => (
          <PaymentDetailsForm
            desktop={desktop}
            submitForm={this.handleSubmit}
            paypalEmail={paypalEmail}
          />
        )}
      </Desktop>
    )
  }
}

export default withTranslation()(PaymentDetails)
export { PaymentDetails as PaymentDetailsWrapped }

