import React from 'react'
import { useQuery } from 'react-apollo-hooks'
import { pluck } from 'ramda'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import SurveysList from '../../components/SurveysList'
import { getAuthenticatedUser } from '../../utils/userAuthentication'

const TasterExclusiveSurveysList = ({ history }) => {
  const user = getAuthenticatedUser()
  const {
    data: { user: { exclusiveSurveys = [], surveyEnrollments = [] } = {} } = {},
    loading
  } = useQuery(EXCLUSIVE_SURVEYS_QUERY, {
    variables: { userId: user.id },
    fetchPolicy: 'cache-and-network'
  })

  const allSurveyEnrollements = pluck('id')(pluck('survey')(surveyEnrollments))

  const validSurveys = exclusiveSurveys.filter(
    survey => survey.state === 'active'
  )

  const findClosedEnrollments = (survey) => {
    return surveyEnrollments.find((enrollment) => {
      return (enrollment.survey.id === survey.id && (enrollment.state === 'finished' || enrollment.state ==='rejected'))
    })
  }

  const checkEnrollment = (survey) => {
    const found = findClosedEnrollments(survey)
    if(!found || survey.allowRetakes){
      history.push(`/survey/${survey.id}`)
    }
  }

  return (
    <SurveysList
      loading={loading}
      surveys={validSurveys}
      allSurveyEnrollements={allSurveyEnrollements}
      handleNavigateToSurvey={checkEnrollment}
      findClosedEnrollments={findClosedEnrollments}
    />
  )
}

const EXCLUSIVE_SURVEYS_QUERY = gql`
  query user($userId: ID) {
    user(id: $userId) {
      surveyEnrollments {
        id
        state
        survey {
          id
        }
      }
      exclusiveSurveys {
        id
        name
        state
        coverPhoto
        allowRetakes
        isScreenerOnly
        products {
          id
          reward
          isAvailable
        }
        linkedSurveys {
          id
          name
          products {
            id
            reward
            isAvailable
          }
        }
      }
    }
  }
`

export default withRouter(TasterExclusiveSurveysList)
