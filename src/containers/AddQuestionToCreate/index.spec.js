import React from 'react'
import AddQuestionToCreate, { SURVEY_QUERY } from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { mount } from 'enzyme'
import wait from '../../utils/testUtils/waait'
import useUpdateSurvey from '../../hooks/useUpdateSurvey'
import CreationButtonContainer from '../../components/CreationButtonContainer'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { defaultSurvey } from '../../mocks'

jest.mock('../../hooks/useUpdateSurvey')

const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    name: 'Survey 1',
    uniqueName: 'survey-1',
    minimumProducts: 1,
    maximumProducts: 1
  },
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand',
      reward: 0
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: [],
  maxProductStatCount: 7
}

const createMockSurveyQuery = state => ({
  request: {
    query: SURVEY_QUERY,
    variables: {
      surveyId: 'survey-1'
    }
  },
  result: {
    data: {
      survey: {
        id: 'survey-1',
        state
      }
    }
  }
})

describe('auto save draft after adding new question on a existed survey', () => {
  let updateSurvey
  let testRender
  let client

  beforeEach(() => {
    updateSurvey = jest.fn()
    useUpdateSurvey.mockImplementation(() => updateSurvey)
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should disable the button while loading SURVEY_QUERY', async () => {
    client = createApolloMockClient({
      mocks: [createMockSurveyQuery('draft')]
    })

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <AddQuestionToCreate.WrappedComponent
          match={{ params: { surveyId: 'survey-1' } }}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    // while loading SURVEY_QUERY, the button should disabled
    expect(testRender.find(CreationButtonContainer).prop('type')).toBe(
      'disabled'
    )
  })

  test('should call updateDraft(clearAfterSave: false) after adding a new question on EXISTED DRAFT survey', async () => {
    client = createApolloMockClient({
      mocks: [createMockSurveyQuery('draft')]
    })

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <AddQuestionToCreate.WrappedComponent
          match={{ params: { surveyId: 'survey-1' } }}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    testRender.update()

    testRender
      .find(CreationButtonContainer)
      .first()
      .prop('onClick')()

    await wait(0)
    expect(updateSurvey).not.toHaveBeenCalled()
  })

  test('should NOT call updateDraft(clearAfterSave: false) after adding a new question on EXISTED PUBLISHED survey', async () => {
    client = createApolloMockClient({
      mocks: [createMockSurveyQuery('active')]
    })

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <AddQuestionToCreate.WrappedComponent
          match={{ params: { surveyId: 'survey-1' } }}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    testRender.update()

    testRender
      .find(CreationButtonContainer)
      .first()
      .prop('onClick')()

    expect(updateSurvey).not.toHaveBeenCalled()
  })

  test('should NOT call updateDraft after adding a new question on NEW survey', async () => {
    client = createApolloMockClient()

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <AddQuestionToCreate.WrappedComponent
          match={{ params: {} }}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    testRender.update()

    testRender
      .find(CreationButtonContainer)
      .first()
      .prop('onClick')()

    expect(updateSurvey).not.toHaveBeenCalled()
  })
})
