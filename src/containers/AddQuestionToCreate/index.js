import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'
import { path } from 'ramda'
import { generate } from 'shortid'
import CreationButtonContainer from '../../components/CreationButtonContainer'
import useUpdateSurvey from '../../hooks/useUpdateSurvey'
import { useTranslation } from 'react-i18next'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import getQueryParams from '../../utils/getQueryParams'
import addQuestion from '../../utils/addQuestion'

export const UPDATE_SURVEY_CREATION_QUESTIONS = gql`
  mutation updateSurveyCreationQuestions($questions: questions) {
    updateSurveyCreationQuestions(questions: $questions) @client
  }
`

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      questions
    }
  }
`

export const SURVEY_QUERY = gql`
  query survey($surveyId: ID!) {
    survey(id: $surveyId) {
      id
      state
    }
  }
`

const AddQuestionToCreate = ({ match, location }) => {
  const [processing, setProcessing] = useState(false)
  const { section } = getQueryParams(location)
  const { t } = useTranslation()
  const {
    params: { surveyId }
  } = match

  const { loading, data: surveyResult } = useQuery(SURVEY_QUERY, {
    variables: { surveyId },
    skip: !surveyId
  })

  const {
    data: {
      surveyCreation: { basics, questions }
    },
    refetch
  } = useQuery(SURVEY_CREATION)

  const updateQuestions = useMutation(UPDATE_SURVEY_CREATION_QUESTIONS)

  const updateDraft = useUpdateSurvey({
    surveyId,
    successMessage: t('containers.addQuestionToCreate.success'),
    errorMessage: t('containers.addQuestionToCreate.error')
  })

  const surveyValidate = useSurveyValidate()

  const isSurveyValid = async () => {
    if (!(await surveyValidate.validateBasics(basics))) {
      return false
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      await surveyValidate.showProductsErrors()
      return false
    }

    const {
      isValid: isQuestionsValid
    } = await surveyValidate.validateQuestions()
    if (!isQuestionsValid) {
      await surveyValidate.showQuestionsErrors()
      return false
    }

    return true
  }

  const createNewOpenQuestion = async () => {
    setProcessing(true)

    try {
      if (surveyId) {
        const isPublished = surveyResult.survey.state === 'active'
        if (!isPublished && (await isSurveyValid()) && questions.length > 0) {
          await updateDraft(false)
        }
      }

      const newQuestion = {
        clientGeneratedId: generate(),
        type: '',
        required: true,
        displayOn: section || 'middle',
        options: [],
        sliderOptions: {
          __typeName: 'sliderOptions'
        },
        showProductImage: true,
        pairsOptions: {
          customFlavorEnabled: false,
          oneFlavorNotPresentEnabled: false,
          neitherFlavorPresentEnabled: false,
          bothFlavorsPresentEnabled: false
        }
      }

      const updatedSurvey = await refetch()
      const updatedQuestions =
        path(['data', 'surveyCreation', 'questions'], updatedSurvey) || []

      await updateQuestions({
        variables: {
          questions: addQuestion(updatedQuestions, newQuestion)
        }
      })
    } catch (ex) {
      //
    }

    setProcessing(false)
  }

  return (
    <CreationButtonContainer
      type={loading || processing ? 'disabled' : 'primary'}
      onClick={createNewOpenQuestion}
      text={t('containers.addQuestionsToCreate.newStep')}
      data-testid='new-step-button'
      tooltip={t('tooltips.newQuestion')}
    />
  )
}

export default withRouter(AddQuestionToCreate)
