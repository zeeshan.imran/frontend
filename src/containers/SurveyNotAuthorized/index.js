import React from 'react'
import { useTranslation } from 'react-i18next'
import { Container, HeaderContainer, BodyContainer, Title, ContentText } from './styles'

const SurveyNotAuthorized = () => {
  const { t } = useTranslation()
  return (
    <Container>
      <HeaderContainer>
        <Title>{t(`components.survey.notAuthorized.title`)}</Title>
      </HeaderContainer>
      <BodyContainer>
        <ContentText>{t(`components.survey.notAuthorized.content`)}</ContentText>
      </BodyContainer>
    </Container>
  )
}

export default SurveyNotAuthorized
