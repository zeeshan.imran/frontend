import React from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Text from '../../components/Text'

const PhotosSurveyButton = ({ surveyId, history, onMenuItemClick }) => {
  const { t } = useTranslation()
  return (
    <Text
      onClick={() => {
        onMenuItemClick()
        history.push(`/operator/survey/photos/${surveyId}`)
      }}
    >
      {t('components.dasboard.survey.actions.photoValidation')}
    </Text>
  )
}
export default withRouter(PhotosSurveyButton)
