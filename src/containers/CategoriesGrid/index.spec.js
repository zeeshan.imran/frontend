import React from 'react'
import { mount } from 'enzyme'
import CategoriesGrid from '.'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { MockedProvider } from 'react-apollo/test-utils'
import wait from '../../utils/testUtils/waait'
import Loader from '../../components/Loader'
import CategoriesGridComponent from '../../components/CategoriesGrid'
import { ROOT_CATEGORIES_QUERY } from '../../queries/CategoriesGrid'

const mocks = [
  {
    request: {
      query: ROOT_CATEGORIES_QUERY
    },
    result: {
      data: {
        productCategories: [
          {
            children: {
              value: 'some value',
              key: 1
            }
          }
        ]
      }
    }
  }
]

describe('CategoriesGrid', () => {
  let testRender

  let user

  beforeEach(() => {
    user = getAuthenticatedUser()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render loader', () => {
    testRender = mount(
      <MockedProvider>
        <CategoriesGrid desktop={''} user={user} />
      </MockedProvider>
    )
    expect(testRender.find(Loader)).toMatchSnapshot()
    // expect(Loader).toHaveLength(1)
  })

  test('should render OngoingSurveysGridComponent', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks}>
        <CategoriesGrid desktop={''} user={user} />
      </MockedProvider>
    )
    await wait(0)
    expect(testRender.find(CategoriesGridComponent)).toMatchSnapshot()
  })
})
