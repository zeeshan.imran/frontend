import React, { Component } from 'react'
import { Query } from 'react-apollo'
import Loader from '../../components/Loader'
import CategoriesGridComponent from '../../components/CategoriesGrid'
import { ROOT_CATEGORIES_QUERY } from '../../queries/CategoriesGrid'

class CategoriesGrid extends Component {
  render () {
    return (
      <Query query={ROOT_CATEGORIES_QUERY} fetchPolicy='cache-and-network'>
        {({ data: { productCategories }, loading }) => {
          if (loading) return <Loader />
          return (
            <CategoriesGridComponent
              categories={productCategories[0].children}
            />
          )
        }}
      </Query>
    )
  }
}

export default CategoriesGrid
