import React, { useCallback, useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import copy from 'copy-to-clipboard'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import OperatorQrCodesComponent from '../../components/OperatorQrCodes'
import AlertModal from '../../components/AlertModal'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import useSearch from '../../hooks/useSearch'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'
import { useTranslation } from 'react-i18next'
import OperatorQrCodeDownload from '../OperatorQrCodeDownload'

const OperatorQrCodes = ({ history, location }) => {
  const {
    keyword,
    orderBy,
    orderDirection,
    page,
    handleKeywordChange,
    handleTableChange
  } = useSearch({
    location,
    history
  })
  const [qrCodeToDownload, setQrCodeToDownload] = useState(null)
  const { t } = useTranslation()
  const { loading, data, error: searchError } = useQuery(QUERY_QRCODES, {
    variables: {
      input: {
        keyword,
        orderBy,
        orderDirection,
        skip: (page - 1) * DEFAULT_N_ELEMENTS_PER_PAGE
      }
    },
    fetchPolicy: 'cache-and-network'
  })

  const { loading: loadingCount, data: dataCount } = useQuery(
    QUERY_COUNT_QRCODES,
    {
      variables: {
        input: {
          keyword
        }
      },
      fetchPolicy: 'cache-and-network'
    }
  )

  const deleteQrCode = useMutation(DELETE_QR_CODE, {
    refetchQueries: ['searchQrCodes']
  })
  const { qrCodes } = data

  useEffect(() => {
    searchError &&
      displayErrorPopup(getErrorMessage(searchError, 'E_QR_CODE_SEARCH'))
  }, [searchError])

  const handleDelete = useCallback(
    qrCode => {
      AlertModal({
        title: t('containers.operatorQrCode.title'),
        description: t('containers.operatorQrCode.description'),
        okText: t('containers.operatorQrCode.okText'),
        handleOk: () =>
          deleteQrCode({
            variables: { id: qrCode.id }
          })
      })
    },
    [t, deleteQrCode]
  )

  return (
    <OperatorPage>
      <OperatorPageContent>
        <OperatorQrCodesComponent
          loading={loading || loadingCount}
          searchTerm={keyword}
          onSearch={handleKeywordChange}
          onCreate={() => history.push(`./qr-codes/create`)}
          onCopy={qrCode =>
            copy(`${window.location.origin}${qrCode.redirectLink}`)
          }
          onDownload={setQrCodeToDownload}
          onDelete={handleDelete}
          onTableChange={handleTableChange}
          onEdit={qrCode => history.push(`./qr-codes/${qrCode.id}`)}
          qrCodes={searchError ? [] : qrCodes}
          total={(dataCount && dataCount.count) || 0}
          page={page}
        />
      </OperatorPageContent>
      <OperatorQrCodeDownload
        qrCode={qrCodeToDownload}
        onCancel={() => setQrCodeToDownload(null)}
      />
    </OperatorPage>
  )
}

export const QUERY_QRCODES = gql`
  query searchQrCodes($input: QrCodesInput!) {
    qrCodes(input: $input) {
      id
      uniqueName
      name
      redirectLink
      targetType
      survey {
        id
        name
        uniqueName
      }
      targetLink
      qrCodePhoto
    }
  }
`

export const QUERY_COUNT_QRCODES = gql`
  query countQrCodes($input: CountQrCodesInput!) {
    count: countQrCodes(input: $input)
  }
`

export const DELETE_QR_CODE = gql`
  mutation deleteQrCode($id: ID!) {
    deleteQrCode(id: $id)
  }
`

export default withRouter(OperatorQrCodes)
