import React, { useEffect, useState } from 'react'
import gql from 'graphql-tag'
import { isEmpty } from 'ramda'
import { useMutation, useQuery } from 'react-apollo-hooks'
import { withRouter, Redirect } from 'react-router-dom'
import SurveyLoginComponent from '../../components/SurveyLogin'
import FillerLoader from '../../components/FillerLoader'
import { SURVEY_QUERY } from '../../queries/Survey'

import useCustomButton from '../../hooks/useCustomButton'
import useUpdateLocale from '../../hooks/useUpdateSurveyLanguage'
import useResumeSurvey from '../../hooks/useResumeSurvey'
import useLoginToSurvey from '../../hooks/useLoginToSurvey'

import { getAuthenticatedUser } from '../../utils/userAuthentication'
import {
  logout as logoutOfPreviousSurveys,
  getAuthenticationToken,
  removeSurveyRedirect
} from '../../utils/surveyAuthentication'
import {
  destroyDraftWarningPopup,
  displayDraftWarningPopup
} from '../../utils/displayDraftWarningPopup'
import getQueryParams from '../../utils/getQueryParams'

const retakeKey = 'retake'

const getShouldSkipLogin = survey => {
  const { authorizationType, settings, state } = survey
  return (
    authorizationType === 'public' &&
    !settings.recaptcha &&
    state !== 'suspended'
  )
}

const SurveyLogin = ({ history, surveyId, location, isTesting }) => {
  const {
    data: { survey = {} } = {},
    loading: fetchingSurvey,
    error
  } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId },
    fetchPolicy: isTesting ? 'cache-first' : 'network-only'
  })
  const resetCache = useMutation(RESET_CACHE)
  const saveCurrentSurveyParticipation = useMutation(SAVE_SURVEY_PARTICIPATION)
  const loginToSurvey = useLoginToSurvey()
  const resumeSurveyProgress = useResumeSurvey(history.push)

  const updateLocale = useUpdateLocale()
  const { buttonLabel } = useCustomButton(surveyId, 'continue')
  const [isLoggingIn, setLoggingIn] = useState(false)
  const [isSelectedTaster, setIsSelectedTaster] = useState(false)
  const [surveyEnrollmentId, setSurveyEnrollmentId] = useState(null)
  const [lastAnsweredQuestion, setLastAnsweredQuestion] = useState(null)
  const [isRetake, setIsRetake] = useState(null)

  const loggedInUser = getAuthenticatedUser()
  const isLoggedIn = loggedInUser && !isEmpty(loggedInUser)
  const isAnExclusiveSurvey =
    survey.authorizationType && (survey.authorizationType === 'selected' || survey.authorizationType === 'enrollment')
  const isForcedAccount =
    survey && survey.forcedAccountLocation
      ? survey.forcedAccount && survey.forcedAccountLocation === 'start'
      : false
  const [isSurveyExpired, setSurveyExpired] = useState(false)
  const [isSurveySuspended, setSurveySuspended] = useState(false)

  useEffect(() => {
    logoutOfPreviousSurveys()
    resetCache()
  }, [])

  if (survey && survey.authorizationType === 'public' && isForcedAccount) {
    // Treating it as an email
    survey.authorizationType = 'enrollment'
  }

  useEffect(() => {
    const { referral } = getQueryParams(location)

    //emptying survey local storage
    removeSurveyRedirect(null)

    if (survey && surveyId === survey.uniqueName) {
      const redirecter = referral
        ? `/survey/${survey.id}?referral=${referral}`
        : `/survey/${survey.id}`

      history.push(redirecter)
      return
    }

    if (
      survey.state &&
      survey.state !== 'active' &&
      survey.state !== 'draft' &&
      survey.state !== 'suspended'
    ) {
      history.push(`/404`)
      return
    }

    if (survey.state === 'suspended') {
      setSurveySuspended(true)
    }

    if (isAnExclusiveSurvey && isLoggedIn) {
      setLoggingIn(true)
      loginToSurvey({
        email: loggedInUser.emailAddress,
        referral,
        surveyId,
        isLoggedIn
      })
        .then(async loggedInInfo => {
          setLoggingIn(false)
          setIsSelectedTaster(!!getAuthenticationToken)
          await saveCurrentSurveyParticipation({
            variables: {
              savedRewards: loggedInInfo.savedRewards
            }
          })
          setSurveyEnrollmentId(loggedInInfo.surveyEnrollmentId)
          setLastAnsweredQuestion(loggedInInfo.lastAnsweredQuestion)
        })
        .catch(err => {
          if (err.message.includes('rejected')) {
            setIsRetake('rejected')
          } else if (err.message.includes('finished')) {
            setIsRetake('finished')
          } else {
            setIsRetake(false)
          }
          setLoggingIn(false)
          err.message.toLowerCase().includes(retakeKey)
            ? setIsSelectedTaster(retakeKey)
            : setIsSelectedTaster(false)
          setSurveyEnrollmentId(null)
          setLastAnsweredQuestion(null)
        })
    } else if (getShouldSkipLogin(survey)) {
      setLoggingIn(true)
      loginToSurvey({
        surveyId,
        email: '',
        referral,
        isLoggedIn
      }).then(
        async ({
          surveyEnrollmentId,
          savedRewards,
          lastAnsweredQuestion,
          lastSelectedProduct: lastSelectedProductDb
        }) => {
          setLastAnsweredQuestion(lastAnsweredQuestion)

          // Only If screener
          if (survey.isScreenerOnly) {
            await resumeSurveyProgress({
              surveyId,
              lastAnsweredQuestionFromLogin: lastAnsweredQuestion
            })
            await saveCurrentSurveyParticipation({
              variables: {
                surveyId
              }
            })
            return
          }

          const availableProducts =
            (survey.products &&
              survey.products.filter(product => product.isAvailable)) ||
            []
          const productId = lastSelectedProductDb
            ? lastSelectedProductDb
            : availableProducts[0] && availableProducts[0].id

          if (productId) {
            await resumeSurveyProgress({
              surveyId,
              lastAnsweredQuestionFromLogin: lastAnsweredQuestion
            })
            await saveCurrentSurveyParticipation({
              variables: {
                surveyId,
                selectedProduct: productId,
                savedRewards: savedRewards
              }
            })
            if (survey.state !== 'suspended') {
              history.push('./instructions')
            }
          }
        }
      )
    }
    return destroyDraftWarningPopup
  }, [survey, surveyId, location])

  if (error) {
    return <Redirect to='/404' />
  }

  if (survey.state === 'draft') {
    displayDraftWarningPopup()
  }

  saveCurrentSurveyParticipation({
    variables: {
      surveyId
    }
  })

  if (
    fetchingSurvey ||
    isLoggingIn ||
    getShouldSkipLogin(survey) ||
    (survey && surveyId === survey.uniqueName)
  ) {
    return <FillerLoader loading />
  }

  const {
    name: surveyTitle,
    state,
    authorizationType,
    settings,
    surveyLanguage,
    products: productsInSurvey,
    loginText,
    pauseText
  } = survey
  updateLocale(surveyLanguage)

  if (state !== 'active' && state !== 'draft' && state !== 'suspended') {
    return <Redirect to='/404' />
  }

  return (
    <SurveyLoginComponent
      authorizationType={authorizationType}
      authorizationState={
        isSelectedTaster === 'retake'
          ? isRetake
          : isLoggedIn
          ? isSelectedTaster
            ? 'valid'
            : 'invalid'
          : 'loggedOut'
      }
      surveyTitle={surveyTitle}
      isSurveyExpired={isSurveyExpired}
      isSurveySuspended={isSurveySuspended}
      handleSubmit={async (email, password, country) => {
        let loggedSurveyEnrollmentId = surveyEnrollmentId
        let loggedLastAnsweredQuestion = lastAnsweredQuestion
        let loggedLastSelectedProduct = ''
        let retake = isSelectedTaster
        if (authorizationType !== 'selected') {
          const { referral } = getQueryParams(location)
          if (isLoggedIn && loggedInUser) {
            email = loggedInUser.emailAddress
          }

          await loginToSurvey({
            email: email || '',
            surveyId,
            referral
          })
            .then(loggedInInfo => {
              
              loggedSurveyEnrollmentId = loggedInInfo.surveyEnrollmentId
              loggedLastAnsweredQuestion = loggedInInfo.lastAnsweredQuestion
              loggedLastSelectedProduct = loggedInInfo.lastSelectedProduct
              setSurveyEnrollmentId(loggedSurveyEnrollmentId)
              setLastAnsweredQuestion(loggedLastAnsweredQuestion)
            })
            .catch(err => {
              if (err.message.toLowerCase().includes('expired')) {
                retake = retakeKey // Adding it to stop retake condition
                setSurveyExpired(true)
              } else {
                setIsSelectedTaster(false)
                if (err.message.toLowerCase().includes(retakeKey)) {
                  retake = retakeKey
                  setIsSelectedTaster(retakeKey)
                } else {
                  setIsSelectedTaster(false)
                }
              }

              setSurveyEnrollmentId(null)
              setLastAnsweredQuestion(null)
              if (err.message.includes('rejected')) {
                setIsRetake('rejected')
              } else if (err.message.includes('finished')) {
                setIsRetake('finished')
              } else {
                setIsRetake(false)
              }
            })
        }
        if (retake !== retakeKey) {
          await saveCurrentSurveyParticipation({
            variables: {
              surveyId,
              selectedProduct: loggedLastSelectedProduct
                ? loggedLastSelectedProduct
                : productsInSurvey[0] && productsInSurvey[0].id
            }
          })
          await resumeSurveyProgress({
            surveyId,
            lastAnsweredQuestionFromLogin: loggedLastAnsweredQuestion
          })
        }
      }}
      requireRecaptcha={settings.recaptcha}
      buttonLabel={buttonLabel}
      loginText={loginText}
      pauseText={pauseText}
    />
  )
}

const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation($surveyId: String) {
    saveCurrentSurveyParticipation(surveyId: $surveyId) @client
  }
`

const RESET_CACHE = gql`
  mutation resetCache {
    resetCache @client
  }
`

export default withRouter(SurveyLogin)
