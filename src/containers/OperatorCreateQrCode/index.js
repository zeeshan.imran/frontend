import { pick, propOr } from 'ramda'
import React, { useState, useRef } from 'react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { Redirect, withRouter } from 'react-router-dom'
import debounce from 'lodash/debounce'
import gql from 'graphql-tag'
import OperatorQrCodeForm from '../../components/OperatorQrCodeForm'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import { useTranslation } from 'react-i18next'

const getSurveys = propOr([], 'surveys')

const OperatorCreateQrCode = ({ history }) => {
  const { t } = useTranslation()
  const { loading: loadingOrganization, data, error } = useQuery(
    QUERY_USER_ORGANIZATION,
    {
      fetchPolicy: 'cache-and-network'
    }
  )
  const [saving, setSaving] = useState(false)
  const createQrCode = useMutation(MUTATION_CREATE_QR_CODE)

  const [keyword, setKeyword] = useState('')

  const { loading: loadingSurveys, data: surveyData } = useQuery(
    QUERY_SURVEYS,
    {
      fetchPolicy: 'cache-and-network',
      variables: { keyword }
    }
  )

  const handleSearchRef = useRef(
    debounce(keyword => {
      setKeyword(keyword)
    }, 250)
  )

  if (!loadingOrganization) {
    if (error) {
      displayErrorPopup(getErrorMessage({}, 'UNAUTHORIZED'))
      return <Redirect to='/' />
    }
    if (!data.me.organization) {
      displayErrorPopup(getErrorMessage({}, 'NO_ORGANIZATION'))
      return <Redirect to='/' />
    }
  }

  if (loadingOrganization) {
    return null
  }

  if (error) {
    return <Redirect to='/' />
  }

  const organizationUniqueName = data.me.organization.uniqueName
  const handleSearch = handleSearchRef.current

  return (
    <OperatorPage>
      <OperatorPageContent>
        <OperatorQrCodeForm
          uniqueNameDisable={false}
          isNewQrCode
          i18nPrefix='components.createQrCode'
          title={t('containers.operatorCreateQrCode.title')}
          saving={saving}
          loadingSurveys={loadingSurveys}
          surveys={getSurveys(surveyData)}
          organizationUniqueName={organizationUniqueName}
          onSearch={handleSearch}
          onCancel={() => history.push('/operator/qr-codes')}
          onSubmit={async form => {
            try {
              setSaving(true)
              const input = pick([
                'name',
                'uniqueName',
                'targetType',
                'survey',
                'targetLink'
              ])(form)

              const {
                data: {
                  createQrCode: { id }
                }
              } = await createQrCode({
                variables: {
                  input
                }
              })

              displaySuccessMessage(
                t('containers.operatorCreateQrCode.successMessage')
              )
              history.push(`/operator/qr-codes/${id}`)
            } catch (error) {
              displayErrorPopup(getErrorMessage(error, 'E_QR_CODE_CREATE'))
            }
            setSaving(false)
          }}
        />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export const MUTATION_CREATE_QR_CODE = gql`
  mutation createQrCode($input: QrCodeCreateInput) {
    createQrCode(input: $input) {
      id
      uniqueName
      redirectLink
      qrCodePhoto
    }
  }
`

export const QUERY_SURVEYS = gql`
  query surveys($keyword: String!) {
    surveys(input: { keyword: $keyword, skip: 0, limit: 20 }) {
      id
      name
      uniqueName
      state
    }
  }
`

export const QUERY_USER_ORGANIZATION = gql`
  query {
    me {
      id
      organization {
        id
        uniqueName
      }
    }
  }
`

export default withRouter(OperatorCreateQrCode)
