import React from 'react'
import { mount } from 'enzyme'
import Redirect from 'react-router-dom/Redirect'
import OperatorCreateQrCode, {
  QUERY_USER_ORGANIZATION,
  QUERY_SURVEYS,
  MUTATION_CREATE_QR_CODE
} from './index'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import wait from '../../utils/testUtils/waait'
import { Select } from 'antd'

jest.mock('../../utils/displayErrorPopup')
jest.mock('react-router-dom/Redirect')

const mockQueryUserOrganization = {
  request: {
    query: QUERY_USER_ORGANIZATION 
  },
  result: {
    data: {
      me: {
        id: 'user-1',
        organization: {
          id: 'org-1',
          uniqueName: 'organization-1'
        }
      }
    }
  }
}

const mockQueryUserOrganizationFailure = {
  request: {
    query: QUERY_USER_ORGANIZATION
  },
  result: {
    errors: [{ message: 'UNAUTHORIZED' }]
  }
}

const mockQuerySurveys = [
  {
    request: { query: QUERY_SURVEYS, variables: { keyword: 'Survey 1' } },
    result: {
      data: {
        surveys: [
          {
            id: 'survey-1',
            uniqueName: 'survey-1',
            name: 'Survey 1',
            state: 'draft',
            savedRewards: [],
            country: 'United States of America'
          }
        ]
      }
    }
  },
  {
    request: { query: QUERY_SURVEYS, variables: { keyword: 'the' } },
    result: {
      data: {
        surveys: [
          {
            id: 'survey-2',
            uniqueName: 'survey-2',
            name: 'The second survey',
            state: 'active',
            savedRewards: [],
            country: 'United States of America'
          }
        ]
      }
    }
  },
  {
    request: {
      query: QUERY_SURVEYS,
      variables: { keyword: 'should print empty' }
    },
    result: {
      data: {
        surveys: []
      }
    }
  },
  {
    request: { query: QUERY_SURVEYS, variables: { keyword: '' } },
    result: {
      data: {
        surveys: [
          {
            id: 'survey-1',
            uniqueName: 'survey-1',
            name: 'Survey 1',
            state: 'draft',
            savedRewards: [],
            country: 'United States of America'
          },
          {
            id: 'survey-2',
            uniqueName: 'survey-2',
            name: 'The second survey',
            state: 'active',
            savedRewards: [],
            country: 'United States of America'
          },
          {
            id: 'survey-3',
            uniqueName: 'survey-3',
            name: 'Survey 3',
            state: 'suspended',
            savedRewards: [],
            country: 'United States of America'
          }
        ]
      }
    }
  },
  {
    request: { query: QUERY_SURVEYS, variables: { keyword: '' } },
    result: {
      data: {
        surveys: [
          {
            id: 'survey-1',
            uniqueName: 'survey-1',
            name: 'Survey 1',
            state: 'draft',
            savedRewards: [],
            country: 'United States of America'
          },
          {
            id: 'survey-2',
            uniqueName: 'survey-2',
            name: 'The second survey',
            state: 'active',
            savedRewards: [],
            country: 'United States of America'
          },
          {
            id: 'survey-3',
            uniqueName: 'survey-3',
            name: 'Survey 3',
            state: 'suspended',
            savedRewards: [],
            country: 'United States of America'
          }
        ]
      }
    }
  }
]

const mockCreateQrCodeSuccess = {
  request: {
    query: MUTATION_CREATE_QR_CODE,
    variables: {
      input: {
        name: 'Link to Survey 1',
        uniqueName: 'link-to-survey-1',
        targetType: 'survey',
        survey: 'survey-1',
        targetLink: null
      }
    }
  },
  result: {
    data: {
      createQrCode: {
        id: 'qr-code-1',
        uniqueName: 'link-to-survey-1',
        redirectLink: 'http://localhost/qr-code/org-1/link-to-survey-1',
        qrCodePhoto: 'https://s3-aws/qr-codes/org-1/link-to-survey-1'
      }
    }
  }
}

const mockCreateQrCodeFailture = {
  request: {
    query: MUTATION_CREATE_QR_CODE,
    variables: {
      input: {
        name: 'Link to Slack',
        uniqueName: 'link-to-slack',
        targetType: 'link',
        survey: '',
        targetLink: 'https://flavor-wiki.slack.com'
      }
    }
  },
  result: {
    errors: [{ message: 'E_QR_CODE_UNIQUE' }]
  }
}

describe('Create Qr Code', () => {
  global.jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000
  let render
  let history

  async function simulateKeywordChange (keyword) {
    render
      .find('.ant-select-search__field')
      .simulate('change', { target: { value: keyword } })

    await wait(300)
    render.update()

    await wait(0)
    render.update()
  }

  async function simulateTargetTypeChange (targetType) {
    render
      .find(`input[name="targetType"][value="${targetType}"]`)
      .simulate('change', {
        target: { name: 'targetType', checked: true }
      })

    await wait(0)
    render.update()
  }

  beforeEach(() => {
    history = {
      push: jest.fn()
    }
  })

  afterEach(() => {
    render.unmount()
  })

  test('should render without crash', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryUserOrganization, ...mockQuerySurveys]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorCreateQrCode.WrappedComponent />
      </ApolloProvider>
    )

    await wait(0)
    render.update()
  })

  test('should change the direct link, uniqueName when changing the name', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryUserOrganization, ...mockQuerySurveys]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorCreateQrCode.WrappedComponent />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    render
      .find('[name="name"]')
      .last()
      .simulate('change', { target: { name: 'name', value: 'new qr code' } })

    await wait(0)
    render.update()

    expect(
      render
        .find('input[name="uniqueName"]')
        .getDOMNode()
        .getAttribute('value')
    ).toBe('new-qr-code')

    expect(
      render.find('input[data-testid="redirect-link"]').prop('value')
    ).toBe('http://localhost/qr-code/organization-1/new-qr-code')
  })

  test('test the search survey input', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryUserOrganization, ...mockQuerySurveys]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorCreateQrCode.WrappedComponent />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    await simulateTargetTypeChange('survey')

    // should show all surveys
    expect(render.find('.ant-select-selection')).toHaveLength(1)
    render.find('.ant-select-search__field').simulate('click')
    await wait(0)
    render.update()
    expect(render.find('.ant-select-dropdown-menu-item')).toHaveLength(3)

    // should not show any survey
    await simulateKeywordChange('should print empty')
    expect(render.find('.ant-select-dropdown-menu-item').text()).toBe(
      'No surveys were found'
    )

    // should show all surveys that start with the
    await simulateKeywordChange('the')
    expect(render.find('.ant-select-dropdown-menu-item')).toHaveLength(1)

    // should change survey to survey-2 when clicking on the second survey
    render.find('.ant-select-dropdown-menu-item').simulate('click')
    await wait(0)
    render.update()
    expect(render.find(Select).prop('value')).toBe('survey-2')
  }, 30000)

  test('should call mutation when clicking on Save button', async () => {
    const client = createApolloMockClient({
      mocks: [
        mockQueryUserOrganization,
        ...mockQuerySurveys,
        mockCreateQrCodeSuccess
      ]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorCreateQrCode.WrappedComponent history={history} />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    // fill the name field
    render.find('input[name="name"]').simulate('change', {
      target: { name: 'name', value: 'Link to Survey 1' }
    })

    // change linkType to survey
    await simulateTargetTypeChange('survey')

    // choose the survey
    await simulateKeywordChange('Survey 1')
    render.find('.ant-select-dropdown-menu-item').simulate('click')
    await wait(0)
    render.update()

    // save
    render.find('button[data-testid="save"]').simulate('click')

    await wait(1000)

    expect(history.push).toHaveBeenCalledWith('/operator/qr-codes/qr-code-1')
  })

  test('should redirect to / if can not fetch user information', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryUserOrganizationFailure, ...mockQuerySurveys]
    })

    const redirect = jest.fn()
    Redirect.mockImplementation(redirect)

    render = mount(
      <ApolloProvider client={client}>
        <OperatorCreateQrCode.WrappedComponent />
      </ApolloProvider>
    )

    await wait(1000)

    expect(redirect).toHaveBeenCalledWith(
      { push: false, to: '/' },
      { router: undefined }
    )
  })

  test('should show error message if can not create the Qr Code', async () => {
    const client = createApolloMockClient({
      mocks: [
        mockQueryUserOrganization,
        ...mockQuerySurveys,
        mockCreateQrCodeFailture
      ]
    })

    const displayError = jest.fn()
    displayErrorPopup.mockImplementation(displayError)

    render = mount(
      <ApolloProvider client={client}>
        <OperatorCreateQrCode.WrappedComponent history={history} />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    // fill the name field
    render.find('input[name="name"]').simulate('change', {
      target: { name: 'name', value: 'Link to Slack' }
    })

    render.find('input[name="targetLink"]').simulate('change', {
      target: { name: 'targetLink', value: 'https://flavor-wiki.slack.com' }
    })

    // save
    render.find('button[data-testid="save"]').simulate('click')

    await wait(1000)

    expect(displayError).toHaveBeenCalledWith(
      'The unique name is already being used by some other QR code'
    )
  })
})
