import React, { useState } from 'react'
import axios from 'axios'
import imageCompression from 'browser-image-compression'
import { useApolloClient } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { displayErrorPopup } from '../../utils/displayErrorPopup/index'
import UploadProductPictureComponent from '../../components/UploadProductPicture/index'
import FieldLabel from '../../components/FieldLabel'
import { useTranslation } from 'react-i18next'
import i18n from '../../utils/internationalization/i18n'

const GET_SIGNED_URL = gql`
  query getSignedUrl($type: String) {
    getSignedUrl(type: $type) {
      signedUrl
      fileLink
    }
  }
`

const optionsForImageUpload = {
  maxSizeMB: 1,
  maxWidthOrHeight: 1920,
  useWebWorker: true
}

const ALLOWED_TYPES = ['image/png', 'image/jpeg', 'image/gif']

const handleUpload = async (
  file,
  client,
  onChange,
  setUploading,
  setUploadError
) => {
  setUploading(true)
  try {

    let fileToUpload = file
    const { type, size } = fileToUpload

    if (!ALLOWED_TYPES.includes(type)) {
      setUploading(false)
      setUploadError(true)
      return displayErrorPopup(i18n.t('errors.uploadProductPictureFailed'))
    }

    if (size / 1024 / 1024 > 5) {
      setUploading(false)
      setUploadError(true)
      return displayErrorPopup(i18n.t('errors.uploadProductPictureFailed'))
    }

    if (size / 1024 / 1024 > 1) {
      let compressedFile = await imageCompression(
        fileToUpload,
        optionsForImageUpload
      )
      while (
        compressedFile &&
        compressedFile.size &&
        compressedFile.size / 1024 / 1024 > 1
      ) {
        compressedFile = await imageCompression(
          compressedFile,
          optionsForImageUpload
        )
      }
      fileToUpload = compressedFile
    }

    const {
      data: {
        getSignedUrl: { signedUrl, fileLink }
      }
    } = await client
      .query({
        query: GET_SIGNED_URL,
        variables: { type: fileToUpload.type },
        fetchPolicy: 'network-only'
      })
      .catch(error => {
        // handle error
        throw error
      })
      
    const result = await axios.put(signedUrl, fileToUpload, {
      headers: {
        'Content-Type': fileToUpload.type
      }
    })
    if (result.status !== 200) {
      setUploadError(true)
      return displayErrorPopup(i18n.t('errors.uploadProductPictureFailed'))
    }
    setUploadError(false)
    setUploading(false)
    onChange([fileLink])
  } catch (error) {
    setUploading(false)
    setUploadError(true)
    return displayErrorPopup(i18n.t('errors.uploadPictureFailed'))
  }
  setUploading(false)
}

const UploadProductPicture = ({
  tooltip,
  tooltipPlacement,
  onChange,
  value,
  ...rest
}) => {
  const { t } = useTranslation()
  const client = useApolloClient()
  const [uploading, setUploading] = useState(false)
  const [uploadError, setUploadError] = useState(false)
  return (
    <FieldLabel
      label={t('containers.uploadProductPicture.label')}
      tooltip={tooltip}
      tooltipPlacement={tooltipPlacement}
      postFill={t('containers.uploadProductPicture.limit')}
    >
      <UploadProductPictureComponent
        {...rest}
        error={uploadError}
        loading={uploading}
        onFileUpload={({ file }) =>
          handleUpload(file, client, onChange, setUploading, setUploadError)
        }
        url={value}
      />
    </FieldLabel>
  )
}

export default UploadProductPicture
