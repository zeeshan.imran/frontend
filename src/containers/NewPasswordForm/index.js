import React, { useState } from 'react'
import { Formik } from 'formik'
import * as yup from 'yup'
import NewPasswordFormComponent from '../../components/NewPasswordForm'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { useTranslation } from 'react-i18next';

const NewPasswordForm = ({ token, email, onSetNewPassword }) => {
  const [error, setError] = useState(false)
  const [message, setMessage] = useState('')
  const [submitted, setSubmitted] = useState(false)
  const [loading, setLoading] = useState(false)
  const resetPassword = useMutation(RESET_PASSWORD)
  const { t } = useTranslation();
  return (
    <Formik
      onSubmit={async ({ password }) => {
        setLoading(true)
        try {
          await resetPassword({
            variables: { input: { email, token, password } }
          })
          setLoading(false)
          setError(false)
          setMessage(t('containers.newPasswordF.success'))
          setSubmitted(true)
          onSetNewPassword()
        } catch (error) {
          setLoading(false)
          setError(true)
          setMessage(t('containers.newPasswordF.error'))
        }
      }}
      validationSchema={yup.object().shape({
        password: yup
          .string()
          .min(8, t('containers.newPasswordF.validation.passwordMin'))
          .required(t('containers.newPasswordF.validation.passwordRequire')),
        confirmPassword: yup
          .string()
          .oneOf([yup.ref('password')], t('containers.newPasswordF.validation.confirmMatch'))
          .required(t('containers.newPasswordF.validation.confirmRequire'))
      })}
      render={({ values, ...formikProps }) => (
        <NewPasswordFormComponent
          {...values}
          {...formikProps}
          loading={loading}
          submitted={submitted}
          infoMessage={message}
          errorSubmitting={error}
        />
      )}
    />
  )
}

const RESET_PASSWORD = gql`
  mutation resetPassword($input: ResetPasswordInput) {
    resetPassword(input: $input)
  }
`

export default NewPasswordForm
