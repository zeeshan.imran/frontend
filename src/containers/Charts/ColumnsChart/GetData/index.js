import { NOT_PRODUCT } from '../../../../utils/chartUtils'

export const getSelectedData = (inputData, products) => {
  let result = inputData.labels.map(el => ({
    label: el,
    data: []
  }))
  if (inputData.results.data) {
    for (let k = 0; k < inputData.results.data.length; k++) {
      let labelIndex = inputData.labels.indexOf(inputData.results.data[k].label)
      if (labelIndex !== -1) {
        result[labelIndex].data.push({
          name: NOT_PRODUCT,
          label: inputData.results.data[k].label,
          value: inputData.results.data[k].value
        })
      }
    }
  } else {
    for (let i = 0; i < inputData.results.length; i++) {
      if (!products[i].isSelected) continue
      for (let k = 0; k < inputData.results[i].data.length; k++) {
        let labelIndex = inputData.labels.indexOf(
          inputData.results[i].data[k].label
        )
        if (labelIndex !== -1) {
          result[labelIndex].data.push({
            name: inputData.results[i].name,
            label: inputData.results[i].data[k].label,
            value: inputData.results[i].data[k].value,
            productIndex: products[i].index
          })
        }
      }
    }
  }
  return result
}
