import stats from './stats.json'
import * as chartUtils from '../../utils/chartUtils'

export const getTabNames = () => {
  return chartUtils.getTabNames(stats)
}

export const getChartTabsContent = () => {
  return chartUtils.getChartTabsContent(stats)
}
