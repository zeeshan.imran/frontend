import { NOT_PRODUCT } from '../../../../utils/chartUtils'

export const parsePolarData = inputData => {
  const labels = inputData.labels

  if (inputData.series.data) {
    const axes = labels.map((label, labelIndex) => {
      return {
        axis: label,
        value: inputData.series.data[labelIndex]
      }
    })
    return [
      {
        name: NOT_PRODUCT,
        axes: axes,
        shape: shapeArr[0],
        shapeType: shapeFill[0]
      }
    ]
  }

  const products = inputData.series

  return products.map((product, index) => {
    const axes = labels.map((label, labelIndex) => {
      return {
        axis: label,
        value: product.data[labelIndex]
      }
    })
    return {
      name: product.name,
      axes: axes,
      shape: shapeArr[index % shapeArr.length],
      shapeType: shapeFill[index % shapeFill.length]
    }
  })
}

export const shapeArr = ['circle', 'triangle', 'rect']

export const shapeFill = ['fill', 'fill'] // fill or stroke
