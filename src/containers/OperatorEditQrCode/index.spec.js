import React from 'react'
import { mount } from 'enzyme'
import OperatorEditQrCode, {
  QUERY_SURVEYS,
  MUTATION_UPDATE_QR_CODE,
  QUERY_QR_CODE
} from './index'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import wait from '../../utils/testUtils/waait'
import { Select } from 'antd'
import OperatorQrCodeForm from '../../components/OperatorQrCodeForm'
import history from '../../history'

jest.mock('../../history')
jest.mock('../../utils/displayErrorPopup')

const mockQueryQrCode = {
  Link: {
    request: {
      query: QUERY_QR_CODE,
      variables: { qrCodeId: 'qr-code-1', checkOrg: true }
    },
    result: {
      data: {
        qrCode: {
          id: 'qr-code-1',
          name: 'Qr Code 1',
          uniqueName: 'qr-code-1',
          targetType: 'link',
          survey: null,
          targetLink: 'https://flavor-wiki.slack.com',
          qrCodePhoto: 'https://s3-aws/bucket/qr-codes/org-1/qr-code-1',
          owner: {
            id: 'org-1',
            uniqueName: 'organization-1'
          }
        }
      }
    }
  },
  Error: {
    request: {
      query: QUERY_QR_CODE,
      variables: {}
    },
    result: {
      data: {
        qrCode: {
          id: 'qr-code-1',
          name: 'Qr Code 1',
          uniqueName: 'qr-code-1',
          targetType: 'link',
          survey: null,
          targetLink: 'https://flavor-wiki.slack.com',
          qrCodePhoto: 'https://s3-aws/bucket/qr-codes/org-1/qr-code-1',
          owner: {
            id: 'org-1',
            uniqueName: 'organization-1'
          }
        }
      }
    }
  },
  Survey: {
    request: {
      query: QUERY_QR_CODE,
      variables: { qrCodeId: 'qr-code-1', checkOrg: true }
    },
    result: {
      data: {
        qrCode: {
          id: 'qr-code-1',
          name: 'Qr Code 1',
          uniqueName: 'qr-code-1',
          targetType: 'survey',
          survey: {
            id: 'survey-1',
            uniqueName: 'survey-1',
            name: 'Survey 1',
            savedRewards: [],
            country: 'United States of America'
          },
          targetLink: '',
          qrCodePhoto: 'https://s3-aws/bucket/qr-codes/org-1/qr-code-1',
          owner: {
            id: 'org-1',
            uniqueName: 'organization-1'
          }
        }
      }
    }
  }
}

const mockQuerySurveys = [
  {
    request: { query: QUERY_SURVEYS, variables: { keyword: 'Survey 1' } },
    result: {
      data: {
        surveys: [
          {
            id: 'survey-1',
            uniqueName: 'survey-1',
            name: 'Survey 1',
            state: 'draft',
            savedRewards: [],
            country: 'United States of America'
          }
        ]
      }
    }
  },
  {
    request: { query: QUERY_SURVEYS, variables: { keyword: 'the' } },
    result: {
      data: {
        surveys: [
          {
            id: 'survey-2',
            uniqueName: 'survey-2',
            name: 'The second survey',
            state: 'active',
            savedRewards: [],
            country: 'United States of America'
          }
        ]
      }
    }
  },
  {
    request: {
      query: QUERY_SURVEYS,
      variables: { keyword: 'should print empty' }
    },
    result: {
      data: {
        surveys: []
      }
    }
  },
  {
    request: { query: QUERY_SURVEYS, variables: { keyword: '' } },
    result: {
      data: {
        surveys: [
          {
            id: 'survey-1',
            uniqueName: 'survey-1',
            name: 'Survey 1',
            state: 'draft',
            savedRewards: [],
            country: 'United States of America'
          },
          {
            id: 'survey-2',
            uniqueName: 'survey-2',
            name: 'The second survey',
            state: 'active',
            savedRewards: [],
            country: 'United States of America'
          },
          {
            id: 'survey-3',
            uniqueName: 'survey-3',
            name: 'Survey 3',
            state: 'suspended',
            savedRewards: [],
            country: 'United States of America'
          }
        ]
      }
    }
  },
  {
    request: { query: QUERY_SURVEYS, variables: { keyword: '' } },
    result: {
      data: {
        surveys: [
          {
            id: 'survey-1',
            uniqueName: 'survey-1',
            name: 'Survey 1',
            state: 'draft',
            savedRewards: [],
            country: 'United States of America'
          },
          {
            id: 'survey-2',
            uniqueName: 'survey-2',
            name: 'The second survey',
            state: 'active',
            savedRewards: [],
            country: 'United States of America'
          },
          {
            id: 'survey-3',
            uniqueName: 'survey-3',
            name: 'Survey 3',
            state: 'suspended',
            savedRewards: [],
            country: 'United States of America'
          }
        ]
      }
    }
  }
]

const mockUpdateQrCode = {
  Success: {
    request: {
      query: MUTATION_UPDATE_QR_CODE,
      variables: {
        id: 'qr-code-1',
        input: {
          name: 'Link to Survey 1',
          uniqueName: 'qr-code-1',
          targetType: 'survey',
          survey: 'survey-1',
          targetLink: null
        }
      }
    },
    result: {
      data: {
        updateQrCode: {
          id: 'qr-code-1',
          uniqueName: 'qr-code-1',
          redirectLink: 'http://localhost/qr-code/org-1/link-to-survey-1',
          qrCodePhoto: 'https://s3-aws/qr-codes/org-1/link-to-survey-1'
        }
      }
    }
  },
  Failure: {
    request: {
      query: MUTATION_UPDATE_QR_CODE,
      variables: {
        id: 'qr-code-1',
        input: {
          name: 'Link to Slack',
          uniqueName: 'qr-code-1',
          targetType: 'link',
          survey: null,
          targetLink: 'https://flavor-wiki.slack.com'
        }
      }
    },
    result: {
      errors: [{ message: 'E_QR_CODE_UNIQUE' }]
    }
  }
}

describe('Edit Qr Code', () => {
  global.jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000
  let render

  async function simulateKeywordChange (keyword) {
    render
      .find('.ant-select-search__field')
      .simulate('change', { target: { value: keyword } })

    await wait(250)
    render.update()

    await wait(0)
    render.update()
  }

  async function simulateTargetTypeChange (targetType) {
    render
      .find(`input[name="targetType"][value="${targetType}"]`)
      .simulate('change', {
        target: { name: 'targetType', checked: true }
      })

    await wait(0)
    render.update()
  }
  beforeEach(() => {
    history.push.mockImplementation(jest.fn())
  })

  afterEach(() => {
    render.unmount()
  })

  test('should render without crash', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryQrCode.Link, ...mockQuerySurveys]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorEditQrCode.WrappedComponent
          match={{ params: { qrCodeId: 'qr-code-1' } }}
          history={history}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    expect(
      render.find('input[data-testid="redirect-link"]').prop('value')
    ).toBe('http://localhost/qr-code/organization-1/qr-code-1')

    render
      .find(OperatorQrCodeForm)
      .first()
      .prop('onCancel')()
    expect(history.push).toHaveBeenCalledWith('/operator/qr-codes')
  })

  test('test the search survey input', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryQrCode.Link, ...mockQuerySurveys]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorEditQrCode.WrappedComponent
          match={{ params: { qrCodeId: 'qr-code-1' } }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    await simulateTargetTypeChange('survey')

    // should show all surveys
    expect(render.find('.ant-select-selection')).toHaveLength(1)
    render.find('.ant-select-search__field').simulate('click')
    await wait(0)
    render.update()
    expect(render.find('.ant-select-dropdown-menu-item')).toHaveLength(3)

    // should not show any survey
    await simulateKeywordChange('should print empty')
    expect(render.find('.ant-select-dropdown-menu-item').text()).toBe(
      'No surveys were found'
    )

    // should show all surveys that start with the
    await simulateKeywordChange('the')
    expect(render.find('.ant-select-dropdown-menu-item')).toHaveLength(1)

    // should change survey to survey-2 when clicking on the second survey
    render
      .find('.ant-select-dropdown-menu-item')
      .at(0)
      .simulate('click')
    await wait(0)

    expect(
      render
        .find(Select)
        .find({ name: 'survey' })
        .first()
        .prop('value')
    ).toBe('survey-2')
  }, 30000)

  test('when the name is changed, the uniqueName, redirectLink should NOT be change', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryQrCode.Link, ...mockQuerySurveys]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorEditQrCode.WrappedComponent
          match={{ params: { qrCodeId: 'qr-code-1' } }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    render
      .find('[name="name"]')
      .last()
      .simulate('change', { target: { name: 'name', value: 'new qr code' } })

    await wait(0)
    render.update()

    expect(
      render
        .find('input[name="uniqueName"]')
        .getDOMNode()
        .getAttribute('value')
    ).toBe('qr-code-1')

    expect(
      render.find('input[data-testid="redirect-link"]').prop('value')
    ).toBe('http://localhost/qr-code/organization-1/qr-code-1')

    const submitvalue = {
      qrCode: {
        uniqueName: 'qr-code-1',
        qrCodePhoto: 'https://s3-aws/bucket/qr-codes/org-1/qr-code-1'
      }
    }
    render
      .find(OperatorQrCodeForm)
      .first()
      .prop('onDownload')(submitvalue)
  })

  test('should call mutation when clicking on Save button', async () => {
    const client = createApolloMockClient({
      mocks: [
        mockQueryQrCode.Link,
        ...mockQuerySurveys,
        mockUpdateQrCode.Success
      ]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorEditQrCode.WrappedComponent
          match={{ params: { qrCodeId: 'qr-code-1' } }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    // fill the name field
    render.find('input[name="name"]').simulate('change', {
      target: { name: 'name', value: 'Link to Survey 1' }
    })

    // change linkType to survey
    await simulateTargetTypeChange('survey')

    // choose the survey
    await simulateKeywordChange('Survey 1')
    render.find('.ant-select-dropdown-menu-item').simulate('click')
    await wait(0)
    render.update()

    // save
    render.find('button[data-testid="save"]').simulate('click')

    await wait(1000)
  })

  test('should show error message if can not update the Qr Code', async () => {
    const client = createApolloMockClient({
      mocks: [
        mockQueryQrCode.Survey,
        ...mockQuerySurveys,
        mockUpdateQrCode.Failure
      ]
    })

    const displayError = jest.fn()
    displayErrorPopup.mockImplementation(displayError)

    render = mount(
      <ApolloProvider client={client}>
        <OperatorEditQrCode.WrappedComponent
          match={{ params: { qrCodeId: 'qr-code-1' } }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    //
    await simulateTargetTypeChange('link')

    // fill the name field
    render.find('input[name="name"]').simulate('change', {
      target: { name: 'name', value: 'Link to Slack' }
    })

    render.find('input[name="targetLink"]').simulate('change', {
      target: { name: 'targetLink', value: 'https://flavor-wiki.slack.com' }
    })

    // save
    render.find('button[data-testid="save"]').simulate('click')

    await wait(1000)

    expect(displayError).toHaveBeenCalledWith(
      'The unique name is already being used by some other QR code'
    )
  })

  test('should render with query error', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryQrCode.Error, ...mockQuerySurveys]
    })

    render = mount(
      <ApolloProvider client={client}>
        <OperatorEditQrCode.WrappedComponent
          history={history}
          match={{ params: { qrCodeId: 'qr-code-1' } }}
        />
      </ApolloProvider>
    )
    await wait(0)

    expect(history.push).toHaveBeenCalledWith('/operator/qr-codes')
    expect(render.html()).toBe('')
  })
})
