import { pick, propOr } from 'ramda'
import React, { useState, useRef, useMemo } from 'react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import debounce from 'lodash/debounce'
import OperatorQrCodeForm from '../../components/OperatorQrCodeForm'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import { useTranslation } from 'react-i18next'
import OperatorQrCodeDownload from '../OperatorQrCodeDownload'

const getSurveys = propOr([], 'surveys')

const OperatorEditQrCode = ({ history, match }) => {
  const { t } = useTranslation()
  const { qrCodeId } = match.params
  const [keyword, setKeyword] = useState('')
  const [saving, setSaving] = useState(false)
  const [qrCodeToDownload, setQrCodeToDownload] = useState(null)
  const updateQrCode = useMutation(MUTATION_UPDATE_QR_CODE)

  // optimize: currently, we use the local search for search case insensitive
  // after implementing search insensitive on the server side
  // we will update to server search
  const { data: surveyData, loading: loadingSurveys } = useQuery(
    QUERY_SURVEYS,
    {
      fetchPolicy: 'cache-and-network',
      variables: { keyword }
    }
  )

  const { loading, data, error } = useQuery(QUERY_QR_CODE, {
    fetchPolicy: 'network-only',
    variables: { qrCodeId, checkOrg: true }
  })

  const handleSearchRef = useRef(
    debounce(keyword => {
      setKeyword(keyword)
    }, 250)
  )

  const qrCode = useMemo(() => {
    if (loading || error) {
      return {}
    }

    const { survey, ...others } = data.qrCode

    if (survey) {
      return { ...others, survey: survey.uniqueName }
    }

    return data.qrCode
  }, [loading, data, error])

  if (loading) {
    return null
  }

  if (error) {
    history.push('/operator/not-authorized')
    return null
  }

  const organizationUniqueName = qrCode.owner.uniqueName
  const handleSearch = handleSearchRef.current

  return (
    <OperatorPage>
      <OperatorPageContent>
        <OperatorQrCodeForm
          uniqueNameDisable
          i18nPrefix='components.editQrCode'
          title={t('containers.operatorEditQrCode.title')}
          loadingSurveys={loadingSurveys}
          saving={saving}
          surveys={getSurveys(surveyData)}
          values={qrCode}
          onSearch={handleSearch}
          onDownload={setQrCodeToDownload}
          organizationUniqueName={organizationUniqueName}
          onCancel={() => history.push('/operator/qr-codes')}
          onSubmit={async form => {
            try {
              setSaving(true)

              const input = pick([
                'name',
                'uniqueName',
                'targetType',
                'survey',
                'targetLink'
              ])(form)

              await updateQrCode({
                variables: {
                  id: form.id,
                  input
                }
              })

              displaySuccessMessage(
                t('containers.operatorEditQrCode.successMessage')
              )
            } catch (error) {
              displayErrorPopup(getErrorMessage(error, 'E_QR_CODE_UPDATE'))
            }
            setSaving(false)
          }}
        />
      </OperatorPageContent>
      <OperatorQrCodeDownload
        qrCode={qrCodeToDownload}
        onCancel={() => setQrCodeToDownload(null)}
      />
    </OperatorPage>
  )
}

export const MUTATION_UPDATE_QR_CODE = gql`
  mutation updateQrCode($id: ID!, $input: QrCodeUpdateInput) {
    updateQrCode(id: $id, input: $input) {
      id
      uniqueName
      redirectLink
      qrCodePhoto
    }
  }
`

export const QUERY_SURVEYS = gql`
  query surveys($keyword: String!) {
    surveys(input: { keyword: $keyword, skip: 0, limit: 20 }) {
      id
      name
      uniqueName
      state
    }
  }
`

export const QUERY_QR_CODE = gql`
  query qrCode($qrCodeId: ID!, $checkOrg: Boolean) {
    qrCode(id: $qrCodeId, checkOrg: $checkOrg) {
      id
      name
      uniqueName
      targetType
      survey {
        id
        name
        uniqueName
      }
      qrCodePhoto
      targetLink
      owner {
        id
        uniqueName
      }
    }
  }
`

export default withRouter(OperatorEditQrCode)
