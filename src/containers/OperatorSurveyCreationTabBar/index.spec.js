import React from 'react'
import { shallow } from 'enzyme'
import OperatorSurveyCreationTabBar from '.'

describe('OperatorSurveyCreationTabBar', () => {
  let testRender
  test('should render OperatorSurveyCreationTabBar', async () => {
    testRender = shallow(<OperatorSurveyCreationTabBar />)
    expect(testRender).toMatchSnapshot()
    testRender.unmount()
  })
})
