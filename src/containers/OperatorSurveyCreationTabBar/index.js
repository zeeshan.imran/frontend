import React from 'react'
import PublishSurvey from '../PublishSurvey'
import SaveSurveyDraft from '../SaveSurveyDraft'
import OperatorSurveyDetailTabBar from '../../components/OperatorSurveyDetailTabBar'

const actions = [
  <SaveSurveyDraft key='save-draft' />,
  <PublishSurvey key='publish' />
]

const OperatorSurveyCreationTabBar = () => (
  <OperatorSurveyDetailTabBar actions={actions} />
)

export default OperatorSurveyCreationTabBar
