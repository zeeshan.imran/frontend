import React, { useState } from 'react'
import gql from 'graphql-tag'
import * as Yup from 'yup'
import { Formik } from 'formik'
import { useMutation } from 'react-apollo-hooks'
import LoginFormComponent from '../../components/LoginForm'
import useToggle from '../../hooks/useToggle/index'
import {
  setAuthenticationToken,
  setAuthenticatedOrganization,
  setAuthenticatedUser,
  logout
} from '../../utils/userAuthentication/index'
import isOperator from '../../utils/isOperator'
import isPowerUser from '../../utils/isPowerUser'
import isAnalytics from '../../utils/isAnalytics'
import { useSlaask } from '../../contexts/SlaaskContext'
import { useTranslation } from 'react-i18next'
import useResponsive from '../../utils/useResponsive/index'
import { displayMessage } from '../../utils/displayMessage/index'

const LoginForm = ({
  onLoginTaster,
  onLoginOperator,
  onLoginPowerUser,
  handleNavigateToRequestAccount,
  handleNavigateToCreateTasterAccount, // Taster Account Link
  handleNavigateToForgotPassword,
  loginProfile
}) => {
  const { reloadSlaask } = useSlaask()
  const { t } = useTranslation()
  const [loginError, setLoginError] = useState('')
  const [loading, toggleLoading] = useToggle(false)
  const [userEmail, setUserEmail] = useState(null)
  const [resendEmailButton, setResendEmailButton] = useState(false)
  const login = useMutation(LOGIN_OPERATOR)
  const resendVerificationEmail = useMutation(RESEND_VERIFICATION_EMAIL)
  const { mobile } = useResponsive()

  const loginUser = async ({ email, password }) => {
    setLoginError('')
    toggleLoading()
    try {
      setUserEmail(email)
      const {
        data: {
          loginUser: { token, user }
        }
      } = await login({ variables: { email, password } })
      if (mobile && user.type === 'operator') {
        //  && !user.isTasterAndOperator
        setLoginError(t('containers.loginForm.preventMobileOperatorLogin'))
        toggleLoading()
        return
      }
      setAuthenticationToken(token)
      setAuthenticatedOrganization(user.organization && user.organization.id)
      if (!user.fullName) {
        user.fullName = user.emailAddress
      }
      setAuthenticatedUser(user)
      await reloadSlaask()

      toggleLoading()
      if (isOperator(user)) {
        onLoginOperator()
      } else if (isAnalytics(user)) {
        onLoginOperator()
      } else if (isPowerUser(user)) {
        onLoginOperator()
      } else if (user) {
        if (user.isVerified) {
          user.paypalEmailAddress !== '' ? loginProfile() : onLoginTaster()
        } else {
          displayMessage(t('containers.loginForm.verifyEmail'))
          setResendEmailButton(true)
          logout()
        }
      } else {
        throw new Error()
      }
    } catch (error) {
      const incomingErrorMessage =
        error.graphQLErrors &&
        error.graphQLErrors.map(graphQLError => graphQLError.message)[0]
      let errorMessage =
        incomingErrorMessage && incomingErrorMessage.startsWith('INVALID')
          ? t('containers.loginForm.incomingValidError')
          : t('containers.loginForm.incomingLoginError')

      setLoginError(errorMessage)
      toggleLoading()
    }
  }

  const resendVerification = async () => {
    if (userEmail) {
      const {
        data: {
          tasterResendVerificationEmail: { user }
        }
      } = await resendVerificationEmail({ variables: { email: userEmail } })
      if (user.emailAddress) {
        setResendEmailButton(false)
        displayMessage(t('containers.loginForm.verificationEmailSent'))
      }
    } else {
      displayMessage(t('containers.loginForm.resendVerifiaction'))
    }
  }

  return (
    <Formik
      validationSchema={Yup.object().shape({
        email: Yup.string()
          .email(t('containers.loginForm.validation.emailValid'))
          .required(t('containers.loginForm.validation.emailRequire')),
        password: Yup.string()
          .min(8, t('containers.loginForm.validation.passwordMin'))
          .required(t('containers.loginForm.validation.passwordRequire'))
      })}
      onSubmit={loginUser}
      render={({ values, ...rest }) => (
        <LoginFormComponent
          {...values}
          {...rest}
          loginError={loginError}
          loading={loading}
          handleNavigateToForgotPassword={handleNavigateToForgotPassword}
          handleNavigateToRequestAccount={handleNavigateToRequestAccount}
          handleNavigateToCreateTasterAccount={
            handleNavigateToCreateTasterAccount
          }
          resendEmailButton={resendEmailButton}
          resendVerification={resendVerification}
        />
      )}
    />
  )
}

const LOGIN_OPERATOR = gql`
  mutation loginUser($email: String!, $password: String!) {
    loginUser(email: $email, password: $password) {
      token
      user {
        id
        initialized
        emailAddress
        type
        isSuperAdmin
        isTaster
        fullName
        dateofbirth
        country
        state
        language
        gender
        paypalEmailAddress
        ethnicity
        incomeRange
        hasChildren
        numberOfChildren
        smokerKind
        foodAllergies
        marketResearchParticipation

        organization {
          name
          id
        }
      }
    }
  }
`

const RESEND_VERIFICATION_EMAIL = gql`
  mutation tasterResendVerificationEmail($email: String!) {
    tasterResendVerificationEmail(email: $email) {
      user {
        emailAddress
      }
    }
  }
`

export default LoginForm
