import React from 'react'
import { mount } from 'enzyme'
import PaidSurveysGrid from '.'
import Loader from '../../components/Loader'
import SurveysGridComponent from '../../components/SurveysGrid'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { MockedProvider } from 'react-apollo/test-utils'
import { PAID_SURVEY_QUERY } from '../../queries/PaidSurveysGrid'
import wait from '../../utils/testUtils/waait'

const mocks = [
  {
    request: {
      query: PAID_SURVEY_QUERY
    },
    result: { data: { surveys: [] } } 
  }
]

describe('PaidSurveysGrid', () => {
  let testRender

  let user

  beforeEach(() => {
    user = getAuthenticatedUser()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render loader', () => {
    testRender = mount(
      <MockedProvider>
        <PaidSurveysGrid desktop={''} user={user} />
      </MockedProvider>
    )

    expect(Loader).toHaveLength(1)
  })

  test('should render SurveysGridComponent', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks}>
        <PaidSurveysGrid desktop={''} user={user} />
      </MockedProvider>
    )
    await wait(0)
    testRender.update()
    expect(testRender.find(SurveysGridComponent).length).toBe(1)
  })
})
