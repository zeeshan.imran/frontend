import React from 'react'
import { PAID_SURVEY_QUERY } from '../../queries/PaidSurveysGrid'
import { Query } from 'react-apollo'
import Loader from '../../components/Loader'
import SurveysGridComponent from '../../components/SurveysGrid'

const PaidSurveysGrid = () => (
  <Query query={PAID_SURVEY_QUERY} fetchPolicy='cache-and-network'>
    {({ data: { surveys }, loading }) => {
      if (loading) return <Loader />
      return <SurveysGridComponent surveys={surveys} />
    }}
  </Query>
)

export default PaidSurveysGrid
