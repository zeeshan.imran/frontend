import React, { useState } from 'react'
import Text from '../../components/Text'
import ImportSurveySharesModal from './ImportSurveySharesModal'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

const ImportGiftCardShares = ({ state, survey, onMenuItemClick }) => {
  const { t } = useTranslation()
  const [isVisible, setIsVisible] = useState(false)

  const toggleModal = () => {
    setIsVisible(!isVisible)
  }
  return (
    <React.Fragment>
      <Text
        onClick={() => {
          toggleModal()
          onMenuItemClick()
        }}
      >
        {t('tooltips.importGiftCardShares')}
      </Text>
      <ImportSurveySharesModal
        visible={isVisible}
        state={state}
        survey={survey}
        t={t}
        toggleModal={toggleModal}
      />
    </React.Fragment>
  )
}

export default withRouter(ImportGiftCardShares)
