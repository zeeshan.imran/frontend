import React from 'react'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import ImpersonateHeaderComponent from '../../components/ImpersonateHeader'
import { getImpersonateUserId } from '../../utils/userAuthentication'

const ImpersonateHeader = ({ history }) => {
  const isImpersonating = !!getImpersonateUserId()

  const { data, loading } = useQuery(QUERY_IMPERSONATE_USER, {
    fetchPolicy: 'cache-and-network',
    skip: !isImpersonating
  })

  if (!isImpersonating || loading) {
    return null
  }

  return (
    <ImpersonateHeaderComponent
      adminUser={data.adminUser}
      impersonateUser={data.impersonateUser}
      onExit={() => {
        history.push('/impersonate/exit')
      }}
    />
  )
}

export const QUERY_IMPERSONATE_USER = gql`
  query impersonateUser {
    impersonateUser: me {
      id
      fullName
      emailAddress
    }
    adminUser: me(impersonate: false) {
      id
      fullName
      emailAddress
    }
  }
`

export default withRouter(ImpersonateHeader)
