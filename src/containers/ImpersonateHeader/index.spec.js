import React from 'react'
import { mount } from 'enzyme'
import wait from '../../utils/testUtils/waait'
import { ApolloProvider } from 'react-apollo-hooks'
import ImpersonateHeaderComponent from '../../components/ImpersonateHeader'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import ImpersonateHeader, { QUERY_IMPERSONATE_USER } from './index'
import {
  getAuthenticatedUser,
  getImpersonateUserId
} from '../../utils/userAuthentication'

jest.mock('../../utils/userAuthentication')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mockQueryImpersonateUser = {
  FullInfo: {
    request: {
      query: QUERY_IMPERSONATE_USER
    },
    result: {
      data: {
        impersonateUser: {
          id: 'operator-1',
          fullName: 'Operator 1',
          emailAddress: 'operator-1@gmail.com'
        },
        adminUser: {
          id: 'admin-1',
          fullName: 'Administrator',
          emailAddress: 'admin@gmail.com'
        }
      }
    }
  },
  MissingFullname: {
    request: {
      query: QUERY_IMPERSONATE_USER
    },
    result: {
      data: {
        impersonateUser: {
          id: 'operator-1',
          fullName: null,
          emailAddress: 'operator-1@gmail.com'
        },
        adminUser: {
          id: 'admin-1',
          fullName: null,
          emailAddress: 'admin@flavorwiki.com'
        }
      }
    }
  }
}

describe('impersonate header', () => {
  let render
  let history

  beforeEach(() => {
    history = {
      push: jest.fn()
    }
  })

  afterEach(() => {
    render.unmount()
  })

  test('should render empty when loading', async () => {
    getAuthenticatedUser.mockImplementation(() => ({
      isSuperAdmin: true
    }))
    getImpersonateUserId.mockImplementation(() => 'operator-1')

    const client = createApolloMockClient({
      mocks: [mockQueryImpersonateUser.MissingFullname]
    })

    render = mount(
      <ApolloProvider client={client}>
        <ImpersonateHeader.WrappedComponent history={history} />
      </ApolloProvider>
    )

    expect(render.isEmptyRender()).toBe(true)

    await wait(0)
    render.update()

    expect(render.find(ImpersonateHeaderComponent).text()).toBe(
      'components.impersonate.impersonatedAscomponents.impersonate.toGoToAdmin'
      // 'You (admin@flavorwiki.com) are signed in as operator-1@gmail.com.To go back to admin click here'
    )
  })

  test('should redirect to /impersonate/exit if the super admin exit impersonate', async () => {
    getAuthenticatedUser.mockImplementation(() => ({
      isSuperAdmin: true
    }))
    getImpersonateUserId.mockImplementation(() => 'operator-1')

    const client = createApolloMockClient({
      mocks: [mockQueryImpersonateUser.FullInfo]
    })

    render = mount(
      <ApolloProvider client={client}>
        <ImpersonateHeader.WrappedComponent history={history} />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    expect(render.find(ImpersonateHeaderComponent).text()).toBe(
      'components.impersonate.impersonatedAscomponents.impersonate.toGoToAdmin'
      // 'You (Administrator) are signed in as Operator 1.To go back to admin click here'
    )
    render.find('.ant-btn-link').simulate('click')

    await wait(0)
    expect(history.push).toHaveBeenCalledWith('/impersonate/exit')
  })
})
