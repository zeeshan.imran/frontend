import React, { memo, useCallback } from 'react'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import SectionSelectComponent from '../../components/QuestionCreationCardSectionSelect'

const QuestionCreationCardSectionSelect = ({ mandatoryQuestion, questionIndex, question }) => {
  const updateQuestionMutation = useMutation(UPDATE_SURVEY_CREATION_QUESTION)

  const updateQuestionSection = useCallback(
    section => {
      updateQuestionMutation({
        variables: {
          questionField: { displayOn: section },
          questionIndex,
          id: question.id
        }
      })
    },
    [questionIndex, updateQuestionMutation]
  )
  return (
    <SectionSelectComponent
      disabled={mandatoryQuestion}
      section={question.displayOn}
      onChangeSection={updateQuestionSection}
    />
  )
}

const UPDATE_SURVEY_CREATION_QUESTION = gql`
  mutation updateSurveyCreationQuestion(
    $questionField: Question
    $questionIndex: Int
  ) {
    updateSurveyCreationQuestion(
      questionField: $questionField
      questionIndex: $questionIndex
    ) @client
  }
`

export default memo(QuestionCreationCardSectionSelect)
