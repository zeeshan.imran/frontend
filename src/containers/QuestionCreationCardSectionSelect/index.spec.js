import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import QuestionCreationCardSectionSelect from '.'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import SectionSelectComponent from '../../components/QuestionCreationCardSectionSelect'
import updateSurveyCreationQuestion from '../../resolvers/updateSurveyCreationQuestion'
import wait from '../../utils/testUtils/waait'

jest.mock('../../resolvers/updateSurveyCreationQuestion')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('QuestionCreationCardSectionSelect', () => {
  let testRender
  let mandatoryQuestion
  let questionIndex
  let question
  let client

  beforeEach(() => {
    mandatoryQuestion = {}
    questionIndex = 1
    question = {
      id: 'question-1',
      displayOn: 'begin'
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render QuestionCreationCardSectionSelect', async () => {
    client = createApolloMockClient()

    testRender = mount(
      <ApolloProvider client={client}>
        <QuestionCreationCardSectionSelect
          mandatoryQuestion={mandatoryQuestion}
          questionIndex={questionIndex}
          question={question}
        />
      </ApolloProvider>
    )

    expect(SectionSelectComponent).toHaveLength(1)

    testRender
      .find(SectionSelectComponent)
      .first()
      .prop('onChangeSection')()

    await wait(0)

    expect(updateSurveyCreationQuestion).toHaveBeenCalled()
    expect(updateSurveyCreationQuestion.mock.calls[0][1]).toMatchObject({
      questionIndex: 1,
      questionField: { displayOn: undefined }
    })
  })
})
