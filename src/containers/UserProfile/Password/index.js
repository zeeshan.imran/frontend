import React from 'react'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'
import { omit } from 'ramda'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { withTranslation } from 'react-i18next'
import ChangeUserPassword from '../../../components/ChangeUserPassword'
import { TableContainer } from '../styles'
import { displayErrorPopup } from '../../../utils/displayErrorPopup'
import { displaySuccessMessage } from '../../../utils/displaySuccessMessage'

const Password = ({ t }) => {
  const changePassword = useMutation(UPDATE_USER_PASSWORD)

  const validationSchema = Yup.object().shape({
    currentPassword: Yup.string()
      .min(8, t(`containers.userProfile.validations.password.currentPassword.minChars`))
      .required(t(`containers.userProfile.validations.password.currentPassword.required`))
      .notOneOf(
        [Yup.ref('newPassword')],
        t(`containers.userProfile.validations.password.currentPassword.sameAsBefore`)
      ),
    newPassword: Yup.string()
      .min(8, t(`containers.userProfile.validations.password.newPassword.minChars`))
      .required(t(`containers.userProfile.validations.password.newPassword.required`)),
    confirmNewPassword: Yup.string()
      .oneOf([Yup.ref('newPassword')], t(`containers.userProfile.validations.password.confirmNewPassword.shouldMatch`))
      .required(t(`containers.userProfile.validations.password.confirmNewPassword.required`))
  })

  const onSubmit = async form => {
    const {
      data: { updateUserPassword }
    } = await changePassword({
      variables: omit(['confirmNewPassword'], form)
    })

    if (updateUserPassword) {
      displaySuccessMessage(t(`containers.userProfile.messages.password.success`))
    } else {
      displayErrorPopup(t(`containers.userProfile.messages.password.error`))
    }
  }

  return (
    <TableContainer>
      <Formik
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        render={({
          values,
          errors,
          touched,
          handleChange,
          setFieldValue,
          setFieldTouched,
          isSubmitting,
          handleSubmit,
          ...formikProps
        }) => (
          <ChangeUserPassword
            {...formikProps}
            {...values}
            errors={errors}
            touched={touched}
            handleSubmit={handleSubmit}
            loading={isSubmitting}
            fieldChangeHandler={field => e => {
              setFieldValue(
                field,
                e.target.value ? e.target.value.replace(/^\s+/g, '') : ''
              )
              setFieldTouched(field, true)
            }}
            dropdownChangeHandler={field => v => {
              setFieldValue(field, v)
              setFieldTouched(field, true)
            }}
          />
        )}
      />
    </TableContainer>
  )
}

const UPDATE_USER_PASSWORD = gql`
  mutation updateUserPassword(
    $currentPassword: String!
    $newPassword: String!
  ) {
    updateUserPassword(
      currentPassword: $currentPassword
      newPassword: $newPassword
    )
  }
`

export default withTranslation()(Password)
