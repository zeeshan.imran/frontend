import React from 'react'
import moment from 'moment'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { Table as AntTable, Tooltip, Icon } from 'antd'
import { withTranslation } from 'react-i18next'
import {
  TableContainer,
  FieldContainer,
  FieldTitleContainer,
  SurveyNameContainer,
  SurveyName,
  IconContainer,
  FieldText,
  PointsFieldText,
  DateFieldText,
  PointsContainer,
  ActionButtonContainer
} from '../styles'

const Personal = ({ t }) => {
  const {
    data: { surveysHistory }
  } = useQuery(SURVEYS_HISTORY, {
    fetchPolicy: 'network-only'
  })

  const columns = [
    {
      title: t('components.operatorSurveys.title'),
      dataIndex: 'listSurveys',
      render: (_, singleItem) => (
        <FieldContainer>
          <FieldTitleContainer>
            <IconContainer>
              <Icon type='check-circle' theme='filled' />
            </IconContainer>
            <SurveyNameContainer>
              <Tooltip
                placement='topLeft'
                title={singleItem.survey.name}
                mouseEnterDelay={0.25}
                overlayStyle={{ maxWidth: '40vw' }}
              >
                <SurveyName>{singleItem.survey.name}</SurveyName>
              </Tooltip>
              <FieldText>
                {t('containers.page.taster.surveys.tabs.completed')}
              </FieldText>
              <DateFieldText>
                {moment(singleItem.finsihedAt).format('DD MMM YYYY')}
              </DateFieldText>
            </SurveyNameContainer>
          </FieldTitleContainer>
          <ActionButtonContainer>
            <PointsContainer>
              <PointsFieldText>{`20PT`}</PointsFieldText>
            </PointsContainer>
          </ActionButtonContainer>
        </FieldContainer>
      )
    }
  ]

  return (
    <TableContainer>
      <AntTable
        rowKey={record => record.id}
        columns={columns}
        dataSource={surveysHistory}
        showHeader={false}
        // pagination={{
        //   pageSize: DEFAULT_N_ELEMENTS_PER_PAGE,
        //   total,
        //   current: page + 1
        // }}
        // onChange={paginationConfig => {
        //   onChangePage(paginationConfig.current - 1)
        // }}
        // loading={loading}
        // onRow={record => ({
        //   onClick: () => onClickRow(record.id)
        // })}
      />
    </TableContainer>
  )
}

const SURVEYS_HISTORY = gql`
  query surveyHistory {
    surveysHistory {
      survey {
        name
      }
      state
      finishedAt
    }
  }
`

export default withTranslation()(Personal)
