import React from 'react'
import OperatorRouteComponent from '../../components/OperatorRoute'
import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { logout } from '../../utils/userAuthentication'
import { useSlaask } from '../../contexts/SlaaskContext'

const OperatorRoute = props => {
  const { reloadSlaask } = useSlaask()
  const { error } = useQuery(GET_ME, {
    fetchPolicy: 'cache-and-network'
  })

  if (error) {
    logout()
    reloadSlaask()
  }

  return <OperatorRouteComponent {...props} />
}

const GET_ME = gql`
  query me {
    me {
      id
    }
  }
`

export default OperatorRoute
