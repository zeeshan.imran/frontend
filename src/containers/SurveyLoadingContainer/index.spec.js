import React from 'react'
import { mount } from 'enzyme'
import SurveyLoadingContainer from '.'

describe('SurveyQuestionLayout', () => {
  window.localStorage = {
    getItem: () => '{}'
  }

  let testRender

  test('should render SurveyLoadingContainer', async () => {
    testRender = mount(<SurveyLoadingContainer />)

    expect(testRender).toMatchSnapshot()

    testRender.unmount()
  })
})
