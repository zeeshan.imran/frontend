import React from 'react'
import SurveyLoading from '../../components/SurveyLoading'

const SurveyLoadingContainer = () => {
  return <SurveyLoading />
}

export default SurveyLoadingContainer
