import React, { useEffect, useState, useRef } from 'react'
import { path, append, remove, clone } from 'ramda'
import gql from 'graphql-tag'
import * as Yup from 'yup'
import TabHorizontal from '../../components/TabHorizontal'
import * as chartUtils from '../../utils/chartUtils'
import { displayInfoPopup } from '../../utils/displayInfoPopup'
import Charts from '../../components/Charts'
import StatsProductSection from '../../components/StatsProductSection'
import LoadingModal from '../../components/LoadingModal'
import { useQuery, useMutation } from 'react-apollo-hooks'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import { Alert, Button, Icon, Card, message } from 'antd'
import { useTranslation } from 'react-i18next'
import ErrorBoundary from './ErrorBoundary'
import EmptyState from './EmptyState'
import useSurveyStatsResult from './useSurveyStatsResult'
import { ERRORS, STATS_FILTER_ACCEPTED_TYPES } from '../../utils/Constants'
import StatsFilterModal from '../../components/StatsFilterModal'
import StatsOptionsForm from '../../components/StatsOptionsForm'
import { ChartsRow } from './styles'
import GeneratePdf from '../../components/GeneratePdf/index'
import { getAuthenticatedOrganization } from '../../utils/userAuthentication'
import TooltipWrapper from '../../components/TooltipWrapper'
import {
  getAvailableChartSettings,
  getDefaultChartSettings
} from '../../utils/chartSettings'
import logger from '../../utils/logger'

const getStats = path(['survey', 'stats'])
const getStatsJobGroupId = path(['survey', 'stats', 'jobGroupId'])
const getStatsJobIds = path(['survey', 'stats', 'jobIds'])
const getOwner = path(['survey', 'owner'])

const SurveyStatus = ({
  match: { params },
  history,
  generateCharts = false
}) => {
  const tabId = useRef()
  if (params.tabId && tabId.current !== params.tabId) {
    tabId.current = params.tabId
  }
  const surveyId = params.surveyId

  const { t } = useTranslation()
  const [productsSelected, setProductsSelected] = useState([])
  const [questionsSelected, setQuestionsSelected] = useState([])
  const [finalQuestionsSelected, setFinalQuestionsSelected] = useState([])
  const [filtersUpdated, setFiltersUpdated] = useState('')

  const [shouldGenerateCharts, setShouldGenerateCharts] = useState(
    generateCharts
  )
  const [hasProductSelection, setHasProductSelection] = useState(false)
  const [questionList, setQuestionList] = useState(false)

  const validationSchema = Yup.object().shape({
    tabs: Yup.array()
      .typeError(t('containers.surveyStats.validation.typeError'))
      .of(
        Yup.object().shape({
          title: Yup.string().required(),
          charts: Yup.array()
        })
      )
  })

  const handleTabChange = tabId => {
    history.push(`/operator/stats/${surveyId}/${tabId}`)
  }

  const { loading, data, error } = useQuery(
    shouldGenerateCharts ? QUERY_SURVEY_STATS : QUERY_SURVEY,
    {
      variables: {
        surveyId,
        checkOrg: true,
        productIds: shouldGenerateCharts ? productsSelected : [],
        questionFilters: filtersUpdated ? finalQuestionsSelected : []
      },
      fetchPolicy: 'network-only'
    }
  )

  const statsResults = useSurveyStatsResult()
  const jobGroupId = getStatsJobGroupId(data)
  const jobIds = getStatsJobIds(data)

  useEffect(() => {
    if (jobGroupId) {
      statsResults.startAutoRefresh(jobGroupId, jobIds, {
        query: QUERY_SURVEY_STATS,
        variables: {
          surveyId,
          checkOrg: true,
          productIds: productsSelected,
          questionFilters: finalQuestionsSelected
        }
      })
    }

    return () => {
      statsResults.stopAutoRefresh()
    }
  }, [
    statsResults,
    jobGroupId,
    jobIds,
    surveyId,
    productsSelected,
    finalQuestionsSelected
  ])

  if (error && error.message.includes(ERRORS.NOT_AUTH_SURVEY)) {
    history.push('/operator/not-authorized')
  }

  const applyFilters = async () => {
    if (hasProductSelection && !shouldGenerateCharts) {
      return displayInfoPopup(
        t('containers.surveyStats.otherFilters.selectProductsFirst', {
          buttonText: t('containers.surveyStats.productsTab.buttonText')
        })
      )
    }
    setFiltersUpdated(Math.floor(Math.random() * 1000).toString())
    setShouldGenerateCharts(true)
  }

  const saveChartsSettings = useMutation(SAVE_CHARTS_SETTINGS)
  const {
    data: { getChartsSettings = {} } = {},
    loading: chartsLoading
  } = useQuery(CHARTS_SETTINGS_QUERY, {
    fetchPolicy: 'network-only',
    variables: {
      surveyId
    }
  })

  const [chartsSettings, setChartsSettings] = useState(false)

  const setDefaultChartSettings = () => {
    stats = getStats(data)
    if (stats) {
      const defaultSettings = getDefaultChartSettings(stats)

      setChartsSettings(defaultSettings)

      saveChartsSettings({
        variables: {
          surveyId: surveyId,
          chartsSettings: defaultSettings
        }
      })
    }
  }

  const createNewSettings = (chartId, newSettings, tag) => {
    let newChartsSettings = { ...chartsSettings }

    if (chartId && chartId !== 'global' && !tag) {
      newChartsSettings = {
        ...newChartsSettings,
        [chartId]: newSettings
      }
      return newChartsSettings
    }

    stats = getStats(data)
    stats.tabs.forEach(tab => {
      tab.charts.forEach(chart => {
        const availableSettings = getAvailableChartSettings({
          chartType: chart.chart_type,
          questionType: chart.question_type,
          hasProducts: chart.question_with_products
        })
        availableSettings.forEach(setting => {
          if (setting.type === 'group-labels') {
            return
          }
          const newValue =
            tag === 'all'
              ? newChartsSettings['global'][setting.name]
              : newSettings[setting.name]
          if (
            setting.type === 'dropdown' &&
            !setting.options.map(el => el.value).includes(newValue)
          ) {
            // New value is not allowed for this setting in this chart
            return
          }
          if (tag === 'all') {
            if (setting.name in newChartsSettings['global']) {
              newChartsSettings[`${chart.question}-${chart.chart_type}`][
                setting.name
              ] = newValue
            }
          } else if (chart.chart_type === tag) {
            if (setting.name in newSettings) {
              newChartsSettings[`${chart.question}-${chart.chart_type}`][
                setting.name
              ] = newValue
            }
          }
        })
      })
    })

    return newChartsSettings
  }

  const saveSettings = (chartId, newSettings, tag) => {
    try {
      let newChartsSettings = createNewSettings(chartId, newSettings, tag)

      setChartsSettings(clone(newChartsSettings))
      saveChartsSettings({
        variables: {
          surveyId: surveyId,
          chartsSettings: newChartsSettings
        }
      })
    } catch (error) {
      message.error(t('settings.charts.error'))
    }
  }

  let tabPages = []
  let contents = []
  let stats = []

  if (data.survey) {
    if (
      data.survey.screeningQuestions &&
      data.survey.screeningQuestions.length &&
      !questionList
    ) {
      setQuestionList(
        data.survey.screeningQuestions.filter(question => {
          return STATS_FILTER_ACCEPTED_TYPES.indexOf(question.type) >= 0
        })
      )
    }
    if (
      data.survey.products &&
      data.survey.products.length >= data.survey.maxProductStatCount
    ) {
      !hasProductSelection && setHasProductSelection(true)
      tabPages = [
        {
          name: t('containers.surveyStats.productsTab.title'),
          path: 'select-products'
        }
      ]
      contents = [
        <StatsProductSection
          products={data.survey.products}
          selectedProducts={productsSelected}
          toggleProductSeletion={productId => {
            productsSelected.indexOf(productId) >= 0
              ? setProductsSelected(
                  remove(
                    productsSelected.indexOf(productId),
                    1,
                    productsSelected
                  )
                )
              : setProductsSelected(append(productId, productsSelected))
          }}
        />
      ]
    } else {
      !shouldGenerateCharts && setShouldGenerateCharts(true)
    }
  }

  useEffect(() => {
    if (loading || error) {
      return
    }

    const activeTabPage = tabPages.find(
      tabPage => tabPage.path === tabId.current
    )
    if (!activeTabPage && tabPages.length > 0 && tabId.current !== 'settings') {
      history.push(`/operator/stats/${surveyId}/${tabPages[0].path}`)
    }
  }, [
    loading,
    error,
    tabPages,
    surveyId,
    tabId.current,
    history,
    stats,
    validationSchema
  ])

  useEffect(() => {
    if (!chartsLoading && !loading) {
      // Chart settings from graphQL
      if (getChartsSettings.chartsSettings) {
        const settings = getChartsSettings.chartsSettings
        const stats = getStats(data)
        if (stats) {
          const defaultSettings = getDefaultChartSettings(stats)
          // Add missing setting or remove obsolete
          Object.keys(defaultSettings).forEach(settingKey => {
            if (settingKey === 'global') {
              return
            }
            if (settings[settingKey]) {
              Object.keys(defaultSettings[settingKey]).forEach(setting => {
                if (!settings[settingKey][setting]) {
                  // Add default if setting missing
                  settings[settingKey][setting] =
                    defaultSettings[settingKey][setting]
                }
              })
            } else {
              // Chart settings missing
              settings[settingKey] = defaultSettings[settingKey]
            }
          })
        }
        setChartsSettings(settings)
      } else {
        setDefaultChartSettings()
      }
    }
  }, [chartsLoading, loading])

  const handleEditOperatorLayout = () => {
    history.push(`/edit-operator-pdf/${surveyId}/${jobGroupId}`)
  }
  const handleEditTasterLayout = () => {
    history.push(`/edit-taster-pdf/${surveyId}/${jobGroupId}`)
  }

  if (error) {
    logger.error(error)
    return (
      <OperatorPage>
        <OperatorPageContent>
          <Alert
            type='error'
            message={t('containers.surveyStats.fetchError.message')}
            description={t('containers.surveyStats.fetchError.description')}
          />
        </OperatorPageContent>
      </OperatorPage>
    )
  }

  if (loading || (!chartsSettings && shouldGenerateCharts)) {
    return <LoadingModal visible />
  }

  const isOwner = getOwner(data) === getAuthenticatedOrganization()
  if (shouldGenerateCharts) {
    stats = getStats(data)

    tabPages =
      stats &&
      validationSchema.isValidSync(stats) &&
      chartUtils.getTabPages(stats)
    contents =
      stats &&
      validationSchema.isValidSync(stats) &&
      chartUtils.getChartTabsContent(stats).map((el, ind) => {
        el.map((chart, chart_i) =>
          Object.assign(chart, { chart_id: ind + '_' + chart_i })
        )
        return (
          <Charts
            charts={el}
            chartsSettings={chartsSettings || {}}
            saveChartsSettings={saveSettings}
            isOwner={isOwner}
            t={t}
          />
        )
      })
  }

  if (tabPages.length === 0) {
    return <EmptyState />
  }

  const isBusy =
    stats &&
    (stats.tabs || []).some(tab =>
      (tab.charts || []).some(chart => chart.loading)
    )

  return (
    <OperatorPage>
      <OperatorPageContent>
        {jobGroupId && (
          <React.Fragment>
            <GeneratePdf
              surveyId={surveyId}
              jobGroupId={jobGroupId}
              disabled={isBusy}
            />{' '}
            {isOwner && (
              <Button
                type='primary'
                disabled={isBusy}
                onClick={handleEditOperatorLayout}
              >
                {t('containers.surveyStats.editOperatorLayout')}
              </Button>
            )}{' '}
            {isOwner && data && data.survey && data.survey.showGeneratePdf && (
              <Button
                type='primary'
                disabled={isBusy}
                onClick={handleEditTasterLayout}
              >
                {t('containers.surveyStats.editTasterLayout')}
              </Button>
            )}
          </React.Fragment>
        )}

        {TabHorizontal(
          [...tabPages],
          contents,
          handleTabChange,
          tabId.current,
          isOwner && shouldGenerateCharts && (
            <Button
              type='secondary'
              size='default'
              onClick={() => {
                handleTabChange('settings')
              }}
            >
              <Icon type='setting' />
            </Button>
          )
        )}
        {tabId.current === 'settings' && (
          <ChartsRow>
            <Card
              title='Settings'
              style={{ minWidth: '930px', margin: '15px auto', padding: 0 }}
              bodyStyle={{ padding: 0 }}
              actions={[
                <TooltipWrapper helperText={t('tooltips.stats.reset')}>
                  <Button
                    key='1'
                    onClick={() => {
                      setDefaultChartSettings()
                    }}
                  >
                    {t('settings.charts.resetDefaults')}
                  </Button>
                </TooltipWrapper>,
                <TooltipWrapper helperText={t('tooltips.stats.setGlobal')}>
                  <Button
                    key='2'
                    type='primary'
                    onClick={() => {
                      saveSettings('global', chartsSettings, 'all')
                    }}
                  >
                    {t('settings.charts.setGlobal')}
                  </Button>
                </TooltipWrapper>
              ]}
            >
              <StatsOptionsForm
                settings={(chartsSettings && chartsSettings.global) || {}}
                setSettings={newSettings => {
                  setChartsSettings({
                    ...chartsSettings,
                    global: { ...chartsSettings.global, ...newSettings }
                  })
                }}
                chartType='all'
              />
            </Card>
          </ChartsRow>
        )}
      </OperatorPageContent>
      <StatsFilterModal
        modalHeaderTitle={t('containers.surveyStats.otherFilters.title')}
        questionList={questionList}
        questionsSelected={questionsSelected}
        setQuestionsSelected={setQuestionsSelected}
        finalQuestionsSelected={finalQuestionsSelected}
        resetText={t('containers.surveyStats.otherFilters.resetText')}
        onReset={() => {
          setQuestionsSelected([])
          setFinalQuestionsSelected([])
          applyFilters()
        }}
        submitText={t('containers.surveyStats.otherFilters.filterText')}
        onSubmit={() => {
          setFinalQuestionsSelected(questionsSelected)
          applyFilters()
        }}
        hasProductSelection={hasProductSelection}
        shouldGenerateCharts={shouldGenerateCharts}
        setShouldGenerateCharts={setShouldGenerateCharts}
        productsSelected={productsSelected}
        data={data}
      />
    </OperatorPage>
  )
}

const QUERY_SURVEY = gql`
  query surveyStats($surveyId: ID!, $checkOrg: Boolean) {
    survey(id: $surveyId, checkOrg: $checkOrg) {
      id
      name
      maxProductStatCount
      products {
        id
        name
        photo
      }
      screeningQuestions {
        id
        prompt
        chartTopic
        chartTitle
        type
        options {
          label
          value
        }
      }
    }
  }
`

const QUERY_SURVEY_STATS = gql`
  query surveyStats(
    $surveyId: ID!
    $productIds: [String!]
    $questionFilters: [Object!]
    $checkOrg: Boolean
  ) {
    survey(id: $surveyId, checkOrg: $checkOrg) {
      id
      owner
      showGeneratePdf
      stats(productIds: $productIds, questionFilters: $questionFilters)
    }
  }
`

const CHARTS_SETTINGS_QUERY = gql`
  query getChartsSettings($surveyId: ID!) {
    getChartsSettings(surveyId: $surveyId)
  }
`

const SAVE_CHARTS_SETTINGS = gql`
  mutation saveChartsSettings($surveyId: ID!, $chartsSettings: JSON) {
    saveChartsSettings(surveyId: $surveyId, chartsSettings: $chartsSettings)
  }
`

export default props => (
  <ErrorBoundary>
    <SurveyStatus {...props} />
  </ErrorBoundary>
)
