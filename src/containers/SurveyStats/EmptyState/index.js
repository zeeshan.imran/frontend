import React from 'react'
import { Button, Icon } from 'antd'
import { withRouter } from 'react-router-dom'
import Title from 'antd/lib/typography/Title'
import OperatorPage from '../../../components/OperatorPage'
import OperatorPageContent from '../../../components/OperatorPageContent'
import { EmptyStateWrapper } from './styles'

const EmptyState = ({ history }) => {
  return (
    <OperatorPage>
      <OperatorPageContent>
        <EmptyStateWrapper>
          <Icon type='info-circle' />
          <Title level='1'>There are no charts on this survey.</Title>
          <Button
            type='primary'
            key='console'
            onClick={() => {
              history.goBack()
            }}
          >
            Go back
          </Button>
        </EmptyStateWrapper>
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default withRouter(EmptyState)
