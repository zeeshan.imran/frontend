import styled from 'styled-components'

export const EmptyStateWrapper = styled.div`
  text-align: center;
  margin-top: 10rem;

  svg {
    width: 48px;
    height: 48px;
  }

  h1 {
    margin-top: 3rem;
    margin-bottom: 2rem;
  }
`
