import React from 'react'
import { Alert } from 'antd'
import OperatorPage from '../../../components/OperatorPage'
import OperatorPageContent from '../../../components/OperatorPageContent'
import { withTranslation } from 'react-i18next'
import logger from '../../../utils/logger'

class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError (error) {
    // report to sentry
    logger.error(error)

    return { hasError: true }
  }

  render () {
    const { t } = this.props
    if (this.state.hasError) {
      return (
        <OperatorPage>
          <OperatorPageContent>
            <Alert
              type='error'
              message={t('containers.surveyStats.invalidError.message')}
              description={t('containers.surveyStats.invalidError.description')}
            />
          </OperatorPageContent>
        </OperatorPage>
      )
    }

    return this.props.children
  }
}

export default withTranslation()(ErrorBoundary)
