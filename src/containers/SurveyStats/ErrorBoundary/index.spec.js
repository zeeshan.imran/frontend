import React from 'react'
import { shallow } from 'enzyme'
import ErrorBoundary from '.'
import sinon from 'sinon'
import OperatorPage from '../../../components/OperatorPage'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('ErrorBoundary', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
    sinon.restore()
  })

  test('should render ErrorBoundary', async () => {
    const spy = sinon.spy()
    testRender = shallow(
      <ErrorBoundary spy={spy}>
        <OperatorPage />
      </ErrorBoundary>
    )
    const error = new Error('hi!')
    testRender.find(OperatorPage).simulateError(error)
  })
})
