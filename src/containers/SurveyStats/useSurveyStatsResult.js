import { useCallback, useMemo, useRef } from 'react'
import { without } from 'ramda'
import gql from 'graphql-tag'
import { useApolloClient } from 'react-apollo-hooks'

const useSurveyStatsResult = () => {
  const client = useApolloClient()
  let timer = useRef(null)

  const startAutoRefresh = useCallback(
    (jobGroupId, jobIds, cacheQuery) => {
      const refresh = async () => {
        const data = client.cache.readQuery(cacheQuery)

        const {
          data: { statsResults }
        } = await client.query({
          query: STATS_RESULTS,
          variables: { jobGroupId, jobIds },
          fetchPolicy: 'network-only'
        })

        for (let statsResult of statsResults) {
          if (['ERROR', 'OK'].includes(statsResult.status)) {
            jobIds = without(statsResult.jobId)(jobIds)
            for (let tab of data.survey.stats.tabs) {
              for (let chart of tab.charts) {
                if (chart.jobId === statsResult.jobId) {
                  tab.completedCount = (tab.completedCount || 0) + 1
                  Object.assign(chart, statsResult.result, {
                    loading: false
                  })
                }
              }
            }
          }
        }

        client.cache.writeQuery({
          ...cacheQuery,
          data
        })

        if (jobIds.length > 0) {
          timer.current = setTimeout(refresh, 500)
        }
      }

      refresh()
    },
    [client]
  )

  const stopAutoRefresh = useCallback(() => {
    clearTimeout(timer.current)
  }, [])

  return useMemo(() => ({ startAutoRefresh, stopAutoRefresh }), [
    startAutoRefresh,
    stopAutoRefresh
  ])
}

const STATS_RESULTS = gql`
  query getStatsResults($jobGroupId: ID!, $jobIds: [ID!]) {
    statsResults: getStatsResults(jobGroupId: $jobGroupId, jobIds: $jobIds)
  }
`

export default useSurveyStatsResult
