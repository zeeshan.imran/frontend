import React from 'react'
import { create } from 'react-test-renderer'
import wait from '../../utils/testUtils/waait'
import { ApolloProvider as ApolloProviderHooks } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import QuestionCreationCardHeader from './index'
import { Checkbox } from 'antd'
import QuestionCreationCardHeaderComponent from '../../components/QuestionCreationCardHeader'
import sinon from 'sinon'
import gql from 'graphql-tag'
import { mount } from 'enzyme'
import AlertModal from '../../components/AlertModal'
import { defaultSurvey } from '../../mocks'

jest.mock('../../components/AlertModal')

const REMOVE_SURVEY_CREATION_QUESTION = gql`
  mutation removeSurveyCreationQuestion($questionIndex: Number) {
    removeSurveyCreationQuestion(questionIndex: $questionIndex) @client
  }
`
const mockAlertModal = ({ handleOk, handleCancel }) => {
  handleOk()
  handleCancel()
}
AlertModal.mockImplementation(mockAlertModal)

const mockSurveyCreation = {
  ...defaultSurvey,
  basics: {
    ...defaultSurvey.basics,
    __typename: undefined,
    customButtons: {
      continue: 'Continue',
      start: 'Start',
      next: 'Next',
      skip: 'Skip'
    },
    autoAdvanceSettings: {
      active: false,
      debounce: 0,
      hideNextButton: false
    },
    pdfFooterSettings: {
      active: false,
      footerNote:""
    }
  },
  surveyId: 'survey-1',
  questions: [
    {
      id: 'question-1',
      required: true
    }
  ]
}

describe('should set required to true while creating a new question', () => {
  let testRender
  let client
  let spyChange

  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should set required = false while unchecking the Checkbox', async () => {
    testRender = create(
      <ApolloProviderHooks client={client}>
        <QuestionCreationCardHeader
          questionIndex={0}
          question={{ required: true }}
        />
      </ApolloProviderHooks>
    )

    testRender.root
      .findByType(Checkbox)
      .props.onChange({ target: { checked: false } })
    await wait(0)

    const { surveyCreation } = client.cache.readQuery({
      query: SURVEY_CREATION
    })
    expect(surveyCreation).toHaveProperty('questions.length', 1)
    expect(surveyCreation).toHaveProperty('questions.0.required', false)
  })

  test('should set required = false while unchecking the Checkbox', async () => {
    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: REMOVE_SURVEY_CREATION_QUESTION }))
      .callsFake(spyChange)
    stub.callThrough()

    testRender = mount(
      <ApolloProviderHooks client={client}>
        <QuestionCreationCardHeader
          questionIndex={0}
          question={{ required: true }}
        />
      </ApolloProviderHooks>
    )
    spyChange.mockReset()
    testRender.find(QuestionCreationCardHeaderComponent).prop('handleRemove')()

    await wait(0)
    spyChange.mockReset()
    testRender.find(QuestionCreationCardHeaderComponent).prop('handleCopy')()
  })

  test('should set required = true while checking the Checkbox', async () => {
    testRender = create(
      <ApolloProviderHooks client={client}>
        <QuestionCreationCardHeader
          questionIndex={0}
          question={{ required: false }}
        />
      </ApolloProviderHooks>
    )

    testRender.root
      .findByType(Checkbox)
      .props.onChange({ target: { checked: true } })
    await wait(0)

    const { surveyCreation } = client.cache.readQuery({
      query: SURVEY_CREATION
    })
    expect(surveyCreation).toHaveProperty('questions.length', 1)
    expect(surveyCreation).toHaveProperty('questions.0.required', true)
  })
})
