import React, { useCallback, memo } from 'react'
import QuestionCreationCardTypeSelectComponent from '../../components/QuestionCreationCardTypeSelect'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import {
  SCREENING_QUESTION_TYPES,
  BEFORE_QUESTION_TYPES,
  LOOP_QUESTION_TYPES,
  AFTER_QUESTION_TYPES,
  PAYMENT_QUESTION_TYPES
} from '../../utils/Constants'

const QuestionCreationCardTypeSelect = ({
  questionIndex,
  question,
  mandatoryQuestion
}) => {
  const {
    data: { surveyCreation = {} }
  } = useQuery(GET_QUESTION_TYPES)

  const { uniqueQuestionsToCreate = [] } = surveyCreation

  const updateQuestionTypeMutation = useMutation(
    UPDATE_SURVEY_CREATION_QUESTION_TYPE
  )
  const updateQuestionType = useCallback(
    type => {
      updateQuestionTypeMutation({
        variables: {
          questionType: type,
          questionIndex,
          id: question.id
        }
      })
    },
    [questionIndex, updateQuestionTypeMutation]
  )

  const updateQuestionMutation = useMutation(UPDATE_SURVEY_CREATION_QUESTION)
  const updateQuestion = useCallback(
    field => {
      updateQuestionMutation({
        variables: {
          questionField: field,
          questionIndex,
          mandatoryQuestion
        }
      })
    },
    [questionIndex, updateQuestionMutation, mandatoryQuestion]
  )

  let types
  switch (question.displayOn) {
    case 'payments':
      types = PAYMENT_QUESTION_TYPES
      break
    case 'screening':
      types = SCREENING_QUESTION_TYPES
      break
    case 'begin':
      types = BEFORE_QUESTION_TYPES
      break
    case 'middle':
      types = LOOP_QUESTION_TYPES
      break
    case 'end':
      types = AFTER_QUESTION_TYPES
      break
    default:
      types = LOOP_QUESTION_TYPES
  }

  if (question.type === 'choose-product' || question.type === 'paypal-email' || question.type === 'choose-payment') {
    return null
  }

  const isStrictMode = question.editMode === 'strict'

  return (
    <QuestionCreationCardTypeSelectComponent
      disabled={mandatoryQuestion || isStrictMode}
      types={types}
      type={question.type}
      existingUniqueTypes={uniqueQuestionsToCreate}
      onSelect={data => {
        if (question.type === 'info') {
          updateQuestion({ secondaryPrompt: '' })
        }
        updateQuestionType(data)
      }}
    />
  )
}

const GET_QUESTION_TYPES = gql`
  query {
    surveyCreation @client {
      products
      uniqueQuestionsToCreate
    }
  }
`

const UPDATE_SURVEY_CREATION_QUESTION_TYPE = gql`
  mutation updateSurveyCreationQuestionType(
    $questionType: String
    $questionIndex: Int
  ) {
    updateSurveyCreationQuestionType(
      questionType: $questionType
      questionIndex: $questionIndex
    ) @client
  }
`
const UPDATE_SURVEY_CREATION_QUESTION = gql`
  mutation updateSurveyCreationQuestion(
    $questionField: Question
    $questionIndex: Int
    $mandatoryQuestion: Boolean
  ) {
    updateSurveyCreationQuestion(
      questionField: $questionField
      questionIndex: $questionIndex
      mandatoryQuestion: $mandatoryQuestion
    ) @client
  }
`

export default memo(QuestionCreationCardTypeSelect)
