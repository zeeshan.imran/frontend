import React from 'react'
import { mount } from 'enzyme'
import ExportGiftCardIncentives from '.'
import gql from 'graphql-tag'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import sinon from 'sinon'

describe('ExportGiftCardIncentives', () => {
  let testRender
  let client
  let spyChange
  let history
  let survey

  beforeEach(() => {
    client = createApolloMockClient()
    survey = {id: 1, referralAmount: '1'}
    const GET_SURVEY_SHARES = gql`
      mutation getProductIncentives($surveyId: ID!) {
        getProductIncentives(surveyId: $surveyId) {
          email
          amount
          currency
        }
      }
    `

    client.cache.writeQuery({
      query: GET_SURVEY_SHARES,
      variables: { surveyId: 'survey-1' },
      data: {
        getProductIncentives: {
          __typename: 'SurveyShares',
          email: 'test@test.com',
          amount: '1',
          currency: 'USD'
        }
      }
    })

    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub.withArgs(sinon.match({ mutation: GET_SURVEY_SHARES })).callsFake(spyChange)
    stub.callThrough()

    history = createBrowserHistory()
  })

  afterEach(() => {
    testRender.unmount()
  })
  
  test('should render ExportProductGiftCardIncentives', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <ExportGiftCardIncentives survey={survey} />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(ExportGiftCardIncentives)).toHaveLength(1)
  })
})
