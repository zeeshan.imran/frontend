import React, { useState, useRef } from 'react'
import moment from 'moment'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useMutation } from 'react-apollo-hooks'
import Text from '../../components/Text'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { CSVLink } from 'react-csv'

const ExportGiftCardShares = ({ survey, onMenuItemClick }) => {
  const { t } = useTranslation()
  const csvLinkRef = useRef()
  const getSurveySharesMutation = useMutation(GET_GIFT_CARD_SHARES)
  const [downloadData, setDownloadData] = useState([])
  const [filename, setFilename] = useState(
    `${survey.uniqueName}-giftcard-payment-${moment(new Date()).format(
      'YYYYMMDD-HHmmss'
    )}.csv`
  )

  const filterData = async () => {
    displaySuccessMessage(
      t('containers.exportSharingContainer.popups.downloading')
    )

    if(!survey.isGiftCardSelected){
      displayErrorPopup(t('containers.exportSharingContainer.popups.giftCardDisabled'))
      return
    }

    const {
      data: { getGiftCardShares }
    } = await getSurveySharesMutation({
      variables: {
        surveyId: survey.id
      }
    })
    if (getGiftCardShares.length) {
      await downloadCSVFile(getGiftCardShares, false) //Download sheet For Paypal
      await downloadCSVFile(getGiftCardShares, true) //Download sheet For internal use
    } else {
      displayErrorPopup(t('containers.exportSharingContainer.popups.error'))
    }
    setDownloadData([])
  }

  const downloadCSVFile = async (getSurveyShares, internal = false) => {
    const finalData = []
    getSurveyShares.forEach(surveyShare => {
      if (internal) {
        if (surveyShare.refferedIds && surveyShare.refferedIds.length) {
          surveyShare.refferedIds.forEach(id => {
            finalData.push({ id: id })
          })
        }
        setFilename(`internal-${filename}`)
      } else {
        finalData.push({
          email: surveyShare.email,
          amount: surveyShare.amount,
          currency: surveyShare.currency
        })
      }
    })
    setDownloadData(finalData)
    displaySuccessMessage(
      t('containers.exportSharingContainer.popups.downloaded')
    )
    csvLinkRef.current.link.click() // Trigger CSVLink automatically
    setFilename(
      `${survey.uniqueName}-giftcard-payment-${moment(new Date()).format(
        'YYYYMMDD-HHmmss'
      )}.csv`
    )
  }

  if (survey && parseInt(survey.referralAmount, 10)) {
    return (
      <React.Fragment>
        <Text
          onClick={() => {
            filterData()
            onMenuItemClick()
          }}
        >
          {t(
            'tooltips.exportGiftCardSharesButton'
          )}
        </Text>
        {downloadData.length ? (
          <CSVLink
            filename={filename}
            target='_self'
            ref={csvLinkRef}
            data={downloadData}
          />
        ) : null}
      </React.Fragment>
    )
  }
  return null
}

export default withRouter(ExportGiftCardShares)

const GET_GIFT_CARD_SHARES = gql`
  mutation getGiftCardShares($surveyId: ID!) {
    getGiftCardShares(surveyId: $surveyId) {
      email
      amount
      currency
      refferedIds
    }
  }
`
