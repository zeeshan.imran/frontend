import React, { useEffect, useRef } from 'react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import OperatorSurveyCreateCoverSelectComponent from '../../components/OperatorSurveyCreateCoverSelect'

const getProductId = product => product.clientGeneratedId || product.id

const OperatorSurveyCreateCoverSelect = ({stricted}) => {
  const {
    data: {
      surveyCreation: { products, basics }
    },
    refetch: refetchCurrent
  } = useQuery(SURVEY_CREATION)
  
  const refetch = useRef(refetchCurrent)
  useEffect(() => {
    if(refetch && refetch.current && typeof refetch.current === 'function'){
      refetch.current()
    }
  }, [products])
  
  const updateProducts = useMutation(UPDATE_SURVEY_CREATION_PRODUCTS)
  const updateBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)

  const coverProduct = products.find(p => p.isSurveyCover)

  const selectProduct = selectedProduct => {
    const nextProducts = products
      // clear cover
      .map(product => {
        if (product.isSurveyCover) {
          return {
            ...product,
            isSurveyCover: false
          }
        }

        return product
      })
      // set new cover
      .map(product => {
        if (
          selectedProduct &&
          getProductId(product) === getProductId(selectedProduct)
        ) {
          return {
            ...product,
            isSurveyCover: true
          }
        }

        return product
      })
    updateProducts({
      variables: {
        products: nextProducts
      }
    })
  }

  const productSorting = sortedProducts => {
    updateProducts({
      variables: {
        products: sortedProducts
      }
    })
  }

  const setProductDisplayType = (productDisplayType, sortedProducts) => {
    const basicsUpdated = { ...basics, productDisplayType: productDisplayType }
    updateProducts({
      variables: {
        products: sortedProducts
      }
    })
    updateBasics({
      variables: {
        basics: basicsUpdated
      }
    })
  }

  return (
    <OperatorSurveyCreateCoverSelectComponent
      selectedProduct={coverProduct}
      onSelectProductChange={product => {
        selectProduct(product)
      }}
      products={products}
      productSorting={productSorting}
      setProductDisplayType={setProductDisplayType}
      productDisplayType={basics.productDisplayType}
      stricted={stricted}
    />
  )
}

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
      basics {
        productDisplayType
      }
    }
  }
`

const UPDATE_SURVEY_CREATION_PRODUCTS = gql`
  mutation updateSurveyCreationProducts($products: products) {
    updateSurveyCreationProducts(products: $products) @client
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`

export default OperatorSurveyCreateCoverSelect
