import React from 'react'
import gql from 'graphql-tag'
import { find, pluck } from 'ramda'
import { useQuery } from 'react-apollo-hooks'
import SpiderChart from '../../components/SpiderChart'
import FillerLoader from '../../components/FillerLoader'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { questionInfo } from '../../fragments/survey'
import { SURVEY_QUERY } from '../../queries/Survey'

const ProfileCharts = ({ questions }) => {
  const {
    data: {
      currentSurveyParticipation: {
        surveyId,
        selectedProduct,
        surveyEnrollmentId,
        answers
      }
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)
  const {
    data: { question = { pairsOptions: {}, options: {}, type: '' } } = {},
    loading: questionLoading
  } = useQuery(QUESTION_QUERY, {
    variables: {
      id: questions[0]
    },
    fetchPolicy: 'cache-and-network'
  })
  const {
    data: { survey: { products = [] } = {} } = {},
    loading: productsLoading
  } = useQuery(SURVEY_QUERY, {
    variables: {
      id: surveyId
    },
    fetchPolicy: 'cache-and-network'
  })

  const selectedProductName = (
    products.find(p => p.id === selectedProduct) || {}
  ).name

  let labels = []

  if (question.pairsOptions && question.pairsOptions.elementsInPairs) {
    labels = pluck('pair', question.pairsOptions.elementsInPairs)
  } else if (question.sliderOptions && question.sliderOptions.sliders) {
    labels = question.sliderOptions.sliders.map(slider => `${slider.label}`)
  }
  let profileData = []
  let chartLoadingData

  const {
    data: { tastingProfileData = {} } = {},
    loading: pairedLoading
  } = useQuery(PROFILE_CHART_QUERY, {
    variables: {
      input: {
        enrollment: surveyEnrollmentId,
        survey: surveyId,
        product: selectedProduct,
        question: questions[0],
        labels: labels,
        profileName: selectedProductName || ''
      }
    },
    skip: !question || !['paired-questions', 'slider'].includes(question.type),
    fetchPolicy: 'network-only'
  })

  if (question.type === 'slider' && !pairedLoading) {
    const answer = find(
      q =>
        q.id.split &&
        q.id.split('-')[0] === question.id &&
        q.context &&
        q.context.selectedProduct === selectedProduct,
      answers
    )
    let results = []
    tastingProfileData.labels.forEach(label => {
      const labelResult = find(el => el.label === label, answer.values)
      results.push(labelResult && labelResult.value)
    })
    tastingProfileData.series[0].data = results
    profileData = tastingProfileData
    chartLoadingData = pairedLoading
  } else if (question.type === 'paired-questions') {
    profileData = tastingProfileData
    chartLoadingData = pairedLoading
  }

  if (chartLoadingData || questionLoading || productsLoading) {
    return <FillerLoader />
  }

  return (
    <React.Fragment>
      <SpiderChart
        labels={profileData.labels || []}
        values={profileData.series || []}
        question={question}
      />
    </React.Fragment>
  )
}

const PROFILE_CHART_QUERY = gql`
  query tastingProfileData($input: TastingProfileDataInput!) {
    tastingProfileData(input: $input) {
      labels
      series {
        name
        data
      }
    }
  }
`

const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      ...questionInfo
    }
  }
  ${questionInfo}
`

export default ProfileCharts
