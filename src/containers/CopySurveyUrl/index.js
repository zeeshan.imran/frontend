import React from 'react'
import CopyToClipboard from '../../components/CopyToClipboard'
import { useTranslation } from 'react-i18next';

const CopySurveyUrlButton = ({ uniqueName }) => {
  const { t } = useTranslation();
  return (
    <CopyToClipboard
      target={`${window.location.origin}/survey/${uniqueName}`}
      tooltip={ t('tooltips.copySurveyUrlButton') }
    />
  )
}

export default CopySurveyUrlButton
