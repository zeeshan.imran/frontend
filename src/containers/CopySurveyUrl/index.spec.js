import React from 'react'
import { shallow } from 'enzyme'
import CopySurveyUrlButton from '.'
import CopyToClipboard from '../../components/CopyToClipboard'
import wait from '../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('CopySurveyUrlButton', () => {
  let testRender
  let uniqueName

  beforeEach(() => {
    uniqueName = 'unique-1'
  })

  test('should render loader', async () => {
    testRender = shallow(<CopySurveyUrlButton uniqueName={uniqueName} />)

    await wait(0)

    expect(CopyToClipboard).toMatchSnapshot()

    testRender.unmount()
  })
})
