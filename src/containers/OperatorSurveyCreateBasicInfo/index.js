import React from 'react'
import SurveyBasicInfoForm from '../../components/SurveyBasicInfoForm'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { withTranslation } from 'react-i18next'
import AlertModal from '../../components/AlertModal'
import { trimSpaces } from '../../utils/trimSpaces'

// this is a simple standardize HTML
// it does not provide absolute correct result
// for comparsion purpose only
// please don't use for the other purposes
const standardizeHtml = html => {
  // use HTML entities ("Ö" from translation is modified by tinyMCE to "&ouml")
  var txt = document.createElement('textarea')
  txt.innerHTML = html
  return txt.value
}

const formatText = text =>
  text
    .replace(/[ \n]/gi, '')
    .replace(/['"]/gi, "'")
    .replace(/(<p>)|(<\/p)>/gi, '')

const htmlEquals = (source, target) =>
  standardizeHtml(formatText(source || '')) ===
  standardizeHtml(formatText(target || ''))

const OperatorSurveyCreateBasicInfo = ({
  t,
  i18n,
  surveyId,
  isDraft = false
}) => {
  const {
    data: {
      surveyCreation: { basics }
    }
  } = useQuery(SURVEY_CREATION)

  const updateBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)
  const {
    data: { isUniqueNameDuplicated }
  } = useQuery(IS_UNIQUE_NAME_DUPLICATED, {
    variables: {
      surveyId: surveyId,
      uniqueName: basics.uniqueName && trimSpaces(basics.uniqueName)
    },
    fetchPolicy: 'cache-and-network'
  })

  const handleChange = value => {
    const basicsUpdated = { ...basics, ...value }

    updateBasics({
      variables: {
        basics: basicsUpdated
      }
    })
  }

  // const handleReferralAmountChange = value => {
  //   const numberValue = parseInt(value, 10)
  //   if (numberValue) {
  //     requirePaypalEmailStep()
  //   } else {
  //     unrequirePaypalEmailStep()
  //   }
  //   handleChange({ referralAmount: numberValue })
  // }

  const handleCustomButtonChange = (key, value) => {
    handleChange({
      customButtons: {
        ...basics.customButtons,
        [key]: value
      }
    })
    return null
  }

  // change here
  const callHandleChange = (defaultGroups, oldT, newT, enT, value) => {
    let updatedValues = {
      instructionSteps: [],
      customButtons: {}
    }

    let basicProps = {
      instructionsText: '',
      thankYouText: '',
      rejectionText: '',
      screeningText: '',
      customizeSharingMessage: '',
      loginText: '',
      pauseText: '',
      ...basics,
      instructionSteps: [...basics.instructionSteps, '', '', '', '', '', '']
    }

    const getTranslation = tKey => {
      if (tKey.includes('customizeSharingMessage')) {
        return {
          newKeyVal: newT(tKey, { amount: '{{amount}}' }),
          oldKeyVal: oldT(tKey, { amount: '{{amount}}' }),
          englishKeyVal: enT(tKey, { amount: '{{amount}}' })
        }
      }
      return {
        newKeyVal: newT(tKey),
        oldKeyVal: oldT(tKey),
        englishKeyVal: enT(tKey)
      }
    }

    defaultGroups.forEach(groupName => {
      if (basicProps[groupName] && typeof basicProps[groupName] === 'object') {
        for (let key of Object.keys(basicProps[groupName])) {
          const tKey = `defaultValues.${groupName}.${key}`
          const { newKeyVal, oldKeyVal, englishKeyVal } = getTranslation(tKey)

          if (
            htmlEquals(basicProps[groupName][key], oldKeyVal) ||
            basicProps[groupName][key] === '' ||
            basicProps[groupName][key] === null ||
            basicProps[groupName][key] === undefined
          ) {
            if (
              (englishKeyVal !== newKeyVal || value === 'en') &&
              newKeyVal !== tKey &&
              newKeyVal !== ''
            ) {
              updatedValues[groupName][key] = newKeyVal
            }
          } else if (basicProps[groupName][key] !== '') {
            updatedValues[groupName][key] = basicProps[groupName][key]
          }
        }
      } else {
        const tKey = `defaultValues.${groupName}`
        const { newKeyVal, oldKeyVal, englishKeyVal } = getTranslation(tKey)

        if (
          htmlEquals(basicProps[groupName], oldKeyVal) ||
          basicProps[groupName] === '' ||
          basicProps[groupName] === null ||
          basicProps[groupName] === undefined
        ) {
          if (
            (englishKeyVal !== newKeyVal || value === 'en') &&
            newKeyVal !== tKey &&
            newKeyVal !== ''
          ) {
            updatedValues[groupName] = newKeyVal
          } else {
            updatedValues[groupName] = basicProps[groupName]
          }
        }
      }
    })

    handleChange({
      surveyLanguage: value,
      ...updatedValues
    })
  }

  // change here
  const handleLanguageChange = (value, setFieldValue, field) => {
    const oldT = i18n.getFixedT(basics.surveyLanguage)
    const newT = i18n.getFixedT(value)
    const enT = i18n.getFixedT('en')
    let isChangeLanguageAlert = false

    const defaultGroups = [
      'customButtons',
      'instructionSteps',
      'instructionsText',
      'thankYouText',
      'rejectionText',
      'screeningText',
      'customizeSharingMessage',
      'loginText',
      'pauseText'
    ]
    defaultGroups.forEach(groupName => {
      for (let key in basics[groupName]) {
        if (key !== '__typename') {
          isChangeLanguageAlert = true
        }
      }
    })

    if (!isChangeLanguageAlert) {
      callHandleChange(defaultGroups, oldT, newT, enT, value)
      setFieldValue(field, value)
    } else {
      AlertModal({
        title: t('containers.operatorSurveyInfo.alertModalLanTitle'),
        okText: t('containers.operatorSurveyInfo.alertModalLanOkText'),
        description: t(
          'containers.operatorSurveyInfo.alertModalkLanDescription'
        ),
        handleOk: () => {
          callHandleChange(defaultGroups, oldT, newT, enT, value)
          setFieldValue(field, value)
        }
      })
    }
  }
  return (
    <SurveyBasicInfoForm
      isNewSurvey={!surveyId || isDraft}
      isUniqueNameDuplicated={isUniqueNameDuplicated}
      onChange={handleChange}
      handleCustomButtonChange={handleCustomButtonChange}
      handleLanguageChange={handleLanguageChange}
      country={basics.country}
      {...basics}
    />
  )
}

const IS_UNIQUE_NAME_DUPLICATED = gql`
  query isUniqueNameDuplicated($surveyId: ID, $uniqueName: String) {
    isUniqueNameDuplicated(surveyId: $surveyId, uniqueName: $uniqueName)
  }
`

//
export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        name
        coverPhoto
        instructionsText
        instructionSteps
        thankYouText
        rejectionText
        screeningText
        uniqueName
        authorizationType
        exclusiveTasters
        allowRetakes
        forcedAccount
        forcedAccountLocation
        referralAmount
        savedRewards
        recaptcha
        minimumProducts
        maximumProducts
        surveyLanguage
        country
        customizeSharingMessage
        loginText
        pauseText
        allowedDaysToFillTheTasting
        isPaypalSelected
        isGiftCardSelected
        isScreenerOnly
        productDisplayType
        showGeneratePdf
        linkedSurveys
        autoAdvanceSettings {
          active
          debounce
          hideNextButton
        }
        pdfFooterSettings {
          active
          footerNote
        }
        maxProductStatCount
        customButtons {
          continue
          start
          next
          skip
        }
      }
      questions
      mandatoryQuestions
      uniqueQuestionsToCreate
      editMode
    }
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`

export default withTranslation()(OperatorSurveyCreateBasicInfo)
