import React from 'react'
import { mount } from 'enzyme'
import OperatorSurveyCreateBasicInfo, { SURVEY_CREATION, UPDATE_SURVEY_CREATION_BASICS } from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import SurveyBasicInfoForm from '../../components/SurveyBasicInfoForm'
import i18n from '../../utils/internationalization/i18n'
import sinon from 'sinon'
import { defaultSurvey } from '../../mocks'

jest.mock('../../components/AlertModal')

const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    name: 'Survey 1',
    uniqueName: 'survey-1'
  },
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ]
}

describe('OperatorSurveyCreateBasicInfo', () => {
  let testRender
  let client
  let spyChange
  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: UPDATE_SURVEY_CREATION_BASICS }))
      .callsFake(spyChange)
    stub.callThrough()
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render OperatorSurveyCreateBasicInfo', () => {
    const mockChangeLang = jest.fn()
    i18n.changeLanguage = mockChangeLang
    i18n.language = 'en'

    testRender = mount(
      <ApolloProvider client={client}>
        <OperatorSurveyCreateBasicInfo surveyId={1} />
      </ApolloProvider>
    )
    expect(SurveyBasicInfoForm).toHaveLength(1)
  })

  test('should change custom buttons and language', () => {
    const mockChangeLang = jest.fn()
    i18n.changeLanguage = mockChangeLang
    i18n.language = 'en'

    testRender = mount(
      <ApolloProvider client={client}>
        <OperatorSurveyCreateBasicInfo surveyId={1} />
      </ApolloProvider>
    )
    const handleButtonsChange = testRender
      .find(SurveyBasicInfoForm)
      .prop('handleCustomButtonChange')
    const handleLanguageChange = testRender
      .find(SurveyBasicInfoForm)
      .prop('handleLanguageChange')

    handleLanguageChange('de', jest.fn())
    i18n.t = jest.fn(() => 'Fortsetzen')
    handleButtonsChange('continue', 'new Continue')

    handleButtonsChange('continue', 'new Continue')
    i18n.t = jest.fn(() => 'not Fortsetzen')
    handleButtonsChange('continue', 'new Continue')

    handleLanguageChange('en', jest.fn())
  })
})
