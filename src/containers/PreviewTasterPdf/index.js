import React, { useEffect, useState, useMemo } from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import Page from './Page'
import * as pdfUtils from '../../utils/pdfUtils'
import useChartResults from '../../hooks/useChartResults'
import { getColWidth, getRowHeight, PAPER_SIZES } from '../../utils/paperSizes'
import useRenderingState from '../../hooks/useRenderingState'
import { TasterPdfContainer } from './styles'

const DEFAULT =
  localStorage.getItem('pdf-layout') &&
  JSON.parse(localStorage.getItem('pdf-layout'))

const DEFAULT_PAPER = PAPER_SIZES[(DEFAULT && DEFAULT.paper) || 'A4']

const PreviewTasterPdf = ({
  paperSize = DEFAULT_PAPER,
  savedLayout = DEFAULT,
  jobGroupId,
  surveyId
}) => {
  const { loading, stats, settings } = useChartResults({ jobGroupId, surveyId })
  const { loading: loadingSurvey, data } = useQuery(QUERY_SURVEY, {
    variables: { surveyId }
  })
  const questions = useMemo(
    () =>
      data &&
      data.survey && [
        ...data.survey.setupQuestions,
        ...data.survey.productsQuestions,
        ...data.survey.paymentQuestions,
        ...data.survey.screeningQuestions,
        ...data.survey.finishingQuestions
      ],
    [data]
  )
  const { rendering, showRendering } = useRenderingState()
  const zoom = 1.5
  const colWidth = getColWidth(paperSize) * zoom
  const rowHeight = getRowHeight(paperSize) * zoom
  const [pages, setPages] = useState([])

  useEffect(() => {
    if (!loading && !loadingSurvey) {
      const formatStats = stats.map(chart => {
        const question = questions.find(q => q.id === chart.result.question)
        return {
          ...chart,
          result: {
            ...chart.result,
            title: (
              <span>
                <strong>Question:</strong>{' '}
                {(question && question.prompt) || chart.result.title}
              </span>
            )
          }
        }
      })
      const elementItems = pdfUtils.createElementItems(
        formatStats,
        settings,
        'taster'
      )

      const pages = savedLayout
        ? savedLayout.pages
        : pdfUtils.calculateElementsLayout(paperSize, elementItems)

      const pagesWithContent = pages.map(page =>
        page.map(gridItem => ({
          ...gridItem,
          ...elementItems.find(e => e.i === gridItem.i)
        }))
      )

      setPages(pagesWithContent)
      showRendering(1000)
    }
  }, [
    loading,
    loadingSurvey,
    settings,
    stats,
    questions,
    paperSize,
    savedLayout,
    showRendering
  ])

  window.__PAGE_HEIGHT = Math.ceil(rowHeight * paperSize.rows)
  return (
    <TasterPdfContainer>
      {rendering && <div id='loading'>loading</div>}
      {pages.map((page, index) => (
        <Page
          key={`page-${index}`}
          paperSize={paperSize}
          colWidth={colWidth}
          rowHeight={rowHeight}
          zoom={zoom}
          layout={page}
        />
      ))}
    </TasterPdfContainer>
  )
}

const QUERY_SURVEY = gql`
  query survey($surveyId: ID!) {
    survey(id: $surveyId) {
      id
      setupQuestions {
        id
        prompt
      }
      productsQuestions {
        id
        prompt
      }
      paymentQuestions {
        id
        prompt
      }
      screeningQuestions {
        id
        prompt
      }
      finishingQuestions {
        id
        prompt
      }
    }
  }
`

export default PreviewTasterPdf
