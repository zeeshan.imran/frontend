import React, { useEffect } from 'react'
import ReactGridLayout from 'react-grid-layout'
import { usePdfLayout } from '../../hooks/usePdfLayout'

const Page = ({ paperSize, colWidth, rowHeight, zoom, layout }) => {
  const { fixLayout } = usePdfLayout()

  useEffect(() => {
    fixLayout(layout, layout, paperSize, zoom)
  }, [layout, fixLayout, paperSize, zoom])

  return (
    <div
      style={{
        minWidth: paperSize.cols * colWidth,
        maxWidth: paperSize.cols * colWidth,
        maxHeight: rowHeight * paperSize.rows,
        minHeight: rowHeight * paperSize.rows
      }}
    >
      <ReactGridLayout
        layout={layout}
        cols={paperSize.cols}
        width={paperSize.cols * colWidth}
        rowHeight={rowHeight}
        margin={[0, 0]}
        containerPadding={[0, 0]}
        verticalCompact={false}
        preventCollision
        isDraggable={false}
        isResizable={false}
      >
        {layout.map(item => (
          <div key={item.i} id={`pdf-${item.i}`}>
            {item.content}
          </div>
        ))}
      </ReactGridLayout>
    </div>
  )
}

export default Page
