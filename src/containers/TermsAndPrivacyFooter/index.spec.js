import React from 'react'
import { mount } from 'enzyme'
import TermsAndPrivacyFooterComponent from '.'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'

describe('TermsAndPrivacyFooterComponent', () => {
  let testRender
  let isSurvey

  beforeEach(() => {
    isSurvey = true
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render TermsAndPrivacyFooterComponent', () => {
    const history = createBrowserHistory()

    testRender = mount(
      <Router history={history}>
        <TermsAndPrivacyFooterComponent history={history} isSurvey={isSurvey} />
      </Router> 
    )
    expect(testRender).toMatchSnapshot()
  })
})
