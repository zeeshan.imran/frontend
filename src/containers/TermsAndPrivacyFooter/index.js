import React from 'react'
import TermsAndPrivacyFooterComponent from '../../components/TermsAndPrivacyFooter'

const TermsAndPrivacyFooter = ({ history, isSurvey }) => {
  return <TermsAndPrivacyFooterComponent isSurvey={isSurvey} />
}

export default TermsAndPrivacyFooter
