import React from 'react'
import { mount } from 'enzyme'
import '../../utils/internationalization/i18n'
import ProductsToCreateList from './index'
import gql from 'graphql-tag'
import { ApolloProvider } from 'react-apollo-hooks'
import wait from '../../utils/testUtils/waait'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import CreateProductCard from '../../components/CreateProductCard'
import sinon from 'sinon'
import AlertModal from '../../components/AlertModal'
import { defaultSurvey } from '../../mocks'

jest.mock('../../components/AlertModal')

const mockSurveyCreation = {
  surveyId: 'survey-1',
  ...defaultSurvey
}

jest.mock('browser-image-compression', () => ({
  __esModule: true,
  default: async (file, option) => file
}))

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
      basics {
        name
        coverPhoto
        instructionsText
        instructionSteps
        thankYouText
        rejectionText
        uniqueName
        authorizationType
        exclusiveTasters
        allowRetakes
        forcedAccount
        forcedAccountLocation
        tastingNotes {
          tastingId
          tastingLeader
          customer
          country
          dateOfTasting
          otherInfo
        }
        isScreenerOnly
        showGeneratePdf
        linkedSurveys
        recaptcha
        minimumProducts
        maximumProducts
        surveyLanguage
        country
        referralAmount
        savedRewards
        screeningText
        maxProductStatCount
        customizeSharingMessage
        loginText
        pauseText
        allowedDaysToFillTheTasting
        isPaypalSelected
        productDisplayType
        isGiftCardSelected
        customButtons {
          continue
          start
          next
          skip
        }
      }
    }
  }
`

const UPDATE_SURVEY_CREATION_PRODUCTS = gql`
  mutation updateSurveyCreationProducts($products: products) {
    updateSurveyCreationProducts(products: $products) @client
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`

const UNREQUIRE_CHOOSE_PRODUCT = gql`
  mutation unrequireChooseProduct {
    unrequireChooseProduct(id: "") @client
  }
`

const REQUIRE_CHOOSE_PRODUCT = gql`
  mutation requireChooseProduct {
    requireChooseProduct(id: "") @client
  }
`

describe('ProductsToCreateList', () => {
  let testRender
  let mockClient
  let mockClient2
  let spyChange

  beforeEach(() => {
    mockClient = createApolloMockClient()

    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          ...mockSurveyCreation,
          products: [
            {
              id: 1,
              name: 'product-1',
              reward: 6
            },
            {
              id: 2,
              name: 'product-2',
              reward: 12
            }
          ]
        }
      }
    })

    const stub = sinon.stub(mockClient, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: UPDATE_SURVEY_CREATION_PRODUCTS }))
      .callsFake(spyChange)
    stub.callThrough()

    stub
      .withArgs(sinon.match({ mutation: UPDATE_SURVEY_CREATION_BASICS }))
      .callsFake(spyChange)
    stub.callThrough()

    stub
      .withArgs(sinon.match({ mutation: REQUIRE_CHOOSE_PRODUCT }))
      .callsFake(spyChange)
    stub.callThrough()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ProductsToCreateList events useRef null', async () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <ProductsToCreateList />
      </ApolloProvider>
    )
    await wait(10)

    expect(testRender.find(CreateProductCard)).toHaveLength(2)
  })

  test('should render ProductsToCreateList events', async () => {
    jest.spyOn(React, 'useRef').mockReturnValue({ current: true })

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <ProductsToCreateList />
      </ApolloProvider>
    )

    await wait(10)

    spyChange.mockReset()

    const onClickFn = testRender
      .find(CreateProductCard)
      .first()
      .prop('handleRemove')

    spyChange.mockReset()
    AlertModal.mockImplementationOnce(({ handleOk }) => handleOk())
    onClickFn()
    expect(spyChange).toHaveBeenCalled()

    AlertModal.mockImplementationOnce(({ handleCancel }) => handleCancel())

    spyChange.mockReset()

    testRender
      .find(CreateProductCard)
      .first()
      .prop('onChange')()
  })

  test('should render ProductsToCreateList events', async () => {
    jest.spyOn(React, 'useRef').mockReturnValue({ current: true })
    mockClient2 = createApolloMockClient()
    mockClient2.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    const stub2 = sinon.stub(mockClient2, 'mutate')
    spyChange = jest.fn()

    stub2.withArgs(sinon.match({ mutation: UNREQUIRE_CHOOSE_PRODUCT }))
      .callsFake(spyChange)
    stub2.callThrough()

    testRender = mount(
      <ApolloProvider client={mockClient2}>
        <ProductsToCreateList />
      </ApolloProvider>
    )
  })
})
