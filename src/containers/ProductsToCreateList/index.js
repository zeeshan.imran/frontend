import React, { useEffect, useRef, useState } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { remove, pluck } from 'ramda'
import CreateProductCard from '../../components/CreateProductCard'
import AlertModal from '../../components/AlertModal'
import { useTranslation } from 'react-i18next'
import defaults from '../../defaults'
import ProductsStatsCount from '../../containers/ProductsStatsCount'
import { PRODUCT_STATS_COUNT_MIN } from '../../utils/Constants'

const ProductsToCreateList = ({ stricted }) => {
  const { t } = useTranslation()
  const [lastAvailable, setLastAvailable] = useState()
  const isInitialized = useRef(false)
  const {
    data: {
      surveyCreation: { products, basics }
    },
    refetch: refetchCurrent
  } = useQuery(SURVEY_CREATION)
  
  const refetch = useRef(refetchCurrent)
  useEffect(() => {
    if(refetch && refetch.current && typeof refetch.current === 'function'){
      refetch.current()
    }
  }, [products])

  const currentCountry = basics ? basics.country : ''

  const updateBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)

  const atLeastOneAvailable = products.filter(product => product.isAvailable)
  if (
    atLeastOneAvailable.length === 1 &&
    lastAvailable !== atLeastOneAvailable[0].id
  ) {
    setLastAvailable(atLeastOneAvailable[0].id)
  }

  const availableCountry = defaults.countries.find(
    country => country.name === currentCountry
  )
  const rewardCurrency = availableCountry || {}
  const prefix = defaults.currencyPrefixes[rewardCurrency.currency] || ''
  const suffix = defaults.currencySuffixes[rewardCurrency.currency] || ''

  const updateProducts = useMutation(UPDATE_SURVEY_CREATION_PRODUCTS)

  const hasManyProducts = products.length > 0 //Since we need to show select product screen so changing the check from > 1 to 0 
  const requireChooseProductStep = useMutation(REQUIRE_CHOOSE_PRODUCT)
  const unrequireChooseProductStep = useMutation(UNREQUIRE_CHOOSE_PRODUCT)
  const { productDisplayType } = basics
  const disabledInputBrandName =
    (productDisplayType === 'reverse' ||
      productDisplayType === 'permutation' ||
      productDisplayType === 'forced') &&
    stricted
  useEffect(
    () => {
      if (!isInitialized.current) {
        isInitialized.current = true
        return
      }
      if (hasManyProducts) {
        requireChooseProductStep()
      } else if (!hasManyProducts) {
        unrequireChooseProductStep()
      }
    },
    // warn: this is a temporary to prevent unwanted useEffect call
    // when requireChooseProductStep, unrequireChooseProductStep are changed
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [hasManyProducts]
  )

  const handleChange = value => {
    const basicsUpdated = { ...basics, ...value }
    updateBasics({
      variables: {
        basics: basicsUpdated
      }
    })
  }

  const removeProduct = index => {
    const updatedProducts = remove(index, 1, products)

    updateProducts({
      variables: {
        products: updatedProducts
      }
    })
  }

  const handleProductCount = value => {
    handleChange({
      maxProductStatCount: Math.trunc(value)
    })
  }

  const removeProductConfirm = index => {
    AlertModal({
      title: t('containers.productsCreateList.alertModalTitle'),
      okText: t('containers.productsCreateList.alertModalOkText'),
      description: t('containers.productsCreateList.alertModalDescription'),
      handleOk: () => removeProduct(index),
      handleCancel: () => {}
    })
  }

  const updateProduct = (field, index) => {
    const targetProduct = products[index]

    updateProducts({
      variables: {
        products: [
          ...products.slice(0, index),
          { ...targetProduct, ...field },
          ...products.slice(index + 1)
        ]
      }
    })
  }

  return (
    <React.Fragment>
      {products.map((product, index) => (
        <CreateProductCard
          key={product.id || product.clientGeneratedId}
          handleRemove={() => removeProductConfirm(index)}
          onChange={field => updateProduct(field, index)}
          isAvailableError={
            !atLeastOneAvailable.length && lastAvailable === product.id
          }
          prefix={prefix}
          suffix={suffix}
          disabledInputBrandName={disabledInputBrandName}
          productNames={products ? pluck('name', products) : []}
          {...product}
        />
      ))}
      {atLeastOneAvailable.length >= PRODUCT_STATS_COUNT_MIN && (
        <ProductsStatsCount
          productCount={handleProductCount}
          maxProductStatCount={
            basics ? basics.maxProductStatCount : PRODUCT_STATS_COUNT_MIN
          }
          totalProductCount={atLeastOneAvailable.length}
        />
      )}
    </React.Fragment>
  )
}

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
      basics {
        name
        coverPhoto
        instructionsText
        instructionSteps
        thankYouText
        rejectionText
        uniqueName
        authorizationType
        exclusiveTasters
        forcedAccount
        forcedAccountLocation
        allowRetakes
        recaptcha
        minimumProducts
        maximumProducts
        surveyLanguage
        country
        referralAmount
        savedRewards
        screeningText
        customizeSharingMessage
        loginText
        pauseText
        maxProductStatCount
        allowedDaysToFillTheTasting
        isPaypalSelected
        isGiftCardSelected
        isScreenerOnly
        productDisplayType
        showGeneratePdf
        linkedSurveys
        customButtons {
          continue
          start
          next
          skip
        }
      }
    }
  }
`

const UPDATE_SURVEY_CREATION_PRODUCTS = gql`
  mutation updateSurveyCreationProducts($products: products) {
    updateSurveyCreationProducts(products: $products) @client
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`

const REQUIRE_CHOOSE_PRODUCT = gql`
  mutation requireChooseProduct {
    requireChooseProduct(id: "") @client
  }
`

const UNREQUIRE_CHOOSE_PRODUCT = gql`
  mutation unrequireChooseProduct {
    unrequireChooseProduct(id: "") @client
  }
`

export default ProductsToCreateList
