import styled from 'styled-components'

export const BaseContainer = styled.div`
  display: flex;
  width: 100%;
  margin: 0;
  padding: 0;
  /* background: #808080; */
  background: #ffffff;
  @media print {
  @page {
    margin-bottom: 0; 
  }
  }
`
export const Container = styled.div`
  background: #ffffff;
  padding: 2em;
`
export const ChartContainer = styled.div`
  margin: 0 auto;
  background: #ffffff;
  @media print {
    .chart-print {
      page-break-inside: avoid !important;
      margin:auto;
    }
  }
`
export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6.4rem;
`
export const Logo = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 18.5rem;
  height: 3.4rem;
  cursor: ${({ onClick }) => (onClick ? 'pointer' : 'auto')};
  margin-top: 20px;
`
export const BreakContainer = styled.div`
  @media print {
    page-break-after: auto !important;
    float: none;
    margin: auto;
  }
`