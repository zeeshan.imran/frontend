import React from 'react'
import { withRouter } from 'react-router-dom'
import UserSurveyCardComponent from '../../components/UserSurveyCard'

const UserSurveyCard = ({ data: { survey }, history, ...otherProps }) => (
  <UserSurveyCardComponent
    {...otherProps}
    onClick={() => history.push(`/survey/${survey.id}`)}
    inProgress={survey.inProgress}
    numberOfProducts={survey.numberOfProducts || 1}
    expirationDate={survey.expirationTime}
  />
)

export default withRouter(UserSurveyCard)
