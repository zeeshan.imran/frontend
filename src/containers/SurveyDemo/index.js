import React from 'react'
import gql from 'graphql-tag'
import { Redirect } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import FillerLoader from '../../components/FillerLoader'

export const DEMO_SURVEY = gql`
  query getDemoSurvey($demoName: String) {
    getDemoSurvey(demoName: $demoName) {
      id
    }
  }
`

const SurveyDemo = ({ match, history }) => {
  const {
    params: { demoName }
  } = match
  const { data, loading, error } = useQuery(DEMO_SURVEY, {
    variables: { demoName },
    fetchPolicy: 'network-only'
  })
  if (loading) return <FillerLoader loading />
  if (error) return <Redirect to='/404' />
  const {
    getDemoSurvey: { id }
  } = data

  return <Redirect to={`/survey/${id}`} />
}

export default SurveyDemo
