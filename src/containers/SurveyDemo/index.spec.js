import React from 'react'
import { mount } from 'enzyme'
import SurveyDemo, { DEMO_SURVEY } from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import history from '../../history'
import { Redirect, Router } from 'react-router-dom'
import wait from '../../utils/testUtils/waait'
jest.mock('react-router-dom')

describe('SurveyDemo', () => {
  let testRender
  let client
  let mockedQuery

  beforeEach(() => {
    mockedQuery = {
      request: {
        query: DEMO_SURVEY,
        variables: {
          demoName: 'demo_name'
        }
      },
      result: {
        data: {
          getDemoSurvey: { id: 'some_id' }
        }
      }
    }
    client = createApolloMockClient({ mocks: [mockedQuery] })
  })

  test('should render loader', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SurveyDemo match={{ params: { demoName: 'demo_name' } }} />
      </ApolloProvider>
    )

    await wait(0)

    expect(Redirect).toHaveBeenCalledWith(
      { push: false, to: '/survey/some_id' },
      { router: undefined }
    )

    testRender.unmount()
  })

  test('should render loader', async () => {
    mockedQuery = {
      request: {
        query: DEMO_SURVEY,
        variables: {
          demoName: 'demo_name'
        }
      },
      result: {
        data: {}
      }
    }
    client = createApolloMockClient({ mocks: [mockedQuery] })

    testRender = mount(
      <ApolloProvider client={client}>
        <SurveyDemo match={{ params: {} }} />
      </ApolloProvider>
    )

    await wait(0)
    expect(Redirect).toHaveBeenCalledWith(
      { push: false, to: '/404' },
      { router: undefined }
    )
    testRender.unmount()
  })
})
