import React, { Component } from 'react'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router-dom'
import Loader from '../../components/Loader'
import CategoriesGridComponent from '../../components/CategoriesGrid'
import { CATEGORIES_QUERY } from '../../queries/SubCategoriesGrid'

class SubCategoriesGrid extends Component {
  // removed because the functionality was moved in frontend/src/containers/CategoryCard/index.js
  render () {
    const {
      match: {
        params: { categoryName }
      } 
    } = this.props

    return (
      <Query
        query={CATEGORIES_QUERY}
        fetchPolicy='cache-and-network'
        variables={{ key: categoryName }}
      >
        {({ data: { productCategories }, loading }) => {
          if (loading) return <Loader />
          return (
            <CategoriesGridComponent
              categories={productCategories[0].children}
            />
          )
        }}
      </Query>
    )
  }
}

export default withRouter(SubCategoriesGrid)
