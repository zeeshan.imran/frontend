import React from 'react'
import { mount } from 'enzyme'
import SubCategoriesGrid from '.'
import wait from '../../utils/testUtils/waait'
import { CATEGORIES_QUERY } from '../../queries/SubCategoriesGrid'
import { Router } from 'react-router-dom'
import { MockedProvider } from 'react-apollo/test-utils'
import { createBrowserHistory } from 'history'

const mocks = [
  {
    request: {
      query: CATEGORIES_QUERY,
      variables: {
        key: 'cate-1'
      }
    },
    result: {
      data: {
        productCategories: [
          {
            children: [
              {
                value: 'cate-1',
                key: 1
              }
            ]
          }
        ]
      }
    }
  }
]

describe('SubCategoriesGrid', () => {
  let testRender
  let historyMock

  beforeEach(() => {
    historyMock = createBrowserHistory()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SubCategoriesGrid', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Router history={historyMock}>
          <SubCategoriesGrid.WrappedComponent
            match={{ params: { categoryName: 'cate-1' } }}
          />
        </Router>
      </MockedProvider>
    )

    await wait(0)

    expect(testRender).toMatchSnapshot()
  })
})
