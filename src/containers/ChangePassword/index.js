import React, { Component } from 'react'
import Composer from 'react-composer'
import { Desktop } from '../../components/Responsive'
import { Query } from 'react-apollo'
import ChangePasswordForm from '../../components/ChangePasswordForm'
import ETHNICITIES_QUERY from '../../queries/ethnicities'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { withTranslation } from 'react-i18next'

class ChangePassword extends Component {
  state = {
    submitError: ''
  }

  handleSubmit = (e, form) => {
    const {
      user: { id },
      updateUser,
      t
    } = this.props
    e.preventDefault()
    form.validateFields(async (err, values) => {
      if (!err) {
        const updateUserData = {
          id,
          oldPassword: values.currentPassword,
          password: values.newPassword
        }
        try {
          const {
            data: {
              updateUser: { ok, error: updateUserError }
            }
          } = await updateUser({ variables: updateUserData })
          if (!ok) {
            const errorMessage =
              updateUserError === 'InvalidPassword'
                ? t('containers.changePassword.userPasswordError')
                : updateUserError
            this.setState({ submitError: errorMessage })
            return displayErrorPopup(
              t('error.update', { error: updateUserError })
            )
          }
        } catch (error) {
          return displayErrorPopup(t('containers.changePassword.error'))
        }
        this.setState({ submitError: '' })
        return displaySuccessMessage(t('containers.changePassword.success'))
      }
    })
  }

  render () {
    const { submitError } = this.state
    return (
      <Composer
        components={[
          ({ render }) => <Desktop children={render} />,
          ({ render }) => (
            <Query
              query={ETHNICITIES_QUERY}
              fetchPolicy='cache-and-network'
              children={render}
            />
          )
        ]}
      >
        {([
          desktop,
          {
            data: { ethnicities },
            loading: loadingEthnicities
          }
        ]) => {
          return (
            <ChangePasswordForm
              desktop={desktop}
              submitForm={this.handleSubmit}
              error={submitError}
            />
          )
        }}
      </Composer>
    )
  }
}

export default withTranslation()(ChangePassword)
