import React from 'react'
import { mount, shallow } from 'enzyme'
import PointsSurveysGrid from '.'
import SurveysGridComponent from '../../components/SurveysGrid'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { MockedProvider } from 'react-apollo/test-utils'
import { POINTS_SURVEY_QUERY } from '../../queries/PointsSurveysGrid'

import wait from '../../utils/testUtils/waait'

const mocks = [
  {
    request: {
      query: POINTS_SURVEY_QUERY
    },
    result: { data: { surveys: [] } }
  }
]

describe('PointsSurveysGrid', () => {
  let testRender

  let user

  beforeEach(() => {
    user = getAuthenticatedUser()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render loader', () => {
    testRender = shallow(
      <MockedProvider>
        <PointsSurveysGrid desktop={''} user={user} />
      </MockedProvider>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('should render SurveysGridComponent', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks}>
        <PointsSurveysGrid desktop={''} user={user} />
      </MockedProvider>
    )
    await wait(0)
    testRender.update()
    expect(testRender.find(SurveysGridComponent).length).toBe(1)
  })
})
