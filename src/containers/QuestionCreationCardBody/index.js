import React, { useCallback, memo } from 'react'
import QuestionCreationCardBodyComponent from '../../components/QuestionCreationCardBody'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'

const QuestionCreationCardBody = ({ questionIndex, question }) => {
  const updateQuestionMutation = useMutation(UPDATE_SURVEY_CREATION_QUESTION)

  // NOTED: should refactor (by combine mandatoryQuestions and questions)
  const legacyMandatoryQuestion = question.type === 'choose-product' || 
    question.type === 'paypal-email' || question.type === 'choose-payment'
    
  const updateQuestion = useCallback(
    field => {
      updateQuestionMutation({
        variables: {
          questionField: field,
          id: question.id,
          questionIndex,
          mandatoryQuestion: legacyMandatoryQuestion,
          question
        }
      })
    },
    [questionIndex, updateQuestionMutation, legacyMandatoryQuestion]
  )

  return (
    <QuestionCreationCardBodyComponent
      handleFieldChange={updateQuestion}
      {...question}
      questionIndex={questionIndex}
    />
  )
}

const UPDATE_SURVEY_CREATION_QUESTION = gql`
  mutation updateSurveyCreationQuestion(
    $questionField: Question
    $questionIndex: Int
    $mandatoryQuestion: Boolean
    $question: Question
  ) {
    updateSurveyCreationQuestion(
      questionField: $questionField
      questionIndex: $questionIndex
      mandatoryQuestion: $mandatoryQuestion
      question: $question
    ) @client
  }
`

export default memo(QuestionCreationCardBody)
