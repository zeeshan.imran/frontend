import React from 'react'
import { mount } from 'enzyme'
import PersonalInformation from '.'
import SignUpFormContainer from '../index'

jest.mock(
  '../../../pages/CreateAccount/PersonalInformation',
  () => ({
    getChangeHandler,
    getBirthdayChangeHandler,
    getOptionChangeHandler
  }) => {
    getChangeHandler('firstname')({
      target: { value: 'First Name is Required' }
    })
    getChangeHandler('lastname')({
      target: { value: 'Last Name is Required' }
    })

    const now = new Date('2017-10-02')
    getBirthdayChangeHandler('birthday')(now)
    getOptionChangeHandler('firstname')('First Name is Required')
    getOptionChangeHandler('lastname')('Last Name is Required')

    return <div />
  }
)
jest.mock('../index')

describe('PersonalInformation', () => {
  let testRender
  let mockFieldChangeHandler

  beforeAll(() => {
    mockFieldChangeHandler = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PersonalInformation', async () => {
    SignUpFormContainer.mockImplementation(({ children }) => (
      <div>
        {children({
          fieldChangeHandler: mockFieldChangeHandler,
          formData: {
            firstname: 'firstname',
            lastname: 'lastname',
            birthday: 'birthday',
            secondStepValid: true
          },
          fieldsData: {
            genders: [],
            languages: []
          }
        })}
      </div>
    ))

    testRender = mount(<PersonalInformation />)
    expect(testRender).toMatchSnapshot()
  })
})
