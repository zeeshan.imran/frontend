import React from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import PersonalInformationComponent from '../../../pages/CreateAccount/PersonalInformation'
import SignUpFormContainer from '..'
import { useTranslation } from 'react-i18next';

const PersonalInformation = () => (
  <SignUpFormContainer>
    {({
      fieldChangeHandler,
      formData: {
        firstname,
        lastname,
        birthday,
        gender,
        language,
        secondStepValid,
        file,
        recaptchaResponseToken
      },
      fieldsData: { genders, languages }
    }) => {
      const { t } = useTranslation();
      return (
        <Formik
          validationSchema={Yup.object().shape({
            firstname: Yup.string().required(t('containers.createAccount.personalInfo.firstnameRequire')),
            lastname: Yup.string().required(t('containers.createAccount.personalInfo.lastnameRequire')),
            birthday: Yup.string().required(t('containers.createAccount.personalInfo.birthdayRequire')),
            recaptchaResponseToken: Yup.string()
              .required(t('containers.createAccount.personalInfo.recaptchaRequire'))
              .typeError(t('containers.createAccount.personalInfo.recaptchaError'))
          })}
          initialValues={{
            firstname,
            lastname,
            birthday,
            gender,
            language
          }}
          render={({
            values,
            handleChange,
            setFieldValue,
            validateForm,
            ...rest
          }) => {
            if (rest.isValid !== secondStepValid) {
              fieldChangeHandler('secondStepValid', rest.isValid)
            }
            const getChangeHandler = field => event => {
              fieldChangeHandler(field, event.target.value)
              return handleChange(event)
            }

            const getBirthdayChangeHandler = field => (_, dateString) => {
              fieldChangeHandler(field, dateString)
              return setFieldValue(field, dateString) // This is needed instead of handleChange
              // so the form is updated correctly
            }

            const getOptionChangeHandler = field => value => {
              fieldChangeHandler(field, value)
              return setFieldValue(field, value)
              // this is needed instead of handleChange so the form is updated correctly
            }

            return (
              <PersonalInformationComponent
                {...rest}
                {...values}
                getChangeHandler={getChangeHandler}
                getBirthdayChangeHandler={getBirthdayChangeHandler}
                getOptionChangeHandler={getOptionChangeHandler}
                genders={genders}
                languages={languages}
              />
            )
          }}
        />
      )
    }}
  </SignUpFormContainer>
)

export default PersonalInformation
