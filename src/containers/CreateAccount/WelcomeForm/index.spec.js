import React from 'react'
import { mount } from 'enzyme'
import WelcomeForm from '.'
import WelcomeFormComponent from '../../../pages/CreateAccount/WelcomeForm'
import SignUpFormContainer from '../index'

jest.mock('../../../pages/CreateAccount/WelcomeForm')
jest.mock('../index')
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('WelcomeForm', () => {
  let testRender
  let mockFieldChangeHandler

  beforeAll(() => {
    mockFieldChangeHandler = jest.fn()
    SignUpFormContainer.mockImplementation(({ children }) => (
      <div>
        {children({
          fieldChangeHandler: mockFieldChangeHandler,
          formData: {
            email: 'email',
            password: 'password',
            firstStepValid: true
          }
        })}
      </div>
    ))
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render WelcomeForm', async () => {
    WelcomeFormComponent.mockImplementation(({ getChangeHandler }) => {
      getChangeHandler('email')({ target: { value: 'new email' } })
      getChangeHandler('password')({ target: { value: 'new password' } })
      getChangeHandler('confirm')({ target: { value: 'new confirm password' } })
      return <div />
    })
    testRender = mount(<WelcomeForm />)
    expect(testRender).toMatchSnapshot()
  })
})
