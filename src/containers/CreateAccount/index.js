import React from 'react'
import { Query, Mutation } from 'react-apollo'
import Composer from 'react-composer'
import { UPDATE_SIGN_UP_FORM_MUTATION } from '../../mutations/signUpForm'
import { UPDATE_CHILDREN_AGES_MUTATION } from '../../mutations/updateChildrenAges'
import SIGN_UP_FORM_QUERY from '../../queries/signUpForm'
import GENDERS_QUERY from '../../queries/genders'
import LANGUAGES_QUERY from '../../queries/languages'
import ETHNICITIES_QUERY from '../../queries/ethnicities'
import SMOKER_KINDS_QUERY from '../../queries/smokerKinds'
import PRODUCT_CATEGORIES_QUERY from '../../queries/productCategories'
import DIETARY_PREFERENCES_QUERY from '../../queries/dietaryPreferences'
import { UPLOAD_PICTURE_MUTATION } from '../../mutations/uploadPicture'

const SignUpFormContainer = ({ children }) => (
  <Composer
    components={[
      ({ render }) => <Query query={SIGN_UP_FORM_QUERY} children={render} />,
      ({ render }) => (
        <Mutation mutation={UPDATE_SIGN_UP_FORM_MUTATION} children={render} />
      ),
      ({ render }) => <Query query={GENDERS_QUERY} children={render} />,
      ({ render }) => <Query query={LANGUAGES_QUERY} children={render} />,
      ({ render }) => <Query query={ETHNICITIES_QUERY} children={render} />,
      ({ render }) => (
        <Mutation mutation={UPDATE_CHILDREN_AGES_MUTATION} children={render} />
      ),
      ({ render }) => <Query query={SMOKER_KINDS_QUERY} children={render} />,
      ({ render }) => (
        <Query query={PRODUCT_CATEGORIES_QUERY} children={render} />
      ),
      ({ render }) => (
        <Query query={DIETARY_PREFERENCES_QUERY} children={render} />
      ),
      ({ render }) => (
        <Mutation mutation={UPLOAD_PICTURE_MUTATION} children={render} />
      )
    ]}
  >
    {([
      {
        data: { signUpForm }
      },
      updateSignUpForm,
      {
        data: { genders },
        loading: loadingGenders
      },
      {
        data: { languages },
        loading: loadingLanguages
      },
      {
        data: { ethnicities },
        loading: loadingEthnicities
      },
      updateChildrenAges,
      {
        data: { smokerKinds },
        loading: loadingSmokerKinds
      },
      {
        data: { productCategories },
        loading: loadingProductCategories
      },
      {
        data: { dietaryPreferences },
        loading: loadingDietaryPreferences
      },
      uploadPicture
    ]) =>
      children({
        formData: signUpForm,
        fieldsDataLoading: {
          loadingGenders,
          loadingLanguages,
          loadingEthnicities,
          loadingSmokerKinds,
          loadingProductCategories,
          loadingDietaryPreferences
        },
        fieldsData: {
          genders,
          languages,
          ethnicities,
          smokerKinds,
          productCategories,
          dietaryPreferences
        },
        uploadPicture,
        fieldChangeHandler: (field, value) =>
          updateSignUpForm({
            variables: {
              field,
              value
            }
          }),
        handleUpdateChildrenAges: (index, value) =>
          updateChildrenAges({
            variables: {
              index,
              value
            }
          })
      })
    }
  </Composer>
)

export default SignUpFormContainer
