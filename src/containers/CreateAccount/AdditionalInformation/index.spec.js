import React from 'react'
import { mount } from 'enzyme'
import AdditionalInformation from '.'
import SignUpFormContainer from '../index'
import { MockedProvider } from 'react-apollo/test-utils'
import AdditionalInformationComponent from '../../../pages/CreateAccount/AdditionalInformation'

jest.mock('../index')

describe('AdditionalInformation', () => {
  let testRender
  let mockFieldChangeHandler

  beforeAll(() => {
    mockFieldChangeHandler = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AdditionalInformation', async () => {
    SignUpFormContainer.mockImplementation(({ children }) => (
      <div>
        {children({
          fieldChangeHandler: mockFieldChangeHandler,
          formData: {
            ethnicity: 'ethnicity',
            currency: '$',
            incomeRange: 10000,
            hasChildren: 'yes',
            numberOfChildren: 2,
            childrenAges: [1, 2],
            smokerKind: 'smokerKind',
            productTypes: ['info', 'update'],
            dietaryRestrictions: [],
            lastStepValid: false,
            userId: 1
          },
          fieldsData: {
            ethnicities: [],
            smokerKinds: [],
            productCategories: [],
            dietaryPreferences: []
          },
          handleUpdateChildrenAges: jest.fn(),
          uploadPicture: jest.fn()
        })}
      </div>
    ))
    testRender = mount(
      <MockedProvider>
        <AdditionalInformation />
      </MockedProvider>
    )

    testRender
      .find(AdditionalInformationComponent)
      .props()
      .getChildrenAgesChangeHandler(0)(3)

    testRender
      .find(AdditionalInformationComponent)
      .props()
      .getOptionChangeHandler('hasChildren')('yes')

    testRender
      .find(AdditionalInformationComponent)
      .props()
      .getChangeHandler('currency')({
        target: { value: '$' }
      })

    testRender
      .find(AdditionalInformationComponent)
      .props()
      .getChangeHandler('incomeRange')({
        target: { value: 10000 }
      })

    testRender
      .find(AdditionalInformationComponent)
      .props()
      .getChangeHandler('numberOfChildren')({
        target: { value: 2 }
      })

    testRender
      .find(AdditionalInformationComponent)
      .props()
      .getChangeHandler('ethnicity')({
        target: { value: 1 }
      })

    testRender
      .find(AdditionalInformationComponent)
      .props()
      .getChangeHandler('smokerKind')({
        target: { value: 'smokerKind' }
      })

    testRender
      .find(AdditionalInformationComponent)
      .props()
      .getChangeHandler('dietaryRestrictions')({
        target: { value: ['1'] }
      })
  })
})
