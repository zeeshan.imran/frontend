import React from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import AdditionalInformationComponent from '../../../pages/CreateAccount/AdditionalInformation'
import SignUpFormContainer from '..'
import { useTranslation } from 'react-i18next';

const AdditionalInformation = () => (
  <SignUpFormContainer>
    {({
      fieldChangeHandler,
      formData: {
        ethnicity,
        currency,
        incomeRange,
        hasChildren,
        numberOfChildren,
        childrenAges,
        smokerKind,
        productTypes,
        dietaryRestrictions,
        lastStepValid,
        userId
      },
      fieldsData: {
        ethnicities,
        smokerKinds,
        productCategories,
        dietaryPreferences
      },
      handleUpdateChildrenAges,
      uploadPicture
    }) => {
      const {t} = useTranslation();
      return (
        <Formik
          isInitialValid={lastStepValid}
          validationSchema={Yup.object().shape({
            hasChildren: Yup.string(),
            numberOfChildren: Yup.number().when('hasChildren', {
              is: 'Yes',
              then: Yup.number()
                .required(t('containers.createAccount.additionalInfo.numberOfChildRequire'))
                .moreThan(0, t('containers.createAccount.additionalInfo.numberOfChildMorethan'))
                .lessThan(30, t('containers.createAccount.additionalInfo.numberOfChildLessthan'))
                .typeError(t('containers.createAccount.additionalInfo.numberOfChildError')),
              otherwise: Yup.number().notRequired()
            }),
            childrenAges: Yup.array().when('numberOfChildren', {
              is: numberOfChildren => numberOfChildren > 0,
              then: Yup.array()
                .required(t('containers.createAccount.additionalInfo.childrenAgesRequire'))
                .min(Yup.ref('numberOfChildren'), t('containers.createAccount.additionalInfo.childrenAgesMin')),
              otherwise: Yup.array().notRequired()
            }),
            currency: Yup.string(),
            incomeRange: Yup.string().when('currency', {
              is: value => !!value,
              then: Yup.string().required(t('containers.createAccount.additionalInfo.incomeRangeRequire')),
              otherwise: Yup.string().notRequired()
            })
          })}
          initialValues={{
            ethnicity,
            currency,
            incomeRange,
            hasChildren,
            numberOfChildren,
            childrenAges,
            smokerKind,
            productTypes,
            dietaryRestrictions
          }}
          render={({ values, handleChange, setFieldValue, ...rest }) => {
            if (lastStepValid !== rest.isValid) {
              fieldChangeHandler('lastStepValid', rest.isValid)
            }

            const getChangeHandler = field => event => {
              fieldChangeHandler(field, event.target.value)
              return handleChange(event)
            }

            const getOptionChangeHandler = field => value => {
              fieldChangeHandler(field, value)
              return setFieldValue(field, value)
              // this is needed instead of handleChange so the form is updated correctly
            }

            const getChildrenAgesChangeHandler = index => value => {
              handleUpdateChildrenAges(index, value)
              return setFieldValue('childrenAges', childrenAges)
            }

            return (
              <AdditionalInformationComponent
                {...rest}
                {...values}
                getChangeHandler={getChangeHandler}
                getOptionChangeHandler={getOptionChangeHandler}
                getChildrenAgesChangeHandler={getChildrenAgesChangeHandler}
                ethnicities={ethnicities}
                childrenAges={childrenAges}
                smokerKinds={smokerKinds}
                productCategories={productCategories}
                dietaryPreferences={dietaryPreferences}
                userId={userId}
                uploadPicture={uploadPicture}
              />
            )
          }}
        />
      )
    }}
  </SignUpFormContainer>
)

export default AdditionalInformation
