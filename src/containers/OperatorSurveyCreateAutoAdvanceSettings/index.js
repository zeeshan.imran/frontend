import React, { useCallback, useEffect, useRef } from 'react'
import { path } from 'ramda'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import AutoAdvanceSettings from '../../components/OperatorSurveyCreateAutoAdvanceSettings'

const getAutoAdvanceSettings = path([
  'surveyCreation',
  'basics',
  'autoAdvanceSettings'
])
const getPdfFooterSettings = path([
  'surveyCreation',
  'basics',
  'pdfFooterSettings'
])
const getQuestions = path(['surveyCreation', 'questions'])
const getSharingButtons = path(['surveyCreation', 'basics', 'sharingButtons'])
const isScreenerOnly = path(['surveyCreation', 'basics', 'isScreenerOnly'])
const getValidatedData = path(['surveyCreation', 'basics', 'validatedData'])
const getGeneratePdf = path(['surveyCreation', 'basics', 'showGeneratePdf'])
const getAuthorizationType = path([
  'surveyCreation',
  'basics',
  'authorizationType'
])
const getShowOnTasterDashboard = path(['surveyCreation', 'basics', 'showOnTasterDashboard'])

const OperatorSurveyCreateSettings = ({ editing = false }) => {
  const { data,
    refetch: refetchCurrent
  } = useQuery(SURVEY_CREATION)
  const updateSurveyBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)
  const updateQuestions = useMutation(UPDATE_SURVEY_CREATION_QUESTIONS)
  const updateProducts = useMutation(UPDATE_SURVEY_CREATION_PRODUCTS)
  const unrequireChooseProductStep = useMutation(UNREQUIRE_CHOOSE_PRODUCT)
  const refetch = useRef(refetchCurrent)

  useEffect(() => {
    refetch.current()
  }, [data.surveyCreation.basics.authorizationType])

  const autoAdvanceSettings = getAutoAdvanceSettings(data) || {
    active: false,
    debounce: 0,
    hideNextButton: false
  }

  const pdfFooterSettings = getPdfFooterSettings(data) || {
    active: false,
    footerNote: ''
  }

  const screenerOnly = isScreenerOnly(data) || false
  const questions = getQuestions(data) || []
  const sharingButtons = getSharingButtons(data) || false
  const validatedData = getValidatedData(data) || false
  const showGeneratePdf = getGeneratePdf(data) || false
  const authorizationType = getAuthorizationType(data)
  const showOnTasterDashboard = getShowOnTasterDashboard(data) || false

  const handleSharingButtonsChange = useCallback(
    async sharingButtons => {
      await updateSurveyBasics({
        variables: {
          basics: {
            sharingButtons
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleValidatedData = useCallback(
    async validatedData => {
      await updateSurveyBasics({
        variables: {
          basics: {
            validatedData
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleShowOnTasterDashboard = useCallback(
    async showOnTasterDashboard => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showOnTasterDashboard
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handlePdfSetting = useCallback(
    async pdfFooterSettings => {
      await updateSurveyBasics({
        variables: {
          basics: {
            pdfFooterSettings
          }
        }
      })
    },
    [updateSurveyBasics]
  )
  const handleGeneratePdfButton = useCallback(
    async showGeneratePdf => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showGeneratePdf
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleSettingsChange = useCallback(
    async (autoAdvanceSettings, screenerOnly) => {
      let data = {
        autoAdvanceSettings: autoAdvanceSettings,
        isScreenerOnly: screenerOnly
      }
      if (screenerOnly) {
        data = {
          autoAdvanceSettings: autoAdvanceSettings,
          isScreenerOnly: screenerOnly,
          forcedAccount: true,
          showGeneratePdf: false
        }
      }
      await updateSurveyBasics({
        variables: {
          basics: data
        }
      })
      if (screenerOnly) {
        if (questions.length) {
          const newQuestions = questions.filter(singleQuestion => {
            return (
              singleQuestion.displayOn === 'screening' ||
              singleQuestion.displayOn === 'payments'
            )
          })
          await updateQuestions({
            variables: {
              questions: newQuestions
            }
          })
          unrequireChooseProductStep()
        }
        await updateProducts({
          variables: {
            products: []
          }
        })
      }
    },
    [questions, updateProducts, updateQuestions, updateSurveyBasics]
  )

  return (
    <AutoAdvanceSettings
      autoAdvanceSettings={autoAdvanceSettings}
      pdfFooterSettings={pdfFooterSettings}
      onSettingsChange={handleSettingsChange}
      screenerOnly={screenerOnly}
      sharingButtons={sharingButtons}
      showGeneratePdf={showGeneratePdf}
      handleGeneratePdfButton={handleGeneratePdfButton}
      onSharingButtonsChange={handleSharingButtonsChange}
      editing={editing}
      handleValidatedDataChange={handleValidatedData}
      validatedData={validatedData}
      handlePdfSetting={handlePdfSetting}
      surveyAuthorizationType={authorizationType}
      handleShowOnTasterDashboard={handleShowOnTasterDashboard}
      showOnTasterDashboard={showOnTasterDashboard}
    />
  )
}

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      questions
      basics {
        isScreenerOnly
        sharingButtons
        validatedData
        showGeneratePdf
        authorizationType
        showOnTasterDashboard
        autoAdvanceSettings {
          active
          debounce
          hideNextButton
        }
        pdfFooterSettings {
          active
          footerNote
        }
      }
    }
  }
`
export const UPDATE_SURVEY_CREATION_QUESTIONS = gql`
  mutation updateSurveyCreationQuestions($questions: questions) {
    updateSurveyCreationQuestions(questions: $questions) @client
  }
`

const UPDATE_SURVEY_CREATION_PRODUCTS = gql`
  mutation updateSurveyCreationProducts($products: products) {
    updateSurveyCreationProducts(products: $products) @client
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`
const UNREQUIRE_CHOOSE_PRODUCT = gql`
  mutation unrequireChooseProduct {
    unrequireChooseProduct(id: "") @client
  }
`

export default OperatorSurveyCreateSettings
