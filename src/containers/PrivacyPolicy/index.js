import React from 'react'
import PrivacyPolicy from '../../components/PrivacyPolicy'
import OperatorPageContent from '../../components/OperatorPageContent'
import OperatorPage from '../../components/OperatorPage'

const PrivacyPolicyContainer = ({ history, location, handleChange }) => {
  const handleGoToTermsOfUser = () => {
    window.scroll(0,0)
    history.push('/terms-of-use')
  }

  const path = location && location.pathname && location.pathname.split('/')
  let isOperator = false
  if (Array.isArray(path)) {
    isOperator = path && path.includes('operator')
  }

  return (
    <OperatorPage>
      <OperatorPageContent>
        <PrivacyPolicy
          goToTermsOfUse={handleChange || handleGoToTermsOfUser}
          isOperator={isOperator}
        />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default PrivacyPolicyContainer
