import React from 'react'
import { mount } from 'enzyme'
import PreviewSurveyDraft from '.'
import { createBrowserHistory } from 'history'
import { Router } from 'react-router-dom'

describe('PreviewSurveyDraft', () => {
  let testRender

  test('should render PreviewSurveyDraft', () => {
    const history = createBrowserHistory()

    testRender = mount(
      <Router history={history}>
        <PreviewSurveyDraft match={{ params: { surveyId: 'demo_name' } }} />
      </Router>
    )

    expect(testRender).toMatchSnapshot()

    testRender.unmount()
  })
})
