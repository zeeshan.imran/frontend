import React from 'react'
import { withRouter } from 'react-router-dom'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import TooltipWrapper from '../../components/TooltipWrapper'

import { ButtonWithMargin } from './styles'

const PreviewSurveyDraft = ({ match }) => {
  const {
    params: { surveyId }
  } = match
  const { t } = useTranslation()

  return (
    <TooltipWrapper
      helperText={t('tooltips.previewDraft')}
      placement='leftTop'
    >
      <Link to={`/survey/${surveyId}`} target='_blank'>
        <ButtonWithMargin type='secondary' size='default'>
          {t('containers.previewSurveyDraft.buttonText')}
        </ButtonWithMargin>
      </Link>
    </TooltipWrapper>
  )
}

export default withRouter(PreviewSurveyDraft)
