import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import useToggle from '../../hooks/useToggle'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import usePublishSurvey from '../../hooks/usePublishSurvey'
import TooltipWrapper from '../../components/TooltipWrapper'

import { ButtonWithMargin } from './styles'
// import { validateSurveyBasics, validateProductSchema } from '../../validates'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'

const SaveDraftSurvey = ({ history }) => {
  const { t } = useTranslation()
  const { data: { surveyCreation: { products, basics } = {} } = {} } = useQuery(
    SURVEY_CREATION
  )
  const [publishing, togglePublishing] = useToggle(false)

  const saveDraftSurvey = usePublishSurvey({
    surveyInitialState: t('containers.saveDraftSurvey.surveyInitialState'),
    successMessage: t('containers.saveDraftSurvey.successMessage'),
    errorMessage: t('containers.saveDraftSurvey.errorMessage')
  })
  const surveyValidate = useSurveyValidate()
  const submitDisabled = !basics.isScreenerOnly
    ? !basics.uniqueName ||
      products.length === 0 ||
      // products.reduce(
      //   (result, product) =>
      //     result || !validateProductSchema.isValidSync(product),
      //   false
      // ) ||
      // !validateSurveyBasics(true).isValidSync(basics) ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
    : !basics.uniqueName || basics.linkedSurveys.length === 0

  const onSaveDraft = async () => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails`)
      return
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion
    } = await surveyValidate.validateQuestions()

    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(`./financial?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    if (!isQuestionsValid) {
      history.push(`./questions?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    togglePublishing()
    try {
      const surveyId = await saveDraftSurvey()
      togglePublishing()
      history.push(`/operator/draft/edit/${surveyId}`)
    } catch (error) {
      togglePublishing()
      displayErrorPopup(error && error.message)
    }
  }

  return (
    <TooltipWrapper helperText={t('tooltips.saveDraftSurvey')} placement='leftTop'>
      <ButtonWithMargin
        data-testid='save-draft-survey-button'
        type={'secondary'}
        disabled={submitDisabled}
        size='default'
        onClick={onSaveDraft}
      >
        {t('containers.saveDraftSurvey.publishButton')}
      </ButtonWithMargin>
      <LoadingModal
        visible={publishing}
        text={t('containers.saveDraftSurvey.loadingText')}
      />
    </TooltipWrapper>
  )
}

export default withRouter(SaveDraftSurvey)
