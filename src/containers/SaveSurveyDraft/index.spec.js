import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import wait from '../../utils/testUtils/waait'
import Button from '../../components/Button'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import usePublishSurvey from '../../hooks/usePublishSurvey'
import SaveSurveyDraft from './index'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { defaultSurvey } from '../../mocks'

const createSurveyCreationMock = secondProduct => ({
  ...defaultSurvey,
  products: [
    {
      name: 'the first product',
      brand: 'the brand'
    },
    secondProduct
  ]
})

jest.mock('../../hooks/usePublishSurvey')
jest.mock('../../hooks/useSurveyValidate')
jest.mock('../../utils/displayErrorPopup')

describe('save survey draft', () => {
  let render
  let publishSurvey
  let history
  let client
  let showProductsErrors
  let showQuestionsErrors
  const match = { params: { surveyId: 'survey-1' } }

  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: '',
          brand: 'the brand'
        })
      }
    })
    publishSurvey = jest.fn(() => 'survey-1')
    showProductsErrors = jest.fn()
    showQuestionsErrors = jest.fn()
    usePublishSurvey.mockImplementation(() => publishSurvey)

    history = {
      push: jest.fn()
    }
  })

  afterEach(() => {
    render.unmount()
  })

  test('should switch to products tab and show errors when the survey has at least one invalid product', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateProducts: () => false,
      validateQuestions: () => ({ isValid: true }),
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <SaveSurveyDraft.WrappedComponent history={history} />
      </ApolloProvider>
    )

    await wait(0)

    render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('./products')
    expect(showProductsErrors).toHaveBeenCalled()
    expect(publishSurvey).not.toHaveBeenCalled()
  })

  test('should call publishSurvey then redirect to /surveys when the survey has all products is valid', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateProducts: () => true,
      validateQuestions: () => ({ isValid: true }),
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <SaveSurveyDraft.WrappedComponent history={history} />
      </ApolloProvider>
    )

    await wait(0)

    render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('/operator/draft/edit/survey-1')
    expect(showProductsErrors).not.toHaveBeenCalled()
    expect(publishSurvey).toHaveBeenCalled()
  })

  test('should call publishSurvey then redirect to /surveys when the survey has all products is invalid valid', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateProducts: () => true,
      validateQuestions: () => ({ invalidQuestion: false }),
      showQuestionsErrors
    }))
    render = mount(
      <ApolloProvider client={client}>
        <SaveSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    await render
      .find(Button)
      .first()
      .prop('onClick')()
  })

  test('should show error message while saving error', async () => {
    usePublishSurvey.mockImplementation(() => () => {
      throw new Error('Error while saving the draft.')
    })
    useSurveyValidate.mockImplementation(() => ({
      validateProducts: () => true,
      validateQuestions: () => ({ isValid: true }),
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <SaveSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)
    expect(displayErrorPopup).toHaveBeenCalledWith(
      'Error while saving the draft.'
    )
  })
})
