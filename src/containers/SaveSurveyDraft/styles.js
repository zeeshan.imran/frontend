import styled from 'styled-components'
import Button from '../../components/Button'

export const ButtonWithMargin = styled(Button)`
  margin-right: 1.5rem;
`
