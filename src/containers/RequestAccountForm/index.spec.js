import React from 'react'
import { mount } from 'enzyme'
import RequestAccountForm from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import sinon from 'sinon'
import gql from 'graphql-tag'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { Formik } from 'formik'

jest.mock('../../utils/userAuthentication', () => ({
  getAuthenticatedUser: jest.fn()
}))

jest.mock('../../utils/userAuthentication')
jest.mock('../../utils/displayErrorPopup')

const REQUEST_ACCOUNT = gql`
  mutation requestAccount($input: RequestAccountInput) {
    requestAccount(input: $input)
  }
`

describe('RequestAccountForm', () => {
  let testRender
  let client
  let historyMock
  let spyChange

  let token
  let email

  beforeEach(() => {
    token = 'token01token'
    email = 'test@test.com'

    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    client = createApolloMockClient()

    client.cache.writeQuery({
      query: REQUEST_ACCOUNT,
      variables: {
        input: {
          email: email,
          token: token,
          password: 'test'
        }
      },

      data: {}
    })

    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: REQUEST_ACCOUNT }))
      .callsFake(spyChange)
    stub.callThrough()
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render RequestAccountForm', () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: false }))

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <RequestAccountForm
            match={{ params: { surveyId: 'survey-1' } }}
            location={{ search: '' }}
            userType={'superAdmin'}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()

    const submitValue = {
      email: 'test@test.com',
      firstName: 'firstName',
      lastName: 'lastName',
      companyName: 'companyName',
      code: 'code',
      number: 1
    }

    const onSubmit = testRender
      .find(Formik)
      .first()
      .props('onSubmit')
    onSubmit.onSubmit(submitValue)
  })

  test('should succed request account', () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: false }))
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <RequestAccountForm
            match={{ params: { surveyId: 'survey-1' } }}
            location={{ search: '' }}
            userType={'superAdmin'}
          />
        </Router>
      </ApolloProvider>
    )

    const submitValue = {
      email: 'test@test.com',
      firstName: 'firstName',
      lastName: 'lastName',
      companyName: 'companyName',
      code: 'code',
      number: 1
    }

    const onSubmit = testRender
      .find(Formik)
      .first()
      .props('onSubmit')
    onSubmit.onSubmit(submitValue)
  })

  test('should fail request account', () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: false }))
    let requestAccountMock = {
      request: {
        query: REQUEST_ACCOUNT,
        variables: { input: { surveyEnrollment: 'survey-enrollment-1' } }
      },
      result: () => {
        throw new Error('testing the error') 
      }
    }
    client = createApolloMockClient({
      mocks: [requestAccountMock]
    })
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <RequestAccountForm
            match={{ params: { surveyId: 'survey-1' } }}
            location={{ search: '' }}
            userType={'superAdmin'}
          />
        </Router>
      </ApolloProvider>
    )

    const submitValue = {
      email: 'test@test.com',
      firstName: 'firstName',
      lastName: 'lastName',
      companyName: 'companyName',
      code: 'code',
      number: 1
    }

    const onSubmit = testRender
      .find(Formik)
      .first()
      .props('onSubmit')
    onSubmit.onSubmit(submitValue)
  })
})
