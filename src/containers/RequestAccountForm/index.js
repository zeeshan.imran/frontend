import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import path from 'path'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'
import { Formik } from 'formik'
import * as Yup from 'yup'
import RequestAccountFormComponent from '../../components/RequestAccountForm'
import { useTranslation } from 'react-i18next';
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'


const RequestAccountForm = ({ match, history, location, userType }) => {
  const { t } = useTranslation();

  const [loading, setLoading] = useState(false)
  const requestAccount = useMutation(REQUEST_ACCOUNT)
  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('containers.requestAccountForm.emailValid'))
      .required(t('containers.requestAccountForm.emailRequired')),
    firstName: Yup.string().required(t('containers.requestAccountForm.firstName')),
    lastName: Yup.string().required(t('containers.requestAccountForm.lastName')),
    companyName: Yup.string().required(t('containers.requestAccountForm.companyName')),
    number: Yup.string().required(t('containers.requestAccountForm.number')),
    code: Yup.string().required(t('containers.requestAccountForm.code')),
  })
 

  const submitRequest = async ({
    email,
    firstName,
    lastName,
    companyName,
    code,
    number
  }) => {
    setLoading(true)
    try {
      await requestAccount({
        variables: {
          input: {
            email,
            userType,
            firstName,
            lastName,
            companyName,
            phoneNumber: `${code} ${number}`
          }
        }
      })
      setLoading(false)

      history.push(path.normalize(`${match.url}/submitted`))
    } catch (error) {
      let messageToDisplay = getErrorMessage(error, 'E_REQUEST_ACCOUNT')

      let errorMessage = error.message.replace('GraphQL error: ', '')
      if(errorMessage.includes('Taster Account')){
        messageToDisplay = t('errors.tasterEmailExist')
      }
      displayErrorPopup(messageToDisplay)
      setLoading(false)
    }
  }

  return (
    <Formik
      validationSchema={validationSchema}
      onSubmit={submitRequest}
      render={({
        values,
        errors,
        touched,
        isValid,
        setFieldValue,
        handleSubmit,
        handleBlur,
        handleChange
      }) => (
        <RequestAccountFormComponent
          {...values}
          loading={loading}
          errors={errors}
          touched={touched}
          setFieldValue={setFieldValue}
          handleChange={handleChange}
          handleBlur={handleBlur}
          handleSubmit={handleSubmit}
          canSubmit={isValid}
        />
      )}
    />
  )
}

const REQUEST_ACCOUNT = gql`
  mutation requestAccount($input: RequestAccountInput) {
    requestAccount(input: $input)
  }
`

export default withRouter(RequestAccountForm)
