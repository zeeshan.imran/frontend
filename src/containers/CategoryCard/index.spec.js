import React from 'react'
import { mount } from 'enzyme'
import CategoryCard from '.'
import { MockedProvider } from 'react-apollo/test-utils'
import { Router } from 'react-router-dom'
import wait from '../../utils/testUtils/waait'
import CategoryCardComponent from '../../components/CategoryCard'

describe('CategoryCard', () => {
  let testRender

  let category
  let history
  let otherProps

  beforeEach(() => {
    category = 'cat-1'
    otherProps = {}
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CategoryCard', async () => {
    testRender = mount(
      <MockedProvider>
        <Router history={history}>
          <CategoryCard
            category={category}
            history={history}
            otherProps={otherProps}
          />
        </Router>
      </MockedProvider>
    )
    expect(testRender).toMatchSnapshot()

    testRender.find(CategoryCardComponent).simulate('click')

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('/categories/undefined')
  })
})
