import React from 'react'
import { withRouter } from 'react-router-dom'
import CategoryCardComponent from '../../components/CategoryCard'
import {
  getCategoryKey,
  getCategoryName
} from '../../utils/getters/CategoryGetters'
import { getImages } from '../../utils/getImages'
import { imagesCollection } from '../../assets/png'
const { desktopImage } = getImages(imagesCollection)

const CategoryCard = ({ category, history, ...otherProps }) => (
  <CategoryCardComponent
    {...otherProps}
    key={getCategoryKey(category)}
    picture={desktopImage} // FIXME Temporary workaround until we have image support from the backend
    name={getCategoryName(category)}
    onClick={() => history.push(`/categories/${getCategoryKey(category)}`)}
  />
)

export default withRouter(CategoryCard)
