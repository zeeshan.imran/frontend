import { path } from 'ramda'
import React, { useState } from 'react'
import UsersListComponent from '../../components/UsersList'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import AlertModal from '../../components/AlertModal'
import { DEFAULT_N_ELEMENTS_PER_PAGE, PUBSUB } from '../../utils/Constants'
import { useTranslation } from 'react-i18next'
import useSearch from '../../hooks/useSearch'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'
import PubSub from 'pubsub-js'

import {
  isUserAuthenticatedAsSuperAdmin,
  setAuthenticatedUser,
  getAuthenticatedUser
} from '../../utils/userAuthentication/index'

const UsersListContainer = ({ history, location }) => {
  const {
    keyword,
    orderBy,
    orderDirection,
    page,
    handleKeywordChange,
    handleTableChange
  } = useSearch({
    location,
    history
  })
  const { t } = useTranslation()
  const isAdmin = isUserAuthenticatedAsSuperAdmin()
  const userTypes = ['operator', 'taster', 'analytics', 'power-user']
  const [isEditUser, setisEditUser] = useState(false)
  const [newUserData, setnewUserData] = useState({})
  const [enableSubmitButton, setEnableSubmitButton] = useState(false)
  const [emailAlreadyInUse, setEmailAlreadyInUse] = useState('')
  const [modalType, setModalType] = useState('edit')

  const addUser = useMutation(ADD_USER)
  const deleteUser = useMutation(DELETE_USER)
  const editUser = useMutation(EDIT_USER)
  const forgotPassword = useMutation(FORGOT_PASSWORD)

  const organizationsListResult = useQuery(GET_ORGANIZATIONS_LIST, {
    variables: { input: { limit: 0 } },
    fetchPolicy: 'cache-and-network',
    skip: !isAdmin
  })
  const organizationsList =
    path(['data', 'organizations'])(organizationsListResult) || []

  let orderByCustom = orderBy === 'organization.name' ? 'organization' : orderBy
  const usersList = useQuery(USERS, {
    variables: {
      input: {
        type: userTypes,
        keyword,
        orderBy: orderByCustom,
        orderDirection,
        skip: (page - 1) * DEFAULT_N_ELEMENTS_PER_PAGE
      }
    },
    fetchPolicy: 'cache-and-network'
  })

  const userCount = useQuery(COUNT_USERS, {
    variables: {
      input: { type: userTypes, keyword }
    },
    fetchPolicy: 'cache-and-network'
  })

  const toggleEditUserForm = isOpen => {
    if (isOpen !== true && isOpen !== false) {
      const existingUserData = usersList.data.users.find(
        user => user.id === isOpen
      )
      setnewUserData({
        ...existingUserData,
        organization:
          existingUserData.organization && existingUserData.organization.id
      })
    } else {
      setnewUserData({})
    }
    setisEditUser(isOpen)
  }

  const getEditUserInput = user => {
    const { __typename, isSuperAdmin, isTaster, ...rest } = user

    // only admin can update isAdmin, isTaster for operators
    if (
      isAdmin &&
      /[^@]+@flavorwiki.com$/i.test(rest.emailAddress) &&
      rest.type === 'operator'
    ) {
      return {
        ...rest,
        isSuperAdmin,
        isTaster
      }
    }

    return rest
  }

  const setEnableSubmit = (errors, values) => {
    setEmailAlreadyInUse('')
    if (Object.keys(values).length > 0 && !Object.keys(errors).length) {
      setEnableSubmitButton(true)
    } else {
      setEnableSubmitButton(false)
    }
  }
  const handleAddUser = async () => {
    try {
      await addUser({
        variables: { input: getEditUserInput(newUserData) }
      })
      displaySuccessMessage(t('containers.usersListContainer.added'))
      usersList.refetch()
      userCount.refetch()
      await forgotPassword({ variables: { email: newUserData.emailAddress } })
      setisEditUser(false)
    } catch (err) {
      if (err.message.includes('already in use')) {
        setEnableSubmitButton(false)
        setEmailAlreadyInUse('Email Address already in use')
      } else {
        displayErrorPopup(getErrorMessage(err, 'E_UNIQUE_USER'))
      }
    }
  }

  const handleEditUser = async () => {
    const editedUser = getEditUserInput(newUserData)
    await editUser({
      variables: { input: getEditUserInput(newUserData) }
    })
    const currentUser = getAuthenticatedUser()
    if (currentUser.id === editedUser.id) {
      setAuthenticatedUser({
        ...currentUser,
        ...editedUser
      })
      PubSub.publish(PUBSUB.UPDATE_OPERATOR_DROPDOWN)
    }
    displaySuccessMessage(t('containers.usersListContainer.updated'))
    setisEditUser(false)
  }

  const handleDeleteUser = async id => {
    await deleteUser({ variables: { id: id } })
    displaySuccessMessage(t('containers.usersListContainer.deleted'))
    usersList.refetch()
    userCount.refetch()
  }
  const handleDeleteUserModal = id => {
    AlertModal({
      title: t('containers.usersListContainer.alertModalTitle'),
      description: t('containers.usersListContainer.alertModalDescription'),
      okText: t('containers.usersListContainer.alertModalokText'),
      handleOk: () => handleDeleteUser(id),
      handleCancel: () => { }
    })
  }

  const handleChangeUserData = newData => {
    setnewUserData(newData)
  }

  return (
    <UsersListComponent
      isAdmin={isAdmin}
      toggleEditUserForm={toggleEditUserForm}
      isEditUser={isEditUser}
      users={usersList && usersList.data ? usersList.data.users : []}
      handleChangeSelection={() => { }}
      handelAdd={handleAddUser}
      handleEdit={handleEditUser}
      handleRemove={handleDeleteUserModal}
      newUserData={newUserData}
      userTypes={userTypes}
      setnewUserData={handleChangeUserData}
      organizationsList={organizationsList}
      page={page}
      userCount={userCount && userCount.data && userCount.data.countUsers}
      onTableChange={handleTableChange}
      handleSearch={handleKeywordChange}
      searchBy={keyword}
      orderBy={orderBy}
      orderDirection={orderDirection}
      // Submit Button Logic
      setEnableSubmit={setEnableSubmit}
      enableSubmit={enableSubmitButton}
      emailAlreadyInUse={emailAlreadyInUse}
      // Modal Logic
      modalType={modalType}
      setModalType={setModalType}
      setUserData={setnewUserData}
      location={location}
      history={history}
    />
  )
}

export const GET_ORGANIZATIONS_LIST = gql`
  query organizations($input: OrganizationsInput!) {
    organizations(input: $input) {
      id
      name
    }
  }
`

export const COUNT_USERS = gql`
  query countUsers($input: CountUsersInput!) {
    countUsers(input: $input)
  }
`

export const USERS = gql`
  query users($input: UsersInput!) {
    users(input: $input) {
      id
      emailAddress
      fullName
      birthYear
      nationality
      gender
      initialized
      type
      isTaster
      isSuperAdmin
      organization {
        id
        name
      }
    }
  }
`

const ADD_USER = gql`
  mutation addUser($input: EditUserInput) {
    addUser(input: $input) {
      id
      emailAddress
      fullName
      birthYear
      nationality
      gender
      initialized
      type
      isTaster
      isSuperAdmin
      organization {
        id
        name
      }
    }
  }
`

export const EDIT_USER = gql`
  mutation editUser($input: EditUserInput) {
    editUser(input: $input) {
      id
      emailAddress
      fullName
      birthYear
      nationality
      gender
      initialized
      type
      isTaster
      isSuperAdmin
      organization {
        id
        name
      }
    }
  }
`

const DELETE_USER = gql`
  mutation deleteUser($id: ID!) {
    deleteUser(id: $id)
  }
`

const FORGOT_PASSWORD = gql`
  mutation forgotPassword($email: String!) {
    forgotPassword(email: $email)
  }
`

export default UsersListContainer
