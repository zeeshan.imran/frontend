import React from 'react'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import UsersListContainer, {
  GET_ORGANIZATIONS_LIST,
  COUNT_USERS,
  USERS
} from './index'
import { Modal, Table } from 'antd'
import { create, act } from 'react-test-renderer'
import { mount } from 'enzyme'
import wait from '../../utils/testUtils/waait'
import UsersListComponent from '../../components/UsersList'

jest.mock('../../utils/displaySuccessMessage')
// jest.doMock('pubsub-js', () => ({
//   publish: jest.fn()
// }))
jest.doMock('./index.js')
jest.mock('rc-util/lib/Portal')

describe('check the operator interaction when adding, searching, and deleting users', () => {
  let testRender
  let client
  let location
  const useRefRpy = jest
    .spyOn(React, 'useRef')
    .mockReturnValueOnce({ current: { validateForm: jest.fn() } })

  let mockPush = jest.fn(({ search }) => {
    location = {
      ...location,
      search
    }
  })

  beforeAll(async () => {
    const localStorageMock = (function () {
      let store = {
        'flavorwiki-user-details': JSON.stringify({ isSuperAdmin: true }),
        'flavorwiki-operator-token': 'some_auth_token'
      }
      return {
        getItem: function (key) {
          return store[key] || null
        },
        setItem: function (key, value) {
          store[key] = value.toString()
        },
        removeItem: function (key) {
          delete store[key]
        },
        clear: function () {
          store = {}
        }
      }
    })()
    Object.defineProperty(window, 'localStorage', {
      value: localStorageMock
    })
    location = {
      path: '/operator/users'
    }
    client = createApolloMockClient()
  })

  afterEach(() => {
    // testRender.unmount()
  })

  const update = () => {
    testRender.update(
      <ApolloProvider client={client} removeTypename>
        <UsersListContainer
          history={{
            push: mockPush
          }}
          location={location}
          match={{ url: '/operator/users' }}
        />
      </ApolloProvider>
    )
  }

  test('modal receives isAdmin prop true', async () => {
    testRender = mount(
      <ApolloProvider client={client} removeTypename>
        <UsersListContainer
          history={{
            push: mockPush
          }}
          location={location}
          match={{ url: '/operator/users' }}
        />
      </ApolloProvider>
    )

    const isAdmin = testRender.find(UsersListComponent).prop('isAdmin')
    update()
    await wait(0)
    expect(isAdmin).toBe(true)
  })

  test('modal receives isAdmin prop false', async () => {
    testRender.unmount()
    window.localStorage.setItem(
      'flavorwiki-user-details',
      JSON.stringify({
        isSuperAdmin: false
      })
    )
    testRender = mount(
      <ApolloProvider client={client} removeTypename>
        <UsersListContainer
          history={{
            push: mockPush
          }}
          location={location}
          match={{ url: '/operator/users' }}
        />
      </ApolloProvider>
    )
    await wait(0)

    const isAdmin = testRender.find(UsersListComponent).prop('isAdmin')
    update()
    expect(isAdmin).toBe(false)
  })

  test('orderBy to history.push', async () => {
    update()
    const names = ['fullName', 'fullName', 'emailAddress', 'type']
    const order = ['descend', 'ascend', 'descend', 'ascend']
    const organizationTable = testRender.find(Table)
    for (let index = 0; index < names.length; index++) {
      act(() => {
        organizationTable.prop('onChange')(
          {
            current: 1
          },
          '',
          {
            field: names[index],
            order: order[index]
          }
        )
      })
      update()
      expect(location.search).toBe(
        `orderBy=${names[index]}&orderDirection=${order[index]}&page=1`
      )
    }
  })

  test('search bar to history.push', async () => {
    testRender = mount(
      <ApolloProvider client={client} removeTypename>
        <UsersListContainer
          history={{
            push: mockPush
          }}
          location={location}
          match={{ url: '/operator/users' }}
        />
      </ApolloProvider>
    )
    const searchBar = testRender.find({ placeholder: 'Search' }).first()
    act(() => {
      searchBar.prop('handleChange')('this is a search text')
    })
    update()
    expect(location.search).toBe(
      'orderBy=type&orderDirection=ascend&page=1&keyword=this%20is%20a%20search%20text'
    )
    testRender.unmount()
  })

  test('open add user modal', async () => {
    testRender = mount(
      <ApolloProvider client={client} removeTypename>
        <UsersListContainer
          history={{
            push: mockPush
          }}
          location={location}
          match={{ url: '/operator/users' }}
        />
      </ApolloProvider>
    )
    act(() => {
      testRender.find({ children: 'Add user' }).first().simulate('click')
    })

    const modal = testRender.find(Modal)
    expect(modal.prop('visible')).toBe(true)
  })

  // test('edit user data', async () => {
  //   const newUserData = {
  //     emailAddress: 'user email address',
  //     fullName: 'this is the user full name',
  //     organization: 'some organization id',
  //     type: 'user type'
  //   }
  //   testRender.find({ id: 'user form' }).prop('setnewUserData')(newUserData)
  //   expect(JSON.stringify(testRender.find(UsersListComponent).prop('newUserData'))).toBe(
  //     JSON.stringify(newUserData)
  //   )
  //   testRender.unmount()
  // })
})
