import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import wait from '../../utils/testUtils/waait'
import Button from '../../components/Button'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import useUpdateSurvey from '../../hooks/useUpdateSurvey'
import PublishSurveyDraft, { RESUME_SURVEY } from './index'
import { defaultSurvey } from '../../mocks'

const createSurveyCreationMock = secondProduct => ({
  ...defaultSurvey,
  __typename: 'SurveyCreation',
  products: [
    {
      name: 'the first product',
      brand: 'the brand'
    },
    secondProduct
  ]
})

jest.mock('../../hooks/useUpdateSurvey')
jest.mock('../../hooks/useSurveyValidate')

describe('publish survey draft', () => {
  let render
  let updateSurvey
  let history
  let client
  let showProductsErrors
  let showQuestionsErrors

  const match = { params: { surveyId: 'survey-1' } }

  beforeEach(() => {
    client = createApolloMockClient({
      mocks: [
        {
          request: {
            query: RESUME_SURVEY,
            variables: { id: 'survey-1' }
          },
          result: {
            data: {}
          }
        }
      ]
    })
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: '',
          brand: 'the brand'
        })
      }
    })
    updateSurvey = jest.fn()
    showProductsErrors = jest.fn()
    showQuestionsErrors = jest.fn()
    useUpdateSurvey.mockImplementation(() => updateSurvey)

    history = {
      push: jest.fn()
    }
  })

  afterEach(() => {
    render.unmount()
  })

  test('should switch to products tab and show errors when the survey has at least one invalid product', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateProducts: () => false,
      validateQuestions: () => ({ isValid: true }),
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <PublishSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('./products')
    expect(showProductsErrors).toHaveBeenCalled()
    expect(updateSurvey).not.toHaveBeenCalled()
  })

  test('should call updateSurvey then redirect to /surveys when the survey has all products is valid', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateProducts: () => true,
      validateQuestions: () => ({ isValid: true }),
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <PublishSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    await render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('/operator/surveys')
    expect(showProductsErrors).not.toHaveBeenCalled()
    expect(updateSurvey).toHaveBeenCalled()
  })

  test('should call publishSurvey then redirect to /surveys when the survey has all products is invalid valid', async () => {
    const match = { params: { surveyId: 'survey-1' } }

    useSurveyValidate.mockImplementation(() => ({
      validateProducts: () => true,
      validateQuestions: () => ({ invalidQuestion: false }),
      showQuestionsErrors
    }))
    render = mount(
      <ApolloProvider client={client}>
        <PublishSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    await render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)
  })

  test('should call publishSurvey then redirect on error of togglePublishing', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateProducts: () => true,
      validateQuestions: () => ({ isValid: true }),
      showProductsErrors
    }))

    const matchParam = {
      params: {}
    }

    render = mount(
      <ApolloProvider client={client}>
        <PublishSurveyDraft.WrappedComponent
          history={history}
          match={matchParam}
        />
      </ApolloProvider>
    )

    await render
      .find(Button)
      .first()
      .prop('onClick')()
  })
})
