import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery, useMutation } from 'react-apollo-hooks'
import Button from '../../components/Button'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useToggle from '../../hooks/useToggle/index'
import useUpdateSurvey from '../../hooks/useUpdateSurvey'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import gql from 'graphql-tag'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'
import TooltipWrapper from '../../components/TooltipWrapper'

const PublishSurveyDraft = ({ history, match }) => {
  const { t } = useTranslation()
  const [publishing, togglePublishing] = useToggle(false)
  const {
    data: {
      surveyCreation: { products, basics, questions, mandatoryQuestions }
    }
  } = useQuery(SURVEY_CREATION)
  const activateSurvey = useMutation(RESUME_SURVEY)

  const {
    params: { surveyId }
  } = match
  const updateDraft = useUpdateSurvey({
    surveyId,
    successMessage: t('containers.publishSurveyDraft.successMessage'),
    errorMessage: t('containers.publishSurveyDraft.errorMessage')
  })
  const surveyValidate = useSurveyValidate()

  const submitDisabled = !basics.isScreenerOnly
    ? !basics.uniqueName ||
      products.length === 0 ||
      questions.length + mandatoryQuestions.length === 0 ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
    : !basics.uniqueName ||
      questions.length + mandatoryQuestions.length === 0 ||
      basics.linkedSurveys.length === 0

  const onPublish = async () => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails`)
      return
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion,
      noQuestionError
    } = await surveyValidate.validateQuestions(true)

    if (noQuestionError) {
      await surveyValidate.showOnlyMandatoryQuestionErrors()
      return
    }

    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(`./financial?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    if (!isQuestionsValid) {
      history.push(`./questions?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    togglePublishing()
    try {
      await updateDraft()
      await activateSurvey({ variables: { id: surveyId } })
      togglePublishing()
      history.push('/operator/surveys')
    } catch (error) {
      togglePublishing()
      displayErrorPopup(error && error.message)
    }
  }

  return (
    <TooltipWrapper
      helperText={t('tooltips.publishSurvey')}
      placement='leftTop'
    >
      <Button
        type={submitDisabled ? 'disabled' : 'primary'}
        size='default'
        onClick={onPublish}
      >
        {t('containers.publishSurveyDraft.publishButton')}
      </Button>
      <LoadingModal
        visible={publishing}
        text={t('containers.publishSurveyDraft.loadingText')}
      />
    </TooltipWrapper>
  )
}

export const RESUME_SURVEY = gql`
  mutation resumeSurvey($id: ID!) {
    resumeSurvey(id: $id) {
      id
      state
    }
  }
`

export default withRouter(PublishSurveyDraft)
