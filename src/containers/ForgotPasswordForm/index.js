import React, { useState, useCallback } from 'react'
import { Formik } from 'formik'
import * as yup from 'yup'
import ForgotPasswordFormComponent from '../../components/ForgotPasswordForm'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { useTranslation } from 'react-i18next';

const ForgotPasswordForm = ({ handleGoBack }) => {
  const {t} = useTranslation();
  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .email(t('containers.forgotPassword.emailValid'))
      .required(t('containers.forgotPassword.emailRequire'))
  })
  const [loading, setLoading] = useState('')
  const [infoMessage, setInfoMessage] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const clearMessages = useCallback(() => {
    setInfoMessage('')
    setErrorMessage('')
  }, [])

  const forgotPassword = useMutation(FORGOT_PASSWORD)

  return (
    <Formik
      onSubmit={async ({ email }) => {
        clearMessages()
        setLoading(true)
        try {
          await forgotPassword({ variables: { email } })
          setInfoMessage(t('containers.forgotPassword.infoMessage'))
        } catch (error) {
          setErrorMessage(t('containers.forgotPassword.error'))
        } finally {
          setLoading(false)
        }
      }}
      validationSchema={validationSchema}
      render={({ values, ...formikProps }) => (
        <ForgotPasswordFormComponent
          {...formikProps}
          {...values}
          loading={loading}
          handleGoBack={handleGoBack}
          errorMessage={errorMessage}
          infoMessage={infoMessage}
        />
      )}
    />
  )
}

const FORGOT_PASSWORD = gql`
  mutation forgotPassword($email: String!) {
    forgotPassword(email: $email)
  }
`

export default ForgotPasswordForm
