import React from 'react'
import { mount } from 'enzyme'
import ForgotPasswordForm from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import sinon from 'sinon'
import gql from 'graphql-tag'
import { Formik } from 'formik'
import { getAuthenticatedUser } from '../../utils/userAuthentication'

jest.mock('../../utils/userAuthentication')
jest.mock('../../utils/displayErrorPopup')
jest.mock('../../utils/displaySuccessMessage')

const FORGOT_PASSWORD = gql`
  mutation forgotPassword($email: String!) {
    forgotPassword(email: $email)
  }
`

describe('ForgotPasswordForm', () => {
  let testRender
  let client
  let historyMock
  let handleGoBack

  beforeEach(() => {
    handleGoBack = jest.fn()

    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    client = createApolloMockClient()

    const stub = sinon.stub(client, 'mutate')

    stub
      .withArgs(sinon.match({ mutation: FORGOT_PASSWORD }))
      .returns({ data: {} }) 
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should success new password', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <ForgotPasswordForm handleGoBack={handleGoBack} />
        </Router>
      </ApolloProvider>
    )

    const submitValue = {
      email: 'test@test.com'
    }

    const onSubmit = testRender
      .find(Formik)
      .first()
      .props('onSubmit')
    onSubmit.onSubmit(submitValue)
  })

  test('should fail new password', () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: false }))
    let requestAccountMock = {
      request: {
        query: FORGOT_PASSWORD,
        variables: { input: { surveyEnrollment: 'survey-enrollment-1' } }
      },
      result: () => {
        throw new Error('testing the error')
      }
    }
    client = createApolloMockClient({
      mocks: [requestAccountMock]
    })
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <ForgotPasswordForm handleGoBack={handleGoBack} />
        </Router>
      </ApolloProvider>
    )

    const submitValue = {
      email: 'test@test.com'
    }

    const onSubmit = testRender
      .find(Formik)
      .first()
      .props('onSubmit')
    onSubmit.onSubmit(submitValue)
  })
})
