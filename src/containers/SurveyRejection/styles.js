import styled from 'styled-components'
import { DEFAULT_MOBILE_PADDING } from '../../utils/Metrics'

export const Container = styled.div`
  padding: ${({ desktop }) =>
    desktop ? '0 0 8rem 0' : `5rem ${DEFAULT_MOBILE_PADDING}rem`};
`
