import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import IconButton from '../../components/IconButton'
import { useTranslation } from 'react-i18next'
import ConfirmEditMode from './ConfirmEditMode'

const EditSurveyButton = ({ surveyId, history }) => {
  const { t } = useTranslation()
  const [modalVisible, setModalVisible] = useState(false)
  const onClick = () => {
    setModalVisible(true)
  }

  return (
    <React.Fragment>
      <IconButton
        tooltip={t('tooltips.editSurveyButton')}
        onClick={onClick}
        type='edit'
      />
      <ConfirmEditMode
        visible={modalVisible}
        onOk={({ editMode }) => {
          if (editMode === 'stricted') {
            history.push(`/operator/survey/stricted-edit/${surveyId}`)
          } else {
            history.push(`/operator/survey/edit/${surveyId}`)
          }
        }}
        onCancel={() => {
          setModalVisible(false)
        }}
      />
    </React.Fragment>
  )
}

export default withRouter(EditSurveyButton)
