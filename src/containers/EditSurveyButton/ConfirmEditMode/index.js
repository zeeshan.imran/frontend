import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Modal } from 'antd'
import HelperText from '../HelperText'
import { RadioGroup,RadioButton } from './styles'

const ConfirmEditMode = ({ visible, onOk, onCancel }) => {
  const { t } = useTranslation()
  const [editMode, setEditMode] = useState('stricted')

  return (
    <Modal
      title={t('containers.editSurveyButton.alertModalTitle')}
      visible={visible}
      onOk={() => onOk({ editMode })}
      onCancel={onCancel}
    >
      Would you like to
      <RadioGroup
        value={editMode}
        onChange={e => {
          setEditMode(e.target.value)
        }}
      >
        <RadioButton value='stricted'>
          {t(
            'containers.editSurveyButton.alertModalStrictedOption.BeforeHelpText'
          )}
          <HelperText
            text={t(
              'containers.editSurveyButton.alertModalStrictedOption.HelpText'
            )}
            helperText={t(
              'tooltips.editSurveyAlertStrictOption'
            )}
          />
          {t(
            'containers.editSurveyButton.alertModalStrictedOption.HelpText'
          )}
        </RadioButton>
        <RadioButton value='default'>
          {t(
            'containers.editSurveyButton.alertModalDefaultOption.BeforeHelpText'
          )}
          <HelperText
            text={t(
              'containers.editSurveyButton.alertModalDefaultOption.HelpText'
            )}
            helperText={t(
              'tooltips.editSurveyAlertFullOption'
            )}
          />
          {t(
            'containers.editSurveyButton.alertModalDefaultOption.AfterHelpText'
          )}
        </RadioButton>
      </RadioGroup>
    </Modal>
  )
}

export default ConfirmEditMode
