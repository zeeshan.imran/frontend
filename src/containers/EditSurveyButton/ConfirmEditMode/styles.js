import { Radio } from 'antd'
import styled from 'styled-components'
import colors from '../../../utils/Colors'

export const RadioGroup = styled(Radio.Group)`
  .ant-radio-wrapper {
    display: flex;
    align-items: center;
    margin: 0.5rem 1rem 0 1rem;
    & > span:nth-child(2) {
      white-space: pre-wrap;
      button {
        border: none;
        border-bottom: 1px dashed ${colors.RADIO_TEXT_COLOR};
        background: none;
        color: ${colors.RADIO_TEXT_COLOR};
        padding: 0;
        line-height: 125%;
        outline: none;
        cursor: pointer;
        margin: 0 0.5rem;
        .anticon {
          padding: 0 0 0 0.25rem;
        }
      }
    }
  }
`
export const RadioButton = styled(Radio)`
.ant-radio-inner::after{
  background-color: ${colors.RADIO_BUTTON_BACKGROUND_COLOR};
}
`