import styled from 'styled-components'

export const TooltipContent = styled.div`
  ul {
    margin: 0.5rem 0 0 0;
    padding-left: 2rem;
    li {
      padding-bottom: 0.5rem;
    }
  }
`
