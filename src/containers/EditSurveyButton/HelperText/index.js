import React from 'react'
import { Tooltip, Icon } from 'antd'
import { TooltipContent } from './styles'
import colors from '../../../utils/Colors'

const HelperText = ({ text, helperText }) => (
  <Tooltip
    title={
      <TooltipContent
        dangerouslySetInnerHTML={{
          __html: helperText
        }}
      />
    }
    placement={'bottomRight'}
    arrowPointAtCenter
  >
    <button type='button'>
      {text}
      <Icon type='question-circle' theme='twoTone' twoToneColor={colors.RADIO_ICON_COLOR} />
    </button>
  </Tooltip>
)

export default HelperText
