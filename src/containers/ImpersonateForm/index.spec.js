import React from 'react'
import { mount } from 'enzyme'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import ImpersonateForm, { QUERY_USERS } from '.'
import {
  getAuthenticatedUser,
  getImpersonateUserId
} from '../../utils/userAuthentication'
import ImpersonateFormComponent from '../../components/ImpersonateForm'
import wait from '../../utils/testUtils/waait'

jest.mock('../../utils/userAuthentication')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mockQueryUsers = {
  request: {
    query: QUERY_USERS,
    variables: {
      keyword: ''
    }
  },
  result: {
    data: {
      users: [
        {
          id: 'operator-1',
          fullName: 'Operator 1',
          emailAddress: 'operator-1@flavor-wiki.com',
          type: 'operator'
        },
        {
          id: 'operator-2',
          fullName: 'Operator 2',
          emailAddress: 'operator-2@flavor-wiki.com',
          type: 'operator'
        }
      ]
    }
  }
}

describe('impersonate form', () => {
  let render
  let history

  beforeEach(() => {
    history = {
      replace: jest.fn()
    }
  })

  afterEach(() => {
    render.unmount()
    getImpersonateUserId.mockReset()
  })

  test('should render null if the user is not supper admin', async () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: false }))
    const client = createApolloMockClient({
      mocks: [mockQueryUsers]
    })
    render = mount(
      <ApolloProvider client={client}>
        <ImpersonateForm.WrappedComponent />
      </ApolloProvider>
    )
    expect(render.isEmptyRender()).toBe(true)
  })

  test('should render null when the admin is impersonating', async () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: false }))
    getImpersonateUserId.mockImplementation(() => 'operator-1')
    const client = createApolloMockClient({
      mocks: [mockQueryUsers]
    })
    render = mount(
      <ApolloProvider client={client}>
        <ImpersonateForm.WrappedComponent />
      </ApolloProvider>
    )
    expect(render.isEmptyRender()).toBe(true)
  })

  test('should render ImpersonateForm if the user is not supper admin', async () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: true }))
    const client = createApolloMockClient({
      mocks: [mockQueryUsers]
    })
    render = mount(
      <ApolloProvider client={client}>
        <ImpersonateForm.WrappedComponent location={{ pathname: '/' }} />
      </ApolloProvider>
    )

    expect(render.find(ImpersonateFormComponent)).toHaveLength(1)
    expect(render.find(ImpersonateFormComponent).prop('loading')).toBe(true)

    await wait(0)
    render.update()

    expect(render.find(ImpersonateFormComponent).prop('loading')).toBe(false)

    render.find('.ant-select-search__field').simulate('click')
    await wait(0)
    render.update()
    expect(render.find('.ant-select-dropdown-menu-item')).toHaveLength(2)
  })
  test('should redirect to /impersonate/:userId if the super admin select the user to impersonate', async () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: true }))
    const client = createApolloMockClient({
      mocks: [mockQueryUsers]
    })

    render = mount(
      <ApolloProvider client={client}>
        <ImpersonateForm.WrappedComponent
          history={history}
          location={{ pathname: '/operator/dashboard' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    render.find('.ant-select-search__field').simulate('click')
    await wait(0)
    render.update()

    render
      .find('.ant-select-dropdown-menu-item')
      .at(1)
      .simulate('click')

    await wait(0)
    expect(history.replace).toHaveBeenCalledWith(
      '/impersonate/operator-2?redirect=/operator/dashboard'
    )
  })
})
