import React from 'react'
import TermsOfUse from '../../components/TermsOfUse'
import OperatorPageContent from '../../components/OperatorPageContent'
import OperatorPage from '../../components/OperatorPage'

const TermsOfUseContainer = ({ history, location, handleChange }) => {
  const handleGoToPrivacyPolicy = () => {
    window.scroll(0,0)
    history.push('/privacy-policy')
  }

  const path = location && location.pathname && location.pathname.split('/')
  let isOperator = false
  if (Array.isArray(path)) {
    isOperator = path && path.includes('operator')
  }

  return (
    <OperatorPage>
      <OperatorPageContent>
        <TermsOfUse
          goToPrivacyPolicy={handleChange || handleGoToPrivacyPolicy}
          isOperator={isOperator}
        />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default TermsOfUseContainer
