import React from 'react'
import { mount } from 'enzyme'
import SurveyCard from '.'
import ProductCard from '../../components/ProductCard'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { Router } from 'react-router-dom'
import { defaultSurvey } from '../../mocks'
const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    name: 'Survey 1',
    uniqueName: 'survey-1',
  },
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ]
}

describe('SurveyCard', () => {
  let testRender
  let survey
  let otherProps
  let mockClient
  let history

  beforeEach(() => {
    survey = {
      inProgress: false,
      numberOfProducts: 2,
      surveyId: 'survey-1',
      surveyEnrollmentId: null,
      state: null
    }

    otherProps = {}
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        survey: mockSurveyCreation
      }
    })
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyCard', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveyCard
            survey={{ survey }}
            otherProps={otherProps}
            history={history}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(ProductCard)).toHaveLength(1)
  })

  test('should render SurveyCard onclick history', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveyCard
            survey={{ survey }}
            otherProps={otherProps}
            history={history}
          />
        </Router>
      </ApolloProvider>
    )
    testRender.find(ProductCard).prop('onClick')()
  })
})
