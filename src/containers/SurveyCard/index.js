import React from 'react'
import { withRouter } from 'react-router-dom'
import ProductCard from '../../components/ProductCard'
import { getSurveyTitle, getSurveyId } from '../../utils/getters/SurveyGetters'

const SurveyCard = ({ survey, history, ...otherProps }) => (
  <ProductCard
    {...otherProps}
    name={getSurveyTitle(survey)}
    image={
      'https://i5.walmartimages.com/asr/be75486b-2623-4f57-9713-efa5bdbec9b1_1.05716c9f3ab241decfb7c7a81f5e6557.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF'
    } // FIXME Temporary image until we have support from the backend
    onClick={() => history.push(`/survey/${getSurveyId(survey)}`)}
  />
)

export default withRouter(SurveyCard)
