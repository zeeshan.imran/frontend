import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'
import BackNavigationComponent from '../../components/BackNavigation'

const SURVEY_ID_QUERY = gql`
  query {
    currentSurveyParticipation @client {
      surveyId
    }
  }
`

const BackNavigation = ({ title, history }) => {
  const { data } = useQuery(SURVEY_ID_QUERY)

  const { surveyId } = data.currentSurveyParticipation

  return (
    <BackNavigationComponent
      title={title}
      action={() =>
        surveyId ? history.push(`/survey/${surveyId}`) : history.goBack()
      }
    />
  )
}

export default withRouter(BackNavigation)
