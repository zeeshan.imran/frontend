import React from 'react'
import { mount } from 'enzyme'
import BackNavigation from '.'
import BackNavigationComponent from '../../components/BackNavigation'
import gql from 'graphql-tag'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

import wait from '../../utils/testUtils/waait'

const SURVEY_ID_QUERY = gql`
  query {
    currentSurveyParticipation @client {
      surveyId
    }
  }
`

describe('BackNavigation', () => {
  let testRender
  let title
  let client
  let history

  beforeEach(() => {
    title = 'Back'
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: SURVEY_ID_QUERY,
      data: {
        currentSurveyParticipation: []
      }
    })
    history = {
      push: jest.fn(),
      goBack: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveysGridComponent', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <BackNavigation desktop={''} title={title} />
        </Router>
      </ApolloProvider>
    )
    await wait(0)
    testRender.update()
    expect(testRender).toMatchSnapshot()
  })

  test('should render SurveysGridComponent', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <BackNavigation desktop={''} title={title} />
        </Router>
      </ApolloProvider>
    )

    testRender.find(BackNavigationComponent).simulate('click')

    await wait(0)

    expect(history.goBack).toHaveBeenCalled()
  })
})
