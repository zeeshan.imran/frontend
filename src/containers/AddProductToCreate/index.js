import React from 'react'
import { generate } from 'shortid'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { useTranslation } from 'react-i18next'
import CreationButtonContainer from '../../components/CreationButtonContainer'

export const UPDATE_SURVEY_CREATION_PRODUCTS = gql`
  mutation updateSurveyCreationProducts($products: products) {
    updateSurveyCreationProducts(products: $products) @client
  }
`

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
      basics {
        productDisplayType
      }
    }
  }
`

const AddProductToCreate = ({ stricted }) => {
  const { t } = useTranslation()

  const {
    data: {
      surveyCreation: { products, basics }
    },
    loading
  } = useQuery(SURVEY_CREATION)

  const updateProducts = useMutation(UPDATE_SURVEY_CREATION_PRODUCTS)

  const createNewProduct = () =>
    updateProducts({
      variables: {
        products: [
          ...products,
          {
            clientGeneratedId: generate(),
            name: '',
            brand: '',
            photo: '',
            isAvailable: true,
            reward: 0,
            sortingOrderId: products.length + 1
          }
        ]
      }
    })

  const addMorePorductsButton = productDisplayType => {
    if (
      (productDisplayType === 'permutation' ||
        productDisplayType === 'reverse' || productDisplayType === 'forced') &&
      stricted
    ) {
      return null
    } else {
      return (
        <CreationButtonContainer
          onClick={createNewProduct}
          text={t('containers.addProductsToCreate.newProduct')}
          data-testid='new-product-button'
          tooltip={t('tooltips.newProduct')}
        />
      )
    }
  }
  const { productDisplayType } = basics
  return !loading && addMorePorductsButton(productDisplayType)
}

export default AddProductToCreate
