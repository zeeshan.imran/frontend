import React from 'react'
import { mount } from 'enzyme'
import AddProductToCreate, {
  SURVEY_CREATION,
  UPDATE_SURVEY_CREATION_PRODUCTS
} from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import sinon from 'sinon'
import CreationButtonContainer from '../../components/CreationButtonContainer'

jest.mock('../../utils/userAuthentication')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('AddProductToCreate', () => {
  let testRender
  let client
  let historyMock
  let spyChange

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    client = createApolloMockClient()

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          products: [
            {
              id: 'product-1',
              name: 'product',
              brand: 'brand'
            }
          ],
          basics: {
            productDisplayType: 'none'
          }
        }
      }
    })

    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: UPDATE_SURVEY_CREATION_PRODUCTS }))
      .callsFake(spyChange)
    stub.callThrough()
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render AddProductToCreate', () => {
    getAuthenticatedUser.mockImplementation(() => ({
      isSuperAdmin: true
    }))

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <AddProductToCreate history={historyMock} />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()

    spyChange.mockReset()
    testRender.find(CreationButtonContainer).prop('onClick')()
    expect(spyChange).toHaveBeenCalled()

    
  })
})
