import React from 'react'
import PreviewSurveyDraft from '../PreviewSurveyDraft'
import UpdateSurveyDraft from '../UpdateSurveyDraft'
import PublishSurveyDraft from '../PublishSurveyDraft'
import OperatorSurveyDetailTabBar from '../../components/OperatorSurveyDetailTabBar'

const publishSurvey = [
  <PreviewSurveyDraft key='previewSurveyDraft' />,
  <UpdateSurveyDraft key='updateSurveyDraft' />,
  <PublishSurveyDraft key='publishSurveyDraft' />
]

const OperatorSurveyCreationTabBar = () => (
  <OperatorSurveyDetailTabBar actions={publishSurvey} />
)

export default OperatorSurveyCreationTabBar
