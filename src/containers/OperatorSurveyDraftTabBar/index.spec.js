import React from 'react'
import { shallow } from 'enzyme'
import OperatorSurveyCreationTabBar from '.'
import OperatorSurveyDetailTabBar from '../../components/OperatorSurveyDetailTabBar'

describe('OperatorSurveyCreationTabBar', () => {
  let testRender
  test('should render OperatorSurveyCreationTabBar', async () => {
    testRender = shallow(<OperatorSurveyCreationTabBar />)
    expect(testRender.find(OperatorSurveyDetailTabBar)).toMatchSnapshot()
    testRender.unmount()
  })
})
 