import React, { useState } from 'react'
import { path, filter } from 'ramda'
import debounce from 'lodash.debounce'
import { useQuery } from 'react-apollo-hooks'
import ExportSurveyModalComponent from '../../components/ExportSurveyModal'
import {
  getAuthenticatedUser,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsSuperAdmin,
  getImpersonateUserId
} from '../../utils/userAuthentication'
import gql from 'graphql-tag'

const ExportSurveyModal = ({ tKey, onCancel, onSubmit }) => {
  const authUser = getAuthenticatedUser()
  // Impersonating USER ID
  const isImpersonating = !!getImpersonateUserId()

  const sendToOthers =
    isUserAuthenticatedAsPowerUser() ||
    isUserAuthenticatedAsSuperAdmin() || isImpersonating

  const [keyword, setKeyword] = useState('')
  const [saving, setSaving] = useState(false)
  const handleKeywordChange = debounce(setKeyword, 250)

  const { data, loading } = useQuery(QUERY_USERS, {
    fetchPolicy: 'cache-and-network',
    skip: !sendToOthers,
    variables: {
      keyword: keyword || ''
    }
  })
  const users = path(['users'])(data) || []
  const filteredUsers = filter(u => u.id !== authUser.id, users)

  const handleSubmit = async values => {
    try {
      setSaving(true)
      await onSubmit(values)
      setSaving(false)
    } catch (ex) {
      setSaving(false)
    }
  }
  
  return (
    <ExportSurveyModalComponent
      tKey={tKey}
      sendToOthers={sendToOthers}
      users={filteredUsers}
      loading={loading}
      saving={saving}
      onSearch={handleKeywordChange}
      onCancel={onCancel}
      onOk={handleSubmit}
      isImpersonating={isImpersonating}
    />
  )
}

const QUERY_USERS = gql`
  query users($keyword: String) {
    users(input: { keyword: $keyword, type: ["operator", "power-user"] }) {
      id
      fullName
      emailAddress
      type
    }
  }
`
export const QUERY_IMPERSONATE_USER = gql`
  query impersonateUser {
    adminUser: me(impersonate: false) {
      id
      fullName
      emailAddress
      isSuperAdmin
    }
  }
`

export default ExportSurveyModal
