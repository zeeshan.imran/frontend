import React, { useEffect, useState } from 'react'
import { withRouter, Route, Redirect } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import path from 'path'
import { isUserAuthenticated } from '../../utils/surveyAuthentication'

const InSurveyRoute = ({ match, history, component: Component, ...rest }) => {
  const [redirectRoute, setRedirectRoute] = useState(null)
  const {
    data: {
      currentSurveyParticipation: {
        currentSurveyStep,
        currentSurveySection
      } = {}
    }
  } = useQuery(GET_NAVIGATION_DATA)
  useEffect(
    () => {
      const baseRoute = `/survey/${match.params.surveyId}`
      if (!isUserAuthenticated()) {
        setRedirectRoute(path.join(baseRoute, '/login'))
        return
      }

      const pathToBe = path.join(
        baseRoute,
        currentSurveySection || '',
        currentSurveyStep || ''
      )

      if (pathToBe !== window.location.pathname) {
        setRedirectRoute(pathToBe)
      } else {
        setRedirectRoute(null)
      }
    },
    [currentSurveyStep, currentSurveySection]
  )

  return (
    <Route
      {...rest}
      render={routeProps => {
        return redirectRoute && redirectRoute !== window.location.pathname ? (
          <Redirect to={redirectRoute} />
        ) : (
            <Component {...routeProps} />
          )
      }}
    />
  )
}

const GET_NAVIGATION_DATA = gql`
  query {
    currentSurveyParticipation @client {
      currentSurveyStep
      currentSurveySection
    }
  }
`

export default withRouter(InSurveyRoute)
