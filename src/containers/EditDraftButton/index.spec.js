import React from 'react'
import { mount } from 'enzyme'
import EditDraftButton from '.'
import { Router } from 'react-router-dom'
import IconButton from '../../components/IconButton'
import wait from '../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('EditDraftButton', () => {
  let testRender
  let surveyId

  beforeEach(() => {
    surveyId = 1
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render EditDraftButton', async () => {
    const history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    testRender = mount(
      <Router history={history}>
        <EditDraftButton surveyId={surveyId} />
      </Router>
    )
    expect(testRender).toMatchSnapshot()

    testRender.find(IconButton).simulate('click')

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('/operator/draft/edit/1')
  })
})
