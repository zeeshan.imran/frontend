import React from 'react'
import IconButton from '../../components/IconButton'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next';

const EditDraftButton = ({ surveyId, history, match }) => {
  const { t } = useTranslation();
  return (
    <IconButton
      tooltip={t('tooltips.editDraftButton')}
      onClick={() => history.push(`/operator/draft/edit/${surveyId}`)}
      type='edit'
    />
  )
}

export default withRouter(EditDraftButton)
