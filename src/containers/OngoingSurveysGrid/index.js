import React from 'react'
import { Query } from 'react-apollo'
import Loader from '../../components/Loader'
import OngoingSurveysGridComponent from '../../components/OngoingSurveysGrid'
import { ONGOING_SURVEYS_QUERY } from '../../queries/OngoingSurveysGrid'

const OngoingSurveysGrid = () => (
  <Query query={ONGOING_SURVEYS_QUERY} fetchPolicy='cache-and-network'>
    {({
      data: { currentUser: { testerInfo: { surveys } = {} } = {} },
      loading
    }) => {
      if (loading) return <Loader />

      return <OngoingSurveysGridComponent surveys={surveys} />
    }}
  </Query>
)

export default OngoingSurveysGrid
