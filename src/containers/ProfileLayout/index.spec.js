import React from 'react'
import { mount } from 'enzyme'
import ProfileLayout from '.'
import { MockedProvider } from 'react-apollo/test-utils'
import { CURRENT_USER_QUERY } from '../../queries/currentUser'
import PersonalDetails from '../../pages/MyProfile/PersonalDetails'
import Person from '../../components/SvgIcons/Person'
import wait from '../../utils/testUtils/waait'
import ProfileLayoutComponent from '../../components/ProfileLayout'

jest.mock('../../components/ProfileLayout')
ProfileLayoutComponent.mockImplementation(
  ({ tabs, renderTab, renderTabContent }) => {
    return (
      <React.Fragment>
        {renderTab(tabs[0])}
        {renderTabContent(tabs[0])}
      </React.Fragment>
    )
  }
)

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mocks = [
  {
    request: {
      query: CURRENT_USER_QUERY
    },
    result: {
      data: {
        currentUser: {
          id: 'user-1',
          firstname: 'Test',
          lastname: 'User',
          email: 'test@gmail.com',
          language: 'en',
          tabs: []
        }
      }
    }
  }
]

describe('ProfileLayout', () => {
  let testRender
  let handleUpdateUser
  let handleUpdateTesterInfo
  let tabs

  beforeEach(() => {
    tabs = [
      {
        icon: <Person />,
        title: 'containers.profileLayout.personalDetails',
        content: PersonalDetails
      }
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render loader', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks}>
        <ProfileLayout tabs={tabs} />
      </MockedProvider>
    )

    await wait(0)
    expect(ProfileLayout).toMatchSnapshot()
  })

  //   test('should render SurveysGridComponent', async () => {
  //     testRender = mount(
  //       <MockedProvider mocks={mocks}>
  //         <ProfileLayout desktop={''} user={user} />
  //       </MockedProvider>
  //     )
  //     await wait(0)
  //     testRender.update()
  //     expect(testRender.find(ProfileLayoutComponent).length).toBe(0)
  //   })
})
