import React from 'react'
import Composer from 'react-composer'
import Person from '../../components/SvgIcons/Person'
import Password from '../../components/SvgIcons/Password'
import Bank from '../../components/SvgIcons/Bank'
import AdditionalInfo from '../../components/SvgIcons/AdditionalInfo'
import PrivacyIcon from '../../components/SvgIcons/Privacy'
import ProfileLayoutComponent from '../../components/ProfileLayout'
import TabHeader from '../../components/TabHeader'

import ChangePassword from '../../pages/MyProfile/ChangePassword'
import PersonalDetails from '../../pages/MyProfile/PersonalDetails'
import AdditionalInformation from '../../pages/MyProfile/AdditionalInformation'
import PaymentDetails from '../../pages/MyProfile/PaymentDetails'
import Privacy from '../../pages/MyProfile/Privacy'
import { Query, Mutation } from 'react-apollo'
import Loader from '../../components/Loader'
import { CURRENT_USER_QUERY } from '../../queries/currentUser'
import { UPDATE_USER_MUTATION } from '../../mutations/updateUser'
import { UPDATE_TESTER_INFO_MUTATION } from '../../mutations/updateTesterInfo'
import { useTranslation } from 'react-i18next'

const ProfileLayout = () => {
  const { t } = useTranslation()
  const MY_PROFILE_TABS = [
    {
      icon: <Person />,
      title: t('containers.profileLayout.personalDetails'),
      content: PersonalDetails
    },
    {
      icon: <AdditionalInfo />,
      title: t('containers.profileLayout.additionalInfo'),
      content: AdditionalInformation
    },
    {
      icon: <Bank />,
      title: t('containers.profileLayout.paymentDetails'),
      content: PaymentDetails
    },
    {
      icon: <Password />,
      title: t('containers.profileLayout.changePassword'),
      content: ChangePassword
    },
    {
      icon: <PrivacyIcon />,
      title: t('containers.profileLayout.privacy'),
      content: Privacy
    }
  ]
  return (
    <Composer
      components={[
        ({ render }) => (
          <Query
            query={CURRENT_USER_QUERY}
            fetchPolicy='cache-and-network'
            children={render}
          />
        ),
        ({ render }) => (
          <Mutation mutation={UPDATE_USER_MUTATION} children={render} />
        ),
        ({ render }) => (
          <Mutation mutation={UPDATE_TESTER_INFO_MUTATION} children={render} />
        )
      ]}
    >
      {([
        {
          data: { currentUser },
          loading
        },
        updateUser,
        updateTesterInfo
      ]) => {
        if (loading) return <Loader />

        return (
          <ProfileLayoutComponent
            tabs={MY_PROFILE_TABS}
            renderTab={tab => {
              const { icon, title } = tab
              return <TabHeader title={title} icon={icon} />
            }}
            renderTabContent={tab => {
              return (
                <tab.content
                  user={currentUser}
                  updateUser={updateUser}
                  updateTesterInfo={updateTesterInfo}
                />
              )
            }}
          />
        )
      }}
    </Composer>
  )
}

export default ProfileLayout
