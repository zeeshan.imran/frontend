import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import Button from '../../components/Button'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useToggle from '../../hooks/useToggle/index'
import usePublishSurvey from '../../hooks/usePublishSurvey'
import { displayErrorPopup } from '../../utils/displayErrorPopup/index'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'
import TooltipWrapper from '../../components/TooltipWrapper'

const PublishEditedSurvey = ({ history, match }) => {
  const { t } = useTranslation()
  const [publishing, togglePublishing] = useToggle(false)
  const {
    data: {
      surveyCreation: { products, basics, questions, mandatoryQuestions }
    }
  } = useQuery(SURVEY_CREATION)

  const deprecateSurvey = useMutation(DEPRECATE_SURVEY)
  const publishSurvey = usePublishSurvey({
    surveyInitialState: 'active',
    predecessorSurvey: match.params.surveyId,
    successMessage: t('containers.publishEditedSurvey.successMessage'),
    errorMessage: t('containers.publishEditedSurvey.errorMessage')
  })
  const surveyValidate = useSurveyValidate()

  const submitDisabled = !basics.isScreenerOnly
    ? !basics.uniqueName ||
      products.length === 0 ||
      questions.length + mandatoryQuestions.length === 0 ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
    : !basics.uniqueName ||
      questions.length + mandatoryQuestions.length === 0 ||
      basics.linkedSurveys.length === 0

  const onPublishSurvey = async () => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails`)
      return
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion,
      noQuestionError
    } = await surveyValidate.validateQuestions(true)

    if (noQuestionError) {
      await surveyValidate.showOnlyMandatoryQuestionErrors()
      return
    }
    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(`./financial?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }
    if (!isQuestionsValid) {
      history.push(`./questions?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    togglePublishing()
    try {
      // DISABLE PREVIOUS SURVEY
      await deprecateSurvey({ variables: { id: match.params.surveyId } })

      await publishSurvey()
      togglePublishing()
      history.push('/operator/surveys')
    } catch (error) {
      togglePublishing()
      displayErrorPopup(error && error.message)
    }
  }

  return (
    <TooltipWrapper
      helperText={t('tooltips.publishSurvey')}
      placement='leftTop'
    >
      <Button
        type={submitDisabled ? 'disabled' : 'primary'}
        size='default'
        onClick={onPublishSurvey}
      >
        {t('containers.publishEditedSurvey.publishButton')}
      </Button>
      <LoadingModal
        visible={publishing}
        text={t('containers.publishEditedSurvey.loadingText')}
      />
    </TooltipWrapper>
  )
}

export const DEPRECATE_SURVEY = gql`
  mutation deprecateSurvey($id: ID!) {
    deprecateSurvey(id: $id)
  }
`

export default withRouter(PublishEditedSurvey)
