import React, { useCallback, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { withTranslation } from 'react-i18next'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import SurveyInstructionsComponent from '../../components/SurveyInstructions'
import FillerLoader from '../../components/FillerLoader'
import { SURVEY_QUERY } from '../../queries/Survey'
import useCustomButton from '../../hooks/useCustomButton'
import useUpdateLocale from '../../hooks/useUpdateSurveyLanguage'
import { destroyDraftWarningPopup, displayDraftWarningPopup } from '../../utils/displayDraftWarningPopup'

const SurveyInstructions = ({ surveyId }) => {
  const { data: surveyData, loading, error } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId }
  })
  useEffect (()=> {
    return destroyDraftWarningPopup
  })
  const handleAdvance = useMutation(ADVANCE_IN_SURVEY)
  const onClickNext = useCallback(async () => {
    await handleAdvance({ variables: { fromStep: 'instructions' } })
  }, [])

  const udpateLocale = useUpdateLocale()
  const { buttonLabel, loading: loadingTranslations } = useCustomButton(
    surveyId,
    'start'
  )

  if (loading || loadingTranslations || error) {
    return <FillerLoader fullScreen loading />
  }

  const {
    survey: { name, instructionsText, instructionSteps, surveyLanguage, state } = {}
  } = surveyData

  udpateLocale(surveyLanguage)

  if (state === 'draft') {
    displayDraftWarningPopup()
  }

  return (
    <AuthenticatedLayout mergeNavbarToContent>
      <SurveyInstructionsComponent
        title={name}
        description={instructionsText}
        instructionSteps={instructionSteps || []}
        onClick={onClickNext}
        buttonLabel={buttonLabel}
      />
    </AuthenticatedLayout>
  )
}

const ADVANCE_IN_SURVEY = gql`
  mutation advanceInSurvey($fromStep: ID!) {
    advanceInSurvey(fromStep: $fromStep) @client
  }
`

export default withRouter(withTranslation()(SurveyInstructions))
