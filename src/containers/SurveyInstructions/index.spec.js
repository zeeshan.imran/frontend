import React from 'react'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { create } from 'react-test-renderer'
import gql from 'graphql-tag'
import { surveyInfo } from '../../fragments/survey'
import SurveyInstructions from './index'
import { Router } from 'react-router-dom'
import history from '../../history'
import '../../utils/internationalization/i18n'
import { displayDraftWarningPopup } from '../../utils/displayDraftWarningPopup'

jest.doMock('./index.js')
jest.mock('../../utils/displayDraftWarningPopup')
 
const mockActiveSurvey = {
  __typename: 'Survey',
  name: 'active survey',
  uniqueName: 'uniqueName',
  state: 'active',
  minimumProducts: 1,
  maximumProducts: 1,
  surveyLanguage: 'en',
  settings: {
    recaptcha: false
  },
  authorizationType: 'public',
  id: `survey-1`,
  surveyEnrollment: 'survey-enrollment-1',
  context: {},
  value: {
    value: 'value-1'
  },
  range: {},
  options: {},
  pairsOptions: {},
  pairs: {},
  productsSkip: {},
  products: [],
  exclusiveTasters: [],
  allowRetakes: false,
  forcedAccount: false,
  forcedAccountLocation: `start`,
  isScreenerOnly: false,
  showGeneratePdf: false,
  linkedSurveys: [],
  referralAmount: 5,
  recaptcha: false,
  maxProductStatCount: 6,
  savedRewards: [],
  emailService: false,
  allowedDaysToFillTheTasting: 5,
  isPaypalSelected: false,
  isGiftCardSelected: false,
  customButtons: {
    __typename: 'CustomButtons',
    continue: 'Continue',
    start: 'Start',
    next: 'Next',
    skip: 'Skip'
  },
  startedAt: Date.now(),
  selectedProduct: 'prod-1',
  instructionSteps: [],
  instructionsText: "",
  thankYouText: "",
  rejectionText: "",
  screeningText: "",
  customizeSharingMessage: '',
  loginText: '',
  pauseText: '',
  screeningQuestions : {},
  setupQuestions : {},
  productsQuestions : {},
  finishingQuestions : {},
  paymentQuestions: {}
  
}

const mockDraftSurvey = {
  ...mockActiveSurvey,
  state: 'draft',
  id: 'survey-2',
  name: 'drafted survey'
}

describe('Test SurveyInstructions', () => {
  let testRender
  window.localStorage = {
    getItem: () => '{}'
  }

  afterEach(() => {
    testRender.unmount()
  })

  test('The active survey should not have a notification popup', () => {
    const mockClient = createApolloMockClient()

    mockClient.cache.writeQuery({
      query: gql`
        query survey($id: ID) {
          survey(id: "survey-1") {
            ...surveyInfo
          }
        }
        ${surveyInfo}
      `,
      data: {
        survey: {
          ...mockActiveSurvey
        }
      }
    })

    testRender = create(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveyInstructions
            match={{ url: `/survey/survey-1/` }}
            surveyId='survey-1'
          />
        </Router>
      </ApolloProvider>
    )

    expect(displayDraftWarningPopup).not.toHaveBeenCalled()
    testRender.unmount()
  })

  // to do luca
  // test('The draft survey should have a proper notification popup', () => {
  //   const mockClient = createApolloMockClient()

  //   mockClient.cache.writeQuery({
  //     query: gql`
  //       query survey($id: ID) {
  //         survey(id: "survey-2") {
  //           ...surveyInfo
  //         }
  //       }
  //       ${surveyInfo}
  //     `,
  //     data: {
  //       survey: {
  //         ...mockDraftSurvey
  //       }
  //     }
  //   })

  //   testRender = create(
  //     <ApolloProvider client={mockClient}>
  //       <Router history={history}>
  //         <SurveyInstructions
  //           match={{ url: `/survey/survey-2/` }}
  //           surveyId='survey-2'
  //         />
  //       </Router>
  //     </ApolloProvider>
  //   )
  //   expect(displayDraftWarningPopup).toHaveBeenCalled()
  //   testRender.unmount()
  // })
})
