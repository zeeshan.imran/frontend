import React, { useCallback } from 'react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { path } from 'ramda'
import OperatorLinkedSurveys from '../../components/OperatorLinkedSurveys'

const linkedSurveysPath = path(['surveyCreation', 'basics', 'linkedSurveys'])

const OperatorSurveyLinkedSurveys = ({ surveyId = '' }) => {
  const updateSurveyBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)
  const { data: surveysData = {} } = useQuery(ALL_SURVEYS, {
    variables: {
      input: {
        limit: 0,
        state: 'active',
        omitScreeners: true,
        authorizationType: 'selected',
        showAllSurveys: false,
        omitUsedTastings: surveyId // passing surveyID if any,
      }
    },
    fetchPolicy: 'cache-and-network'
  })
  const { data } = useQuery(SURVEY_CREATION)

  const linkedSurveys = linkedSurveysPath(data)
  
  const handleSettingsChange = useCallback(
    async linkedSurveys => {
      await updateSurveyBasics({
        variables: {
          basics: { linkedSurveys }
        }
      })
    },
    [updateSurveyBasics]
  )

  return (
    <OperatorLinkedSurveys
      allSurveys={surveysData}
      linkedSurveys={linkedSurveys}
      handleSettingsChange={handleSettingsChange}
    />
  )
}

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        linkedSurveys
      }
    }
  }
`
export const ALL_SURVEYS = gql`
  query surveys($input: SurveysInput!) {
    surveys(input: $input) {
      id
      name
    }
  }
`
export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`
export default OperatorSurveyLinkedSurveys
