import React, { useEffect, useRef, useState } from 'react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import sortBy from 'lodash.sortby'
import indexOf from 'lodash.indexof'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { SURVEY_QUERY } from '../../queries/Survey'
import FillerLoader from '../../components/FillerLoader'
import ChooseProductComponent from '../../components/SurveyChooseProduct'
import useProductsBrands from '../../hooks/useProductsBrands'
import defaults from '../../defaults'

// remove products that are greater than maximum product selection from array for only permutaion and reverse order products
const removeProducts = (allProducts, selectedProducts, maximumProducts) =>
  allProducts.slice(0, maximumProducts).map((product, index) => {
    if (index === selectedProducts.length) {
      return {
        ...product,
        selectionDisable: false
      }
    } else {
      return {
        ...product,
        selectionDisable: true
      }
    }
  })

const productOrderSet = (
  products,
  productOrder,
  displayType,
  selectedProducts,
  maximumProducts
) => {
  if (displayType === 'permutation' || displayType === 'reverse' || displayType === 'forced') {
    return removeProducts(
      sortBy(products, obj => indexOf(productOrder, obj.id)),
      selectedProducts,
      maximumProducts
    )
  } else if (displayType === 'none' && productOrder.length) {
    return sortBy(products, obj => indexOf(productOrder, obj.id))
  } else {
    return products
  }
}

const DEFAULT_PRODUCTS = []

const ChooseProduct = ({
  selectedProduct,
  handleChange,
  country: availableIn,
  productsSkip = []
}) => {
  const [productsList, setProductsList] = useState([])
  const saveSurveyParticipation = useMutation(SAVE_SURVEY_PARTICIPATION)
  const saveRewards = useMutation(SAVE_REWARDS)

  const availableCountry =
    defaults.countries.find(country => {
      return country.name === availableIn
    }) || {}

  const prefix = defaults.currencyPrefixes[availableCountry.currency] || ''
  const suffix = defaults.currencySuffixes[availableCountry.currency] || ''

  const {
    data: {
      currentSurveyParticipation: {
        surveyId,
        selectedProducts,
        savedRewards = [],
        surveyEnrollmentId,
        productDisplayOrder,
        productDisplayType
      }
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const {
    data: {
      survey: { products = DEFAULT_PRODUCTS, maximumProducts } = {}
    } = {},
    loading
  } = useQuery(SURVEY_QUERY, {
    variables: {
      id: surveyId
    },
    fetchPolicy: 'network-only'
  })
  useEffect(() => {
    if (!loading) {
      if (products.length) {
        setProductsList(
          productOrderSet(
            products,
            productDisplayOrder,
            productDisplayType,
            selectedProducts,
            maximumProducts
          )
        )
      } else {
        setProductsList(products)
      }
    }
  }, [
    products,
    productDisplayOrder,
    productDisplayType,
    loading,
    selectedProducts,
    maximumProducts
  ])

  const areRewardsSaved = useRef(false)

  const updateReward = async () => {
    if (!areRewardsSaved.current && productsList.length) {
      areRewardsSaved.current = true
      if (savedRewards.length === 0) {
        let rewards = productsList
          .filter(product => product.isAvailable)
          .map(product => ({
            id: product.id,
            reward: product.reward,
            answered: false
          }))
        await saveSurveyParticipation({
          variables: {
            savedRewards: rewards
          }
        })
        await saveRewards({
          variables: {
            rewards: rewards,
            surveyEnrollment: surveyEnrollmentId
          }
        })
      }
    }
  }
  useEffect(() => {
    updateReward()
  })

  const availableProducts = productsList.filter(product =>
    savedRewards.find(reward => reward.id === product.id)
  )
  const { data: brandsData, loading: loadingBrands } = useProductsBrands(
    availableProducts
  )

  const onSelectProduct = async selected => {
    await saveSurveyParticipation({
      variables: {
        selectedProduct: selected
      }
    })
    handleChange(selected)
  }

  if (loading || loadingBrands) return <FillerLoader loading />
  const { brands } = brandsData

  return (
    availableProducts.length && (
      <ChooseProductComponent
        brands={brands}
        takenProducts={selectedProducts}
        products={availableProducts}
        selectedProduct={selectedProduct}
        onChange={onSelectProduct}
        productsSkip={productsSkip}
        savedRewards={savedRewards}
        prefix={prefix}
        suffix={suffix}
      />
    )
  )
}

const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation(
    $selectedProduct: String
    $savedRewards: JSON
  ) {
    saveCurrentSurveyParticipation(
      selectedProduct: $selectedProduct
      savedRewards: $savedRewards
    ) @client
  }
`

const SAVE_REWARDS = gql`
  mutation saveRewards($rewards: JSON, $surveyEnrollment: ID) {
    saveRewards(rewards: $rewards, surveyEnrollment: $surveyEnrollment)
  }
`

export default ChooseProduct
