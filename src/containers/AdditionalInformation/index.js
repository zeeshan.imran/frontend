import React, { Component } from 'react'
import Composer from 'react-composer'
import { Desktop } from '../../components/Responsive'
import { Query } from 'react-apollo'
import AdditionalInformationForm from '../../components/AdditionalInformationForm'
import ETHNICITIES_QUERY from '../../queries/ethnicities'
import { isNil } from 'ramda'
import SMOKER_KINDS_QUERY from '../../queries/smokerKinds'
import PRODUCT_CATEGORIES_QUERY from '../../queries/productCategories'
import DIETARY_PREFERENCES_QUERY from '../../queries/dietaryPreferences'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { withTranslation } from 'react-i18next';

class AdditionalInformation extends Component {
  handleSubmit = (e, form) => {
    const {
      user: { id },
      updateTesterInfo,
      t
    } = this.props
    e.preventDefault()
    form.validateFields(async (err, values) => {
      if (!err) {
        const hasChildrenAges =
          !!form.isFieldTouched('hasChildren') && values.hasChildren === 'Yes'

        const formatChildrenAges = values => {
          let childrenAgesArray = []
          const numberOfChildren = values.numberOfChildren
          for (let index = 0; index < numberOfChildren; index++) {
            childrenAgesArray.push(values[`childrenAge-${index}`])
          }
          return childrenAgesArray.join(', ')
        }

        const updateTesterInfoData = {
          id,
          ...(!!form.isFieldTouched('ethnicity') && {
            ethnicityId: values.ethnicity
          }),
          ...(!!form.isFieldTouched('incomeRange') && {
            incomeRangeId: values.incomeRange
          }),
          ...(!!form.isFieldTouched('smokerKind') && {
            smokerKindId: values.smokerKind
          }),
          ...(!!form.isFieldTouched('productTypes') && {
            preferredProductCategoriesIds: values.productTypes
          }),
          ...(!!form.isFieldTouched('dietaryRestrictions') && {
            dietaryPreferenceId: values.dietaryRestrictions
          }),
          ...(hasChildrenAges && { childrenAges: formatChildrenAges(values) })
        }

        const shouldUpdateTesterInfo =
          Object.keys(updateTesterInfoData).length > 1

        if (shouldUpdateTesterInfo) {
          try {
            const {
              data: {
                updateTesterInfo: {
                  ok: updateTesterInfoOk,
                  error: updateTesterInfoError
                }
              }
            } = await updateTesterInfo({ variables: updateTesterInfoData })
            if (!updateTesterInfoOk) {
              return displayErrorPopup(
                `Error while updating: ${updateTesterInfoError}`
              )
            } else {
              this.setState({ imageHash: Date.now() }) // This is used to fix the image not reloading after a user uploads a picture
            }
          } catch (error) {
            return displayErrorPopup(t('containers.additionalInfo.error'))
          }
        }
      }
    })

    return displaySuccessMessage(t('containers.additionalInfo.success'))
  }

  render () {
    const {
      user: {
        testerInfo: {
          ethnicityId,
          incomeRangeId,
          incomeRange,
          childrenAges,
          smokerKindId,
          preferredProductCategories,
          dietaryPreferenceId
        } = {}
      } = {}
    } = this.props
    return (
      <Composer
        components={[
          ({ render }) => <Desktop children={render} />,
          ({ render }) => (
            <Query
              query={ETHNICITIES_QUERY}
              fetchPolicy='cache-and-network'
              children={render}
            />
          ),
          ({ render }) => (
            <Query
              query={SMOKER_KINDS_QUERY}
              fetchPolicy='cache-and-network'
              children={render}
            />
          ),
          ({ render }) => (
            <Query
              query={PRODUCT_CATEGORIES_QUERY}
              fetchPolicy='cache-and-network'
              children={render}
            />
          ),
          ({ render }) => (
            <Query
              query={DIETARY_PREFERENCES_QUERY}
              fetchPolicy='cache-and-network'
              children={render}
            />
          )
        ]}
      >
        {([
          desktop,
          {
            data: { ethnicities },
            loading: loadingEthnicities
          },
          {
            data: { smokerKinds },
            loading: loadingSmokerKind
          },
          {
            data: { productCategories },
            loading: loadingProductCategories
          },
          {
            data: { dietaryPreferences },
            loading: loadingDietaryPreferences
          }
        ]) => {
          return (
            <AdditionalInformationForm
              desktop={desktop}
              submitForm={this.handleSubmit}
              ethnicities={ethnicities}
              ethnicity={ethnicityId}
              incomeRange={incomeRangeId}
              currency={incomeRange && incomeRange.currency}
              hasChildren={isNil(childrenAges) ? 'No' : 'Yes'}
              childrenAges={childrenAges && childrenAges.split(',')}
              smokerKinds={smokerKinds}
              smokerKind={smokerKindId}
              productCategories={productCategories}
              preferredProductCategories={preferredProductCategories.map(
                productCategory => productCategory.categoryId
              )}
              dietaryPreferences={dietaryPreferences}
              dietaryPreference={dietaryPreferenceId}
            />
          )
        }}
      </Composer>
    )
  }
}

export default withTranslation()(AdditionalInformation)
