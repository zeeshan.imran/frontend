import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import sinon from 'sinon'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import wait from '../../utils/testUtils/waait'

import LoadSurvey, {
  GET_SURVEY,
  RESET_CREATION_FORM,
  LOAD_SURVEY_BASICS_TO_FORM,
  LOAD_SURVEY_PRODUCTS_TO_FORM,
  LOAD_SURVEY_QUESTIONS_TO_FORM,
  REQUIRE_CHOOSE_PRODUCT,
  UNREQUIRE_CHOOSE_PRODUCT
} from '.'
import { isUserAuthenticatedAsOperator } from '../../utils/userAuthentication'

jest.mock('../../utils/userAuthentication')
jest.mock('../../hooks/useAllQuestions', () => () => {
  return {
    data: {
      questions: [{}, {}]
    }
  }
})

let mockSurvey = {
  id: 'survey-1',
  name: '',
  uniqueName: '',
  state: '',
  minimumProducts: '',
  maximumProducts: '',
  surveyLanguage: '',
  exclusiveTasters: [],
  allowRetakes: false,
  forcedAccount: false,
  forcedAccountLocation: `start`,
  isScreenerOnly: false,
  showGeneratePdf: false,
  linkedSurveys: [],
  referralAmount: 5,
  maxProductStatCount: 6,
  settings: {},
  customButtons: {
    continue: '',
    start: '',
    next: '',
    skip: ''
  },
  instructionsText: '',
  thankYouText: '',
  rejectionText: '',
  saveRewards: [],
  screeningQuestions: [],
  setupQuestions: [],
  productsQuestions: [],
  finishingQuestions: [],
  authorizationType: '',
  customizeSharingMessage: '',
  loginText: '',
  pauseText: '',
  allowedDaysToFillTheTasting: 5,
  isPaypalSelected: false,
  isGiftCardSelected: false
}

describe('LoadSurvey', () => {
  let testRender
  let client
  let history = {
    push: jest.fn(),
    replace: jest.fn(),
    listen: () => {
      return jest.fn()
    },
    location: { pathname: '' }
  }

  beforeEach(() => {
    isUserAuthenticatedAsOperator.mockImplementation(() => true)
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render', async () => {
    client = createApolloMockClient()
    const stub = sinon.stub(client, 'mutate')

    client.cache.writeQuery({
      query: GET_SURVEY,
      variables: { id: 'survey-1' },
      data: {
        survey: {
          ...mockSurvey,
          hasMultipleTastings: false,
          products: []
        }
      }
    })

    stub
      .withArgs(sinon.match({ mutation: RESET_CREATION_FORM }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: LOAD_SURVEY_BASICS_TO_FORM }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: LOAD_SURVEY_PRODUCTS_TO_FORM }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: LOAD_SURVEY_QUESTIONS_TO_FORM }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: REQUIRE_CHOOSE_PRODUCT }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: UNREQUIRE_CHOOSE_PRODUCT }))
      .returns({ data: {} })

    testRender = mount(
      <ApolloProvider client={client} addTypename={false}>
        <Router history={history}>
          <LoadSurvey
            location={{ pathname: '/operator/survey/edit/survey-1' }}
            match={{ params: { surveyId: 'survey-1' } }}
          />
        </Router>
      </ApolloProvider>
    )

    await wait(0)
    expect(testRender).toMatchSnapshot()
  })

  test('survey has Multiple Tastings', async () => {
    client = createApolloMockClient()

    client.cache.writeQuery({
      query: GET_SURVEY,
      variables: { id: 'survey-1' },
      data: {
        survey: {
          ...mockSurvey,
          hasMultipleTastings: true,
          products: [{}, {}]
        }
      }
    })

    const stub = sinon.stub(client, 'mutate')

    stub
      .withArgs(sinon.match({ mutation: RESET_CREATION_FORM }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: LOAD_SURVEY_BASICS_TO_FORM }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: LOAD_SURVEY_PRODUCTS_TO_FORM }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: LOAD_SURVEY_QUESTIONS_TO_FORM }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: REQUIRE_CHOOSE_PRODUCT }))
      .returns({ data: {} })
    stub
      .withArgs(sinon.match({ mutation: UNREQUIRE_CHOOSE_PRODUCT }))
      .returns({ data: {} })

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <LoadSurvey
            location={{ pathname: '/operator/draft/edit/survey-1' }}
            cation
            match={{ params: { surveyId: 'survey-1' } }}
          />
        </Router>
      </ApolloProvider>
    )

    await wait(0)
    expect(testRender).toMatchSnapshot()
  })
})
