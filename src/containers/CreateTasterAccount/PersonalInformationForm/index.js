import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import * as Yup from 'yup'
import PersonalInformationFormComponent from '../../../components/CreateTasterAccount/PersonalInformationFormComponent'

const PersonalInformationForm = ({ setForm, stepNumber, initialValues, initialValid }) => {
  const { t } = useTranslation()
  const [loading] = useState(false)
  const validationSchema = Yup.object().shape({
    firstName: Yup.string().trim()
      .required(t(`components.createTasterAccount.validations.second.firstName.required`)),
    lastName: Yup.string().trim()
      .required(t(`components.createTasterAccount.validations.second.lastName.required`)),
    dateofbirth: Yup.string().nullable()
      .required(t(`components.createTasterAccount.validations.second.dateofbirth.required`)),
    gender: Yup.string()
      .required(t(`components.createTasterAccount.validations.second.gender.required`)),
    language: Yup.string()
      .required(t(`components.createTasterAccount.validations.second.language.required`)),
    country: Yup.string()
      .required(t(`components.createTasterAccount.validations.second.country.required`)),
    state: Yup.string()
      .required(t(`components.createTasterAccount.validations.second.state.required`))
  })

  return (
    <Formik
      validationSchema={validationSchema}
      isInitialValid={initialValid}
      initialValues={initialValues}
      render={({
        values,
        errors,
        touched,
        isValid,
        handleSubmit,
        handleBlur,
        handleChange,
        setFieldValue,
        setFieldTouched
      }) => {
        setForm(isValid, values, stepNumber);
        return (
          <PersonalInformationFormComponent
            {...values}
            loading={loading}
            errors={errors}
            touched={touched}
            setFieldValue={setFieldValue}
            handleChange={handleChange}
            dropdownChangeCountryHandler={field => v => {
              setFieldValue(field, v)
              setFieldTouched(field, true)
              setFieldValue('state', null)
              setFieldTouched('state', true)
            }}
            dropdownChangeHandler={field => v => {
              setFieldValue(field, v)
              setFieldTouched(field, true)
            }}
            getDatePickerChangeHandler={field => v => {
              setFieldValue(field, v)
              setFieldTouched(field, true)
            }}
            handleBlur={handleBlur}
            handleSubmit={handleSubmit}
            canSubmit={isValid}
          /> 
        )
      }}
    />
  )
}
export default withRouter(PersonalInformationForm)
