import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import * as Yup from 'yup'
import WelcomeFormComponent from '../../../components/CreateTasterAccount/WelcomeFormComponent'

const WelcomeForm = ({ setForm, stepNumber, initialValues, initialValid }) => {
  const { t } = useTranslation()
  const [loading] = useState(false)
  const validationSchema = Yup.object().shape({
    emailAddress: Yup.string()
      .email(t(`components.createTasterAccount.validations.first.email.valid`))
      .required(t(`components.createTasterAccount.validations.first.email.required`)),
    password: Yup.string()
    .min(8, t(`components.createTasterAccount.validations.first.password.min`))
    .required(t(`components.createTasterAccount.validations.first.password.required`)),
    confirmPassword: Yup.string()
    .oneOf([Yup.ref('password')], t(`components.createTasterAccount.validations.first.confirm.oneOf`))
    .required(t(`components.createTasterAccount.validations.first.confirm.required`))
  })

  return (
    <Formik
      validationSchema={validationSchema}
      isInitialValid={initialValid}
      initialValues={initialValues}
      render={({
        values,
        errors,
        touched,
        isValid,
        setFieldValue,
        handleSubmit,
        handleBlur,
        handleChange
      }) => {
        setForm(isValid, values, stepNumber);
        return (
          <WelcomeFormComponent
            {...values}
            loading={loading}
            errors={errors}
            touched={touched}
            setFieldValue={setFieldValue}
            handleChange={handleChange}
            handleBlur={handleBlur}
            handleSubmit={handleSubmit}
            canSubmit={isValid}
          />
        )
      }}
    />
  )
}
export default withRouter(WelcomeForm)
