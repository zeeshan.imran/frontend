import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import * as Yup from 'yup'
import AdditionalInformationFormComponent from '../../../components/CreateTasterAccount/AdditionalInformationFormComponent'

const AdditionalInformationForm = ({
  setForm,
  stepNumber,
  initialValues,
  initialValid,
  completeForm
}) => {
  const [loading] = useState(false)
  const { t } = useTranslation()
  const validationSchema = Yup.object().shape({
    ethnicity: Yup.string().required(),
    incomeRange: Yup.string().notRequired(),
    numberOfChildren: Yup.string().notRequired(),
    childrenAges: Yup.string().when('numberOfChildren', {
      is: numberOfChildren => numberOfChildren > 0,
      then: Yup.string()
        .notRequired(
          t('components.createTasterAccount.validations.third.childrenAges.required')
        )
        .test(
          'is-json',
          t('components.createTasterAccount.validations.third.childrenAges.required'),
          value => {
            if (!value) return false
            let fullValue = JSON.parse(value)
            let allValue = Object.values(fullValue)
            if (allValue.includes('')) return false
            return true
          }
        ),
      otherwise: Yup.string().notRequired()
    }),
    smokerKind: Yup.string().required(),
    foodAllergies: Yup.string().required(),
    marketResearchParticipation: Yup.string().required(),
    paypalEmailAddress: Yup.string()
      .email(t('components.createTasterAccount.validations.third.paypal.valid'))
      .required(t('components.createTasterAccount.validations.third.paypal.required'))
  })

  return (
    <Formik
      validationSchema={validationSchema}
      isInitialValid={initialValid}
      initialValues={initialValues}
      render={({
        values,
        errors,
        touched,
        isValid,
        handleSubmit,
        handleBlur,
        handleChange,
        setFieldValue,
        setFieldTouched
      }) => {
        setForm(isValid, values, stepNumber)
        return (
          <AdditionalInformationFormComponent
            {...values}
            loading={loading}
            errors={errors}
            touched={touched}
            setFieldValue={setFieldValue}
            handleChange={handleChange}
            /*
            handleNumberOfChildrenChange={field => e => {
              let currentValue = e.target.value
              if (parseInt(currentValue, 10) > 10) {
                currentValue = 10
              }
              setFieldValue(field, currentValue)

              let childrenAges = values['childrenAges']
                ? JSON.parse(values['childrenAges'])
                : {}
              let newChange = {}
              for (let count = 0; count < currentValue; count++) {
                newChange[count] = childrenAges[count] ? childrenAges[count] : ''
              }
              setFieldValue('childrenAges', JSON.stringify(newChange))
            }}
            onChangeAges={index => e => {
              let currentValue = e
              let currentFieldValue = values['childrenAges']
                ? JSON.parse(values['childrenAges'])
                : {}

              if (currentValue > 100) {
                currentValue = 100
              }
              currentFieldValue[index - 1] = currentValue
              setFieldValue('childrenAges', JSON.stringify(currentFieldValue))
            }}
            */
            dropdownChangeHandler={field => v => {
              setFieldValue(field, v)
              setFieldTouched(field, true)
            }}
            getDatePickerChangeHandler={field => v => {
              setFieldValue(field, v)
              setFieldTouched(field, true)
            }}
            handleBlur={handleBlur}
            handleSubmit={handleSubmit}
            canSubmit={isValid}
            completeForm={completeForm}
          />
        )
      }}
    />
  )
}
export default withRouter(AdditionalInformationForm)
