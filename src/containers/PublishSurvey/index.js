import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import Button from '../../components/Button'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import useToggle from '../../hooks/useToggle/index'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import usePublishSurvey from '../../hooks/usePublishSurvey'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'
import TooltipWrapper from '../../components/TooltipWrapper'

const PublishSurvey = ({ history }) => {
  const { t } = useTranslation()
  const {
    data: {
      surveyCreation: { products, basics, questions, mandatoryQuestions }
    }
  } = useQuery(SURVEY_CREATION)
  const [publishing, togglePublishing] = useToggle(false)

  const publishSurvey = usePublishSurvey({
    surveyInitialState: 'active',
    successMessage: t('containers.publishSurvey.successMessage'),
    errorMessage: t('containers.publishSurvey.errorMessage')
  })
  const surveyValidate = useSurveyValidate()

  const submitDisabled = !basics.isScreenerOnly
    ? !basics.uniqueName ||
      products.length === 0 ||
      questions.length + mandatoryQuestions.length === 0 ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
    : !basics.uniqueName ||
      questions.length + mandatoryQuestions.length === 0 ||
      basics.linkedSurveys.length === 0

  const onPublishSurvey = async () => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails`)
      return
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion,
      noQuestionError
    } = await surveyValidate.validateQuestions(true)

    if (noQuestionError) {
      await surveyValidate.showOnlyMandatoryQuestionErrors()
      return
    }

    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(`./financial?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    if (!isQuestionsValid) {
      history.push(`./questions?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    togglePublishing()
    try {
      await publishSurvey()
      togglePublishing()
      history.push('/operator/surveys')
    } catch (error) {
      togglePublishing()
      displayErrorPopup(error && error.message)
    }
  }

  return (
    <TooltipWrapper
      helperText={t('tooltips.publishSurvey')}
      placement='leftTop'
    >
      <Button
        data-testid='publish-survey-button'
        type={submitDisabled ? 'disabled' : 'primary'}
        size='default'
        onClick={onPublishSurvey}
      >
        {t('containers.publishSurvey.publishButton')}
      </Button>
      <LoadingModal
        visible={publishing}
        text={t('containers.publishSurvey.loadingText')}
      />
    </TooltipWrapper>
  )
}

export default withRouter(PublishSurvey)
