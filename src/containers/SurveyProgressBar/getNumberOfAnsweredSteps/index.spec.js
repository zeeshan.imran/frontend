import getNumberOfAnsweredSteps from './index'

test('should return the step index in screening', () => {
  const n = getNumberOfAnsweredSteps({
    step: 'a',
    section: 'screening',
    nTimesSelectedAProduct: 123,
    survey: {
      screeningQuestions: ['c', 'b', 'a'].map(v => ({ id: v }))
    }
  })

  expect(n).toEqual(2)
})

test('should return the normal step index if there is no tasting loop', () => {
  const n = getNumberOfAnsweredSteps({
    step: 'a',
    section: 'not-screening',
    nTimesSelectedAProduct: 123,
    survey: {
      setupQuestions: ['other-a', 'other-b'].map(v => ({ id: v })),
      productsQuestions: ['c', 'b', 'a'].map(v => ({ id: v })),
      finishingQuestions: [],
      minimumProducts: 1,
      maximumProducts: 1
    }
  })

  expect(n).toEqual(4)
})

test("should return the normal step index if didn't start loop yet", () => {
  const n = getNumberOfAnsweredSteps({
    step: 'a',
    section: 'not-screening',
    nTimesSelectedAProduct: 2,
    survey: {
      setupQuestions: ['other-b', 'a'].map(v => ({ id: v })),
      productsQuestions: ['c', 'b', 'other-a'].map(v => ({ id: v })),
      finishingQuestions: []
    }
  })

  expect(n).toEqual(1)
})

test('should consider that no steps were answerd', () => {
  const answeredSteps = getNumberOfAnsweredSteps({
    step: 'c',
    section: 'not-screening',
    nTimesSelectedAProduct: 0,
    survey: {
      setupQuestions: [],
      productsQuestions: ['c', 'b', 'other-a'].map(v => ({ id: v })),
      finishingQuestions: []
    }
  })

  expect(answeredSteps).toEqual(0)
})

test('should consider the number of products the user tasted before', () => {
  const answeredSteps = getNumberOfAnsweredSteps({
    step: 'b',
    section: 'not-screening',
    nTimesSelectedAProduct: 3,
    survey: {
      setupQuestions: [],
      productsQuestions: ['c', 'b', 'other-a'].map(v => ({ id: v })),
      finishingQuestions: []
    }
  })

  expect(answeredSteps).toEqual(7)
})

test("should compensate for the first question of the loop, since the user didn't select a product for the loop yet", () => {
  const answeredSteps = getNumberOfAnsweredSteps({
    step: 'choose product',
    section: 'not-screening',
    nTimesSelectedAProduct: 2,
    survey: {
      setupQuestions: [],
      productsQuestions: ['choose product', 'b', 'other-a'].map(v => ({
        id: v
      })),
      finishingQuestions: []
    }
  })

  expect(answeredSteps).toEqual(6)
})

test('should consider answers to previous sections, besides the loop', () => {
  const answeredSteps = getNumberOfAnsweredSteps({
    step: 'choose product',
    section: 'not-screening',
    nTimesSelectedAProduct: 2,
    survey: {
      screeningQuestions: ['sa'],
      setupQuestions: ['aa', 'bb'],
      productsQuestions: ['choose product', 'b', 'other-a'].map(v => ({
        id: v
      })),
      finishingQuestions: []
    }
  })

  expect(answeredSteps).toEqual(8)
})

test('should consider all previously done loops when on a more advanced step', () => {
  const answeredSteps = getNumberOfAnsweredSteps({
    step: 'finishing',
    section: 'not-screening',
    nTimesSelectedAProduct: 3,
    survey: {
      screeningQuestions: ['sa'],
      setupQuestions: ['aa', 'bb'],
      productsQuestions: ['choose product', 'b', 'other-a'].map(v => ({
        id: v
      })),
      finishingQuestions: ['finishing'].map(v => ({ id: v }))
    }
  })

  expect(answeredSteps).toEqual(11)
})
