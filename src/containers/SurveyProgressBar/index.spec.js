import React from 'react'
import wait from '../../utils/testUtils/waait'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { mount } from 'enzyme'
import SurveyProgressBar, { CURRENT_SURVEY_QUERY, SURVEY_QUERY } from './index'
import '../../utils/internationalization/i18n'
import { act } from 'react-dom/test-utils'
import getTotalNumberOfSteps from './getTotalNumberOfSteps'
import getNumberOfAnsweredSteps from './getNumberOfAnsweredSteps'
import defaults from '../../defaults'

jest.mock('./getTotalNumberOfSteps')
jest.mock('./getNumberOfAnsweredSteps')

const SURVEY_ID = 'survey-1'

const surveyMock = {
  id: SURVEY_ID,
  screeningQuestions: [],
  setupQuestions: [],
  productsQuestions: [],
  finishingQuestions: []
}

const mockSurveyQuery = {
  request: {
    query: SURVEY_QUERY,
    variables: { id: SURVEY_ID }
  },
  result: {
    data: {
      survey: surveyMock
    }
  }
}

describe('SurveyProgressBar component', () => {
  let testRender
  let mockClient

  afterEach(() => {
    testRender.unmount()
  })

  beforeAll(() => {})

  test('Should call percentage calculations', async () => {
    mockClient = createApolloMockClient({ mocks: [mockSurveyQuery] })
    mockClient.cache.writeQuery({
      query: CURRENT_SURVEY_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: SURVEY_ID,
          currentSurveyStep: 1,
          currentSurveySection: 1,
          selectedProducts: ['product-1']
        }
      }
    })
    act(() => {
      testRender = mount(
        <ApolloProvider client={mockClient}>
          <SurveyProgressBar />
        </ApolloProvider>
      )
    })
    await wait(0)
    testRender.update()
    await wait(0)

    expect(getTotalNumberOfSteps).toBeCalledWith({
      nTimesSelectedAProduct: 1,
      section: 1
    })
    expect(getNumberOfAnsweredSteps).toBeCalledWith({
      nTimesSelectedAProduct: 1,
      section: 1,
      step: 1
    })
  })
})
