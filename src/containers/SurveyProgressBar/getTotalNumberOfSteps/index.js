const getTotalNumberOfSteps = ({
  section,
  nTimesSelectedAProduct,
  survey: {
    screeningQuestions = [],
    setupQuestions = [],
    productsQuestions = [],
    finishingQuestions = [],
    minimumProducts,
    maximumProducts
  } = {}
}) => {
  if (section === 'screening') {
    return screeningQuestions.length
  }

  const nFixedQuestions = setupQuestions.length + finishingQuestions.length
  const nLoops = Math.max(minimumProducts, nTimesSelectedAProduct)
  const nTastingQuestions = nLoops * productsQuestions.length

  return nFixedQuestions + nTastingQuestions
}

export default getTotalNumberOfSteps
