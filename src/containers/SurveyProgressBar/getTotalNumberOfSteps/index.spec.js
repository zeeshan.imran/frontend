import getTotalNumberOfSteps from './index'

test('should return the sum of steps when there is no tasting loop', () => {
  const n = getTotalNumberOfSteps({
    section: 'not-screening',
    nTimesSelectedAProduct: 1234,
    survey: {
      screeningQuestions: ['hum'],
      setupQuestions: ['a', 'b', 'c'],
      productsQuestions: ['d', 'e', 'f'],
      finishingQuestions: ['g'],
      minimumProducts: 1,
      maximumProducts: 1
    }
  })

  expect(n).toEqual(3706)
})

test('should only consider screening questions for total', () => {
  const n = getTotalNumberOfSteps({
    section: 'screening',
    nTimesSelectedAProduct: 1234,
    survey: {
      screeningQuestions: ['1', '2'],
      setupQuestions: ['a', 'b', 'c'],
      productsQuestions: ['d', 'e', 'f'],
      finishingQuestions: ['g'],
      minimumProducts: 1
    }
  })

  expect(n).toEqual(2)
})

test('should return the sum of steps counting a minimum number of loops', () => {
  const n = getTotalNumberOfSteps({
    section: 'not-screening',
    nTimesSelectedAProduct: 0,
    survey: {
      screeningQuestions: ['hum'],
      setupQuestions: ['a', 'b', 'c'],
      productsQuestions: ['d', 'e', 'f'],
      finishingQuestions: ['g'],
      minimumProducts: 2
    }
  })

  expect(n).toEqual(10)
})

test('should return the sum of steps plus the steps from the number of loops the user will eventually do', () => {
  const n = getTotalNumberOfSteps({
    section: 'not-screening',
    nTimesSelectedAProduct: 3,
    survey: {
      screeningQuestions: ['hum'],
      setupQuestions: ['a', 'b', 'c'],
      productsQuestions: ['d', 'e', 'f'], // 3 times exectued
      finishingQuestions: ['g'],
      minimumProducts: 2
    }
  })

  expect(n).toEqual(13)
})
