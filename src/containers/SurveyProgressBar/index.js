import React, { useState, useEffect } from 'react'
import gql from 'graphql-tag'
import { surveyInfo } from '../../fragments/survey'
import { useQuery } from 'react-apollo-hooks'
import getTotalNumberOfSteps from './getTotalNumberOfSteps'
import getNumberOfAnsweredSteps from './getNumberOfAnsweredSteps'
import { withTranslation } from 'react-i18next'
import ProgressBar from '../../components/ProgressBar'
import colors from '../../utils/Colors'

const SurveyProgressBar = ({ t }) => {
  const {
    data: {
      currentSurveyParticipation: {
        surveyId,
        currentSurveyStep,
        currentSurveySection,
        selectedProducts = []
      } = {}
    }
  } = useQuery(CURRENT_SURVEY_QUERY)

  const {
    data: { survey }
  } = useQuery(SURVEY_QUERY, { variables: { id: surveyId } })

  const [numberOfAnsweredSteps, setNumberOfAnsweredSteps] = useState(0)
  useEffect(() => {
    setNumberOfAnsweredSteps(
      getNumberOfAnsweredSteps({
        step: currentSurveyStep,
        section: currentSurveySection,
        nTimesSelectedAProduct: selectedProducts.length,
        survey
      })
    )
  }, [currentSurveyStep, currentSurveySection, survey])

  const totalNumberOfSteps = getTotalNumberOfSteps({
    section: currentSurveySection,
    nTimesSelectedAProduct: selectedProducts ? selectedProducts.length : 0,
    survey
  })

  return (
    <ProgressBar
      strokeColor={colors.PROGRESS_BAR_COLOR}
      percentage={(numberOfAnsweredSteps / totalNumberOfSteps) * 100}
      showPercentage={false}
    />
  )
}

export const CURRENT_SURVEY_QUERY = gql`
  query {
    currentSurveyParticipation @client {
      surveyId
      currentSurveyStep
      currentSurveySection
      selectedProducts
    }
  }
`

export const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyInfo
    }
  }

  ${surveyInfo}
`

export default withTranslation()(SurveyProgressBar)
