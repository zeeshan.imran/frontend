import React from 'react'
import { withRouter } from 'react-router-dom'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import Text from '../../components/Text'
import AlertModal from '../../components/AlertModal'
import { useTranslation } from 'react-i18next'
import { message } from 'antd'

const CloneSurveyButton = ({
  state,
  surveyId,
  onMenuItemClick
}) => {
  const { t } = useTranslation()
  const cloneSurvey = useMutation(CLONE_SURVEY, {
    variables: { id: surveyId },
    refetchQueries: ['surveys', 'surveysTotal']
  })

  const onClick = () => {
    onMenuItemClick()
    AlertModal({
      title:
        state === 'draft'
          ? t('containers.cloneSurveyButton.alertModalTitleYes')
          : t('containers.cloneSurveyButton.alertModalTitleNo'),
      description:
        state === 'draft'
          ? t('containers.cloneSurveyButton.alertModalDescriptionYes')
          : t('containers.cloneSurveyButton.alertModalDescriptionNo'),
      okText: t('containers.cloneSurveyButton.alertModalOkText'),
      handleOk: async () => {
        const response = await cloneSurvey()
        if (response && response.data && response.data.cloneSurvey) {
          message.success(t(`containers.cloneSurveyButton.success`))
        } else {
          message.error(t(`containers.cloneSurveyButton.error`))
        }
      },
      handleCancel: () => {}
    })
  }
  return (
    <Text onClick={onClick}>
      {' '}
      {t('tooltips.cloneSurveyButton')}{' '}
    </Text>
  )
}

const CLONE_SURVEY = gql`
  mutation cloneSurvey($id: ID!) {
    cloneSurvey(id: $id)
  }
`

export default withRouter(CloneSurveyButton)
