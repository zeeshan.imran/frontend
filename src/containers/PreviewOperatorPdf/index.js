import React, { useEffect, useState } from 'react'
import Page from './Page'
import * as pdfUtils from '../../utils/pdfUtils'
import useChartResults from '../../hooks/useChartResults'
import { getColWidth, getRowHeight, PAPER_SIZES } from '../../utils/paperSizes'
import useRenderingState from '../../hooks/useRenderingState'
import { FooterNote, OperatorPdfContainer } from './styles'

const DEFAULT =
  localStorage.getItem('pdf-layout') &&
  JSON.parse(localStorage.getItem('pdf-layout'))

const DEFAULT_PAPER = PAPER_SIZES[(DEFAULT && DEFAULT.paper) || 'A4']

const FOOTER_NOTE = localStorage.getItem('footer-note')

const PreviewOperatorPdf = ({
  paperSize = DEFAULT_PAPER,
  savedLayout = DEFAULT,
  jobGroupId,
  surveyId
}) => {
  const { loading, stats, settings } = useChartResults({ jobGroupId, surveyId })
  const { rendering, showRendering } = useRenderingState()
  const zoom = paperSize.printZoom
  const colWidth = getColWidth(paperSize) * zoom
  const rowHeight = getRowHeight(paperSize) * zoom
  const [pages, setPages] = useState([])

  useEffect(() => {
    if (!loading) {
      const elementItems = pdfUtils.createElementItems(
        stats,
        settings,
        'operator'
      )

      const pages = savedLayout
        ? savedLayout.pages
        : pdfUtils.calculateElementsLayout(paperSize, elementItems)

      const pagesWithContent = pages.map(page =>
        page.map(gridItem => ({
          ...gridItem,
          ...elementItems.find(e => e.i === gridItem.i)
        }))
      )

      setPages(pagesWithContent)
      showRendering(1000)
    }
  }, [loading, settings, stats, paperSize, savedLayout, showRendering])

  window.__PAGE_HEIGHT = Math.ceil(rowHeight * paperSize.rows)
  return (
    <OperatorPdfContainer>
      {rendering && <div id='loading'>loading</div>}
      {pages.map((page, index) => (
        <Page
          key={`page-${index}`}
          paperSize={paperSize}
          colWidth={colWidth}
          rowHeight={rowHeight}
          zoom={zoom}
          layout={page}
        />
      ))}

      {FOOTER_NOTE && (
        <FooterNote dangerouslySetInnerHTML={{ __html: FOOTER_NOTE }} />
      )}
    </OperatorPdfContainer>
  )
}

export default PreviewOperatorPdf
