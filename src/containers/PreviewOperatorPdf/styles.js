import styled from 'styled-components'

export const OperatorPdfContainer = styled.div`
  position: relative;
`

export const FooterNote = styled.div`
  position: absolute;
  bottom: 20px;
`
