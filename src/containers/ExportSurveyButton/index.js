import React, { useState } from 'react'
import { useMutation } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import ExportSurveyButtonComponent from '../../components/ExportSurveyButton'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { useTranslation } from 'react-i18next'
import ExportSurveyModal from '../ExportSurveyModal'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'

const ExportSurveyButton = ({ surveyId, onMenuItemClick }) => {
  const { t } = useTranslation()
  const sendReport = useMutation(MUTATION_SEND_REPORT)
  const [modalVisible, setModalVisible] = useState(false)

  const handleSubmit = async userId => {
    try {
      setModalVisible(false)
      await sendReport({
        variables: {
          surveyId,
          userId
        }
      })
      displaySuccessMessage(t(`containers.exportSurveyButton.emailSend`))
    } catch (ex) {
      displayErrorPopup(getErrorMessage(ex, 'E_EXPORT_DEFAULT'))
    }
  }

  return (
    <React.Fragment>
      <ExportSurveyButtonComponent
        onClick={() => {
          onMenuItemClick()
          setModalVisible(true)
        }}
        icon='cloud-upload'
        tKey='exportSurveyButton'
      />
      {modalVisible && (
        <ExportSurveyModal
          tKey='exportSurveyButton'
          onCancel={() => setModalVisible(false)}
          onSubmit={handleSubmit}
        />
      )}
    </React.Fragment>
  )
}

const MUTATION_SEND_REPORT = gql`
  mutation sendReport($surveyId: ID!, $userId: ID!) {
    sendSurveyReport(surveyId: $surveyId, userId: $userId)
  }
`

export default withRouter(ExportSurveyButton)
