import React, { Component } from 'react'
import PrivacyForm from '../../components/PrivacyForm'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { withTranslation } from 'react-i18next'

class Privacy extends Component {
  handleToggle = async (field, value) => {
    const {
      user: { id },
      updateTesterInfo,
      t
    } = this.props
    const updateTesterInfoData = {
      id,
      [`${field}`]: value
    }
    const shouldUpdateTesterInfo = Object.keys(updateTesterInfoData).length > 1

    if (shouldUpdateTesterInfo) {
      try {
        const {
          data: {
            updateTesterInfo: {
              ok: updateTesterInfoOk,
              error: updateTesterInfoError
            }
          }
        } = await updateTesterInfo({ variables: updateTesterInfoData })
        if (!updateTesterInfoOk) {
          return displayErrorPopup(
            t('errors.privacyUpdateError', { error: updateTesterInfoError })
          )
        }
      } catch (error) {
        return displayErrorPopup(t('containers.privacy.error'))
      }
    }
  }

  render () {
    const {
      user: {
        testerInfo: {
          privacyMarketingEmails,
          privacyMarketingPushNotifications,
          privacyOtherPushNotifications
        }
      }
    } = this.props

    return (
      <PrivacyForm
        marketingEmails={!!privacyMarketingEmails}
        marketingPushes={!!privacyMarketingPushNotifications}
        pushNotifications={!!privacyOtherPushNotifications}
        toggleMarketingEmails={value =>
          this.handleToggle('privacyMarketingEmails', value)
        }
        toggleMarketingPushes={value =>
          this.handleToggle('privacyMarketingPushNotifications', value)
        }
        togglePushNotifications={value =>
          this.handleToggle('privacyOtherPushNotifications', value)
        }
      />
    )
  }
}

export default withTranslation()(Privacy)
