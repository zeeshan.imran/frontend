import React from 'react'
import { mount } from 'enzyme'
import Privacy from '.'
import PrivacyForm from '../../components/PrivacyForm'
import wait from '../../utils/testUtils/waait'
import { displayErrorPopup } from '../../utils/displayErrorPopup'

jest.mock('../../utils/displayErrorPopup')

describe('Privacy', () => {
  let testRender
  let toggelmarketingEmail
  let updateTesterInfo
  let user

  beforeAll(() => { 
    toggelmarketingEmail = jest.fn()
    toggelmarketingEmail.mockImplementation(function () {
      this.email = jest.fn(() => {
        return 'flowor-wiki@gmail.com'
      })
    })

    user = {
      id: 'userId',
      testerInfo: {
        privacyMarketingEmails: 'flowor-wiki@gmail.com',
        privacyMarketingPushNotifications: true,
        privacyOtherPushNotifications: false
      }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should display refresh page error', async () => {
    testRender = mount(
      <Privacy user={user} updateTesterInfo={updateTesterInfo} />
    )
    testRender
      .find(PrivacyForm)
      .first()
      .prop('toggleMarketingEmails')({
        target: {
          email: 'flavor-wiki@gmail.com'
        }
      })
    await wait(0)
    expect(displayErrorPopup).toHaveBeenCalledWith(
      "containers.privacy.error"
    )
  })

  test('should display updating error', async () => {
    updateTesterInfo = () => {
      return { data: { updateTesterInfo: { ok: false, error: 'some error' } } }
    }
    displayErrorPopup.mockReset()
    user = {
      testerInfo: {
        privacyMarketingEmails: 'flowor-wiki@gmail.com',
        privacyMarketingPushNotifications: true,
        privacyOtherPushNotifications: false
      }
    }
    testRender = mount(
      <Privacy user={user} updateTesterInfo={updateTesterInfo} />
    )
    testRender
      .find(PrivacyForm)
      .first()
      .prop('toggleMarketingEmails')({
        target: {
          email: 'flavor-wiki@gmail.com'
        }
      })
    await wait(0)
    expect(displayErrorPopup).toHaveBeenCalledWith(
      "errors.privacyUpdateError"
    )
  })

  test('should render Privacy', () => {
    updateTesterInfo = () => {
      return { data: { updateTesterInfo: { ok: true } } }
    }
    let newMock = new toggelmarketingEmail()
    newMock.email()
    testRender = mount(
      <Privacy user={user} updateTesterInfo={updateTesterInfo} />
    )
    expect(PrivacyForm).toHaveLength(1)

    testRender
      .find(PrivacyForm)
      .first()
      .prop('toggleMarketingEmails')({
        target: {
          email: 'flavor-wiki@gmail.com'
        }
      })

    expect(toggelmarketingEmail.mock.instances[0].email).toHaveBeenCalled()
  })

  test('should render Privacy', async () => {
    updateTesterInfo = () => {
      return { data: { updateTesterInfo: { ok: true } } }
    }
    let newMock = new toggelmarketingEmail()
    newMock.email()
    testRender = mount(
      <Privacy user={user} updateTesterInfo={updateTesterInfo} />
    )
    await wait(0)

    testRender
      .find(PrivacyForm)
      .first()
      .prop('toggleMarketingPushes')()

    expect(toggelmarketingEmail.mock.instances[0].email).toHaveBeenCalled()

    testRender
      .find(PrivacyForm)
      .first()
      .prop('togglePushNotifications')()

    expect(toggelmarketingEmail.mock.instances[0].email).toHaveBeenCalled()
  })
})
