import React from 'react'
import { mount } from 'enzyme'
import SurveyQuestionLayout, { QUESTION_QUERY, SURVEY_ENROLLMENT } from '.'
import { surveyBasicInfo } from '../../fragments/survey'

import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import defaults from '../../defaults'
import gql from 'graphql-tag'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'

jest.mock('../../utils/surveyAuthentication')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const QUESTION_ID = 'question-1'
const SURVEY_ID = 'survey-1'

const mockActiveSurvey = {
  __typename: 'Survey',
  name: 'active survey',
  uniqueName: 'uniqueName',
  state: 'active',
  minimumProducts: null,
  surveyLanguage: 'en',
  settings: {
    recaptcha: false
  },
  authorizationType: null,
  id: `survey-1`,
  surveyEnrollment: 'survey-enrollment-1',
  context: {},
  value: {
    value: 'value-1'
  },
  range: {},
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ],
  pairsOptions: {},
  pairs: {},
  productsSkip: {},
  products: [{ id: 'product-1', name: 'product one', photo: '' }],
  exclusiveTasters: [],
  allowRetakes: false,
  forcedAccount: false,
  forcedAccountLocation: `start`,
  isScreenerOnly: false,
  showGeneratePdf: false,
  linkedSurveys: [],
  maxProductStatCount: 6,
  savedRewards: [],
  customizeSharingMessage: '',
  loginText: '',
  pauseText: '',
  allowedDaysToFillTheTasting: 5,
  customButtons: {
    continue: null,
    start: null,
    skip: null,
    next: null
  },
  startedAt: Date.now(),
  selectedProduct: 'prod-1'
}

describe('SurveyQuestionLayout', () => {
  let testRender
  let children
  let mockClient

  beforeEach(() => {
    children = <div>Back Default</div>
    window.localStorage = {
      getItem: () => '{}'
    }

    mockClient = createApolloMockClient()

    mockClient.cache.writeQuery({
      query: gql`
        query survey($id: ID) {
          survey(id: "survey-1") {
            ...surveyBasicInfo
          }
        }
        ${surveyBasicInfo}
      `,
      data: {
        survey: {
          ...mockActiveSurvey
        }
      }
    })

    mockClient.cache.writeQuery({
      query: QUESTION_QUERY,
      variables: {
        id: QUESTION_ID
      },
      data: {
        question: {
          id: 'question-1',
          type: 'info',
          displayOn: 'section'
        }
      }
    })

    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-1',
          surveyEnrollmentId: 'survey-enrollment-1',
          savedRewards: []
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyQuestionLayout', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <SurveyQuestionLayout
          surveyId={SURVEY_ID}
          questionId={QUESTION_ID}
          children={children}
        />
      </ApolloProvider>
    )
    expect(testRender.find(SurveyQuestionLayout)).toHaveLength(1)
  })
})
