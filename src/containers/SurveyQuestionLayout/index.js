import React, { useRef, useEffect } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import SurveyQuestionLayoutComponent from '../../components/SurveyQuestionLayout'
import FillerLoader from '../../components/FillerLoader'
import { surveyBasicInfo } from '../../fragments/survey'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'

const SurveyQuestionLayout = ({ surveyId, questionId, children }) => {
  const { data, loading } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId }
  })
  const { data: questionData, loading: questionLoading } = useQuery(
    QUESTION_QUERY,
    {
      variables: { id: questionId }
    }
  )
  
  let products = []
  if(data && data.survey && data.survey.products){
    products = data.survey.products
  }

  const { data: currentSurveyParticipationData } = useQuery(
    SURVEY_PARTICIPATION_QUERY
  )
  const saveRewards = useMutation(SAVE_REWARDS)

  const {
    currentSurveyParticipation: { savedRewards = [], surveyEnrollmentId } = {}
  } = currentSurveyParticipationData || { currentSurveyParticipation: {} }

  const { type, displayOn } =
    questionData && questionData.question ? questionData.question : {}

  const hideProductPicture =
    type === 'info' ||
    // type === 'profile' ||
    type === 'upload-picture' ||
    type === 'choose-product' ||
    displayOn !== 'middle'

  const areRewardsSaved = useRef(false)
  const availableProducts = products.filter(product => product.isAvailable)
  useEffect(() => {
    if (availableProducts.length > 1) {
      areRewardsSaved.current = true
    } else if (
      surveyEnrollmentId &&
      !hideProductPicture && // the product must be visible
      !areRewardsSaved.current &&
      availableProducts.length === 1
    ) {
      areRewardsSaved.current = true
      if (savedRewards && savedRewards.length === 0) {
        let rewards = availableProducts.map(product => ({
          id: product.id,
          reward: product.reward
        }))
        saveRewards({
          variables: {
            rewards: rewards,
            surveyEnrollment: surveyEnrollmentId
          }
        })
      }
    }
  })

  if (loading || questionLoading) {
    return <FillerLoader fullScreen loading />
  }

  const {
    survey: { name }
  } = data

  return <SurveyQuestionLayoutComponent title={name} children={children} />
}

export const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
    }
  }

  ${surveyBasicInfo}
`

export const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      type
      displayOn
    }
  }
`

const SAVE_REWARDS = gql`
  mutation saveRewards($rewards: JSON, $surveyEnrollment: ID) {
    saveRewards(rewards: $rewards, surveyEnrollment: $surveyEnrollment)
  }
`

export default SurveyQuestionLayout
