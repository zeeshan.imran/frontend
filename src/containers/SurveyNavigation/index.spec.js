import React from 'react'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { create } from 'react-test-renderer'
import gql from 'graphql-tag'
import { surveyInfo } from '../../fragments/survey'
import SurveyNavigation from './index'
import { Router } from 'react-router-dom'
import history from '../../history'
import '../../utils/internationalization/i18n'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { displayDraftWarningPopup } from '../../utils/displayDraftWarningPopup'
import defaults from '../../defaults'

jest.doMock('./index.js')
jest.mock('../../utils/displayDraftWarningPopup')

const mockActiveSurvey = {
  __typename: 'Survey',
  name: 'active survey',
  uniqueName: 'uniqueName',
  state: 'active',
  minimumProducts: 1,
  maximumProducts: 1,
  surveyLanguage: 'en',
  settings: {
    recaptcha: false
  },
  authorizationType: 'public',
  id: `survey-1`,
  surveyEnrollment: 'survey-enrollment-1',
  context: {},
  value: {
    value: 'value-1'
  },
  range: {},
  options: {},
  pairsOptions: {},
  pairs: {},
  productsSkip: {},
  products: [],
  exclusiveTasters: [],
  allowRetakes: false,
  forcedAccount: false,
  forcedAccountLocation: `start`,
  isScreenerOnly: false,
  showGeneratePdf: false,
  linkedSurveys: [],
  referralAmount: 5,
  country: 'United States of America',
  recaptcha: false,
  maxProductStatCount: 6,
  savedRewards: [],
  emailService: false,
  allowedDaysToFillTheTasting: 5,
  isPaypalSelected: false,
  isGiftCardSelected: false,
  customButtons: {
    __typename: 'CustomButtons',
    continue: 'Continue',
    start: 'Start',
    next: 'Next',
    skip: 'Skip'
  },
  startedAt: Date.now(),
  selectedProduct: 'prod-1',
  instructionSteps: [],
  instructionsText: '',
  thankYouText: '',
  rejectionText: '',
  screeningText: '',
  customizeSharingMessage: '',
  loginText: '',
  pauseText: '',
  screeningQuestions: {},
  setupQuestions: {},
  productsQuestions: {},
  finishingQuestions: {},
  paymentQuestions: {}
}

const mockDraftSurvey = {
  ...mockActiveSurvey,
  state: 'draft',
  id: 'survey-2',
  name: 'drafted survey'
}

describe('', () => {
  let testRender
  let mockClient

  beforeAll(() => {
    mockClient = createApolloMockClient()

    mockClient.cache.writeQuery({
      query: gql`
        query survey($id: ID) {
          survey(id: "survey-1") {
            ...surveyInfo
          }
        }
        ${surveyInfo}
      `,
      data: {
        survey: {
          ...mockActiveSurvey
        }
      }
    })

    mockClient.cache.writeQuery({
      query: gql`
        query survey($id: ID) {
          survey(id: "survey-2") {
            ...surveyInfo
          }
        }
        ${surveyInfo}
      `,
      data: {
        survey: {
          ...mockDraftSurvey
        }
      }
    })

    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-1',
          surveyEnrollmentId: 'survey-enrollment-1',
          state: 'active',
          canSkipCurrentQuestion: false,
          paypalEmail: '',
          selectedProduct: null,
          selectedProducts: [],
          currentSurveyStep: null,
          currentSurveySection: null,
          isCurrentAnswerValid: true,
          currentAnswerSkipTarget: null,
          answers: [],
          lastAnsweredQuestion: null,
          country: 'United States of America',
          savedRewards: []
        }
      }
    })

    testRender = create(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveyNavigation match={{ url: `/survey/survey-1/` }} />
        </Router>
      </ApolloProvider>
    )
  })

  afterEach(() => {
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-2',
          surveyEnrollmentId: 'survey-enrollment-1',
          state: 'draft',
          canSkipCurrentQuestion: false,
          paypalEmail: '',
          selectedProduct: null,
          selectedProducts: [],
          currentSurveyStep: null,
          currentSurveySection: null,
          isCurrentAnswerValid: true,
          currentAnswerSkipTarget: null,
          answers: [],
          lastAnsweredQuestion: null,
          country: 'United States of America',
          savedRewards: []
        }
      }
    })
    testRender.update(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveyNavigation match={{ url: `/survey/survey-2/` }} />
        </Router>
      </ApolloProvider>
    )
  })

  afterAll(() => {
    testRender.unmount()
  })

  test('The active survey should not have a notification popup', () => {
    expect(displayDraftWarningPopup).not.toHaveBeenCalled()
  })

  // to do luca
  // test('The draft survey should have a proper notification popup', () => {
  //   expect(displayDraftWarningPopup).toHaveBeenCalled()
  // })
})
