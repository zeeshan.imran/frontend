import React from 'react'
import wait from '../../utils/testUtils/waait'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { mount } from 'enzyme'
import MandatoryQuestionsToCreate from './index'
import '../../utils/internationalization/i18n'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import surveyCreationDefaults from '../../defaults/surveyCreation'

jest.mock('react-router-dom', () => ({
  withRouter: child => child
}))

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  exists: val => !!val
}))

describe('QuestionCreation List', () => {
  let testRender
  let mockClient

  afterEach(() => {
    testRender.unmount()
  })

  test('Should update MandatoryQuestionsToCreate mandatoryQuestions toBe null', async () => {
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          questions: [],
          mandatoryQuestions: []
        }
      }
    })

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <MandatoryQuestionsToCreate location={{ search: '?section=end' }} />
      </ApolloProvider>
    )

    await wait(0)
    expect(testRender).toMatchSnapshot()
  })

  test('Should update MandatoryQuestionsToCreate', async () => {
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          ...surveyCreationDefaults.surveyCreation,
          questions: [
            {
              id: 'question-1',
              type: 'email',
              prompt: 'What is your email?',
              displayOn: 'section'
            },
            {
              id: 'question-2',
              type: 'email',
              prompt: 'What is your email?',
              displayOn: 'middel'
            },
            {
              id: 'question-3',
              type: 'email',
              prompt: 'What is your email?',
              displayOn: 'middle'
            }
          ],
          mandatoryQuestions: [
            {
              id: 'question-4',
              type: 'email',
              prompt: 'What is your email?',
              displayOn: 'section'
            },
            {
              id: 'question-5',
              type: 'email',
              prompt: 'What is your email?',
              displayOn: 'middle'
            }
          ]
        }
      }
    })

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <MandatoryQuestionsToCreate location={{ search: '?section=middle' }} />
      </ApolloProvider>
    )
    await wait(0)
    expect(testRender.find(MandatoryQuestionsToCreate)).toHaveLength(1)
  })

  test('Should update test for filterdMandatoryQuestions length 0', async () => {
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          ...surveyCreationDefaults.surveyCreation,
          questions: [
            {
              id: 'question-2',
              type: 'email',
              prompt: 'What is your email?',
              displayOn: 'middel'
            }
          ],
          mandatoryQuestions: [
            {
              id: 'question-5',
              type: 'email',
              prompt: 'What is your email?',
              displayOn: 'middle'
            }
          ]
        }
      }
    })

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <MandatoryQuestionsToCreate location={{ search: '?section=section' }} />
      </ApolloProvider>
    )
    await wait(0)
    expect(testRender.find(MandatoryQuestionsToCreate)).toHaveLength(1)
  })
})
