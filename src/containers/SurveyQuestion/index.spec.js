import React from 'react'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { mount } from 'enzyme'
import SurveyQuestion, {
  QUESTION_QUERY,
  SURVEY_QUERY,
  SURVEY_ENROLLMENT
} from './index'
import i18n from '../../utils/internationalization/i18n'
import wait from '../../utils/testUtils/waait'
import defaults from '../../defaults'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import SurveyQuestionComponent from '../../components/SurveyQuestion'
import { defaultSurvey, defaultQuestion } from '../../mocks'

jest.mock('../../components/SurveyQuestion', () => () => null)

const QUESTION_ID = 'question-1'
const SURVEY_ID = 'survey-1'
const SURVEY_ENROLLMENT_ID = 'survey-enrollment-1'

const mockActiveSurvey = {
  ...defaultSurvey,
  name: 'active survey',
  uniqueName: 'uniqueName',
  state: 'active',
  surveyLanguage: 'de',
  minimumProducts: 1,
  maximumProducts: 1,
  authorizationType: 'public',
  id: SURVEY_ID,
  surveyEnrollment: 'survey-enrollment-1',
  selectedProduct: 'prod-1'
}

const mockQuestionQuery = isRequired => ({
  request: {
    query: QUESTION_QUERY,
    variables: { id: QUESTION_ID }
  },
  result: {
    data: {
      question: {
        ...defaultQuestion,
        id: QUESTION_ID,
        required: isRequired,
        __typename: 'Question'
      }
    }
  }
})

const mockSurveyQuery = {
  request: {
    query: SURVEY_QUERY,
    variables: { id: SURVEY_ID }
  },
  result: {
    data: {
      survey: {
        ...mockActiveSurvey
      }
    }
  }
}

const mockSurveyEnrollment = {
  request: {
    query: SURVEY_ENROLLMENT,
    variables: { id: SURVEY_ENROLLMENT_ID }
  },
  result: {
    data: {
      surveyEnrollment: {
        selectedProducts: []
      }
    }
  }
}

describe('SurveyQuestion', () => {
  let testRender
  let mockClient

  afterEach(() => {
    // testRender.unmount()
  })

  test('Should set Values for SurveyQuestion component ', async () => {
    expect(1).toBe(1)
    // mockClient = createApolloMockClient({
    //   mocks: [mockQuestionQuery(false), mockSurveyQuery, mockSurveyEnrollment]
    // })
    // mockClient.cache.writeQuery({
    //   query: SURVEY_PARTICIPATION_QUERY,
    //   data: {
    //     currentSurveyParticipation: {
    //       ...defaults.currentSurveyParticipation,
    //       surveyEnrollmentId: SURVEY_ENROLLMENT_ID,
    //       surveyId: SURVEY_ID,
    //       selectedProducts: ['product-1'],
    //       answers: []
    //     }
    //   }
    // })

    // testRender = mount(
    //   <ApolloProvider client={mockClient}>
    //     <SurveyQuestion questionId={QUESTION_ID} surveyId={SURVEY_ID} />
    //   </ApolloProvider>
    // )

    // await wait(100)
    // testRender.update()

    // const {
    //   currentSurveyParticipation: surveyParticipation
    // } = mockClient.cache.readQuery({ query: SURVEY_PARTICIPATION_QUERY }, false)
    // expect(surveyParticipation.isCurrentAnswerValid).toBe(false)

    // const values = ['value-1']
    // testRender
    //   .find(SurveyQuestionComponent)
    //   .props()
    //   .onChange({ values })

    // await wait(0)
    // testRender.update()

    // const questionProps = testRender.find(SurveyQuestionComponent).props()

    // expect(questionProps.value).toBe(values)
    // expect(questionProps.id).toBe(QUESTION_ID)
  })

  // test('Test invalid answers', async () => {
  //   mockClient = createApolloMockClient({
  //     mocks: [mockQuestionQuery(true), mockSurveyQuery, mockSurveyEnrollment]
  //   })
  //   mockClient.cache.writeQuery({
  //     query: SURVEY_PARTICIPATION_QUERY,
  //     data: {
  //       currentSurveyParticipation: {
  //         ...defaults.currentSurveyParticipation,
  //         surveyId: SURVEY_ID,
  //         selectedProducts: ['product-1']
  //       }
  //     }
  //   })
  //   testRender = mount(
  //     <ApolloProvider client={mockClient}>
  //       <SurveyQuestion questionId={QUESTION_ID} surveyId={SURVEY_ID} />
  //     </ApolloProvider>
  //   )
  //   await wait(0)
  //   testRender.update()
  //   await wait(0)

  //   const {
  //     currentSurveyParticipation: surveyParticipation
  //   } = mockClient.cache.readQuery({ query: SURVEY_PARTICIPATION_QUERY }, false)

  //   expect(surveyParticipation.isCurrentAnswerValid).toBe(false)

  //   const values = ['value-1']

  //   testRender
  //     .find(SurveyQuestionComponent)
  //     .props()
  //     .onChange({ values, isValid: true })

  //   await wait(0)
  //   const {
  //     currentSurveyParticipation: surveyParticipation2
  //   } = mockClient.cache.readQuery({ query: SURVEY_PARTICIPATION_QUERY }, false)

  //   expect(surveyParticipation2.isCurrentAnswerValid).toBe(true)

  //   testRender
  //     .find(SurveyQuestionComponent)
  //     .props()
  //     .onChange({ values, isValid: false })

  //   await wait(0)
  //   const {
  //     currentSurveyParticipation: surveyParticipation3
  //   } = mockClient.cache.readQuery({ query: SURVEY_PARTICIPATION_QUERY }, false)
  //   expect(surveyParticipation3.isCurrentAnswerValid).toBe(false)
  // })

  // test('Should change language', async () => {
  //   mockClient = createApolloMockClient({
  //     mocks: [mockQuestionQuery(true), mockSurveyQuery, mockSurveyEnrollment]
  //   })
  //   mockClient.cache.writeQuery({
  //     query: SURVEY_PARTICIPATION_QUERY,
  //     data: {
  //       currentSurveyParticipation: {
  //         ...defaults.currentSurveyParticipation,
  //         surveyId: SURVEY_ID,
  //         selectedProducts: ['product-1']
  //       }
  //     }
  //   })
  //   const mockChangeLang = jest.fn()
  //   i18n.changeLanguage = mockChangeLang
  //   i18n.language = 'en'
  //   testRender = mount(
  //     <ApolloProvider client={mockClient}>
  //       <SurveyQuestion questionId={QUESTION_ID} surveyId={SURVEY_ID} />
  //     </ApolloProvider>
  //   )
  //   await wait(0)
  //   expect(mockChangeLang).toBeCalledWith('de')
  // })
})
