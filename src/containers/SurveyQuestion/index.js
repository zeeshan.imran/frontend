import React, { useEffect, useState, useCallback } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'

import SurveyQuestionComponent from '../../components/SurveyQuestion'
import FillerLoader from '../../components/FillerLoader'

import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { questionInfo, surveyInfo } from '../../fragments/survey'

import getAnswerLocalId from '../../utils/getAnswerLocalId'
import { withTranslation } from 'react-i18next'
import defaults from '../../defaults'
import useDebounceAdvanceStep from '../../hooks/useDebounceAdvanceStep'

const AUTO_ADVANCE_IMMEDIATE = [
  'open-answer',
  'email',
  'numeric',
  'select-and-justify'
]

export const canSkipCurrentQuestion = ({ question, canSkipAnswer }) =>
  canSkipAnswer && question && !question.required

const SurveyQuestion = ({
  questionId,
  i18n,
  surveyId,
  onSubmit = () => {}
}) => {
  const saveCurrentSurveyParticipation = useMutation(SAVE_SURVEY_PARTICIPATION)
  const updateParticipation = useCallback(
    properties =>
      saveCurrentSurveyParticipation({
        variables: {
          ...properties
        }
      }),
    [saveCurrentSurveyParticipation]
  )
  const { data } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId }
  })

  const autoAdvanceSettings = (data &&
    data.survey &&
    data.survey.autoAdvanceSettings) || {
    active: false,
    debounce: 0,
    hideNextButton: true
  }

  if (data.survey) {
    let surveyLanguage = data.survey.surveyLanguage || 'en'
    if (i18n.language !== surveyLanguage) {
      i18n.changeLanguage(surveyLanguage)
    }
  }

  const { data: questionData, loading: loadingQuestion } = useQuery(
    QUESTION_QUERY,
    {
      variables: { id: questionId },
      fetchPolicy: 'network-only'
    }
  )
  const {
    data: {
      currentSurveyParticipation: { answers: currentAnswers }
    },
    loading: loadingParticipation
  } = useQuery(CURRENT_ANSWERS_QUERY)

  const {
    data: {
      currentSurveyParticipation: {
        surveyEnrollmentId = '',
        selectedProduct = {}
      } = {}
    } = {}
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const {
    data: { surveyEnrollment: { selectedProducts = [] } = {} }
  } = useQuery(SURVEY_ENROLLMENT, {
    variables: { id: surveyEnrollmentId },
    fetchPolicy: 'network-only'
  })

  const [startTime, setStartTime] = useState(new Date())
  useEffect(() => {
    setStartTime(new Date())
  }, [questionId])

  const { question } = questionData || {}

  const { values } =
    currentAnswers.find(
      ({ id }) => getAnswerLocalId(questionId, selectedProducts) === id
    ) || {}

  const onChangeAnswer = ({
    values: updatedValues = [],
    isValid = false,
    canSkipAnswer = true,
    context
  }) => {
    const answer = {
      __typename: 'LocalAnswer',
      id: getAnswerLocalId(questionId, selectedProducts),
      values: updatedValues,
      context,
      startedAt: startTime.toISOString()
    }

    const isAnswered =
      (question && !question.required) ||
      (updatedValues && updatedValues.length > 0)

    updateParticipation({
      answers: [
        ...currentAnswers.slice(0, answerIndex),
        answer,
        ...currentAnswers.slice(answerIndex + 1)
      ],
      isCurrentAnswerValid: isAnswered && isValid,
      canSkipCurrentQuestion: canSkipCurrentQuestion({
        question,
        canSkipAnswer
      })
    })
  }

  useEffect(() => {
    if (question) {
      onChangeAnswer({ values })
    }
  }, [question])

  const debounceSeconds =
    question && AUTO_ADVANCE_IMMEDIATE.includes(question.type)
      ? 0
      : autoAdvanceSettings.debounce

  const debounceAdvanceStep = useDebounceAdvanceStep(debounceSeconds)

  const [isSubmitting, setIsSubmitting] = useState(false)

  const handleSubmit = useCallback(() => {
    onSubmit()
    setIsSubmitting(true)
  }, [onSubmit])

  const handleSubmitDone = useCallback(() => {
    setIsSubmitting(false)
  }, [])

  let answerIndex = currentAnswers.findIndex(
    ({ id }) => id === getAnswerLocalId(questionId, selectedProducts)
  )
  answerIndex = answerIndex === -1 ? currentAnswers.length : answerIndex // push to answer stack if not there already

  const availableIn = (data.survey && data.survey.country) || ''
  const availableCountry =
    defaults.countries.find(country => {
      return country.name === availableIn
    }) || {}

  const prefix = defaults.currencyPrefixes[availableCountry.currency] || ''
  const suffix = defaults.currencySuffixes[availableCountry.currency] || ''

  let currentSelectedProduct = []
  if (selectedProduct && selectedProducts.length) {
    currentSelectedProduct = selectedProducts.filter(
      singleProduct => singleProduct.id === selectedProduct
    )
  }
  const productImage =
    (currentSelectedProduct.length > 0 && currentSelectedProduct[0].photo) || ''
  const productName =
    (currentSelectedProduct.length > 0 && currentSelectedProduct[0].name) || ''
  const reward =
    (currentSelectedProduct.length > 0 && currentSelectedProduct[0].reward) ||
    ''

  if (loadingQuestion || loadingParticipation || isSubmitting) {
    return <FillerLoader fullScreen loading />
  }
  const hideProductPicture =
    question.type === 'info' ||
    // question.type === 'profile' ||
    question.type === 'upload-picture' ||
    question.type === 'choose-product'

  const isBaseLayout =
    question.type !== 'profile' && question.type !== 'paired-questions'
  const isSecondaryPrompt =
    question.type !== 'select-and-justify' && question.type !== 'info'

  return (
    <SurveyQuestionComponent
      onChange={onChangeAnswer}
      value={values}
      id={questionId}
      onSubmit={handleSubmit}
      onSubmitError={handleSubmitDone}
      onSubmitSuccess={handleSubmitDone}
      autoAdvanceSettings={autoAdvanceSettings}
      onSubmitReady={debounceAdvanceStep}
      onSubmitNotReady={() => {
        debounceAdvanceStep.cancel()
      }}
      {...question}
      selectedProduct={selectedProduct}
      country={data && data.survey && data.survey.country}
      hideProductPicture={
        question.displayOn !== 'middle' ? hideProductPicture : false
      }
      productPictureUrl={productImage}
      productName={productName}
      reward={reward && `${prefix}${reward}${suffix}`}
      isBaseLayout={isBaseLayout}
      isSecondaryPrompt={isSecondaryPrompt}
      language={data.survey.surveyLanguage}
      optionDisplayType={question.optionDisplayType}
    />
  )
}

const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation(
    $isCurrentAnswerValid: Boolean
    $canSkipCurrentQuestion: Boolean
    $answers: [LocalAnswer]
  ) {
    saveCurrentSurveyParticipation(
      isCurrentAnswerValid: $isCurrentAnswerValid
      canSkipCurrentQuestion: $canSkipCurrentQuestion
      answers: $answers
    ) @client
  }
`

const CURRENT_ANSWERS_QUERY = gql`
  query {
    currentSurveyParticipation @client {
      answers
    }
  }
`

export const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      ...questionInfo
    }
  }
  ${questionInfo}
`

export const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyInfo
    }
  }

  ${surveyInfo}
`

export const SURVEY_ENROLLMENT = gql`
  query surveyEnrollment($id: ID) {
    surveyEnrollment(id: $id) {
      selectedProducts {
        id
        name
        photo
        reward
      }
    }
  }
`

export default withTranslation()(SurveyQuestion)
