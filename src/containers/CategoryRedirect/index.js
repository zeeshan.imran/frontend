import React from 'react'
import { Query } from 'react-apollo'
import { Redirect, withRouter } from 'react-router-dom'
import CATEGORIES_QUERY from '../../queries/CategoriesRedirect'
import Loader from '../../components/Loader'

const CategoryRedirect = ({ categoryName, match }) => (
  <Query
    query={CATEGORIES_QUERY}
    fetchPolicy='cache-and-network'
    variables={{ rootOnly: false, key: categoryName }}
  >
    {({ data: { productCategories }, loading }) => {
      if (loading) return <Loader />

      const categoryChildren = productCategories[0].children
      const hasChildren =
        productCategories.length > 0 &&
        categoryChildren &&
        categoryChildren.length > 0

      return hasChildren ? (
        <Redirect to={`${match.url}/subcategories`} />
      ) : (
        // <Redirect to={`${match.url}/products`} /> FIXME Implement this as soon as we get backend support
        <div>{`${categoryName} Products Page`}</div> // FIXME Temporary template until we get backend support
      )
    }}
  </Query>
)

export default withRouter(CategoryRedirect)
