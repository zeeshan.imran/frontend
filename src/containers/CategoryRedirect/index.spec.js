import React from 'react'
import { mount, shallow } from 'enzyme'
import CategoryRedirect from '.'
import { MockedProvider } from 'react-apollo/test-utils'
import CATEGORIES_QUERY from '../../queries/CategoriesRedirect'
import Loader from '../../components/Loader'
import { Router, Redirect } from 'react-router-dom'
import wait from '../../utils/testUtils/waait'

jest.mock('react-router-dom/Redirect')

const mocks = [
  {
    request: {
      query: CATEGORIES_QUERY,
      variables: { rootOnly: false, key: 'cate-1' }
    },
    result: {
      data: {
        productCategories: [
          {
            children: [
              {
                value: 'cate-1',
                key: 1
              }
            ]
          }
        ]
      }
    }
  }
]

describe('CategoryRedirect', () => {
  let testRender
  let historyMock

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render loader', async () => {
    testRender = shallow(
      <MockedProvider>
        <Router history={historyMock}>
          <CategoryRedirect.WrappedComponent categoryName={'cate-1'} />
        </Router>
      </MockedProvider>
    )
    await wait(0)

    expect(testRender.find(Loader)).toMatchSnapshot()
  })

  test('should render OngoingSurveysGridComponent', async () => {
    const redirect = jest.fn()
    Redirect.mockImplementation(redirect)

    testRender = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Router history={historyMock}>
          <CategoryRedirect.WrappedComponent
            match={{ params: { url: 'survey' } }}
            categoryName={'cate-1'}
          />
        </Router>
      </MockedProvider>
    )

    await wait(0)

    expect(CategoryRedirect).toHaveLength(1)
  })
})
