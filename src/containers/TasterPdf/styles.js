import styled from 'styled-components'
import { Col, Row } from 'antd'

export const BaseContainer = styled.div`
  display: flex;
  width: 100%;
  margin: 0;
  padding: 0;

  background: #ffffff;
`
export const Container = styled.div`
  background: #ffffff;
  padding: 2em;
`
export const ChartContainer = styled.div`
  margin: 0 auto;
  background: #ffffff;
  @media print {
    .chart-print {
      page-break-inside: avoid !important;
      margin: auto;
    }
  }
`
export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6.4rem;
`
export const Logo = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 18.5rem;
  height: 3.4rem;
  cursor: ${({ onClick }) => (onClick ? 'pointer' : 'auto')};
  margin-top: 20px;
`
export const BreakContainer = styled.div`
  @media print {
    page-break-after: auto !important;
    float: none;
    margin: auto;
  }
`
export const TextColumn = styled(Col)`
  padding: 15px;
`
export const GraphColumn = styled(Col)`
  text-align: center;
  max-width: 100%;
`
export const MainRow = styled(Row)`
  box-shadow: 0px 0px 5px 2px #cccccc;
  border: 1px solid #ccc;
  border-radius: 5px;
  padding: 0 15px;
  margin-bottom: 20px;
  @media print {
    page-break-inside: avoid !important;
  }
`
