import React from 'react'
import {
  TextColumn,
  GraphColumn,
  MainRow,
  QuestionStyle,
  HeadingStyle
} from './styles.js'
import ChartsPdf from '../../../components/Charts/ChartsPdf'
const PairedQuestion = ({
  mainCharts,
  index,
  question,
  product,
  value,
  singleChart,
  chartsSettings,
  t
}) => {
  const updatedChart = {...chartsSettings,...{isDataTableShown : false}}
  return (
    <MainRow className='main_chart-view'>
      <TextColumn span={24}>
        <HeadingStyle>Question:</HeadingStyle>
        <QuestionStyle>{question}</QuestionStyle>
      </TextColumn>
      <GraphColumn span={24}>
        {singleChart && (
          <React.Fragment>
            <TextColumn span={12}>
              <HeadingStyle>Personal Profile:</HeadingStyle>
            </TextColumn>
            <ChartsPdf
              charts={singleChart}
              hideFilter
              idx={index}
              fullWidth='550px'
              chartsSettings={updatedChart}
              operator={false}
              t={t}
            />
          </React.Fragment>
        )}
      </GraphColumn>
      <GraphColumn span={24}>
        {mainCharts && (
          <React.Fragment>
            <TextColumn span={12}>
              <HeadingStyle>Group Profile:</HeadingStyle>
            </TextColumn>
            <ChartsPdf
              charts={mainCharts}
              chartsSettings={updatedChart}
              hideFilter
              idx={index}
              operator={false}
              fullWidth='550px'
              t={t}
            />
          </React.Fragment>
        )}
      </GraphColumn>
    </MainRow>
  )
}
export default PairedQuestion
