import React from 'react'
import {
  TextColumn,
  GraphColumn,
  MainRow,
  AnswerStyle,
  QuestionStyle,
  HeadingStyle
} from './styles.js'
import ChartsPdf from '../../../components/Charts/ChartsPdf'

const ChooseMultiple = ({
  mainCharts,
  index,
  question,
  product,
  answer,
  chartsSettings,
  t
}) => {
  const multipleList = items => {
    const listItem = items.map((item, index) => {
      return <li key={index}>{item}</li>
    })
    return <ul>{listItem}</ul>
  }

  return (
    <MainRow className='main_chart-view'>
      <TextColumn span={product ? 8 : 12}>
        <HeadingStyle>Question:</HeadingStyle>
        <QuestionStyle>{question}</QuestionStyle>
      </TextColumn>
      {
        (product) && (
          <TextColumn span={8}>
            <HeadingStyle>Product:</HeadingStyle>
            <QuestionStyle>{product}</QuestionStyle>
          </TextColumn>
        ) 
      }
      <TextColumn span={product ? 8 : 12}>
        <HeadingStyle>Answer:</HeadingStyle>
        <AnswerStyle>{answer && multipleList(answer)}</AnswerStyle>
      </TextColumn>
      <GraphColumn span={24}>
        {mainCharts && (
          <ChartsPdf
            charts={mainCharts}
            hideFilter
            idx={index}
            chartsSettings={chartsSettings}
            operator={false}
            t={t}
          />
        )}
      </GraphColumn>
    </MainRow>
  )
}
export default ChooseMultiple
