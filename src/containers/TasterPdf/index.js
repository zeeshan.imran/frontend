import React, { useEffect, useState } from 'react'
import { path } from 'ramda'
import gql from 'graphql-tag'
import * as Yup from 'yup'
import * as chartUtils from '../../utils/chartUtils'
import LoadingModal from '../../components/LoadingModal'
import { useQuery } from 'react-apollo-hooks'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import { useTranslation } from 'react-i18next'
import ErrorBoundary from '../SurveyStats/ErrorBoundary'
import { getImages } from '../../utils/getImages'
import Question from './Question'
import { BaseContainer, Container, HeaderContainer, Logo } from './styles'
import { imagesCollection } from '../../assets/png'
import getQueryParams from '../../utils/getQueryParams'
import { getDefaultChartSettings } from '../../utils/chartSettings'
const { desktopImage } = getImages(imagesCollection)

const TasterPdf = ({
  match: {
    params: { surveyId }
  },
  history
}) => {
  const { t } = useTranslation()
  const changeView = item => {
    let finalItem = []
    finalItem = item.map((element, index) => {
      if (element.mainChart) {
        element.mainChart = { ...element.mainChart, ...{ chart_id: index } }
      }
      if (element.singleChart) {
        element.singleChart = {
          ...element.singleChart,
          ...{ chart_id: `${index}_0` }
        }
      }
      return (
        <Question
          key={index}
          answer={element.mainValue}
          mainCharts={element.mainChart && element.mainChart}
          singleChart={element.singleChart && element.singleChart}
          generatePaired={element.generatePaired}
          index={index}
          question={element.prompt}
          product={element.product ? element.product : ''}
          typeOf={element.typeOf}
          value={element.value}
          chartsSettings={
            chartsSettings &&
            element.mainChart &&
            chartsSettings[
              `${element.mainChart.question}-${element.mainChart.chart_type}`
            ]
              ? chartsSettings[
                  `${element.mainChart.question}-${element.mainChart.chart_type}`
                ]
              : {}
          }
          t={t}
        />
      )
    })
    return finalItem
  }

  const {
    data: { getChartsSettings = {} } = {},
    loading: chartsLoading
  } = useQuery(CHARTS_SETTINGS_QUERY, {
    variables: {
      surveyId
    }
  })
  const [chartsSettings, setChartsSettings] = useState(
    getChartsSettings.chartsSettings || {}
  )
  const { location } = history
  const { enrollmentId } = getQueryParams(location)
  const getStats = path(['survey', 'stats'])
  const getExtended = path(['survey', 'stats', 'extendedPairedChart'])
  const [chartGeneration, setChartGeneration] = useState(true)
  const [enrollmentCharts, setEnrollmentCharts] = useState([])
  const validationSchema = Yup.object().shape({
    tabs: Yup.array()
      .typeError(t('containers.surveyStats.validation.typeError'))
      .of(
        Yup.object().shape({
          title: Yup.string().required(),
          charts: Yup.array()
        })
      )
  })

  const { loading, data } = useQuery(Survey_ANSWER_WITH_STATS, {
    variables: {
      surveyId: surveyId,
      checkOrg: false,
      productIds: [],
      questionFilters: [],
      enrollmentId: enrollmentId,
      sync: true
    },
    fetchPolicy: 'network-only'
  })
  let stats = []
  let chartsData = []
  let singlePaired = []
  let surveyAnswerWithCharts
  if (chartGeneration) {
    stats = getStats(data)
    singlePaired = getExtended(data)
    chartsData =
      stats &&
      validationSchema.isValidSync(stats) &&
      chartUtils.getChartView(stats)
    if (singlePaired) {
      singlePaired = chartUtils.getChartView(stats.extendedPairedChart)
    }
    const { surveyAnswers } = data

    if (surveyAnswers && surveyAnswers.length > 0) {
      surveyAnswerWithCharts = surveyAnswers.map((item, index) => {
        let foundChart = {}
        let foundPairedChart = {}
        if (item.typeOf === 'paired-questions' && singlePaired.length > 0) {
          foundChart = chartsData.find(
            element =>
              element.title === item.chartTitle &&
              element.question_id === item.question_id
          )
          foundPairedChart = singlePaired.find(
            element =>
              element.title === item.chartTitle &&
              element.question_id === item.question_id
          )
          return {
            ...item,
            ...{ generatePaired: true },
            ...{ mainChart: foundChart },
            ...{ singleChart: foundPairedChart }
          }
        } else {
          foundChart = chartsData.find(
            element =>
              element.title === item.chartTitle &&
              element.question_id === item.question_id
          )
          return { ...item, ...{ mainChart: foundChart } }
        }
      })
      setChartGeneration(false)
      if (surveyAnswerWithCharts.length > 0) {
        setEnrollmentCharts(surveyAnswerWithCharts)
      }
    }
  }

  const setDefaultChartSettings = () => {
    stats = getStats(data)
    if (stats) {
      const defaultSettings = getDefaultChartSettings(stats)

      setChartsSettings(defaultSettings)
    }
  }

  useEffect(() => {
    if (!chartsLoading && !loading) {
      // Chart settings from graphQL
      if (getChartsSettings.chartsSettings) {
        setChartsSettings(getChartsSettings.chartsSettings)
      } else {
        setDefaultChartSettings()
      }
    }
  }, [chartsLoading, loading])

  if (loading) {
    return <LoadingModal visible />
  }

  return (
    <BaseContainer>
      <Container id='main-container'>
        <OperatorPage>
          <OperatorPageContent>
            <HeaderContainer>
              <Logo onClick={() => history.push('/')} src={desktopImage} />
            </HeaderContainer>
            {enrollmentCharts && changeView(enrollmentCharts)}
          </OperatorPageContent>
        </OperatorPage>
      </Container>
    </BaseContainer>
  )
}

const Survey_ANSWER_WITH_STATS = gql`
  query surveyStatsData(
    $surveyId: ID!
    $productIds: [String!]
    $questionFilters: [Object!]
    $checkOrg: Boolean
    $sync: Boolean
    $enrollmentId: [String!]
  ) {
    survey(id: $surveyId, checkOrg: $checkOrg) {
      id
      stats(
        productIds: $productIds
        questionFilters: $questionFilters
        sync: $sync
        enrollmentId: $enrollmentId
      )
    }
    surveyAnswers(enrollmentId: $enrollmentId)
  }
`
const CHARTS_SETTINGS_QUERY = gql`
  query getChartsSettings($surveyId: ID!) {
    getChartsSettings(surveyId: $surveyId)
  }
`

export default props => (
  <ErrorBoundary>
    <TasterPdf {...props} />
  </ErrorBoundary>
)
