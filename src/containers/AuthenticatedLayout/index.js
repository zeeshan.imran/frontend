import React from 'react'
import AuthenticatedLayout from '../../components/AuthenticatedLayout'
import history from '../../history'
import { logout, getAuthenticatedUser } from '../../utils/userAuthentication'
import i18n from '../../utils/internationalization/i18n'

const NAVIGATION_ENTRIES = [
  {
    title: i18n.t('containers.authenticatedLayut.home'),
    route: '/'
  },
  // {
  //   title: 'My Profile',
  //   route: 'my-profile'
  // },
  {
    title: i18n.t('containers.authenticatedLayut.signOut'),
    action: logout
  }
]

const getEntryName = ({ title }) => title

const handleNavigate = entry =>
  entry.action ? entry.action() : history.push(entry.route)

const AuthenticatedLayoutContainer = props => {
  const user = getAuthenticatedUser()

  return (
    <AuthenticatedLayout
      user={user}
      getEntryName={getEntryName}
      handleNavigate={handleNavigate}
      navigationEntries={NAVIGATION_ENTRIES}
      {...props}
    />
  )
}

export default AuthenticatedLayoutContainer
