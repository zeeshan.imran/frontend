import React from 'react'
import { mount } from 'enzyme'
import AuthenticatedLayoutContainer from '.'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { MockedProvider } from 'react-apollo/test-utils'
import AuthenticatedLayout from '../../components/AuthenticatedLayout'

describe('AuthenticatedLayoutContainer', () => {
  let testRender

  let user

  beforeEach(() => {
    user = getAuthenticatedUser()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AuthenticatedLayout', () => {
    testRender = mount(
      <MockedProvider>
        <AuthenticatedLayoutContainer desktop={''} user={user} />
      </MockedProvider>
    )

    expect(testRender).toMatchSnapshot()
  })

  test('should render AuthenticatedLayout', () => {
    testRender = mount(
      <MockedProvider>
        <AuthenticatedLayoutContainer desktop={''} user={user} />
      </MockedProvider>
    )

    testRender.find(AuthenticatedLayout).prop('handleNavigate')(jest.fn())
  })
})
