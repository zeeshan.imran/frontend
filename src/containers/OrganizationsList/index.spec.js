import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import OrganizationsListContainer, {
  ORGANIZATIONS,
  COUNT_ORGANIZATIONS
} from './index'
import { Table, Button } from 'antd'
import wait from '../../utils/testUtils/waait'
import OrganizationForm from '../../components/OrganizationsList/OrganizationForm'

jest.doMock('./index.js')
jest.mock('rc-util/lib/Portal')
jest.mock('formik')
jest.doMock('antd')
 
describe('check the operator interaction when adding, searching, and deleting organizations', () => {
  let testRender
  let apolloClient
  let location
  const useRefRpy = jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { validateForm: jest.fn() } });

  let mockPush = jest.fn(({ search }) => {
    location = {
      ...location,
      search
    } 
  })

  beforeEach(async () => {
    apolloClient = createApolloMockClient({
      mocks: [
        {
          request: {
            query: ORGANIZATIONS,
            variables: {
              input: {
                keyword: '',
                skip: 0,
                orderBy: undefined,
                orderDirection: undefined
              }
            }
          },
          result: {
            data: {
              organizations: []
            }
          }
        },
        {
          request: {
            query: COUNT_ORGANIZATIONS,
            variables: { input: { keyword: '' } }
          },
          result: {
            data: {
              countOrganizations: 0
            }
          }
        }
      ]
    })

    location = {
      path: '/operator/users'
    }

    testRender = mount(
      <ApolloProvider client={apolloClient}>
        <OrganizationsListContainer
          history={{
            push: mockPush
          }}
          location={location}
          match={{ url: '/operator/organizations' }}
        />
      </ApolloProvider>
    )
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('orderBy to history.push', async () => {
    const names = ['name', 'name', 'createdBy', 'orderBy']
    const order = ['descend', 'ascend', 'descend', 'ascend']
    const organizationTable = testRender.find(Table)
    for (let index = 0; index < names.length; index++) {
      act(() => {
        organizationTable.prop('onChange')(
          {
            current: 1
          },
          '',
          {
            field: names[index],
            order: order[index]
          }
        )
      })
      expect(location.search).toBe(
        `orderBy=${names[index]}&orderDirection=${order[index]}&page=1`
      )
    }
  })

  test('search bar to history.push', async () => {
    const searchBar = testRender.find({ placeholder: 'Search' }).first()
    act(() => {
      searchBar.prop('handleChange')('this is a search text')
    })
    expect(location.search).toBe('keyword=this%20is%20a%20search%20text&page=1')
  })

  test('open add organization modal', async () => {
    act(() => {
      const addButton = testRender
        .find(Button)
        .find({
          children: 'Add organization'
        })
        .first()

      addButton.prop('onClick')()
    })

    await wait(0)
    testRender.update()

    expect(testRender.find(OrganizationForm)).toHaveLength(1)
    expect(testRender.find(OrganizationForm).props()).toMatchObject({
      editOrganizationId: true,
      newOrganizationData: {},
      modalType: 'add'
    })
  })
})
