import React from 'react'
import { withRouter } from 'react-router-dom'
import QuestionCreationCard from '../../components/QuestionCreationCard'
import getQueryParams from '../../utils/getQueryParams'

const QuestionsToCreateList = ({
  location,
  questions,
  unsetMandatory,
  unsetPicture,
  allToMandatory,
  allToPicture
}) => {
  const { section } = getQueryParams(location)

  let questionIndexInSection = 0
  return (
    <React.Fragment>
      {questions.map((question, index) => {
        if (section && question.displayOn !== section) {
          return null
        }
        return (
          <QuestionCreationCard
            key={index}
            questionIndex={index}
            questionIndexInSection={questionIndexInSection++}
            question={question}
            mandatory={question.required}
            unsetMandatory={unsetMandatory}
            allToMandatory={allToMandatory}
            unsetPicture={unsetPicture}
            allToPicture={allToPicture}
          />
        )
      })}
    </React.Fragment>
  )
}

export default withRouter(QuestionsToCreateList)
