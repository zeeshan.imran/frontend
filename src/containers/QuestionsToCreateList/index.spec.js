import React from 'react'
import wait from '../../utils/testUtils/waait'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { mount } from 'enzyme'
import QuestionsToCreateList from './index'
import '../../utils/internationalization/i18n'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { UPDATE_SURVEY_CREATION_QUESTIONS } from '.'
import QuestionCreationCard from '../../components/QuestionCreationCard'
import { Router } from 'react-router-dom'
import history from '../../history'
import surveyCreationDefaults from '../../defaults/surveyCreation'
import sinon from 'sinon'

jest.mock('../../components/QuestionCreationCard', () => () => null)

const mockSurveyCreation = {
  questions: [
    {
      type: 'vertical-rating',
      range: {
        labels: ['1', '2']
      }
    },
    {
      type: 'vertical-rating',
      range: {
        labels: ['1', '3']
      }
    }
  ]
}

describe('QuestionCreation List', () => {
  let testRender
  let mockClient
  let allToMandatory
  let changeQuestionRequredValues
  let setChangeQuestionRequredValues
  let setQuestions

  beforeEach(() => {
    setChangeQuestionRequredValues = jest.fn()
    setQuestions = jest.fn()
    changeQuestionRequredValues = false
    allToMandatory = false
  })
  let spy = jest.fn()

  afterEach(() => {
    testRender.unmount()
  })

  test('update SkipTo values when adding or deleting a question', async () => {
    const initialSkipToOptions = [
      {
        type: 'vertical-rating',
        range: {
          labels: [],
          skipTo: 1
        },
        options: [
          {
            skipTo: 1
          },
          {
            skipTo: 4
          },
          {
            skipTo: 0
          },
          {
            skipTo: 3
          }
        ],
        productsSkip: [
          {
            skipTo: 1
          },
          {
            skipTo: 4
          },
          {
            skipTo: 0
          },
          {
            skipTo: 3
          }
        ]
      },
      {
        type: 'vertical-rating'
      },
      {
        type: 'vertical-rating'
      },
      {
        type: 'vertical-rating'
      }
    ]
    const updatedSkipToOptions = [
      {
        type: 'vertical-rating',
        range: {
          labels: [],
          skipTo: 2
        },
        options: [
          {
            skipTo: 1
          },
          {
            skipTo: 4
          },
          {
            skipTo: 0
          },
          {
            skipTo: 3
          }
        ],
        productsSkip: [
          {
            skipTo: 1
          },
          {
            skipTo: 4
          },
          {
            skipTo: 0
          },
          {
            skipTo: 3
          }
        ]
      },
      {
        prompt: 'smthh',
        type: 'vertical-rating'
      },
      {
        prompt: 'smth',
        type: 'vertical-rating'
      },
      {
        type: 'vertical-rating'
      },
      {
        type: 'vertical-rating'
      }
    ]
    const expectedSkipToOptions = [
      {
        type: 'vertical-rating',
        range: {
          labels: [],
          skipTo: 2
        },
        options: [
          {
            skipTo: 2
          },
          {
            skipTo: 5
          },
          {
            skipTo: 0
          },
          {
            skipTo: 4
          }
        ],
        productsSkip: [
          {
            skipTo: 2
          },
          {
            skipTo: 5
          },
          {
            skipTo: 0
          },
          {
            skipTo: 4
          }
        ]
      },
      {
        prompt: 'smthh',
        type: 'vertical-rating'
      },
      {
        prompt: 'smth',
        type: 'vertical-rating'
      },
      {
        type: 'vertical-rating'
      },
      {
        type: 'vertical-rating'
      }
    ]

    mockClient = createApolloMockClient()

    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          ...surveyCreationDefaults.surveyCreation,
          questions: initialSkipToOptions
        }
      }
    })

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <QuestionsToCreateList
            allToMandatory={allToMandatory}
            changeQuestionRequredValues={changeQuestionRequredValues}
            setChangeQuestionRequredValues={setChangeQuestionRequredValues}
            setParentQuestions={setQuestions}
            questions={initialSkipToOptions}
          />
        </Router>
      </ApolloProvider>
    )
    const stub = sinon.stub(mockClient, 'mutate')
    stub
      .withArgs(sinon.match({ mutation: UPDATE_SURVEY_CREATION_QUESTIONS }))
      .callsFake(spy())
    stub.callThrough()
    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          ...surveyCreationDefaults.surveyCreation,
          questions: updatedSkipToOptions
        }
      }
    })
    testRender.update()
    await wait(0)
    expect(spy).toHaveBeenCalled()
  })

  test('filter questions from current section', () => {
    const mockedHistory = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '', search: `?section=screening` }
    }
    const questions = [
      {
        type: 'vertical-rating',
        displayOn: 'begin'
      },
      {
        type: 'vertical-rating',
        displayOn: 'screening'
      },
      {
        type: 'vertical-rating',
        displayOn: 'screening'
      },
      {
        type: 'vertical-rating',
        displayOn: 'screening'
      }
    ]
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: {
          ...surveyCreationDefaults.surveyCreation,
          questions: questions
        }
      }
    })
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={mockedHistory}>
          <QuestionsToCreateList
            allToMandatory={allToMandatory}
            changeQuestionRequredValues={changeQuestionRequredValues}
            setChangeQuestionRequredValues={setChangeQuestionRequredValues}
            setParentQuestions={setQuestions}
            questions={questions}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(QuestionCreationCard)).toHaveLength(3)
  })
})
