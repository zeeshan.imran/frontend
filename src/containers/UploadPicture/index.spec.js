import React from 'react'
import { mount } from 'enzyme'
import UploadPicture from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import UploadPictureComponent from '../../components/UploadPicture'

import axios from 'axios'
import survey from '../../defaults/surveyCreation'
import { displayErrorPopup } from '../../utils/displayErrorPopup/index'
import gql from 'graphql-tag'
import wait from '../../utils/testUtils/waait'
import { act } from 'react-dom/test-utils'
jest.mock('axios')
axios.put.mockImplementation(() => {
  return { status: 200 }
})
jest.mock('../../utils/displayErrorPopup/index')

jest.mock('browser-image-compression', () => ({
  __esModule: true,
  default: async (file, option) => file
}))

const mockSurveyCreation = {
  ...survey.surveyCreation,
  type: 'SurveyCreation',
  questions: [
    {
      type: 'vertical-rating',
      range: {
        labels: ['1', '2']
      }
    },
    {
      type: 'vertical-rating',
      range: {
        labels: ['1', '3']
      }
    }
  ]
}
describe('UploadPicture', () => {
  let testRender
  let onChange
  let value
  let rest
  let mockClient
  let mockedQueris
  beforeEach(() => {
    onChange = jest.fn()
    value = ''
    rest = {}
    mockedQueris = [
      {
        request: {
          query: GET_SIGNED_URL,
          variables: { type: 'image/png' }
        },
        result: {
          data: {
            getSignedUrl: { signedUrl: 'rigned url', fileLink: 'file link' }
          }
        }
      },
      {
        request: {
          query: SURVEY_CREATION
        },
        result: {
          data: {
            surveyCreation: mockSurveyCreation
          }
        }
      }
    ]
    mockClient = createApolloMockClient({ mocks: mockedQueris })
  })
  afterEach(() => {
    testRender.unmount()
  })
  test('should render UploadPicture', async () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <UploadPicture onChange={onChange} value={value} rest={rest} />
      </ApolloProvider>
    )
    expect(testRender.find(UploadPicture)).toHaveLength(1)
  })
  test('should handle before upload', () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <UploadPicture onChange={onChange} value={value} rest={rest} />
      </ApolloProvider>
    )
    var file = { type: 'image/png' }
    act(() => {
      testRender
        .find(UploadPictureComponent)
        .props()
        .onFileUpload({ file })
    })
  })
  test('should fail', async () => {
    const displayError = jest.fn()
    displayErrorPopup.mockImplementation(displayError)

    axios.put.mockImplementationOnce(() => {
      return { status: 'not 200' }
    })
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <UploadPicture onChange={onChange} value={value} rest={rest} />
      </ApolloProvider>
    )
    var file = { type: 'image/png' }
    act(() => {
      testRender
        .find(UploadPictureComponent)
        .props()
        .onFileUpload({ file })
    })
    await wait(0)
  })
})
const GET_SIGNED_URL = gql`
  query getSignedUrl($type: String) {
    getSignedUrl(type: $type) {
      signedUrl
      fileLink
    }
  }
`
