import React, { useState, useEffect, useRef } from 'react'
import { Button, Icon, Dropdown } from 'antd'
import { StyledModal, StyledButton } from './styles'
import StatsOptionsForm from '../../components/StatsOptionsForm'
import { withTranslation } from 'react-i18next'
import ChartDownloadMenu from '../../components/ChartDownloadMenu'

const StatsOptions = ({
  chartSettings,
  saveChartsSettings,
  chartType,
  question_with_products,
  questionType,
  question_id,
  question,
  chartName,
  isOwner,
  t,
  chartId
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [settings, setSettings] = useState({})
  const chartSettingsRef = useRef()

  useEffect(() => {
    if (chartSettingsRef.current !== chartSettings) {
      setSettings(chartSettings)
      chartSettingsRef.current = chartSettings
    }
  })

  const setNewSettings = newSettings => {
    setSettings({ ...settings, ...newSettings })
  }

  const saveSettings = tag => {
    saveChartsSettings(`${question}-${chartType}`, settings, tag)
    setIsModalVisible(false)
  }
  return (
    <React.Fragment>
      {isOwner && (
        <Button
          type='secondary'
          size='default'
          onClick={() => {
            setIsModalVisible(true)
          }}
        >
          <Icon type='setting' />
        </Button>
      )}
      <Dropdown
        overlay={ChartDownloadMenu(chartId, chartName, t)}
        trigger={['click']}
      >
        <StyledButton type='secondary'>
          <Icon type='bars' />
        </StyledButton>
      </Dropdown>

      <StyledModal
        title={t('settings.charts.settingsPopupTitle', { chartName })}
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false)
        }}
        footer={[
          <Button
            key='0'
            onClick={() => {
              setIsModalVisible(false)
            }}
          >
            {t('settings.charts.cancel')}
          </Button>,
          <Button
            key='1'
            type='primary'
            onClick={() => {
              saveSettings(chartType)
            }}
          >
            {t('settings.charts.setSimilar')}
          </Button>,
          <Button
            key='2'
            type='primary'
            onClick={() => {
              saveSettings()
            }}
          >
            {t('settings.charts.set')}
          </Button>
        ]}
      >
        <StatsOptionsForm
          settings={settings}
          setSettings={setNewSettings}
          chartType={chartType}
          questionType={questionType}
          hasProducts={question_with_products}
        />
      </StyledModal>
    </React.Fragment>
  )
}

export default withTranslation()(StatsOptions)
