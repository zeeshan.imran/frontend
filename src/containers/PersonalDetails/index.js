import React, { Component } from 'react'
import Composer from 'react-composer'
import { Desktop } from '../../components/Responsive'
import PersonalDetailsForm from '../../components/PersonalDetailsForm'
import { Query, Mutation } from 'react-apollo'
import GENDERS_QUERY from '../../queries/genders'
import LANGUAGES_QUERY from '../../queries/languages'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { UPLOAD_PICTURE_MUTATION } from '../../mutations/uploadPicture'
import { withTranslation } from 'react-i18next'
class PersonalDetails extends Component {
  state = {
    imageHash: Date.now()
  }

  handleSubmit = (e, form) => {
    e.preventDefault()

    const {
      user: { id },
      updateUser,
      updateTesterInfo,
      t
    } = this.props
    const fieldsChanged =
      form.isFieldsTouched() || !!form.getFieldValue('picture')

    if (!fieldsChanged) return

    form.validateFields(async (err, values) => {
      if (!err) {
        const updateUserData = {
          id,
          ...(!!form.isFieldTouched('firstname') && {
            firstname: values.firstname
          }),
          ...(!!form.isFieldTouched('lastname') && {
            lastname: values.lastname
          }),
          ...(!!form.isFieldTouched('email') && { email: values.email }),
          ...(!!form.isFieldTouched('language') && {
            languageId: values.language
          })
        }

        const updateTesterInfoData = {
          id,
          ...(!!form.isFieldTouched('gender') && { genderId: values.gender }),
          ...(!!form.isFieldTouched('birthday') && {
            birthday: values.birthday
          }),
          ...(!!form.getFieldValue('picture') && { photo: values.picture })
        }

        const shouldUpdateUser = Object.keys(updateUserData).length > 1

        const shouldUpdateTesterInfo =
          Object.keys(updateTesterInfoData).length > 1

        if (shouldUpdateUser) {
          try {
            const {
              data: {
                updateUser: { ok, error: updateUserError }
              }
            } = await updateUser({ variables: updateUserData })
            if (!ok) {
              return displayErrorPopup(
                t('error.update', { error: updateUserError })
              )
            }
          } catch (error) {
            return displayErrorPopup(t('containers.personalDetails.error'))
          }
        }

        if (shouldUpdateTesterInfo) {
          try {
            const {
              data: {
                updateTesterInfo: {
                  ok: updateTesterInfoOk,
                  error: updateTesterInfoError
                }
              }
            } = await updateTesterInfo({ variables: updateTesterInfoData })
            if (!updateTesterInfoOk) {
              return displayErrorPopup(
                t('error.update', { error: updateTesterInfoError })
              )
            } else {
              this.setState({ imageHash: Date.now() }) // This is used to fix the image not reloading after a user uploads a picture
            }
          } catch (error) {
            return displayErrorPopup(t('containers.personalDetails.error'))
          }
        }
      }
    })
    return displaySuccessMessage(t('containers.personalDetails.success'))
  }

  render () {
    const {
      user: {
        id,
        firstname,
        lastname,
        email,
        language,
        testerInfo: { birthday, genderId }
      }
    } = this.props
    const { imageHash } = this.state

    const pictureLink =
      `${
        process.env.REACT_APP_FLAVORWIKI_API_URL
      }/photo/testerInfo/${id}?imageHash=` + imageHash

    return (
      <Composer
        components={[
          ({ render }) => <Desktop children={render} />,
          ({ render }) => (
            <Query
              query={GENDERS_QUERY}
              fetchPolicy='cache-and-network'
              children={render}
            />
          ),
          ({ render }) => (
            <Query
              query={LANGUAGES_QUERY}
              fetchPolicy='cache-and-network'
              children={render}
            />
          ),
          ({ render }) => (
            <Mutation mutation={UPLOAD_PICTURE_MUTATION} children={render} />
          )
        ]}
      >
        {([
          desktop,
          {
            data: { genders },
            loading: loadingGenders
          },
          {
            data: { languages },
            loading: loadingLanguages
          },
          uploadPicture
        ]) => {
          return (
            <PersonalDetailsForm
              desktop={desktop}
              submitForm={this.handleSubmit}
              imageUrl={pictureLink}
              firstname={firstname}
              lastname={lastname}
              email={email}
              birthday={birthday}
              gender={genderId}
              language={language && language.id}
              genders={genders}
              languages={languages}
            />
          )
        }}
      </Composer>
    )
  }
}

export default withTranslation()(PersonalDetails)
export { PersonalDetails as PersonalDetailsWrapped }
