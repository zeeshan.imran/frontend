import React from 'react'
import { mount } from 'enzyme'
import { PersonalDetailsWrapped } from './index'
import { MockedProvider } from 'react-apollo/test-utils'
import GENDERS_QUERY from '../../queries/genders'
import LANGUAGES_QUERY from '../../queries/languages'
import { UPLOAD_PICTURE_MUTATION } from '../../mutations/uploadPicture'
import wait from '../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PersonalDetails', () => {
  let testRender
  let user
  let mocks
  let handleUpdateUser
  let handleUpdateTesterInfo
  let t

  function fillForm () {
    testRender
      .find({ id: 'firstname' })
      .first()
      .prop('onChange')({ target: { value: 'Lorem' } })

    testRender
      .find({ id: 'lastname' })
      .first()
      .prop('onChange')({ target: { value: 'Ipsum' } })

    testRender
      .find({ id: 'email' })
      .first()
      .prop('onChange')({ target: { value: 'test@flavorwiki.com' } })

    testRender
      .find({ id: 'language' })
      .first()
      .prop('onChange')({ target: { value: 'en' } })

    testRender
      .find({ id: 'gender' })
      .first()
      .prop('onChange')({ target: { value: 'F' } })
    //
    testRender
      .find({ id: 'birthday' })
      .first()
      .prop('onChange')({
      target: { value: new Date('1980-01-01') }
    })
  }

  beforeAll(() => {
    global.Date.now = () => 1546300800000
  })

  beforeEach(() => {
    t = jest.fn(key => key)

    handleUpdateUser = {
      ok: jest.fn(() => ({
        data: { updateUser: { ok: true } }
      })),
      error: jest.fn(() => ({
        data: { updateUser: { error: 'ERROR on updateUser' } }
      })),
      throw: jest.fn(async () => {
        throw 'Error'
      })
    }

    handleUpdateTesterInfo = {
      ok: jest.fn(() => ({
        data: { updateTesterInfo: { ok: true } }
      })),
      error: jest.fn(() => ({
        data: { updateTesterInfo: { error: 'ERROR on updateTesterInfo' } }
      })),
      throw: jest.fn(() => {
        throw 'Error'
      })
    }

    user = {
      id: 'user-1',
      firstname: 'Test',
      lastname: 'User',
      email: 'test@gmail.com',
      language: 'en',
      testerInfo: {
        ethnicityId: 'ethnicity-id',
        incomeRangeId: 'income-range-id',
        incomeRange: {
          id: 'income-range-id',
          currency: '$'
        },
        childrenAges: '10,15,30',
        smokerKindId: 'smoker-kind-id',
        preferredProductCategories: [],
        dietaryPreferenceId: 'dietary-preference-id'
      }
    }
    mocks = [
      {
        request: { query: GENDERS_QUERY },
        result: {
          data: {
            genders: []
          }
        }
      },
      {
        request: { query: LANGUAGES_QUERY },
        result: { data: { languages: [] } }
      },
      {
        request: {
          query: UPLOAD_PICTURE_MUTATION,
          variables: {
            id: 'cat-1',
            photo: 'cat-1'
          }
        },
        result: {
          data: { ok: '', error: '' }
        }
      }
    ]
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PersonalDetails', () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <PersonalDetailsWrapped t={t} desktop={''} user={user} />
      </MockedProvider>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('skip process when nothing is touched', () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <PersonalDetailsWrapped t={t} desktop={''} user={user} />
      </MockedProvider>
    )
    testRender.find('form').simulate('submit')
  })

  test('should call handleUpdateUser, handleUpdateTesterInfo when the user touched fields', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <PersonalDetailsWrapped
          t={t}
          desktop={''}
          user={user}
          updateUser={handleUpdateUser.ok}
          updateTesterInfo={handleUpdateTesterInfo.ok}
        />
      </MockedProvider>
    )

    fillForm()
    testRender.find('form').simulate('submit')

    await wait(0)

    expect(handleUpdateUser.ok).toHaveBeenCalledWith({
      variables: {
        email: 'test@flavorwiki.com',
        firstname: 'Lorem',
        id: 'user-1',
        languageId: 'en',
        lastname: 'Ipsum'
      }
    })
    expect(handleUpdateTesterInfo.ok).toHaveBeenCalledWith({
      variables: {
        birthday: new Date('1980-01-01'),
        genderId: 'F',
        id: 'user-1'
      }
    })
  })

  test('should display error when updateUser returns error', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <PersonalDetailsWrapped
          t={t}
          desktop={''}
          user={user}
          updateUser={handleUpdateUser.error}
          updateTesterInfo={handleUpdateTesterInfo.ok}
        />
      </MockedProvider>
    )
    fillForm()
    testRender.find('form').simulate('submit')

    await wait(0)

    expect(handleUpdateUser.error).toHaveBeenCalledWith({
      variables: {
        email: 'test@flavorwiki.com',
        firstname: 'Lorem',
        id: 'user-1',
        languageId: 'en',
        lastname: 'Ipsum'
      }
    })
    expect(handleUpdateUser.ok).not.toHaveBeenCalled()
    expect(t).toHaveBeenCalledWith('error.update', {
      error: 'ERROR on updateUser'
    })
  })

  test('should display error when handleUpdateTesterInfo returns error', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <PersonalDetailsWrapped
          t={t}
          desktop={''}
          user={user}
          updateUser={handleUpdateUser.ok}
          updateTesterInfo={handleUpdateTesterInfo.error}
        />
      </MockedProvider>
    )
    fillForm()
    testRender.find('form').simulate('submit')

    await wait(0)

    expect(handleUpdateUser.ok).toHaveBeenCalledWith({
      variables: {
        email: 'test@flavorwiki.com',
        firstname: 'Lorem',
        id: 'user-1',
        languageId: 'en',
        lastname: 'Ipsum'
      }
    })
    expect(handleUpdateTesterInfo.error).toHaveBeenCalledWith({
      variables: {
        birthday: new Date('1980-01-01'),
        genderId: 'F',
        id: 'user-1'
      }
    })
    expect(t).toHaveBeenCalledWith('error.update', {
      error: 'ERROR on updateTesterInfo'
    })
  })

  test('should display error when updateUser throws network error', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <PersonalDetailsWrapped
          t={t}
          desktop={''}
          user={user}
          updateUser={handleUpdateUser.throw}
          updateTesterInfo={handleUpdateTesterInfo.ok}
        />
      </MockedProvider>
    )
    fillForm()
    testRender.find('form').simulate('submit')

    await wait(0)

    expect(handleUpdateUser.throw).toHaveBeenCalledWith({
      variables: {
        email: 'test@flavorwiki.com',
        firstname: 'Lorem',
        id: 'user-1',
        languageId: 'en',
        lastname: 'Ipsum'
      }
    })
    expect(handleUpdateUser.ok).not.toHaveBeenCalled()
    expect(t).toHaveBeenCalledWith('containers.personalDetails.error')
  })

  test('should display error when handleUpdateTesterInfo throws network error', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <PersonalDetailsWrapped
          t={t}
          desktop={''}
          user={user}
          updateUser={handleUpdateUser.ok}
          updateTesterInfo={handleUpdateTesterInfo.throw}
        />
      </MockedProvider>
    )
    fillForm()
    testRender.find('form').simulate('submit')

    await wait(0)

    expect(handleUpdateUser.ok).toHaveBeenCalledWith({
      variables: {
        email: 'test@flavorwiki.com',
        firstname: 'Lorem',
        id: 'user-1',
        languageId: 'en',
        lastname: 'Ipsum'
      }
    })
    expect(handleUpdateTesterInfo.throw).toHaveBeenCalledWith({
      variables: {
        birthday: new Date('1980-01-01'),
        genderId: 'F',
        id: 'user-1'
      }
    })
    expect(t).toHaveBeenCalledWith('containers.personalDetails.error')
  })
})
