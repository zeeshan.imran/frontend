import React, { useState, useEffect, useMemo } from 'react'
import { flatten, pipe } from 'ramda'
import { withRouter } from 'react-router-dom'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { useTranslation } from 'react-i18next'
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import EditPdfLayoutComponent from '../../components/EditPdfLayout'
import { getColWidth, getRowHeight, PAPER_SIZES } from '../../utils/paperSizes'
import * as pdfUtils from '../../utils/pdfUtils'
import useChartResults from '../../hooks/useChartResults'
import useRenderingState from '../../hooks/useRenderingState'
import createPageItems from './createPageItems'
import calculatePagesLayout from './calculatePagesLayout'
import { usePdfLayout } from '../../hooks/usePdfLayout'
import AlertModal from '../../components/AlertModal'
import { message } from 'antd'

const flatternPages = (pages, colsPerPage) =>
  pipe(
    pages =>
      pages.map((elementInPageLayouts, page) =>
        elementInPageLayouts.map(e => ({
          ...e,
          x: e.x + page * colsPerPage
        }))
      ),
    flatten
  )(pages)

const compareItems = (a, b) =>
  a.x > b.x || (a.x === b.x && a.y > b.y) ? 1 : -1
const isSimilar = (pages_1, pages_2) => {
  if (pages_1.length !== pages_2.length) {
    return false
  }

  return pages_1.every((items_1, index) => {
    const items_2 = [...pages_2[index]].sort(compareItems)
    if ((items_1 || []).length !== (items_2 || []).length) {
      return false
    }

    return [...items_1].sort(compareItems).every((item_1, index) => {
      const item_2 = items_2[index]

      return ['i', 'x', 'y', 'w', 'h'].every(
        name => item_1[name] === item_2[name]
      )
    })
  })
}

// # http://localhost:3000/operator/stats/5e889970e206f96c186e0305/demographics
const EditPdfLayout = ({ history, templateName, jobGroupId, surveyId }) => {
  const {
    stats,
    settings,
    operatorLayout,
    tasterLayout,
    loading,
    reloadChartResults
  } = useChartResults({
    surveyId,
    jobGroupId
  })
  const { t } = useTranslation()
  const loadedLayout = useMemo(
    () => (templateName === 'operator' ? operatorLayout : tasterLayout),
    [templateName, operatorLayout, tasterLayout]
  )
  const getDownloadLink = useMutation(GET_DOWNLOAD_LINK)
  const saveSettings = useMutation(SAVE_SETTINGS)
  const { rendering, showRendering } = useRenderingState()
  const [paperSize, setPaperSize] = useState(PAPER_SIZES.A4)
  const [pageItems, setPageItems] = useState([])
  const [gridLayout, setGridLayout] = useState([])
  const [ready, setReady] = useState(false)
  const [showLayoutBorder, setShowLayoutBorder] = useState(true)
  const [userZoom, setUserZoom] = useState(0.5)
  const zoom = useMemo(() => {
    const colWidth = getColWidth(paperSize) * userZoom
    return (userZoom * Math.round(colWidth)) / colWidth
  }, [paperSize, userZoom])
  const colWidth = Math.round(getColWidth(paperSize) * zoom)
  const rowHeight = getRowHeight(paperSize) * zoom
  const [elementItems, setElementItems] = useState([])
  const colsPerPage = paperSize.colsBetweenPages + paperSize.cols

  const { fixLayout, fitGridItem } = usePdfLayout()

  useEffect(() => {
    if (!ready && !loading) {
      const elementItems = pdfUtils.createElementItems(
        stats,
        settings,
        templateName
      )

      const newPaperSize = PAPER_SIZES[loadedLayout ? loadedLayout.paper : 'A4']
      const pages = loadedLayout
        ? loadedLayout.pages
        : pdfUtils.calculateElementsLayout(newPaperSize, elementItems)

      const pageItems = createPageItems(t, pages.length)
      const pagesLayout = calculatePagesLayout(newPaperSize, pages.length)

      const gridLayout = [...pagesLayout, ...flatternPages(pages, colsPerPage)]

      setPaperSize(newPaperSize)
      setPageItems(pageItems)
      setElementItems(elementItems)
      setGridLayout(gridLayout)
      fixLayout(elementItems, gridLayout, newPaperSize, zoom)

      setReady(true)
    }
  }, [
    t,
    zoom,
    templateName,
    colsPerPage,
    loading,
    stats,
    settings,
    loadedLayout,
    ready,
    fixLayout
  ])

  useEffect(
    () => {
      fixLayout(elementItems, gridLayout, paperSize, zoom)
      showRendering(1000)
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [zoom, showRendering, fixLayout, elementItems]
  )

  useEffect(() => {
    if (!loading && !ready) {
      showRendering(1000)
    }
  }, [loading, ready, showRendering])

  const handleDownload = async layoutData => {
    try {
      const {
        data: { downloadFile }
      } = await getDownloadLink({
        variables: {
          surveyId,
          jobGroupId,
          templateName,
          layout: { paper: paperSize.name, pages: layoutData }
        }
      })

      const url = `${process.env.REACT_APP_BACKEND_API_URL}/pdf/${downloadFile}`
      window.open(url, '_self')
    } catch (ex) {
      message.error(t('pdfLayoutEditor.messages.downloadError'))
    }
  }

  const handleTemplateSave = async layoutData => {
    try {
      const targetProp =
        templateName === 'operator' ? 'operatorPdfLayout' : 'tasterPdfLayout'
      await saveSettings({
        variables: {
          surveyId,
          [targetProp]: { paper: paperSize.name, pages: layoutData }
        }
      })
      await reloadChartResults()
      message.success(t('pdfLayoutEditor.messages.saveSuccess'))
    } catch (ex) {
      message.error(t('pdfLayoutEditor.messages.saveError'))
    }
  }

  const handleClose = async layoutData => {
    const savedPages = layoutData
    let initPages = []
    if (!loadedLayout) {
      initPages = pdfUtils.calculateElementsLayout(paperSize, elementItems)
    } else {
      initPages = loadedLayout.pages
    }
    if (!isSimilar(initPages, savedPages)) {
      AlertModal({
        title: t('pdfLayoutEditor.discard.title'),
        description: t('pdfLayoutEditor.discard.description'),
        okText: t('pdfLayoutEditor.discard.ok'),
        handleOk: async () => {
          history.goBack()
        },
        handleCancel: () => {}
      })
    } else {
      history.goBack()
    }
  }

  return (
    <EditPdfLayoutComponent
      loading={loading}
      rendering={rendering}
      ready={ready}
      //
      templateName={templateName}
      paperSize={paperSize}
      onPaperSizeChange={name => {
        setPaperSize(PAPER_SIZES[name])
      }}
      colsPerPage={colsPerPage}
      colWidth={colWidth}
      rowHeight={rowHeight}
      zoom={userZoom}
      onZoom={setUserZoom}
      showLayoutBorder={showLayoutBorder}
      onShowLayoutBorderChange={setShowLayoutBorder}
      onDownload={handleDownload}
      onTemplateSave={handleTemplateSave}
      onClose={handleClose}
      //
      pages={pageItems.length}
      pageItems={pageItems}
      elementItems={elementItems}
      gridLayout={gridLayout}
      onDeletePage={page => {
        const newGridLayout = gridLayout.map(gridItem => {
          if (gridItem.x > page * colsPerPage) {
            return { ...gridItem, x: gridItem.x - colsPerPage }
          }
          return gridItem
        })

        const newPageCount = pageItems.length - 1
        setPageItems(createPageItems(t, newPageCount))
        setGridLayout([
          ...calculatePagesLayout(paperSize, newPageCount),
          ...newGridLayout
        ])
      }}
      onAddPage={page => {
        const newGridLayout = gridLayout.map(gridItem => {
          if (gridItem.x > (page + 1) * colsPerPage) {
            return { ...gridItem, x: gridItem.x + colsPerPage }
          }
          return gridItem
        })

        const newPageCount = pageItems.length + 1
        setPageItems(createPageItems(t, newPageCount))
        setGridLayout([
          ...calculatePagesLayout(paperSize, newPageCount),
          ...newGridLayout
        ])
      }}
      onLayoutResizeStop={gridItem => {
        const elementItem = elementItems.find(item => item.i === gridItem.i)
        if (elementItem) {
          fitGridItem(elementItem, gridItem, { colWidth, rowHeight })
        }
      }}
      onLayoutChange={setGridLayout}
    />
  )
}

const GET_DOWNLOAD_LINK = gql`
  mutation getDownloadLink(
    $surveyId: ID!
    $jobGroupId: ID!
    $templateName: TemplateName!
    $layout: JSON
  ) {
    downloadFile: getDownloadLink(
      surveyId: $surveyId
      jobGroupId: $jobGroupId
      templateName: $templateName
      layout: $layout
    )
  }
`

const SAVE_SETTINGS = gql`
  mutation saveChartsSettings(
    $surveyId: ID
    $operatorPdfLayout: JSON
    $tasterPdfLayout: JSON
  ) {
    saveChartsSettings(
      surveyId: $surveyId
      operatorPdfLayout: $operatorPdfLayout
      tasterPdfLayout: $tasterPdfLayout
    )
  }
`

export default withRouter(EditPdfLayout)
