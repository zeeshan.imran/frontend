import React from 'react'
import { range } from 'ramda'
import { Typography, Button, Icon, Tooltip } from 'antd'
import { PageInfo } from '../../components/EditPdfLayout/styles'

const createPageItems = (t, pageCount) => {
  return range(0, pageCount).map(page => ({
    i: `end-of-page-${page}-${pageCount}`,
    page,
    content: ({ canDelete, onAddPage, onDeletePage }) => (
      <PageInfo>
        <Typography.Text type='secondary'>
          {t(`pdfLayoutEditor.pageText`, { page: page + 1, total: pageCount })}
        </Typography.Text>
        <Tooltip title={t('pdfLayoutEditor.addPage.tooltip')}>
          <Button
            shape='circle'
            type='primary'
            size='small'
            onClick={() => onAddPage(page)}
          >
            <Icon type='file' />
          </Button>
        </Tooltip>
        <Tooltip
          title={
            canDelete
              ? t('pdfLayoutEditor.removePage.tooltip')
              : t('pdfLayoutEditor.removePage.disabledTooltip')
          }
        >
          <Button
            shape='circle'
            type='danger'
            size='small'
            disabled={!canDelete}
            onClick={() => onDeletePage(page)}
          >
            <Icon type='delete' />
          </Button>
        </Tooltip>
      </PageInfo>
    )
  }))
}

export default createPageItems
