import { range } from 'ramda'

const calculatePagesLayout = (paperSize, pageCount) => {
  return range(0, pageCount).map(page => ({
    i: `end-of-page-${page}-${pageCount}`,
    x: paperSize.cols + page * (paperSize.cols + paperSize.colsBetweenPages),
    y: 0,
    w: paperSize.colsBetweenPages,
    h: paperSize.rows,
    static: true
  }))
}

export default calculatePagesLayout
