import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useToggle from '../../hooks/useToggle/index'
import useUpdateSurvey from '../../hooks/useUpdateSurvey'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import TooltipWrapper from '../../components/TooltipWrapper'

import { ButtonWithMargin } from './styles'
import useSurveyValidate from '../../hooks/useSurveyValidate'
// import { validateProductSchema, validateSurveyBasics } from '../../validates'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'

const UpdateSurveyDraft = ({ history, match }) => {
  const { t } = useTranslation()
  const [publishing, togglePublishing] = useToggle(false)
  const {
    data: { surveyCreation: { products, basics } = {} }
  } = useQuery(SURVEY_CREATION)

  const {
    params: { surveyId }
  } = match
  const updateDraft = useUpdateSurvey({
    surveyId,
    successMessage: t('containers.updateSurveyDraft.successMessage'),
    errorMessage: t('containers.updateSurveyDraft.errorMessage')
  })
  const surveyValidate = useSurveyValidate()
  const submitDisabled = !basics.isScreenerOnly
    ? !basics.uniqueName ||
      products.length === 0 ||
      // products.reduce(
      //   (result, product) =>
      //     result || !validateProductSchema.isValidSync(product),
      //   false
      // ) ||
      // !validateSurveyBasics(true).isValidSync(basics) ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
    : !basics.uniqueName || basics.linkedSurveys.length === 0

  const onUpdateDraft = async () => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails`)
      return
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion
    } = await surveyValidate.validateQuestions()

    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(`./financial?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    if (!isQuestionsValid) {
      history.push(`./questions?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    togglePublishing()
    try {
      await updateDraft(true)
      togglePublishing()
      history.push(`/operator/draft/edit/${surveyId}`)
    } catch (error) {
      togglePublishing()
      displayErrorPopup(error && error.message)
    }
  }

  return (
    <TooltipWrapper
      helperText={t('tooltips.updateSurveyDraft')}
      placement='leftTop'
    >
      <ButtonWithMargin
        type='secondary'
        disabled={submitDisabled}
        size='default'
        onClick={onUpdateDraft}
      >
        {t('containers.updateSurveyDraft.buttonText')}
      </ButtonWithMargin>
      <LoadingModal
        visible={publishing}
        text={t('containers.updateSurveyDraft.updatingText')}
      />
    </TooltipWrapper>
  )
}

export default withRouter(UpdateSurveyDraft)
