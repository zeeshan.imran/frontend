import React from 'react'
import { mount } from 'enzyme'
import OperatorActiveSurveys from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import ActiveSurveysComponent from '../../components/OperatorActiveSurveys'
import { defaultSurvey } from '../../mocks'

const createSurveyCreationMock = secondProduct => ({
  ...defaultSurvey,
  products: [
    {
      name: 'the first product',
      brand: 'the brand'
    },
    secondProduct
  ]
})

describe('OperatorActiveSurveys', () => {
  let testRender
  let history
  let client
  let spyChange

  beforeEach(() => {
    spyChange = jest.fn()

    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    client = createApolloMockClient()

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: '',
          brand: 'the brand'
        })
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render loader', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorActiveSurveys
            history={history}
            match={{ params: { surveyId: 'survey-1' } }}
            location={{ search: '' }}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('should render OperatorActiveSurveys onpage change event', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorActiveSurveys
            history={history}
            match={{ params: { surveyId: 'survey-1' } }}
            location={{ search: '' }}
          />
        </Router>
      </ApolloProvider>
    )

    spyChange.mockReset()
    testRender.find(ActiveSurveysComponent).prop('onChangePage')()
    expect(history.push).toHaveBeenCalledWith('/?page=0')
  })
})
