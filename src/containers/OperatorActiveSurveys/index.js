import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import ActiveSurveysComponent from '../../components/OperatorActiveSurveys'
import useSurveys from '../../hooks/useSurveys'
import useAllOrganizations from '../../hooks/useAllOrganizations'
import getQueryParams from '../../utils/getQueryParams'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'

const OperatorActiveSurveys = ({ location, history, match }) => {
  const { page = 0 } = getQueryParams(location)
  const [searchTerm, setSearchTerm] = useState('')
  const { surveys, total, loading } = useSurveys({
    page,
    state: 'active',
    keyword: searchTerm
  })
  const { organizations } = useAllOrganizations()
  
  return (
    <OperatorPage>
      <OperatorPageContent>
        <ActiveSurveysComponent
          loading={loading}
          handleSearch={setSearchTerm}
          searchTerm={searchTerm}
          surveys={surveys}
          organizations={organizations}
          total={total}
          page={Number(page)}
          onChangePage={pageNumber => {
            history.push(`${match.url}?page=${pageNumber || 0}`)
          }}
        />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default withRouter(OperatorActiveSurveys)
