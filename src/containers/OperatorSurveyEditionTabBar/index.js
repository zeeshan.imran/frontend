import React from 'react'

import PublishEditedSurvey from '../PublishEditedSurvey'
import OperatorSurveyDetailTabBar from '../../components/OperatorSurveyDetailTabBar'
import UpdateSurvey from '../UpdateSurvey'

const OperatorSurveyCreationTabBar = ({ stricted }) => {
  if (stricted) {
    return <OperatorSurveyDetailTabBar actions={<UpdateSurvey />} />
  }
  return <OperatorSurveyDetailTabBar actions={<PublishEditedSurvey />} />
}

export default OperatorSurveyCreationTabBar
