import React from 'react'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { create } from 'react-test-renderer'
import gql from 'graphql-tag'
import { surveyBasicInfo } from '../../fragments/survey'
import SurveySharing from './index'
import { Router } from 'react-router-dom'
import history from '../../history'
import '../../utils/internationalization/i18n'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { displayDraftWarningPopup } from '../../utils/displayDraftWarningPopup'
import defaults from '../../defaults'

jest.doMock('./index.js')
jest.mock('../../utils/displayDraftWarningPopup')

const mockActiveSurvey = {
  __typename: 'Survey',
  name: 'active survey',
  uniqueName: 'uniqueName',
  state: 'active',
  maximumProducts: 1,
  minimumProducts: null,
  surveyLanguage: 'en',
  settings: null,
  authorizationType: null,
  id: `survey-1`,
  surveyEnrollment: 'survey-enrollment-1',
  context: {},
  value: {
    value: 'value-1'
  },
  range: {},
  options: {},
  pairsOptions: {},
  pairs: {},
  productsSkip: {},
  products: [],
  exclusiveTasters: [],
  instructionSteps: [],
  allowRetakes: false,
  forcedAccount: false,
  forcedAccountLocation: `start`,
  isScreenerOnly: false,
  showGeneratePdf: false,
  linkedSurveys: [],
  referralAmount: 5,
  maxProductStatCount: 6,
  savedRewards: [],
  customizeSharingMessage: '',
  loginText: '',
  pauseText: '',
  allowedDaysToFillTheTasting: 5,
  isPaypalSelected: false,
  isGiftCardSelected: false,
  customButtons: {
    continue: null,
    start: null,
    skip: null,
    next: null
  },
  startedAt: Date.now(),
  selectedProduct: 'prod-1',
  country: 'United States of America'
}

const mockDraftSurvey = {
  ...mockActiveSurvey,
  state: 'draft',
  id: 'survey-2',
  name: 'drafted survey'
}

describe('', () => {
  let testRender
  let mockClient

  beforeAll(() => {
    mockClient = createApolloMockClient()

    mockClient.cache.writeQuery({
      query: gql`
        query survey($id: ID) {
          survey(id: "survey-1") {
            ...surveyBasicInfo
          }
        }
        ${surveyBasicInfo}
      `,
      data: {
        survey: {
          ...mockActiveSurvey
        }
      }
    })

    mockClient.cache.writeQuery({
      query: gql`
        query survey($id: ID) {
          survey(id: "survey-2") {
            ...surveyBasicInfo
          }
        }
        ${surveyBasicInfo}
      `,
      data: {
        survey: {
          ...mockDraftSurvey
        }
      }
    })

    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-1',
          surveyEnrollmentId: 'survey-enrollment-1',
          state: 'active',
          canSkipCurrentQuestion: false,
          paypalEmail: '',
          selectedProduct: null,
          selectedProducts: [],
          currentSurveyStep: null,
          currentSurveySection: null,
          isCurrentAnswerValid: true,
          currentAnswerSkipTarget: null,
          answers: [],
          lastAnsweredQuestion: null,
          savedRewards: null
        }
      }
    })

    testRender = create(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveySharing isDesktop />
        </Router>
      </ApolloProvider>
    )
  })

  afterEach(() => {
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-2',
          surveyEnrollmentId: 'survey-enrollment-1',
          state: 'draft',
          canSkipCurrentQuestion: false,
          paypalEmail: '',
          selectedProduct: null,
          selectedProducts: [],
          currentSurveyStep: null,
          currentSurveySection: null,
          isCurrentAnswerValid: true,
          currentAnswerSkipTarget: null,
          answers: [],
          lastAnsweredQuestion: null
        }
      }
    })
    testRender.update(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveySharing isDesktop />
        </Router>
      </ApolloProvider>
    )
  })

  afterAll(() => {
    testRender.unmount()
  })

  test('The active survey should not have a notification popup', () => {
    expect(displayDraftWarningPopup).not.toHaveBeenCalled()
  })
})
