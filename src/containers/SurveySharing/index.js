import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { surveyBasicInfo } from '../../fragments/survey'
import SurveySharingComponent from '../../components/SurveySharing'

const SurveySharing = ({ isDesktop, centerAlign }) => {
  const {
    data: {
      currentSurveyParticipation: { surveyId, surveyEnrollmentId }
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const {
    data: { survey }
  } = useQuery(SURVEY_QUERY, { variables: { id: surveyId } })

  if (survey && surveyEnrollmentId) {
    return (
      <SurveySharingComponent
        isDesktop={isDesktop}
        survey={survey}
        surveyEnrollment={surveyEnrollmentId}
        centerAlign={centerAlign}
      />
    )
  }
  return null
}

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
      screeners {
        id
        uniqueName
        isPaypalSelected
        isGiftCardSelected
        customizeSharingMessage
        referralAmount
      }
    }
  }

  ${surveyBasicInfo}
`
export default SurveySharing
