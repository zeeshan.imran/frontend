import React, { useState, useEffect, useRef } from 'react'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { useMutation } from 'react-apollo-hooks'
import OperatorSurveysComponent from '../../components/OperatorSurveys'

import useSurveys from '../../hooks/useSurveys'
import useAllOrganizations from '../../hooks/useAllOrganizations'
import getQueryParams from '../../utils/getQueryParams'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'

const OperatorSurveys = ({ location, history, match }) => {
  const getSurveyState = {
    Draft: 'draft',
    Suspended: 'suspended',
    Published: 'active'
  }

  const { page = 0 } = getQueryParams(location)
  const [searchTerm, setSearchTerm] = useState('')
  const [surveyState, setSurveyState] = useState('')
  const surveyStateReference = useRef()

  useEffect(() => {
    if (surveyStateReference.current !== surveyState) {
      surveyStateReference.current = surveyState
      history.push(`${match.url}?page=${0}`)
    }
  })
  const { surveys, total, loading } = useSurveys({
    page,
    state: getSurveyState[surveyState],
    keyword: searchTerm
  })

  const { organizations } = useAllOrganizations()
  const resetCreationForm = useMutation(RESET_CREATION_FORM)

  const handleCreateNew = () => {
    resetCreationForm()
    history.push(`/operator/survey/create`)
  }

  return (
    <OperatorPage>
      <OperatorPageContent>
        <OperatorSurveysComponent
          loading={loading}
          surveys={surveys}
          organizations={organizations}
          searchTerm={searchTerm}
          onSearch={setSearchTerm}
          surveyState={surveyState}
          setSurveyState={setSurveyState}
          total={total}
          page={Number(page)}
          onChangePage={pageNumber => {
            history.push(`${match.url}?page=${pageNumber || 0}`)
          }}
          createNew={handleCreateNew}
        />
      </OperatorPageContent>
    </OperatorPage>
  )
}

const RESET_CREATION_FORM = gql`
  mutation resetCreationForm {
    resetCreationForm @client
  }
`

export default withRouter(OperatorSurveys)
