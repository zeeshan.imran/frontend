import * as Yup from 'yup'
import i18n from '../utils/internationalization/i18n'

const settingsSchema = Yup.object().shape({
  active: Yup.boolean(),
  debounce: Yup.mixed().when('active', {
    is: true,
    then: Yup.number()
      .min(0, i18n.t('surveySettings.validations.debounce.min'))
      .max(600, i18n.t('surveySettings.validations.debounce.max'))
      .required(i18n.t('surveySettings.validations.debounce.required'))
      .test(
        'wrong-step',
        i18n.t('surveySettings.validations.debounce.step'),
        value => value % 0.5 <= Number.EPSILON
      )
  })
})

export default settingsSchema
