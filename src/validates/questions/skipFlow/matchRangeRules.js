import * as Yup from 'yup'
import i18n from '../../../utils/internationalization/i18n'

function emptyStringToNull (value, originalValue) {
  if (typeof originalValue === 'string' && originalValue === '') {
    return null
  }
  return value
}

const getIntersectError = function ({ value, usedIn }) {
  return `${value} already used in ${usedIn
    .map(({ minValue, maxValue }) => `(${minValue}, ${maxValue})`)
    .join(', ')}`
}

const testIntersect = rules =>
  function (value) {
    const usedIn = []
    for (var r of rules) {
      if (r !== this.parent) {
        if (r.minValue <= value && value <= r.maxValue) {
          usedIn.push(r)
        }
      }
    }

    if (usedIn.length > 0) {
      return this.createError({
        params: { usedIn }
      })
    }
    return true
  }

const matchRangeRules = range =>
  Yup.lazy(rules => {
    if (!range) {
      return Yup.mixed()
    }

    const { min, max } = range

    return Yup.array().of(
      Yup.object().shape({
        minValue: Yup.number()
          .integer(i18n.t('validation.vertical.range.skipMin.integer'))
          .when([], {
            is: () => !isNaN(parseFloat(min)),
            then: schema =>
              schema.min(
                min,
                i18n.t('validation.vertical.range.skipMin.moreThan', {
                  value: min
                })
              )
          })

          .when('maxValue', {
            is: null,
            otherwise: Yup.number().lessThan(Yup.ref('maxValue'), ({ less }) =>
              i18n.t('validation.vertical.range.skipMin.lessThan', {
                value: less
              })
            )
          })
          .test('intersect-rules', getIntersectError, testIntersect(rules))
          .typeError(i18n.t('validation.vertical.range.skipMin.number'))
          .transform(emptyStringToNull)
          .nullable(),
        maxValue: Yup.number()
          .integer(i18n.t('validation.vertical.range.skipMax.integer'))
          .moreThan(Yup.ref('minValue'), ({ more }) =>
            i18n.t('validation.vertical.range.skipMax.moreThan', {
              value: more
            })
          )
          .when([], {
            is: () => !isNaN(parseFloat(max)),
            then: schema =>
              schema.max(
                max,
                i18n.t('validation.vertical.range.skipMax.lessThan', {
                  value: max
                })
              )
          })
          .test('intersect-rules', getIntersectError, testIntersect(rules))
          .typeError(i18n.t('validation.vertical.range.skipMax.number'))
          .transform(emptyStringToNull)
          .nullable()
      })
    )
  })

export default matchRangeRules
