import * as Yup from 'yup'
import i18n from '../../utils/internationalization/i18n'

export default Yup.object().shape({
  prompt: Yup.string()
    .test(
      'not-empty',
      i18n.t('validation.prompt'),
      value => !/^[ ]+$/.test(value)
    )
    .test(
      'not-empty',
      i18n.t('validation.promptWithoutSpaces'),
      value => /^(?! ).*/.test(value)
    )
    .required(i18n.t('validation.prompt')),
  chooseProductOptions: Yup.object().shape({
    minimumProducts: Yup.number()
      .typeError(i18n.t('validation.chooseProduct.minProducts.integer'))
      .integer(i18n.t('validation.chooseProduct.minProducts.integer'))
      .min(1, ({ min }) =>
        i18n.t('validation.chooseProduct.greaterThan', {
          value: min || 'minimum products'
        })
      )
      .max(Yup.ref('maximumProducts'), ({ max }) =>
        i18n.t('validation.chooseProduct.equalOrLess', {
          value: max || 'number of products'
        })
      )
      .required(i18n.t('validation.chooseProduct.minProducts.required')),
    maximumProducts: Yup.number()
      .typeError(i18n.t('validation.chooseProduct.maxProducts.integer'))
      .integer(i18n.t('validation.chooseProduct.maxProducts.integer'))
      .max(Yup.ref('$products.length'), ({ max }) =>
        i18n.t('validation.chooseProduct.equalOrLess', {
          value: max || 'number of products'
        })
      )
      .min(Yup.ref('minimumProducts'), ({ min }) =>
        i18n.t('validation.chooseProduct.greaterThan', {
          value: min || 'minimum products'
        })
      )
      .required(i18n.t('validation.chooseProduct.maxProducts.required'))
  })
})
