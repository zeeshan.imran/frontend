import validationSchema from './index'
describe('chart section validation', () => {
  const positiveChartSection = {
    chartTitle: 'Chart title',
    chartTopic: 'Chart topic',
    chartType: 'column'
  }
  const options = {
    context: { chartTypes: ['column', 'pie'] }
  }
  test('should valid', async () => {
    expect(
      validationSchema.validate(positiveChartSection, options)
    ).resolves.toBe(positiveChartSection)
  })
  describe('chart title rules', () => {
    test('chart title validation', async () => {
      expect(
        validationSchema.validate(
          {
            ...positiveChartSection,
            chartTitle: ''
          },
          options
        )
      ).rejects.toMatchObject({ errors: ['Chart title is required'] })

      expect(
        validationSchema.validate(
          {
            ...positiveChartSection,
            chartTitle: ' paired question '
          },
          options
        )
      ).rejects.toMatchObject({
        errors: ['Chart title should not start with white space']
      })
    })

    test('chartTopic validation', async () => {
      expect(
        validationSchema.validate(
          {
            ...positiveChartSection,
            chartTopic: ''
          },
          options
        )
      ).rejects.toMatchObject({ errors: ['Chart topic is required'] })

      expect(
        validationSchema.validate(
          {
            ...positiveChartSection,
            chartTopic: ' charttopic'
          },
          options
        )
      ).rejects.toMatchObject({
        errors: ['Chart topic should not start with white space']
      })
    })

    test('chartType validation', async () => {
      expect(
        validationSchema.validate(
          {
            ...positiveChartSection,
            chartType: ''
          },
          options
        )
      ).rejects.toMatchObject({ errors: ['Chart type is required'] })
    })
  })

  test('chartTypes should be null', async () => {
    const optionsnull = {
      context: { chartTypes: [] }
    }

    expect(
      validationSchema.validate(
        {
          ...positiveChartSection,
          chartType: ''
        },
        optionsnull
      )
    ).toMatchObject({})
  })
})
