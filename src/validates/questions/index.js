import { QUESTION_TYPES } from '../../utils/Constants'

const validateQuestionSchemas = QUESTION_TYPES.reduce(
  (acc, name) => ({
    ...acc,
    [name]: require('./' + name).default
  }),
  {}
)

export default validateQuestionSchemas
