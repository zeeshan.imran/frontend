import * as Yup from 'yup'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'

export default Yup.lazy(values =>
  Yup.object()
    .shape({
      prompt: Yup.string()
        .test(
          'not-empty',
          i18n.t('validation.prompt'),
          value => !/^[ ]+$/.test(value)
        )
        .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
          /^(?! ).*/.test(value)
        )
        .required(i18n.t('validation.prompt')),
      settings: Yup.object().shape({
        minLength: Yup.number()
          .integer(i18n.t('validation.tasterName.settings.minLength.typeError'))
          .typeError(i18n.t('validation.tasterName.settings.minLength.typeError'))
          .min(1, i18n.t('validation.tasterName.settings.minLength.min'))
          .when('maxLength', (maxLength, schema) =>
            maxLength
              ? schema.max(
                  Yup.ref('maxLength'),
                  i18n.t('validation.tasterName.settings.minLength.lessThanMax')
                )
              : schema
          ),
        maxLength: Yup.number()
          .integer(i18n.t('validation.tasterName.settings.maxLength.typeError'))
          .typeError(i18n.t('validation.tasterName.settings.maxLength.typeError'))
          .min(1, i18n.t('validation.tasterName.settings.maxLength.min'))
          .min(
            Yup.ref('minLength'),
            i18n.t('validation.tasterName.settings.maxLength.moreThanMin')
          )
          .nullable()
      })
    })
    .concat(chartSection)
)
