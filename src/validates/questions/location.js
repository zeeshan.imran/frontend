import * as Yup from 'yup'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'
import { locationQuestionCountries } from '../../utils/Constants'

export default Yup.object()
  .shape({
    prompt: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.prompt'),
        value => !/^[ ]+$/.test(value)
      )
      .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
        /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.prompt')),
    region: Yup.string()
      .oneOf(locationQuestionCountries, i18n.t('validation.location.region'))
      .required(i18n.t('validation.location.region'))
  })
  .concat(chartSection)
