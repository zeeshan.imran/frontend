import * as Yup from 'yup'
import chartSection from './chart-section'

export default Yup.object().concat(chartSection)
