import validationSchema from './choose-date.js'

describe('choose date validation', () => {
  test('choose date require', () => {
    expect(validationSchema.validate({ prompt: '' })).rejects.toMatchObject({
      errors: ['Question text is required']
    })
  })

  test('choose date prompt without spaces', () => {
    expect(
      validationSchema.validate({ prompt: ' promt' })
    ).rejects.toMatchObject({
      errors: ['Question text should not start with white space']
    })
  })
})
 