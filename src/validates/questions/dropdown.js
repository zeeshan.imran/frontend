import * as Yup from 'yup'
import { uniqBy } from 'ramda'
import { trimSpaces } from '../../utils/trimSpaces'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'

export const minOptions = 2

export default Yup.object()
  .shape({
    prompt: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.dropdown.prompt'),
        value => !/^[ ]+$/.test(value)
      )
      .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
        /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.dropdown.prompt')),
    options: Yup.array()
      .of(
        Yup.object().shape({
          label: Yup.string()
            .test('not-empty', i18n.t('validation.labelWithoutSpaces'), value =>
              /^(?! ).*/.test(value)
            )
            .test(
              'not-empty',
              i18n.t('validation.label'),
              value => !/^[ ]+$/.test(value)
            )
            .required(i18n.t('validation.label')),
          value: Yup.string()
            .test(
              'not-empty',
              i18n.t('validation.analyticsValueWithoutSpaces'),
              value => /^(?! ).*/.test(value)
            )
            .test(
              'not-empty',
              i18n.t('validation.analyticsValue'),
              value => !/^[ ]+$/.test(value)
            )
            .required(i18n.t('validation.analyticsValue')),
          skipTo: Yup.string().nullable()
        })
      )
      .test(
        'uniq-options',
        i18n.t('validation.dropdown.uniqueOptions'),
        value =>
          value &&
          uniqBy(el => trimSpaces(el.value).toLowerCase(), value).length ===
            value.length &&
          uniqBy(el => trimSpaces(el.label).toLowerCase(), value).length ===
            value.length
      )
      .min(minOptions, i18n.t('validation.dropdown.minim'))
  })
  .concat(chartSection)
