import * as Yup from 'yup'
import { filter, uniqBy } from 'ramda'
import { trimSpaces } from '../../utils/trimSpaces'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'

export const minOptions = 2

const getUniqueValues = uniqBy(
  el => el.value && trimSpaces(el.value).toLowerCase()
)
const getUniqueLabels = uniqBy(
  el => el.label && trimSpaces(el.label).toLowerCase()
)
const getClosedAnswers = filter(q => !q.isOpenAnswer)

export default Yup.lazy(values =>
  Yup.object()
    .shape({
      prompt: Yup.string()
        .test(
          'not-empty',
          i18n.t('validation.prompt'),
          value => !/^[ ]+$/.test(value)
        )
        .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
          /^(?! ).*/.test(value)
        )
        .required(i18n.t('validation.prompt')),
      options: Yup.array()
        .of(
          Yup.object().shape({
            label: Yup.string()
              .typeError(i18n.t('validation.label'))
              .test(
                'not-empty',
                i18n.t('validation.labelWithoutSpaces'),
                value => /^(?! ).*/.test(value)
              )
              .test(
                'not-empty',
                i18n.t('validation.label'),
                value => !/^[ ]+$/.test(value)
              )
              .required(i18n.t('validation.label')),
            value: Yup.string().when('isOpenAnswer', {
              is: isOpenAnswer => !isOpenAnswer,
              then: Yup.string()
                // .typeError(i18n.t('validation.analyticsValue'))
                .test(
                  'not-empty',
                  i18n.t('validation.analyticsValueWithoutSpaces'),
                  value => /^(?! ).*/.test(value)
                )
                .test(
                  'not-empty',
                  i18n.t('validation.analyticsValue'),
                  value => !/^[ ]+$/.test(value)
                )
                .test(
                  'is-positive',
                  i18n.t('validation.analyticsValueIsPositive'),
                  value => parseInt(value, 10) > 0
                )
                .required(i18n.t('validation.analyticsValue'))
            })
          })
        )
        .test(
          'uniq-options',
          i18n.t('validation.multiple.uniqueOptions'),
          value => {
            if (!value) return false

            const closeAnswers = getClosedAnswers(value) || []

            if (getUniqueValues(closeAnswers).length !== closeAnswers.length) {
              return false
            }

            if (getUniqueLabels(closeAnswers).length !== closeAnswers.length) {
              return false
            }
            return true
          }
        )
        .min(minOptions, i18n.t('validation.multiple.minim')),
      settings: Yup.object().shape({
        chooseMultiple: Yup.boolean(),
        minAnswerValues: Yup.number()
        .when('chooseMultiple', {
          is: true,
          then: Yup.number()
            .integer(i18n.t('validation.multiple.minAnswerValues.typeError'))
            .typeError(i18n.t('validation.multiple.minAnswerValues.typeError'))
            .min(1, i18n.t('validation.multiple.minAnswerValues.min'))
            .when('maxAnswerValues', (maxAnswerValues, schema) =>
              maxAnswerValues
                ? schema.max(
                    Yup.ref('maxAnswerValues'),
                    i18n.t('validation.multiple.minAnswerValues.lessThanMax')
                  )
                : schema
            )
            .max((values.options && values.options.length) || 1, () =>
              i18n.t('validation.multiple.minAnswerValues.lessThanTotal', {
                total: (values.options && values.options.length) || 1
              })
            ),
          otherwise: Yup.number().nullable()
        }),
        maxAnswerValues: Yup.number()
        .nullable()
        .when('chooseMultiple', {
          is: true,
          then: Yup.number()
            .integer(i18n.t('validation.multiple.maxAnswerValues.typeError'))
            .typeError(i18n.t('validation.multiple.minAnswerValues.typeError'))
            .min(1, i18n.t('validation.multiple.minAnswerValues.min'))
            .min(
              Yup.ref('minAnswerValues'),
              i18n.t('validation.multiple.maxAnswerValues.moreThanMin')
            )
            .max((values.options && values.options.length) || 1, () => {
              return i18n.t(
                'validation.multiple.maxAnswerValues.lessThanTotal',
                {
                  total: (values.options && values.options.length) || 1
                }
              )
            })
            .nullable()
        })
      }),
      matrixOptions: Yup.object().shape({
        question: Yup.array()
          .of(
            Yup.object().shape({
              label: Yup.string()
                .test(
                  'not-empty',
                  i18n.t('validation.slider.range.sliders.withoutSpaces'),
                  value => /^(?! ).*/.test(value)
                )
                .test(
                  'not-empty',
                  i18n.t('validation.slider.range.sliders.required'),
                  value => !/^[ ]+$/.test(value)
                )
                .required(i18n.t('validation.slider.range.sliders.required'))
                .typeError(i18n.t('validation.slider.range.sliders.required'))
            })
          )
          .test(
            'uniq-sliders',
            i18n.t('validation.slider.range.sliders.unique'),
            value =>
              !value ||
              !value.length ||
              uniqBy(
                el =>
                  el.label ? trimSpaces(el.label).toLowerCase() : Math.random(),
                value
              ).length === value.length
          )
          .min(1, i18n.t('validation.slider.range.sliders.atLeast'))
      })
    })
    .concat(chartSection)
)
