import * as Yup from 'yup'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'

export default Yup.object()
  .shape({
    chartType: Yup.string(),
    prompt: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.prompt'),
        value => !/^[ ]+$/.test(value)
      )
      .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
        /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.prompt'))
  })
  .concat(chartSection)
