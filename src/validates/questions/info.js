import * as Yup from 'yup'
import i18n from '../../utils/internationalization/i18n'

export default Yup.object().shape({
  prompt: Yup.string()
    .test(
      'not-empty',
      i18n.t('validation.info.prompt'),
      value => !/^[ ]+$/.test(value)
    )
    .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
      /^(?! ).*/.test(value)
    )
    .required(i18n.t('validation.info.prompt')),
  secondaryPrompt: Yup.string().required(
    i18n.t('validation.info.secondaryPrompt')
  )
})
