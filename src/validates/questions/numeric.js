import * as Yup from 'yup'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'

export default Yup.object()
  .shape({
    prompt: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.prompt'),
        value => !/^[ ]+$/.test(value)
      )
      .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
        /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.prompt')),
    numericOptions: Yup.object().shape({
      min: Yup.number('Minimum must be number')
        .transform(value => (!value && value !== 0 ? null : value))
        .nullable(),
      max: Yup.number('Maximum must be number')
        .transform(value => (!value && value !== 0 ? null : value))
        .moreThan(
          Yup.ref('min'),
          'Maximum number must be greater then Minimum number'
        )
        .nullable()
    })
  })
  .concat(chartSection)
