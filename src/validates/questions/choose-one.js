import * as Yup from 'yup'
import { filter, uniqBy } from 'ramda'
import { trimSpaces } from '../../utils/trimSpaces'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'

export const minOptions = 2

const getUniqueValues = uniqBy(el => trimSpaces(el.value).toLowerCase())
const getUniqueLabels = uniqBy(el => trimSpaces(el.label).toLowerCase())
const getClosedAnswers = filter(q => !q.isOpenAnswer)

export default Yup.object()
  .shape({
    prompt: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.prompt'),
        value => !/^[ ]+$/.test(value)
      )
      .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
        /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.prompt')),
    options: Yup.array()
      .of(
        Yup.object().shape({
          label: Yup.string()
            .test('not-empty', i18n.t('validation.labelWithoutSpaces'), value =>
              /^(?! ).*/.test(value)
            )
            .test(
              'not-empty',
              i18n.t('validation.label'),
              value => !/^[ ]+$/.test(value)
            )
            .required(i18n.t('validation.label')),
          value: Yup.string().when('isOpenAnswer', {
            is: isOpenAnswer => !isOpenAnswer,
            then: Yup.string()
              .test(
                'not-empty',
                i18n.t('validation.analyticsValueWithoutSpaces'),
                value => /^(?! ).*/.test(value)
              )
              .test(
                'not-empty',
                i18n.t('validation.analyticsValue'),
                value => !/^[ ]+$/.test(value)
              )
              .test(
                'is-positive',
                i18n.t('validation.analyticsValueIsPositive'),
                value => parseInt(value, 10) > 0
              )
              .required(i18n.t('validation.analyticsValue'))
          })
        })
      )
      .test('uniq-options', i18n.t('validation.one.uniqueOptions'), value => {
        if (!value) return false

        const closeAnswers = getClosedAnswers(value)
        if (getUniqueValues(closeAnswers).length !== closeAnswers.length) {
          return false
        }

        if (getUniqueLabels(closeAnswers).length !== closeAnswers.length) {
          return false
        }

        return true
      })
      .min(minOptions, i18n.t('validation.one.minim'))
  })
  .concat(chartSection)
