import * as Yup from 'yup'
import i18n from '../utils/internationalization/i18n'
import {
  PRODUCT_STATS_COUNT_MIN,
  PRODUCT_STATS_COUNT_MAX
} from '../utils/Constants'

export default Yup.lazy(values => {
  const maxStat =
    values.totalProductCount < PRODUCT_STATS_COUNT_MAX
      ? values.totalProductCount
      : PRODUCT_STATS_COUNT_MAX
  return Yup.object().shape({
    maxProductStatCount: Yup.number()
      .min(
        PRODUCT_STATS_COUNT_MIN,
        i18n.t('validation.basics.maxProductStatCount.min')
      )
      .max(
        maxStat,
        i18n.t('validation.basics.maxProductStatCount.isMax', { max: maxStat })
      )

      .required(i18n.t('validation.basics.maxProductStatCount.required'))
  })
})
