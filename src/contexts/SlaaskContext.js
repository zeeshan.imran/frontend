import React, { useContext, useMemo } from 'react'
import { getAuthenticatedUser } from '../utils/userAuthentication'
import checkSlaaskLocation from '../utils/checkSlaaskLocation'
const keys = {
  default: 'spk-06e763ca-ff93-44d8-b15d-5ed5f0e8a273',
  // key for special users base on the user type
  operator: 'spk-a7111c16-e1a8-4723-a113-8a8755d69d4d'
}

const SlaaskContext = React.createContext()
export const SlaaskProvider = ({ flag, children, history }) => {
  const { location } = history
  const value = useMemo(() => {
    const reloadSlaask = async () => {
      if (flag !== 'disabled') {
        if (!checkSlaaskLocation(location)) {
          const { _slaask } = window
          const user = getAuthenticatedUser()
          const key = keys[user && user.type] || keys.default

          window._slaaskSettings = {
            key,
            identify: () => {
              if (!user) return {}

              return {
                id: user.id,
                name: user.fullName || user.emailAddress,
                email: user.emailAddress,
                type: user.type
              }
            }
          }

          await _slaask._reloadApp()
        }
      }
    }

    reloadSlaask()

    return {
      reloadSlaask
    }
  }, [flag, location])
  return (
    <SlaaskContext.Provider value={value}>{children}</SlaaskContext.Provider>
  )
}

export const useSlaask = () => useContext(SlaaskContext)
