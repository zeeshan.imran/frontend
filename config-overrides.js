const { injectBabelPlugin } = require('react-app-rewired')
const rewireLess = require('react-app-rewire-less')
const Theme = require('./src/utils/Theme')

module.exports = {
  webpack: function override (config, env) {
    config = injectBabelPlugin(
      ['import', { libraryName: 'antd', style: true }],
      config
    )

    config = injectBabelPlugin(
      [
        'transform-runtime',
        {
          polyfill: false,
          regenerator: true
        }
      ],
      config
    )

    config = injectBabelPlugin(
      [
        'transform-class-properties',
        {
          spec: true
        }
      ],
      config
    )

    config = injectBabelPlugin(['macros'], config)

    config = rewireLess.withLoaderOptions({
      modifyVars: Theme(),
      javascriptEnabled: true
    })(config, env)

    config = injectBabelPlugin('styled-components', config)

    config = injectBabelPlugin(
      '@quickbaseoss/babel-plugin-styled-components-css-namespace',
      config
    )
    
    config.module.rules.push({
      test: /geojson-[a-z0-9]+\.json$/i,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[name].json',
            outputPath: 'static/geojson'
          }
        }
      ]
    })
    return config
  },
  jest: function (config) {
    config.transformIgnorePatterns = ['node_modules']
    config.setupFiles = ['<rootDir>/src/setupTests.js']
    config.setupTestFrameworkScriptFile = undefined
    return config
  }
}
