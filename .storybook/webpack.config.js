const path = require('path')
const webpack = require('webpack')
const Theme = require('../src/utils/Theme')

module.exports = (storybookBaseConfig, configType) => {
  const DEV = configType === 'DEVELOPMENT'

  storybookBaseConfig.module.rules.push({
    test: /\.(gif|jpe?g|png|svg)$/,
    use: {
      loader: 'url-loader',
      options: { name: '[name].[ext]' }
    }
  })

  storybookBaseConfig.plugins.push(
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(
        process.env.NODE_ENV || 'development'
      )
    })
  )

  storybookBaseConfig.module.rules.push({
    test: /\.(less|css)$/,
    loaders: [
      'style-loader',
      'css-loader',
      {
        loader: 'less-loader',
        options: {
          modifyVars: Theme(),
          javascriptEnabled: true
        }
      }
    ],
    include: path.resolve(__dirname, '../')
  })

  storybookBaseConfig.module.rules.push({
    test: /\.ttf$|\.woff$|\.woff2$/,
    loader: 'file-loader',
    options: {
      name: 'fonts/[name].[ext]'
    },
    include: path.resolve(__dirname, '../')
  })

  return storybookBaseConfig
}
