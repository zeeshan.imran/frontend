import { configure } from '@storybook/react'
import React from 'react'
import styled from 'styled-components'
import { addDecorator } from '@storybook/react'
import '../src/index.css'

const Wrapper = styled.div``

const Container = storyFn => storyFn()

addDecorator(Container)

// automatically import all files ending in *.stories.js
const context = require.context('../src', true, /.stories.js$/)

const loadStories = () => {
  context.keys().forEach(context)
}

configure(loadStories, module)
