;(function (window) {
  var browserDetect = window.browserDetect
  var browser = browserDetect()

  const ua = navigator.userAgent || navigator.vendor || window.opera
  const isInstagram = (ua.indexOf('Instagram') > -1) || (ua.indexOf('Instagram') > -1);
  if (browser.name === 'ie' || isInstagram) {
    window.location = '/browser-recommendations.html'
  }
})(window)
